import { browserHistory } from 'react-router';
import wrappedFetch from '../fetchWrapper';
import AppConfig from '../../config/config';
import handleException from '../handleException';

import ACTIONS from './Actions';
import TmcActionCreator from '../TmcActionCreator';
import i18n from '../../config/i18n';

const INFO = 'info';
const ERROR = 'error';

export default class ActionCreator {
	static readSchedules(environmentId) {
		return dispatch => {
			dispatch(this.loading({ schedulesList: true }));
			return wrappedFetch(
				`${AppConfig.URL.TIPAAS_SERVER_URL}/schedules/list?environment=${environmentId}`,
			)
				.then(response => response.json())
				.then(data => {
					dispatch(this.readSchedulesSuccess(data));
					dispatch(this.loading({ schedulesList: false }));
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(this.readSchedulesFail());
						dispatch(
							TmcActionCreator.showNotification(
								ERROR,
								i18n.t('NOTIFICATION_SCHEDULER_LIST_FETCH_ERROR', {
									defaultValue: 'Get schedulers failed',
								}),
							),
						);
						dispatch(this.loading({ schedulesList: false }));
					}),
				);
		};
	}
	static readSchedulesFail() {
		return {
			type: ACTIONS.READ_SCHEDULES_FAIL,
		};
	}
	static readSchedulesSuccess(payload) {
		return {
			type: ACTIONS.READ_SCHEDULES_SUCCESS,
			payload,
		};
	}

	static loading(loading) {
		return {
			type: ACTIONS.ON_LOAD,
			loading,
		};
	}

	static addExecution(executionType) {
		const paths = browserHistory.getCurrentLocation().pathname.split('/');
		browserHistory.push(`/${paths[1]}/${paths[2]}/add`);
		return {
			type: ACTIONS.EDIT_WIZARD_STATE,
			state: {
				type: executionType,
			},
		};
	}

	static closeWizard() {
		browserHistory.push('/scheduler');
		return {
			type: ACTIONS.WIZARD_CLOSE,
		};
	}

	static readJobs(environmentId) {
		return dispatch => {
			dispatch(this.loading({ jobList: true }));
			return wrappedFetch(
				`${AppConfig.URL.TIPAAS_SERVER_URL}/schedules/jobs?environmentId=${environmentId}`,
			)
				.then(response => response.json())
				.then(data => {
					dispatch(this.readJobsSuccess(data));
					dispatch(this.loading({ jobList: false }));
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(this.readJobsFail());
						dispatch(
							TmcActionCreator.showNotification(
								ERROR,
								i18n.t('NOTIFICATION_JOBS_FETCH_ERROR', { defaultValue: 'Get Jobs failed' }),
							),
						);
						dispatch(this.loading({ jobList: false }));
					}),
				);
		};
	}

	static readJobsFail() {
		return {
			type: ACTIONS.READ_JOBS_FAIL,
		};
	}

	static readJobsSuccess(payload) {
		return {
			type: ACTIONS.READ_JOBS_SUCCESS,
			payload,
		};
	}

	static readWorkspaces(environmentId) {
		return dispatch => {
			dispatch(this.loading({ workspaceList: true }));
			return wrappedFetch(
				`${AppConfig.URL.TIPAAS_SERVER_URL}/workspaces?environmentId=${environmentId}`,
			)
				.then(response => response.json())
				.then(data => {
					dispatch(this.readWorkspacesSuccess(data));
					dispatch(this.loading({ workspaceList: false }));
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(this.readWorkspacesFail());
						dispatch(
							TmcActionCreator.showNotification(
								ERROR,
								i18n.t('NOTIFICATION_WORKSPACES_FETCH_ERROR', {
									defaultValue: "Can't fetch workspaces",
								}),
							),
						);
						dispatch(this.loading({ workspaceList: false }));
					}),
				);
		};
	}

	static readWorkspacesFail() {
		return {
			type: ACTIONS.READ_WORKSPACES_FAIL,
		};
	}

	static readWorkspacesSuccess(payload) {
		return {
			type: ACTIONS.READ_WORKSPACES_SUCCESS,
			payload,
		};
	}

	static readEngines(environmentId) {
		return dispatch => {
			dispatch(this.loading({ engineList: true }));
			return wrappedFetch(
				`${
					AppConfig.URL.TIPAAS_SERVER_URL
				}/user/remote/engines/available?environmentId=${environmentId}`,
			)
				.then(response => response.json())
				.then(data => {
					dispatch(this.readEnginesSuccess(data));
					dispatch(this.loading({ engineList: false }));
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(this.readEnginesFail());
						dispatch(
							TmcActionCreator.showNotification(
								ERROR,
								i18n.t('NOTIFICATION_ENGINES_FETCH_ERROR', { defaultValue: "Can't fetch engines" }),
							),
						);
						dispatch(this.loading({ engineList: false }));
					}),
				);
		};
	}

	static readClusters(environmentId) {
		return dispatch => {
			dispatch(this.loading({ engineClusterList: true }));
			return wrappedFetch(
				`${
					AppConfig.URL.TIPAAS_SERVER_URL
				}/user/remote/clusters/available?environmentId=${environmentId}`,
			)
				.then(response => response.json())
				.then(data => {
					dispatch(this.readClustersSuccess(data));
					dispatch(this.loading({ engineClusterList: false }));
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(this.readClustersFail());
						dispatch(
							TmcActionCreator.showNotification(
								ERROR,
								i18n.t('NOTIFICATION_CLUSTERS_FETCH_ERROR', {
									defaultValue: 'Get remote Engines clusters failed',
								}),
							),
						);
						dispatch(this.loading({ engineClusterList: false }));
					}),
				);
		};
	}

	static readEnginesFail() {
		return {
			type: ACTIONS.READ_ENGINES_FAIL,
		};
	}

	static readEnginesSuccess(payload) {
		return {
			type: ACTIONS.READ_ENGINES_SUCCESS,
			payload,
		};
	}

	static readClustersFail() {
		return {
			type: ACTIONS.READ_CLUSTERS_FAIL,
		};
	}

	static readClustersSuccess(payload) {
		return {
			type: ACTIONS.READ_CLUSTERS_SUCCESS,
			payload,
		};
	}

	static editFlow(flow) {
		return {
			type: ACTIONS.EDIT_FLOW,
			flow,
		};
	}

	static editSchedule(schedule) {
		return {
			type: ACTIONS.EDIT_SCHEDULE,
			schedule,
		};
	}

	static editExecutionEnvironment(destination, engineId) {
		return {
			type: ACTIONS.EDIT_EXECUTION_ENVIRONMENT,
			destination,
			engineId,
		};
	}

	static runFlow(execution, workspaceId) {
		return dispatch => {
			dispatch(this.loading({ runFlow: true }));
			return wrappedFetch(
				`${AppConfig.URL.TIPAAS_SERVER_URL}/workspaces/${workspaceId}/flows/${
					execution.flow.id
				}/run`,
				{
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
					},
					method: 'PUT',
					body: JSON.stringify(execution),
				},
			)
				.then(response => response.json())
				.then(() => {
					dispatch(this.loading({ runFlow: false }));
					dispatch(
						TmcActionCreator.showNotification(
							INFO,
							i18n.t('NOTIFICATION_FLOW_RUN_SUCCESS', {
								defaultValue: 'Execution was successfully runned',
							}),
						),
					);
					dispatch(this.closeWizard());
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(this.loading({ runFlow: false }));
						dispatch(
							TmcActionCreator.showNotification(
								ERROR,
								i18n.t('NOTIFICATION_FLOW_RUN_ERROR', {
									defaultValue: 'Error occurred while running execution',
								}),
							),
						);
					}),
				);
		};
	}

	static readFlow(flowId, workspaceId) {
		return dispatch => {
			dispatch(this.loading({ flow: true }));
			return wrappedFetch(
				`${AppConfig.URL.TIPAAS_SERVER_URL}/workspaces/${workspaceId}/flows/${flowId}`,
			)
				.then(response => response.json())
				.then(data => {
					dispatch(this.readFlowSuccess(data));
					dispatch(this.loading({ flow: false }));
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(this.readFlowFail());
						dispatch(
							TmcActionCreator.showNotification(
								ERROR,
								i18n.t('NOTIFICATION_FLOW_READ_ERROR', { defaultValue: 'Get flow failed' }),
							),
						);
						dispatch(this.loading({ flow: false }));
					}),
				);
		};
	}

	static readFlowFail() {
		return {
			type: ACTIONS.READ_FLOW_FAIL,
		};
	}

	static readFlowSuccess(payload) {
		return {
			type: ACTIONS.READ_FLOW_SUCCESS,
			payload,
		};
	}

	static createFlow(flow) {
		return dispatch => {
			dispatch(this.loading({ createFlow: true }));
			return wrappedFetch(
				`${AppConfig.URL.TIPAAS_SERVER_URL}/workspaces/${flow.workspaceId}/flows`,
				{
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
					},
					method: 'POST',
					body: JSON.stringify(flow),
				},
			)
				.then(response => {
					const location = response.headers.get('Location');
					let id;
					if (location) {
						id = location.substring(location.lastIndexOf('/') + 1, location.length);
					}
					return id;
				})
				.then(data => {
					dispatch(
						TmcActionCreator.showNotification(
							INFO,
							i18n.t('NOTIFICATION_FLOW_CREATE_SUCCESS', {
								defaultValue: 'Execution was successfully created',
							}),
						),
					);
					dispatch(this.loading({ createFlow: false }));
					dispatch(
						this.editFlow({
							...flow,
							id: data,
							content: {
								...flow.content,
								id: data,
							},
						}),
					);
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(this.loading({ createFlow: false }));
						dispatch(
							TmcActionCreator.showNotification(
								ERROR,
								i18n.t('NOTIFICATION_FLOW_CREATE_ERROR', {
									defaultValue: 'Error occurred while creating execution',
								}),
							),
						);
					}),
				);
		};
	}

	static saveFlow(flow) {
		return dispatch => {
			dispatch(this.loading({ saveFlow: true }));
			return wrappedFetch(
				`${AppConfig.URL.TIPAAS_SERVER_URL}/workspaces/${flow.workspaceId}/flows/${flow.id}`,
				{
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
					},
					method: 'PUT',
					body: JSON.stringify(flow),
				},
			)
				.then(response => response.json())
				.then(data => {
					dispatch(
						TmcActionCreator.showNotification(
							INFO,
							i18n.t('NOTIFICATION_FLOW_SAVE_SUCCESS', {
								defaultValue: 'Execution was successfully saved',
							}),
						),
					);
					dispatch(this.readFlowSuccess(data));
					dispatch(this.loading({ saveFlow: false }));
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(
							TmcActionCreator.showNotification(
								ERROR,
								i18n.t('NOTIFICATION_FLOW_SAVE_ERROR', {
									defaultValue: 'Error occurred while saving execution',
								}),
							),
						);
						dispatch(this.loading({ saveFlow: false }));
					}),
				);
		};
	}

	static createWebhook(flowId, workspaceId, webhook) {
		return dispatch => {
			dispatch(this.loading({ createWebhook: true }));
			return wrappedFetch(
				`${AppConfig.URL.TIPAAS_SERVER_URL}/workspaces/${workspaceId}/flow/${flowId}/webhook`,
				{
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
					},
					method: 'POST',
					body: JSON.stringify(webhook),
				},
			)
				.then(data => {
					dispatch(
						TmcActionCreator.showNotification(
							INFO,
							i18n.t('NOTIFICATION_WEBHOOK_CREATE_SUCCESS', {
								defaultValue: 'Webhook was successfully created',
							}),
						),
					);
					dispatch(this.onWebhookChange({ ...webhook, id: data }));
					dispatch(this.loading({ createWebhook: false }));
					dispatch(this.closeWizard());
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(this.loading({ createWebhook: false }));
						dispatch(
							TmcActionCreator.showNotification(
								ERROR,
								i18n.t('NOTIFICATION_WEBHOOK_CREATE_ERROR', {
									defaultValue: 'Error occurred while creating webhook',
								}),
							),
						);
					}),
				);
		};
	}

	static readWebhook(flowId, workspaceId) {
		return dispatch => {
			dispatch(this.loading({ webhook: true }));
			return wrappedFetch(
				`${AppConfig.URL.TIPAAS_SERVER_URL}/workspaces/${workspaceId}/flow/${flowId}/webhook`,
			)
				.then(response => response.json())
				.then(data => {
					dispatch(this.readWebhookSuccess(data));
					dispatch(this.loading({ webhook: false }));
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(
							TmcActionCreator.showNotification(
								ERROR,
								i18n.t('NOTIFICATION_WEBHOOK_READ_ERROR', { defaultValue: 'Get webhook failed' }),
							),
						);
						dispatch(this.readWebhookFail());
						dispatch(this.loading({ webhook: false }));
					}),
				);
		};
	}

	static readWebhookSuccess(webhook) {
		return {
			type: ACTIONS.READ_WEBHOOK_SUCCESS,
			webhook,
		};
	}

	static readWebhookFail() {
		return {
			type: ACTIONS.READ_WEBHOOK_FAIL,
		};
	}

	static onWebhookChange(webhook) {
		return {
			type: ACTIONS.WEBHOOK_CHANGE,
			webhook,
		};
	}

	static stopSchedule(environmentId, workspaceId, flowId) {
		return dispatch => {
			dispatch(this.loading({ stopSchedule: true }));
			return wrappedFetch(
				`${AppConfig.URL.TIPAAS_SERVER_URL}/workspaces/${workspaceId}/flows/${flowId}/stopSchedule`,
			)
				.then(response => response.json())
				.then(data => {
					dispatch(
						TmcActionCreator.showNotification(
							INFO,
							i18n.t('NOTIFICATION_SCHEDULES_STOP_SUCCESS', {
								defaultValue: 'Schedule was successfully stopped',
							}),
						),
					);
					dispatch(this.shopScheduleSuccess(data));
					dispatch(this.readSchedules(environmentId));
					dispatch(this.loading({ stopSchedule: false }));
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(this.shopScheduleFail());
						dispatch(
							TmcActionCreator.showNotification(
								ERROR,
								i18n.t('NOTIFICATION_SCHEDULES_STOP_FAILURE', {
									defaultValue: 'Stop schedule failed',
								}),
							),
						);
						dispatch(this.loading({ stopSchedule: false }));
					}),
				);
		};
	}
	static shopScheduleFail() {
		return {
			type: ACTIONS.STOP_SCHEDULE_FAIL,
		};
	}
	static shopScheduleSuccess(payload) {
		return {
			type: ACTIONS.STOP_SCHEDULE_SUCCESS,
			payload,
		};
	}
	static editWizardState(state) {
		return {
			type: ACTIONS.EDIT_WIZARD_STATE,
			state,
		};
	}

	static readWorkspaceRoles() {
		return dispatch => {
			dispatch(this.loading({ workspaceRoles: true }));
			return wrappedFetch(`${AppConfig.URL.TIPAAS_SERVER_URL}/user/workspace-roles`)
				.then(response => response.json())
				.then(data => {
					dispatch(this.readWorkspaceRolesSuccess(data));
					dispatch(this.loading({ workspaceRoles: false }));
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(this.readWorkspaceRolesFail());
						dispatch(this.loading({ workspaceRoles: false }));
					}),
				);
		};
	}

	static readWorkspaceRolesFail() {
		return {
			type: ACTIONS.READ_WORKSPACE_ROLES_FAIL,
		};
	}

	static readWorkspaceRolesSuccess(payload) {
		return {
			type: ACTIONS.READ_WORKSPACE_ROLES_SUCCESS,
			payload,
		};
	}

	static editExecution(environmentId, executionInfo) {
		return dispatch => {
			dispatch(this.readFlow(executionInfo.id, executionInfo.workspace.id)).then(() => {
				const paths = browserHistory.getCurrentLocation().pathname.split('/');
				browserHistory.push(`/${paths[1]}/${paths[2]}/edit/${executionInfo.id}`);
			});
			dispatch(this.editWizardState({ action: 'edit', type: executionInfo.type }));
			dispatch(
				this.editExecutionEnvironment(
					executionInfo.scheduleModel.destination,
					executionInfo.scheduleModel.engineId,
				),
			);
			dispatch(this.editSchedule(executionInfo.scheduleModel));
		};
	}

	static readUsersByRole(workspaceId, role) {
		return dispatch => {
			dispatch(this.loading({ userList: true }));
			return wrappedFetch(
				`${AppConfig.URL.TIPAAS_SERVER_URL}/workspaces/${workspaceId}/usersByRole/${role}`,
			)
				.then(response => response.json())
				.then(data => {
					dispatch(this.readUserListSuccess(data));
					dispatch(this.loading({ userList: false }));
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(
							TmcActionCreator.showNotification(
								ERROR,
								i18n.t('NOTIFICATION_USERS_READ_ERROR', { defaultValue: 'Get user list failed' }),
							),
						);
						dispatch(this.readUserListFail());
						dispatch(this.loading({ userList: false }));
					}),
				);
		};
	}

	static readUserListFail() {
		return {
			type: ACTIONS.READ_USER_LIST_FAIL,
		};
	}

	static readUserListSuccess(payload) {
		return {
			type: ACTIONS.READ_USER_LIST_SUCCESS,
			payload,
		};
	}

	static readConnections(environmentId, app) {
		return dispatch => {
			const loading = {};
			loading[`connections_${app}`] = true;
			dispatch(this.loading(loading));
			return wrappedFetch(
				`${
					AppConfig.URL.TIPAAS_SERVER_URL
				}/schedules/connections?environment=${environmentId}&app=${app}`,
			)
				.then(response => response.json())
				.then(data => {
					dispatch(this.readConnectionsSuccess(app, data));
					loading[`connections_${app}`] = false;
					dispatch(this.loading(loading));
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(
							TmcActionCreator.showNotification(
								ERROR,
								i18n.t('NOTIFICATION_CONNECTIONS_FETCH_ERROR', {
									defaultValue: 'Get connections failed',
								}),
							),
						);
						dispatch(this.readConnectionsFail(app));
						loading[`connections_${app}`] = false;
						dispatch(this.loading(loading));
					}),
				);
		};
	}

	static readConnectionsFail(app) {
		return {
			type: ACTIONS.READ_CONNECTIONS_FAIL,
			app,
		};
	}

	static readConnectionsSuccess(app, payload) {
		return {
			type: ACTIONS.READ_CONNECTIONS_SUCCESS,
			app,
			payload,
		};
	}

	static readResources(workspaceId) {
		return dispatch => {
			dispatch(this.loading({ resources: true }));
			return wrappedFetch(
				`${AppConfig.URL.TIPAAS_SERVER_URL}/workspaces/${workspaceId}/custom-resources/listForUse`,
			)
				.then(response => response.json())
				.then(data => {
					dispatch(this.readResourcesSuccess(data));
					dispatch(this.loading({ resources: false }));
				})
				.catch(exception =>
					handleException(exception).then(() => {
						dispatch(
							TmcActionCreator.showNotification(
								ERROR,
								i18n.t('NOTIFICATION_RESOURCES_FETCH_ERROR', {
									defaultValue: 'Get resources failed',
								}),
							),
						);
						dispatch(this.readResourcesFail());
						dispatch(this.loading({ resources: false }));
					}),
				);
		};
	}

	static readResourcesFail() {
		return {
			type: ACTIONS.READ_RESOURCES_FAIL,
		};
	}

	static readResourcesSuccess(payload) {
		return {
			type: ACTIONS.READ_RESOURCES_SUCCESS,
			payload,
		};
	}
}
