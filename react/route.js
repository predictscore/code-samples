import { call, put, select, takeEvery, takeLatest, all } from 'redux-saga/effects';
import { differenceWith, isEqual, chunk, filter } from 'lodash';
import { sagas } from '@talend/react-cmf';
import AppConfig from '../../config/config';
import i18n from '../../config/i18n';
import { insertParams, getLocalizedMoment, getSearchParams } from '../../utils/searchParams';
import IAM_ACTIONS from '../../actions/IAM/IamActions';
import { loadLicenseInformation } from '../subscription/licenses';
import { getPreviousRoute, showErrorNotification, showWarningNotification, showSuccessNotification } from '../commons/helpers';
import { deleteResources } from './iam';
import { loadUserDetails } from './users';

import trimObjectFields from '../../utils/trimObjectFields';
import IamActionCreator from '../../actions/IAM/IamActionCreator';

export function getRolesParams(state) {
	const roleParams = getSearchParams(state);
	roleParams.applicationName = state.iam.application;

	if (state.iam.filter && state.iam.filter.length > 1) {
		roleParams.roleName = state.iam.filter;
	}

	return roleParams;
}

export function* loadRoles() {
	const state = yield select();
	const params = getRolesParams(state);

	const roles = yield call(
		sagas.http.get,
		insertParams(`${AppConfig.URL.IAM_SERVER_URL}/roles`, params),
	);

	if (!roles.response.ok) {
		yield call(
			showErrorNotification,
			i18n.t(
				'NOTIFICATION_IAM_ROLES_FETCH_FAILURE',
				{ defaultValue: 'Can\'t fetch roles' }
			)
		);
	}

	const items = roles.data.Resources.map(item =>
		Object.assign({}, item, {
			lastModified: getLocalizedMoment()(item.lastModified).fromNow(),
		}),
	);

	const rolesData = { items, totalResults: roles.data.totalResults };
	yield put({
		type: IAM_ACTIONS.SEARCH_RESOURCES_SUCCESS,
		payload: rolesData,
	});
}

export function* loadRolesOptions() {
	const params = yield select(getRolesParams);
	const rolesOptions = yield call(
		sagas.http.get,
		`${AppConfig.URL.IAM_SERVER_URL}/roles/options?applicationName=${params.applicationName}`,
	);
	if (!rolesOptions.response.ok) {
		yield call(showErrorNotification,
			i18n.t('NOTIFICATION_IAM_FETCH_FORM_OPTIONS_ERROR',
			{ defaultValue: 'Can\'t fetch roles options' })
		);
	}
	yield put({
		type: IAM_ACTIONS.READ_FORM_OPTIONS_SUCCESS,
		payload: rolesOptions.data,
	});
}

export function* loadRoleDetails(id) {
	let roleDetails;
	yield put({
		type: IAM_ACTIONS.SEARCH_ROLE_REQUEST,
	});
	try {
		roleDetails = yield call(
			sagas.http.get,
			`${AppConfig.URL.IAM_SERVER_URL}/roles/${id}`,
		);
	} catch (e) {
		yield call(
			showErrorNotification,
			i18n.t(
				'NOTIFICATION_IAM_FETCH_FORM_OPTIONS_ERROR',
				{ defaultValue: 'Can\'t fetch form options' }
			)
		);
		yield put({
			type: IAM_ACTIONS.SEARCH_ROLE_FAIL,
		});
	}

	if (!roleDetails.response.ok) {
		yield put({
			type: '@@HTTP/ERRORS',
			error: roleDetails,
		});
		yield call(
			showErrorNotification,
			i18n.t(
				'NOTIFICATION_IAM_FETCH_FORM_OPTIONS_ERROR',
				{ defaultValue: 'Can\'t fetch roles options' }
			)
		);
	}
	yield put({
		type: IAM_ACTIONS.READ_ROLES_SUCCESS,
		payload: roleDetails.data,
	});
}

export function* loadPermissionDetails(id) {
	const rolesOptions = yield call(
		sagas.http.get,
		`${AppConfig.URL.IAM_SERVER_URL}/roles/${id}/permissions`,
	);
	if (!rolesOptions.response.ok) {
		yield call(showErrorNotification,
			i18n.t(
				'NOTIFICATION_IAM_FETCH_FORM_OPTIONS_ERROR',
				{ defaultValue: 'Can\'t fetch roles options' }
			)
		);
	}
	yield put({
		type: IAM_ACTIONS.READ_FORM_OPTIONS_SUCCESS,
		payload: rolesOptions.data,
	});
}

export const selectSavedUsers = state => state.iam.savedUsers;
export const selectFormDataUsers = state => state.iam.formData.users;
export const selectFetchedUsers = state => state.iam.users;
export const selectCurrentProfileId = state => state.tmc.profile.id;

export function* checkEmptyRoles() {
	const savedUsers = yield select(selectSavedUsers);
	const selectedUsers = yield select(selectFormDataUsers);
	const unassignedUsers = differenceWith(savedUsers, selectedUsers, isEqual);

	if (unassignedUsers.length === 0) {
		return [];
	}

	const chunks = chunk(unassignedUsers, 10);
	for (let i = 0, len = chunks.length; i < len; i += 1) {
		yield all(chunks[i].map(item => (
			call(loadUserDetails, item)
		)));
	}

	const fetchedUsers = yield select(selectFetchedUsers);
	const warningList = filter(fetchedUsers, item => item.roles.length === 1);

	if (warningList.length === 0) {
		return [];
	}

	const currentProfileId = yield select(selectCurrentProfileId);
	if (warningList.some(item => item.id === currentProfileId)) {
		yield call(showErrorNotification, i18n.t('NOTIFICATION_IAM_USER_NEEDS_AT_LEAST_ONE_ROLE', {
			defaultValue: 'You need at least one role to be able to login',
		}));
		return [currentProfileId];
	}

	yield call(showWarningNotification, i18n.t('NOTIFICATION_IAM_USER_CAN_NOT_LOGIN_WITHOUT_ROLES', {
		defaultValue: 'User cannot log in until a role is assigned',
	}));

	return [];
}

export function* updateRole(action) {
	const revertUsers = yield call(checkEmptyRoles);
	const roleName = yield select(state => state.iam.application);
	let updateRoleResult;
	const body = trimObjectFields({
		...action.payload,
		users: [...action.payload.users, ...revertUsers],
	});
	try {
		updateRoleResult = yield call(
			sagas.http.put,
			`${AppConfig.URL.IAM_SERVER_URL}/roles/${action.payload.id}`,
			body,
			null,
			true,
		);
	} catch (e) {
		yield call(showErrorNotification,
			i18n.t('NOTIFICATION_IAM_ROLE_UPDATE_ERROR',
			{ defaultValue: 'Failed to update role.' })
		);
		yield put({
			type: IAM_ACTIONS.UPDATE_RESOURCE_FAIL,
		});
	}

	if (!updateRoleResult.response.ok) {
		yield call(showErrorNotification,
			updateRoleResult.data
			? updateRoleResult.data.detail
			: (i18n.t('NOTIFICATION_IAM_ROLE_UPDATE_ERROR', {
				defaultValue: 'Failed to update role.',
			})));
		yield put({
			type: IAM_ACTIONS.UPDATE_RESOURCE_FAIL,
		});
	} else {
		yield call(showSuccessNotification,
			i18n.t('NOTIFICATION_IAM_ROLE_UPDATE_SUCCESS',
			{ defaultValue: 'Role successfully updated.' })
		);
		yield put({
			type: IAM_ACTIONS.UPDATE_ROLE_SUCCESS,
			cmf: {
				routerReplace: `/roles/${roleName}`,
			},
		});
	}
}

export function* createRole(action) {
	const roleName = yield select(state => state.iam.application);
	const body = trimObjectFields(action.payload.data);
	let createRoleResult;
	try {
		createRoleResult = yield call(
			sagas.http.post,
			`${AppConfig.URL.IAM_SERVER_URL}/roles`,
			body,
		);
	} catch (e) {
		yield call(
			showErrorNotification,
			i18n.t('NOTIFICATION_IAM_ROLE_POST_FAILURE', {
				defaultValue: 'Failed to create role.',
			})
		);
		yield put({
			type: IAM_ACTIONS.CREATE_RESOURCE_FAIL,
		});
		return;
	}

	if (!createRoleResult.response.ok) {
		yield call(
			showErrorNotification,
			createRoleResult.data
			? createRoleResult.data.detail
			: (i18n.t('NOTIFICATION_IAM_ROLE_POST_FAILURE',
				{ defaultValue: 'Failed to create role.' })
			)
		);
		yield put({
			type: IAM_ACTIONS.CREATE_RESOURCE_FAIL,
		});
	} else {
		yield call(
			showSuccessNotification,
			i18n.t('NOTIFICATION_IAM_ROLE_POST_SUCCESS',
			{ defaultValue: 'Role successfully created.' }
		));
		if (action.payload.isCreateAnother) {
			yield put({
				type: IAM_ACTIONS.CREATE_ROLE_SUCCESS,
			});
		} else {
			yield put({
				type: IAM_ACTIONS.CREATE_ROLE_SUCCESS,
				cmf: {
					routerReplace: `/roles/${roleName}`,
				},
			});
		}
	}
}

export function* searchRoles() {
	yield all([
		call(loadRoles),
		call(loadRolesOptions),
	]);
}

const rolesWatchers = [
	takeEvery(IAM_ACTIONS.UPDATE_ROLE_REQUEST, updateRole),
	takeEvery(IAM_ACTIONS.CREATE_ROLE_REQUEST, createRole),
	takeLatest(IAM_ACTIONS.DELETE_RESOURCES_REQUEST, deleteResources),
];

export const routes = {
	'/roles/:roleName': function* loadRolesWatch({ roleName }) {
		yield all(rolesWatchers);
		if (!getPreviousRoute().startsWith(`/roles/${roleName}`)) {
			yield put(IamActionCreator.searchRolesRequest(roleName));
		}
	},
	'/newroles': function* loadRolesWatch() {
		yield all(rolesWatchers);
		if (!getPreviousRoute().startsWith('/newroles')) {
			yield put(IamActionCreator.searchRolesRequest());
		}
	},
	'/roles/:roleName/:id': function* loadRoleDetailsWatch({ id }) {
		if (id !== 'add') {
			yield loadRoleDetails(id);
		}
		yield loadLicenseInformation();
	},
	'/roles/:roleName/:id/permission': function* loadPermissionDetailsWatch({ id }) {
		yield loadPermissionDetails(id);
	},
	'/newroles/:id': function* loadRoleDetailsWatch({ id }) {
		if (id !== 'add') {
			yield loadRoleDetails(id);
		}
		yield loadLicenseInformation();
	},
	'/newroles/:id/permission': function* loadPermissionDetailsWatch({ id }) {
		yield loadPermissionDetails(id);
	},
};
