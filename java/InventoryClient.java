package org.talend.ipaas.services.inventory.client;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.StatusType;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import org.apache.cxf.configuration.security.AuthorizationPolicy;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.talend.ipaas.model.metadata.Action;
import org.talend.ipaas.services.inventory.InventoryException;
import org.talend.ipaas.services.inventory.InventoryService;
import org.talend.ipaas.services.inventory.InventoryServiceAccelerate;
import org.talend.ipaas.services.inventory.Workspace;
import org.talend.ipaas.services.inventory.client.impl.InventoryExceptionMapper;

public class InventoryClient implements InventoryService {

    // https://jira.talendforge.org/browse/TIPAAS-931 [publish] Exception when send a null as multipart
    private static final InputStream EMPTY_STREAM = new ByteArrayInputStream(new byte[0]);

    private final String address;
    private final String username;
    private final String password;

    private boolean acceleratePublish;

    private InventoryServiceAccelerate inventoryService;

    public InventoryClient(String address, String username, String password) {
        this.address = address;
        this.username = username;
        this.password = password;
    }

    public void setAcceleratePublish(boolean accelerate) {
        acceleratePublish = accelerate;
    }

    @Override
    public Map<String, Boolean> getStatus() throws InventoryException {
        return getInventoryService().getStatus();
    }

    @Override
    public Collection<Workspace> getAvailableWorkspacesInfo(String id, String groupId) throws InventoryException {
        try {
            return getInventoryService().getAvailableWorkspacesInfo(id, groupId);
        } catch (WebApplicationException e) {
            throw convertToInventoryException(e);
        }
    }

    @Override
    public String getLatestActionVersion(String id, String groupId) throws InventoryException {
        try {
            return getInventoryService().getLatestActionVersion(id, groupId);
        } catch (WebApplicationException e) {
            throw convertToInventoryException(e);
        }
    }

    @Override
    public String publishAction(
            Action action,
            InputStream actionArtifact,
            InputStream screenshot,
            InputStream image,
            InputStream source,
            String workspace,
            boolean createOrUpdateFlow)
            throws InventoryException {
        if (acceleratePublish) {
            String acceleratePublishId = action.getId() + '_' + UUID.randomUUID();

            accelerateUploadArtifact(actionArtifact, acceleratePublishId);

            return getInventoryService().acceleratePublishAction(acceleratePublishId, action,
                    noStreamCheck(screenshot),
                    noStreamCheck(image),
                    noStreamCheck(source),
                    workspace, createOrUpdateFlow);
        } else {
            try {
                return getInventoryService().publishAction(
                        action,
                        actionArtifact,
                        noStreamCheck(screenshot),
                        noStreamCheck(image),
                        noStreamCheck(source),
                        workspace,
                        createOrUpdateFlow);
            } catch (WebApplicationException e) {
                throw convertToInventoryException(e);
            }
        }
    }

    private void accelerateUploadArtifact(InputStream actionArtifact, String acceleratePublishId)
            throws InventoryException {
        try {
            final URL accelerateUploadUrl = new URL(
                    getInventoryService().getAcceleratePublishUploadUrl(acceleratePublishId));

            final HttpURLConnection connection = (HttpURLConnection) accelerateUploadUrl.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("PUT");
            connection.setFixedLengthStreamingMode(actionArtifact.available());
            try (OutputStream out = connection.getOutputStream()) {
                IOUtils.copy(actionArtifact, out);
            }

            int responseStatus = connection.getResponseCode();
            if (responseStatus == -1 || connection.getResponseCode() >= 400) {
                throw new InventoryException("failed to accelerate upload artifact - " + responseStatus);
            }
        } catch (IOException e) {
            throw new InventoryException("failed to accelerate upload artifact - " + e.getMessage());
        }
    }

    private InventoryServiceAccelerate getInventoryService() {
        if (null == inventoryService) {
            final JacksonJsonProvider jacksonJsonProvider = new JacksonJsonProvider();
            jacksonJsonProvider.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            inventoryService = JAXRSClientFactory.create(address, InventoryServiceAccelerate.class,
                    Arrays.asList(jacksonJsonProvider, new InventoryExceptionMapper()));

            final AuthorizationPolicy authorizationPolicy = new AuthorizationPolicy();
            authorizationPolicy.setUserName(username);
            authorizationPolicy.setPassword(password);
            //authorizationPolicy.setAuthorizationType(securityType);
            WebClient.getConfig(inventoryService).getHttpConduit().setAuthorization(authorizationPolicy);
        }
        return inventoryService;
    }

    private static InventoryException convertToInventoryException(WebApplicationException e) {
        StatusType statusType = e.getResponse().getStatusInfo();
        String message = (429 == statusType.getStatusCode()) ? "Too Many Requests" : statusType.getReasonPhrase();
//        if (e.getResponse().hasEntity()) {
//            String response = e.getResponse().readEntity(String.class);
//            if (response.length() > 0) {
//                message += ": " + response; //$NON-NLS-1$
//            }
//        }
        return new InventoryException(message, e);
    }

    private static InputStream noStreamCheck(InputStream stream) {
        return null == stream ? EMPTY_STREAM : stream;
    }

}

