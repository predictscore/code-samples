package org.talend.ipaas.api.execution.model;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Environment info")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EnvironmentInfo {

    private String id;

    private String name;

    private String description;

    private Boolean isDefault;


    @ApiModelProperty(value = "Environment identifier", required = true,
            example = "57f64991e4b0b689a64feed3")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @XmlElement(name = "name")
    @ApiModelProperty(value = "Environment name", required = true,
            example = "Development")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @XmlElement(name = "description")
    @ApiModelProperty(value = "Environment description", required = false,
            example = "environment detail description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @XmlElement(name = "default")
    @ApiModelProperty(value = "Default environment flag", required = false)
    public Boolean getDefault() { // "get" for FIQL
        return isDefault;
    }

    public void setDefault(Boolean flag) {
        this.isDefault = flag;
    }

}
