'use strict';

describe('Controller Tests', function() {

    describe('NewsToRelatedAuthor Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockNewsToRelatedAuthor, MockNews, MockAuthor;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockNewsToRelatedAuthor = jasmine.createSpy('MockNewsToRelatedAuthor');
            MockNews = jasmine.createSpy('MockNews');
            MockAuthor = jasmine.createSpy('MockAuthor');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'NewsToRelatedAuthor': MockNewsToRelatedAuthor,
                'News': MockNews,
                'Author': MockAuthor
            };
            createController = function() {
                $injector.get('$controller')("NewsToRelatedAuthorDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:newsToRelatedAuthorUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
