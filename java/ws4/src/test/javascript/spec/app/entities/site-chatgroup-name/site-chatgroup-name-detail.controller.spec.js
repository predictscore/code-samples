'use strict';

describe('Controller Tests', function() {

    describe('SiteChatgroupName Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockSiteChatgroupName, MockSiteChatgroup;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockSiteChatgroupName = jasmine.createSpy('MockSiteChatgroupName');
            MockSiteChatgroup = jasmine.createSpy('MockSiteChatgroup');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'SiteChatgroupName': MockSiteChatgroupName,
                'SiteChatgroup': MockSiteChatgroup
            };
            createController = function() {
                $injector.get('$controller')("SiteChatgroupNameDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:siteChatgroupNameUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
