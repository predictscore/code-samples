'use strict';

describe('Controller Tests', function() {

    describe('SiteChatgroupNumVisitors Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockSiteChatgroupNumVisitors, MockSiteChatgroup;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockSiteChatgroupNumVisitors = jasmine.createSpy('MockSiteChatgroupNumVisitors');
            MockSiteChatgroup = jasmine.createSpy('MockSiteChatgroup');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'SiteChatgroupNumVisitors': MockSiteChatgroupNumVisitors,
                'SiteChatgroup': MockSiteChatgroup
            };
            createController = function() {
                $injector.get('$controller')("SiteChatgroupNumVisitorsDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:siteChatgroupNumVisitorsUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
