'use strict';

describe('Controller Tests', function() {

    describe('AuthorToLanguage Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockAuthorToLanguage, MockAuthor, MockLanguage;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockAuthorToLanguage = jasmine.createSpy('MockAuthorToLanguage');
            MockAuthor = jasmine.createSpy('MockAuthor');
            MockLanguage = jasmine.createSpy('MockLanguage');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'AuthorToLanguage': MockAuthorToLanguage,
                'Author': MockAuthor,
                'Language': MockLanguage
            };
            createController = function() {
                $injector.get('$controller')("AuthorToLanguageDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:authorToLanguageUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
