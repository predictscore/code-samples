'use strict';

describe('Controller Tests', function() {

    describe('News Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockNews, MockAuthor, MockSiteChatgroup, MockLocation, MockNewsComment, MockNewsAttachment, MockNewsOriginalImage, MockNewsPublicImage, MockNewsUrl, MockNewsDownloadableContentUrl, MockNewsConfidentialImage;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockNews = jasmine.createSpy('MockNews');
            MockAuthor = jasmine.createSpy('MockAuthor');
            MockSiteChatgroup = jasmine.createSpy('MockSiteChatgroup');
            MockLocation = jasmine.createSpy('MockLocation');
            MockNewsComment = jasmine.createSpy('MockNewsComment');
            MockNewsAttachment = jasmine.createSpy('MockNewsAttachment');
            MockNewsOriginalImage = jasmine.createSpy('MockNewsOriginalImage');
            MockNewsPublicImage = jasmine.createSpy('MockNewsPublicImage');
            MockNewsUrl = jasmine.createSpy('MockNewsUrl');
            MockNewsDownloadableContentUrl = jasmine.createSpy('MockNewsDownloadableContentUrl');
            MockNewsConfidentialImage = jasmine.createSpy('MockNewsConfidentialImage');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'News': MockNews,
                'Author': MockAuthor,
                'SiteChatgroup': MockSiteChatgroup,
                'Location': MockLocation,
                'NewsComment': MockNewsComment,
                'NewsAttachment': MockNewsAttachment,
                'NewsOriginalImage': MockNewsOriginalImage,
                'NewsPublicImage': MockNewsPublicImage,
                'NewsUrl': MockNewsUrl,
                'NewsDownloadableContentUrl': MockNewsDownloadableContentUrl,
                'NewsConfidentialImage': MockNewsConfidentialImage
            };
            createController = function() {
                $injector.get('$controller')("NewsDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:newsUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
