'use strict';

describe('Controller Tests', function() {

    describe('SiteChatgroupNumUsers Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockSiteChatgroupNumUsers, MockSiteChatgroup;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockSiteChatgroupNumUsers = jasmine.createSpy('MockSiteChatgroupNumUsers');
            MockSiteChatgroup = jasmine.createSpy('MockSiteChatgroup');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'SiteChatgroupNumUsers': MockSiteChatgroupNumUsers,
                'SiteChatgroup': MockSiteChatgroup
            };
            createController = function() {
                $injector.get('$controller')("SiteChatgroupNumUsersDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:siteChatgroupNumUsersUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
