'use strict';

describe('Controller Tests', function() {

    describe('SiteChatgroupToTag Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockSiteChatgroupToTag, MockSiteChatgroup, MockTag;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockSiteChatgroupToTag = jasmine.createSpy('MockSiteChatgroupToTag');
            MockSiteChatgroup = jasmine.createSpy('MockSiteChatgroup');
            MockTag = jasmine.createSpy('MockTag');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'SiteChatgroupToTag': MockSiteChatgroupToTag,
                'SiteChatgroup': MockSiteChatgroup,
                'Tag': MockTag
            };
            createController = function() {
                $injector.get('$controller')("SiteChatgroupToTagDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:siteChatgroupToTagUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
