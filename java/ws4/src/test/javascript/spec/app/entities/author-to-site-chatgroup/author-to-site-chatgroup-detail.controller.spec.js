'use strict';

describe('Controller Tests', function() {

    describe('AuthorToSiteChatgroup Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockAuthorToSiteChatgroup, MockAuthor, MockSiteChatgroup;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockAuthorToSiteChatgroup = jasmine.createSpy('MockAuthorToSiteChatgroup');
            MockAuthor = jasmine.createSpy('MockAuthor');
            MockSiteChatgroup = jasmine.createSpy('MockSiteChatgroup');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'AuthorToSiteChatgroup': MockAuthorToSiteChatgroup,
                'Author': MockAuthor,
                'SiteChatgroup': MockSiteChatgroup
            };
            createController = function() {
                $injector.get('$controller')("AuthorToSiteChatgroupDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:authorToSiteChatgroupUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
