'use strict';

describe('Controller Tests', function() {

    describe('AuthorToContact Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockAuthorToContact, MockAuthor, MockContactType;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockAuthorToContact = jasmine.createSpy('MockAuthorToContact');
            MockAuthor = jasmine.createSpy('MockAuthor');
            MockContactType = jasmine.createSpy('MockContactType');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'AuthorToContact': MockAuthorToContact,
                'Author': MockAuthor,
                'ContactType': MockContactType
            };
            createController = function() {
                $injector.get('$controller')("AuthorToContactDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:authorToContactUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
