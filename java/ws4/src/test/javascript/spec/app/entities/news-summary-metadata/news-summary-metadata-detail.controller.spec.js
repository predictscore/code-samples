'use strict';

describe('Controller Tests', function() {

    describe('NewsSummaryMetadata Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockNewsSummaryMetadata, MockNewsSummary, MockMetagroup, MockMetafield;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockNewsSummaryMetadata = jasmine.createSpy('MockNewsSummaryMetadata');
            MockNewsSummary = jasmine.createSpy('MockNewsSummary');
            MockMetagroup = jasmine.createSpy('MockMetagroup');
            MockMetafield = jasmine.createSpy('MockMetafield');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'NewsSummaryMetadata': MockNewsSummaryMetadata,
                'NewsSummary': MockNewsSummary,
                'Metagroup': MockMetagroup,
                'Metafield': MockMetafield
            };
            createController = function() {
                $injector.get('$controller')("NewsSummaryMetadataDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:newsSummaryMetadataUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
