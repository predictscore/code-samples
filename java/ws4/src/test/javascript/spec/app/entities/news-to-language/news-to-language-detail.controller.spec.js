'use strict';

describe('Controller Tests', function() {

    describe('NewsToLanguage Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockNewsToLanguage, MockNews, MockLanguage;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockNewsToLanguage = jasmine.createSpy('MockNewsToLanguage');
            MockNews = jasmine.createSpy('MockNews');
            MockLanguage = jasmine.createSpy('MockLanguage');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'NewsToLanguage': MockNewsToLanguage,
                'News': MockNews,
                'Language': MockLanguage
            };
            createController = function() {
                $injector.get('$controller')("NewsToLanguageDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:newsToLanguageUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
