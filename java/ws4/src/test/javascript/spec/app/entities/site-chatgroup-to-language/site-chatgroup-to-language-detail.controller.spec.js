'use strict';

describe('Controller Tests', function() {

    describe('SiteChatgroupToLanguage Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockSiteChatgroupToLanguage, MockSiteChatgroup, MockLanguage;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockSiteChatgroupToLanguage = jasmine.createSpy('MockSiteChatgroupToLanguage');
            MockSiteChatgroup = jasmine.createSpy('MockSiteChatgroup');
            MockLanguage = jasmine.createSpy('MockLanguage');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'SiteChatgroupToLanguage': MockSiteChatgroupToLanguage,
                'SiteChatgroup': MockSiteChatgroup,
                'Language': MockLanguage
            };
            createController = function() {
                $injector.get('$controller')("SiteChatgroupToLanguageDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:siteChatgroupToLanguageUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
