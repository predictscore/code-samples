'use strict';

describe('Controller Tests', function() {

    describe('NewsSummaryAttachment Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockNewsSummaryAttachment, MockNewsSummary;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockNewsSummaryAttachment = jasmine.createSpy('MockNewsSummaryAttachment');
            MockNewsSummary = jasmine.createSpy('MockNewsSummary');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'NewsSummaryAttachment': MockNewsSummaryAttachment,
                'NewsSummary': MockNewsSummary
            };
            createController = function() {
                $injector.get('$controller')("NewsSummaryAttachmentDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:newsSummaryAttachmentUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
