'use strict';

describe('Controller Tests', function() {

    describe('Author Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockAuthor, MockLocation, MockAuthorProfile, MockAuthorName, MockAuthorComment, MockNews;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockAuthor = jasmine.createSpy('MockAuthor');
            MockLocation = jasmine.createSpy('MockLocation');
            MockAuthorProfile = jasmine.createSpy('MockAuthorProfile');
            MockAuthorName = jasmine.createSpy('MockAuthorName');
            MockAuthorComment = jasmine.createSpy('MockAuthorComment');
            MockNews = jasmine.createSpy('MockNews');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Author': MockAuthor,
                'Location': MockLocation,
                'AuthorProfile': MockAuthorProfile,
                'AuthorName': MockAuthorName,
                'AuthorComment': MockAuthorComment,
                'News': MockNews
            };
            createController = function() {
                $injector.get('$controller')("AuthorDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:authorUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
