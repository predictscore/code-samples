'use strict';

describe('Controller Tests', function() {

    describe('NewsSummaryPublicImage Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockNewsSummaryPublicImage, MockNewsSummary;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockNewsSummaryPublicImage = jasmine.createSpy('MockNewsSummaryPublicImage');
            MockNewsSummary = jasmine.createSpy('MockNewsSummary');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'NewsSummaryPublicImage': MockNewsSummaryPublicImage,
                'NewsSummary': MockNewsSummary
            };
            createController = function() {
                $injector.get('$controller')("NewsSummaryPublicImageDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:newsSummaryPublicImageUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
