'use strict';

describe('Controller Tests', function() {

    describe('NewsSummaryConfidentialImage Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockNewsSummaryConfidentialImage, MockNewsSummary;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockNewsSummaryConfidentialImage = jasmine.createSpy('MockNewsSummaryConfidentialImage');
            MockNewsSummary = jasmine.createSpy('MockNewsSummary');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'NewsSummaryConfidentialImage': MockNewsSummaryConfidentialImage,
                'NewsSummary': MockNewsSummary
            };
            createController = function() {
                $injector.get('$controller')("NewsSummaryConfidentialImageDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:newsSummaryConfidentialImageUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
