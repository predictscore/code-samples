'use strict';

describe('Controller Tests', function() {

    describe('SiteChatgroup Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockSiteChatgroup, MockLocation, MockSiteChatgroupComment, MockSiteChatgroupDomain, MockSiteChatgroupIp, MockSiteChatgroupName, MockSiteChatgroupProfile, MockSiteChatgroupNumVisitors, MockSiteChatgroupNumUsers, MockSiteChatgroupToLanguage, MockSiteChatgroupToTag, MockAuthorToSiteChatgroup;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockSiteChatgroup = jasmine.createSpy('MockSiteChatgroup');
            MockLocation = jasmine.createSpy('MockLocation');
            MockSiteChatgroupComment = jasmine.createSpy('MockSiteChatgroupComment');
            MockSiteChatgroupDomain = jasmine.createSpy('MockSiteChatgroupDomain');
            MockSiteChatgroupIp = jasmine.createSpy('MockSiteChatgroupIp');
            MockSiteChatgroupName = jasmine.createSpy('MockSiteChatgroupName');
            MockSiteChatgroupProfile = jasmine.createSpy('MockSiteChatgroupProfile');
            MockSiteChatgroupNumVisitors = jasmine.createSpy('MockSiteChatgroupNumVisitors');
            MockSiteChatgroupNumUsers = jasmine.createSpy('MockSiteChatgroupNumUsers');
            MockSiteChatgroupToLanguage = jasmine.createSpy('MockSiteChatgroupToLanguage');
            MockSiteChatgroupToTag = jasmine.createSpy('MockSiteChatgroupToTag');
            MockAuthorToSiteChatgroup = jasmine.createSpy('MockAuthorToSiteChatgroup');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'SiteChatgroup': MockSiteChatgroup,
                'Location': MockLocation,
                'SiteChatgroupComment': MockSiteChatgroupComment,
                'SiteChatgroupDomain': MockSiteChatgroupDomain,
                'SiteChatgroupIp': MockSiteChatgroupIp,
                'SiteChatgroupName': MockSiteChatgroupName,
                'SiteChatgroupProfile': MockSiteChatgroupProfile,
                'SiteChatgroupNumVisitors': MockSiteChatgroupNumVisitors,
                'SiteChatgroupNumUsers': MockSiteChatgroupNumUsers,
                'SiteChatgroupToLanguage': MockSiteChatgroupToLanguage,
                'SiteChatgroupToTag': MockSiteChatgroupToTag,
                'AuthorToSiteChatgroup': MockAuthorToSiteChatgroup
            };
            createController = function() {
                $injector.get('$controller')("SiteChatgroupDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:siteChatgroupUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
