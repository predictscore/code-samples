'use strict';

describe('Controller Tests', function() {

    describe('NewsSummaryToTag Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockNewsSummaryToTag, MockNewsSummary, MockTag;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockNewsSummaryToTag = jasmine.createSpy('MockNewsSummaryToTag');
            MockNewsSummary = jasmine.createSpy('MockNewsSummary');
            MockTag = jasmine.createSpy('MockTag');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'NewsSummaryToTag': MockNewsSummaryToTag,
                'NewsSummary': MockNewsSummary,
                'Tag': MockTag
            };
            createController = function() {
                $injector.get('$controller')("NewsSummaryToTagDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:newsSummaryToTagUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
