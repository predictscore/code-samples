'use strict';

describe('Controller Tests', function() {

    describe('NewsDownloadableContentUrl Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockNewsDownloadableContentUrl, MockNews;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockNewsDownloadableContentUrl = jasmine.createSpy('MockNewsDownloadableContentUrl');
            MockNews = jasmine.createSpy('MockNews');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'NewsDownloadableContentUrl': MockNewsDownloadableContentUrl,
                'News': MockNews
            };
            createController = function() {
                $injector.get('$controller')("NewsDownloadableContentUrlDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'newmeridianApp:newsDownloadableContentUrlUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
