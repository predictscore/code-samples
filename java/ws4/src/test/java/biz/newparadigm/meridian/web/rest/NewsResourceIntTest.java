package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.domain.Author;
import biz.newparadigm.meridian.domain.SiteChatgroup;
import biz.newparadigm.meridian.domain.Location;
import biz.newparadigm.meridian.domain.NewsComment;
import biz.newparadigm.meridian.domain.NewsAttachment;
import biz.newparadigm.meridian.domain.NewsOriginalImage;
import biz.newparadigm.meridian.domain.NewsPublicImage;
import biz.newparadigm.meridian.domain.NewsUrl;
import biz.newparadigm.meridian.domain.NewsDownloadableContentUrl;
import biz.newparadigm.meridian.domain.NewsConfidentialImage;
import biz.newparadigm.meridian.repository.NewsRepository;
import biz.newparadigm.meridian.service.NewsService;
import biz.newparadigm.meridian.repository.search.NewsSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsCriteria;
import biz.newparadigm.meridian.service.NewsQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.sameInstant;
import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsResource REST controller.
 *
 * @see NewsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsResourceIntTest {

    private static final ZonedDateTime DEFAULT_DATETIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATETIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_ORIGINAL_SUBJECT = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_SUBJECT = "BBBBBBBBBB";

    private static final String DEFAULT_ENGLISH_SUBJECT = "AAAAAAAAAA";
    private static final String UPDATED_ENGLISH_SUBJECT = "BBBBBBBBBB";

    private static final String DEFAULT_ORIGINAL_POST = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_POST = "BBBBBBBBBB";

    private static final String DEFAULT_ENGLISH_POST = "AAAAAAAAAA";
    private static final String UPDATED_ENGLISH_POST = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private NewsService newsService;

    @Autowired
    private NewsSearchRepository newsSearchRepository;

    @Autowired
    private NewsQueryService newsQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsMockMvc;

    private News news;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsResource newsResource = new NewsResource();
        this.restNewsMockMvc = MockMvcBuilders.standaloneSetup(newsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static News createEntity(EntityManager em) {
        News news = new News()
            .datetime(DEFAULT_DATETIME)
            .originalSubject(DEFAULT_ORIGINAL_SUBJECT)
            .englishSubject(DEFAULT_ENGLISH_SUBJECT)
            .originalPost(DEFAULT_ORIGINAL_POST)
            .englishPost(DEFAULT_ENGLISH_POST);
        // Add required entity
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        news.setAuthor(author);
        // Add required entity
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        news.setSiteChatgroup(siteChatgroup);
        // Add required entity
        Location location = LocationResourceIntTest.createEntity(em);
        em.persist(location);
        em.flush();
        news.setLocation(location);
        return news;
    }

    @Before
    public void initTest() {
        newsSearchRepository.deleteAll();
        news = createEntity(em);
    }

    @Test
    @Transactional
    public void createNews() throws Exception {
        int databaseSizeBeforeCreate = newsRepository.findAll().size();

        // Create the News
        restNewsMockMvc.perform(post("/api/news")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(news)))
            .andExpect(status().isCreated());

        // Validate the News in the database
        List<News> newsList = newsRepository.findAll();
        assertThat(newsList).hasSize(databaseSizeBeforeCreate + 1);
        News testNews = newsList.get(newsList.size() - 1);
        assertThat(testNews.getDatetime()).isEqualTo(DEFAULT_DATETIME);
        assertThat(testNews.getOriginalSubject()).isEqualTo(DEFAULT_ORIGINAL_SUBJECT);
        assertThat(testNews.getEnglishSubject()).isEqualTo(DEFAULT_ENGLISH_SUBJECT);
        assertThat(testNews.getOriginalPost()).isEqualTo(DEFAULT_ORIGINAL_POST);
        assertThat(testNews.getEnglishPost()).isEqualTo(DEFAULT_ENGLISH_POST);

        // Validate the News in Elasticsearch
        News newsEs = newsSearchRepository.findOne(testNews.getId());
        assertThat(newsEs).isEqualToComparingFieldByField(testNews);
    }

    @Test
    @Transactional
    public void createNewsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsRepository.findAll().size();

        // Create the News with an existing ID
        news.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsMockMvc.perform(post("/api/news")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(news)))
            .andExpect(status().isBadRequest());

        // Validate the News in the database
        List<News> newsList = newsRepository.findAll();
        assertThat(newsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDatetimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsRepository.findAll().size();
        // set the field null
        news.setDatetime(null);

        // Create the News, which fails.

        restNewsMockMvc.perform(post("/api/news")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(news)))
            .andExpect(status().isBadRequest());

        List<News> newsList = newsRepository.findAll();
        assertThat(newsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOriginalSubjectIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsRepository.findAll().size();
        // set the field null
        news.setOriginalSubject(null);

        // Create the News, which fails.

        restNewsMockMvc.perform(post("/api/news")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(news)))
            .andExpect(status().isBadRequest());

        List<News> newsList = newsRepository.findAll();
        assertThat(newsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOriginalPostIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsRepository.findAll().size();
        // set the field null
        news.setOriginalPost(null);

        // Create the News, which fails.

        restNewsMockMvc.perform(post("/api/news")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(news)))
            .andExpect(status().isBadRequest());

        List<News> newsList = newsRepository.findAll();
        assertThat(newsList).hasSize(databaseSizeBeforeTest);
    }


    @Test
    @Transactional
    public void getAllNews() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList
        restNewsMockMvc.perform(get("/api/news?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(news.getId().intValue())))
            .andExpect(jsonPath("$.[*].datetime").value(hasItem(sameInstant(DEFAULT_DATETIME))))
            .andExpect(jsonPath("$.[*].originalSubject").value(hasItem(DEFAULT_ORIGINAL_SUBJECT.toString())))
            .andExpect(jsonPath("$.[*].englishSubject").value(hasItem(DEFAULT_ENGLISH_SUBJECT.toString())))
            .andExpect(jsonPath("$.[*].originalPost").value(hasItem(DEFAULT_ORIGINAL_POST.toString())))
            .andExpect(jsonPath("$.[*].englishPost").value(hasItem(DEFAULT_ENGLISH_POST.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(sameInstant(DEFAULT_CREATION_DATE))));
    }

    @Test
    @Transactional
    public void getNews() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get the news
        restNewsMockMvc.perform(get("/api/news/{id}", news.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(news.getId().intValue()))
            .andExpect(jsonPath("$.datetime").value(sameInstant(DEFAULT_DATETIME)))
            .andExpect(jsonPath("$.originalSubject").value(DEFAULT_ORIGINAL_SUBJECT.toString()))
            .andExpect(jsonPath("$.englishSubject").value(DEFAULT_ENGLISH_SUBJECT.toString()))
            .andExpect(jsonPath("$.originalPost").value(DEFAULT_ORIGINAL_POST.toString()))
            .andExpect(jsonPath("$.englishPost").value(DEFAULT_ENGLISH_POST.toString()))
            .andExpect(jsonPath("$.creationDate").value(sameInstant(DEFAULT_CREATION_DATE)));
    }

    @Test
    @Transactional
    public void getAllNewsByDatetimeIsEqualToSomething() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where datetime equals to DEFAULT_DATETIME
        defaultNewsShouldBeFound("datetime.equals=" + DEFAULT_DATETIME);

        // Get all the newsList where datetime equals to UPDATED_DATETIME
        defaultNewsShouldNotBeFound("datetime.equals=" + UPDATED_DATETIME);
    }

    @Test
    @Transactional
    public void getAllNewsByDatetimeIsInShouldWork() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where datetime in DEFAULT_DATETIME or UPDATED_DATETIME
        defaultNewsShouldBeFound("datetime.in=" + DEFAULT_DATETIME + "," + UPDATED_DATETIME);

        // Get all the newsList where datetime equals to UPDATED_DATETIME
        defaultNewsShouldNotBeFound("datetime.in=" + UPDATED_DATETIME);
    }

    @Test
    @Transactional
    public void getAllNewsByDatetimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where datetime is not null
        defaultNewsShouldBeFound("datetime.specified=true");

        // Get all the newsList where datetime is null
        defaultNewsShouldNotBeFound("datetime.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsByDatetimeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where datetime greater than or equals to DEFAULT_DATETIME
        defaultNewsShouldBeFound("datetime.greaterOrEqualThan=" + DEFAULT_DATETIME);

        // Get all the newsList where datetime greater than or equals to UPDATED_DATETIME
        defaultNewsShouldNotBeFound("datetime.greaterOrEqualThan=" + UPDATED_DATETIME);
    }

    @Test
    @Transactional
    public void getAllNewsByDatetimeIsLessThanSomething() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where datetime less than or equals to DEFAULT_DATETIME
        defaultNewsShouldNotBeFound("datetime.lessThan=" + DEFAULT_DATETIME);

        // Get all the newsList where datetime less than or equals to UPDATED_DATETIME
        defaultNewsShouldBeFound("datetime.lessThan=" + UPDATED_DATETIME);
    }


    @Test
    @Transactional
    public void getAllNewsByOriginalSubjectIsEqualToSomething() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where originalSubject equals to DEFAULT_ORIGINAL_SUBJECT
        defaultNewsShouldBeFound("originalSubject.equals=" + DEFAULT_ORIGINAL_SUBJECT);

        // Get all the newsList where originalSubject equals to UPDATED_ORIGINAL_SUBJECT
        defaultNewsShouldNotBeFound("originalSubject.equals=" + UPDATED_ORIGINAL_SUBJECT);
    }

    @Test
    @Transactional
    public void getAllNewsByOriginalSubjectIsInShouldWork() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where originalSubject in DEFAULT_ORIGINAL_SUBJECT or UPDATED_ORIGINAL_SUBJECT
        defaultNewsShouldBeFound("originalSubject.in=" + DEFAULT_ORIGINAL_SUBJECT + "," + UPDATED_ORIGINAL_SUBJECT);

        // Get all the newsList where originalSubject equals to UPDATED_ORIGINAL_SUBJECT
        defaultNewsShouldNotBeFound("originalSubject.in=" + UPDATED_ORIGINAL_SUBJECT);
    }

    @Test
    @Transactional
    public void getAllNewsByOriginalSubjectIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where originalSubject is not null
        defaultNewsShouldBeFound("originalSubject.specified=true");

        // Get all the newsList where originalSubject is null
        defaultNewsShouldNotBeFound("originalSubject.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsByEnglishSubjectIsEqualToSomething() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where englishSubject equals to DEFAULT_ENGLISH_SUBJECT
        defaultNewsShouldBeFound("englishSubject.equals=" + DEFAULT_ENGLISH_SUBJECT);

        // Get all the newsList where englishSubject equals to UPDATED_ENGLISH_SUBJECT
        defaultNewsShouldNotBeFound("englishSubject.equals=" + UPDATED_ENGLISH_SUBJECT);
    }

    @Test
    @Transactional
    public void getAllNewsByEnglishSubjectIsInShouldWork() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where englishSubject in DEFAULT_ENGLISH_SUBJECT or UPDATED_ENGLISH_SUBJECT
        defaultNewsShouldBeFound("englishSubject.in=" + DEFAULT_ENGLISH_SUBJECT + "," + UPDATED_ENGLISH_SUBJECT);

        // Get all the newsList where englishSubject equals to UPDATED_ENGLISH_SUBJECT
        defaultNewsShouldNotBeFound("englishSubject.in=" + UPDATED_ENGLISH_SUBJECT);
    }

    @Test
    @Transactional
    public void getAllNewsByEnglishSubjectIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where englishSubject is not null
        defaultNewsShouldBeFound("englishSubject.specified=true");

        // Get all the newsList where englishSubject is null
        defaultNewsShouldNotBeFound("englishSubject.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsByOriginalPostIsEqualToSomething() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where originalPost equals to DEFAULT_ORIGINAL_POST
        defaultNewsShouldBeFound("originalPost.equals=" + DEFAULT_ORIGINAL_POST);

        // Get all the newsList where originalPost equals to UPDATED_ORIGINAL_POST
        defaultNewsShouldNotBeFound("originalPost.equals=" + UPDATED_ORIGINAL_POST);
    }

    @Test
    @Transactional
    public void getAllNewsByOriginalPostIsInShouldWork() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where originalPost in DEFAULT_ORIGINAL_POST or UPDATED_ORIGINAL_POST
        defaultNewsShouldBeFound("originalPost.in=" + DEFAULT_ORIGINAL_POST + "," + UPDATED_ORIGINAL_POST);

        // Get all the newsList where originalPost equals to UPDATED_ORIGINAL_POST
        defaultNewsShouldNotBeFound("originalPost.in=" + UPDATED_ORIGINAL_POST);
    }

    @Test
    @Transactional
    public void getAllNewsByOriginalPostIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where originalPost is not null
        defaultNewsShouldBeFound("originalPost.specified=true");

        // Get all the newsList where originalPost is null
        defaultNewsShouldNotBeFound("originalPost.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsByEnglishPostIsEqualToSomething() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where englishPost equals to DEFAULT_ENGLISH_POST
        defaultNewsShouldBeFound("englishPost.equals=" + DEFAULT_ENGLISH_POST);

        // Get all the newsList where englishPost equals to UPDATED_ENGLISH_POST
        defaultNewsShouldNotBeFound("englishPost.equals=" + UPDATED_ENGLISH_POST);
    }

    @Test
    @Transactional
    public void getAllNewsByEnglishPostIsInShouldWork() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where englishPost in DEFAULT_ENGLISH_POST or UPDATED_ENGLISH_POST
        defaultNewsShouldBeFound("englishPost.in=" + DEFAULT_ENGLISH_POST + "," + UPDATED_ENGLISH_POST);

        // Get all the newsList where englishPost equals to UPDATED_ENGLISH_POST
        defaultNewsShouldNotBeFound("englishPost.in=" + UPDATED_ENGLISH_POST);
    }

    @Test
    @Transactional
    public void getAllNewsByEnglishPostIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where englishPost is not null
        defaultNewsShouldBeFound("englishPost.specified=true");

        // Get all the newsList where englishPost is null
        defaultNewsShouldNotBeFound("englishPost.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsByCreationDateIsEqualToSomething() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where creationDate equals to DEFAULT_CREATION_DATE
        defaultNewsShouldBeFound("creationDate.equals=" + DEFAULT_CREATION_DATE);

        // Get all the newsList where creationDate equals to UPDATED_CREATION_DATE
        defaultNewsShouldNotBeFound("creationDate.equals=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllNewsByCreationDateIsInShouldWork() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where creationDate in DEFAULT_CREATION_DATE or UPDATED_CREATION_DATE
        defaultNewsShouldBeFound("creationDate.in=" + DEFAULT_CREATION_DATE + "," + UPDATED_CREATION_DATE);

        // Get all the newsList where creationDate equals to UPDATED_CREATION_DATE
        defaultNewsShouldNotBeFound("creationDate.in=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllNewsByCreationDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where creationDate is not null
        defaultNewsShouldBeFound("creationDate.specified=true");

        // Get all the newsList where creationDate is null
        defaultNewsShouldNotBeFound("creationDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsByCreationDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where creationDate greater than or equals to DEFAULT_CREATION_DATE
        defaultNewsShouldBeFound("creationDate.greaterOrEqualThan=" + DEFAULT_CREATION_DATE);

        // Get all the newsList where creationDate greater than or equals to UPDATED_CREATION_DATE
        defaultNewsShouldNotBeFound("creationDate.greaterOrEqualThan=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllNewsByCreationDateIsLessThanSomething() throws Exception {
        // Initialize the database
        newsRepository.saveAndFlush(news);

        // Get all the newsList where creationDate less than or equals to DEFAULT_CREATION_DATE
        defaultNewsShouldNotBeFound("creationDate.lessThan=" + DEFAULT_CREATION_DATE);

        // Get all the newsList where creationDate less than or equals to UPDATED_CREATION_DATE
        defaultNewsShouldBeFound("creationDate.lessThan=" + UPDATED_CREATION_DATE);
    }


    @Test
    @Transactional
    public void getAllNewsByAuthorIsEqualToSomething() throws Exception {
        // Initialize the database
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        news.setAuthor(author);
        newsRepository.saveAndFlush(news);
        Long authorId = author.getId();

        // Get all the newsList where author equals to authorId
        defaultNewsShouldBeFound("authorId.equals=" + authorId);

        // Get all the newsList where author equals to authorId + 1
        defaultNewsShouldNotBeFound("authorId.equals=" + (authorId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsBySiteChatgroupIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        news.setSiteChatgroup(siteChatgroup);
        newsRepository.saveAndFlush(news);
        Long siteChatgroupId = siteChatgroup.getId();

        // Get all the newsList where siteChatgroup equals to siteChatgroupId
        defaultNewsShouldBeFound("siteChatgroupId.equals=" + siteChatgroupId);

        // Get all the newsList where siteChatgroup equals to siteChatgroupId + 1
        defaultNewsShouldNotBeFound("siteChatgroupId.equals=" + (siteChatgroupId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsByLocationIsEqualToSomething() throws Exception {
        // Initialize the database
        Location location = LocationResourceIntTest.createEntity(em);
        em.persist(location);
        em.flush();
        news.setLocation(location);
        newsRepository.saveAndFlush(news);
        Long locationId = location.getId();

        // Get all the newsList where location equals to locationId
        defaultNewsShouldBeFound("locationId.equals=" + locationId);

        // Get all the newsList where location equals to locationId + 1
        defaultNewsShouldNotBeFound("locationId.equals=" + (locationId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsByCommentsIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsComment comments = NewsCommentResourceIntTest.createEntity(em);
        em.persist(comments);
        em.flush();
        newsRepository.saveAndFlush(news);
        Long commentsId = comments.getId();

        // Get all the newsList where comments equals to commentsId
        defaultNewsShouldBeFound("commentsId.equals=" + commentsId);

        // Get all the newsList where comments equals to commentsId + 1
        defaultNewsShouldNotBeFound("commentsId.equals=" + (commentsId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsByAttachmentsIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsAttachment attachments = NewsAttachmentResourceIntTest.createEntity(em);
        em.persist(attachments);
        em.flush();
        news.addAttachments(attachments);
        newsRepository.saveAndFlush(news);
        Long attachmentsId = attachments.getId();

        // Get all the newsList where attachments equals to attachmentsId
        defaultNewsShouldBeFound("attachmentsId.equals=" + attachmentsId);

        // Get all the newsList where attachments equals to attachmentsId + 1
        defaultNewsShouldNotBeFound("attachmentsId.equals=" + (attachmentsId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsByOriginalImagesIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsOriginalImage originalImages = NewsOriginalImageResourceIntTest.createEntity(em);
        em.persist(originalImages);
        em.flush();
        news.addOriginalImages(originalImages);
        newsRepository.saveAndFlush(news);
        Long originalImagesId = originalImages.getId();

        // Get all the newsList where originalImages equals to originalImagesId
        defaultNewsShouldBeFound("originalImagesId.equals=" + originalImagesId);

        // Get all the newsList where originalImages equals to originalImagesId + 1
        defaultNewsShouldNotBeFound("originalImagesId.equals=" + (originalImagesId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsByPublicImagesIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsPublicImage publicImages = NewsPublicImageResourceIntTest.createEntity(em);
        em.persist(publicImages);
        em.flush();
        news.addPublicImages(publicImages);
        newsRepository.saveAndFlush(news);
        Long publicImagesId = publicImages.getId();

        // Get all the newsList where publicImages equals to publicImagesId
        defaultNewsShouldBeFound("publicImagesId.equals=" + publicImagesId);

        // Get all the newsList where publicImages equals to publicImagesId + 1
        defaultNewsShouldNotBeFound("publicImagesId.equals=" + (publicImagesId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsByUrlsIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsUrl urls = NewsUrlResourceIntTest.createEntity(em);
        em.persist(urls);
        em.flush();
        news.addUrls(urls);
        newsRepository.saveAndFlush(news);
        Long urlsId = urls.getId();

        // Get all the newsList where urls equals to urlsId
        defaultNewsShouldBeFound("urlsId.equals=" + urlsId);

        // Get all the newsList where urls equals to urlsId + 1
        defaultNewsShouldNotBeFound("urlsId.equals=" + (urlsId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsByDownloadableContentUrlsIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsDownloadableContentUrl downloadableContentUrls = NewsDownloadableContentUrlResourceIntTest.createEntity(em);
        em.persist(downloadableContentUrls);
        em.flush();
        news.addDownloadableContentUrls(downloadableContentUrls);
        newsRepository.saveAndFlush(news);
        Long downloadableContentUrlsId = downloadableContentUrls.getId();

        // Get all the newsList where downloadableContentUrls equals to downloadableContentUrlsId
        defaultNewsShouldBeFound("downloadableContentUrlsId.equals=" + downloadableContentUrlsId);

        // Get all the newsList where downloadableContentUrls equals to downloadableContentUrlsId + 1
        defaultNewsShouldNotBeFound("downloadableContentUrlsId.equals=" + (downloadableContentUrlsId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsByConfidentialImagesIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsConfidentialImage confidentialImages = NewsConfidentialImageResourceIntTest.createEntity(em);
        em.persist(confidentialImages);
        em.flush();
        news.addConfidentialImages(confidentialImages);
        newsRepository.saveAndFlush(news);
        Long confidentialImagesId = confidentialImages.getId();

        // Get all the newsList where confidentialImages equals to confidentialImagesId
        defaultNewsShouldBeFound("confidentialImagesId.equals=" + confidentialImagesId);

        // Get all the newsList where confidentialImages equals to confidentialImagesId + 1
        defaultNewsShouldNotBeFound("confidentialImagesId.equals=" + (confidentialImagesId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsShouldBeFound(String filter) throws Exception {
        restNewsMockMvc.perform(get("/api/news?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(news.getId().intValue())))
            .andExpect(jsonPath("$.[*].datetime").value(hasItem(sameInstant(DEFAULT_DATETIME))))
            .andExpect(jsonPath("$.[*].originalSubject").value(hasItem(DEFAULT_ORIGINAL_SUBJECT.toString())))
            .andExpect(jsonPath("$.[*].englishSubject").value(hasItem(DEFAULT_ENGLISH_SUBJECT.toString())))
            .andExpect(jsonPath("$.[*].originalPost").value(hasItem(DEFAULT_ORIGINAL_POST.toString())))
            .andExpect(jsonPath("$.[*].englishPost").value(hasItem(DEFAULT_ENGLISH_POST.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(sameInstant(DEFAULT_CREATION_DATE))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsShouldNotBeFound(String filter) throws Exception {
        restNewsMockMvc.perform(get("/api/news?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNews() throws Exception {
        // Get the news
        restNewsMockMvc.perform(get("/api/news/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNews() throws Exception {
        // Initialize the database
        newsService.save(news);

        int databaseSizeBeforeUpdate = newsRepository.findAll().size();

        // Update the news
        News updatedNews = newsRepository.findOne(news.getId());
        updatedNews
            .datetime(UPDATED_DATETIME)
            .originalSubject(UPDATED_ORIGINAL_SUBJECT)
            .englishSubject(UPDATED_ENGLISH_SUBJECT)
            .originalPost(UPDATED_ORIGINAL_POST)
            .englishPost(UPDATED_ENGLISH_POST);

        restNewsMockMvc.perform(put("/api/news")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNews)))
            .andExpect(status().isOk());

        // Validate the News in the database
        List<News> newsList = newsRepository.findAll();
        assertThat(newsList).hasSize(databaseSizeBeforeUpdate);
        News testNews = newsList.get(newsList.size() - 1);
        assertThat(testNews.getDatetime()).isEqualTo(UPDATED_DATETIME);
        assertThat(testNews.getOriginalSubject()).isEqualTo(UPDATED_ORIGINAL_SUBJECT);
        assertThat(testNews.getEnglishSubject()).isEqualTo(UPDATED_ENGLISH_SUBJECT);
        assertThat(testNews.getOriginalPost()).isEqualTo(UPDATED_ORIGINAL_POST);
        assertThat(testNews.getEnglishPost()).isEqualTo(UPDATED_ENGLISH_POST);

        // Validate the News in Elasticsearch
        News newsEs = newsSearchRepository.findOne(testNews.getId());
        assertThat(newsEs).isEqualToComparingFieldByField(testNews);
    }

    @Test
    @Transactional
    public void updateNonExistingNews() throws Exception {
        int databaseSizeBeforeUpdate = newsRepository.findAll().size();

        // Create the News

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsMockMvc.perform(put("/api/news")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(news)))
            .andExpect(status().isCreated());

        // Validate the News in the database
        List<News> newsList = newsRepository.findAll();
        assertThat(newsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNews() throws Exception {
        // Initialize the database
        newsService.save(news);

        int databaseSizeBeforeDelete = newsRepository.findAll().size();

        // Get the news
        restNewsMockMvc.perform(delete("/api/news/{id}", news.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsExistsInEs = newsSearchRepository.exists(news.getId());
        assertThat(newsExistsInEs).isFalse();

        // Validate the database is empty
        List<News> newsList = newsRepository.findAll();
        assertThat(newsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNews() throws Exception {
        // Initialize the database
        newsService.save(news);

        // Search the news
        restNewsMockMvc.perform(get("/api/_search/news?query=id:" + news.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(news.getId().intValue())))
            .andExpect(jsonPath("$.[*].datetime").value(hasItem(sameInstant(DEFAULT_DATETIME))))
            .andExpect(jsonPath("$.[*].originalSubject").value(hasItem(DEFAULT_ORIGINAL_SUBJECT.toString())))
            .andExpect(jsonPath("$.[*].englishSubject").value(hasItem(DEFAULT_ENGLISH_SUBJECT.toString())))
            .andExpect(jsonPath("$.[*].originalPost").value(hasItem(DEFAULT_ORIGINAL_POST.toString())))
            .andExpect(jsonPath("$.[*].englishPost").value(hasItem(DEFAULT_ENGLISH_POST.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(sameInstant(DEFAULT_CREATION_DATE))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(News.class);
        News news1 = new News();
        news1.setId(1L);
        News news2 = new News();
        news2.setId(news1.getId());
        assertThat(news1).isEqualTo(news2);
        news2.setId(2L);
        assertThat(news1).isNotEqualTo(news2);
        news1.setId(null);
        assertThat(news1).isNotEqualTo(news2);
    }
}
