package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsSummaryConfidentialImage;
import biz.newparadigm.meridian.domain.NewsSummary;
import biz.newparadigm.meridian.repository.NewsSummaryConfidentialImageRepository;
import biz.newparadigm.meridian.service.NewsSummaryConfidentialImageService;
import biz.newparadigm.meridian.repository.search.NewsSummaryConfidentialImageSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsSummaryConfidentialImageCriteria;
import biz.newparadigm.meridian.service.NewsSummaryConfidentialImageQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsSummaryConfidentialImageResource REST controller.
 *
 * @see NewsSummaryConfidentialImageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsSummaryConfidentialImageResourceIntTest {

    private static final String DEFAULT_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_FILENAME = "BBBBBBBBBB";

    private static final byte[] DEFAULT_BLOB = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_BLOB = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_BLOB_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_BLOB_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_TRANSLATION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_TRANSLATION = "BBBBBBBBBB";

    private static final Integer DEFAULT_IMAGE_ORDER = 1;
    private static final Integer UPDATED_IMAGE_ORDER = 2;

    @Autowired
    private NewsSummaryConfidentialImageRepository newsSummaryConfidentialImageRepository;

    @Autowired
    private NewsSummaryConfidentialImageService newsSummaryConfidentialImageService;

    @Autowired
    private NewsSummaryConfidentialImageSearchRepository newsSummaryConfidentialImageSearchRepository;

    @Autowired
    private NewsSummaryConfidentialImageQueryService newsSummaryConfidentialImageQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsSummaryConfidentialImageMockMvc;

    private NewsSummaryConfidentialImage newsSummaryConfidentialImage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsSummaryConfidentialImageResource newsSummaryConfidentialImageResource = new NewsSummaryConfidentialImageResource(newsSummaryConfidentialImageService, newsSummaryConfidentialImageQueryService);
        this.restNewsSummaryConfidentialImageMockMvc = MockMvcBuilders.standaloneSetup(newsSummaryConfidentialImageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsSummaryConfidentialImage createEntity(EntityManager em) {
        NewsSummaryConfidentialImage newsSummaryConfidentialImage = new NewsSummaryConfidentialImage()
            .filename(DEFAULT_FILENAME)
            .blob(DEFAULT_BLOB)
            .blobContentType(DEFAULT_BLOB_CONTENT_TYPE)
            .description(DEFAULT_DESCRIPTION)
            .descriptionTranslation(DEFAULT_DESCRIPTION_TRANSLATION)
            .imageOrder(DEFAULT_IMAGE_ORDER);
        // Add required entity
        NewsSummary newsSummary = NewsSummaryResourceIntTest.createEntity(em);
        em.persist(newsSummary);
        em.flush();
        newsSummaryConfidentialImage.setNewsSummary(newsSummary);
        return newsSummaryConfidentialImage;
    }

    @Before
    public void initTest() {
        newsSummaryConfidentialImageSearchRepository.deleteAll();
        newsSummaryConfidentialImage = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsSummaryConfidentialImage() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryConfidentialImageRepository.findAll().size();

        // Create the NewsSummaryConfidentialImage
        restNewsSummaryConfidentialImageMockMvc.perform(post("/api/news-summary-confidential-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryConfidentialImage)))
            .andExpect(status().isCreated());

        // Validate the NewsSummaryConfidentialImage in the database
        List<NewsSummaryConfidentialImage> newsSummaryConfidentialImageList = newsSummaryConfidentialImageRepository.findAll();
        assertThat(newsSummaryConfidentialImageList).hasSize(databaseSizeBeforeCreate + 1);
        NewsSummaryConfidentialImage testNewsSummaryConfidentialImage = newsSummaryConfidentialImageList.get(newsSummaryConfidentialImageList.size() - 1);
        assertThat(testNewsSummaryConfidentialImage.getFilename()).isEqualTo(DEFAULT_FILENAME);
        assertThat(testNewsSummaryConfidentialImage.getBlob()).isEqualTo(DEFAULT_BLOB);
        assertThat(testNewsSummaryConfidentialImage.getBlobContentType()).isEqualTo(DEFAULT_BLOB_CONTENT_TYPE);
        assertThat(testNewsSummaryConfidentialImage.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testNewsSummaryConfidentialImage.getDescriptionTranslation()).isEqualTo(DEFAULT_DESCRIPTION_TRANSLATION);
        assertThat(testNewsSummaryConfidentialImage.getImageOrder()).isEqualTo(DEFAULT_IMAGE_ORDER);

        // Validate the NewsSummaryConfidentialImage in Elasticsearch
        NewsSummaryConfidentialImage newsSummaryConfidentialImageEs = newsSummaryConfidentialImageSearchRepository.findOne(testNewsSummaryConfidentialImage.getId());
        assertThat(newsSummaryConfidentialImageEs).isEqualToComparingFieldByField(testNewsSummaryConfidentialImage);
    }

    @Test
    @Transactional
    public void createNewsSummaryConfidentialImageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryConfidentialImageRepository.findAll().size();

        // Create the NewsSummaryConfidentialImage with an existing ID
        newsSummaryConfidentialImage.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsSummaryConfidentialImageMockMvc.perform(post("/api/news-summary-confidential-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryConfidentialImage)))
            .andExpect(status().isBadRequest());

        // Validate the NewsSummaryConfidentialImage in the database
        List<NewsSummaryConfidentialImage> newsSummaryConfidentialImageList = newsSummaryConfidentialImageRepository.findAll();
        assertThat(newsSummaryConfidentialImageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFilenameIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsSummaryConfidentialImageRepository.findAll().size();
        // set the field null
        newsSummaryConfidentialImage.setFilename(null);

        // Create the NewsSummaryConfidentialImage, which fails.

        restNewsSummaryConfidentialImageMockMvc.perform(post("/api/news-summary-confidential-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryConfidentialImage)))
            .andExpect(status().isBadRequest());

        List<NewsSummaryConfidentialImage> newsSummaryConfidentialImageList = newsSummaryConfidentialImageRepository.findAll();
        assertThat(newsSummaryConfidentialImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBlobIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsSummaryConfidentialImageRepository.findAll().size();
        // set the field null
        newsSummaryConfidentialImage.setBlob(null);

        // Create the NewsSummaryConfidentialImage, which fails.

        restNewsSummaryConfidentialImageMockMvc.perform(post("/api/news-summary-confidential-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryConfidentialImage)))
            .andExpect(status().isBadRequest());

        List<NewsSummaryConfidentialImage> newsSummaryConfidentialImageList = newsSummaryConfidentialImageRepository.findAll();
        assertThat(newsSummaryConfidentialImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImages() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get all the newsSummaryConfidentialImageList
        restNewsSummaryConfidentialImageMockMvc.perform(get("/api/news-summary-confidential-images?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryConfidentialImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionTranslation").value(hasItem(DEFAULT_DESCRIPTION_TRANSLATION.toString())))
            .andExpect(jsonPath("$.[*].imageOrder").value(hasItem(DEFAULT_IMAGE_ORDER)));
    }

    @Test
    @Transactional
    public void getNewsSummaryConfidentialImage() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get the newsSummaryConfidentialImage
        restNewsSummaryConfidentialImageMockMvc.perform(get("/api/news-summary-confidential-images/{id}", newsSummaryConfidentialImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsSummaryConfidentialImage.getId().intValue()))
            .andExpect(jsonPath("$.filename").value(DEFAULT_FILENAME.toString()))
            .andExpect(jsonPath("$.blobContentType").value(DEFAULT_BLOB_CONTENT_TYPE))
            .andExpect(jsonPath("$.blob").value(Base64Utils.encodeToString(DEFAULT_BLOB)))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.descriptionTranslation").value(DEFAULT_DESCRIPTION_TRANSLATION.toString()))
            .andExpect(jsonPath("$.imageOrder").value(DEFAULT_IMAGE_ORDER));
    }

    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImagesByFilenameIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get all the newsSummaryConfidentialImageList where filename equals to DEFAULT_FILENAME
        defaultNewsSummaryConfidentialImageShouldBeFound("filename.equals=" + DEFAULT_FILENAME);

        // Get all the newsSummaryConfidentialImageList where filename equals to UPDATED_FILENAME
        defaultNewsSummaryConfidentialImageShouldNotBeFound("filename.equals=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImagesByFilenameIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get all the newsSummaryConfidentialImageList where filename in DEFAULT_FILENAME or UPDATED_FILENAME
        defaultNewsSummaryConfidentialImageShouldBeFound("filename.in=" + DEFAULT_FILENAME + "," + UPDATED_FILENAME);

        // Get all the newsSummaryConfidentialImageList where filename equals to UPDATED_FILENAME
        defaultNewsSummaryConfidentialImageShouldNotBeFound("filename.in=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImagesByFilenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get all the newsSummaryConfidentialImageList where filename is not null
        defaultNewsSummaryConfidentialImageShouldBeFound("filename.specified=true");

        // Get all the newsSummaryConfidentialImageList where filename is null
        defaultNewsSummaryConfidentialImageShouldNotBeFound("filename.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImagesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get all the newsSummaryConfidentialImageList where description equals to DEFAULT_DESCRIPTION
        defaultNewsSummaryConfidentialImageShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the newsSummaryConfidentialImageList where description equals to UPDATED_DESCRIPTION
        defaultNewsSummaryConfidentialImageShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImagesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get all the newsSummaryConfidentialImageList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultNewsSummaryConfidentialImageShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the newsSummaryConfidentialImageList where description equals to UPDATED_DESCRIPTION
        defaultNewsSummaryConfidentialImageShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImagesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get all the newsSummaryConfidentialImageList where description is not null
        defaultNewsSummaryConfidentialImageShouldBeFound("description.specified=true");

        // Get all the newsSummaryConfidentialImageList where description is null
        defaultNewsSummaryConfidentialImageShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImagesByDescriptionTranslationIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get all the newsSummaryConfidentialImageList where descriptionTranslation equals to DEFAULT_DESCRIPTION_TRANSLATION
        defaultNewsSummaryConfidentialImageShouldBeFound("descriptionTranslation.equals=" + DEFAULT_DESCRIPTION_TRANSLATION);

        // Get all the newsSummaryConfidentialImageList where descriptionTranslation equals to UPDATED_DESCRIPTION_TRANSLATION
        defaultNewsSummaryConfidentialImageShouldNotBeFound("descriptionTranslation.equals=" + UPDATED_DESCRIPTION_TRANSLATION);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImagesByDescriptionTranslationIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get all the newsSummaryConfidentialImageList where descriptionTranslation in DEFAULT_DESCRIPTION_TRANSLATION or UPDATED_DESCRIPTION_TRANSLATION
        defaultNewsSummaryConfidentialImageShouldBeFound("descriptionTranslation.in=" + DEFAULT_DESCRIPTION_TRANSLATION + "," + UPDATED_DESCRIPTION_TRANSLATION);

        // Get all the newsSummaryConfidentialImageList where descriptionTranslation equals to UPDATED_DESCRIPTION_TRANSLATION
        defaultNewsSummaryConfidentialImageShouldNotBeFound("descriptionTranslation.in=" + UPDATED_DESCRIPTION_TRANSLATION);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImagesByDescriptionTranslationIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get all the newsSummaryConfidentialImageList where descriptionTranslation is not null
        defaultNewsSummaryConfidentialImageShouldBeFound("descriptionTranslation.specified=true");

        // Get all the newsSummaryConfidentialImageList where descriptionTranslation is null
        defaultNewsSummaryConfidentialImageShouldNotBeFound("descriptionTranslation.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImagesByImageOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get all the newsSummaryConfidentialImageList where imageOrder equals to DEFAULT_IMAGE_ORDER
        defaultNewsSummaryConfidentialImageShouldBeFound("imageOrder.equals=" + DEFAULT_IMAGE_ORDER);

        // Get all the newsSummaryConfidentialImageList where imageOrder equals to UPDATED_IMAGE_ORDER
        defaultNewsSummaryConfidentialImageShouldNotBeFound("imageOrder.equals=" + UPDATED_IMAGE_ORDER);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImagesByImageOrderIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get all the newsSummaryConfidentialImageList where imageOrder in DEFAULT_IMAGE_ORDER or UPDATED_IMAGE_ORDER
        defaultNewsSummaryConfidentialImageShouldBeFound("imageOrder.in=" + DEFAULT_IMAGE_ORDER + "," + UPDATED_IMAGE_ORDER);

        // Get all the newsSummaryConfidentialImageList where imageOrder equals to UPDATED_IMAGE_ORDER
        defaultNewsSummaryConfidentialImageShouldNotBeFound("imageOrder.in=" + UPDATED_IMAGE_ORDER);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImagesByImageOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get all the newsSummaryConfidentialImageList where imageOrder is not null
        defaultNewsSummaryConfidentialImageShouldBeFound("imageOrder.specified=true");

        // Get all the newsSummaryConfidentialImageList where imageOrder is null
        defaultNewsSummaryConfidentialImageShouldNotBeFound("imageOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImagesByImageOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get all the newsSummaryConfidentialImageList where imageOrder greater than or equals to DEFAULT_IMAGE_ORDER
        defaultNewsSummaryConfidentialImageShouldBeFound("imageOrder.greaterOrEqualThan=" + DEFAULT_IMAGE_ORDER);

        // Get all the newsSummaryConfidentialImageList where imageOrder greater than or equals to UPDATED_IMAGE_ORDER
        defaultNewsSummaryConfidentialImageShouldNotBeFound("imageOrder.greaterOrEqualThan=" + UPDATED_IMAGE_ORDER);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImagesByImageOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);

        // Get all the newsSummaryConfidentialImageList where imageOrder less than or equals to DEFAULT_IMAGE_ORDER
        defaultNewsSummaryConfidentialImageShouldNotBeFound("imageOrder.lessThan=" + DEFAULT_IMAGE_ORDER);

        // Get all the newsSummaryConfidentialImageList where imageOrder less than or equals to UPDATED_IMAGE_ORDER
        defaultNewsSummaryConfidentialImageShouldBeFound("imageOrder.lessThan=" + UPDATED_IMAGE_ORDER);
    }


    @Test
    @Transactional
    public void getAllNewsSummaryConfidentialImagesByNewsSummaryIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsSummary newsSummary = NewsSummaryResourceIntTest.createEntity(em);
        em.persist(newsSummary);
        em.flush();
        newsSummaryConfidentialImage.setNewsSummary(newsSummary);
        newsSummaryConfidentialImageRepository.saveAndFlush(newsSummaryConfidentialImage);
        Long newsSummaryId = newsSummary.getId();

        // Get all the newsSummaryConfidentialImageList where newsSummary equals to newsSummaryId
        defaultNewsSummaryConfidentialImageShouldBeFound("newsSummaryId.equals=" + newsSummaryId);

        // Get all the newsSummaryConfidentialImageList where newsSummary equals to newsSummaryId + 1
        defaultNewsSummaryConfidentialImageShouldNotBeFound("newsSummaryId.equals=" + (newsSummaryId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsSummaryConfidentialImageShouldBeFound(String filter) throws Exception {
        restNewsSummaryConfidentialImageMockMvc.perform(get("/api/news-summary-confidential-images?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryConfidentialImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionTranslation").value(hasItem(DEFAULT_DESCRIPTION_TRANSLATION.toString())))
            .andExpect(jsonPath("$.[*].imageOrder").value(hasItem(DEFAULT_IMAGE_ORDER)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsSummaryConfidentialImageShouldNotBeFound(String filter) throws Exception {
        restNewsSummaryConfidentialImageMockMvc.perform(get("/api/news-summary-confidential-images?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsSummaryConfidentialImage() throws Exception {
        // Get the newsSummaryConfidentialImage
        restNewsSummaryConfidentialImageMockMvc.perform(get("/api/news-summary-confidential-images/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsSummaryConfidentialImage() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageService.save(newsSummaryConfidentialImage);

        int databaseSizeBeforeUpdate = newsSummaryConfidentialImageRepository.findAll().size();

        // Update the newsSummaryConfidentialImage
        NewsSummaryConfidentialImage updatedNewsSummaryConfidentialImage = newsSummaryConfidentialImageRepository.findOne(newsSummaryConfidentialImage.getId());
        updatedNewsSummaryConfidentialImage
            .filename(UPDATED_FILENAME)
            .blob(UPDATED_BLOB)
            .blobContentType(UPDATED_BLOB_CONTENT_TYPE)
            .description(UPDATED_DESCRIPTION)
            .descriptionTranslation(UPDATED_DESCRIPTION_TRANSLATION)
            .imageOrder(UPDATED_IMAGE_ORDER);

        restNewsSummaryConfidentialImageMockMvc.perform(put("/api/news-summary-confidential-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsSummaryConfidentialImage)))
            .andExpect(status().isOk());

        // Validate the NewsSummaryConfidentialImage in the database
        List<NewsSummaryConfidentialImage> newsSummaryConfidentialImageList = newsSummaryConfidentialImageRepository.findAll();
        assertThat(newsSummaryConfidentialImageList).hasSize(databaseSizeBeforeUpdate);
        NewsSummaryConfidentialImage testNewsSummaryConfidentialImage = newsSummaryConfidentialImageList.get(newsSummaryConfidentialImageList.size() - 1);
        assertThat(testNewsSummaryConfidentialImage.getFilename()).isEqualTo(UPDATED_FILENAME);
        assertThat(testNewsSummaryConfidentialImage.getBlob()).isEqualTo(UPDATED_BLOB);
        assertThat(testNewsSummaryConfidentialImage.getBlobContentType()).isEqualTo(UPDATED_BLOB_CONTENT_TYPE);
        assertThat(testNewsSummaryConfidentialImage.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testNewsSummaryConfidentialImage.getDescriptionTranslation()).isEqualTo(UPDATED_DESCRIPTION_TRANSLATION);
        assertThat(testNewsSummaryConfidentialImage.getImageOrder()).isEqualTo(UPDATED_IMAGE_ORDER);

        // Validate the NewsSummaryConfidentialImage in Elasticsearch
        NewsSummaryConfidentialImage newsSummaryConfidentialImageEs = newsSummaryConfidentialImageSearchRepository.findOne(testNewsSummaryConfidentialImage.getId());
        assertThat(newsSummaryConfidentialImageEs).isEqualToComparingFieldByField(testNewsSummaryConfidentialImage);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsSummaryConfidentialImage() throws Exception {
        int databaseSizeBeforeUpdate = newsSummaryConfidentialImageRepository.findAll().size();

        // Create the NewsSummaryConfidentialImage

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsSummaryConfidentialImageMockMvc.perform(put("/api/news-summary-confidential-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryConfidentialImage)))
            .andExpect(status().isCreated());

        // Validate the NewsSummaryConfidentialImage in the database
        List<NewsSummaryConfidentialImage> newsSummaryConfidentialImageList = newsSummaryConfidentialImageRepository.findAll();
        assertThat(newsSummaryConfidentialImageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsSummaryConfidentialImage() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageService.save(newsSummaryConfidentialImage);

        int databaseSizeBeforeDelete = newsSummaryConfidentialImageRepository.findAll().size();

        // Get the newsSummaryConfidentialImage
        restNewsSummaryConfidentialImageMockMvc.perform(delete("/api/news-summary-confidential-images/{id}", newsSummaryConfidentialImage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsSummaryConfidentialImageExistsInEs = newsSummaryConfidentialImageSearchRepository.exists(newsSummaryConfidentialImage.getId());
        assertThat(newsSummaryConfidentialImageExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsSummaryConfidentialImage> newsSummaryConfidentialImageList = newsSummaryConfidentialImageRepository.findAll();
        assertThat(newsSummaryConfidentialImageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsSummaryConfidentialImage() throws Exception {
        // Initialize the database
        newsSummaryConfidentialImageService.save(newsSummaryConfidentialImage);

        // Search the newsSummaryConfidentialImage
        restNewsSummaryConfidentialImageMockMvc.perform(get("/api/_search/news-summary-confidential-images?query=id:" + newsSummaryConfidentialImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryConfidentialImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionTranslation").value(hasItem(DEFAULT_DESCRIPTION_TRANSLATION.toString())))
            .andExpect(jsonPath("$.[*].imageOrder").value(hasItem(DEFAULT_IMAGE_ORDER)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsSummaryConfidentialImage.class);
        NewsSummaryConfidentialImage newsSummaryConfidentialImage1 = new NewsSummaryConfidentialImage();
        newsSummaryConfidentialImage1.setId(1L);
        NewsSummaryConfidentialImage newsSummaryConfidentialImage2 = new NewsSummaryConfidentialImage();
        newsSummaryConfidentialImage2.setId(newsSummaryConfidentialImage1.getId());
        assertThat(newsSummaryConfidentialImage1).isEqualTo(newsSummaryConfidentialImage2);
        newsSummaryConfidentialImage2.setId(2L);
        assertThat(newsSummaryConfidentialImage1).isNotEqualTo(newsSummaryConfidentialImage2);
        newsSummaryConfidentialImage1.setId(null);
        assertThat(newsSummaryConfidentialImage1).isNotEqualTo(newsSummaryConfidentialImage2);
    }
}
