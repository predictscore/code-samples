package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.AuthorName;
import biz.newparadigm.meridian.domain.Author;
import biz.newparadigm.meridian.repository.AuthorNameRepository;
import biz.newparadigm.meridian.service.AuthorNameService;
import biz.newparadigm.meridian.repository.search.AuthorNameSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.AuthorNameCriteria;
import biz.newparadigm.meridian.service.AuthorNameQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AuthorNameResource REST controller.
 *
 * @see AuthorNameResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class AuthorNameResourceIntTest {

    private static final String DEFAULT_ORIGINAL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ENGLISH_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ENGLISH_NAME = "BBBBBBBBBB";

    @Autowired
    private AuthorNameRepository authorNameRepository;

    @Autowired
    private AuthorNameService authorNameService;

    @Autowired
    private AuthorNameSearchRepository authorNameSearchRepository;

    @Autowired
    private AuthorNameQueryService authorNameQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAuthorNameMockMvc;

    private AuthorName authorName;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AuthorNameResource authorNameResource = new AuthorNameResource(authorNameService, authorNameQueryService);
        this.restAuthorNameMockMvc = MockMvcBuilders.standaloneSetup(authorNameResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AuthorName createEntity(EntityManager em) {
        AuthorName authorName = new AuthorName()
            .originalName(DEFAULT_ORIGINAL_NAME)
            .englishName(DEFAULT_ENGLISH_NAME);
        // Add required entity
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        authorName.setAuthor(author);
        return authorName;
    }

    @Before
    public void initTest() {
        authorNameSearchRepository.deleteAll();
        authorName = createEntity(em);
    }

    @Test
    @Transactional
    public void createAuthorName() throws Exception {
        int databaseSizeBeforeCreate = authorNameRepository.findAll().size();

        // Create the AuthorName
        restAuthorNameMockMvc.perform(post("/api/author-names")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorName)))
            .andExpect(status().isCreated());

        // Validate the AuthorName in the database
        List<AuthorName> authorNameList = authorNameRepository.findAll();
        assertThat(authorNameList).hasSize(databaseSizeBeforeCreate + 1);
        AuthorName testAuthorName = authorNameList.get(authorNameList.size() - 1);
        assertThat(testAuthorName.getOriginalName()).isEqualTo(DEFAULT_ORIGINAL_NAME);
        assertThat(testAuthorName.getEnglishName()).isEqualTo(DEFAULT_ENGLISH_NAME);

        // Validate the AuthorName in Elasticsearch
        AuthorName authorNameEs = authorNameSearchRepository.findOne(testAuthorName.getId());
        assertThat(authorNameEs).isEqualToComparingFieldByField(testAuthorName);
    }

    @Test
    @Transactional
    public void createAuthorNameWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = authorNameRepository.findAll().size();

        // Create the AuthorName with an existing ID
        authorName.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAuthorNameMockMvc.perform(post("/api/author-names")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorName)))
            .andExpect(status().isBadRequest());

        // Validate the AuthorName in the database
        List<AuthorName> authorNameList = authorNameRepository.findAll();
        assertThat(authorNameList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkOriginalNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorNameRepository.findAll().size();
        // set the field null
        authorName.setOriginalName(null);

        // Create the AuthorName, which fails.

        restAuthorNameMockMvc.perform(post("/api/author-names")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorName)))
            .andExpect(status().isBadRequest());

        List<AuthorName> authorNameList = authorNameRepository.findAll();
        assertThat(authorNameList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEnglishNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorNameRepository.findAll().size();
        // set the field null
        authorName.setEnglishName(null);

        // Create the AuthorName, which fails.

        restAuthorNameMockMvc.perform(post("/api/author-names")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorName)))
            .andExpect(status().isBadRequest());

        List<AuthorName> authorNameList = authorNameRepository.findAll();
        assertThat(authorNameList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAuthorNames() throws Exception {
        // Initialize the database
        authorNameRepository.saveAndFlush(authorName);

        // Get all the authorNameList
        restAuthorNameMockMvc.perform(get("/api/author-names?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorName.getId().intValue())))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].englishName").value(hasItem(DEFAULT_ENGLISH_NAME.toString())));
    }

    @Test
    @Transactional
    public void getAuthorName() throws Exception {
        // Initialize the database
        authorNameRepository.saveAndFlush(authorName);

        // Get the authorName
        restAuthorNameMockMvc.perform(get("/api/author-names/{id}", authorName.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(authorName.getId().intValue()))
            .andExpect(jsonPath("$.originalName").value(DEFAULT_ORIGINAL_NAME.toString()))
            .andExpect(jsonPath("$.englishName").value(DEFAULT_ENGLISH_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllAuthorNamesByOriginalNameIsEqualToSomething() throws Exception {
        // Initialize the database
        authorNameRepository.saveAndFlush(authorName);

        // Get all the authorNameList where originalName equals to DEFAULT_ORIGINAL_NAME
        defaultAuthorNameShouldBeFound("originalName.equals=" + DEFAULT_ORIGINAL_NAME);

        // Get all the authorNameList where originalName equals to UPDATED_ORIGINAL_NAME
        defaultAuthorNameShouldNotBeFound("originalName.equals=" + UPDATED_ORIGINAL_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorNamesByOriginalNameIsInShouldWork() throws Exception {
        // Initialize the database
        authorNameRepository.saveAndFlush(authorName);

        // Get all the authorNameList where originalName in DEFAULT_ORIGINAL_NAME or UPDATED_ORIGINAL_NAME
        defaultAuthorNameShouldBeFound("originalName.in=" + DEFAULT_ORIGINAL_NAME + "," + UPDATED_ORIGINAL_NAME);

        // Get all the authorNameList where originalName equals to UPDATED_ORIGINAL_NAME
        defaultAuthorNameShouldNotBeFound("originalName.in=" + UPDATED_ORIGINAL_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorNamesByOriginalNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorNameRepository.saveAndFlush(authorName);

        // Get all the authorNameList where originalName is not null
        defaultAuthorNameShouldBeFound("originalName.specified=true");

        // Get all the authorNameList where originalName is null
        defaultAuthorNameShouldNotBeFound("originalName.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorNamesByEnglishNameIsEqualToSomething() throws Exception {
        // Initialize the database
        authorNameRepository.saveAndFlush(authorName);

        // Get all the authorNameList where englishName equals to DEFAULT_ENGLISH_NAME
        defaultAuthorNameShouldBeFound("englishName.equals=" + DEFAULT_ENGLISH_NAME);

        // Get all the authorNameList where englishName equals to UPDATED_ENGLISH_NAME
        defaultAuthorNameShouldNotBeFound("englishName.equals=" + UPDATED_ENGLISH_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorNamesByEnglishNameIsInShouldWork() throws Exception {
        // Initialize the database
        authorNameRepository.saveAndFlush(authorName);

        // Get all the authorNameList where englishName in DEFAULT_ENGLISH_NAME or UPDATED_ENGLISH_NAME
        defaultAuthorNameShouldBeFound("englishName.in=" + DEFAULT_ENGLISH_NAME + "," + UPDATED_ENGLISH_NAME);

        // Get all the authorNameList where englishName equals to UPDATED_ENGLISH_NAME
        defaultAuthorNameShouldNotBeFound("englishName.in=" + UPDATED_ENGLISH_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorNamesByEnglishNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorNameRepository.saveAndFlush(authorName);

        // Get all the authorNameList where englishName is not null
        defaultAuthorNameShouldBeFound("englishName.specified=true");

        // Get all the authorNameList where englishName is null
        defaultAuthorNameShouldNotBeFound("englishName.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorNamesByAuthorIsEqualToSomething() throws Exception {
        // Initialize the database
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        authorName.setAuthor(author);
        authorNameRepository.saveAndFlush(authorName);
        Long authorId = author.getId();

        // Get all the authorNameList where author equals to authorId
        defaultAuthorNameShouldBeFound("authorId.equals=" + authorId);

        // Get all the authorNameList where author equals to authorId + 1
        defaultAuthorNameShouldNotBeFound("authorId.equals=" + (authorId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultAuthorNameShouldBeFound(String filter) throws Exception {
        restAuthorNameMockMvc.perform(get("/api/author-names?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorName.getId().intValue())))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].englishName").value(hasItem(DEFAULT_ENGLISH_NAME.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultAuthorNameShouldNotBeFound(String filter) throws Exception {
        restAuthorNameMockMvc.perform(get("/api/author-names?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingAuthorName() throws Exception {
        // Get the authorName
        restAuthorNameMockMvc.perform(get("/api/author-names/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAuthorName() throws Exception {
        // Initialize the database
        authorNameService.save(authorName);

        int databaseSizeBeforeUpdate = authorNameRepository.findAll().size();

        // Update the authorName
        AuthorName updatedAuthorName = authorNameRepository.findOne(authorName.getId());
        updatedAuthorName
            .originalName(UPDATED_ORIGINAL_NAME)
            .englishName(UPDATED_ENGLISH_NAME);

        restAuthorNameMockMvc.perform(put("/api/author-names")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAuthorName)))
            .andExpect(status().isOk());

        // Validate the AuthorName in the database
        List<AuthorName> authorNameList = authorNameRepository.findAll();
        assertThat(authorNameList).hasSize(databaseSizeBeforeUpdate);
        AuthorName testAuthorName = authorNameList.get(authorNameList.size() - 1);
        assertThat(testAuthorName.getOriginalName()).isEqualTo(UPDATED_ORIGINAL_NAME);
        assertThat(testAuthorName.getEnglishName()).isEqualTo(UPDATED_ENGLISH_NAME);

        // Validate the AuthorName in Elasticsearch
        AuthorName authorNameEs = authorNameSearchRepository.findOne(testAuthorName.getId());
        assertThat(authorNameEs).isEqualToComparingFieldByField(testAuthorName);
    }

    @Test
    @Transactional
    public void updateNonExistingAuthorName() throws Exception {
        int databaseSizeBeforeUpdate = authorNameRepository.findAll().size();

        // Create the AuthorName

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAuthorNameMockMvc.perform(put("/api/author-names")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorName)))
            .andExpect(status().isCreated());

        // Validate the AuthorName in the database
        List<AuthorName> authorNameList = authorNameRepository.findAll();
        assertThat(authorNameList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAuthorName() throws Exception {
        // Initialize the database
        authorNameService.save(authorName);

        int databaseSizeBeforeDelete = authorNameRepository.findAll().size();

        // Get the authorName
        restAuthorNameMockMvc.perform(delete("/api/author-names/{id}", authorName.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean authorNameExistsInEs = authorNameSearchRepository.exists(authorName.getId());
        assertThat(authorNameExistsInEs).isFalse();

        // Validate the database is empty
        List<AuthorName> authorNameList = authorNameRepository.findAll();
        assertThat(authorNameList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAuthorName() throws Exception {
        // Initialize the database
        authorNameService.save(authorName);

        // Search the authorName
        restAuthorNameMockMvc.perform(get("/api/_search/author-names?query=id:" + authorName.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorName.getId().intValue())))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].englishName").value(hasItem(DEFAULT_ENGLISH_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AuthorName.class);
        AuthorName authorName1 = new AuthorName();
        authorName1.setId(1L);
        AuthorName authorName2 = new AuthorName();
        authorName2.setId(authorName1.getId());
        assertThat(authorName1).isEqualTo(authorName2);
        authorName2.setId(2L);
        assertThat(authorName1).isNotEqualTo(authorName2);
        authorName1.setId(null);
        assertThat(authorName1).isNotEqualTo(authorName2);
    }
}
