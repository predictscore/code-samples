package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.SiteChatgroupName;
import biz.newparadigm.meridian.domain.SiteChatgroup;
import biz.newparadigm.meridian.repository.SiteChatgroupNameRepository;
import biz.newparadigm.meridian.service.SiteChatgroupNameService;
import biz.newparadigm.meridian.repository.search.SiteChatgroupNameSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.SiteChatgroupNameCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupNameQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SiteChatgroupNameResource REST controller.
 *
 * @see SiteChatgroupNameResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class SiteChatgroupNameResourceIntTest {

    private static final String DEFAULT_ORIGINAL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ENGLISH_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ENGLISH_NAME = "BBBBBBBBBB";

    @Autowired
    private SiteChatgroupNameRepository siteChatgroupNameRepository;

    @Autowired
    private SiteChatgroupNameService siteChatgroupNameService;

    @Autowired
    private SiteChatgroupNameSearchRepository siteChatgroupNameSearchRepository;

    @Autowired
    private SiteChatgroupNameQueryService siteChatgroupNameQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSiteChatgroupNameMockMvc;

    private SiteChatgroupName siteChatgroupName;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SiteChatgroupNameResource siteChatgroupNameResource = new SiteChatgroupNameResource(siteChatgroupNameService, siteChatgroupNameQueryService);
        this.restSiteChatgroupNameMockMvc = MockMvcBuilders.standaloneSetup(siteChatgroupNameResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SiteChatgroupName createEntity(EntityManager em) {
        SiteChatgroupName siteChatgroupName = new SiteChatgroupName()
            .originalName(DEFAULT_ORIGINAL_NAME)
            .englishName(DEFAULT_ENGLISH_NAME);
        // Add required entity
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupName.setSiteChatgroup(siteChatgroup);
        return siteChatgroupName;
    }

    @Before
    public void initTest() {
        siteChatgroupNameSearchRepository.deleteAll();
        siteChatgroupName = createEntity(em);
    }

    @Test
    @Transactional
    public void createSiteChatgroupName() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupNameRepository.findAll().size();

        // Create the SiteChatgroupName
        restSiteChatgroupNameMockMvc.perform(post("/api/site-chatgroup-names")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupName)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupName in the database
        List<SiteChatgroupName> siteChatgroupNameList = siteChatgroupNameRepository.findAll();
        assertThat(siteChatgroupNameList).hasSize(databaseSizeBeforeCreate + 1);
        SiteChatgroupName testSiteChatgroupName = siteChatgroupNameList.get(siteChatgroupNameList.size() - 1);
        assertThat(testSiteChatgroupName.getOriginalName()).isEqualTo(DEFAULT_ORIGINAL_NAME);
        assertThat(testSiteChatgroupName.getEnglishName()).isEqualTo(DEFAULT_ENGLISH_NAME);

        // Validate the SiteChatgroupName in Elasticsearch
        SiteChatgroupName siteChatgroupNameEs = siteChatgroupNameSearchRepository.findOne(testSiteChatgroupName.getId());
        assertThat(siteChatgroupNameEs).isEqualToComparingFieldByField(testSiteChatgroupName);
    }

    @Test
    @Transactional
    public void createSiteChatgroupNameWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupNameRepository.findAll().size();

        // Create the SiteChatgroupName with an existing ID
        siteChatgroupName.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSiteChatgroupNameMockMvc.perform(post("/api/site-chatgroup-names")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupName)))
            .andExpect(status().isBadRequest());

        // Validate the SiteChatgroupName in the database
        List<SiteChatgroupName> siteChatgroupNameList = siteChatgroupNameRepository.findAll();
        assertThat(siteChatgroupNameList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkOriginalNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupNameRepository.findAll().size();
        // set the field null
        siteChatgroupName.setOriginalName(null);

        // Create the SiteChatgroupName, which fails.

        restSiteChatgroupNameMockMvc.perform(post("/api/site-chatgroup-names")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupName)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupName> siteChatgroupNameList = siteChatgroupNameRepository.findAll();
        assertThat(siteChatgroupNameList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEnglishNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupNameRepository.findAll().size();
        // set the field null
        siteChatgroupName.setEnglishName(null);

        // Create the SiteChatgroupName, which fails.

        restSiteChatgroupNameMockMvc.perform(post("/api/site-chatgroup-names")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupName)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupName> siteChatgroupNameList = siteChatgroupNameRepository.findAll();
        assertThat(siteChatgroupNameList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNames() throws Exception {
        // Initialize the database
        siteChatgroupNameRepository.saveAndFlush(siteChatgroupName);

        // Get all the siteChatgroupNameList
        restSiteChatgroupNameMockMvc.perform(get("/api/site-chatgroup-names?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupName.getId().intValue())))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].englishName").value(hasItem(DEFAULT_ENGLISH_NAME.toString())));
    }

    @Test
    @Transactional
    public void getSiteChatgroupName() throws Exception {
        // Initialize the database
        siteChatgroupNameRepository.saveAndFlush(siteChatgroupName);

        // Get the siteChatgroupName
        restSiteChatgroupNameMockMvc.perform(get("/api/site-chatgroup-names/{id}", siteChatgroupName.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(siteChatgroupName.getId().intValue()))
            .andExpect(jsonPath("$.originalName").value(DEFAULT_ORIGINAL_NAME.toString()))
            .andExpect(jsonPath("$.englishName").value(DEFAULT_ENGLISH_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNamesByOriginalNameIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupNameRepository.saveAndFlush(siteChatgroupName);

        // Get all the siteChatgroupNameList where originalName equals to DEFAULT_ORIGINAL_NAME
        defaultSiteChatgroupNameShouldBeFound("originalName.equals=" + DEFAULT_ORIGINAL_NAME);

        // Get all the siteChatgroupNameList where originalName equals to UPDATED_ORIGINAL_NAME
        defaultSiteChatgroupNameShouldNotBeFound("originalName.equals=" + UPDATED_ORIGINAL_NAME);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNamesByOriginalNameIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupNameRepository.saveAndFlush(siteChatgroupName);

        // Get all the siteChatgroupNameList where originalName in DEFAULT_ORIGINAL_NAME or UPDATED_ORIGINAL_NAME
        defaultSiteChatgroupNameShouldBeFound("originalName.in=" + DEFAULT_ORIGINAL_NAME + "," + UPDATED_ORIGINAL_NAME);

        // Get all the siteChatgroupNameList where originalName equals to UPDATED_ORIGINAL_NAME
        defaultSiteChatgroupNameShouldNotBeFound("originalName.in=" + UPDATED_ORIGINAL_NAME);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNamesByOriginalNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupNameRepository.saveAndFlush(siteChatgroupName);

        // Get all the siteChatgroupNameList where originalName is not null
        defaultSiteChatgroupNameShouldBeFound("originalName.specified=true");

        // Get all the siteChatgroupNameList where originalName is null
        defaultSiteChatgroupNameShouldNotBeFound("originalName.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNamesByEnglishNameIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupNameRepository.saveAndFlush(siteChatgroupName);

        // Get all the siteChatgroupNameList where englishName equals to DEFAULT_ENGLISH_NAME
        defaultSiteChatgroupNameShouldBeFound("englishName.equals=" + DEFAULT_ENGLISH_NAME);

        // Get all the siteChatgroupNameList where englishName equals to UPDATED_ENGLISH_NAME
        defaultSiteChatgroupNameShouldNotBeFound("englishName.equals=" + UPDATED_ENGLISH_NAME);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNamesByEnglishNameIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupNameRepository.saveAndFlush(siteChatgroupName);

        // Get all the siteChatgroupNameList where englishName in DEFAULT_ENGLISH_NAME or UPDATED_ENGLISH_NAME
        defaultSiteChatgroupNameShouldBeFound("englishName.in=" + DEFAULT_ENGLISH_NAME + "," + UPDATED_ENGLISH_NAME);

        // Get all the siteChatgroupNameList where englishName equals to UPDATED_ENGLISH_NAME
        defaultSiteChatgroupNameShouldNotBeFound("englishName.in=" + UPDATED_ENGLISH_NAME);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNamesByEnglishNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupNameRepository.saveAndFlush(siteChatgroupName);

        // Get all the siteChatgroupNameList where englishName is not null
        defaultSiteChatgroupNameShouldBeFound("englishName.specified=true");

        // Get all the siteChatgroupNameList where englishName is null
        defaultSiteChatgroupNameShouldNotBeFound("englishName.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNamesBySiteChatgroupIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupName.setSiteChatgroup(siteChatgroup);
        siteChatgroupNameRepository.saveAndFlush(siteChatgroupName);
        Long siteChatgroupId = siteChatgroup.getId();

        // Get all the siteChatgroupNameList where siteChatgroup equals to siteChatgroupId
        defaultSiteChatgroupNameShouldBeFound("siteChatgroupId.equals=" + siteChatgroupId);

        // Get all the siteChatgroupNameList where siteChatgroup equals to siteChatgroupId + 1
        defaultSiteChatgroupNameShouldNotBeFound("siteChatgroupId.equals=" + (siteChatgroupId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSiteChatgroupNameShouldBeFound(String filter) throws Exception {
        restSiteChatgroupNameMockMvc.perform(get("/api/site-chatgroup-names?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupName.getId().intValue())))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].englishName").value(hasItem(DEFAULT_ENGLISH_NAME.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSiteChatgroupNameShouldNotBeFound(String filter) throws Exception {
        restSiteChatgroupNameMockMvc.perform(get("/api/site-chatgroup-names?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingSiteChatgroupName() throws Exception {
        // Get the siteChatgroupName
        restSiteChatgroupNameMockMvc.perform(get("/api/site-chatgroup-names/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSiteChatgroupName() throws Exception {
        // Initialize the database
        siteChatgroupNameService.save(siteChatgroupName);

        int databaseSizeBeforeUpdate = siteChatgroupNameRepository.findAll().size();

        // Update the siteChatgroupName
        SiteChatgroupName updatedSiteChatgroupName = siteChatgroupNameRepository.findOne(siteChatgroupName.getId());
        updatedSiteChatgroupName
            .originalName(UPDATED_ORIGINAL_NAME)
            .englishName(UPDATED_ENGLISH_NAME);

        restSiteChatgroupNameMockMvc.perform(put("/api/site-chatgroup-names")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSiteChatgroupName)))
            .andExpect(status().isOk());

        // Validate the SiteChatgroupName in the database
        List<SiteChatgroupName> siteChatgroupNameList = siteChatgroupNameRepository.findAll();
        assertThat(siteChatgroupNameList).hasSize(databaseSizeBeforeUpdate);
        SiteChatgroupName testSiteChatgroupName = siteChatgroupNameList.get(siteChatgroupNameList.size() - 1);
        assertThat(testSiteChatgroupName.getOriginalName()).isEqualTo(UPDATED_ORIGINAL_NAME);
        assertThat(testSiteChatgroupName.getEnglishName()).isEqualTo(UPDATED_ENGLISH_NAME);

        // Validate the SiteChatgroupName in Elasticsearch
        SiteChatgroupName siteChatgroupNameEs = siteChatgroupNameSearchRepository.findOne(testSiteChatgroupName.getId());
        assertThat(siteChatgroupNameEs).isEqualToComparingFieldByField(testSiteChatgroupName);
    }

    @Test
    @Transactional
    public void updateNonExistingSiteChatgroupName() throws Exception {
        int databaseSizeBeforeUpdate = siteChatgroupNameRepository.findAll().size();

        // Create the SiteChatgroupName

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSiteChatgroupNameMockMvc.perform(put("/api/site-chatgroup-names")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupName)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupName in the database
        List<SiteChatgroupName> siteChatgroupNameList = siteChatgroupNameRepository.findAll();
        assertThat(siteChatgroupNameList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSiteChatgroupName() throws Exception {
        // Initialize the database
        siteChatgroupNameService.save(siteChatgroupName);

        int databaseSizeBeforeDelete = siteChatgroupNameRepository.findAll().size();

        // Get the siteChatgroupName
        restSiteChatgroupNameMockMvc.perform(delete("/api/site-chatgroup-names/{id}", siteChatgroupName.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean siteChatgroupNameExistsInEs = siteChatgroupNameSearchRepository.exists(siteChatgroupName.getId());
        assertThat(siteChatgroupNameExistsInEs).isFalse();

        // Validate the database is empty
        List<SiteChatgroupName> siteChatgroupNameList = siteChatgroupNameRepository.findAll();
        assertThat(siteChatgroupNameList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSiteChatgroupName() throws Exception {
        // Initialize the database
        siteChatgroupNameService.save(siteChatgroupName);

        // Search the siteChatgroupName
        restSiteChatgroupNameMockMvc.perform(get("/api/_search/site-chatgroup-names?query=id:" + siteChatgroupName.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupName.getId().intValue())))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].englishName").value(hasItem(DEFAULT_ENGLISH_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SiteChatgroupName.class);
        SiteChatgroupName siteChatgroupName1 = new SiteChatgroupName();
        siteChatgroupName1.setId(1L);
        SiteChatgroupName siteChatgroupName2 = new SiteChatgroupName();
        siteChatgroupName2.setId(siteChatgroupName1.getId());
        assertThat(siteChatgroupName1).isEqualTo(siteChatgroupName2);
        siteChatgroupName2.setId(2L);
        assertThat(siteChatgroupName1).isNotEqualTo(siteChatgroupName2);
        siteChatgroupName1.setId(null);
        assertThat(siteChatgroupName1).isNotEqualTo(siteChatgroupName2);
    }
}
