package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.AuthorToContact;
import biz.newparadigm.meridian.domain.Author;
import biz.newparadigm.meridian.domain.ContactType;
import biz.newparadigm.meridian.repository.AuthorToContactRepository;
import biz.newparadigm.meridian.service.AuthorToContactService;
import biz.newparadigm.meridian.repository.search.AuthorToContactSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.AuthorToContactCriteria;
import biz.newparadigm.meridian.service.AuthorToContactQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AuthorToContactResource REST controller.
 *
 * @see AuthorToContactResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class AuthorToContactResourceIntTest {

    private static final String DEFAULT_CONTACT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_VALUE = "BBBBBBBBBB";

    @Autowired
    private AuthorToContactRepository authorToContactRepository;

    @Autowired
    private AuthorToContactService authorToContactService;

    @Autowired
    private AuthorToContactSearchRepository authorToContactSearchRepository;

    @Autowired
    private AuthorToContactQueryService authorToContactQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAuthorToContactMockMvc;

    private AuthorToContact authorToContact;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AuthorToContactResource authorToContactResource = new AuthorToContactResource(authorToContactService, authorToContactQueryService);
        this.restAuthorToContactMockMvc = MockMvcBuilders.standaloneSetup(authorToContactResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AuthorToContact createEntity(EntityManager em) {
        AuthorToContact authorToContact = new AuthorToContact()
            .contactValue(DEFAULT_CONTACT_VALUE);
        // Add required entity
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        authorToContact.setAuthor(author);
        // Add required entity
        ContactType contactType = ContactTypeResourceIntTest.createEntity(em);
        em.persist(contactType);
        em.flush();
        authorToContact.setContactType(contactType);
        return authorToContact;
    }

    @Before
    public void initTest() {
        authorToContactSearchRepository.deleteAll();
        authorToContact = createEntity(em);
    }

    @Test
    @Transactional
    public void createAuthorToContact() throws Exception {
        int databaseSizeBeforeCreate = authorToContactRepository.findAll().size();

        // Create the AuthorToContact
        restAuthorToContactMockMvc.perform(post("/api/author-to-contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorToContact)))
            .andExpect(status().isCreated());

        // Validate the AuthorToContact in the database
        List<AuthorToContact> authorToContactList = authorToContactRepository.findAll();
        assertThat(authorToContactList).hasSize(databaseSizeBeforeCreate + 1);
        AuthorToContact testAuthorToContact = authorToContactList.get(authorToContactList.size() - 1);
        assertThat(testAuthorToContact.getContactValue()).isEqualTo(DEFAULT_CONTACT_VALUE);

        // Validate the AuthorToContact in Elasticsearch
        AuthorToContact authorToContactEs = authorToContactSearchRepository.findOne(testAuthorToContact.getId());
        assertThat(authorToContactEs).isEqualToComparingFieldByField(testAuthorToContact);
    }

    @Test
    @Transactional
    public void createAuthorToContactWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = authorToContactRepository.findAll().size();

        // Create the AuthorToContact with an existing ID
        authorToContact.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAuthorToContactMockMvc.perform(post("/api/author-to-contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorToContact)))
            .andExpect(status().isBadRequest());

        // Validate the AuthorToContact in the database
        List<AuthorToContact> authorToContactList = authorToContactRepository.findAll();
        assertThat(authorToContactList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkContactValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorToContactRepository.findAll().size();
        // set the field null
        authorToContact.setContactValue(null);

        // Create the AuthorToContact, which fails.

        restAuthorToContactMockMvc.perform(post("/api/author-to-contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorToContact)))
            .andExpect(status().isBadRequest());

        List<AuthorToContact> authorToContactList = authorToContactRepository.findAll();
        assertThat(authorToContactList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAuthorToContacts() throws Exception {
        // Initialize the database
        authorToContactRepository.saveAndFlush(authorToContact);

        // Get all the authorToContactList
        restAuthorToContactMockMvc.perform(get("/api/author-to-contacts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorToContact.getId().intValue())))
            .andExpect(jsonPath("$.[*].contactValue").value(hasItem(DEFAULT_CONTACT_VALUE.toString())));
    }

    @Test
    @Transactional
    public void getAuthorToContact() throws Exception {
        // Initialize the database
        authorToContactRepository.saveAndFlush(authorToContact);

        // Get the authorToContact
        restAuthorToContactMockMvc.perform(get("/api/author-to-contacts/{id}", authorToContact.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(authorToContact.getId().intValue()))
            .andExpect(jsonPath("$.contactValue").value(DEFAULT_CONTACT_VALUE.toString()));
    }

    @Test
    @Transactional
    public void getAllAuthorToContactsByContactValueIsEqualToSomething() throws Exception {
        // Initialize the database
        authorToContactRepository.saveAndFlush(authorToContact);

        // Get all the authorToContactList where contactValue equals to DEFAULT_CONTACT_VALUE
        defaultAuthorToContactShouldBeFound("contactValue.equals=" + DEFAULT_CONTACT_VALUE);

        // Get all the authorToContactList where contactValue equals to UPDATED_CONTACT_VALUE
        defaultAuthorToContactShouldNotBeFound("contactValue.equals=" + UPDATED_CONTACT_VALUE);
    }

    @Test
    @Transactional
    public void getAllAuthorToContactsByContactValueIsInShouldWork() throws Exception {
        // Initialize the database
        authorToContactRepository.saveAndFlush(authorToContact);

        // Get all the authorToContactList where contactValue in DEFAULT_CONTACT_VALUE or UPDATED_CONTACT_VALUE
        defaultAuthorToContactShouldBeFound("contactValue.in=" + DEFAULT_CONTACT_VALUE + "," + UPDATED_CONTACT_VALUE);

        // Get all the authorToContactList where contactValue equals to UPDATED_CONTACT_VALUE
        defaultAuthorToContactShouldNotBeFound("contactValue.in=" + UPDATED_CONTACT_VALUE);
    }

    @Test
    @Transactional
    public void getAllAuthorToContactsByContactValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorToContactRepository.saveAndFlush(authorToContact);

        // Get all the authorToContactList where contactValue is not null
        defaultAuthorToContactShouldBeFound("contactValue.specified=true");

        // Get all the authorToContactList where contactValue is null
        defaultAuthorToContactShouldNotBeFound("contactValue.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorToContactsByAuthorIsEqualToSomething() throws Exception {
        // Initialize the database
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        authorToContact.setAuthor(author);
        authorToContactRepository.saveAndFlush(authorToContact);
        Long authorId = author.getId();

        // Get all the authorToContactList where author equals to authorId
        defaultAuthorToContactShouldBeFound("authorId.equals=" + authorId);

        // Get all the authorToContactList where author equals to authorId + 1
        defaultAuthorToContactShouldNotBeFound("authorId.equals=" + (authorId + 1));
    }


    @Test
    @Transactional
    public void getAllAuthorToContactsByContactTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        ContactType contactType = ContactTypeResourceIntTest.createEntity(em);
        em.persist(contactType);
        em.flush();
        authorToContact.setContactType(contactType);
        authorToContactRepository.saveAndFlush(authorToContact);
        Long contactTypeId = contactType.getId();

        // Get all the authorToContactList where contactType equals to contactTypeId
        defaultAuthorToContactShouldBeFound("contactTypeId.equals=" + contactTypeId);

        // Get all the authorToContactList where contactType equals to contactTypeId + 1
        defaultAuthorToContactShouldNotBeFound("contactTypeId.equals=" + (contactTypeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultAuthorToContactShouldBeFound(String filter) throws Exception {
        restAuthorToContactMockMvc.perform(get("/api/author-to-contacts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorToContact.getId().intValue())))
            .andExpect(jsonPath("$.[*].contactValue").value(hasItem(DEFAULT_CONTACT_VALUE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultAuthorToContactShouldNotBeFound(String filter) throws Exception {
        restAuthorToContactMockMvc.perform(get("/api/author-to-contacts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingAuthorToContact() throws Exception {
        // Get the authorToContact
        restAuthorToContactMockMvc.perform(get("/api/author-to-contacts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAuthorToContact() throws Exception {
        // Initialize the database
        authorToContactService.save(authorToContact);

        int databaseSizeBeforeUpdate = authorToContactRepository.findAll().size();

        // Update the authorToContact
        AuthorToContact updatedAuthorToContact = authorToContactRepository.findOne(authorToContact.getId());
        updatedAuthorToContact
            .contactValue(UPDATED_CONTACT_VALUE);

        restAuthorToContactMockMvc.perform(put("/api/author-to-contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAuthorToContact)))
            .andExpect(status().isOk());

        // Validate the AuthorToContact in the database
        List<AuthorToContact> authorToContactList = authorToContactRepository.findAll();
        assertThat(authorToContactList).hasSize(databaseSizeBeforeUpdate);
        AuthorToContact testAuthorToContact = authorToContactList.get(authorToContactList.size() - 1);
        assertThat(testAuthorToContact.getContactValue()).isEqualTo(UPDATED_CONTACT_VALUE);

        // Validate the AuthorToContact in Elasticsearch
        AuthorToContact authorToContactEs = authorToContactSearchRepository.findOne(testAuthorToContact.getId());
        assertThat(authorToContactEs).isEqualToComparingFieldByField(testAuthorToContact);
    }

    @Test
    @Transactional
    public void updateNonExistingAuthorToContact() throws Exception {
        int databaseSizeBeforeUpdate = authorToContactRepository.findAll().size();

        // Create the AuthorToContact

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAuthorToContactMockMvc.perform(put("/api/author-to-contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorToContact)))
            .andExpect(status().isCreated());

        // Validate the AuthorToContact in the database
        List<AuthorToContact> authorToContactList = authorToContactRepository.findAll();
        assertThat(authorToContactList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAuthorToContact() throws Exception {
        // Initialize the database
        authorToContactService.save(authorToContact);

        int databaseSizeBeforeDelete = authorToContactRepository.findAll().size();

        // Get the authorToContact
        restAuthorToContactMockMvc.perform(delete("/api/author-to-contacts/{id}", authorToContact.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean authorToContactExistsInEs = authorToContactSearchRepository.exists(authorToContact.getId());
        assertThat(authorToContactExistsInEs).isFalse();

        // Validate the database is empty
        List<AuthorToContact> authorToContactList = authorToContactRepository.findAll();
        assertThat(authorToContactList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAuthorToContact() throws Exception {
        // Initialize the database
        authorToContactService.save(authorToContact);

        // Search the authorToContact
        restAuthorToContactMockMvc.perform(get("/api/_search/author-to-contacts?query=id:" + authorToContact.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorToContact.getId().intValue())))
            .andExpect(jsonPath("$.[*].contactValue").value(hasItem(DEFAULT_CONTACT_VALUE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AuthorToContact.class);
        AuthorToContact authorToContact1 = new AuthorToContact();
        authorToContact1.setId(1L);
        AuthorToContact authorToContact2 = new AuthorToContact();
        authorToContact2.setId(authorToContact1.getId());
        assertThat(authorToContact1).isEqualTo(authorToContact2);
        authorToContact2.setId(2L);
        assertThat(authorToContact1).isNotEqualTo(authorToContact2);
        authorToContact1.setId(null);
        assertThat(authorToContact1).isNotEqualTo(authorToContact2);
    }
}
