package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.SiteChatgroupNumUsers;
import biz.newparadigm.meridian.domain.SiteChatgroup;
import biz.newparadigm.meridian.repository.SiteChatgroupNumUsersRepository;
import biz.newparadigm.meridian.service.SiteChatgroupNumUsersService;
import biz.newparadigm.meridian.repository.search.SiteChatgroupNumUsersSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.SiteChatgroupNumUsersCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupNumUsersQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.sameInstant;
import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SiteChatgroupNumUsersResource REST controller.
 *
 * @see SiteChatgroupNumUsersResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class SiteChatgroupNumUsersResourceIntTest {

    private static final Integer DEFAULT_NUM_USERS = 1;
    private static final Integer UPDATED_NUM_USERS = 2;

    private static final ZonedDateTime DEFAULT_NUM_USER_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_NUM_USER_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private SiteChatgroupNumUsersRepository siteChatgroupNumUsersRepository;

    @Autowired
    private SiteChatgroupNumUsersService siteChatgroupNumUsersService;

    @Autowired
    private SiteChatgroupNumUsersSearchRepository siteChatgroupNumUsersSearchRepository;

    @Autowired
    private SiteChatgroupNumUsersQueryService siteChatgroupNumUsersQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSiteChatgroupNumUsersMockMvc;

    private SiteChatgroupNumUsers siteChatgroupNumUsers;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SiteChatgroupNumUsersResource siteChatgroupNumUsersResource = new SiteChatgroupNumUsersResource(siteChatgroupNumUsersService, siteChatgroupNumUsersQueryService);
        this.restSiteChatgroupNumUsersMockMvc = MockMvcBuilders.standaloneSetup(siteChatgroupNumUsersResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SiteChatgroupNumUsers createEntity(EntityManager em) {
        SiteChatgroupNumUsers siteChatgroupNumUsers = new SiteChatgroupNumUsers()
            .numUsers(DEFAULT_NUM_USERS)
            .numUserDate(DEFAULT_NUM_USER_DATE);
        // Add required entity
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupNumUsers.setSiteChatgroup(siteChatgroup);
        return siteChatgroupNumUsers;
    }

    @Before
    public void initTest() {
        siteChatgroupNumUsersSearchRepository.deleteAll();
        siteChatgroupNumUsers = createEntity(em);
    }

    @Test
    @Transactional
    public void createSiteChatgroupNumUsers() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupNumUsersRepository.findAll().size();

        // Create the SiteChatgroupNumUsers
        restSiteChatgroupNumUsersMockMvc.perform(post("/api/site-chatgroup-num-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupNumUsers)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupNumUsers in the database
        List<SiteChatgroupNumUsers> siteChatgroupNumUsersList = siteChatgroupNumUsersRepository.findAll();
        assertThat(siteChatgroupNumUsersList).hasSize(databaseSizeBeforeCreate + 1);
        SiteChatgroupNumUsers testSiteChatgroupNumUsers = siteChatgroupNumUsersList.get(siteChatgroupNumUsersList.size() - 1);
        assertThat(testSiteChatgroupNumUsers.getNumUsers()).isEqualTo(DEFAULT_NUM_USERS);
        assertThat(testSiteChatgroupNumUsers.getNumUserDate()).isEqualTo(DEFAULT_NUM_USER_DATE);

        // Validate the SiteChatgroupNumUsers in Elasticsearch
        SiteChatgroupNumUsers siteChatgroupNumUsersEs = siteChatgroupNumUsersSearchRepository.findOne(testSiteChatgroupNumUsers.getId());
        assertThat(siteChatgroupNumUsersEs).isEqualToComparingFieldByField(testSiteChatgroupNumUsers);
    }

    @Test
    @Transactional
    public void createSiteChatgroupNumUsersWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupNumUsersRepository.findAll().size();

        // Create the SiteChatgroupNumUsers with an existing ID
        siteChatgroupNumUsers.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSiteChatgroupNumUsersMockMvc.perform(post("/api/site-chatgroup-num-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupNumUsers)))
            .andExpect(status().isBadRequest());

        // Validate the SiteChatgroupNumUsers in the database
        List<SiteChatgroupNumUsers> siteChatgroupNumUsersList = siteChatgroupNumUsersRepository.findAll();
        assertThat(siteChatgroupNumUsersList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNumUsersIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupNumUsersRepository.findAll().size();
        // set the field null
        siteChatgroupNumUsers.setNumUsers(null);

        // Create the SiteChatgroupNumUsers, which fails.

        restSiteChatgroupNumUsersMockMvc.perform(post("/api/site-chatgroup-num-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupNumUsers)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupNumUsers> siteChatgroupNumUsersList = siteChatgroupNumUsersRepository.findAll();
        assertThat(siteChatgroupNumUsersList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumUserDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupNumUsersRepository.findAll().size();
        // set the field null
        siteChatgroupNumUsers.setNumUserDate(null);

        // Create the SiteChatgroupNumUsers, which fails.

        restSiteChatgroupNumUsersMockMvc.perform(post("/api/site-chatgroup-num-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupNumUsers)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupNumUsers> siteChatgroupNumUsersList = siteChatgroupNumUsersRepository.findAll();
        assertThat(siteChatgroupNumUsersList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumUsers() throws Exception {
        // Initialize the database
        siteChatgroupNumUsersRepository.saveAndFlush(siteChatgroupNumUsers);

        // Get all the siteChatgroupNumUsersList
        restSiteChatgroupNumUsersMockMvc.perform(get("/api/site-chatgroup-num-users?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupNumUsers.getId().intValue())))
            .andExpect(jsonPath("$.[*].numUsers").value(hasItem(DEFAULT_NUM_USERS)))
            .andExpect(jsonPath("$.[*].numUserDate").value(hasItem(sameInstant(DEFAULT_NUM_USER_DATE))));
    }

    @Test
    @Transactional
    public void getSiteChatgroupNumUsers() throws Exception {
        // Initialize the database
        siteChatgroupNumUsersRepository.saveAndFlush(siteChatgroupNumUsers);

        // Get the siteChatgroupNumUsers
        restSiteChatgroupNumUsersMockMvc.perform(get("/api/site-chatgroup-num-users/{id}", siteChatgroupNumUsers.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(siteChatgroupNumUsers.getId().intValue()))
            .andExpect(jsonPath("$.numUsers").value(DEFAULT_NUM_USERS))
            .andExpect(jsonPath("$.numUserDate").value(sameInstant(DEFAULT_NUM_USER_DATE)));
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumUsersByNumUsersIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupNumUsersRepository.saveAndFlush(siteChatgroupNumUsers);

        // Get all the siteChatgroupNumUsersList where numUsers equals to DEFAULT_NUM_USERS
        defaultSiteChatgroupNumUsersShouldBeFound("numUsers.equals=" + DEFAULT_NUM_USERS);

        // Get all the siteChatgroupNumUsersList where numUsers equals to UPDATED_NUM_USERS
        defaultSiteChatgroupNumUsersShouldNotBeFound("numUsers.equals=" + UPDATED_NUM_USERS);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumUsersByNumUsersIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupNumUsersRepository.saveAndFlush(siteChatgroupNumUsers);

        // Get all the siteChatgroupNumUsersList where numUsers in DEFAULT_NUM_USERS or UPDATED_NUM_USERS
        defaultSiteChatgroupNumUsersShouldBeFound("numUsers.in=" + DEFAULT_NUM_USERS + "," + UPDATED_NUM_USERS);

        // Get all the siteChatgroupNumUsersList where numUsers equals to UPDATED_NUM_USERS
        defaultSiteChatgroupNumUsersShouldNotBeFound("numUsers.in=" + UPDATED_NUM_USERS);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumUsersByNumUsersIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupNumUsersRepository.saveAndFlush(siteChatgroupNumUsers);

        // Get all the siteChatgroupNumUsersList where numUsers is not null
        defaultSiteChatgroupNumUsersShouldBeFound("numUsers.specified=true");

        // Get all the siteChatgroupNumUsersList where numUsers is null
        defaultSiteChatgroupNumUsersShouldNotBeFound("numUsers.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumUsersByNumUsersIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupNumUsersRepository.saveAndFlush(siteChatgroupNumUsers);

        // Get all the siteChatgroupNumUsersList where numUsers greater than or equals to DEFAULT_NUM_USERS
        defaultSiteChatgroupNumUsersShouldBeFound("numUsers.greaterOrEqualThan=" + DEFAULT_NUM_USERS);

        // Get all the siteChatgroupNumUsersList where numUsers greater than or equals to UPDATED_NUM_USERS
        defaultSiteChatgroupNumUsersShouldNotBeFound("numUsers.greaterOrEqualThan=" + UPDATED_NUM_USERS);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumUsersByNumUsersIsLessThanSomething() throws Exception {
        // Initialize the database
        siteChatgroupNumUsersRepository.saveAndFlush(siteChatgroupNumUsers);

        // Get all the siteChatgroupNumUsersList where numUsers less than or equals to DEFAULT_NUM_USERS
        defaultSiteChatgroupNumUsersShouldNotBeFound("numUsers.lessThan=" + DEFAULT_NUM_USERS);

        // Get all the siteChatgroupNumUsersList where numUsers less than or equals to UPDATED_NUM_USERS
        defaultSiteChatgroupNumUsersShouldBeFound("numUsers.lessThan=" + UPDATED_NUM_USERS);
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupNumUsersByNumUserDateIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupNumUsersRepository.saveAndFlush(siteChatgroupNumUsers);

        // Get all the siteChatgroupNumUsersList where numUserDate equals to DEFAULT_NUM_USER_DATE
        defaultSiteChatgroupNumUsersShouldBeFound("numUserDate.equals=" + DEFAULT_NUM_USER_DATE);

        // Get all the siteChatgroupNumUsersList where numUserDate equals to UPDATED_NUM_USER_DATE
        defaultSiteChatgroupNumUsersShouldNotBeFound("numUserDate.equals=" + UPDATED_NUM_USER_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumUsersByNumUserDateIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupNumUsersRepository.saveAndFlush(siteChatgroupNumUsers);

        // Get all the siteChatgroupNumUsersList where numUserDate in DEFAULT_NUM_USER_DATE or UPDATED_NUM_USER_DATE
        defaultSiteChatgroupNumUsersShouldBeFound("numUserDate.in=" + DEFAULT_NUM_USER_DATE + "," + UPDATED_NUM_USER_DATE);

        // Get all the siteChatgroupNumUsersList where numUserDate equals to UPDATED_NUM_USER_DATE
        defaultSiteChatgroupNumUsersShouldNotBeFound("numUserDate.in=" + UPDATED_NUM_USER_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumUsersByNumUserDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupNumUsersRepository.saveAndFlush(siteChatgroupNumUsers);

        // Get all the siteChatgroupNumUsersList where numUserDate is not null
        defaultSiteChatgroupNumUsersShouldBeFound("numUserDate.specified=true");

        // Get all the siteChatgroupNumUsersList where numUserDate is null
        defaultSiteChatgroupNumUsersShouldNotBeFound("numUserDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumUsersByNumUserDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupNumUsersRepository.saveAndFlush(siteChatgroupNumUsers);

        // Get all the siteChatgroupNumUsersList where numUserDate greater than or equals to DEFAULT_NUM_USER_DATE
        defaultSiteChatgroupNumUsersShouldBeFound("numUserDate.greaterOrEqualThan=" + DEFAULT_NUM_USER_DATE);

        // Get all the siteChatgroupNumUsersList where numUserDate greater than or equals to UPDATED_NUM_USER_DATE
        defaultSiteChatgroupNumUsersShouldNotBeFound("numUserDate.greaterOrEqualThan=" + UPDATED_NUM_USER_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumUsersByNumUserDateIsLessThanSomething() throws Exception {
        // Initialize the database
        siteChatgroupNumUsersRepository.saveAndFlush(siteChatgroupNumUsers);

        // Get all the siteChatgroupNumUsersList where numUserDate less than or equals to DEFAULT_NUM_USER_DATE
        defaultSiteChatgroupNumUsersShouldNotBeFound("numUserDate.lessThan=" + DEFAULT_NUM_USER_DATE);

        // Get all the siteChatgroupNumUsersList where numUserDate less than or equals to UPDATED_NUM_USER_DATE
        defaultSiteChatgroupNumUsersShouldBeFound("numUserDate.lessThan=" + UPDATED_NUM_USER_DATE);
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupNumUsersBySiteChatgroupIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupNumUsers.setSiteChatgroup(siteChatgroup);
        siteChatgroupNumUsersRepository.saveAndFlush(siteChatgroupNumUsers);
        Long siteChatgroupId = siteChatgroup.getId();

        // Get all the siteChatgroupNumUsersList where siteChatgroup equals to siteChatgroupId
        defaultSiteChatgroupNumUsersShouldBeFound("siteChatgroupId.equals=" + siteChatgroupId);

        // Get all the siteChatgroupNumUsersList where siteChatgroup equals to siteChatgroupId + 1
        defaultSiteChatgroupNumUsersShouldNotBeFound("siteChatgroupId.equals=" + (siteChatgroupId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSiteChatgroupNumUsersShouldBeFound(String filter) throws Exception {
        restSiteChatgroupNumUsersMockMvc.perform(get("/api/site-chatgroup-num-users?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupNumUsers.getId().intValue())))
            .andExpect(jsonPath("$.[*].numUsers").value(hasItem(DEFAULT_NUM_USERS)))
            .andExpect(jsonPath("$.[*].numUserDate").value(hasItem(sameInstant(DEFAULT_NUM_USER_DATE))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSiteChatgroupNumUsersShouldNotBeFound(String filter) throws Exception {
        restSiteChatgroupNumUsersMockMvc.perform(get("/api/site-chatgroup-num-users?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingSiteChatgroupNumUsers() throws Exception {
        // Get the siteChatgroupNumUsers
        restSiteChatgroupNumUsersMockMvc.perform(get("/api/site-chatgroup-num-users/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSiteChatgroupNumUsers() throws Exception {
        // Initialize the database
        siteChatgroupNumUsersService.save(siteChatgroupNumUsers);

        int databaseSizeBeforeUpdate = siteChatgroupNumUsersRepository.findAll().size();

        // Update the siteChatgroupNumUsers
        SiteChatgroupNumUsers updatedSiteChatgroupNumUsers = siteChatgroupNumUsersRepository.findOne(siteChatgroupNumUsers.getId());
        updatedSiteChatgroupNumUsers
            .numUsers(UPDATED_NUM_USERS)
            .numUserDate(UPDATED_NUM_USER_DATE);

        restSiteChatgroupNumUsersMockMvc.perform(put("/api/site-chatgroup-num-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSiteChatgroupNumUsers)))
            .andExpect(status().isOk());

        // Validate the SiteChatgroupNumUsers in the database
        List<SiteChatgroupNumUsers> siteChatgroupNumUsersList = siteChatgroupNumUsersRepository.findAll();
        assertThat(siteChatgroupNumUsersList).hasSize(databaseSizeBeforeUpdate);
        SiteChatgroupNumUsers testSiteChatgroupNumUsers = siteChatgroupNumUsersList.get(siteChatgroupNumUsersList.size() - 1);
        assertThat(testSiteChatgroupNumUsers.getNumUsers()).isEqualTo(UPDATED_NUM_USERS);
        assertThat(testSiteChatgroupNumUsers.getNumUserDate()).isEqualTo(UPDATED_NUM_USER_DATE);

        // Validate the SiteChatgroupNumUsers in Elasticsearch
        SiteChatgroupNumUsers siteChatgroupNumUsersEs = siteChatgroupNumUsersSearchRepository.findOne(testSiteChatgroupNumUsers.getId());
        assertThat(siteChatgroupNumUsersEs).isEqualToComparingFieldByField(testSiteChatgroupNumUsers);
    }

    @Test
    @Transactional
    public void updateNonExistingSiteChatgroupNumUsers() throws Exception {
        int databaseSizeBeforeUpdate = siteChatgroupNumUsersRepository.findAll().size();

        // Create the SiteChatgroupNumUsers

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSiteChatgroupNumUsersMockMvc.perform(put("/api/site-chatgroup-num-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupNumUsers)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupNumUsers in the database
        List<SiteChatgroupNumUsers> siteChatgroupNumUsersList = siteChatgroupNumUsersRepository.findAll();
        assertThat(siteChatgroupNumUsersList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSiteChatgroupNumUsers() throws Exception {
        // Initialize the database
        siteChatgroupNumUsersService.save(siteChatgroupNumUsers);

        int databaseSizeBeforeDelete = siteChatgroupNumUsersRepository.findAll().size();

        // Get the siteChatgroupNumUsers
        restSiteChatgroupNumUsersMockMvc.perform(delete("/api/site-chatgroup-num-users/{id}", siteChatgroupNumUsers.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean siteChatgroupNumUsersExistsInEs = siteChatgroupNumUsersSearchRepository.exists(siteChatgroupNumUsers.getId());
        assertThat(siteChatgroupNumUsersExistsInEs).isFalse();

        // Validate the database is empty
        List<SiteChatgroupNumUsers> siteChatgroupNumUsersList = siteChatgroupNumUsersRepository.findAll();
        assertThat(siteChatgroupNumUsersList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSiteChatgroupNumUsers() throws Exception {
        // Initialize the database
        siteChatgroupNumUsersService.save(siteChatgroupNumUsers);

        // Search the siteChatgroupNumUsers
        restSiteChatgroupNumUsersMockMvc.perform(get("/api/_search/site-chatgroup-num-users?query=id:" + siteChatgroupNumUsers.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupNumUsers.getId().intValue())))
            .andExpect(jsonPath("$.[*].numUsers").value(hasItem(DEFAULT_NUM_USERS)))
            .andExpect(jsonPath("$.[*].numUserDate").value(hasItem(sameInstant(DEFAULT_NUM_USER_DATE))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SiteChatgroupNumUsers.class);
        SiteChatgroupNumUsers siteChatgroupNumUsers1 = new SiteChatgroupNumUsers();
        siteChatgroupNumUsers1.setId(1L);
        SiteChatgroupNumUsers siteChatgroupNumUsers2 = new SiteChatgroupNumUsers();
        siteChatgroupNumUsers2.setId(siteChatgroupNumUsers1.getId());
        assertThat(siteChatgroupNumUsers1).isEqualTo(siteChatgroupNumUsers2);
        siteChatgroupNumUsers2.setId(2L);
        assertThat(siteChatgroupNumUsers1).isNotEqualTo(siteChatgroupNumUsers2);
        siteChatgroupNumUsers1.setId(null);
        assertThat(siteChatgroupNumUsers1).isNotEqualTo(siteChatgroupNumUsers2);
    }
}
