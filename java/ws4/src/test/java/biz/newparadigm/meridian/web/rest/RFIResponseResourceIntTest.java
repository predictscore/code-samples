package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.RFIResponse;
import biz.newparadigm.meridian.repository.RFIResponseRepository;
import biz.newparadigm.meridian.service.RFIResponseService;
import biz.newparadigm.meridian.repository.search.RFIResponseSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.RFIResponseCriteria;
import biz.newparadigm.meridian.service.RFIResponseQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.sameInstant;
import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RFIResponseResource REST controller.
 *
 * @see RFIResponseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class RFIResponseResourceIntTest {

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_SUBMISSION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_SUBMISSION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private RFIResponseRepository rFIResponseRepository;

    @Autowired
    private RFIResponseService rFIResponseService;

    @Autowired
    private RFIResponseSearchRepository rFIResponseSearchRepository;

    @Autowired
    private RFIResponseQueryService rFIResponseQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRFIResponseMockMvc;

    private RFIResponse rFIResponse;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RFIResponseResource rFIResponseResource = new RFIResponseResource(rFIResponseService, rFIResponseQueryService);
        this.restRFIResponseMockMvc = MockMvcBuilders.standaloneSetup(rFIResponseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RFIResponse createEntity(EntityManager em) {
        RFIResponse rFIResponse = new RFIResponse()
            .note(DEFAULT_NOTE)
            .username(DEFAULT_USERNAME)
            .submissionDate(DEFAULT_SUBMISSION_DATE);
        return rFIResponse;
    }

    @Before
    public void initTest() {
        rFIResponseSearchRepository.deleteAll();
        rFIResponse = createEntity(em);
    }

    @Test
    @Transactional
    public void createRFIResponse() throws Exception {
        int databaseSizeBeforeCreate = rFIResponseRepository.findAll().size();

        // Create the RFIResponse
        restRFIResponseMockMvc.perform(post("/api/r-fi-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rFIResponse)))
            .andExpect(status().isCreated());

        // Validate the RFIResponse in the database
        List<RFIResponse> rFIResponseList = rFIResponseRepository.findAll();
        assertThat(rFIResponseList).hasSize(databaseSizeBeforeCreate + 1);
        RFIResponse testRFIResponse = rFIResponseList.get(rFIResponseList.size() - 1);
        assertThat(testRFIResponse.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testRFIResponse.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testRFIResponse.getSubmissionDate()).isEqualTo(DEFAULT_SUBMISSION_DATE);

        // Validate the RFIResponse in Elasticsearch
        RFIResponse rFIResponseEs = rFIResponseSearchRepository.findOne(testRFIResponse.getId());
        assertThat(rFIResponseEs).isEqualToComparingFieldByField(testRFIResponse);
    }

    @Test
    @Transactional
    public void createRFIResponseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = rFIResponseRepository.findAll().size();

        // Create the RFIResponse with an existing ID
        rFIResponse.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRFIResponseMockMvc.perform(post("/api/r-fi-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rFIResponse)))
            .andExpect(status().isBadRequest());

        // Validate the RFIResponse in the database
        List<RFIResponse> rFIResponseList = rFIResponseRepository.findAll();
        assertThat(rFIResponseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNoteIsRequired() throws Exception {
        int databaseSizeBeforeTest = rFIResponseRepository.findAll().size();
        // set the field null
        rFIResponse.setNote(null);

        // Create the RFIResponse, which fails.

        restRFIResponseMockMvc.perform(post("/api/r-fi-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rFIResponse)))
            .andExpect(status().isBadRequest());

        List<RFIResponse> rFIResponseList = rFIResponseRepository.findAll();
        assertThat(rFIResponseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUsernameIsRequired() throws Exception {
        int databaseSizeBeforeTest = rFIResponseRepository.findAll().size();
        // set the field null
        rFIResponse.setUsername(null);

        // Create the RFIResponse, which fails.

        restRFIResponseMockMvc.perform(post("/api/r-fi-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rFIResponse)))
            .andExpect(status().isBadRequest());

        List<RFIResponse> rFIResponseList = rFIResponseRepository.findAll();
        assertThat(rFIResponseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSubmissionDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = rFIResponseRepository.findAll().size();
        // set the field null
        rFIResponse.setSubmissionDate(null);

        // Create the RFIResponse, which fails.

        restRFIResponseMockMvc.perform(post("/api/r-fi-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rFIResponse)))
            .andExpect(status().isBadRequest());

        List<RFIResponse> rFIResponseList = rFIResponseRepository.findAll();
        assertThat(rFIResponseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRFIResponses() throws Exception {
        // Initialize the database
        rFIResponseRepository.saveAndFlush(rFIResponse);

        // Get all the rFIResponseList
        restRFIResponseMockMvc.perform(get("/api/r-fi-responses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(rFIResponse.getId().intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].submissionDate").value(hasItem(sameInstant(DEFAULT_SUBMISSION_DATE))));
    }

    @Test
    @Transactional
    public void getRFIResponse() throws Exception {
        // Initialize the database
        rFIResponseRepository.saveAndFlush(rFIResponse);

        // Get the rFIResponse
        restRFIResponseMockMvc.perform(get("/api/r-fi-responses/{id}", rFIResponse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(rFIResponse.getId().intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()))
            .andExpect(jsonPath("$.submissionDate").value(sameInstant(DEFAULT_SUBMISSION_DATE)));
    }

    @Test
    @Transactional
    public void getAllRFIResponsesByNoteIsEqualToSomething() throws Exception {
        // Initialize the database
        rFIResponseRepository.saveAndFlush(rFIResponse);

        // Get all the rFIResponseList where note equals to DEFAULT_NOTE
        defaultRFIResponseShouldBeFound("note.equals=" + DEFAULT_NOTE);

        // Get all the rFIResponseList where note equals to UPDATED_NOTE
        defaultRFIResponseShouldNotBeFound("note.equals=" + UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void getAllRFIResponsesByNoteIsInShouldWork() throws Exception {
        // Initialize the database
        rFIResponseRepository.saveAndFlush(rFIResponse);

        // Get all the rFIResponseList where note in DEFAULT_NOTE or UPDATED_NOTE
        defaultRFIResponseShouldBeFound("note.in=" + DEFAULT_NOTE + "," + UPDATED_NOTE);

        // Get all the rFIResponseList where note equals to UPDATED_NOTE
        defaultRFIResponseShouldNotBeFound("note.in=" + UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void getAllRFIResponsesByNoteIsNullOrNotNull() throws Exception {
        // Initialize the database
        rFIResponseRepository.saveAndFlush(rFIResponse);

        // Get all the rFIResponseList where note is not null
        defaultRFIResponseShouldBeFound("note.specified=true");

        // Get all the rFIResponseList where note is null
        defaultRFIResponseShouldNotBeFound("note.specified=false");
    }

    @Test
    @Transactional
    public void getAllRFIResponsesByUsernameIsEqualToSomething() throws Exception {
        // Initialize the database
        rFIResponseRepository.saveAndFlush(rFIResponse);

        // Get all the rFIResponseList where username equals to DEFAULT_USERNAME
        defaultRFIResponseShouldBeFound("username.equals=" + DEFAULT_USERNAME);

        // Get all the rFIResponseList where username equals to UPDATED_USERNAME
        defaultRFIResponseShouldNotBeFound("username.equals=" + UPDATED_USERNAME);
    }

    @Test
    @Transactional
    public void getAllRFIResponsesByUsernameIsInShouldWork() throws Exception {
        // Initialize the database
        rFIResponseRepository.saveAndFlush(rFIResponse);

        // Get all the rFIResponseList where username in DEFAULT_USERNAME or UPDATED_USERNAME
        defaultRFIResponseShouldBeFound("username.in=" + DEFAULT_USERNAME + "," + UPDATED_USERNAME);

        // Get all the rFIResponseList where username equals to UPDATED_USERNAME
        defaultRFIResponseShouldNotBeFound("username.in=" + UPDATED_USERNAME);
    }

    @Test
    @Transactional
    public void getAllRFIResponsesByUsernameIsNullOrNotNull() throws Exception {
        // Initialize the database
        rFIResponseRepository.saveAndFlush(rFIResponse);

        // Get all the rFIResponseList where username is not null
        defaultRFIResponseShouldBeFound("username.specified=true");

        // Get all the rFIResponseList where username is null
        defaultRFIResponseShouldNotBeFound("username.specified=false");
    }

    @Test
    @Transactional
    public void getAllRFIResponsesBySubmissionDateIsEqualToSomething() throws Exception {
        // Initialize the database
        rFIResponseRepository.saveAndFlush(rFIResponse);

        // Get all the rFIResponseList where submissionDate equals to DEFAULT_SUBMISSION_DATE
        defaultRFIResponseShouldBeFound("submissionDate.equals=" + DEFAULT_SUBMISSION_DATE);

        // Get all the rFIResponseList where submissionDate equals to UPDATED_SUBMISSION_DATE
        defaultRFIResponseShouldNotBeFound("submissionDate.equals=" + UPDATED_SUBMISSION_DATE);
    }

    @Test
    @Transactional
    public void getAllRFIResponsesBySubmissionDateIsInShouldWork() throws Exception {
        // Initialize the database
        rFIResponseRepository.saveAndFlush(rFIResponse);

        // Get all the rFIResponseList where submissionDate in DEFAULT_SUBMISSION_DATE or UPDATED_SUBMISSION_DATE
        defaultRFIResponseShouldBeFound("submissionDate.in=" + DEFAULT_SUBMISSION_DATE + "," + UPDATED_SUBMISSION_DATE);

        // Get all the rFIResponseList where submissionDate equals to UPDATED_SUBMISSION_DATE
        defaultRFIResponseShouldNotBeFound("submissionDate.in=" + UPDATED_SUBMISSION_DATE);
    }

    @Test
    @Transactional
    public void getAllRFIResponsesBySubmissionDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        rFIResponseRepository.saveAndFlush(rFIResponse);

        // Get all the rFIResponseList where submissionDate is not null
        defaultRFIResponseShouldBeFound("submissionDate.specified=true");

        // Get all the rFIResponseList where submissionDate is null
        defaultRFIResponseShouldNotBeFound("submissionDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllRFIResponsesBySubmissionDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        rFIResponseRepository.saveAndFlush(rFIResponse);

        // Get all the rFIResponseList where submissionDate greater than or equals to DEFAULT_SUBMISSION_DATE
        defaultRFIResponseShouldBeFound("submissionDate.greaterOrEqualThan=" + DEFAULT_SUBMISSION_DATE);

        // Get all the rFIResponseList where submissionDate greater than or equals to UPDATED_SUBMISSION_DATE
        defaultRFIResponseShouldNotBeFound("submissionDate.greaterOrEqualThan=" + UPDATED_SUBMISSION_DATE);
    }

    @Test
    @Transactional
    public void getAllRFIResponsesBySubmissionDateIsLessThanSomething() throws Exception {
        // Initialize the database
        rFIResponseRepository.saveAndFlush(rFIResponse);

        // Get all the rFIResponseList where submissionDate less than or equals to DEFAULT_SUBMISSION_DATE
        defaultRFIResponseShouldNotBeFound("submissionDate.lessThan=" + DEFAULT_SUBMISSION_DATE);

        // Get all the rFIResponseList where submissionDate less than or equals to UPDATED_SUBMISSION_DATE
        defaultRFIResponseShouldBeFound("submissionDate.lessThan=" + UPDATED_SUBMISSION_DATE);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultRFIResponseShouldBeFound(String filter) throws Exception {
        restRFIResponseMockMvc.perform(get("/api/r-fi-responses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(rFIResponse.getId().intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].submissionDate").value(hasItem(sameInstant(DEFAULT_SUBMISSION_DATE))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultRFIResponseShouldNotBeFound(String filter) throws Exception {
        restRFIResponseMockMvc.perform(get("/api/r-fi-responses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingRFIResponse() throws Exception {
        // Get the rFIResponse
        restRFIResponseMockMvc.perform(get("/api/r-fi-responses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRFIResponse() throws Exception {
        // Initialize the database
        rFIResponseService.save(rFIResponse);

        int databaseSizeBeforeUpdate = rFIResponseRepository.findAll().size();

        // Update the rFIResponse
        RFIResponse updatedRFIResponse = rFIResponseRepository.findOne(rFIResponse.getId());
        updatedRFIResponse
            .note(UPDATED_NOTE)
            .username(UPDATED_USERNAME)
            .submissionDate(UPDATED_SUBMISSION_DATE);

        restRFIResponseMockMvc.perform(put("/api/r-fi-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRFIResponse)))
            .andExpect(status().isOk());

        // Validate the RFIResponse in the database
        List<RFIResponse> rFIResponseList = rFIResponseRepository.findAll();
        assertThat(rFIResponseList).hasSize(databaseSizeBeforeUpdate);
        RFIResponse testRFIResponse = rFIResponseList.get(rFIResponseList.size() - 1);
        assertThat(testRFIResponse.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testRFIResponse.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testRFIResponse.getSubmissionDate()).isEqualTo(UPDATED_SUBMISSION_DATE);

        // Validate the RFIResponse in Elasticsearch
        RFIResponse rFIResponseEs = rFIResponseSearchRepository.findOne(testRFIResponse.getId());
        assertThat(rFIResponseEs).isEqualToComparingFieldByField(testRFIResponse);
    }

    @Test
    @Transactional
    public void updateNonExistingRFIResponse() throws Exception {
        int databaseSizeBeforeUpdate = rFIResponseRepository.findAll().size();

        // Create the RFIResponse

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRFIResponseMockMvc.perform(put("/api/r-fi-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rFIResponse)))
            .andExpect(status().isCreated());

        // Validate the RFIResponse in the database
        List<RFIResponse> rFIResponseList = rFIResponseRepository.findAll();
        assertThat(rFIResponseList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRFIResponse() throws Exception {
        // Initialize the database
        rFIResponseService.save(rFIResponse);

        int databaseSizeBeforeDelete = rFIResponseRepository.findAll().size();

        // Get the rFIResponse
        restRFIResponseMockMvc.perform(delete("/api/r-fi-responses/{id}", rFIResponse.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean rFIResponseExistsInEs = rFIResponseSearchRepository.exists(rFIResponse.getId());
        assertThat(rFIResponseExistsInEs).isFalse();

        // Validate the database is empty
        List<RFIResponse> rFIResponseList = rFIResponseRepository.findAll();
        assertThat(rFIResponseList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchRFIResponse() throws Exception {
        // Initialize the database
        rFIResponseService.save(rFIResponse);

        // Search the rFIResponse
        restRFIResponseMockMvc.perform(get("/api/_search/r-fi-responses?query=id:" + rFIResponse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(rFIResponse.getId().intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].submissionDate").value(hasItem(sameInstant(DEFAULT_SUBMISSION_DATE))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RFIResponse.class);
        RFIResponse rFIResponse1 = new RFIResponse();
        rFIResponse1.setId(1L);
        RFIResponse rFIResponse2 = new RFIResponse();
        rFIResponse2.setId(rFIResponse1.getId());
        assertThat(rFIResponse1).isEqualTo(rFIResponse2);
        rFIResponse2.setId(2L);
        assertThat(rFIResponse1).isNotEqualTo(rFIResponse2);
        rFIResponse1.setId(null);
        assertThat(rFIResponse1).isNotEqualTo(rFIResponse2);
    }
}
