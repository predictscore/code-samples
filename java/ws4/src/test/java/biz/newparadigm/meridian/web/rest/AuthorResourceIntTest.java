package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.Author;
import biz.newparadigm.meridian.domain.Location;
import biz.newparadigm.meridian.domain.AuthorProfile;
import biz.newparadigm.meridian.domain.AuthorName;
import biz.newparadigm.meridian.domain.AuthorComment;
import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.repository.AuthorRepository;
import biz.newparadigm.meridian.service.AuthorService;
import biz.newparadigm.meridian.repository.search.AuthorSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.AuthorCriteria;
import biz.newparadigm.meridian.service.AuthorQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AuthorResource REST controller.
 *
 * @see AuthorResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class AuthorResourceIntTest {

    private static final String DEFAULT_ORIGINAL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ENGLISH_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ENGLISH_NAME = "BBBBBBBBBB";

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private AuthorSearchRepository authorSearchRepository;

    @Autowired
    private AuthorQueryService authorQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAuthorMockMvc;

    private Author author;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AuthorResource authorResource = new AuthorResource(authorService, authorQueryService);
        this.restAuthorMockMvc = MockMvcBuilders.standaloneSetup(authorResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Author createEntity(EntityManager em) {
        Author author = new Author()
            .originalName(DEFAULT_ORIGINAL_NAME)
            .englishName(DEFAULT_ENGLISH_NAME);
        // Add required entity
        Location location = LocationResourceIntTest.createEntity(em);
        em.persist(location);
        em.flush();
        author.setLocation(location);
        return author;
    }

    @Before
    public void initTest() {
        authorSearchRepository.deleteAll();
        author = createEntity(em);
    }

    @Test
    @Transactional
    public void createAuthor() throws Exception {
        int databaseSizeBeforeCreate = authorRepository.findAll().size();

        // Create the Author
        restAuthorMockMvc.perform(post("/api/authors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(author)))
            .andExpect(status().isCreated());

        // Validate the Author in the database
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeCreate + 1);
        Author testAuthor = authorList.get(authorList.size() - 1);
        assertThat(testAuthor.getOriginalName()).isEqualTo(DEFAULT_ORIGINAL_NAME);
        assertThat(testAuthor.getEnglishName()).isEqualTo(DEFAULT_ENGLISH_NAME);

        // Validate the Author in Elasticsearch
        Author authorEs = authorSearchRepository.findOne(testAuthor.getId());
        assertThat(authorEs).isEqualToComparingFieldByField(testAuthor);
    }

    @Test
    @Transactional
    public void createAuthorWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = authorRepository.findAll().size();

        // Create the Author with an existing ID
        author.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAuthorMockMvc.perform(post("/api/authors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(author)))
            .andExpect(status().isBadRequest());

        // Validate the Author in the database
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkOriginalNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorRepository.findAll().size();
        // set the field null
        author.setOriginalName(null);

        // Create the Author, which fails.

        restAuthorMockMvc.perform(post("/api/authors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(author)))
            .andExpect(status().isBadRequest());

        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEnglishNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorRepository.findAll().size();
        // set the field null
        author.setEnglishName(null);

        // Create the Author, which fails.

        restAuthorMockMvc.perform(post("/api/authors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(author)))
            .andExpect(status().isBadRequest());

        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAuthors() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList
        restAuthorMockMvc.perform(get("/api/authors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(author.getId().intValue())))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].englishName").value(hasItem(DEFAULT_ENGLISH_NAME.toString())));
    }

    @Test
    @Transactional
    public void getAuthor() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get the author
        restAuthorMockMvc.perform(get("/api/authors/{id}", author.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(author.getId().intValue()))
            .andExpect(jsonPath("$.originalName").value(DEFAULT_ORIGINAL_NAME.toString()))
            .andExpect(jsonPath("$.englishName").value(DEFAULT_ENGLISH_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllAuthorsByOriginalNameIsEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where originalName equals to DEFAULT_ORIGINAL_NAME
        defaultAuthorShouldBeFound("originalName.equals=" + DEFAULT_ORIGINAL_NAME);

        // Get all the authorList where originalName equals to UPDATED_ORIGINAL_NAME
        defaultAuthorShouldNotBeFound("originalName.equals=" + UPDATED_ORIGINAL_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorsByOriginalNameIsInShouldWork() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where originalName in DEFAULT_ORIGINAL_NAME or UPDATED_ORIGINAL_NAME
        defaultAuthorShouldBeFound("originalName.in=" + DEFAULT_ORIGINAL_NAME + "," + UPDATED_ORIGINAL_NAME);

        // Get all the authorList where originalName equals to UPDATED_ORIGINAL_NAME
        defaultAuthorShouldNotBeFound("originalName.in=" + UPDATED_ORIGINAL_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorsByOriginalNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where originalName is not null
        defaultAuthorShouldBeFound("originalName.specified=true");

        // Get all the authorList where originalName is null
        defaultAuthorShouldNotBeFound("originalName.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorsByEnglishNameIsEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where englishName equals to DEFAULT_ENGLISH_NAME
        defaultAuthorShouldBeFound("englishName.equals=" + DEFAULT_ENGLISH_NAME);

        // Get all the authorList where englishName equals to UPDATED_ENGLISH_NAME
        defaultAuthorShouldNotBeFound("englishName.equals=" + UPDATED_ENGLISH_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorsByEnglishNameIsInShouldWork() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where englishName in DEFAULT_ENGLISH_NAME or UPDATED_ENGLISH_NAME
        defaultAuthorShouldBeFound("englishName.in=" + DEFAULT_ENGLISH_NAME + "," + UPDATED_ENGLISH_NAME);

        // Get all the authorList where englishName equals to UPDATED_ENGLISH_NAME
        defaultAuthorShouldNotBeFound("englishName.in=" + UPDATED_ENGLISH_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorsByEnglishNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where englishName is not null
        defaultAuthorShouldBeFound("englishName.specified=true");

        // Get all the authorList where englishName is null
        defaultAuthorShouldNotBeFound("englishName.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorsByLocationIsEqualToSomething() throws Exception {
        // Initialize the database
        Location location = LocationResourceIntTest.createEntity(em);
        em.persist(location);
        em.flush();
        author.setLocation(location);
        authorRepository.saveAndFlush(author);
        Long locationId = location.getId();

        // Get all the authorList where location equals to locationId
        defaultAuthorShouldBeFound("locationId.equals=" + locationId);

        // Get all the authorList where location equals to locationId + 1
        defaultAuthorShouldNotBeFound("locationId.equals=" + (locationId + 1));
    }



    @Test
    @Transactional
    public void getAllAuthorsByNamesIsEqualToSomething() throws Exception {
        // Initialize the database
        AuthorName names = AuthorNameResourceIntTest.createEntity(em);
        em.persist(names);
        em.flush();
        author.addNames(names);
        authorRepository.saveAndFlush(author);
        Long namesId = names.getId();

        // Get all the authorList where names equals to namesId
        defaultAuthorShouldBeFound("namesId.equals=" + namesId);

        // Get all the authorList where names equals to namesId + 1
        defaultAuthorShouldNotBeFound("namesId.equals=" + (namesId + 1));
    }


    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultAuthorShouldBeFound(String filter) throws Exception {
        restAuthorMockMvc.perform(get("/api/authors?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(author.getId().intValue())))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].englishName").value(hasItem(DEFAULT_ENGLISH_NAME.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultAuthorShouldNotBeFound(String filter) throws Exception {
        restAuthorMockMvc.perform(get("/api/authors?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingAuthor() throws Exception {
        // Get the author
        restAuthorMockMvc.perform(get("/api/authors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAuthor() throws Exception {
        // Initialize the database
        authorService.save(author);

        int databaseSizeBeforeUpdate = authorRepository.findAll().size();

        // Update the author
        Author updatedAuthor = authorRepository.findOne(author.getId());
        updatedAuthor
            .originalName(UPDATED_ORIGINAL_NAME)
            .englishName(UPDATED_ENGLISH_NAME);

        restAuthorMockMvc.perform(put("/api/authors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAuthor)))
            .andExpect(status().isOk());

        // Validate the Author in the database
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeUpdate);
        Author testAuthor = authorList.get(authorList.size() - 1);
        assertThat(testAuthor.getOriginalName()).isEqualTo(UPDATED_ORIGINAL_NAME);
        assertThat(testAuthor.getEnglishName()).isEqualTo(UPDATED_ENGLISH_NAME);

        // Validate the Author in Elasticsearch
        Author authorEs = authorSearchRepository.findOne(testAuthor.getId());
        assertThat(authorEs).isEqualToComparingFieldByField(testAuthor);
    }

    @Test
    @Transactional
    public void updateNonExistingAuthor() throws Exception {
        int databaseSizeBeforeUpdate = authorRepository.findAll().size();

        // Create the Author

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAuthorMockMvc.perform(put("/api/authors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(author)))
            .andExpect(status().isCreated());

        // Validate the Author in the database
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAuthor() throws Exception {
        // Initialize the database
        authorService.save(author);

        int databaseSizeBeforeDelete = authorRepository.findAll().size();

        // Get the author
        restAuthorMockMvc.perform(delete("/api/authors/{id}", author.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean authorExistsInEs = authorSearchRepository.exists(author.getId());
        assertThat(authorExistsInEs).isFalse();

        // Validate the database is empty
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAuthor() throws Exception {
        // Initialize the database
        authorService.save(author);

        // Search the author
        restAuthorMockMvc.perform(get("/api/_search/authors?query=id:" + author.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(author.getId().intValue())))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].englishName").value(hasItem(DEFAULT_ENGLISH_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Author.class);
        Author author1 = new Author();
        author1.setId(1L);
        Author author2 = new Author();
        author2.setId(author1.getId());
        assertThat(author1).isEqualTo(author2);
        author2.setId(2L);
        assertThat(author1).isNotEqualTo(author2);
        author1.setId(null);
        assertThat(author1).isNotEqualTo(author2);
    }
}
