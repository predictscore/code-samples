package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsToLanguage;
import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.domain.Language;
import biz.newparadigm.meridian.repository.NewsToLanguageRepository;
import biz.newparadigm.meridian.service.NewsToLanguageService;
import biz.newparadigm.meridian.repository.search.NewsToLanguageSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsToLanguageCriteria;
import biz.newparadigm.meridian.service.NewsToLanguageQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsToLanguageResource REST controller.
 *
 * @see NewsToLanguageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsToLanguageResourceIntTest {

    @Autowired
    private NewsToLanguageRepository newsToLanguageRepository;

    @Autowired
    private NewsToLanguageService newsToLanguageService;

    @Autowired
    private NewsToLanguageSearchRepository newsToLanguageSearchRepository;

    @Autowired
    private NewsToLanguageQueryService newsToLanguageQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsToLanguageMockMvc;

    private NewsToLanguage newsToLanguage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsToLanguageResource newsToLanguageResource = new NewsToLanguageResource(newsToLanguageService, newsToLanguageQueryService);
        this.restNewsToLanguageMockMvc = MockMvcBuilders.standaloneSetup(newsToLanguageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsToLanguage createEntity(EntityManager em) {
        NewsToLanguage newsToLanguage = new NewsToLanguage();
        // Add required entity
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsToLanguage.setNews(news);
        // Add required entity
        Language language = LanguageResourceIntTest.createEntity(em);
        em.persist(language);
        em.flush();
        newsToLanguage.setLanguage(language);
        return newsToLanguage;
    }

    @Before
    public void initTest() {
        newsToLanguageSearchRepository.deleteAll();
        newsToLanguage = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsToLanguage() throws Exception {
        int databaseSizeBeforeCreate = newsToLanguageRepository.findAll().size();

        // Create the NewsToLanguage
        restNewsToLanguageMockMvc.perform(post("/api/news-to-languages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsToLanguage)))
            .andExpect(status().isCreated());

        // Validate the NewsToLanguage in the database
        List<NewsToLanguage> newsToLanguageList = newsToLanguageRepository.findAll();
        assertThat(newsToLanguageList).hasSize(databaseSizeBeforeCreate + 1);
        NewsToLanguage testNewsToLanguage = newsToLanguageList.get(newsToLanguageList.size() - 1);

        // Validate the NewsToLanguage in Elasticsearch
        NewsToLanguage newsToLanguageEs = newsToLanguageSearchRepository.findOne(testNewsToLanguage.getId());
        assertThat(newsToLanguageEs).isEqualToComparingFieldByField(testNewsToLanguage);
    }

    @Test
    @Transactional
    public void createNewsToLanguageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsToLanguageRepository.findAll().size();

        // Create the NewsToLanguage with an existing ID
        newsToLanguage.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsToLanguageMockMvc.perform(post("/api/news-to-languages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsToLanguage)))
            .andExpect(status().isBadRequest());

        // Validate the NewsToLanguage in the database
        List<NewsToLanguage> newsToLanguageList = newsToLanguageRepository.findAll();
        assertThat(newsToLanguageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllNewsToLanguages() throws Exception {
        // Initialize the database
        newsToLanguageRepository.saveAndFlush(newsToLanguage);

        // Get all the newsToLanguageList
        restNewsToLanguageMockMvc.perform(get("/api/news-to-languages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsToLanguage.getId().intValue())));
    }

    @Test
    @Transactional
    public void getNewsToLanguage() throws Exception {
        // Initialize the database
        newsToLanguageRepository.saveAndFlush(newsToLanguage);

        // Get the newsToLanguage
        restNewsToLanguageMockMvc.perform(get("/api/news-to-languages/{id}", newsToLanguage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsToLanguage.getId().intValue()));
    }

    @Test
    @Transactional
    public void getAllNewsToLanguagesByNewsIsEqualToSomething() throws Exception {
        // Initialize the database
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsToLanguage.setNews(news);
        newsToLanguageRepository.saveAndFlush(newsToLanguage);
        Long newsId = news.getId();

        // Get all the newsToLanguageList where news equals to newsId
        defaultNewsToLanguageShouldBeFound("newsId.equals=" + newsId);

        // Get all the newsToLanguageList where news equals to newsId + 1
        defaultNewsToLanguageShouldNotBeFound("newsId.equals=" + (newsId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsToLanguagesByLanguageIsEqualToSomething() throws Exception {
        // Initialize the database
        Language language = LanguageResourceIntTest.createEntity(em);
        em.persist(language);
        em.flush();
        newsToLanguage.setLanguage(language);
        newsToLanguageRepository.saveAndFlush(newsToLanguage);
        Long languageId = language.getId();

        // Get all the newsToLanguageList where language equals to languageId
        defaultNewsToLanguageShouldBeFound("languageId.equals=" + languageId);

        // Get all the newsToLanguageList where language equals to languageId + 1
        defaultNewsToLanguageShouldNotBeFound("languageId.equals=" + (languageId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsToLanguageShouldBeFound(String filter) throws Exception {
        restNewsToLanguageMockMvc.perform(get("/api/news-to-languages?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsToLanguage.getId().intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsToLanguageShouldNotBeFound(String filter) throws Exception {
        restNewsToLanguageMockMvc.perform(get("/api/news-to-languages?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsToLanguage() throws Exception {
        // Get the newsToLanguage
        restNewsToLanguageMockMvc.perform(get("/api/news-to-languages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsToLanguage() throws Exception {
        // Initialize the database
        newsToLanguageService.save(newsToLanguage);

        int databaseSizeBeforeUpdate = newsToLanguageRepository.findAll().size();

        // Update the newsToLanguage
        NewsToLanguage updatedNewsToLanguage = newsToLanguageRepository.findOne(newsToLanguage.getId());

        restNewsToLanguageMockMvc.perform(put("/api/news-to-languages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsToLanguage)))
            .andExpect(status().isOk());

        // Validate the NewsToLanguage in the database
        List<NewsToLanguage> newsToLanguageList = newsToLanguageRepository.findAll();
        assertThat(newsToLanguageList).hasSize(databaseSizeBeforeUpdate);
        NewsToLanguage testNewsToLanguage = newsToLanguageList.get(newsToLanguageList.size() - 1);

        // Validate the NewsToLanguage in Elasticsearch
        NewsToLanguage newsToLanguageEs = newsToLanguageSearchRepository.findOne(testNewsToLanguage.getId());
        assertThat(newsToLanguageEs).isEqualToComparingFieldByField(testNewsToLanguage);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsToLanguage() throws Exception {
        int databaseSizeBeforeUpdate = newsToLanguageRepository.findAll().size();

        // Create the NewsToLanguage

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsToLanguageMockMvc.perform(put("/api/news-to-languages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsToLanguage)))
            .andExpect(status().isCreated());

        // Validate the NewsToLanguage in the database
        List<NewsToLanguage> newsToLanguageList = newsToLanguageRepository.findAll();
        assertThat(newsToLanguageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsToLanguage() throws Exception {
        // Initialize the database
        newsToLanguageService.save(newsToLanguage);

        int databaseSizeBeforeDelete = newsToLanguageRepository.findAll().size();

        // Get the newsToLanguage
        restNewsToLanguageMockMvc.perform(delete("/api/news-to-languages/{id}", newsToLanguage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsToLanguageExistsInEs = newsToLanguageSearchRepository.exists(newsToLanguage.getId());
        assertThat(newsToLanguageExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsToLanguage> newsToLanguageList = newsToLanguageRepository.findAll();
        assertThat(newsToLanguageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsToLanguage() throws Exception {
        // Initialize the database
        newsToLanguageService.save(newsToLanguage);

        // Search the newsToLanguage
        restNewsToLanguageMockMvc.perform(get("/api/_search/news-to-languages?query=id:" + newsToLanguage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsToLanguage.getId().intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsToLanguage.class);
        NewsToLanguage newsToLanguage1 = new NewsToLanguage();
        newsToLanguage1.setId(1L);
        NewsToLanguage newsToLanguage2 = new NewsToLanguage();
        newsToLanguage2.setId(newsToLanguage1.getId());
        assertThat(newsToLanguage1).isEqualTo(newsToLanguage2);
        newsToLanguage2.setId(2L);
        assertThat(newsToLanguage1).isNotEqualTo(newsToLanguage2);
        newsToLanguage1.setId(null);
        assertThat(newsToLanguage1).isNotEqualTo(newsToLanguage2);
    }
}
