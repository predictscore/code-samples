package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsSummaryMetadata;
import biz.newparadigm.meridian.domain.NewsSummary;
import biz.newparadigm.meridian.domain.Metagroup;
import biz.newparadigm.meridian.domain.Metafield;
import biz.newparadigm.meridian.repository.NewsSummaryMetadataRepository;
import biz.newparadigm.meridian.service.NewsSummaryMetadataService;
import biz.newparadigm.meridian.repository.search.NewsSummaryMetadataSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsSummaryMetadataCriteria;
import biz.newparadigm.meridian.service.NewsSummaryMetadataQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.sameInstant;
import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsSummaryMetadataResource REST controller.
 *
 * @see NewsSummaryMetadataResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsSummaryMetadataResourceIntTest {

    private static final String DEFAULT_VALUE_STRING = "AAAAAAAAAA";
    private static final String UPDATED_VALUE_STRING = "BBBBBBBBBB";

    private static final Float DEFAULT_VALUE_NUMBER = 1F;
    private static final Float UPDATED_VALUE_NUMBER = 2F;

    private static final ZonedDateTime DEFAULT_VALUE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_VALUE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private NewsSummaryMetadataRepository newsSummaryMetadataRepository;

    @Autowired
    private NewsSummaryMetadataService newsSummaryMetadataService;

    @Autowired
    private NewsSummaryMetadataSearchRepository newsSummaryMetadataSearchRepository;

    @Autowired
    private NewsSummaryMetadataQueryService newsSummaryMetadataQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsSummaryMetadataMockMvc;

    private NewsSummaryMetadata newsSummaryMetadata;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsSummaryMetadataResource newsSummaryMetadataResource = new NewsSummaryMetadataResource(newsSummaryMetadataService, newsSummaryMetadataQueryService);
        this.restNewsSummaryMetadataMockMvc = MockMvcBuilders.standaloneSetup(newsSummaryMetadataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsSummaryMetadata createEntity(EntityManager em) {
        NewsSummaryMetadata newsSummaryMetadata = new NewsSummaryMetadata()
            .valueString(DEFAULT_VALUE_STRING)
            .valueNumber(DEFAULT_VALUE_NUMBER)
            .valueDate(DEFAULT_VALUE_DATE);
        // Add required entity
        NewsSummary newsSummary = NewsSummaryResourceIntTest.createEntity(em);
        em.persist(newsSummary);
        em.flush();
        newsSummaryMetadata.setNewsSummary(newsSummary);
        // Add required entity
        Metagroup metagroup = MetagroupResourceIntTest.createEntity(em);
        em.persist(metagroup);
        em.flush();
        newsSummaryMetadata.setMetagroup(metagroup);
        // Add required entity
        Metafield metafield = MetafieldResourceIntTest.createEntity(em);
        em.persist(metafield);
        em.flush();
        newsSummaryMetadata.setMetafield(metafield);
        return newsSummaryMetadata;
    }

    @Before
    public void initTest() {
        newsSummaryMetadataSearchRepository.deleteAll();
        newsSummaryMetadata = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsSummaryMetadata() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryMetadataRepository.findAll().size();

        // Create the NewsSummaryMetadata
        restNewsSummaryMetadataMockMvc.perform(post("/api/news-summary-metadata")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryMetadata)))
            .andExpect(status().isCreated());

        // Validate the NewsSummaryMetadata in the database
        List<NewsSummaryMetadata> newsSummaryMetadataList = newsSummaryMetadataRepository.findAll();
        assertThat(newsSummaryMetadataList).hasSize(databaseSizeBeforeCreate + 1);
        NewsSummaryMetadata testNewsSummaryMetadata = newsSummaryMetadataList.get(newsSummaryMetadataList.size() - 1);
        assertThat(testNewsSummaryMetadata.getValueString()).isEqualTo(DEFAULT_VALUE_STRING);
        assertThat(testNewsSummaryMetadata.getValueNumber()).isEqualTo(DEFAULT_VALUE_NUMBER);
        assertThat(testNewsSummaryMetadata.getValueDate()).isEqualTo(DEFAULT_VALUE_DATE);

        // Validate the NewsSummaryMetadata in Elasticsearch
        NewsSummaryMetadata newsSummaryMetadataEs = newsSummaryMetadataSearchRepository.findOne(testNewsSummaryMetadata.getId());
        assertThat(newsSummaryMetadataEs).isEqualToComparingFieldByField(testNewsSummaryMetadata);
    }

    @Test
    @Transactional
    public void createNewsSummaryMetadataWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryMetadataRepository.findAll().size();

        // Create the NewsSummaryMetadata with an existing ID
        newsSummaryMetadata.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsSummaryMetadataMockMvc.perform(post("/api/news-summary-metadata")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryMetadata)))
            .andExpect(status().isBadRequest());

        // Validate the NewsSummaryMetadata in the database
        List<NewsSummaryMetadata> newsSummaryMetadataList = newsSummaryMetadataRepository.findAll();
        assertThat(newsSummaryMetadataList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryMetadata() throws Exception {
        // Initialize the database
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);

        // Get all the newsSummaryMetadataList
        restNewsSummaryMetadataMockMvc.perform(get("/api/news-summary-metadata?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryMetadata.getId().intValue())))
            .andExpect(jsonPath("$.[*].valueString").value(hasItem(DEFAULT_VALUE_STRING.toString())))
            .andExpect(jsonPath("$.[*].valueNumber").value(hasItem(DEFAULT_VALUE_NUMBER.doubleValue())))
            .andExpect(jsonPath("$.[*].valueDate").value(hasItem(sameInstant(DEFAULT_VALUE_DATE))));
    }

    @Test
    @Transactional
    public void getNewsSummaryMetadata() throws Exception {
        // Initialize the database
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);

        // Get the newsSummaryMetadata
        restNewsSummaryMetadataMockMvc.perform(get("/api/news-summary-metadata/{id}", newsSummaryMetadata.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsSummaryMetadata.getId().intValue()))
            .andExpect(jsonPath("$.valueString").value(DEFAULT_VALUE_STRING.toString()))
            .andExpect(jsonPath("$.valueNumber").value(DEFAULT_VALUE_NUMBER.doubleValue()))
            .andExpect(jsonPath("$.valueDate").value(sameInstant(DEFAULT_VALUE_DATE)));
    }

    @Test
    @Transactional
    public void getAllNewsSummaryMetadataByValueStringIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);

        // Get all the newsSummaryMetadataList where valueString equals to DEFAULT_VALUE_STRING
        defaultNewsSummaryMetadataShouldBeFound("valueString.equals=" + DEFAULT_VALUE_STRING);

        // Get all the newsSummaryMetadataList where valueString equals to UPDATED_VALUE_STRING
        defaultNewsSummaryMetadataShouldNotBeFound("valueString.equals=" + UPDATED_VALUE_STRING);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryMetadataByValueStringIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);

        // Get all the newsSummaryMetadataList where valueString in DEFAULT_VALUE_STRING or UPDATED_VALUE_STRING
        defaultNewsSummaryMetadataShouldBeFound("valueString.in=" + DEFAULT_VALUE_STRING + "," + UPDATED_VALUE_STRING);

        // Get all the newsSummaryMetadataList where valueString equals to UPDATED_VALUE_STRING
        defaultNewsSummaryMetadataShouldNotBeFound("valueString.in=" + UPDATED_VALUE_STRING);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryMetadataByValueStringIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);

        // Get all the newsSummaryMetadataList where valueString is not null
        defaultNewsSummaryMetadataShouldBeFound("valueString.specified=true");

        // Get all the newsSummaryMetadataList where valueString is null
        defaultNewsSummaryMetadataShouldNotBeFound("valueString.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryMetadataByValueNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);

        // Get all the newsSummaryMetadataList where valueNumber equals to DEFAULT_VALUE_NUMBER
        defaultNewsSummaryMetadataShouldBeFound("valueNumber.equals=" + DEFAULT_VALUE_NUMBER);

        // Get all the newsSummaryMetadataList where valueNumber equals to UPDATED_VALUE_NUMBER
        defaultNewsSummaryMetadataShouldNotBeFound("valueNumber.equals=" + UPDATED_VALUE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryMetadataByValueNumberIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);

        // Get all the newsSummaryMetadataList where valueNumber in DEFAULT_VALUE_NUMBER or UPDATED_VALUE_NUMBER
        defaultNewsSummaryMetadataShouldBeFound("valueNumber.in=" + DEFAULT_VALUE_NUMBER + "," + UPDATED_VALUE_NUMBER);

        // Get all the newsSummaryMetadataList where valueNumber equals to UPDATED_VALUE_NUMBER
        defaultNewsSummaryMetadataShouldNotBeFound("valueNumber.in=" + UPDATED_VALUE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryMetadataByValueNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);

        // Get all the newsSummaryMetadataList where valueNumber is not null
        defaultNewsSummaryMetadataShouldBeFound("valueNumber.specified=true");

        // Get all the newsSummaryMetadataList where valueNumber is null
        defaultNewsSummaryMetadataShouldNotBeFound("valueNumber.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryMetadataByValueDateIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);

        // Get all the newsSummaryMetadataList where valueDate equals to DEFAULT_VALUE_DATE
        defaultNewsSummaryMetadataShouldBeFound("valueDate.equals=" + DEFAULT_VALUE_DATE);

        // Get all the newsSummaryMetadataList where valueDate equals to UPDATED_VALUE_DATE
        defaultNewsSummaryMetadataShouldNotBeFound("valueDate.equals=" + UPDATED_VALUE_DATE);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryMetadataByValueDateIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);

        // Get all the newsSummaryMetadataList where valueDate in DEFAULT_VALUE_DATE or UPDATED_VALUE_DATE
        defaultNewsSummaryMetadataShouldBeFound("valueDate.in=" + DEFAULT_VALUE_DATE + "," + UPDATED_VALUE_DATE);

        // Get all the newsSummaryMetadataList where valueDate equals to UPDATED_VALUE_DATE
        defaultNewsSummaryMetadataShouldNotBeFound("valueDate.in=" + UPDATED_VALUE_DATE);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryMetadataByValueDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);

        // Get all the newsSummaryMetadataList where valueDate is not null
        defaultNewsSummaryMetadataShouldBeFound("valueDate.specified=true");

        // Get all the newsSummaryMetadataList where valueDate is null
        defaultNewsSummaryMetadataShouldNotBeFound("valueDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryMetadataByValueDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);

        // Get all the newsSummaryMetadataList where valueDate greater than or equals to DEFAULT_VALUE_DATE
        defaultNewsSummaryMetadataShouldBeFound("valueDate.greaterOrEqualThan=" + DEFAULT_VALUE_DATE);

        // Get all the newsSummaryMetadataList where valueDate greater than or equals to UPDATED_VALUE_DATE
        defaultNewsSummaryMetadataShouldNotBeFound("valueDate.greaterOrEqualThan=" + UPDATED_VALUE_DATE);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryMetadataByValueDateIsLessThanSomething() throws Exception {
        // Initialize the database
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);

        // Get all the newsSummaryMetadataList where valueDate less than or equals to DEFAULT_VALUE_DATE
        defaultNewsSummaryMetadataShouldNotBeFound("valueDate.lessThan=" + DEFAULT_VALUE_DATE);

        // Get all the newsSummaryMetadataList where valueDate less than or equals to UPDATED_VALUE_DATE
        defaultNewsSummaryMetadataShouldBeFound("valueDate.lessThan=" + UPDATED_VALUE_DATE);
    }


    @Test
    @Transactional
    public void getAllNewsSummaryMetadataByNewsSummaryIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsSummary newsSummary = NewsSummaryResourceIntTest.createEntity(em);
        em.persist(newsSummary);
        em.flush();
        newsSummaryMetadata.setNewsSummary(newsSummary);
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);
        Long newsSummaryId = newsSummary.getId();

        // Get all the newsSummaryMetadataList where newsSummary equals to newsSummaryId
        defaultNewsSummaryMetadataShouldBeFound("newsSummaryId.equals=" + newsSummaryId);

        // Get all the newsSummaryMetadataList where newsSummary equals to newsSummaryId + 1
        defaultNewsSummaryMetadataShouldNotBeFound("newsSummaryId.equals=" + (newsSummaryId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsSummaryMetadataByMetagroupIsEqualToSomething() throws Exception {
        // Initialize the database
        Metagroup metagroup = MetagroupResourceIntTest.createEntity(em);
        em.persist(metagroup);
        em.flush();
        newsSummaryMetadata.setMetagroup(metagroup);
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);
        Long metagroupId = metagroup.getId();

        // Get all the newsSummaryMetadataList where metagroup equals to metagroupId
        defaultNewsSummaryMetadataShouldBeFound("metagroupId.equals=" + metagroupId);

        // Get all the newsSummaryMetadataList where metagroup equals to metagroupId + 1
        defaultNewsSummaryMetadataShouldNotBeFound("metagroupId.equals=" + (metagroupId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsSummaryMetadataByMetafieldIsEqualToSomething() throws Exception {
        // Initialize the database
        Metafield metafield = MetafieldResourceIntTest.createEntity(em);
        em.persist(metafield);
        em.flush();
        newsSummaryMetadata.setMetafield(metafield);
        newsSummaryMetadataRepository.saveAndFlush(newsSummaryMetadata);
        Long metafieldId = metafield.getId();

        // Get all the newsSummaryMetadataList where metafield equals to metafieldId
        defaultNewsSummaryMetadataShouldBeFound("metafieldId.equals=" + metafieldId);

        // Get all the newsSummaryMetadataList where metafield equals to metafieldId + 1
        defaultNewsSummaryMetadataShouldNotBeFound("metafieldId.equals=" + (metafieldId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsSummaryMetadataShouldBeFound(String filter) throws Exception {
        restNewsSummaryMetadataMockMvc.perform(get("/api/news-summary-metadata?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryMetadata.getId().intValue())))
            .andExpect(jsonPath("$.[*].valueString").value(hasItem(DEFAULT_VALUE_STRING.toString())))
            .andExpect(jsonPath("$.[*].valueNumber").value(hasItem(DEFAULT_VALUE_NUMBER.doubleValue())))
            .andExpect(jsonPath("$.[*].valueDate").value(hasItem(sameInstant(DEFAULT_VALUE_DATE))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsSummaryMetadataShouldNotBeFound(String filter) throws Exception {
        restNewsSummaryMetadataMockMvc.perform(get("/api/news-summary-metadata?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsSummaryMetadata() throws Exception {
        // Get the newsSummaryMetadata
        restNewsSummaryMetadataMockMvc.perform(get("/api/news-summary-metadata/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsSummaryMetadata() throws Exception {
        // Initialize the database
        newsSummaryMetadataService.save(newsSummaryMetadata);

        int databaseSizeBeforeUpdate = newsSummaryMetadataRepository.findAll().size();

        // Update the newsSummaryMetadata
        NewsSummaryMetadata updatedNewsSummaryMetadata = newsSummaryMetadataRepository.findOne(newsSummaryMetadata.getId());
        updatedNewsSummaryMetadata
            .valueString(UPDATED_VALUE_STRING)
            .valueNumber(UPDATED_VALUE_NUMBER)
            .valueDate(UPDATED_VALUE_DATE);

        restNewsSummaryMetadataMockMvc.perform(put("/api/news-summary-metadata")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsSummaryMetadata)))
            .andExpect(status().isOk());

        // Validate the NewsSummaryMetadata in the database
        List<NewsSummaryMetadata> newsSummaryMetadataList = newsSummaryMetadataRepository.findAll();
        assertThat(newsSummaryMetadataList).hasSize(databaseSizeBeforeUpdate);
        NewsSummaryMetadata testNewsSummaryMetadata = newsSummaryMetadataList.get(newsSummaryMetadataList.size() - 1);
        assertThat(testNewsSummaryMetadata.getValueString()).isEqualTo(UPDATED_VALUE_STRING);
        assertThat(testNewsSummaryMetadata.getValueNumber()).isEqualTo(UPDATED_VALUE_NUMBER);
        assertThat(testNewsSummaryMetadata.getValueDate()).isEqualTo(UPDATED_VALUE_DATE);

        // Validate the NewsSummaryMetadata in Elasticsearch
        NewsSummaryMetadata newsSummaryMetadataEs = newsSummaryMetadataSearchRepository.findOne(testNewsSummaryMetadata.getId());
        assertThat(newsSummaryMetadataEs).isEqualToComparingFieldByField(testNewsSummaryMetadata);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsSummaryMetadata() throws Exception {
        int databaseSizeBeforeUpdate = newsSummaryMetadataRepository.findAll().size();

        // Create the NewsSummaryMetadata

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsSummaryMetadataMockMvc.perform(put("/api/news-summary-metadata")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryMetadata)))
            .andExpect(status().isCreated());

        // Validate the NewsSummaryMetadata in the database
        List<NewsSummaryMetadata> newsSummaryMetadataList = newsSummaryMetadataRepository.findAll();
        assertThat(newsSummaryMetadataList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsSummaryMetadata() throws Exception {
        // Initialize the database
        newsSummaryMetadataService.save(newsSummaryMetadata);

        int databaseSizeBeforeDelete = newsSummaryMetadataRepository.findAll().size();

        // Get the newsSummaryMetadata
        restNewsSummaryMetadataMockMvc.perform(delete("/api/news-summary-metadata/{id}", newsSummaryMetadata.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsSummaryMetadataExistsInEs = newsSummaryMetadataSearchRepository.exists(newsSummaryMetadata.getId());
        assertThat(newsSummaryMetadataExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsSummaryMetadata> newsSummaryMetadataList = newsSummaryMetadataRepository.findAll();
        assertThat(newsSummaryMetadataList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsSummaryMetadata() throws Exception {
        // Initialize the database
        newsSummaryMetadataService.save(newsSummaryMetadata);

        // Search the newsSummaryMetadata
        restNewsSummaryMetadataMockMvc.perform(get("/api/_search/news-summary-metadata?query=id:" + newsSummaryMetadata.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryMetadata.getId().intValue())))
            .andExpect(jsonPath("$.[*].valueString").value(hasItem(DEFAULT_VALUE_STRING.toString())))
            .andExpect(jsonPath("$.[*].valueNumber").value(hasItem(DEFAULT_VALUE_NUMBER.doubleValue())))
            .andExpect(jsonPath("$.[*].valueDate").value(hasItem(sameInstant(DEFAULT_VALUE_DATE))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsSummaryMetadata.class);
        NewsSummaryMetadata newsSummaryMetadata1 = new NewsSummaryMetadata();
        newsSummaryMetadata1.setId(1L);
        NewsSummaryMetadata newsSummaryMetadata2 = new NewsSummaryMetadata();
        newsSummaryMetadata2.setId(newsSummaryMetadata1.getId());
        assertThat(newsSummaryMetadata1).isEqualTo(newsSummaryMetadata2);
        newsSummaryMetadata2.setId(2L);
        assertThat(newsSummaryMetadata1).isNotEqualTo(newsSummaryMetadata2);
        newsSummaryMetadata1.setId(null);
        assertThat(newsSummaryMetadata1).isNotEqualTo(newsSummaryMetadata2);
    }
}
