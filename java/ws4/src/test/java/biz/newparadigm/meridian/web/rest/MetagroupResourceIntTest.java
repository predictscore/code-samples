package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.Metagroup;
import biz.newparadigm.meridian.repository.MetagroupRepository;
import biz.newparadigm.meridian.service.MetagroupService;
import biz.newparadigm.meridian.repository.search.MetagroupSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.MetagroupCriteria;
import biz.newparadigm.meridian.service.MetagroupQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MetagroupResource REST controller.
 *
 * @see MetagroupResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class MetagroupResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private MetagroupRepository metagroupRepository;

    @Autowired
    private MetagroupService metagroupService;

    @Autowired
    private MetagroupSearchRepository metagroupSearchRepository;

    @Autowired
    private MetagroupQueryService metagroupQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMetagroupMockMvc;

    private Metagroup metagroup;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MetagroupResource metagroupResource = new MetagroupResource(metagroupService, metagroupQueryService);
        this.restMetagroupMockMvc = MockMvcBuilders.standaloneSetup(metagroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Metagroup createEntity(EntityManager em) {
        Metagroup metagroup = new Metagroup()
            .name(DEFAULT_NAME);
        return metagroup;
    }

    @Before
    public void initTest() {
        metagroupSearchRepository.deleteAll();
        metagroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createMetagroup() throws Exception {
        int databaseSizeBeforeCreate = metagroupRepository.findAll().size();

        // Create the Metagroup
        restMetagroupMockMvc.perform(post("/api/metagroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(metagroup)))
            .andExpect(status().isCreated());

        // Validate the Metagroup in the database
        List<Metagroup> metagroupList = metagroupRepository.findAll();
        assertThat(metagroupList).hasSize(databaseSizeBeforeCreate + 1);
        Metagroup testMetagroup = metagroupList.get(metagroupList.size() - 1);
        assertThat(testMetagroup.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the Metagroup in Elasticsearch
        Metagroup metagroupEs = metagroupSearchRepository.findOne(testMetagroup.getId());
        assertThat(metagroupEs).isEqualToComparingFieldByField(testMetagroup);
    }

    @Test
    @Transactional
    public void createMetagroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = metagroupRepository.findAll().size();

        // Create the Metagroup with an existing ID
        metagroup.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMetagroupMockMvc.perform(post("/api/metagroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(metagroup)))
            .andExpect(status().isBadRequest());

        // Validate the Metagroup in the database
        List<Metagroup> metagroupList = metagroupRepository.findAll();
        assertThat(metagroupList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = metagroupRepository.findAll().size();
        // set the field null
        metagroup.setName(null);

        // Create the Metagroup, which fails.

        restMetagroupMockMvc.perform(post("/api/metagroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(metagroup)))
            .andExpect(status().isBadRequest());

        List<Metagroup> metagroupList = metagroupRepository.findAll();
        assertThat(metagroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMetagroups() throws Exception {
        // Initialize the database
        metagroupRepository.saveAndFlush(metagroup);

        // Get all the metagroupList
        restMetagroupMockMvc.perform(get("/api/metagroups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(metagroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getMetagroup() throws Exception {
        // Initialize the database
        metagroupRepository.saveAndFlush(metagroup);

        // Get the metagroup
        restMetagroupMockMvc.perform(get("/api/metagroups/{id}", metagroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(metagroup.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllMetagroupsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        metagroupRepository.saveAndFlush(metagroup);

        // Get all the metagroupList where name equals to DEFAULT_NAME
        defaultMetagroupShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the metagroupList where name equals to UPDATED_NAME
        defaultMetagroupShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllMetagroupsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        metagroupRepository.saveAndFlush(metagroup);

        // Get all the metagroupList where name in DEFAULT_NAME or UPDATED_NAME
        defaultMetagroupShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the metagroupList where name equals to UPDATED_NAME
        defaultMetagroupShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllMetagroupsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        metagroupRepository.saveAndFlush(metagroup);

        // Get all the metagroupList where name is not null
        defaultMetagroupShouldBeFound("name.specified=true");

        // Get all the metagroupList where name is null
        defaultMetagroupShouldNotBeFound("name.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultMetagroupShouldBeFound(String filter) throws Exception {
        restMetagroupMockMvc.perform(get("/api/metagroups?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(metagroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultMetagroupShouldNotBeFound(String filter) throws Exception {
        restMetagroupMockMvc.perform(get("/api/metagroups?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingMetagroup() throws Exception {
        // Get the metagroup
        restMetagroupMockMvc.perform(get("/api/metagroups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMetagroup() throws Exception {
        // Initialize the database
        metagroupService.save(metagroup);

        int databaseSizeBeforeUpdate = metagroupRepository.findAll().size();

        // Update the metagroup
        Metagroup updatedMetagroup = metagroupRepository.findOne(metagroup.getId());
        updatedMetagroup
            .name(UPDATED_NAME);

        restMetagroupMockMvc.perform(put("/api/metagroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMetagroup)))
            .andExpect(status().isOk());

        // Validate the Metagroup in the database
        List<Metagroup> metagroupList = metagroupRepository.findAll();
        assertThat(metagroupList).hasSize(databaseSizeBeforeUpdate);
        Metagroup testMetagroup = metagroupList.get(metagroupList.size() - 1);
        assertThat(testMetagroup.getName()).isEqualTo(UPDATED_NAME);

        // Validate the Metagroup in Elasticsearch
        Metagroup metagroupEs = metagroupSearchRepository.findOne(testMetagroup.getId());
        assertThat(metagroupEs).isEqualToComparingFieldByField(testMetagroup);
    }

    @Test
    @Transactional
    public void updateNonExistingMetagroup() throws Exception {
        int databaseSizeBeforeUpdate = metagroupRepository.findAll().size();

        // Create the Metagroup

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMetagroupMockMvc.perform(put("/api/metagroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(metagroup)))
            .andExpect(status().isCreated());

        // Validate the Metagroup in the database
        List<Metagroup> metagroupList = metagroupRepository.findAll();
        assertThat(metagroupList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMetagroup() throws Exception {
        // Initialize the database
        metagroupService.save(metagroup);

        int databaseSizeBeforeDelete = metagroupRepository.findAll().size();

        // Get the metagroup
        restMetagroupMockMvc.perform(delete("/api/metagroups/{id}", metagroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean metagroupExistsInEs = metagroupSearchRepository.exists(metagroup.getId());
        assertThat(metagroupExistsInEs).isFalse();

        // Validate the database is empty
        List<Metagroup> metagroupList = metagroupRepository.findAll();
        assertThat(metagroupList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMetagroup() throws Exception {
        // Initialize the database
        metagroupService.save(metagroup);

        // Search the metagroup
        restMetagroupMockMvc.perform(get("/api/_search/metagroups?query=id:" + metagroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(metagroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Metagroup.class);
        Metagroup metagroup1 = new Metagroup();
        metagroup1.setId(1L);
        Metagroup metagroup2 = new Metagroup();
        metagroup2.setId(metagroup1.getId());
        assertThat(metagroup1).isEqualTo(metagroup2);
        metagroup2.setId(2L);
        assertThat(metagroup1).isNotEqualTo(metagroup2);
        metagroup1.setId(null);
        assertThat(metagroup1).isNotEqualTo(metagroup2);
    }
}
