package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.SiteChatgroup;
import biz.newparadigm.meridian.domain.Location;
import biz.newparadigm.meridian.domain.SiteChatgroupComment;
import biz.newparadigm.meridian.domain.SiteChatgroupDomain;
import biz.newparadigm.meridian.domain.SiteChatgroupIp;
import biz.newparadigm.meridian.domain.SiteChatgroupName;
import biz.newparadigm.meridian.domain.SiteChatgroupProfile;
import biz.newparadigm.meridian.domain.SiteChatgroupNumVisitors;
import biz.newparadigm.meridian.domain.SiteChatgroupNumUsers;
import biz.newparadigm.meridian.domain.SiteChatgroupToLanguage;
import biz.newparadigm.meridian.domain.SiteChatgroupToTag;
import biz.newparadigm.meridian.domain.AuthorToSiteChatgroup;
import biz.newparadigm.meridian.repository.SiteChatgroupRepository;
import biz.newparadigm.meridian.service.SiteChatgroupService;
import biz.newparadigm.meridian.repository.search.SiteChatgroupSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.SiteChatgroupCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SiteChatgroupResource REST controller.
 *
 * @see SiteChatgroupResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class SiteChatgroupResourceIntTest {

    private static final Boolean DEFAULT_IS_SITE = false;
    private static final Boolean UPDATED_IS_SITE = true;

    private static final String DEFAULT_ORIGINAL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ENGLISH_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ENGLISH_NAME = "BBBBBBBBBB";

    @Autowired
    private SiteChatgroupRepository siteChatgroupRepository;

    @Autowired
    private SiteChatgroupService siteChatgroupService;

    @Autowired
    private SiteChatgroupSearchRepository siteChatgroupSearchRepository;

    @Autowired
    private SiteChatgroupQueryService siteChatgroupQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSiteChatgroupMockMvc;

    private SiteChatgroup siteChatgroup;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SiteChatgroupResource siteChatgroupResource = new SiteChatgroupResource();
        this.restSiteChatgroupMockMvc = MockMvcBuilders.standaloneSetup(siteChatgroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SiteChatgroup createEntity(EntityManager em) {
        SiteChatgroup siteChatgroup = new SiteChatgroup()
            .isSite(DEFAULT_IS_SITE)
            .originalName(DEFAULT_ORIGINAL_NAME)
            .englishName(DEFAULT_ENGLISH_NAME);
        // Add required entity
        Location location = LocationResourceIntTest.createEntity(em);
        em.persist(location);
        em.flush();
        siteChatgroup.setLocation(location);
        return siteChatgroup;
    }

    @Before
    public void initTest() {
        siteChatgroupSearchRepository.deleteAll();
        siteChatgroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createSiteChatgroup() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupRepository.findAll().size();

        // Create the SiteChatgroup
        restSiteChatgroupMockMvc.perform(post("/api/site-chatgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroup)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroup in the database
        List<SiteChatgroup> siteChatgroupList = siteChatgroupRepository.findAll();
        assertThat(siteChatgroupList).hasSize(databaseSizeBeforeCreate + 1);
        SiteChatgroup testSiteChatgroup = siteChatgroupList.get(siteChatgroupList.size() - 1);
        assertThat(testSiteChatgroup.isIsSite()).isEqualTo(DEFAULT_IS_SITE);
        assertThat(testSiteChatgroup.getOriginalName()).isEqualTo(DEFAULT_ORIGINAL_NAME);
        assertThat(testSiteChatgroup.getEnglishName()).isEqualTo(DEFAULT_ENGLISH_NAME);

        // Validate the SiteChatgroup in Elasticsearch
        SiteChatgroup siteChatgroupEs = siteChatgroupSearchRepository.findOne(testSiteChatgroup.getId());
        assertThat(siteChatgroupEs).isEqualToComparingFieldByField(testSiteChatgroup);
    }

    @Test
    @Transactional
    public void createSiteChatgroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupRepository.findAll().size();

        // Create the SiteChatgroup with an existing ID
        siteChatgroup.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSiteChatgroupMockMvc.perform(post("/api/site-chatgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroup)))
            .andExpect(status().isBadRequest());

        // Validate the SiteChatgroup in the database
        List<SiteChatgroup> siteChatgroupList = siteChatgroupRepository.findAll();
        assertThat(siteChatgroupList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkIsSiteIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupRepository.findAll().size();
        // set the field null
        siteChatgroup.setIsSite(null);

        // Create the SiteChatgroup, which fails.

        restSiteChatgroupMockMvc.perform(post("/api/site-chatgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroup)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroup> siteChatgroupList = siteChatgroupRepository.findAll();
        assertThat(siteChatgroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOriginalNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupRepository.findAll().size();
        // set the field null
        siteChatgroup.setOriginalName(null);

        // Create the SiteChatgroup, which fails.

        restSiteChatgroupMockMvc.perform(post("/api/site-chatgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroup)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroup> siteChatgroupList = siteChatgroupRepository.findAll();
        assertThat(siteChatgroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEnglishNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupRepository.findAll().size();
        // set the field null
        siteChatgroup.setEnglishName(null);

        // Create the SiteChatgroup, which fails.

        restSiteChatgroupMockMvc.perform(post("/api/site-chatgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroup)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroup> siteChatgroupList = siteChatgroupRepository.findAll();
        assertThat(siteChatgroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroups() throws Exception {
        // Initialize the database
        siteChatgroupRepository.saveAndFlush(siteChatgroup);

        // Get all the siteChatgroupList
        restSiteChatgroupMockMvc.perform(get("/api/site-chatgroups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].isSite").value(hasItem(DEFAULT_IS_SITE.booleanValue())))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].englishName").value(hasItem(DEFAULT_ENGLISH_NAME.toString())));
    }

    @Test
    @Transactional
    public void getSiteChatgroup() throws Exception {
        // Initialize the database
        siteChatgroupRepository.saveAndFlush(siteChatgroup);

        // Get the siteChatgroup
        restSiteChatgroupMockMvc.perform(get("/api/site-chatgroups/{id}", siteChatgroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(siteChatgroup.getId().intValue()))
            .andExpect(jsonPath("$.isSite").value(DEFAULT_IS_SITE.booleanValue()))
            .andExpect(jsonPath("$.originalName").value(DEFAULT_ORIGINAL_NAME.toString()))
            .andExpect(jsonPath("$.englishName").value(DEFAULT_ENGLISH_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupsByIsSiteIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupRepository.saveAndFlush(siteChatgroup);

        // Get all the siteChatgroupList where isSite equals to DEFAULT_IS_SITE
        defaultSiteChatgroupShouldBeFound("isSite.equals=" + DEFAULT_IS_SITE);

        // Get all the siteChatgroupList where isSite equals to UPDATED_IS_SITE
        defaultSiteChatgroupShouldNotBeFound("isSite.equals=" + UPDATED_IS_SITE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupsByIsSiteIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupRepository.saveAndFlush(siteChatgroup);

        // Get all the siteChatgroupList where isSite in DEFAULT_IS_SITE or UPDATED_IS_SITE
        defaultSiteChatgroupShouldBeFound("isSite.in=" + DEFAULT_IS_SITE + "," + UPDATED_IS_SITE);

        // Get all the siteChatgroupList where isSite equals to UPDATED_IS_SITE
        defaultSiteChatgroupShouldNotBeFound("isSite.in=" + UPDATED_IS_SITE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupsByIsSiteIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupRepository.saveAndFlush(siteChatgroup);

        // Get all the siteChatgroupList where isSite is not null
        defaultSiteChatgroupShouldBeFound("isSite.specified=true");

        // Get all the siteChatgroupList where isSite is null
        defaultSiteChatgroupShouldNotBeFound("isSite.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupsByOriginalNameIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupRepository.saveAndFlush(siteChatgroup);

        // Get all the siteChatgroupList where originalName equals to DEFAULT_ORIGINAL_NAME
        defaultSiteChatgroupShouldBeFound("originalName.equals=" + DEFAULT_ORIGINAL_NAME);

        // Get all the siteChatgroupList where originalName equals to UPDATED_ORIGINAL_NAME
        defaultSiteChatgroupShouldNotBeFound("originalName.equals=" + UPDATED_ORIGINAL_NAME);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupsByOriginalNameIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupRepository.saveAndFlush(siteChatgroup);

        // Get all the siteChatgroupList where originalName in DEFAULT_ORIGINAL_NAME or UPDATED_ORIGINAL_NAME
        defaultSiteChatgroupShouldBeFound("originalName.in=" + DEFAULT_ORIGINAL_NAME + "," + UPDATED_ORIGINAL_NAME);

        // Get all the siteChatgroupList where originalName equals to UPDATED_ORIGINAL_NAME
        defaultSiteChatgroupShouldNotBeFound("originalName.in=" + UPDATED_ORIGINAL_NAME);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupsByOriginalNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupRepository.saveAndFlush(siteChatgroup);

        // Get all the siteChatgroupList where originalName is not null
        defaultSiteChatgroupShouldBeFound("originalName.specified=true");

        // Get all the siteChatgroupList where originalName is null
        defaultSiteChatgroupShouldNotBeFound("originalName.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupsByEnglishNameIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupRepository.saveAndFlush(siteChatgroup);

        // Get all the siteChatgroupList where englishName equals to DEFAULT_ENGLISH_NAME
        defaultSiteChatgroupShouldBeFound("englishName.equals=" + DEFAULT_ENGLISH_NAME);

        // Get all the siteChatgroupList where englishName equals to UPDATED_ENGLISH_NAME
        defaultSiteChatgroupShouldNotBeFound("englishName.equals=" + UPDATED_ENGLISH_NAME);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupsByEnglishNameIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupRepository.saveAndFlush(siteChatgroup);

        // Get all the siteChatgroupList where englishName in DEFAULT_ENGLISH_NAME or UPDATED_ENGLISH_NAME
        defaultSiteChatgroupShouldBeFound("englishName.in=" + DEFAULT_ENGLISH_NAME + "," + UPDATED_ENGLISH_NAME);

        // Get all the siteChatgroupList where englishName equals to UPDATED_ENGLISH_NAME
        defaultSiteChatgroupShouldNotBeFound("englishName.in=" + UPDATED_ENGLISH_NAME);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupsByEnglishNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupRepository.saveAndFlush(siteChatgroup);

        // Get all the siteChatgroupList where englishName is not null
        defaultSiteChatgroupShouldBeFound("englishName.specified=true");

        // Get all the siteChatgroupList where englishName is null
        defaultSiteChatgroupShouldNotBeFound("englishName.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupsByLocationIsEqualToSomething() throws Exception {
        // Initialize the database
        Location location = LocationResourceIntTest.createEntity(em);
        em.persist(location);
        em.flush();
        siteChatgroup.setLocation(location);
        siteChatgroupRepository.saveAndFlush(siteChatgroup);
        Long locationId = location.getId();

        // Get all the siteChatgroupList where location equals to locationId
        defaultSiteChatgroupShouldBeFound("locationId.equals=" + locationId);

        // Get all the siteChatgroupList where location equals to locationId + 1
        defaultSiteChatgroupShouldNotBeFound("locationId.equals=" + (locationId + 1));
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupsByCommentsIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroupComment comments = SiteChatgroupCommentResourceIntTest.createEntity(em);
        em.persist(comments);
        em.flush();
        siteChatgroupRepository.saveAndFlush(siteChatgroup);
        Long commentsId = comments.getId();

        // Get all the siteChatgroupList where comments equals to commentsId
        defaultSiteChatgroupShouldBeFound("commentsId.equals=" + commentsId);

        // Get all the siteChatgroupList where comments equals to commentsId + 1
        defaultSiteChatgroupShouldNotBeFound("commentsId.equals=" + (commentsId + 1));
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupsByDomainsIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroupDomain domains = SiteChatgroupDomainResourceIntTest.createEntity(em);
        em.persist(domains);
        em.flush();
        siteChatgroup.addDomains(domains);
        siteChatgroupRepository.saveAndFlush(siteChatgroup);
        Long domainsId = domains.getId();

        // Get all the siteChatgroupList where domains equals to domainsId
        defaultSiteChatgroupShouldBeFound("domainsId.equals=" + domainsId);

        // Get all the siteChatgroupList where domains equals to domainsId + 1
        defaultSiteChatgroupShouldNotBeFound("domainsId.equals=" + (domainsId + 1));
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupsByIpsIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroupIp ips = SiteChatgroupIpResourceIntTest.createEntity(em);
        em.persist(ips);
        em.flush();
        siteChatgroup.addIps(ips);
        siteChatgroupRepository.saveAndFlush(siteChatgroup);
        Long ipsId = ips.getId();

        // Get all the siteChatgroupList where ips equals to ipsId
        defaultSiteChatgroupShouldBeFound("ipsId.equals=" + ipsId);

        // Get all the siteChatgroupList where ips equals to ipsId + 1
        defaultSiteChatgroupShouldNotBeFound("ipsId.equals=" + (ipsId + 1));
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupsByNamesIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroupName names = SiteChatgroupNameResourceIntTest.createEntity(em);
        em.persist(names);
        em.flush();
        siteChatgroup.addNames(names);
        siteChatgroupRepository.saveAndFlush(siteChatgroup);
        Long namesId = names.getId();

        // Get all the siteChatgroupList where names equals to namesId
        defaultSiteChatgroupShouldBeFound("namesId.equals=" + namesId);

        // Get all the siteChatgroupList where names equals to namesId + 1
        defaultSiteChatgroupShouldNotBeFound("namesId.equals=" + (namesId + 1));
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupsByInfoIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroupProfile info = SiteChatgroupProfileResourceIntTest.createEntity(em);
        em.persist(info);
        em.flush();
        siteChatgroupRepository.saveAndFlush(siteChatgroup);
        Long infoId = info.getId();

        // Get all the siteChatgroupList where info equals to infoId
        defaultSiteChatgroupShouldBeFound("infoId.equals=" + infoId);

        // Get all the siteChatgroupList where info equals to infoId + 1
        defaultSiteChatgroupShouldNotBeFound("infoId.equals=" + (infoId + 1));
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupsByNumVisitorsIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroupNumVisitors numVisitors = SiteChatgroupNumVisitorsResourceIntTest.createEntity(em);
        em.persist(numVisitors);
        em.flush();
        siteChatgroup.addNumVisitors(numVisitors);
        siteChatgroupRepository.saveAndFlush(siteChatgroup);
        Long numVisitorsId = numVisitors.getId();

        // Get all the siteChatgroupList where numVisitors equals to numVisitorsId
        defaultSiteChatgroupShouldBeFound("numVisitorsId.equals=" + numVisitorsId);

        // Get all the siteChatgroupList where numVisitors equals to numVisitorsId + 1
        defaultSiteChatgroupShouldNotBeFound("numVisitorsId.equals=" + (numVisitorsId + 1));
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupsByNumUsersIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroupNumUsers numUsers = SiteChatgroupNumUsersResourceIntTest.createEntity(em);
        em.persist(numUsers);
        em.flush();
        siteChatgroup.addNumUsers(numUsers);
        siteChatgroupRepository.saveAndFlush(siteChatgroup);
        Long numUsersId = numUsers.getId();

        // Get all the siteChatgroupList where numUsers equals to numUsersId
        defaultSiteChatgroupShouldBeFound("numUsersId.equals=" + numUsersId);

        // Get all the siteChatgroupList where numUsers equals to numUsersId + 1
        defaultSiteChatgroupShouldNotBeFound("numUsersId.equals=" + (numUsersId + 1));
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupsByLanguagesIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroupToLanguage languages = SiteChatgroupToLanguageResourceIntTest.createEntity(em);
        em.persist(languages);
        em.flush();
        siteChatgroup.addLanguages(languages);
        siteChatgroupRepository.saveAndFlush(siteChatgroup);
        Long languagesId = languages.getId();

        // Get all the siteChatgroupList where languages equals to languagesId
        defaultSiteChatgroupShouldBeFound("languagesId.equals=" + languagesId);

        // Get all the siteChatgroupList where languages equals to languagesId + 1
        defaultSiteChatgroupShouldNotBeFound("languagesId.equals=" + (languagesId + 1));
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupsByTagsIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroupToTag tags = SiteChatgroupToTagResourceIntTest.createEntity(em);
        em.persist(tags);
        em.flush();
        siteChatgroup.addTags(tags);
        siteChatgroupRepository.saveAndFlush(siteChatgroup);
        Long tagsId = tags.getId();

        // Get all the siteChatgroupList where tags equals to tagsId
        defaultSiteChatgroupShouldBeFound("tagsId.equals=" + tagsId);

        // Get all the siteChatgroupList where tags equals to tagsId + 1
        defaultSiteChatgroupShouldNotBeFound("tagsId.equals=" + (tagsId + 1));
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupsByAuthorsIsEqualToSomething() throws Exception {
        // Initialize the database
        AuthorToSiteChatgroup authors = AuthorToSiteChatgroupResourceIntTest.createEntity(em);
        em.persist(authors);
        em.flush();
        siteChatgroup.addAuthors(authors);
        siteChatgroupRepository.saveAndFlush(siteChatgroup);
        Long authorsId = authors.getId();

        // Get all the siteChatgroupList where authors equals to authorsId
        defaultSiteChatgroupShouldBeFound("authorsId.equals=" + authorsId);

        // Get all the siteChatgroupList where authors equals to authorsId + 1
        defaultSiteChatgroupShouldNotBeFound("authorsId.equals=" + (authorsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSiteChatgroupShouldBeFound(String filter) throws Exception {
        restSiteChatgroupMockMvc.perform(get("/api/site-chatgroups?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].isSite").value(hasItem(DEFAULT_IS_SITE.booleanValue())))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].englishName").value(hasItem(DEFAULT_ENGLISH_NAME.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSiteChatgroupShouldNotBeFound(String filter) throws Exception {
        restSiteChatgroupMockMvc.perform(get("/api/site-chatgroups?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingSiteChatgroup() throws Exception {
        // Get the siteChatgroup
        restSiteChatgroupMockMvc.perform(get("/api/site-chatgroups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSiteChatgroup() throws Exception {
        // Initialize the database
        siteChatgroupService.save(siteChatgroup);

        int databaseSizeBeforeUpdate = siteChatgroupRepository.findAll().size();

        // Update the siteChatgroup
        SiteChatgroup updatedSiteChatgroup = siteChatgroupRepository.findOne(siteChatgroup.getId());
        updatedSiteChatgroup
            .isSite(UPDATED_IS_SITE)
            .originalName(UPDATED_ORIGINAL_NAME)
            .englishName(UPDATED_ENGLISH_NAME);

        restSiteChatgroupMockMvc.perform(put("/api/site-chatgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSiteChatgroup)))
            .andExpect(status().isOk());

        // Validate the SiteChatgroup in the database
        List<SiteChatgroup> siteChatgroupList = siteChatgroupRepository.findAll();
        assertThat(siteChatgroupList).hasSize(databaseSizeBeforeUpdate);
        SiteChatgroup testSiteChatgroup = siteChatgroupList.get(siteChatgroupList.size() - 1);
        assertThat(testSiteChatgroup.isIsSite()).isEqualTo(UPDATED_IS_SITE);
        assertThat(testSiteChatgroup.getOriginalName()).isEqualTo(UPDATED_ORIGINAL_NAME);
        assertThat(testSiteChatgroup.getEnglishName()).isEqualTo(UPDATED_ENGLISH_NAME);

        // Validate the SiteChatgroup in Elasticsearch
        SiteChatgroup siteChatgroupEs = siteChatgroupSearchRepository.findOne(testSiteChatgroup.getId());
        assertThat(siteChatgroupEs).isEqualToComparingFieldByField(testSiteChatgroup);
    }

    @Test
    @Transactional
    public void updateNonExistingSiteChatgroup() throws Exception {
        int databaseSizeBeforeUpdate = siteChatgroupRepository.findAll().size();

        // Create the SiteChatgroup

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSiteChatgroupMockMvc.perform(put("/api/site-chatgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroup)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroup in the database
        List<SiteChatgroup> siteChatgroupList = siteChatgroupRepository.findAll();
        assertThat(siteChatgroupList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSiteChatgroup() throws Exception {
        // Initialize the database
        siteChatgroupService.save(siteChatgroup);

        int databaseSizeBeforeDelete = siteChatgroupRepository.findAll().size();

        // Get the siteChatgroup
        restSiteChatgroupMockMvc.perform(delete("/api/site-chatgroups/{id}", siteChatgroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean siteChatgroupExistsInEs = siteChatgroupSearchRepository.exists(siteChatgroup.getId());
        assertThat(siteChatgroupExistsInEs).isFalse();

        // Validate the database is empty
        List<SiteChatgroup> siteChatgroupList = siteChatgroupRepository.findAll();
        assertThat(siteChatgroupList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSiteChatgroup() throws Exception {
        // Initialize the database
        siteChatgroupService.save(siteChatgroup);

        // Search the siteChatgroup
        restSiteChatgroupMockMvc.perform(get("/api/_search/site-chatgroups?query=id:" + siteChatgroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].isSite").value(hasItem(DEFAULT_IS_SITE.booleanValue())))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].englishName").value(hasItem(DEFAULT_ENGLISH_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SiteChatgroup.class);
        SiteChatgroup siteChatgroup1 = new SiteChatgroup();
        siteChatgroup1.setId(1L);
        SiteChatgroup siteChatgroup2 = new SiteChatgroup();
        siteChatgroup2.setId(siteChatgroup1.getId());
        assertThat(siteChatgroup1).isEqualTo(siteChatgroup2);
        siteChatgroup2.setId(2L);
        assertThat(siteChatgroup1).isNotEqualTo(siteChatgroup2);
        siteChatgroup1.setId(null);
        assertThat(siteChatgroup1).isNotEqualTo(siteChatgroup2);
    }
}
