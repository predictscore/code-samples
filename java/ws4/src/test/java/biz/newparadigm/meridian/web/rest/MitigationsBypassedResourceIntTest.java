package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.MitigationsBypassed;
import biz.newparadigm.meridian.repository.MitigationsBypassedRepository;
import biz.newparadigm.meridian.service.MitigationsBypassedService;
import biz.newparadigm.meridian.repository.search.MitigationsBypassedSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.MitigationsBypassedCriteria;
import biz.newparadigm.meridian.service.MitigationsBypassedQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MitigationsBypassedResource REST controller.
 *
 * @see MitigationsBypassedResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class MitigationsBypassedResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private MitigationsBypassedRepository mitigationsBypassedRepository;

    @Autowired
    private MitigationsBypassedService mitigationsBypassedService;

    @Autowired
    private MitigationsBypassedSearchRepository mitigationsBypassedSearchRepository;

    @Autowired
    private MitigationsBypassedQueryService mitigationsBypassedQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMitigationsBypassedMockMvc;

    private MitigationsBypassed mitigationsBypassed;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MitigationsBypassedResource mitigationsBypassedResource = new MitigationsBypassedResource(mitigationsBypassedService, mitigationsBypassedQueryService);
        this.restMitigationsBypassedMockMvc = MockMvcBuilders.standaloneSetup(mitigationsBypassedResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MitigationsBypassed createEntity(EntityManager em) {
        MitigationsBypassed mitigationsBypassed = new MitigationsBypassed()
            .description(DEFAULT_DESCRIPTION);
        return mitigationsBypassed;
    }

    @Before
    public void initTest() {
        mitigationsBypassedSearchRepository.deleteAll();
        mitigationsBypassed = createEntity(em);
    }

    @Test
    @Transactional
    public void createMitigationsBypassed() throws Exception {
        int databaseSizeBeforeCreate = mitigationsBypassedRepository.findAll().size();

        // Create the MitigationsBypassed
        restMitigationsBypassedMockMvc.perform(post("/api/mitigations-bypasseds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mitigationsBypassed)))
            .andExpect(status().isCreated());

        // Validate the MitigationsBypassed in the database
        List<MitigationsBypassed> mitigationsBypassedList = mitigationsBypassedRepository.findAll();
        assertThat(mitigationsBypassedList).hasSize(databaseSizeBeforeCreate + 1);
        MitigationsBypassed testMitigationsBypassed = mitigationsBypassedList.get(mitigationsBypassedList.size() - 1);
        assertThat(testMitigationsBypassed.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the MitigationsBypassed in Elasticsearch
        MitigationsBypassed mitigationsBypassedEs = mitigationsBypassedSearchRepository.findOne(testMitigationsBypassed.getId());
        assertThat(mitigationsBypassedEs).isEqualToComparingFieldByField(testMitigationsBypassed);
    }

    @Test
    @Transactional
    public void createMitigationsBypassedWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = mitigationsBypassedRepository.findAll().size();

        // Create the MitigationsBypassed with an existing ID
        mitigationsBypassed.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMitigationsBypassedMockMvc.perform(post("/api/mitigations-bypasseds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mitigationsBypassed)))
            .andExpect(status().isBadRequest());

        // Validate the MitigationsBypassed in the database
        List<MitigationsBypassed> mitigationsBypassedList = mitigationsBypassedRepository.findAll();
        assertThat(mitigationsBypassedList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllMitigationsBypasseds() throws Exception {
        // Initialize the database
        mitigationsBypassedRepository.saveAndFlush(mitigationsBypassed);

        // Get all the mitigationsBypassedList
        restMitigationsBypassedMockMvc.perform(get("/api/mitigations-bypasseds?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mitigationsBypassed.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getMitigationsBypassed() throws Exception {
        // Initialize the database
        mitigationsBypassedRepository.saveAndFlush(mitigationsBypassed);

        // Get the mitigationsBypassed
        restMitigationsBypassedMockMvc.perform(get("/api/mitigations-bypasseds/{id}", mitigationsBypassed.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(mitigationsBypassed.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getAllMitigationsBypassedsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        mitigationsBypassedRepository.saveAndFlush(mitigationsBypassed);

        // Get all the mitigationsBypassedList where description equals to DEFAULT_DESCRIPTION
        defaultMitigationsBypassedShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the mitigationsBypassedList where description equals to UPDATED_DESCRIPTION
        defaultMitigationsBypassedShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllMitigationsBypassedsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        mitigationsBypassedRepository.saveAndFlush(mitigationsBypassed);

        // Get all the mitigationsBypassedList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultMitigationsBypassedShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the mitigationsBypassedList where description equals to UPDATED_DESCRIPTION
        defaultMitigationsBypassedShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllMitigationsBypassedsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        mitigationsBypassedRepository.saveAndFlush(mitigationsBypassed);

        // Get all the mitigationsBypassedList where description is not null
        defaultMitigationsBypassedShouldBeFound("description.specified=true");

        // Get all the mitigationsBypassedList where description is null
        defaultMitigationsBypassedShouldNotBeFound("description.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultMitigationsBypassedShouldBeFound(String filter) throws Exception {
        restMitigationsBypassedMockMvc.perform(get("/api/mitigations-bypasseds?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mitigationsBypassed.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultMitigationsBypassedShouldNotBeFound(String filter) throws Exception {
        restMitigationsBypassedMockMvc.perform(get("/api/mitigations-bypasseds?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingMitigationsBypassed() throws Exception {
        // Get the mitigationsBypassed
        restMitigationsBypassedMockMvc.perform(get("/api/mitigations-bypasseds/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMitigationsBypassed() throws Exception {
        // Initialize the database
        mitigationsBypassedService.save(mitigationsBypassed);

        int databaseSizeBeforeUpdate = mitigationsBypassedRepository.findAll().size();

        // Update the mitigationsBypassed
        MitigationsBypassed updatedMitigationsBypassed = mitigationsBypassedRepository.findOne(mitigationsBypassed.getId());
        updatedMitigationsBypassed
            .description(UPDATED_DESCRIPTION);

        restMitigationsBypassedMockMvc.perform(put("/api/mitigations-bypasseds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMitigationsBypassed)))
            .andExpect(status().isOk());

        // Validate the MitigationsBypassed in the database
        List<MitigationsBypassed> mitigationsBypassedList = mitigationsBypassedRepository.findAll();
        assertThat(mitigationsBypassedList).hasSize(databaseSizeBeforeUpdate);
        MitigationsBypassed testMitigationsBypassed = mitigationsBypassedList.get(mitigationsBypassedList.size() - 1);
        assertThat(testMitigationsBypassed.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the MitigationsBypassed in Elasticsearch
        MitigationsBypassed mitigationsBypassedEs = mitigationsBypassedSearchRepository.findOne(testMitigationsBypassed.getId());
        assertThat(mitigationsBypassedEs).isEqualToComparingFieldByField(testMitigationsBypassed);
    }

    @Test
    @Transactional
    public void updateNonExistingMitigationsBypassed() throws Exception {
        int databaseSizeBeforeUpdate = mitigationsBypassedRepository.findAll().size();

        // Create the MitigationsBypassed

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMitigationsBypassedMockMvc.perform(put("/api/mitigations-bypasseds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mitigationsBypassed)))
            .andExpect(status().isCreated());

        // Validate the MitigationsBypassed in the database
        List<MitigationsBypassed> mitigationsBypassedList = mitigationsBypassedRepository.findAll();
        assertThat(mitigationsBypassedList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMitigationsBypassed() throws Exception {
        // Initialize the database
        mitigationsBypassedService.save(mitigationsBypassed);

        int databaseSizeBeforeDelete = mitigationsBypassedRepository.findAll().size();

        // Get the mitigationsBypassed
        restMitigationsBypassedMockMvc.perform(delete("/api/mitigations-bypasseds/{id}", mitigationsBypassed.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean mitigationsBypassedExistsInEs = mitigationsBypassedSearchRepository.exists(mitigationsBypassed.getId());
        assertThat(mitigationsBypassedExistsInEs).isFalse();

        // Validate the database is empty
        List<MitigationsBypassed> mitigationsBypassedList = mitigationsBypassedRepository.findAll();
        assertThat(mitigationsBypassedList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMitigationsBypassed() throws Exception {
        // Initialize the database
        mitigationsBypassedService.save(mitigationsBypassed);

        // Search the mitigationsBypassed
        restMitigationsBypassedMockMvc.perform(get("/api/_search/mitigations-bypasseds?query=id:" + mitigationsBypassed.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mitigationsBypassed.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MitigationsBypassed.class);
        MitigationsBypassed mitigationsBypassed1 = new MitigationsBypassed();
        mitigationsBypassed1.setId(1L);
        MitigationsBypassed mitigationsBypassed2 = new MitigationsBypassed();
        mitigationsBypassed2.setId(mitigationsBypassed1.getId());
        assertThat(mitigationsBypassed1).isEqualTo(mitigationsBypassed2);
        mitigationsBypassed2.setId(2L);
        assertThat(mitigationsBypassed1).isNotEqualTo(mitigationsBypassed2);
        mitigationsBypassed1.setId(null);
        assertThat(mitigationsBypassed1).isNotEqualTo(mitigationsBypassed2);
    }
}
