package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsAttachment;
import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.repository.NewsAttachmentRepository;
import biz.newparadigm.meridian.service.NewsAttachmentService;
import biz.newparadigm.meridian.repository.search.NewsAttachmentSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsAttachmentCriteria;
import biz.newparadigm.meridian.service.NewsAttachmentQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsAttachmentResource REST controller.
 *
 * @see NewsAttachmentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsAttachmentResourceIntTest {

    private static final String DEFAULT_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_FILENAME = "BBBBBBBBBB";

    private static final byte[] DEFAULT_BLOB = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_BLOB = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_BLOB_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_BLOB_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private NewsAttachmentRepository newsAttachmentRepository;

    @Autowired
    private NewsAttachmentService newsAttachmentService;

    @Autowired
    private NewsAttachmentSearchRepository newsAttachmentSearchRepository;

    @Autowired
    private NewsAttachmentQueryService newsAttachmentQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsAttachmentMockMvc;

    private NewsAttachment newsAttachment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsAttachmentResource newsAttachmentResource = new NewsAttachmentResource(newsAttachmentService, newsAttachmentQueryService);
        this.restNewsAttachmentMockMvc = MockMvcBuilders.standaloneSetup(newsAttachmentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsAttachment createEntity(EntityManager em) {
        NewsAttachment newsAttachment = new NewsAttachment()
            .filename(DEFAULT_FILENAME)
            .blob(DEFAULT_BLOB)
            .blobContentType(DEFAULT_BLOB_CONTENT_TYPE)
            .description(DEFAULT_DESCRIPTION);
        // Add required entity
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsAttachment.setNews(news);
        return newsAttachment;
    }

    @Before
    public void initTest() {
        newsAttachmentSearchRepository.deleteAll();
        newsAttachment = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsAttachment() throws Exception {
        int databaseSizeBeforeCreate = newsAttachmentRepository.findAll().size();

        // Create the NewsAttachment
        restNewsAttachmentMockMvc.perform(post("/api/news-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsAttachment)))
            .andExpect(status().isCreated());

        // Validate the NewsAttachment in the database
        List<NewsAttachment> newsAttachmentList = newsAttachmentRepository.findAll();
        assertThat(newsAttachmentList).hasSize(databaseSizeBeforeCreate + 1);
        NewsAttachment testNewsAttachment = newsAttachmentList.get(newsAttachmentList.size() - 1);
        assertThat(testNewsAttachment.getFilename()).isEqualTo(DEFAULT_FILENAME);
        assertThat(testNewsAttachment.getBlob()).isEqualTo(DEFAULT_BLOB);
        assertThat(testNewsAttachment.getBlobContentType()).isEqualTo(DEFAULT_BLOB_CONTENT_TYPE);
        assertThat(testNewsAttachment.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the NewsAttachment in Elasticsearch
        NewsAttachment newsAttachmentEs = newsAttachmentSearchRepository.findOne(testNewsAttachment.getId());
        assertThat(newsAttachmentEs).isEqualToComparingFieldByField(testNewsAttachment);
    }

    @Test
    @Transactional
    public void createNewsAttachmentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsAttachmentRepository.findAll().size();

        // Create the NewsAttachment with an existing ID
        newsAttachment.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsAttachmentMockMvc.perform(post("/api/news-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsAttachment)))
            .andExpect(status().isBadRequest());

        // Validate the NewsAttachment in the database
        List<NewsAttachment> newsAttachmentList = newsAttachmentRepository.findAll();
        assertThat(newsAttachmentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFilenameIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsAttachmentRepository.findAll().size();
        // set the field null
        newsAttachment.setFilename(null);

        // Create the NewsAttachment, which fails.

        restNewsAttachmentMockMvc.perform(post("/api/news-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsAttachment)))
            .andExpect(status().isBadRequest());

        List<NewsAttachment> newsAttachmentList = newsAttachmentRepository.findAll();
        assertThat(newsAttachmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBlobIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsAttachmentRepository.findAll().size();
        // set the field null
        newsAttachment.setBlob(null);

        // Create the NewsAttachment, which fails.

        restNewsAttachmentMockMvc.perform(post("/api/news-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsAttachment)))
            .andExpect(status().isBadRequest());

        List<NewsAttachment> newsAttachmentList = newsAttachmentRepository.findAll();
        assertThat(newsAttachmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNewsAttachments() throws Exception {
        // Initialize the database
        newsAttachmentRepository.saveAndFlush(newsAttachment);

        // Get all the newsAttachmentList
        restNewsAttachmentMockMvc.perform(get("/api/news-attachments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsAttachment.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getNewsAttachment() throws Exception {
        // Initialize the database
        newsAttachmentRepository.saveAndFlush(newsAttachment);

        // Get the newsAttachment
        restNewsAttachmentMockMvc.perform(get("/api/news-attachments/{id}", newsAttachment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsAttachment.getId().intValue()))
            .andExpect(jsonPath("$.filename").value(DEFAULT_FILENAME.toString()))
            .andExpect(jsonPath("$.blobContentType").value(DEFAULT_BLOB_CONTENT_TYPE))
            .andExpect(jsonPath("$.blob").value(Base64Utils.encodeToString(DEFAULT_BLOB)))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getAllNewsAttachmentsByFilenameIsEqualToSomething() throws Exception {
        // Initialize the database
        newsAttachmentRepository.saveAndFlush(newsAttachment);

        // Get all the newsAttachmentList where filename equals to DEFAULT_FILENAME
        defaultNewsAttachmentShouldBeFound("filename.equals=" + DEFAULT_FILENAME);

        // Get all the newsAttachmentList where filename equals to UPDATED_FILENAME
        defaultNewsAttachmentShouldNotBeFound("filename.equals=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllNewsAttachmentsByFilenameIsInShouldWork() throws Exception {
        // Initialize the database
        newsAttachmentRepository.saveAndFlush(newsAttachment);

        // Get all the newsAttachmentList where filename in DEFAULT_FILENAME or UPDATED_FILENAME
        defaultNewsAttachmentShouldBeFound("filename.in=" + DEFAULT_FILENAME + "," + UPDATED_FILENAME);

        // Get all the newsAttachmentList where filename equals to UPDATED_FILENAME
        defaultNewsAttachmentShouldNotBeFound("filename.in=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllNewsAttachmentsByFilenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsAttachmentRepository.saveAndFlush(newsAttachment);

        // Get all the newsAttachmentList where filename is not null
        defaultNewsAttachmentShouldBeFound("filename.specified=true");

        // Get all the newsAttachmentList where filename is null
        defaultNewsAttachmentShouldNotBeFound("filename.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsAttachmentsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        newsAttachmentRepository.saveAndFlush(newsAttachment);

        // Get all the newsAttachmentList where description equals to DEFAULT_DESCRIPTION
        defaultNewsAttachmentShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the newsAttachmentList where description equals to UPDATED_DESCRIPTION
        defaultNewsAttachmentShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsAttachmentsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        newsAttachmentRepository.saveAndFlush(newsAttachment);

        // Get all the newsAttachmentList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultNewsAttachmentShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the newsAttachmentList where description equals to UPDATED_DESCRIPTION
        defaultNewsAttachmentShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsAttachmentsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsAttachmentRepository.saveAndFlush(newsAttachment);

        // Get all the newsAttachmentList where description is not null
        defaultNewsAttachmentShouldBeFound("description.specified=true");

        // Get all the newsAttachmentList where description is null
        defaultNewsAttachmentShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsAttachmentsByNewsIsEqualToSomething() throws Exception {
        // Initialize the database
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsAttachment.setNews(news);
        newsAttachmentRepository.saveAndFlush(newsAttachment);
        Long newsId = news.getId();

        // Get all the newsAttachmentList where news equals to newsId
        defaultNewsAttachmentShouldBeFound("newsId.equals=" + newsId);

        // Get all the newsAttachmentList where news equals to newsId + 1
        defaultNewsAttachmentShouldNotBeFound("newsId.equals=" + (newsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsAttachmentShouldBeFound(String filter) throws Exception {
        restNewsAttachmentMockMvc.perform(get("/api/news-attachments?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsAttachment.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsAttachmentShouldNotBeFound(String filter) throws Exception {
        restNewsAttachmentMockMvc.perform(get("/api/news-attachments?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsAttachment() throws Exception {
        // Get the newsAttachment
        restNewsAttachmentMockMvc.perform(get("/api/news-attachments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsAttachment() throws Exception {
        // Initialize the database
        newsAttachmentService.save(newsAttachment);

        int databaseSizeBeforeUpdate = newsAttachmentRepository.findAll().size();

        // Update the newsAttachment
        NewsAttachment updatedNewsAttachment = newsAttachmentRepository.findOne(newsAttachment.getId());
        updatedNewsAttachment
            .filename(UPDATED_FILENAME)
            .blob(UPDATED_BLOB)
            .blobContentType(UPDATED_BLOB_CONTENT_TYPE)
            .description(UPDATED_DESCRIPTION);

        restNewsAttachmentMockMvc.perform(put("/api/news-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsAttachment)))
            .andExpect(status().isOk());

        // Validate the NewsAttachment in the database
        List<NewsAttachment> newsAttachmentList = newsAttachmentRepository.findAll();
        assertThat(newsAttachmentList).hasSize(databaseSizeBeforeUpdate);
        NewsAttachment testNewsAttachment = newsAttachmentList.get(newsAttachmentList.size() - 1);
        assertThat(testNewsAttachment.getFilename()).isEqualTo(UPDATED_FILENAME);
        assertThat(testNewsAttachment.getBlob()).isEqualTo(UPDATED_BLOB);
        assertThat(testNewsAttachment.getBlobContentType()).isEqualTo(UPDATED_BLOB_CONTENT_TYPE);
        assertThat(testNewsAttachment.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the NewsAttachment in Elasticsearch
        NewsAttachment newsAttachmentEs = newsAttachmentSearchRepository.findOne(testNewsAttachment.getId());
        assertThat(newsAttachmentEs).isEqualToComparingFieldByField(testNewsAttachment);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsAttachment() throws Exception {
        int databaseSizeBeforeUpdate = newsAttachmentRepository.findAll().size();

        // Create the NewsAttachment

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsAttachmentMockMvc.perform(put("/api/news-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsAttachment)))
            .andExpect(status().isCreated());

        // Validate the NewsAttachment in the database
        List<NewsAttachment> newsAttachmentList = newsAttachmentRepository.findAll();
        assertThat(newsAttachmentList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsAttachment() throws Exception {
        // Initialize the database
        newsAttachmentService.save(newsAttachment);

        int databaseSizeBeforeDelete = newsAttachmentRepository.findAll().size();

        // Get the newsAttachment
        restNewsAttachmentMockMvc.perform(delete("/api/news-attachments/{id}", newsAttachment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsAttachmentExistsInEs = newsAttachmentSearchRepository.exists(newsAttachment.getId());
        assertThat(newsAttachmentExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsAttachment> newsAttachmentList = newsAttachmentRepository.findAll();
        assertThat(newsAttachmentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsAttachment() throws Exception {
        // Initialize the database
        newsAttachmentService.save(newsAttachment);

        // Search the newsAttachment
        restNewsAttachmentMockMvc.perform(get("/api/_search/news-attachments?query=id:" + newsAttachment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsAttachment.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsAttachment.class);
        NewsAttachment newsAttachment1 = new NewsAttachment();
        newsAttachment1.setId(1L);
        NewsAttachment newsAttachment2 = new NewsAttachment();
        newsAttachment2.setId(newsAttachment1.getId());
        assertThat(newsAttachment1).isEqualTo(newsAttachment2);
        newsAttachment2.setId(2L);
        assertThat(newsAttachment1).isNotEqualTo(newsAttachment2);
        newsAttachment1.setId(null);
        assertThat(newsAttachment1).isNotEqualTo(newsAttachment2);
    }
}
