package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsSummary;
import biz.newparadigm.meridian.domain.Location;
import biz.newparadigm.meridian.repository.NewsSummaryRepository;
import biz.newparadigm.meridian.service.NewsSummaryService;
import biz.newparadigm.meridian.repository.search.NewsSummarySearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsSummaryCriteria;
import biz.newparadigm.meridian.service.NewsSummaryQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsSummaryResource REST controller.
 *
 * @see NewsSummaryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsSummaryResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private NewsSummaryRepository newsSummaryRepository;

    @Autowired
    private NewsSummaryService newsSummaryService;

    @Autowired
    private NewsSummarySearchRepository newsSummarySearchRepository;

    @Autowired
    private NewsSummaryQueryService newsSummaryQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsSummaryMockMvc;

    private NewsSummary newsSummary;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsSummaryResource newsSummaryResource = new NewsSummaryResource(newsSummaryService, newsSummaryQueryService);
        this.restNewsSummaryMockMvc = MockMvcBuilders.standaloneSetup(newsSummaryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsSummary createEntity(EntityManager em) {
        NewsSummary newsSummary = new NewsSummary()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        // Add required entity
        Location location = LocationResourceIntTest.createEntity(em);
        em.persist(location);
        em.flush();
        newsSummary.setLocation(location);
        return newsSummary;
    }

    @Before
    public void initTest() {
        newsSummarySearchRepository.deleteAll();
        newsSummary = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsSummary() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryRepository.findAll().size();

        // Create the NewsSummary
        restNewsSummaryMockMvc.perform(post("/api/news-summaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummary)))
            .andExpect(status().isCreated());

        // Validate the NewsSummary in the database
        List<NewsSummary> newsSummaryList = newsSummaryRepository.findAll();
        assertThat(newsSummaryList).hasSize(databaseSizeBeforeCreate + 1);
        NewsSummary testNewsSummary = newsSummaryList.get(newsSummaryList.size() - 1);
        assertThat(testNewsSummary.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testNewsSummary.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the NewsSummary in Elasticsearch
        NewsSummary newsSummaryEs = newsSummarySearchRepository.findOne(testNewsSummary.getId());
        assertThat(newsSummaryEs).isEqualToComparingFieldByField(testNewsSummary);
    }

    @Test
    @Transactional
    public void createNewsSummaryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryRepository.findAll().size();

        // Create the NewsSummary with an existing ID
        newsSummary.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsSummaryMockMvc.perform(post("/api/news-summaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummary)))
            .andExpect(status().isBadRequest());

        // Validate the NewsSummary in the database
        List<NewsSummary> newsSummaryList = newsSummaryRepository.findAll();
        assertThat(newsSummaryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllNewsSummaries() throws Exception {
        // Initialize the database
        newsSummaryRepository.saveAndFlush(newsSummary);

        // Get all the newsSummaryList
        restNewsSummaryMockMvc.perform(get("/api/news-summaries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummary.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getNewsSummary() throws Exception {
        // Initialize the database
        newsSummaryRepository.saveAndFlush(newsSummary);

        // Get the newsSummary
        restNewsSummaryMockMvc.perform(get("/api/news-summaries/{id}", newsSummary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsSummary.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getAllNewsSummariesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryRepository.saveAndFlush(newsSummary);

        // Get all the newsSummaryList where name equals to DEFAULT_NAME
        defaultNewsSummaryShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the newsSummaryList where name equals to UPDATED_NAME
        defaultNewsSummaryShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllNewsSummariesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryRepository.saveAndFlush(newsSummary);

        // Get all the newsSummaryList where name in DEFAULT_NAME or UPDATED_NAME
        defaultNewsSummaryShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the newsSummaryList where name equals to UPDATED_NAME
        defaultNewsSummaryShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllNewsSummariesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryRepository.saveAndFlush(newsSummary);

        // Get all the newsSummaryList where name is not null
        defaultNewsSummaryShouldBeFound("name.specified=true");

        // Get all the newsSummaryList where name is null
        defaultNewsSummaryShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummariesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryRepository.saveAndFlush(newsSummary);

        // Get all the newsSummaryList where description equals to DEFAULT_DESCRIPTION
        defaultNewsSummaryShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the newsSummaryList where description equals to UPDATED_DESCRIPTION
        defaultNewsSummaryShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsSummariesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryRepository.saveAndFlush(newsSummary);

        // Get all the newsSummaryList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultNewsSummaryShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the newsSummaryList where description equals to UPDATED_DESCRIPTION
        defaultNewsSummaryShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsSummariesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryRepository.saveAndFlush(newsSummary);

        // Get all the newsSummaryList where description is not null
        defaultNewsSummaryShouldBeFound("description.specified=true");

        // Get all the newsSummaryList where description is null
        defaultNewsSummaryShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummariesByLocationIsEqualToSomething() throws Exception {
        // Initialize the database
        Location location = LocationResourceIntTest.createEntity(em);
        em.persist(location);
        em.flush();
        newsSummary.setLocation(location);
        newsSummaryRepository.saveAndFlush(newsSummary);
        Long locationId = location.getId();

        // Get all the newsSummaryList where location equals to locationId
        defaultNewsSummaryShouldBeFound("locationId.equals=" + locationId);

        // Get all the newsSummaryList where location equals to locationId + 1
        defaultNewsSummaryShouldNotBeFound("locationId.equals=" + (locationId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsSummaryShouldBeFound(String filter) throws Exception {
        restNewsSummaryMockMvc.perform(get("/api/news-summaries?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummary.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsSummaryShouldNotBeFound(String filter) throws Exception {
        restNewsSummaryMockMvc.perform(get("/api/news-summaries?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsSummary() throws Exception {
        // Get the newsSummary
        restNewsSummaryMockMvc.perform(get("/api/news-summaries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsSummary() throws Exception {
        // Initialize the database
        newsSummaryService.save(newsSummary);

        int databaseSizeBeforeUpdate = newsSummaryRepository.findAll().size();

        // Update the newsSummary
        NewsSummary updatedNewsSummary = newsSummaryRepository.findOne(newsSummary.getId());
        updatedNewsSummary
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);

        restNewsSummaryMockMvc.perform(put("/api/news-summaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsSummary)))
            .andExpect(status().isOk());

        // Validate the NewsSummary in the database
        List<NewsSummary> newsSummaryList = newsSummaryRepository.findAll();
        assertThat(newsSummaryList).hasSize(databaseSizeBeforeUpdate);
        NewsSummary testNewsSummary = newsSummaryList.get(newsSummaryList.size() - 1);
        assertThat(testNewsSummary.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testNewsSummary.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the NewsSummary in Elasticsearch
        NewsSummary newsSummaryEs = newsSummarySearchRepository.findOne(testNewsSummary.getId());
        assertThat(newsSummaryEs).isEqualToComparingFieldByField(testNewsSummary);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsSummary() throws Exception {
        int databaseSizeBeforeUpdate = newsSummaryRepository.findAll().size();

        // Create the NewsSummary

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsSummaryMockMvc.perform(put("/api/news-summaries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummary)))
            .andExpect(status().isCreated());

        // Validate the NewsSummary in the database
        List<NewsSummary> newsSummaryList = newsSummaryRepository.findAll();
        assertThat(newsSummaryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsSummary() throws Exception {
        // Initialize the database
        newsSummaryService.save(newsSummary);

        int databaseSizeBeforeDelete = newsSummaryRepository.findAll().size();

        // Get the newsSummary
        restNewsSummaryMockMvc.perform(delete("/api/news-summaries/{id}", newsSummary.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsSummaryExistsInEs = newsSummarySearchRepository.exists(newsSummary.getId());
        assertThat(newsSummaryExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsSummary> newsSummaryList = newsSummaryRepository.findAll();
        assertThat(newsSummaryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsSummary() throws Exception {
        // Initialize the database
        newsSummaryService.save(newsSummary);

        // Search the newsSummary
        restNewsSummaryMockMvc.perform(get("/api/_search/news-summaries?query=id:" + newsSummary.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummary.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsSummary.class);
        NewsSummary newsSummary1 = new NewsSummary();
        newsSummary1.setId(1L);
        NewsSummary newsSummary2 = new NewsSummary();
        newsSummary2.setId(newsSummary1.getId());
        assertThat(newsSummary1).isEqualTo(newsSummary2);
        newsSummary2.setId(2L);
        assertThat(newsSummary1).isNotEqualTo(newsSummary2);
        newsSummary1.setId(null);
        assertThat(newsSummary1).isNotEqualTo(newsSummary2);
    }
}
