package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsOriginalImage;
import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.repository.NewsOriginalImageRepository;
import biz.newparadigm.meridian.service.NewsOriginalImageService;
import biz.newparadigm.meridian.repository.search.NewsOriginalImageSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsOriginalImageCriteria;
import biz.newparadigm.meridian.service.NewsOriginalImageQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsOriginalImageResource REST controller.
 *
 * @see NewsOriginalImageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsOriginalImageResourceIntTest {

    private static final String DEFAULT_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_FILENAME = "BBBBBBBBBB";

    private static final byte[] DEFAULT_BLOB = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_BLOB = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_BLOB_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_BLOB_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_IMAGE_ORDER = 1;
    private static final Integer UPDATED_IMAGE_ORDER = 2;

    @Autowired
    private NewsOriginalImageRepository newsOriginalImageRepository;

    @Autowired
    private NewsOriginalImageService newsOriginalImageService;

    @Autowired
    private NewsOriginalImageSearchRepository newsOriginalImageSearchRepository;

    @Autowired
    private NewsOriginalImageQueryService newsOriginalImageQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsOriginalImageMockMvc;

    private NewsOriginalImage newsOriginalImage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsOriginalImageResource newsOriginalImageResource = new NewsOriginalImageResource(newsOriginalImageService, newsOriginalImageQueryService);
        this.restNewsOriginalImageMockMvc = MockMvcBuilders.standaloneSetup(newsOriginalImageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsOriginalImage createEntity(EntityManager em) {
        NewsOriginalImage newsOriginalImage = new NewsOriginalImage()
            .filename(DEFAULT_FILENAME)
            .blob(DEFAULT_BLOB)
            .blobContentType(DEFAULT_BLOB_CONTENT_TYPE)
            .description(DEFAULT_DESCRIPTION)
            .imageOrder(DEFAULT_IMAGE_ORDER);
        // Add required entity
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsOriginalImage.setNews(news);
        return newsOriginalImage;
    }

    @Before
    public void initTest() {
        newsOriginalImageSearchRepository.deleteAll();
        newsOriginalImage = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsOriginalImage() throws Exception {
        int databaseSizeBeforeCreate = newsOriginalImageRepository.findAll().size();

        // Create the NewsOriginalImage
        restNewsOriginalImageMockMvc.perform(post("/api/news-original-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsOriginalImage)))
            .andExpect(status().isCreated());

        // Validate the NewsOriginalImage in the database
        List<NewsOriginalImage> newsOriginalImageList = newsOriginalImageRepository.findAll();
        assertThat(newsOriginalImageList).hasSize(databaseSizeBeforeCreate + 1);
        NewsOriginalImage testNewsOriginalImage = newsOriginalImageList.get(newsOriginalImageList.size() - 1);
        assertThat(testNewsOriginalImage.getFilename()).isEqualTo(DEFAULT_FILENAME);
        assertThat(testNewsOriginalImage.getBlob()).isEqualTo(DEFAULT_BLOB);
        assertThat(testNewsOriginalImage.getBlobContentType()).isEqualTo(DEFAULT_BLOB_CONTENT_TYPE);
        assertThat(testNewsOriginalImage.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testNewsOriginalImage.getImageOrder()).isEqualTo(DEFAULT_IMAGE_ORDER);

        // Validate the NewsOriginalImage in Elasticsearch
        NewsOriginalImage newsOriginalImageEs = newsOriginalImageSearchRepository.findOne(testNewsOriginalImage.getId());
        assertThat(newsOriginalImageEs).isEqualToComparingFieldByField(testNewsOriginalImage);
    }

    @Test
    @Transactional
    public void createNewsOriginalImageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsOriginalImageRepository.findAll().size();

        // Create the NewsOriginalImage with an existing ID
        newsOriginalImage.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsOriginalImageMockMvc.perform(post("/api/news-original-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsOriginalImage)))
            .andExpect(status().isBadRequest());

        // Validate the NewsOriginalImage in the database
        List<NewsOriginalImage> newsOriginalImageList = newsOriginalImageRepository.findAll();
        assertThat(newsOriginalImageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFilenameIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsOriginalImageRepository.findAll().size();
        // set the field null
        newsOriginalImage.setFilename(null);

        // Create the NewsOriginalImage, which fails.

        restNewsOriginalImageMockMvc.perform(post("/api/news-original-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsOriginalImage)))
            .andExpect(status().isBadRequest());

        List<NewsOriginalImage> newsOriginalImageList = newsOriginalImageRepository.findAll();
        assertThat(newsOriginalImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBlobIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsOriginalImageRepository.findAll().size();
        // set the field null
        newsOriginalImage.setBlob(null);

        // Create the NewsOriginalImage, which fails.

        restNewsOriginalImageMockMvc.perform(post("/api/news-original-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsOriginalImage)))
            .andExpect(status().isBadRequest());

        List<NewsOriginalImage> newsOriginalImageList = newsOriginalImageRepository.findAll();
        assertThat(newsOriginalImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNewsOriginalImages() throws Exception {
        // Initialize the database
        newsOriginalImageRepository.saveAndFlush(newsOriginalImage);

        // Get all the newsOriginalImageList
        restNewsOriginalImageMockMvc.perform(get("/api/news-original-images?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsOriginalImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].imageOrder").value(hasItem(DEFAULT_IMAGE_ORDER)));
    }

    @Test
    @Transactional
    public void getNewsOriginalImage() throws Exception {
        // Initialize the database
        newsOriginalImageRepository.saveAndFlush(newsOriginalImage);

        // Get the newsOriginalImage
        restNewsOriginalImageMockMvc.perform(get("/api/news-original-images/{id}", newsOriginalImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsOriginalImage.getId().intValue()))
            .andExpect(jsonPath("$.filename").value(DEFAULT_FILENAME.toString()))
            .andExpect(jsonPath("$.blobContentType").value(DEFAULT_BLOB_CONTENT_TYPE))
            .andExpect(jsonPath("$.blob").value(Base64Utils.encodeToString(DEFAULT_BLOB)))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.imageOrder").value(DEFAULT_IMAGE_ORDER));
    }

    @Test
    @Transactional
    public void getAllNewsOriginalImagesByFilenameIsEqualToSomething() throws Exception {
        // Initialize the database
        newsOriginalImageRepository.saveAndFlush(newsOriginalImage);

        // Get all the newsOriginalImageList where filename equals to DEFAULT_FILENAME
        defaultNewsOriginalImageShouldBeFound("filename.equals=" + DEFAULT_FILENAME);

        // Get all the newsOriginalImageList where filename equals to UPDATED_FILENAME
        defaultNewsOriginalImageShouldNotBeFound("filename.equals=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllNewsOriginalImagesByFilenameIsInShouldWork() throws Exception {
        // Initialize the database
        newsOriginalImageRepository.saveAndFlush(newsOriginalImage);

        // Get all the newsOriginalImageList where filename in DEFAULT_FILENAME or UPDATED_FILENAME
        defaultNewsOriginalImageShouldBeFound("filename.in=" + DEFAULT_FILENAME + "," + UPDATED_FILENAME);

        // Get all the newsOriginalImageList where filename equals to UPDATED_FILENAME
        defaultNewsOriginalImageShouldNotBeFound("filename.in=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllNewsOriginalImagesByFilenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsOriginalImageRepository.saveAndFlush(newsOriginalImage);

        // Get all the newsOriginalImageList where filename is not null
        defaultNewsOriginalImageShouldBeFound("filename.specified=true");

        // Get all the newsOriginalImageList where filename is null
        defaultNewsOriginalImageShouldNotBeFound("filename.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsOriginalImagesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        newsOriginalImageRepository.saveAndFlush(newsOriginalImage);

        // Get all the newsOriginalImageList where description equals to DEFAULT_DESCRIPTION
        defaultNewsOriginalImageShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the newsOriginalImageList where description equals to UPDATED_DESCRIPTION
        defaultNewsOriginalImageShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsOriginalImagesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        newsOriginalImageRepository.saveAndFlush(newsOriginalImage);

        // Get all the newsOriginalImageList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultNewsOriginalImageShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the newsOriginalImageList where description equals to UPDATED_DESCRIPTION
        defaultNewsOriginalImageShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsOriginalImagesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsOriginalImageRepository.saveAndFlush(newsOriginalImage);

        // Get all the newsOriginalImageList where description is not null
        defaultNewsOriginalImageShouldBeFound("description.specified=true");

        // Get all the newsOriginalImageList where description is null
        defaultNewsOriginalImageShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsOriginalImagesByImageOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        newsOriginalImageRepository.saveAndFlush(newsOriginalImage);

        // Get all the newsOriginalImageList where imageOrder equals to DEFAULT_IMAGE_ORDER
        defaultNewsOriginalImageShouldBeFound("imageOrder.equals=" + DEFAULT_IMAGE_ORDER);

        // Get all the newsOriginalImageList where imageOrder equals to UPDATED_IMAGE_ORDER
        defaultNewsOriginalImageShouldNotBeFound("imageOrder.equals=" + UPDATED_IMAGE_ORDER);
    }

    @Test
    @Transactional
    public void getAllNewsOriginalImagesByImageOrderIsInShouldWork() throws Exception {
        // Initialize the database
        newsOriginalImageRepository.saveAndFlush(newsOriginalImage);

        // Get all the newsOriginalImageList where imageOrder in DEFAULT_IMAGE_ORDER or UPDATED_IMAGE_ORDER
        defaultNewsOriginalImageShouldBeFound("imageOrder.in=" + DEFAULT_IMAGE_ORDER + "," + UPDATED_IMAGE_ORDER);

        // Get all the newsOriginalImageList where imageOrder equals to UPDATED_IMAGE_ORDER
        defaultNewsOriginalImageShouldNotBeFound("imageOrder.in=" + UPDATED_IMAGE_ORDER);
    }

    @Test
    @Transactional
    public void getAllNewsOriginalImagesByImageOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsOriginalImageRepository.saveAndFlush(newsOriginalImage);

        // Get all the newsOriginalImageList where imageOrder is not null
        defaultNewsOriginalImageShouldBeFound("imageOrder.specified=true");

        // Get all the newsOriginalImageList where imageOrder is null
        defaultNewsOriginalImageShouldNotBeFound("imageOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsOriginalImagesByImageOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        newsOriginalImageRepository.saveAndFlush(newsOriginalImage);

        // Get all the newsOriginalImageList where imageOrder greater than or equals to DEFAULT_IMAGE_ORDER
        defaultNewsOriginalImageShouldBeFound("imageOrder.greaterOrEqualThan=" + DEFAULT_IMAGE_ORDER);

        // Get all the newsOriginalImageList where imageOrder greater than or equals to UPDATED_IMAGE_ORDER
        defaultNewsOriginalImageShouldNotBeFound("imageOrder.greaterOrEqualThan=" + UPDATED_IMAGE_ORDER);
    }

    @Test
    @Transactional
    public void getAllNewsOriginalImagesByImageOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        newsOriginalImageRepository.saveAndFlush(newsOriginalImage);

        // Get all the newsOriginalImageList where imageOrder less than or equals to DEFAULT_IMAGE_ORDER
        defaultNewsOriginalImageShouldNotBeFound("imageOrder.lessThan=" + DEFAULT_IMAGE_ORDER);

        // Get all the newsOriginalImageList where imageOrder less than or equals to UPDATED_IMAGE_ORDER
        defaultNewsOriginalImageShouldBeFound("imageOrder.lessThan=" + UPDATED_IMAGE_ORDER);
    }


    @Test
    @Transactional
    public void getAllNewsOriginalImagesByNewsIsEqualToSomething() throws Exception {
        // Initialize the database
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsOriginalImage.setNews(news);
        newsOriginalImageRepository.saveAndFlush(newsOriginalImage);
        Long newsId = news.getId();

        // Get all the newsOriginalImageList where news equals to newsId
        defaultNewsOriginalImageShouldBeFound("newsId.equals=" + newsId);

        // Get all the newsOriginalImageList where news equals to newsId + 1
        defaultNewsOriginalImageShouldNotBeFound("newsId.equals=" + (newsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsOriginalImageShouldBeFound(String filter) throws Exception {
        restNewsOriginalImageMockMvc.perform(get("/api/news-original-images?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsOriginalImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].imageOrder").value(hasItem(DEFAULT_IMAGE_ORDER)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsOriginalImageShouldNotBeFound(String filter) throws Exception {
        restNewsOriginalImageMockMvc.perform(get("/api/news-original-images?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsOriginalImage() throws Exception {
        // Get the newsOriginalImage
        restNewsOriginalImageMockMvc.perform(get("/api/news-original-images/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsOriginalImage() throws Exception {
        // Initialize the database
        newsOriginalImageService.save(newsOriginalImage);

        int databaseSizeBeforeUpdate = newsOriginalImageRepository.findAll().size();

        // Update the newsOriginalImage
        NewsOriginalImage updatedNewsOriginalImage = newsOriginalImageRepository.findOne(newsOriginalImage.getId());
        updatedNewsOriginalImage
            .filename(UPDATED_FILENAME)
            .blob(UPDATED_BLOB)
            .blobContentType(UPDATED_BLOB_CONTENT_TYPE)
            .description(UPDATED_DESCRIPTION)
            .imageOrder(UPDATED_IMAGE_ORDER);

        restNewsOriginalImageMockMvc.perform(put("/api/news-original-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsOriginalImage)))
            .andExpect(status().isOk());

        // Validate the NewsOriginalImage in the database
        List<NewsOriginalImage> newsOriginalImageList = newsOriginalImageRepository.findAll();
        assertThat(newsOriginalImageList).hasSize(databaseSizeBeforeUpdate);
        NewsOriginalImage testNewsOriginalImage = newsOriginalImageList.get(newsOriginalImageList.size() - 1);
        assertThat(testNewsOriginalImage.getFilename()).isEqualTo(UPDATED_FILENAME);
        assertThat(testNewsOriginalImage.getBlob()).isEqualTo(UPDATED_BLOB);
        assertThat(testNewsOriginalImage.getBlobContentType()).isEqualTo(UPDATED_BLOB_CONTENT_TYPE);
        assertThat(testNewsOriginalImage.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testNewsOriginalImage.getImageOrder()).isEqualTo(UPDATED_IMAGE_ORDER);

        // Validate the NewsOriginalImage in Elasticsearch
        NewsOriginalImage newsOriginalImageEs = newsOriginalImageSearchRepository.findOne(testNewsOriginalImage.getId());
        assertThat(newsOriginalImageEs).isEqualToComparingFieldByField(testNewsOriginalImage);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsOriginalImage() throws Exception {
        int databaseSizeBeforeUpdate = newsOriginalImageRepository.findAll().size();

        // Create the NewsOriginalImage

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsOriginalImageMockMvc.perform(put("/api/news-original-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsOriginalImage)))
            .andExpect(status().isCreated());

        // Validate the NewsOriginalImage in the database
        List<NewsOriginalImage> newsOriginalImageList = newsOriginalImageRepository.findAll();
        assertThat(newsOriginalImageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsOriginalImage() throws Exception {
        // Initialize the database
        newsOriginalImageService.save(newsOriginalImage);

        int databaseSizeBeforeDelete = newsOriginalImageRepository.findAll().size();

        // Get the newsOriginalImage
        restNewsOriginalImageMockMvc.perform(delete("/api/news-original-images/{id}", newsOriginalImage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsOriginalImageExistsInEs = newsOriginalImageSearchRepository.exists(newsOriginalImage.getId());
        assertThat(newsOriginalImageExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsOriginalImage> newsOriginalImageList = newsOriginalImageRepository.findAll();
        assertThat(newsOriginalImageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsOriginalImage() throws Exception {
        // Initialize the database
        newsOriginalImageService.save(newsOriginalImage);

        // Search the newsOriginalImage
        restNewsOriginalImageMockMvc.perform(get("/api/_search/news-original-images?query=id:" + newsOriginalImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsOriginalImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].imageOrder").value(hasItem(DEFAULT_IMAGE_ORDER)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsOriginalImage.class);
        NewsOriginalImage newsOriginalImage1 = new NewsOriginalImage();
        newsOriginalImage1.setId(1L);
        NewsOriginalImage newsOriginalImage2 = new NewsOriginalImage();
        newsOriginalImage2.setId(newsOriginalImage1.getId());
        assertThat(newsOriginalImage1).isEqualTo(newsOriginalImage2);
        newsOriginalImage2.setId(2L);
        assertThat(newsOriginalImage1).isNotEqualTo(newsOriginalImage2);
        newsOriginalImage1.setId(null);
        assertThat(newsOriginalImage1).isNotEqualTo(newsOriginalImage2);
    }
}
