package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.SiteChatgroupComment;
import biz.newparadigm.meridian.domain.SiteChatgroup;
import biz.newparadigm.meridian.domain.SiteChatgroupComment;
import biz.newparadigm.meridian.repository.SiteChatgroupCommentRepository;
import biz.newparadigm.meridian.service.SiteChatgroupCommentService;
import biz.newparadigm.meridian.repository.search.SiteChatgroupCommentSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.SiteChatgroupCommentCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupCommentQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.sameInstant;
import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SiteChatgroupCommentResource REST controller.
 *
 * @see SiteChatgroupCommentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class SiteChatgroupCommentResourceIntTest {

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATETIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATETIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private SiteChatgroupCommentRepository siteChatgroupCommentRepository;

    @Autowired
    private SiteChatgroupCommentService siteChatgroupCommentService;

    @Autowired
    private SiteChatgroupCommentSearchRepository siteChatgroupCommentSearchRepository;

    @Autowired
    private SiteChatgroupCommentQueryService siteChatgroupCommentQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSiteChatgroupCommentMockMvc;

    private SiteChatgroupComment siteChatgroupComment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SiteChatgroupCommentResource siteChatgroupCommentResource = new SiteChatgroupCommentResource(siteChatgroupCommentService, siteChatgroupCommentQueryService);
        this.restSiteChatgroupCommentMockMvc = MockMvcBuilders.standaloneSetup(siteChatgroupCommentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SiteChatgroupComment createEntity(EntityManager em) {
        SiteChatgroupComment siteChatgroupComment = new SiteChatgroupComment()
            .userId(DEFAULT_USER_ID)
            .comment(DEFAULT_COMMENT)
            .datetime(DEFAULT_DATETIME);
        // Add required entity
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupComment.setSiteChatgroup(siteChatgroup);
        return siteChatgroupComment;
    }

    @Before
    public void initTest() {
        siteChatgroupCommentSearchRepository.deleteAll();
        siteChatgroupComment = createEntity(em);
    }

    @Test
    @Transactional
    public void createSiteChatgroupComment() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupCommentRepository.findAll().size();

        // Create the SiteChatgroupComment
        restSiteChatgroupCommentMockMvc.perform(post("/api/site-chatgroup-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupComment)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupComment in the database
        List<SiteChatgroupComment> siteChatgroupCommentList = siteChatgroupCommentRepository.findAll();
        assertThat(siteChatgroupCommentList).hasSize(databaseSizeBeforeCreate + 1);
        SiteChatgroupComment testSiteChatgroupComment = siteChatgroupCommentList.get(siteChatgroupCommentList.size() - 1);
        assertThat(testSiteChatgroupComment.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testSiteChatgroupComment.getComment()).isEqualTo(DEFAULT_COMMENT);
        assertThat(testSiteChatgroupComment.getDatetime()).isEqualTo(DEFAULT_DATETIME);

        // Validate the SiteChatgroupComment in Elasticsearch
        SiteChatgroupComment siteChatgroupCommentEs = siteChatgroupCommentSearchRepository.findOne(testSiteChatgroupComment.getId());
        assertThat(siteChatgroupCommentEs).isEqualToComparingFieldByField(testSiteChatgroupComment);
    }

    @Test
    @Transactional
    public void createSiteChatgroupCommentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupCommentRepository.findAll().size();

        // Create the SiteChatgroupComment with an existing ID
        siteChatgroupComment.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSiteChatgroupCommentMockMvc.perform(post("/api/site-chatgroup-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupComment)))
            .andExpect(status().isBadRequest());

        // Validate the SiteChatgroupComment in the database
        List<SiteChatgroupComment> siteChatgroupCommentList = siteChatgroupCommentRepository.findAll();
        assertThat(siteChatgroupCommentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupCommentRepository.findAll().size();
        // set the field null
        siteChatgroupComment.setUserId(null);

        // Create the SiteChatgroupComment, which fails.

        restSiteChatgroupCommentMockMvc.perform(post("/api/site-chatgroup-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupComment)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupComment> siteChatgroupCommentList = siteChatgroupCommentRepository.findAll();
        assertThat(siteChatgroupCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCommentIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupCommentRepository.findAll().size();
        // set the field null
        siteChatgroupComment.setComment(null);

        // Create the SiteChatgroupComment, which fails.

        restSiteChatgroupCommentMockMvc.perform(post("/api/site-chatgroup-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupComment)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupComment> siteChatgroupCommentList = siteChatgroupCommentRepository.findAll();
        assertThat(siteChatgroupCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatetimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupCommentRepository.findAll().size();
        // set the field null
        siteChatgroupComment.setDatetime(null);

        // Create the SiteChatgroupComment, which fails.

        restSiteChatgroupCommentMockMvc.perform(post("/api/site-chatgroup-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupComment)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupComment> siteChatgroupCommentList = siteChatgroupCommentRepository.findAll();
        assertThat(siteChatgroupCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupComments() throws Exception {
        // Initialize the database
        siteChatgroupCommentRepository.saveAndFlush(siteChatgroupComment);

        // Get all the siteChatgroupCommentList
        restSiteChatgroupCommentMockMvc.perform(get("/api/site-chatgroup-comments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
            .andExpect(jsonPath("$.[*].datetime").value(hasItem(sameInstant(DEFAULT_DATETIME))));
    }

    @Test
    @Transactional
    public void getSiteChatgroupComment() throws Exception {
        // Initialize the database
        siteChatgroupCommentRepository.saveAndFlush(siteChatgroupComment);

        // Get the siteChatgroupComment
        restSiteChatgroupCommentMockMvc.perform(get("/api/site-chatgroup-comments/{id}", siteChatgroupComment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(siteChatgroupComment.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.toString()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()))
            .andExpect(jsonPath("$.datetime").value(sameInstant(DEFAULT_DATETIME)));
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupCommentsByUserIdIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupCommentRepository.saveAndFlush(siteChatgroupComment);

        // Get all the siteChatgroupCommentList where userId equals to DEFAULT_USER_ID
        defaultSiteChatgroupCommentShouldBeFound("userId.equals=" + DEFAULT_USER_ID);

        // Get all the siteChatgroupCommentList where userId equals to UPDATED_USER_ID
        defaultSiteChatgroupCommentShouldNotBeFound("userId.equals=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupCommentsByUserIdIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupCommentRepository.saveAndFlush(siteChatgroupComment);

        // Get all the siteChatgroupCommentList where userId in DEFAULT_USER_ID or UPDATED_USER_ID
        defaultSiteChatgroupCommentShouldBeFound("userId.in=" + DEFAULT_USER_ID + "," + UPDATED_USER_ID);

        // Get all the siteChatgroupCommentList where userId equals to UPDATED_USER_ID
        defaultSiteChatgroupCommentShouldNotBeFound("userId.in=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupCommentsByUserIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupCommentRepository.saveAndFlush(siteChatgroupComment);

        // Get all the siteChatgroupCommentList where userId is not null
        defaultSiteChatgroupCommentShouldBeFound("userId.specified=true");

        // Get all the siteChatgroupCommentList where userId is null
        defaultSiteChatgroupCommentShouldNotBeFound("userId.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupCommentsByCommentIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupCommentRepository.saveAndFlush(siteChatgroupComment);

        // Get all the siteChatgroupCommentList where comment equals to DEFAULT_COMMENT
        defaultSiteChatgroupCommentShouldBeFound("comment.equals=" + DEFAULT_COMMENT);

        // Get all the siteChatgroupCommentList where comment equals to UPDATED_COMMENT
        defaultSiteChatgroupCommentShouldNotBeFound("comment.equals=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupCommentsByCommentIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupCommentRepository.saveAndFlush(siteChatgroupComment);

        // Get all the siteChatgroupCommentList where comment in DEFAULT_COMMENT or UPDATED_COMMENT
        defaultSiteChatgroupCommentShouldBeFound("comment.in=" + DEFAULT_COMMENT + "," + UPDATED_COMMENT);

        // Get all the siteChatgroupCommentList where comment equals to UPDATED_COMMENT
        defaultSiteChatgroupCommentShouldNotBeFound("comment.in=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupCommentsByCommentIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupCommentRepository.saveAndFlush(siteChatgroupComment);

        // Get all the siteChatgroupCommentList where comment is not null
        defaultSiteChatgroupCommentShouldBeFound("comment.specified=true");

        // Get all the siteChatgroupCommentList where comment is null
        defaultSiteChatgroupCommentShouldNotBeFound("comment.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupCommentsByDatetimeIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupCommentRepository.saveAndFlush(siteChatgroupComment);

        // Get all the siteChatgroupCommentList where datetime equals to DEFAULT_DATETIME
        defaultSiteChatgroupCommentShouldBeFound("datetime.equals=" + DEFAULT_DATETIME);

        // Get all the siteChatgroupCommentList where datetime equals to UPDATED_DATETIME
        defaultSiteChatgroupCommentShouldNotBeFound("datetime.equals=" + UPDATED_DATETIME);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupCommentsByDatetimeIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupCommentRepository.saveAndFlush(siteChatgroupComment);

        // Get all the siteChatgroupCommentList where datetime in DEFAULT_DATETIME or UPDATED_DATETIME
        defaultSiteChatgroupCommentShouldBeFound("datetime.in=" + DEFAULT_DATETIME + "," + UPDATED_DATETIME);

        // Get all the siteChatgroupCommentList where datetime equals to UPDATED_DATETIME
        defaultSiteChatgroupCommentShouldNotBeFound("datetime.in=" + UPDATED_DATETIME);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupCommentsByDatetimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupCommentRepository.saveAndFlush(siteChatgroupComment);

        // Get all the siteChatgroupCommentList where datetime is not null
        defaultSiteChatgroupCommentShouldBeFound("datetime.specified=true");

        // Get all the siteChatgroupCommentList where datetime is null
        defaultSiteChatgroupCommentShouldNotBeFound("datetime.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupCommentsByDatetimeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupCommentRepository.saveAndFlush(siteChatgroupComment);

        // Get all the siteChatgroupCommentList where datetime greater than or equals to DEFAULT_DATETIME
        defaultSiteChatgroupCommentShouldBeFound("datetime.greaterOrEqualThan=" + DEFAULT_DATETIME);

        // Get all the siteChatgroupCommentList where datetime greater than or equals to UPDATED_DATETIME
        defaultSiteChatgroupCommentShouldNotBeFound("datetime.greaterOrEqualThan=" + UPDATED_DATETIME);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupCommentsByDatetimeIsLessThanSomething() throws Exception {
        // Initialize the database
        siteChatgroupCommentRepository.saveAndFlush(siteChatgroupComment);

        // Get all the siteChatgroupCommentList where datetime less than or equals to DEFAULT_DATETIME
        defaultSiteChatgroupCommentShouldNotBeFound("datetime.lessThan=" + DEFAULT_DATETIME);

        // Get all the siteChatgroupCommentList where datetime less than or equals to UPDATED_DATETIME
        defaultSiteChatgroupCommentShouldBeFound("datetime.lessThan=" + UPDATED_DATETIME);
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupCommentsBySiteChatgroupIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupComment.setSiteChatgroup(siteChatgroup);
        siteChatgroupCommentRepository.saveAndFlush(siteChatgroupComment);
        Long siteChatgroupId = siteChatgroup.getId();

        // Get all the siteChatgroupCommentList where siteChatgroup equals to siteChatgroupId
        defaultSiteChatgroupCommentShouldBeFound("siteChatgroupId.equals=" + siteChatgroupId);

        // Get all the siteChatgroupCommentList where siteChatgroup equals to siteChatgroupId + 1
        defaultSiteChatgroupCommentShouldNotBeFound("siteChatgroupId.equals=" + (siteChatgroupId + 1));
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupCommentsByParentCommentIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroupComment parentComment = SiteChatgroupCommentResourceIntTest.createEntity(em);
        em.persist(parentComment);
        em.flush();
        siteChatgroupComment.setParentComment(parentComment);
        siteChatgroupCommentRepository.saveAndFlush(siteChatgroupComment);
        Long parentCommentId = parentComment.getId();

        // Get all the siteChatgroupCommentList where parentComment equals to parentCommentId
        defaultSiteChatgroupCommentShouldBeFound("parentCommentId.equals=" + parentCommentId);

        // Get all the siteChatgroupCommentList where parentComment equals to parentCommentId + 1
        defaultSiteChatgroupCommentShouldNotBeFound("parentCommentId.equals=" + (parentCommentId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSiteChatgroupCommentShouldBeFound(String filter) throws Exception {
        restSiteChatgroupCommentMockMvc.perform(get("/api/site-chatgroup-comments?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
            .andExpect(jsonPath("$.[*].datetime").value(hasItem(sameInstant(DEFAULT_DATETIME))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSiteChatgroupCommentShouldNotBeFound(String filter) throws Exception {
        restSiteChatgroupCommentMockMvc.perform(get("/api/site-chatgroup-comments?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingSiteChatgroupComment() throws Exception {
        // Get the siteChatgroupComment
        restSiteChatgroupCommentMockMvc.perform(get("/api/site-chatgroup-comments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSiteChatgroupComment() throws Exception {
        // Initialize the database
        siteChatgroupCommentService.save(siteChatgroupComment);

        int databaseSizeBeforeUpdate = siteChatgroupCommentRepository.findAll().size();

        // Update the siteChatgroupComment
        SiteChatgroupComment updatedSiteChatgroupComment = siteChatgroupCommentRepository.findOne(siteChatgroupComment.getId());
        updatedSiteChatgroupComment
            .userId(UPDATED_USER_ID)
            .comment(UPDATED_COMMENT)
            .datetime(UPDATED_DATETIME);

        restSiteChatgroupCommentMockMvc.perform(put("/api/site-chatgroup-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSiteChatgroupComment)))
            .andExpect(status().isOk());

        // Validate the SiteChatgroupComment in the database
        List<SiteChatgroupComment> siteChatgroupCommentList = siteChatgroupCommentRepository.findAll();
        assertThat(siteChatgroupCommentList).hasSize(databaseSizeBeforeUpdate);
        SiteChatgroupComment testSiteChatgroupComment = siteChatgroupCommentList.get(siteChatgroupCommentList.size() - 1);
        assertThat(testSiteChatgroupComment.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testSiteChatgroupComment.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testSiteChatgroupComment.getDatetime()).isEqualTo(UPDATED_DATETIME);

        // Validate the SiteChatgroupComment in Elasticsearch
        SiteChatgroupComment siteChatgroupCommentEs = siteChatgroupCommentSearchRepository.findOne(testSiteChatgroupComment.getId());
        assertThat(siteChatgroupCommentEs).isEqualToComparingFieldByField(testSiteChatgroupComment);
    }

    @Test
    @Transactional
    public void updateNonExistingSiteChatgroupComment() throws Exception {
        int databaseSizeBeforeUpdate = siteChatgroupCommentRepository.findAll().size();

        // Create the SiteChatgroupComment

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSiteChatgroupCommentMockMvc.perform(put("/api/site-chatgroup-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupComment)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupComment in the database
        List<SiteChatgroupComment> siteChatgroupCommentList = siteChatgroupCommentRepository.findAll();
        assertThat(siteChatgroupCommentList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSiteChatgroupComment() throws Exception {
        // Initialize the database
        siteChatgroupCommentService.save(siteChatgroupComment);

        int databaseSizeBeforeDelete = siteChatgroupCommentRepository.findAll().size();

        // Get the siteChatgroupComment
        restSiteChatgroupCommentMockMvc.perform(delete("/api/site-chatgroup-comments/{id}", siteChatgroupComment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean siteChatgroupCommentExistsInEs = siteChatgroupCommentSearchRepository.exists(siteChatgroupComment.getId());
        assertThat(siteChatgroupCommentExistsInEs).isFalse();

        // Validate the database is empty
        List<SiteChatgroupComment> siteChatgroupCommentList = siteChatgroupCommentRepository.findAll();
        assertThat(siteChatgroupCommentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSiteChatgroupComment() throws Exception {
        // Initialize the database
        siteChatgroupCommentService.save(siteChatgroupComment);

        // Search the siteChatgroupComment
        restSiteChatgroupCommentMockMvc.perform(get("/api/_search/site-chatgroup-comments?query=id:" + siteChatgroupComment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
            .andExpect(jsonPath("$.[*].datetime").value(hasItem(sameInstant(DEFAULT_DATETIME))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SiteChatgroupComment.class);
        SiteChatgroupComment siteChatgroupComment1 = new SiteChatgroupComment();
        siteChatgroupComment1.setId(1L);
        SiteChatgroupComment siteChatgroupComment2 = new SiteChatgroupComment();
        siteChatgroupComment2.setId(siteChatgroupComment1.getId());
        assertThat(siteChatgroupComment1).isEqualTo(siteChatgroupComment2);
        siteChatgroupComment2.setId(2L);
        assertThat(siteChatgroupComment1).isNotEqualTo(siteChatgroupComment2);
        siteChatgroupComment1.setId(null);
        assertThat(siteChatgroupComment1).isNotEqualTo(siteChatgroupComment2);
    }
}
