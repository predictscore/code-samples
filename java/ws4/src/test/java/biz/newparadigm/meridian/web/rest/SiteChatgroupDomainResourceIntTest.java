package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.SiteChatgroupDomain;
import biz.newparadigm.meridian.domain.SiteChatgroup;
import biz.newparadigm.meridian.repository.SiteChatgroupDomainRepository;
import biz.newparadigm.meridian.service.SiteChatgroupDomainService;
import biz.newparadigm.meridian.repository.search.SiteChatgroupDomainSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.SiteChatgroupDomainCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupDomainQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.sameInstant;
import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SiteChatgroupDomainResource REST controller.
 *
 * @see SiteChatgroupDomainResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class SiteChatgroupDomainResourceIntTest {

    private static final String DEFAULT_DOMAIN_ID = "AAAAAAAAAA";
    private static final String UPDATED_DOMAIN_ID = "BBBBBBBBBB";

    private static final String DEFAULT_DOMAIN_INFO = "AAAAAAAAAA";
    private static final String UPDATED_DOMAIN_INFO = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_FIRST_USED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FIRST_USED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_LAST_USED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_USED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private SiteChatgroupDomainRepository siteChatgroupDomainRepository;

    @Autowired
    private SiteChatgroupDomainService siteChatgroupDomainService;

    @Autowired
    private SiteChatgroupDomainSearchRepository siteChatgroupDomainSearchRepository;

    @Autowired
    private SiteChatgroupDomainQueryService siteChatgroupDomainQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSiteChatgroupDomainMockMvc;

    private SiteChatgroupDomain siteChatgroupDomain;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SiteChatgroupDomainResource siteChatgroupDomainResource = new SiteChatgroupDomainResource(siteChatgroupDomainService, siteChatgroupDomainQueryService);
        this.restSiteChatgroupDomainMockMvc = MockMvcBuilders.standaloneSetup(siteChatgroupDomainResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SiteChatgroupDomain createEntity(EntityManager em) {
        SiteChatgroupDomain siteChatgroupDomain = new SiteChatgroupDomain()
            .domainId(DEFAULT_DOMAIN_ID)
            .domainInfo(DEFAULT_DOMAIN_INFO)
            .firstUsedDate(DEFAULT_FIRST_USED_DATE)
            .lastUsedDate(DEFAULT_LAST_USED_DATE);
        // Add required entity
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupDomain.setSiteChatgroup(siteChatgroup);
        return siteChatgroupDomain;
    }

    @Before
    public void initTest() {
        siteChatgroupDomainSearchRepository.deleteAll();
        siteChatgroupDomain = createEntity(em);
    }

    @Test
    @Transactional
    public void createSiteChatgroupDomain() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupDomainRepository.findAll().size();

        // Create the SiteChatgroupDomain
        restSiteChatgroupDomainMockMvc.perform(post("/api/site-chatgroup-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupDomain)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupDomain in the database
        List<SiteChatgroupDomain> siteChatgroupDomainList = siteChatgroupDomainRepository.findAll();
        assertThat(siteChatgroupDomainList).hasSize(databaseSizeBeforeCreate + 1);
        SiteChatgroupDomain testSiteChatgroupDomain = siteChatgroupDomainList.get(siteChatgroupDomainList.size() - 1);
        assertThat(testSiteChatgroupDomain.getDomainId()).isEqualTo(DEFAULT_DOMAIN_ID);
        assertThat(testSiteChatgroupDomain.getDomainInfo()).isEqualTo(DEFAULT_DOMAIN_INFO);
        assertThat(testSiteChatgroupDomain.getFirstUsedDate()).isEqualTo(DEFAULT_FIRST_USED_DATE);
        assertThat(testSiteChatgroupDomain.getLastUsedDate()).isEqualTo(DEFAULT_LAST_USED_DATE);

        // Validate the SiteChatgroupDomain in Elasticsearch
        SiteChatgroupDomain siteChatgroupDomainEs = siteChatgroupDomainSearchRepository.findOne(testSiteChatgroupDomain.getId());
        assertThat(siteChatgroupDomainEs).isEqualToComparingFieldByField(testSiteChatgroupDomain);
    }

    @Test
    @Transactional
    public void createSiteChatgroupDomainWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupDomainRepository.findAll().size();

        // Create the SiteChatgroupDomain with an existing ID
        siteChatgroupDomain.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSiteChatgroupDomainMockMvc.perform(post("/api/site-chatgroup-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupDomain)))
            .andExpect(status().isBadRequest());

        // Validate the SiteChatgroupDomain in the database
        List<SiteChatgroupDomain> siteChatgroupDomainList = siteChatgroupDomainRepository.findAll();
        assertThat(siteChatgroupDomainList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDomainIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupDomainRepository.findAll().size();
        // set the field null
        siteChatgroupDomain.setDomainId(null);

        // Create the SiteChatgroupDomain, which fails.

        restSiteChatgroupDomainMockMvc.perform(post("/api/site-chatgroup-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupDomain)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupDomain> siteChatgroupDomainList = siteChatgroupDomainRepository.findAll();
        assertThat(siteChatgroupDomainList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDomainInfoIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupDomainRepository.findAll().size();
        // set the field null
        siteChatgroupDomain.setDomainInfo(null);

        // Create the SiteChatgroupDomain, which fails.

        restSiteChatgroupDomainMockMvc.perform(post("/api/site-chatgroup-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupDomain)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupDomain> siteChatgroupDomainList = siteChatgroupDomainRepository.findAll();
        assertThat(siteChatgroupDomainList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFirstUsedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupDomainRepository.findAll().size();
        // set the field null
        siteChatgroupDomain.setFirstUsedDate(null);

        // Create the SiteChatgroupDomain, which fails.

        restSiteChatgroupDomainMockMvc.perform(post("/api/site-chatgroup-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupDomain)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupDomain> siteChatgroupDomainList = siteChatgroupDomainRepository.findAll();
        assertThat(siteChatgroupDomainList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomains() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList
        restSiteChatgroupDomainMockMvc.perform(get("/api/site-chatgroup-domains?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupDomain.getId().intValue())))
            .andExpect(jsonPath("$.[*].domainId").value(hasItem(DEFAULT_DOMAIN_ID.toString())))
            .andExpect(jsonPath("$.[*].domainInfo").value(hasItem(DEFAULT_DOMAIN_INFO.toString())))
            .andExpect(jsonPath("$.[*].firstUsedDate").value(hasItem(sameInstant(DEFAULT_FIRST_USED_DATE))))
            .andExpect(jsonPath("$.[*].lastUsedDate").value(hasItem(sameInstant(DEFAULT_LAST_USED_DATE))));
    }

    @Test
    @Transactional
    public void getSiteChatgroupDomain() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get the siteChatgroupDomain
        restSiteChatgroupDomainMockMvc.perform(get("/api/site-chatgroup-domains/{id}", siteChatgroupDomain.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(siteChatgroupDomain.getId().intValue()))
            .andExpect(jsonPath("$.domainId").value(DEFAULT_DOMAIN_ID.toString()))
            .andExpect(jsonPath("$.domainInfo").value(DEFAULT_DOMAIN_INFO.toString()))
            .andExpect(jsonPath("$.firstUsedDate").value(sameInstant(DEFAULT_FIRST_USED_DATE)))
            .andExpect(jsonPath("$.lastUsedDate").value(sameInstant(DEFAULT_LAST_USED_DATE)));
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByDomainIdIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where domainId equals to DEFAULT_DOMAIN_ID
        defaultSiteChatgroupDomainShouldBeFound("domainId.equals=" + DEFAULT_DOMAIN_ID);

        // Get all the siteChatgroupDomainList where domainId equals to UPDATED_DOMAIN_ID
        defaultSiteChatgroupDomainShouldNotBeFound("domainId.equals=" + UPDATED_DOMAIN_ID);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByDomainIdIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where domainId in DEFAULT_DOMAIN_ID or UPDATED_DOMAIN_ID
        defaultSiteChatgroupDomainShouldBeFound("domainId.in=" + DEFAULT_DOMAIN_ID + "," + UPDATED_DOMAIN_ID);

        // Get all the siteChatgroupDomainList where domainId equals to UPDATED_DOMAIN_ID
        defaultSiteChatgroupDomainShouldNotBeFound("domainId.in=" + UPDATED_DOMAIN_ID);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByDomainIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where domainId is not null
        defaultSiteChatgroupDomainShouldBeFound("domainId.specified=true");

        // Get all the siteChatgroupDomainList where domainId is null
        defaultSiteChatgroupDomainShouldNotBeFound("domainId.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByDomainInfoIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where domainInfo equals to DEFAULT_DOMAIN_INFO
        defaultSiteChatgroupDomainShouldBeFound("domainInfo.equals=" + DEFAULT_DOMAIN_INFO);

        // Get all the siteChatgroupDomainList where domainInfo equals to UPDATED_DOMAIN_INFO
        defaultSiteChatgroupDomainShouldNotBeFound("domainInfo.equals=" + UPDATED_DOMAIN_INFO);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByDomainInfoIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where domainInfo in DEFAULT_DOMAIN_INFO or UPDATED_DOMAIN_INFO
        defaultSiteChatgroupDomainShouldBeFound("domainInfo.in=" + DEFAULT_DOMAIN_INFO + "," + UPDATED_DOMAIN_INFO);

        // Get all the siteChatgroupDomainList where domainInfo equals to UPDATED_DOMAIN_INFO
        defaultSiteChatgroupDomainShouldNotBeFound("domainInfo.in=" + UPDATED_DOMAIN_INFO);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByDomainInfoIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where domainInfo is not null
        defaultSiteChatgroupDomainShouldBeFound("domainInfo.specified=true");

        // Get all the siteChatgroupDomainList where domainInfo is null
        defaultSiteChatgroupDomainShouldNotBeFound("domainInfo.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByFirstUsedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where firstUsedDate equals to DEFAULT_FIRST_USED_DATE
        defaultSiteChatgroupDomainShouldBeFound("firstUsedDate.equals=" + DEFAULT_FIRST_USED_DATE);

        // Get all the siteChatgroupDomainList where firstUsedDate equals to UPDATED_FIRST_USED_DATE
        defaultSiteChatgroupDomainShouldNotBeFound("firstUsedDate.equals=" + UPDATED_FIRST_USED_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByFirstUsedDateIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where firstUsedDate in DEFAULT_FIRST_USED_DATE or UPDATED_FIRST_USED_DATE
        defaultSiteChatgroupDomainShouldBeFound("firstUsedDate.in=" + DEFAULT_FIRST_USED_DATE + "," + UPDATED_FIRST_USED_DATE);

        // Get all the siteChatgroupDomainList where firstUsedDate equals to UPDATED_FIRST_USED_DATE
        defaultSiteChatgroupDomainShouldNotBeFound("firstUsedDate.in=" + UPDATED_FIRST_USED_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByFirstUsedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where firstUsedDate is not null
        defaultSiteChatgroupDomainShouldBeFound("firstUsedDate.specified=true");

        // Get all the siteChatgroupDomainList where firstUsedDate is null
        defaultSiteChatgroupDomainShouldNotBeFound("firstUsedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByFirstUsedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where firstUsedDate greater than or equals to DEFAULT_FIRST_USED_DATE
        defaultSiteChatgroupDomainShouldBeFound("firstUsedDate.greaterOrEqualThan=" + DEFAULT_FIRST_USED_DATE);

        // Get all the siteChatgroupDomainList where firstUsedDate greater than or equals to UPDATED_FIRST_USED_DATE
        defaultSiteChatgroupDomainShouldNotBeFound("firstUsedDate.greaterOrEqualThan=" + UPDATED_FIRST_USED_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByFirstUsedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where firstUsedDate less than or equals to DEFAULT_FIRST_USED_DATE
        defaultSiteChatgroupDomainShouldNotBeFound("firstUsedDate.lessThan=" + DEFAULT_FIRST_USED_DATE);

        // Get all the siteChatgroupDomainList where firstUsedDate less than or equals to UPDATED_FIRST_USED_DATE
        defaultSiteChatgroupDomainShouldBeFound("firstUsedDate.lessThan=" + UPDATED_FIRST_USED_DATE);
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByLastUsedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where lastUsedDate equals to DEFAULT_LAST_USED_DATE
        defaultSiteChatgroupDomainShouldBeFound("lastUsedDate.equals=" + DEFAULT_LAST_USED_DATE);

        // Get all the siteChatgroupDomainList where lastUsedDate equals to UPDATED_LAST_USED_DATE
        defaultSiteChatgroupDomainShouldNotBeFound("lastUsedDate.equals=" + UPDATED_LAST_USED_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByLastUsedDateIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where lastUsedDate in DEFAULT_LAST_USED_DATE or UPDATED_LAST_USED_DATE
        defaultSiteChatgroupDomainShouldBeFound("lastUsedDate.in=" + DEFAULT_LAST_USED_DATE + "," + UPDATED_LAST_USED_DATE);

        // Get all the siteChatgroupDomainList where lastUsedDate equals to UPDATED_LAST_USED_DATE
        defaultSiteChatgroupDomainShouldNotBeFound("lastUsedDate.in=" + UPDATED_LAST_USED_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByLastUsedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where lastUsedDate is not null
        defaultSiteChatgroupDomainShouldBeFound("lastUsedDate.specified=true");

        // Get all the siteChatgroupDomainList where lastUsedDate is null
        defaultSiteChatgroupDomainShouldNotBeFound("lastUsedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByLastUsedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where lastUsedDate greater than or equals to DEFAULT_LAST_USED_DATE
        defaultSiteChatgroupDomainShouldBeFound("lastUsedDate.greaterOrEqualThan=" + DEFAULT_LAST_USED_DATE);

        // Get all the siteChatgroupDomainList where lastUsedDate greater than or equals to UPDATED_LAST_USED_DATE
        defaultSiteChatgroupDomainShouldNotBeFound("lastUsedDate.greaterOrEqualThan=" + UPDATED_LAST_USED_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsByLastUsedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);

        // Get all the siteChatgroupDomainList where lastUsedDate less than or equals to DEFAULT_LAST_USED_DATE
        defaultSiteChatgroupDomainShouldNotBeFound("lastUsedDate.lessThan=" + DEFAULT_LAST_USED_DATE);

        // Get all the siteChatgroupDomainList where lastUsedDate less than or equals to UPDATED_LAST_USED_DATE
        defaultSiteChatgroupDomainShouldBeFound("lastUsedDate.lessThan=" + UPDATED_LAST_USED_DATE);
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupDomainsBySiteChatgroupIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupDomain.setSiteChatgroup(siteChatgroup);
        siteChatgroupDomainRepository.saveAndFlush(siteChatgroupDomain);
        Long siteChatgroupId = siteChatgroup.getId();

        // Get all the siteChatgroupDomainList where siteChatgroup equals to siteChatgroupId
        defaultSiteChatgroupDomainShouldBeFound("siteChatgroupId.equals=" + siteChatgroupId);

        // Get all the siteChatgroupDomainList where siteChatgroup equals to siteChatgroupId + 1
        defaultSiteChatgroupDomainShouldNotBeFound("siteChatgroupId.equals=" + (siteChatgroupId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSiteChatgroupDomainShouldBeFound(String filter) throws Exception {
        restSiteChatgroupDomainMockMvc.perform(get("/api/site-chatgroup-domains?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupDomain.getId().intValue())))
            .andExpect(jsonPath("$.[*].domainId").value(hasItem(DEFAULT_DOMAIN_ID.toString())))
            .andExpect(jsonPath("$.[*].domainInfo").value(hasItem(DEFAULT_DOMAIN_INFO.toString())))
            .andExpect(jsonPath("$.[*].firstUsedDate").value(hasItem(sameInstant(DEFAULT_FIRST_USED_DATE))))
            .andExpect(jsonPath("$.[*].lastUsedDate").value(hasItem(sameInstant(DEFAULT_LAST_USED_DATE))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSiteChatgroupDomainShouldNotBeFound(String filter) throws Exception {
        restSiteChatgroupDomainMockMvc.perform(get("/api/site-chatgroup-domains?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingSiteChatgroupDomain() throws Exception {
        // Get the siteChatgroupDomain
        restSiteChatgroupDomainMockMvc.perform(get("/api/site-chatgroup-domains/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSiteChatgroupDomain() throws Exception {
        // Initialize the database
        siteChatgroupDomainService.save(siteChatgroupDomain);

        int databaseSizeBeforeUpdate = siteChatgroupDomainRepository.findAll().size();

        // Update the siteChatgroupDomain
        SiteChatgroupDomain updatedSiteChatgroupDomain = siteChatgroupDomainRepository.findOne(siteChatgroupDomain.getId());
        updatedSiteChatgroupDomain
            .domainId(UPDATED_DOMAIN_ID)
            .domainInfo(UPDATED_DOMAIN_INFO)
            .firstUsedDate(UPDATED_FIRST_USED_DATE)
            .lastUsedDate(UPDATED_LAST_USED_DATE);

        restSiteChatgroupDomainMockMvc.perform(put("/api/site-chatgroup-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSiteChatgroupDomain)))
            .andExpect(status().isOk());

        // Validate the SiteChatgroupDomain in the database
        List<SiteChatgroupDomain> siteChatgroupDomainList = siteChatgroupDomainRepository.findAll();
        assertThat(siteChatgroupDomainList).hasSize(databaseSizeBeforeUpdate);
        SiteChatgroupDomain testSiteChatgroupDomain = siteChatgroupDomainList.get(siteChatgroupDomainList.size() - 1);
        assertThat(testSiteChatgroupDomain.getDomainId()).isEqualTo(UPDATED_DOMAIN_ID);
        assertThat(testSiteChatgroupDomain.getDomainInfo()).isEqualTo(UPDATED_DOMAIN_INFO);
        assertThat(testSiteChatgroupDomain.getFirstUsedDate()).isEqualTo(UPDATED_FIRST_USED_DATE);
        assertThat(testSiteChatgroupDomain.getLastUsedDate()).isEqualTo(UPDATED_LAST_USED_DATE);

        // Validate the SiteChatgroupDomain in Elasticsearch
        SiteChatgroupDomain siteChatgroupDomainEs = siteChatgroupDomainSearchRepository.findOne(testSiteChatgroupDomain.getId());
        assertThat(siteChatgroupDomainEs).isEqualToComparingFieldByField(testSiteChatgroupDomain);
    }

    @Test
    @Transactional
    public void updateNonExistingSiteChatgroupDomain() throws Exception {
        int databaseSizeBeforeUpdate = siteChatgroupDomainRepository.findAll().size();

        // Create the SiteChatgroupDomain

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSiteChatgroupDomainMockMvc.perform(put("/api/site-chatgroup-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupDomain)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupDomain in the database
        List<SiteChatgroupDomain> siteChatgroupDomainList = siteChatgroupDomainRepository.findAll();
        assertThat(siteChatgroupDomainList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSiteChatgroupDomain() throws Exception {
        // Initialize the database
        siteChatgroupDomainService.save(siteChatgroupDomain);

        int databaseSizeBeforeDelete = siteChatgroupDomainRepository.findAll().size();

        // Get the siteChatgroupDomain
        restSiteChatgroupDomainMockMvc.perform(delete("/api/site-chatgroup-domains/{id}", siteChatgroupDomain.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean siteChatgroupDomainExistsInEs = siteChatgroupDomainSearchRepository.exists(siteChatgroupDomain.getId());
        assertThat(siteChatgroupDomainExistsInEs).isFalse();

        // Validate the database is empty
        List<SiteChatgroupDomain> siteChatgroupDomainList = siteChatgroupDomainRepository.findAll();
        assertThat(siteChatgroupDomainList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSiteChatgroupDomain() throws Exception {
        // Initialize the database
        siteChatgroupDomainService.save(siteChatgroupDomain);

        // Search the siteChatgroupDomain
        restSiteChatgroupDomainMockMvc.perform(get("/api/_search/site-chatgroup-domains?query=id:" + siteChatgroupDomain.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupDomain.getId().intValue())))
            .andExpect(jsonPath("$.[*].domainId").value(hasItem(DEFAULT_DOMAIN_ID.toString())))
            .andExpect(jsonPath("$.[*].domainInfo").value(hasItem(DEFAULT_DOMAIN_INFO.toString())))
            .andExpect(jsonPath("$.[*].firstUsedDate").value(hasItem(sameInstant(DEFAULT_FIRST_USED_DATE))))
            .andExpect(jsonPath("$.[*].lastUsedDate").value(hasItem(sameInstant(DEFAULT_LAST_USED_DATE))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SiteChatgroupDomain.class);
        SiteChatgroupDomain siteChatgroupDomain1 = new SiteChatgroupDomain();
        siteChatgroupDomain1.setId(1L);
        SiteChatgroupDomain siteChatgroupDomain2 = new SiteChatgroupDomain();
        siteChatgroupDomain2.setId(siteChatgroupDomain1.getId());
        assertThat(siteChatgroupDomain1).isEqualTo(siteChatgroupDomain2);
        siteChatgroupDomain2.setId(2L);
        assertThat(siteChatgroupDomain1).isNotEqualTo(siteChatgroupDomain2);
        siteChatgroupDomain1.setId(null);
        assertThat(siteChatgroupDomain1).isNotEqualTo(siteChatgroupDomain2);
    }
}
