package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.AuthorToSiteChatgroup;
import biz.newparadigm.meridian.domain.Author;
import biz.newparadigm.meridian.domain.SiteChatgroup;
import biz.newparadigm.meridian.repository.AuthorToSiteChatgroupRepository;
import biz.newparadigm.meridian.service.AuthorToSiteChatgroupService;
import biz.newparadigm.meridian.repository.search.AuthorToSiteChatgroupSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.AuthorToSiteChatgroupCriteria;
import biz.newparadigm.meridian.service.AuthorToSiteChatgroupQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.sameInstant;
import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AuthorToSiteChatgroupResource REST controller.
 *
 * @see AuthorToSiteChatgroupResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class AuthorToSiteChatgroupResourceIntTest {

    private static final ZonedDateTime DEFAULT_FROM_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FROM_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_ORIGINAL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ENGLISH_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ENGLISH_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PROFILE_LINK = "AAAAAAAAAA";
    private static final String UPDATED_PROFILE_LINK = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TO_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TO_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_STAFF_FUNCTION = "AAAAAAAAAA";
    private static final String UPDATED_STAFF_FUNCTION = "BBBBBBBBBB";

    @Autowired
    private AuthorToSiteChatgroupRepository authorToSiteChatgroupRepository;

    @Autowired
    private AuthorToSiteChatgroupService authorToSiteChatgroupService;

    @Autowired
    private AuthorToSiteChatgroupSearchRepository authorToSiteChatgroupSearchRepository;

    @Autowired
    private AuthorToSiteChatgroupQueryService authorToSiteChatgroupQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAuthorToSiteChatgroupMockMvc;

    private AuthorToSiteChatgroup authorToSiteChatgroup;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AuthorToSiteChatgroupResource authorToSiteChatgroupResource = new AuthorToSiteChatgroupResource(authorToSiteChatgroupService, authorToSiteChatgroupQueryService);
        this.restAuthorToSiteChatgroupMockMvc = MockMvcBuilders.standaloneSetup(authorToSiteChatgroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AuthorToSiteChatgroup createEntity(EntityManager em) {
        AuthorToSiteChatgroup authorToSiteChatgroup = new AuthorToSiteChatgroup()
            .fromDate(DEFAULT_FROM_DATE)
            .originalName(DEFAULT_ORIGINAL_NAME)
            .englishName(DEFAULT_ENGLISH_NAME)
            .profileLink(DEFAULT_PROFILE_LINK)
            .toDate(DEFAULT_TO_DATE)
            .staffFunction(DEFAULT_STAFF_FUNCTION);
        // Add required entity
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        authorToSiteChatgroup.setAuthor(author);
        // Add required entity
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        authorToSiteChatgroup.setSiteChatgroup(siteChatgroup);
        return authorToSiteChatgroup;
    }

    @Before
    public void initTest() {
        authorToSiteChatgroupSearchRepository.deleteAll();
        authorToSiteChatgroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createAuthorToSiteChatgroup() throws Exception {
        int databaseSizeBeforeCreate = authorToSiteChatgroupRepository.findAll().size();

        // Create the AuthorToSiteChatgroup
        restAuthorToSiteChatgroupMockMvc.perform(post("/api/author-to-site-chatgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorToSiteChatgroup)))
            .andExpect(status().isCreated());

        // Validate the AuthorToSiteChatgroup in the database
        List<AuthorToSiteChatgroup> authorToSiteChatgroupList = authorToSiteChatgroupRepository.findAll();
        assertThat(authorToSiteChatgroupList).hasSize(databaseSizeBeforeCreate + 1);
        AuthorToSiteChatgroup testAuthorToSiteChatgroup = authorToSiteChatgroupList.get(authorToSiteChatgroupList.size() - 1);
        assertThat(testAuthorToSiteChatgroup.getFromDate()).isEqualTo(DEFAULT_FROM_DATE);
        assertThat(testAuthorToSiteChatgroup.getOriginalName()).isEqualTo(DEFAULT_ORIGINAL_NAME);
        assertThat(testAuthorToSiteChatgroup.getEnglishName()).isEqualTo(DEFAULT_ENGLISH_NAME);
        assertThat(testAuthorToSiteChatgroup.getProfileLink()).isEqualTo(DEFAULT_PROFILE_LINK);
        assertThat(testAuthorToSiteChatgroup.getToDate()).isEqualTo(DEFAULT_TO_DATE);
        assertThat(testAuthorToSiteChatgroup.getStaffFunction()).isEqualTo(DEFAULT_STAFF_FUNCTION);

        // Validate the AuthorToSiteChatgroup in Elasticsearch
        AuthorToSiteChatgroup authorToSiteChatgroupEs = authorToSiteChatgroupSearchRepository.findOne(testAuthorToSiteChatgroup.getId());
        assertThat(authorToSiteChatgroupEs).isEqualToComparingFieldByField(testAuthorToSiteChatgroup);
    }

    @Test
    @Transactional
    public void createAuthorToSiteChatgroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = authorToSiteChatgroupRepository.findAll().size();

        // Create the AuthorToSiteChatgroup with an existing ID
        authorToSiteChatgroup.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAuthorToSiteChatgroupMockMvc.perform(post("/api/author-to-site-chatgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorToSiteChatgroup)))
            .andExpect(status().isBadRequest());

        // Validate the AuthorToSiteChatgroup in the database
        List<AuthorToSiteChatgroup> authorToSiteChatgroupList = authorToSiteChatgroupRepository.findAll();
        assertThat(authorToSiteChatgroupList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroups() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList
        restAuthorToSiteChatgroupMockMvc.perform(get("/api/author-to-site-chatgroups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorToSiteChatgroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].fromDate").value(hasItem(sameInstant(DEFAULT_FROM_DATE))))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].englishName").value(hasItem(DEFAULT_ENGLISH_NAME.toString())))
            .andExpect(jsonPath("$.[*].profileLink").value(hasItem(DEFAULT_PROFILE_LINK.toString())))
            .andExpect(jsonPath("$.[*].toDate").value(hasItem(sameInstant(DEFAULT_TO_DATE))))
            .andExpect(jsonPath("$.[*].staffFunction").value(hasItem(DEFAULT_STAFF_FUNCTION.toString())));
    }

    @Test
    @Transactional
    public void getAuthorToSiteChatgroup() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get the authorToSiteChatgroup
        restAuthorToSiteChatgroupMockMvc.perform(get("/api/author-to-site-chatgroups/{id}", authorToSiteChatgroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(authorToSiteChatgroup.getId().intValue()))
            .andExpect(jsonPath("$.fromDate").value(sameInstant(DEFAULT_FROM_DATE)))
            .andExpect(jsonPath("$.originalName").value(DEFAULT_ORIGINAL_NAME.toString()))
            .andExpect(jsonPath("$.englishName").value(DEFAULT_ENGLISH_NAME.toString()))
            .andExpect(jsonPath("$.profileLink").value(DEFAULT_PROFILE_LINK.toString()))
            .andExpect(jsonPath("$.toDate").value(sameInstant(DEFAULT_TO_DATE)))
            .andExpect(jsonPath("$.staffFunction").value(DEFAULT_STAFF_FUNCTION.toString()));
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByFromDateIsEqualToSomething() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where fromDate equals to DEFAULT_FROM_DATE
        defaultAuthorToSiteChatgroupShouldBeFound("fromDate.equals=" + DEFAULT_FROM_DATE);

        // Get all the authorToSiteChatgroupList where fromDate equals to UPDATED_FROM_DATE
        defaultAuthorToSiteChatgroupShouldNotBeFound("fromDate.equals=" + UPDATED_FROM_DATE);
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByFromDateIsInShouldWork() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where fromDate in DEFAULT_FROM_DATE or UPDATED_FROM_DATE
        defaultAuthorToSiteChatgroupShouldBeFound("fromDate.in=" + DEFAULT_FROM_DATE + "," + UPDATED_FROM_DATE);

        // Get all the authorToSiteChatgroupList where fromDate equals to UPDATED_FROM_DATE
        defaultAuthorToSiteChatgroupShouldNotBeFound("fromDate.in=" + UPDATED_FROM_DATE);
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByFromDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where fromDate is not null
        defaultAuthorToSiteChatgroupShouldBeFound("fromDate.specified=true");

        // Get all the authorToSiteChatgroupList where fromDate is null
        defaultAuthorToSiteChatgroupShouldNotBeFound("fromDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByFromDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where fromDate greater than or equals to DEFAULT_FROM_DATE
        defaultAuthorToSiteChatgroupShouldBeFound("fromDate.greaterOrEqualThan=" + DEFAULT_FROM_DATE);

        // Get all the authorToSiteChatgroupList where fromDate greater than or equals to UPDATED_FROM_DATE
        defaultAuthorToSiteChatgroupShouldNotBeFound("fromDate.greaterOrEqualThan=" + UPDATED_FROM_DATE);
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByFromDateIsLessThanSomething() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where fromDate less than or equals to DEFAULT_FROM_DATE
        defaultAuthorToSiteChatgroupShouldNotBeFound("fromDate.lessThan=" + DEFAULT_FROM_DATE);

        // Get all the authorToSiteChatgroupList where fromDate less than or equals to UPDATED_FROM_DATE
        defaultAuthorToSiteChatgroupShouldBeFound("fromDate.lessThan=" + UPDATED_FROM_DATE);
    }


    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByOriginalNameIsEqualToSomething() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where originalName equals to DEFAULT_ORIGINAL_NAME
        defaultAuthorToSiteChatgroupShouldBeFound("originalName.equals=" + DEFAULT_ORIGINAL_NAME);

        // Get all the authorToSiteChatgroupList where originalName equals to UPDATED_ORIGINAL_NAME
        defaultAuthorToSiteChatgroupShouldNotBeFound("originalName.equals=" + UPDATED_ORIGINAL_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByOriginalNameIsInShouldWork() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where originalName in DEFAULT_ORIGINAL_NAME or UPDATED_ORIGINAL_NAME
        defaultAuthorToSiteChatgroupShouldBeFound("originalName.in=" + DEFAULT_ORIGINAL_NAME + "," + UPDATED_ORIGINAL_NAME);

        // Get all the authorToSiteChatgroupList where originalName equals to UPDATED_ORIGINAL_NAME
        defaultAuthorToSiteChatgroupShouldNotBeFound("originalName.in=" + UPDATED_ORIGINAL_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByOriginalNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where originalName is not null
        defaultAuthorToSiteChatgroupShouldBeFound("originalName.specified=true");

        // Get all the authorToSiteChatgroupList where originalName is null
        defaultAuthorToSiteChatgroupShouldNotBeFound("originalName.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByEnglishNameIsEqualToSomething() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where englishName equals to DEFAULT_ENGLISH_NAME
        defaultAuthorToSiteChatgroupShouldBeFound("englishName.equals=" + DEFAULT_ENGLISH_NAME);

        // Get all the authorToSiteChatgroupList where englishName equals to UPDATED_ENGLISH_NAME
        defaultAuthorToSiteChatgroupShouldNotBeFound("englishName.equals=" + UPDATED_ENGLISH_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByEnglishNameIsInShouldWork() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where englishName in DEFAULT_ENGLISH_NAME or UPDATED_ENGLISH_NAME
        defaultAuthorToSiteChatgroupShouldBeFound("englishName.in=" + DEFAULT_ENGLISH_NAME + "," + UPDATED_ENGLISH_NAME);

        // Get all the authorToSiteChatgroupList where englishName equals to UPDATED_ENGLISH_NAME
        defaultAuthorToSiteChatgroupShouldNotBeFound("englishName.in=" + UPDATED_ENGLISH_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByEnglishNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where englishName is not null
        defaultAuthorToSiteChatgroupShouldBeFound("englishName.specified=true");

        // Get all the authorToSiteChatgroupList where englishName is null
        defaultAuthorToSiteChatgroupShouldNotBeFound("englishName.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByProfileLinkIsEqualToSomething() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where profileLink equals to DEFAULT_PROFILE_LINK
        defaultAuthorToSiteChatgroupShouldBeFound("profileLink.equals=" + DEFAULT_PROFILE_LINK);

        // Get all the authorToSiteChatgroupList where profileLink equals to UPDATED_PROFILE_LINK
        defaultAuthorToSiteChatgroupShouldNotBeFound("profileLink.equals=" + UPDATED_PROFILE_LINK);
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByProfileLinkIsInShouldWork() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where profileLink in DEFAULT_PROFILE_LINK or UPDATED_PROFILE_LINK
        defaultAuthorToSiteChatgroupShouldBeFound("profileLink.in=" + DEFAULT_PROFILE_LINK + "," + UPDATED_PROFILE_LINK);

        // Get all the authorToSiteChatgroupList where profileLink equals to UPDATED_PROFILE_LINK
        defaultAuthorToSiteChatgroupShouldNotBeFound("profileLink.in=" + UPDATED_PROFILE_LINK);
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByProfileLinkIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where profileLink is not null
        defaultAuthorToSiteChatgroupShouldBeFound("profileLink.specified=true");

        // Get all the authorToSiteChatgroupList where profileLink is null
        defaultAuthorToSiteChatgroupShouldNotBeFound("profileLink.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByToDateIsEqualToSomething() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where toDate equals to DEFAULT_TO_DATE
        defaultAuthorToSiteChatgroupShouldBeFound("toDate.equals=" + DEFAULT_TO_DATE);

        // Get all the authorToSiteChatgroupList where toDate equals to UPDATED_TO_DATE
        defaultAuthorToSiteChatgroupShouldNotBeFound("toDate.equals=" + UPDATED_TO_DATE);
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByToDateIsInShouldWork() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where toDate in DEFAULT_TO_DATE or UPDATED_TO_DATE
        defaultAuthorToSiteChatgroupShouldBeFound("toDate.in=" + DEFAULT_TO_DATE + "," + UPDATED_TO_DATE);

        // Get all the authorToSiteChatgroupList where toDate equals to UPDATED_TO_DATE
        defaultAuthorToSiteChatgroupShouldNotBeFound("toDate.in=" + UPDATED_TO_DATE);
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByToDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where toDate is not null
        defaultAuthorToSiteChatgroupShouldBeFound("toDate.specified=true");

        // Get all the authorToSiteChatgroupList where toDate is null
        defaultAuthorToSiteChatgroupShouldNotBeFound("toDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByToDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where toDate greater than or equals to DEFAULT_TO_DATE
        defaultAuthorToSiteChatgroupShouldBeFound("toDate.greaterOrEqualThan=" + DEFAULT_TO_DATE);

        // Get all the authorToSiteChatgroupList where toDate greater than or equals to UPDATED_TO_DATE
        defaultAuthorToSiteChatgroupShouldNotBeFound("toDate.greaterOrEqualThan=" + UPDATED_TO_DATE);
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByToDateIsLessThanSomething() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where toDate less than or equals to DEFAULT_TO_DATE
        defaultAuthorToSiteChatgroupShouldNotBeFound("toDate.lessThan=" + DEFAULT_TO_DATE);

        // Get all the authorToSiteChatgroupList where toDate less than or equals to UPDATED_TO_DATE
        defaultAuthorToSiteChatgroupShouldBeFound("toDate.lessThan=" + UPDATED_TO_DATE);
    }


    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByStaffFunctionIsEqualToSomething() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where staffFunction equals to DEFAULT_STAFF_FUNCTION
        defaultAuthorToSiteChatgroupShouldBeFound("staffFunction.equals=" + DEFAULT_STAFF_FUNCTION);

        // Get all the authorToSiteChatgroupList where staffFunction equals to UPDATED_STAFF_FUNCTION
        defaultAuthorToSiteChatgroupShouldNotBeFound("staffFunction.equals=" + UPDATED_STAFF_FUNCTION);
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByStaffFunctionIsInShouldWork() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where staffFunction in DEFAULT_STAFF_FUNCTION or UPDATED_STAFF_FUNCTION
        defaultAuthorToSiteChatgroupShouldBeFound("staffFunction.in=" + DEFAULT_STAFF_FUNCTION + "," + UPDATED_STAFF_FUNCTION);

        // Get all the authorToSiteChatgroupList where staffFunction equals to UPDATED_STAFF_FUNCTION
        defaultAuthorToSiteChatgroupShouldNotBeFound("staffFunction.in=" + UPDATED_STAFF_FUNCTION);
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByStaffFunctionIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);

        // Get all the authorToSiteChatgroupList where staffFunction is not null
        defaultAuthorToSiteChatgroupShouldBeFound("staffFunction.specified=true");

        // Get all the authorToSiteChatgroupList where staffFunction is null
        defaultAuthorToSiteChatgroupShouldNotBeFound("staffFunction.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsByAuthorIsEqualToSomething() throws Exception {
        // Initialize the database
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        authorToSiteChatgroup.setAuthor(author);
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);
        Long authorId = author.getId();

        // Get all the authorToSiteChatgroupList where author equals to authorId
        defaultAuthorToSiteChatgroupShouldBeFound("authorId.equals=" + authorId);

        // Get all the authorToSiteChatgroupList where author equals to authorId + 1
        defaultAuthorToSiteChatgroupShouldNotBeFound("authorId.equals=" + (authorId + 1));
    }


    @Test
    @Transactional
    public void getAllAuthorToSiteChatgroupsBySiteChatgroupIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        authorToSiteChatgroup.setSiteChatgroup(siteChatgroup);
        authorToSiteChatgroupRepository.saveAndFlush(authorToSiteChatgroup);
        Long siteChatgroupId = siteChatgroup.getId();

        // Get all the authorToSiteChatgroupList where siteChatgroup equals to siteChatgroupId
        defaultAuthorToSiteChatgroupShouldBeFound("siteChatgroupId.equals=" + siteChatgroupId);

        // Get all the authorToSiteChatgroupList where siteChatgroup equals to siteChatgroupId + 1
        defaultAuthorToSiteChatgroupShouldNotBeFound("siteChatgroupId.equals=" + (siteChatgroupId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultAuthorToSiteChatgroupShouldBeFound(String filter) throws Exception {
        restAuthorToSiteChatgroupMockMvc.perform(get("/api/author-to-site-chatgroups?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorToSiteChatgroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].fromDate").value(hasItem(sameInstant(DEFAULT_FROM_DATE))))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].englishName").value(hasItem(DEFAULT_ENGLISH_NAME.toString())))
            .andExpect(jsonPath("$.[*].profileLink").value(hasItem(DEFAULT_PROFILE_LINK.toString())))
            .andExpect(jsonPath("$.[*].toDate").value(hasItem(sameInstant(DEFAULT_TO_DATE))))
            .andExpect(jsonPath("$.[*].staffFunction").value(hasItem(DEFAULT_STAFF_FUNCTION.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultAuthorToSiteChatgroupShouldNotBeFound(String filter) throws Exception {
        restAuthorToSiteChatgroupMockMvc.perform(get("/api/author-to-site-chatgroups?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingAuthorToSiteChatgroup() throws Exception {
        // Get the authorToSiteChatgroup
        restAuthorToSiteChatgroupMockMvc.perform(get("/api/author-to-site-chatgroups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAuthorToSiteChatgroup() throws Exception {
        // Initialize the database
        authorToSiteChatgroupService.save(authorToSiteChatgroup);

        int databaseSizeBeforeUpdate = authorToSiteChatgroupRepository.findAll().size();

        // Update the authorToSiteChatgroup
        AuthorToSiteChatgroup updatedAuthorToSiteChatgroup = authorToSiteChatgroupRepository.findOne(authorToSiteChatgroup.getId());
        updatedAuthorToSiteChatgroup
            .fromDate(UPDATED_FROM_DATE)
            .originalName(UPDATED_ORIGINAL_NAME)
            .englishName(UPDATED_ENGLISH_NAME)
            .profileLink(UPDATED_PROFILE_LINK)
            .toDate(UPDATED_TO_DATE)
            .staffFunction(UPDATED_STAFF_FUNCTION);

        restAuthorToSiteChatgroupMockMvc.perform(put("/api/author-to-site-chatgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAuthorToSiteChatgroup)))
            .andExpect(status().isOk());

        // Validate the AuthorToSiteChatgroup in the database
        List<AuthorToSiteChatgroup> authorToSiteChatgroupList = authorToSiteChatgroupRepository.findAll();
        assertThat(authorToSiteChatgroupList).hasSize(databaseSizeBeforeUpdate);
        AuthorToSiteChatgroup testAuthorToSiteChatgroup = authorToSiteChatgroupList.get(authorToSiteChatgroupList.size() - 1);
        assertThat(testAuthorToSiteChatgroup.getFromDate()).isEqualTo(UPDATED_FROM_DATE);
        assertThat(testAuthorToSiteChatgroup.getOriginalName()).isEqualTo(UPDATED_ORIGINAL_NAME);
        assertThat(testAuthorToSiteChatgroup.getEnglishName()).isEqualTo(UPDATED_ENGLISH_NAME);
        assertThat(testAuthorToSiteChatgroup.getProfileLink()).isEqualTo(UPDATED_PROFILE_LINK);
        assertThat(testAuthorToSiteChatgroup.getToDate()).isEqualTo(UPDATED_TO_DATE);
        assertThat(testAuthorToSiteChatgroup.getStaffFunction()).isEqualTo(UPDATED_STAFF_FUNCTION);

        // Validate the AuthorToSiteChatgroup in Elasticsearch
        AuthorToSiteChatgroup authorToSiteChatgroupEs = authorToSiteChatgroupSearchRepository.findOne(testAuthorToSiteChatgroup.getId());
        assertThat(authorToSiteChatgroupEs).isEqualToComparingFieldByField(testAuthorToSiteChatgroup);
    }

    @Test
    @Transactional
    public void updateNonExistingAuthorToSiteChatgroup() throws Exception {
        int databaseSizeBeforeUpdate = authorToSiteChatgroupRepository.findAll().size();

        // Create the AuthorToSiteChatgroup

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAuthorToSiteChatgroupMockMvc.perform(put("/api/author-to-site-chatgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorToSiteChatgroup)))
            .andExpect(status().isCreated());

        // Validate the AuthorToSiteChatgroup in the database
        List<AuthorToSiteChatgroup> authorToSiteChatgroupList = authorToSiteChatgroupRepository.findAll();
        assertThat(authorToSiteChatgroupList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAuthorToSiteChatgroup() throws Exception {
        // Initialize the database
        authorToSiteChatgroupService.save(authorToSiteChatgroup);

        int databaseSizeBeforeDelete = authorToSiteChatgroupRepository.findAll().size();

        // Get the authorToSiteChatgroup
        restAuthorToSiteChatgroupMockMvc.perform(delete("/api/author-to-site-chatgroups/{id}", authorToSiteChatgroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean authorToSiteChatgroupExistsInEs = authorToSiteChatgroupSearchRepository.exists(authorToSiteChatgroup.getId());
        assertThat(authorToSiteChatgroupExistsInEs).isFalse();

        // Validate the database is empty
        List<AuthorToSiteChatgroup> authorToSiteChatgroupList = authorToSiteChatgroupRepository.findAll();
        assertThat(authorToSiteChatgroupList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAuthorToSiteChatgroup() throws Exception {
        // Initialize the database
        authorToSiteChatgroupService.save(authorToSiteChatgroup);

        // Search the authorToSiteChatgroup
        restAuthorToSiteChatgroupMockMvc.perform(get("/api/_search/author-to-site-chatgroups?query=id:" + authorToSiteChatgroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorToSiteChatgroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].fromDate").value(hasItem(sameInstant(DEFAULT_FROM_DATE))))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME.toString())))
            .andExpect(jsonPath("$.[*].englishName").value(hasItem(DEFAULT_ENGLISH_NAME.toString())))
            .andExpect(jsonPath("$.[*].profileLink").value(hasItem(DEFAULT_PROFILE_LINK.toString())))
            .andExpect(jsonPath("$.[*].toDate").value(hasItem(sameInstant(DEFAULT_TO_DATE))))
            .andExpect(jsonPath("$.[*].staffFunction").value(hasItem(DEFAULT_STAFF_FUNCTION.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AuthorToSiteChatgroup.class);
        AuthorToSiteChatgroup authorToSiteChatgroup1 = new AuthorToSiteChatgroup();
        authorToSiteChatgroup1.setId(1L);
        AuthorToSiteChatgroup authorToSiteChatgroup2 = new AuthorToSiteChatgroup();
        authorToSiteChatgroup2.setId(authorToSiteChatgroup1.getId());
        assertThat(authorToSiteChatgroup1).isEqualTo(authorToSiteChatgroup2);
        authorToSiteChatgroup2.setId(2L);
        assertThat(authorToSiteChatgroup1).isNotEqualTo(authorToSiteChatgroup2);
        authorToSiteChatgroup1.setId(null);
        assertThat(authorToSiteChatgroup1).isNotEqualTo(authorToSiteChatgroup2);
    }
}
