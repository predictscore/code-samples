package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsUrl;
import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.repository.NewsUrlRepository;
import biz.newparadigm.meridian.service.NewsUrlService;
import biz.newparadigm.meridian.repository.search.NewsUrlSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsUrlCriteria;
import biz.newparadigm.meridian.service.NewsUrlQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsUrlResource REST controller.
 *
 * @see NewsUrlResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsUrlResourceIntTest {

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    @Autowired
    private NewsUrlRepository newsUrlRepository;

    @Autowired
    private NewsUrlService newsUrlService;

    @Autowired
    private NewsUrlSearchRepository newsUrlSearchRepository;

    @Autowired
    private NewsUrlQueryService newsUrlQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsUrlMockMvc;

    private NewsUrl newsUrl;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsUrlResource newsUrlResource = new NewsUrlResource(newsUrlService, newsUrlQueryService);
        this.restNewsUrlMockMvc = MockMvcBuilders.standaloneSetup(newsUrlResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsUrl createEntity(EntityManager em) {
        NewsUrl newsUrl = new NewsUrl()
            .url(DEFAULT_URL);
        // Add required entity
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsUrl.setNews(news);
        return newsUrl;
    }

    @Before
    public void initTest() {
        newsUrlSearchRepository.deleteAll();
        newsUrl = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsUrl() throws Exception {
        int databaseSizeBeforeCreate = newsUrlRepository.findAll().size();

        // Create the NewsUrl
        restNewsUrlMockMvc.perform(post("/api/news-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsUrl)))
            .andExpect(status().isCreated());

        // Validate the NewsUrl in the database
        List<NewsUrl> newsUrlList = newsUrlRepository.findAll();
        assertThat(newsUrlList).hasSize(databaseSizeBeforeCreate + 1);
        NewsUrl testNewsUrl = newsUrlList.get(newsUrlList.size() - 1);
        assertThat(testNewsUrl.getUrl()).isEqualTo(DEFAULT_URL);

        // Validate the NewsUrl in Elasticsearch
        NewsUrl newsUrlEs = newsUrlSearchRepository.findOne(testNewsUrl.getId());
        assertThat(newsUrlEs).isEqualToComparingFieldByField(testNewsUrl);
    }

    @Test
    @Transactional
    public void createNewsUrlWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsUrlRepository.findAll().size();

        // Create the NewsUrl with an existing ID
        newsUrl.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsUrlMockMvc.perform(post("/api/news-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsUrl)))
            .andExpect(status().isBadRequest());

        // Validate the NewsUrl in the database
        List<NewsUrl> newsUrlList = newsUrlRepository.findAll();
        assertThat(newsUrlList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUrlIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsUrlRepository.findAll().size();
        // set the field null
        newsUrl.setUrl(null);

        // Create the NewsUrl, which fails.

        restNewsUrlMockMvc.perform(post("/api/news-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsUrl)))
            .andExpect(status().isBadRequest());

        List<NewsUrl> newsUrlList = newsUrlRepository.findAll();
        assertThat(newsUrlList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNewsUrls() throws Exception {
        // Initialize the database
        newsUrlRepository.saveAndFlush(newsUrl);

        // Get all the newsUrlList
        restNewsUrlMockMvc.perform(get("/api/news-urls?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsUrl.getId().intValue())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())));
    }

    @Test
    @Transactional
    public void getNewsUrl() throws Exception {
        // Initialize the database
        newsUrlRepository.saveAndFlush(newsUrl);

        // Get the newsUrl
        restNewsUrlMockMvc.perform(get("/api/news-urls/{id}", newsUrl.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsUrl.getId().intValue()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()));
    }

    @Test
    @Transactional
    public void getAllNewsUrlsByUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        newsUrlRepository.saveAndFlush(newsUrl);

        // Get all the newsUrlList where url equals to DEFAULT_URL
        defaultNewsUrlShouldBeFound("url.equals=" + DEFAULT_URL);

        // Get all the newsUrlList where url equals to UPDATED_URL
        defaultNewsUrlShouldNotBeFound("url.equals=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllNewsUrlsByUrlIsInShouldWork() throws Exception {
        // Initialize the database
        newsUrlRepository.saveAndFlush(newsUrl);

        // Get all the newsUrlList where url in DEFAULT_URL or UPDATED_URL
        defaultNewsUrlShouldBeFound("url.in=" + DEFAULT_URL + "," + UPDATED_URL);

        // Get all the newsUrlList where url equals to UPDATED_URL
        defaultNewsUrlShouldNotBeFound("url.in=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllNewsUrlsByUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsUrlRepository.saveAndFlush(newsUrl);

        // Get all the newsUrlList where url is not null
        defaultNewsUrlShouldBeFound("url.specified=true");

        // Get all the newsUrlList where url is null
        defaultNewsUrlShouldNotBeFound("url.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsUrlsByNewsIsEqualToSomething() throws Exception {
        // Initialize the database
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsUrl.setNews(news);
        newsUrlRepository.saveAndFlush(newsUrl);
        Long newsId = news.getId();

        // Get all the newsUrlList where news equals to newsId
        defaultNewsUrlShouldBeFound("newsId.equals=" + newsId);

        // Get all the newsUrlList where news equals to newsId + 1
        defaultNewsUrlShouldNotBeFound("newsId.equals=" + (newsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsUrlShouldBeFound(String filter) throws Exception {
        restNewsUrlMockMvc.perform(get("/api/news-urls?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsUrl.getId().intValue())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsUrlShouldNotBeFound(String filter) throws Exception {
        restNewsUrlMockMvc.perform(get("/api/news-urls?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsUrl() throws Exception {
        // Get the newsUrl
        restNewsUrlMockMvc.perform(get("/api/news-urls/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsUrl() throws Exception {
        // Initialize the database
        newsUrlService.save(newsUrl);

        int databaseSizeBeforeUpdate = newsUrlRepository.findAll().size();

        // Update the newsUrl
        NewsUrl updatedNewsUrl = newsUrlRepository.findOne(newsUrl.getId());
        updatedNewsUrl
            .url(UPDATED_URL);

        restNewsUrlMockMvc.perform(put("/api/news-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsUrl)))
            .andExpect(status().isOk());

        // Validate the NewsUrl in the database
        List<NewsUrl> newsUrlList = newsUrlRepository.findAll();
        assertThat(newsUrlList).hasSize(databaseSizeBeforeUpdate);
        NewsUrl testNewsUrl = newsUrlList.get(newsUrlList.size() - 1);
        assertThat(testNewsUrl.getUrl()).isEqualTo(UPDATED_URL);

        // Validate the NewsUrl in Elasticsearch
        NewsUrl newsUrlEs = newsUrlSearchRepository.findOne(testNewsUrl.getId());
        assertThat(newsUrlEs).isEqualToComparingFieldByField(testNewsUrl);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsUrl() throws Exception {
        int databaseSizeBeforeUpdate = newsUrlRepository.findAll().size();

        // Create the NewsUrl

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsUrlMockMvc.perform(put("/api/news-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsUrl)))
            .andExpect(status().isCreated());

        // Validate the NewsUrl in the database
        List<NewsUrl> newsUrlList = newsUrlRepository.findAll();
        assertThat(newsUrlList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsUrl() throws Exception {
        // Initialize the database
        newsUrlService.save(newsUrl);

        int databaseSizeBeforeDelete = newsUrlRepository.findAll().size();

        // Get the newsUrl
        restNewsUrlMockMvc.perform(delete("/api/news-urls/{id}", newsUrl.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsUrlExistsInEs = newsUrlSearchRepository.exists(newsUrl.getId());
        assertThat(newsUrlExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsUrl> newsUrlList = newsUrlRepository.findAll();
        assertThat(newsUrlList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsUrl() throws Exception {
        // Initialize the database
        newsUrlService.save(newsUrl);

        // Search the newsUrl
        restNewsUrlMockMvc.perform(get("/api/_search/news-urls?query=id:" + newsUrl.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsUrl.getId().intValue())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsUrl.class);
        NewsUrl newsUrl1 = new NewsUrl();
        newsUrl1.setId(1L);
        NewsUrl newsUrl2 = new NewsUrl();
        newsUrl2.setId(newsUrl1.getId());
        assertThat(newsUrl1).isEqualTo(newsUrl2);
        newsUrl2.setId(2L);
        assertThat(newsUrl1).isNotEqualTo(newsUrl2);
        newsUrl1.setId(null);
        assertThat(newsUrl1).isNotEqualTo(newsUrl2);
    }
}
