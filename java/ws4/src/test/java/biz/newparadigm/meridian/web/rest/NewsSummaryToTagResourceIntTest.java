package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsSummaryToTag;
import biz.newparadigm.meridian.domain.NewsSummary;
import biz.newparadigm.meridian.domain.Tag;
import biz.newparadigm.meridian.repository.NewsSummaryToTagRepository;
import biz.newparadigm.meridian.service.NewsSummaryToTagService;
import biz.newparadigm.meridian.repository.search.NewsSummaryToTagSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsSummaryToTagCriteria;
import biz.newparadigm.meridian.service.NewsSummaryToTagQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsSummaryToTagResource REST controller.
 *
 * @see NewsSummaryToTagResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsSummaryToTagResourceIntTest {

    @Autowired
    private NewsSummaryToTagRepository newsSummaryToTagRepository;

    @Autowired
    private NewsSummaryToTagService newsSummaryToTagService;

    @Autowired
    private NewsSummaryToTagSearchRepository newsSummaryToTagSearchRepository;

    @Autowired
    private NewsSummaryToTagQueryService newsSummaryToTagQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsSummaryToTagMockMvc;

    private NewsSummaryToTag newsSummaryToTag;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsSummaryToTagResource newsSummaryToTagResource = new NewsSummaryToTagResource(newsSummaryToTagService, newsSummaryToTagQueryService);
        this.restNewsSummaryToTagMockMvc = MockMvcBuilders.standaloneSetup(newsSummaryToTagResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsSummaryToTag createEntity(EntityManager em) {
        NewsSummaryToTag newsSummaryToTag = new NewsSummaryToTag();
        // Add required entity
        NewsSummary newsSummary = NewsSummaryResourceIntTest.createEntity(em);
        em.persist(newsSummary);
        em.flush();
        newsSummaryToTag.setNewsSummary(newsSummary);
        // Add required entity
        Tag tag = TagResourceIntTest.createEntity(em);
        em.persist(tag);
        em.flush();
        newsSummaryToTag.setTag(tag);
        return newsSummaryToTag;
    }

    @Before
    public void initTest() {
        newsSummaryToTagSearchRepository.deleteAll();
        newsSummaryToTag = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsSummaryToTag() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryToTagRepository.findAll().size();

        // Create the NewsSummaryToTag
        restNewsSummaryToTagMockMvc.perform(post("/api/news-summary-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryToTag)))
            .andExpect(status().isCreated());

        // Validate the NewsSummaryToTag in the database
        List<NewsSummaryToTag> newsSummaryToTagList = newsSummaryToTagRepository.findAll();
        assertThat(newsSummaryToTagList).hasSize(databaseSizeBeforeCreate + 1);
        NewsSummaryToTag testNewsSummaryToTag = newsSummaryToTagList.get(newsSummaryToTagList.size() - 1);

        // Validate the NewsSummaryToTag in Elasticsearch
        NewsSummaryToTag newsSummaryToTagEs = newsSummaryToTagSearchRepository.findOne(testNewsSummaryToTag.getId());
        assertThat(newsSummaryToTagEs).isEqualToComparingFieldByField(testNewsSummaryToTag);
    }

    @Test
    @Transactional
    public void createNewsSummaryToTagWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryToTagRepository.findAll().size();

        // Create the NewsSummaryToTag with an existing ID
        newsSummaryToTag.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsSummaryToTagMockMvc.perform(post("/api/news-summary-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryToTag)))
            .andExpect(status().isBadRequest());

        // Validate the NewsSummaryToTag in the database
        List<NewsSummaryToTag> newsSummaryToTagList = newsSummaryToTagRepository.findAll();
        assertThat(newsSummaryToTagList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryToTags() throws Exception {
        // Initialize the database
        newsSummaryToTagRepository.saveAndFlush(newsSummaryToTag);

        // Get all the newsSummaryToTagList
        restNewsSummaryToTagMockMvc.perform(get("/api/news-summary-to-tags?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryToTag.getId().intValue())));
    }

    @Test
    @Transactional
    public void getNewsSummaryToTag() throws Exception {
        // Initialize the database
        newsSummaryToTagRepository.saveAndFlush(newsSummaryToTag);

        // Get the newsSummaryToTag
        restNewsSummaryToTagMockMvc.perform(get("/api/news-summary-to-tags/{id}", newsSummaryToTag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsSummaryToTag.getId().intValue()));
    }

    @Test
    @Transactional
    public void getAllNewsSummaryToTagsByNewsSummaryIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsSummary newsSummary = NewsSummaryResourceIntTest.createEntity(em);
        em.persist(newsSummary);
        em.flush();
        newsSummaryToTag.setNewsSummary(newsSummary);
        newsSummaryToTagRepository.saveAndFlush(newsSummaryToTag);
        Long newsSummaryId = newsSummary.getId();

        // Get all the newsSummaryToTagList where newsSummary equals to newsSummaryId
        defaultNewsSummaryToTagShouldBeFound("newsSummaryId.equals=" + newsSummaryId);

        // Get all the newsSummaryToTagList where newsSummary equals to newsSummaryId + 1
        defaultNewsSummaryToTagShouldNotBeFound("newsSummaryId.equals=" + (newsSummaryId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsSummaryToTagsByTagIsEqualToSomething() throws Exception {
        // Initialize the database
        Tag tag = TagResourceIntTest.createEntity(em);
        em.persist(tag);
        em.flush();
        newsSummaryToTag.setTag(tag);
        newsSummaryToTagRepository.saveAndFlush(newsSummaryToTag);
        Long tagId = tag.getId();

        // Get all the newsSummaryToTagList where tag equals to tagId
        defaultNewsSummaryToTagShouldBeFound("tagId.equals=" + tagId);

        // Get all the newsSummaryToTagList where tag equals to tagId + 1
        defaultNewsSummaryToTagShouldNotBeFound("tagId.equals=" + (tagId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsSummaryToTagShouldBeFound(String filter) throws Exception {
        restNewsSummaryToTagMockMvc.perform(get("/api/news-summary-to-tags?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryToTag.getId().intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsSummaryToTagShouldNotBeFound(String filter) throws Exception {
        restNewsSummaryToTagMockMvc.perform(get("/api/news-summary-to-tags?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsSummaryToTag() throws Exception {
        // Get the newsSummaryToTag
        restNewsSummaryToTagMockMvc.perform(get("/api/news-summary-to-tags/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsSummaryToTag() throws Exception {
        // Initialize the database
        newsSummaryToTagService.save(newsSummaryToTag);

        int databaseSizeBeforeUpdate = newsSummaryToTagRepository.findAll().size();

        // Update the newsSummaryToTag
        NewsSummaryToTag updatedNewsSummaryToTag = newsSummaryToTagRepository.findOne(newsSummaryToTag.getId());

        restNewsSummaryToTagMockMvc.perform(put("/api/news-summary-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsSummaryToTag)))
            .andExpect(status().isOk());

        // Validate the NewsSummaryToTag in the database
        List<NewsSummaryToTag> newsSummaryToTagList = newsSummaryToTagRepository.findAll();
        assertThat(newsSummaryToTagList).hasSize(databaseSizeBeforeUpdate);
        NewsSummaryToTag testNewsSummaryToTag = newsSummaryToTagList.get(newsSummaryToTagList.size() - 1);

        // Validate the NewsSummaryToTag in Elasticsearch
        NewsSummaryToTag newsSummaryToTagEs = newsSummaryToTagSearchRepository.findOne(testNewsSummaryToTag.getId());
        assertThat(newsSummaryToTagEs).isEqualToComparingFieldByField(testNewsSummaryToTag);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsSummaryToTag() throws Exception {
        int databaseSizeBeforeUpdate = newsSummaryToTagRepository.findAll().size();

        // Create the NewsSummaryToTag

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsSummaryToTagMockMvc.perform(put("/api/news-summary-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryToTag)))
            .andExpect(status().isCreated());

        // Validate the NewsSummaryToTag in the database
        List<NewsSummaryToTag> newsSummaryToTagList = newsSummaryToTagRepository.findAll();
        assertThat(newsSummaryToTagList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsSummaryToTag() throws Exception {
        // Initialize the database
        newsSummaryToTagService.save(newsSummaryToTag);

        int databaseSizeBeforeDelete = newsSummaryToTagRepository.findAll().size();

        // Get the newsSummaryToTag
        restNewsSummaryToTagMockMvc.perform(delete("/api/news-summary-to-tags/{id}", newsSummaryToTag.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsSummaryToTagExistsInEs = newsSummaryToTagSearchRepository.exists(newsSummaryToTag.getId());
        assertThat(newsSummaryToTagExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsSummaryToTag> newsSummaryToTagList = newsSummaryToTagRepository.findAll();
        assertThat(newsSummaryToTagList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsSummaryToTag() throws Exception {
        // Initialize the database
        newsSummaryToTagService.save(newsSummaryToTag);

        // Search the newsSummaryToTag
        restNewsSummaryToTagMockMvc.perform(get("/api/_search/news-summary-to-tags?query=id:" + newsSummaryToTag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryToTag.getId().intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsSummaryToTag.class);
        NewsSummaryToTag newsSummaryToTag1 = new NewsSummaryToTag();
        newsSummaryToTag1.setId(1L);
        NewsSummaryToTag newsSummaryToTag2 = new NewsSummaryToTag();
        newsSummaryToTag2.setId(newsSummaryToTag1.getId());
        assertThat(newsSummaryToTag1).isEqualTo(newsSummaryToTag2);
        newsSummaryToTag2.setId(2L);
        assertThat(newsSummaryToTag1).isNotEqualTo(newsSummaryToTag2);
        newsSummaryToTag1.setId(null);
        assertThat(newsSummaryToTag1).isNotEqualTo(newsSummaryToTag2);
    }
}
