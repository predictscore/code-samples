package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.SiteChatgroupNumVisitors;
import biz.newparadigm.meridian.domain.SiteChatgroup;
import biz.newparadigm.meridian.repository.SiteChatgroupNumVisitorsRepository;
import biz.newparadigm.meridian.service.SiteChatgroupNumVisitorsService;
import biz.newparadigm.meridian.repository.search.SiteChatgroupNumVisitorsSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.SiteChatgroupNumVisitorsCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupNumVisitorsQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.sameInstant;
import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SiteChatgroupNumVisitorsResource REST controller.
 *
 * @see SiteChatgroupNumVisitorsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class SiteChatgroupNumVisitorsResourceIntTest {

    private static final Integer DEFAULT_NUM_VISITORS = 1;
    private static final Integer UPDATED_NUM_VISITORS = 2;

    private static final ZonedDateTime DEFAULT_NUM_VISITOR_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_NUM_VISITOR_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private SiteChatgroupNumVisitorsRepository siteChatgroupNumVisitorsRepository;

    @Autowired
    private SiteChatgroupNumVisitorsService siteChatgroupNumVisitorsService;

    @Autowired
    private SiteChatgroupNumVisitorsSearchRepository siteChatgroupNumVisitorsSearchRepository;

    @Autowired
    private SiteChatgroupNumVisitorsQueryService siteChatgroupNumVisitorsQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSiteChatgroupNumVisitorsMockMvc;

    private SiteChatgroupNumVisitors siteChatgroupNumVisitors;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SiteChatgroupNumVisitorsResource siteChatgroupNumVisitorsResource = new SiteChatgroupNumVisitorsResource(siteChatgroupNumVisitorsService, siteChatgroupNumVisitorsQueryService);
        this.restSiteChatgroupNumVisitorsMockMvc = MockMvcBuilders.standaloneSetup(siteChatgroupNumVisitorsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SiteChatgroupNumVisitors createEntity(EntityManager em) {
        SiteChatgroupNumVisitors siteChatgroupNumVisitors = new SiteChatgroupNumVisitors()
            .numVisitors(DEFAULT_NUM_VISITORS)
            .numVisitorDate(DEFAULT_NUM_VISITOR_DATE);
        // Add required entity
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupNumVisitors.setSiteChatgroup(siteChatgroup);
        return siteChatgroupNumVisitors;
    }

    @Before
    public void initTest() {
        siteChatgroupNumVisitorsSearchRepository.deleteAll();
        siteChatgroupNumVisitors = createEntity(em);
    }

    @Test
    @Transactional
    public void createSiteChatgroupNumVisitors() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupNumVisitorsRepository.findAll().size();

        // Create the SiteChatgroupNumVisitors
        restSiteChatgroupNumVisitorsMockMvc.perform(post("/api/site-chatgroup-num-visitors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupNumVisitors)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupNumVisitors in the database
        List<SiteChatgroupNumVisitors> siteChatgroupNumVisitorsList = siteChatgroupNumVisitorsRepository.findAll();
        assertThat(siteChatgroupNumVisitorsList).hasSize(databaseSizeBeforeCreate + 1);
        SiteChatgroupNumVisitors testSiteChatgroupNumVisitors = siteChatgroupNumVisitorsList.get(siteChatgroupNumVisitorsList.size() - 1);
        assertThat(testSiteChatgroupNumVisitors.getNumVisitors()).isEqualTo(DEFAULT_NUM_VISITORS);
        assertThat(testSiteChatgroupNumVisitors.getNumVisitorDate()).isEqualTo(DEFAULT_NUM_VISITOR_DATE);

        // Validate the SiteChatgroupNumVisitors in Elasticsearch
        SiteChatgroupNumVisitors siteChatgroupNumVisitorsEs = siteChatgroupNumVisitorsSearchRepository.findOne(testSiteChatgroupNumVisitors.getId());
        assertThat(siteChatgroupNumVisitorsEs).isEqualToComparingFieldByField(testSiteChatgroupNumVisitors);
    }

    @Test
    @Transactional
    public void createSiteChatgroupNumVisitorsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupNumVisitorsRepository.findAll().size();

        // Create the SiteChatgroupNumVisitors with an existing ID
        siteChatgroupNumVisitors.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSiteChatgroupNumVisitorsMockMvc.perform(post("/api/site-chatgroup-num-visitors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupNumVisitors)))
            .andExpect(status().isBadRequest());

        // Validate the SiteChatgroupNumVisitors in the database
        List<SiteChatgroupNumVisitors> siteChatgroupNumVisitorsList = siteChatgroupNumVisitorsRepository.findAll();
        assertThat(siteChatgroupNumVisitorsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNumVisitorsIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupNumVisitorsRepository.findAll().size();
        // set the field null
        siteChatgroupNumVisitors.setNumVisitors(null);

        // Create the SiteChatgroupNumVisitors, which fails.

        restSiteChatgroupNumVisitorsMockMvc.perform(post("/api/site-chatgroup-num-visitors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupNumVisitors)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupNumVisitors> siteChatgroupNumVisitorsList = siteChatgroupNumVisitorsRepository.findAll();
        assertThat(siteChatgroupNumVisitorsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumVisitorDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupNumVisitorsRepository.findAll().size();
        // set the field null
        siteChatgroupNumVisitors.setNumVisitorDate(null);

        // Create the SiteChatgroupNumVisitors, which fails.

        restSiteChatgroupNumVisitorsMockMvc.perform(post("/api/site-chatgroup-num-visitors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupNumVisitors)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupNumVisitors> siteChatgroupNumVisitorsList = siteChatgroupNumVisitorsRepository.findAll();
        assertThat(siteChatgroupNumVisitorsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumVisitors() throws Exception {
        // Initialize the database
        siteChatgroupNumVisitorsRepository.saveAndFlush(siteChatgroupNumVisitors);

        // Get all the siteChatgroupNumVisitorsList
        restSiteChatgroupNumVisitorsMockMvc.perform(get("/api/site-chatgroup-num-visitors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupNumVisitors.getId().intValue())))
            .andExpect(jsonPath("$.[*].numVisitors").value(hasItem(DEFAULT_NUM_VISITORS)))
            .andExpect(jsonPath("$.[*].numVisitorDate").value(hasItem(sameInstant(DEFAULT_NUM_VISITOR_DATE))));
    }

    @Test
    @Transactional
    public void getSiteChatgroupNumVisitors() throws Exception {
        // Initialize the database
        siteChatgroupNumVisitorsRepository.saveAndFlush(siteChatgroupNumVisitors);

        // Get the siteChatgroupNumVisitors
        restSiteChatgroupNumVisitorsMockMvc.perform(get("/api/site-chatgroup-num-visitors/{id}", siteChatgroupNumVisitors.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(siteChatgroupNumVisitors.getId().intValue()))
            .andExpect(jsonPath("$.numVisitors").value(DEFAULT_NUM_VISITORS))
            .andExpect(jsonPath("$.numVisitorDate").value(sameInstant(DEFAULT_NUM_VISITOR_DATE)));
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumVisitorsByNumVisitorsIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupNumVisitorsRepository.saveAndFlush(siteChatgroupNumVisitors);

        // Get all the siteChatgroupNumVisitorsList where numVisitors equals to DEFAULT_NUM_VISITORS
        defaultSiteChatgroupNumVisitorsShouldBeFound("numVisitors.equals=" + DEFAULT_NUM_VISITORS);

        // Get all the siteChatgroupNumVisitorsList where numVisitors equals to UPDATED_NUM_VISITORS
        defaultSiteChatgroupNumVisitorsShouldNotBeFound("numVisitors.equals=" + UPDATED_NUM_VISITORS);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumVisitorsByNumVisitorsIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupNumVisitorsRepository.saveAndFlush(siteChatgroupNumVisitors);

        // Get all the siteChatgroupNumVisitorsList where numVisitors in DEFAULT_NUM_VISITORS or UPDATED_NUM_VISITORS
        defaultSiteChatgroupNumVisitorsShouldBeFound("numVisitors.in=" + DEFAULT_NUM_VISITORS + "," + UPDATED_NUM_VISITORS);

        // Get all the siteChatgroupNumVisitorsList where numVisitors equals to UPDATED_NUM_VISITORS
        defaultSiteChatgroupNumVisitorsShouldNotBeFound("numVisitors.in=" + UPDATED_NUM_VISITORS);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumVisitorsByNumVisitorsIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupNumVisitorsRepository.saveAndFlush(siteChatgroupNumVisitors);

        // Get all the siteChatgroupNumVisitorsList where numVisitors is not null
        defaultSiteChatgroupNumVisitorsShouldBeFound("numVisitors.specified=true");

        // Get all the siteChatgroupNumVisitorsList where numVisitors is null
        defaultSiteChatgroupNumVisitorsShouldNotBeFound("numVisitors.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumVisitorsByNumVisitorsIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupNumVisitorsRepository.saveAndFlush(siteChatgroupNumVisitors);

        // Get all the siteChatgroupNumVisitorsList where numVisitors greater than or equals to DEFAULT_NUM_VISITORS
        defaultSiteChatgroupNumVisitorsShouldBeFound("numVisitors.greaterOrEqualThan=" + DEFAULT_NUM_VISITORS);

        // Get all the siteChatgroupNumVisitorsList where numVisitors greater than or equals to UPDATED_NUM_VISITORS
        defaultSiteChatgroupNumVisitorsShouldNotBeFound("numVisitors.greaterOrEqualThan=" + UPDATED_NUM_VISITORS);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumVisitorsByNumVisitorsIsLessThanSomething() throws Exception {
        // Initialize the database
        siteChatgroupNumVisitorsRepository.saveAndFlush(siteChatgroupNumVisitors);

        // Get all the siteChatgroupNumVisitorsList where numVisitors less than or equals to DEFAULT_NUM_VISITORS
        defaultSiteChatgroupNumVisitorsShouldNotBeFound("numVisitors.lessThan=" + DEFAULT_NUM_VISITORS);

        // Get all the siteChatgroupNumVisitorsList where numVisitors less than or equals to UPDATED_NUM_VISITORS
        defaultSiteChatgroupNumVisitorsShouldBeFound("numVisitors.lessThan=" + UPDATED_NUM_VISITORS);
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupNumVisitorsByNumVisitorDateIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupNumVisitorsRepository.saveAndFlush(siteChatgroupNumVisitors);

        // Get all the siteChatgroupNumVisitorsList where numVisitorDate equals to DEFAULT_NUM_VISITOR_DATE
        defaultSiteChatgroupNumVisitorsShouldBeFound("numVisitorDate.equals=" + DEFAULT_NUM_VISITOR_DATE);

        // Get all the siteChatgroupNumVisitorsList where numVisitorDate equals to UPDATED_NUM_VISITOR_DATE
        defaultSiteChatgroupNumVisitorsShouldNotBeFound("numVisitorDate.equals=" + UPDATED_NUM_VISITOR_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumVisitorsByNumVisitorDateIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupNumVisitorsRepository.saveAndFlush(siteChatgroupNumVisitors);

        // Get all the siteChatgroupNumVisitorsList where numVisitorDate in DEFAULT_NUM_VISITOR_DATE or UPDATED_NUM_VISITOR_DATE
        defaultSiteChatgroupNumVisitorsShouldBeFound("numVisitorDate.in=" + DEFAULT_NUM_VISITOR_DATE + "," + UPDATED_NUM_VISITOR_DATE);

        // Get all the siteChatgroupNumVisitorsList where numVisitorDate equals to UPDATED_NUM_VISITOR_DATE
        defaultSiteChatgroupNumVisitorsShouldNotBeFound("numVisitorDate.in=" + UPDATED_NUM_VISITOR_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumVisitorsByNumVisitorDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupNumVisitorsRepository.saveAndFlush(siteChatgroupNumVisitors);

        // Get all the siteChatgroupNumVisitorsList where numVisitorDate is not null
        defaultSiteChatgroupNumVisitorsShouldBeFound("numVisitorDate.specified=true");

        // Get all the siteChatgroupNumVisitorsList where numVisitorDate is null
        defaultSiteChatgroupNumVisitorsShouldNotBeFound("numVisitorDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumVisitorsByNumVisitorDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupNumVisitorsRepository.saveAndFlush(siteChatgroupNumVisitors);

        // Get all the siteChatgroupNumVisitorsList where numVisitorDate greater than or equals to DEFAULT_NUM_VISITOR_DATE
        defaultSiteChatgroupNumVisitorsShouldBeFound("numVisitorDate.greaterOrEqualThan=" + DEFAULT_NUM_VISITOR_DATE);

        // Get all the siteChatgroupNumVisitorsList where numVisitorDate greater than or equals to UPDATED_NUM_VISITOR_DATE
        defaultSiteChatgroupNumVisitorsShouldNotBeFound("numVisitorDate.greaterOrEqualThan=" + UPDATED_NUM_VISITOR_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupNumVisitorsByNumVisitorDateIsLessThanSomething() throws Exception {
        // Initialize the database
        siteChatgroupNumVisitorsRepository.saveAndFlush(siteChatgroupNumVisitors);

        // Get all the siteChatgroupNumVisitorsList where numVisitorDate less than or equals to DEFAULT_NUM_VISITOR_DATE
        defaultSiteChatgroupNumVisitorsShouldNotBeFound("numVisitorDate.lessThan=" + DEFAULT_NUM_VISITOR_DATE);

        // Get all the siteChatgroupNumVisitorsList where numVisitorDate less than or equals to UPDATED_NUM_VISITOR_DATE
        defaultSiteChatgroupNumVisitorsShouldBeFound("numVisitorDate.lessThan=" + UPDATED_NUM_VISITOR_DATE);
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupNumVisitorsBySiteChatgroupIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupNumVisitors.setSiteChatgroup(siteChatgroup);
        siteChatgroupNumVisitorsRepository.saveAndFlush(siteChatgroupNumVisitors);
        Long siteChatgroupId = siteChatgroup.getId();

        // Get all the siteChatgroupNumVisitorsList where siteChatgroup equals to siteChatgroupId
        defaultSiteChatgroupNumVisitorsShouldBeFound("siteChatgroupId.equals=" + siteChatgroupId);

        // Get all the siteChatgroupNumVisitorsList where siteChatgroup equals to siteChatgroupId + 1
        defaultSiteChatgroupNumVisitorsShouldNotBeFound("siteChatgroupId.equals=" + (siteChatgroupId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSiteChatgroupNumVisitorsShouldBeFound(String filter) throws Exception {
        restSiteChatgroupNumVisitorsMockMvc.perform(get("/api/site-chatgroup-num-visitors?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupNumVisitors.getId().intValue())))
            .andExpect(jsonPath("$.[*].numVisitors").value(hasItem(DEFAULT_NUM_VISITORS)))
            .andExpect(jsonPath("$.[*].numVisitorDate").value(hasItem(sameInstant(DEFAULT_NUM_VISITOR_DATE))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSiteChatgroupNumVisitorsShouldNotBeFound(String filter) throws Exception {
        restSiteChatgroupNumVisitorsMockMvc.perform(get("/api/site-chatgroup-num-visitors?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingSiteChatgroupNumVisitors() throws Exception {
        // Get the siteChatgroupNumVisitors
        restSiteChatgroupNumVisitorsMockMvc.perform(get("/api/site-chatgroup-num-visitors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSiteChatgroupNumVisitors() throws Exception {
        // Initialize the database
        siteChatgroupNumVisitorsService.save(siteChatgroupNumVisitors);

        int databaseSizeBeforeUpdate = siteChatgroupNumVisitorsRepository.findAll().size();

        // Update the siteChatgroupNumVisitors
        SiteChatgroupNumVisitors updatedSiteChatgroupNumVisitors = siteChatgroupNumVisitorsRepository.findOne(siteChatgroupNumVisitors.getId());
        updatedSiteChatgroupNumVisitors
            .numVisitors(UPDATED_NUM_VISITORS)
            .numVisitorDate(UPDATED_NUM_VISITOR_DATE);

        restSiteChatgroupNumVisitorsMockMvc.perform(put("/api/site-chatgroup-num-visitors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSiteChatgroupNumVisitors)))
            .andExpect(status().isOk());

        // Validate the SiteChatgroupNumVisitors in the database
        List<SiteChatgroupNumVisitors> siteChatgroupNumVisitorsList = siteChatgroupNumVisitorsRepository.findAll();
        assertThat(siteChatgroupNumVisitorsList).hasSize(databaseSizeBeforeUpdate);
        SiteChatgroupNumVisitors testSiteChatgroupNumVisitors = siteChatgroupNumVisitorsList.get(siteChatgroupNumVisitorsList.size() - 1);
        assertThat(testSiteChatgroupNumVisitors.getNumVisitors()).isEqualTo(UPDATED_NUM_VISITORS);
        assertThat(testSiteChatgroupNumVisitors.getNumVisitorDate()).isEqualTo(UPDATED_NUM_VISITOR_DATE);

        // Validate the SiteChatgroupNumVisitors in Elasticsearch
        SiteChatgroupNumVisitors siteChatgroupNumVisitorsEs = siteChatgroupNumVisitorsSearchRepository.findOne(testSiteChatgroupNumVisitors.getId());
        assertThat(siteChatgroupNumVisitorsEs).isEqualToComparingFieldByField(testSiteChatgroupNumVisitors);
    }

    @Test
    @Transactional
    public void updateNonExistingSiteChatgroupNumVisitors() throws Exception {
        int databaseSizeBeforeUpdate = siteChatgroupNumVisitorsRepository.findAll().size();

        // Create the SiteChatgroupNumVisitors

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSiteChatgroupNumVisitorsMockMvc.perform(put("/api/site-chatgroup-num-visitors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupNumVisitors)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupNumVisitors in the database
        List<SiteChatgroupNumVisitors> siteChatgroupNumVisitorsList = siteChatgroupNumVisitorsRepository.findAll();
        assertThat(siteChatgroupNumVisitorsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSiteChatgroupNumVisitors() throws Exception {
        // Initialize the database
        siteChatgroupNumVisitorsService.save(siteChatgroupNumVisitors);

        int databaseSizeBeforeDelete = siteChatgroupNumVisitorsRepository.findAll().size();

        // Get the siteChatgroupNumVisitors
        restSiteChatgroupNumVisitorsMockMvc.perform(delete("/api/site-chatgroup-num-visitors/{id}", siteChatgroupNumVisitors.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean siteChatgroupNumVisitorsExistsInEs = siteChatgroupNumVisitorsSearchRepository.exists(siteChatgroupNumVisitors.getId());
        assertThat(siteChatgroupNumVisitorsExistsInEs).isFalse();

        // Validate the database is empty
        List<SiteChatgroupNumVisitors> siteChatgroupNumVisitorsList = siteChatgroupNumVisitorsRepository.findAll();
        assertThat(siteChatgroupNumVisitorsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSiteChatgroupNumVisitors() throws Exception {
        // Initialize the database
        siteChatgroupNumVisitorsService.save(siteChatgroupNumVisitors);

        // Search the siteChatgroupNumVisitors
        restSiteChatgroupNumVisitorsMockMvc.perform(get("/api/_search/site-chatgroup-num-visitors?query=id:" + siteChatgroupNumVisitors.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupNumVisitors.getId().intValue())))
            .andExpect(jsonPath("$.[*].numVisitors").value(hasItem(DEFAULT_NUM_VISITORS)))
            .andExpect(jsonPath("$.[*].numVisitorDate").value(hasItem(sameInstant(DEFAULT_NUM_VISITOR_DATE))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SiteChatgroupNumVisitors.class);
        SiteChatgroupNumVisitors siteChatgroupNumVisitors1 = new SiteChatgroupNumVisitors();
        siteChatgroupNumVisitors1.setId(1L);
        SiteChatgroupNumVisitors siteChatgroupNumVisitors2 = new SiteChatgroupNumVisitors();
        siteChatgroupNumVisitors2.setId(siteChatgroupNumVisitors1.getId());
        assertThat(siteChatgroupNumVisitors1).isEqualTo(siteChatgroupNumVisitors2);
        siteChatgroupNumVisitors2.setId(2L);
        assertThat(siteChatgroupNumVisitors1).isNotEqualTo(siteChatgroupNumVisitors2);
        siteChatgroupNumVisitors1.setId(null);
        assertThat(siteChatgroupNumVisitors1).isNotEqualTo(siteChatgroupNumVisitors2);
    }
}
