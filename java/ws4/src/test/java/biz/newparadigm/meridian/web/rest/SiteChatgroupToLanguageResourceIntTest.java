package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.SiteChatgroupToLanguage;
import biz.newparadigm.meridian.domain.SiteChatgroup;
import biz.newparadigm.meridian.domain.Language;
import biz.newparadigm.meridian.repository.SiteChatgroupToLanguageRepository;
import biz.newparadigm.meridian.service.SiteChatgroupToLanguageService;
import biz.newparadigm.meridian.repository.search.SiteChatgroupToLanguageSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.SiteChatgroupToLanguageCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupToLanguageQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SiteChatgroupToLanguageResource REST controller.
 *
 * @see SiteChatgroupToLanguageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class SiteChatgroupToLanguageResourceIntTest {

    @Autowired
    private SiteChatgroupToLanguageRepository siteChatgroupToLanguageRepository;

    @Autowired
    private SiteChatgroupToLanguageService siteChatgroupToLanguageService;

    @Autowired
    private SiteChatgroupToLanguageSearchRepository siteChatgroupToLanguageSearchRepository;

    @Autowired
    private SiteChatgroupToLanguageQueryService siteChatgroupToLanguageQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSiteChatgroupToLanguageMockMvc;

    private SiteChatgroupToLanguage siteChatgroupToLanguage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SiteChatgroupToLanguageResource siteChatgroupToLanguageResource = new SiteChatgroupToLanguageResource(siteChatgroupToLanguageService, siteChatgroupToLanguageQueryService);
        this.restSiteChatgroupToLanguageMockMvc = MockMvcBuilders.standaloneSetup(siteChatgroupToLanguageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SiteChatgroupToLanguage createEntity(EntityManager em) {
        SiteChatgroupToLanguage siteChatgroupToLanguage = new SiteChatgroupToLanguage();
        // Add required entity
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupToLanguage.setSiteChatgroup(siteChatgroup);
        // Add required entity
        Language language = LanguageResourceIntTest.createEntity(em);
        em.persist(language);
        em.flush();
        siteChatgroupToLanguage.setLanguage(language);
        return siteChatgroupToLanguage;
    }

    @Before
    public void initTest() {
        siteChatgroupToLanguageSearchRepository.deleteAll();
        siteChatgroupToLanguage = createEntity(em);
    }

    @Test
    @Transactional
    public void createSiteChatgroupToLanguage() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupToLanguageRepository.findAll().size();

        // Create the SiteChatgroupToLanguage
        restSiteChatgroupToLanguageMockMvc.perform(post("/api/site-chatgroup-to-languages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupToLanguage)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupToLanguage in the database
        List<SiteChatgroupToLanguage> siteChatgroupToLanguageList = siteChatgroupToLanguageRepository.findAll();
        assertThat(siteChatgroupToLanguageList).hasSize(databaseSizeBeforeCreate + 1);
        SiteChatgroupToLanguage testSiteChatgroupToLanguage = siteChatgroupToLanguageList.get(siteChatgroupToLanguageList.size() - 1);

        // Validate the SiteChatgroupToLanguage in Elasticsearch
        SiteChatgroupToLanguage siteChatgroupToLanguageEs = siteChatgroupToLanguageSearchRepository.findOne(testSiteChatgroupToLanguage.getId());
        assertThat(siteChatgroupToLanguageEs).isEqualToComparingFieldByField(testSiteChatgroupToLanguage);
    }

    @Test
    @Transactional
    public void createSiteChatgroupToLanguageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupToLanguageRepository.findAll().size();

        // Create the SiteChatgroupToLanguage with an existing ID
        siteChatgroupToLanguage.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSiteChatgroupToLanguageMockMvc.perform(post("/api/site-chatgroup-to-languages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupToLanguage)))
            .andExpect(status().isBadRequest());

        // Validate the SiteChatgroupToLanguage in the database
        List<SiteChatgroupToLanguage> siteChatgroupToLanguageList = siteChatgroupToLanguageRepository.findAll();
        assertThat(siteChatgroupToLanguageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupToLanguages() throws Exception {
        // Initialize the database
        siteChatgroupToLanguageRepository.saveAndFlush(siteChatgroupToLanguage);

        // Get all the siteChatgroupToLanguageList
        restSiteChatgroupToLanguageMockMvc.perform(get("/api/site-chatgroup-to-languages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupToLanguage.getId().intValue())));
    }

    @Test
    @Transactional
    public void getSiteChatgroupToLanguage() throws Exception {
        // Initialize the database
        siteChatgroupToLanguageRepository.saveAndFlush(siteChatgroupToLanguage);

        // Get the siteChatgroupToLanguage
        restSiteChatgroupToLanguageMockMvc.perform(get("/api/site-chatgroup-to-languages/{id}", siteChatgroupToLanguage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(siteChatgroupToLanguage.getId().intValue()));
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupToLanguagesBySiteChatgroupIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupToLanguage.setSiteChatgroup(siteChatgroup);
        siteChatgroupToLanguageRepository.saveAndFlush(siteChatgroupToLanguage);
        Long siteChatgroupId = siteChatgroup.getId();

        // Get all the siteChatgroupToLanguageList where siteChatgroup equals to siteChatgroupId
        defaultSiteChatgroupToLanguageShouldBeFound("siteChatgroupId.equals=" + siteChatgroupId);

        // Get all the siteChatgroupToLanguageList where siteChatgroup equals to siteChatgroupId + 1
        defaultSiteChatgroupToLanguageShouldNotBeFound("siteChatgroupId.equals=" + (siteChatgroupId + 1));
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupToLanguagesByLanguageIsEqualToSomething() throws Exception {
        // Initialize the database
        Language language = LanguageResourceIntTest.createEntity(em);
        em.persist(language);
        em.flush();
        siteChatgroupToLanguage.setLanguage(language);
        siteChatgroupToLanguageRepository.saveAndFlush(siteChatgroupToLanguage);
        Long languageId = language.getId();

        // Get all the siteChatgroupToLanguageList where language equals to languageId
        defaultSiteChatgroupToLanguageShouldBeFound("languageId.equals=" + languageId);

        // Get all the siteChatgroupToLanguageList where language equals to languageId + 1
        defaultSiteChatgroupToLanguageShouldNotBeFound("languageId.equals=" + (languageId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSiteChatgroupToLanguageShouldBeFound(String filter) throws Exception {
        restSiteChatgroupToLanguageMockMvc.perform(get("/api/site-chatgroup-to-languages?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupToLanguage.getId().intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSiteChatgroupToLanguageShouldNotBeFound(String filter) throws Exception {
        restSiteChatgroupToLanguageMockMvc.perform(get("/api/site-chatgroup-to-languages?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingSiteChatgroupToLanguage() throws Exception {
        // Get the siteChatgroupToLanguage
        restSiteChatgroupToLanguageMockMvc.perform(get("/api/site-chatgroup-to-languages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSiteChatgroupToLanguage() throws Exception {
        // Initialize the database
        siteChatgroupToLanguageService.save(siteChatgroupToLanguage);

        int databaseSizeBeforeUpdate = siteChatgroupToLanguageRepository.findAll().size();

        // Update the siteChatgroupToLanguage
        SiteChatgroupToLanguage updatedSiteChatgroupToLanguage = siteChatgroupToLanguageRepository.findOne(siteChatgroupToLanguage.getId());

        restSiteChatgroupToLanguageMockMvc.perform(put("/api/site-chatgroup-to-languages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSiteChatgroupToLanguage)))
            .andExpect(status().isOk());

        // Validate the SiteChatgroupToLanguage in the database
        List<SiteChatgroupToLanguage> siteChatgroupToLanguageList = siteChatgroupToLanguageRepository.findAll();
        assertThat(siteChatgroupToLanguageList).hasSize(databaseSizeBeforeUpdate);
        SiteChatgroupToLanguage testSiteChatgroupToLanguage = siteChatgroupToLanguageList.get(siteChatgroupToLanguageList.size() - 1);

        // Validate the SiteChatgroupToLanguage in Elasticsearch
        SiteChatgroupToLanguage siteChatgroupToLanguageEs = siteChatgroupToLanguageSearchRepository.findOne(testSiteChatgroupToLanguage.getId());
        assertThat(siteChatgroupToLanguageEs).isEqualToComparingFieldByField(testSiteChatgroupToLanguage);
    }

    @Test
    @Transactional
    public void updateNonExistingSiteChatgroupToLanguage() throws Exception {
        int databaseSizeBeforeUpdate = siteChatgroupToLanguageRepository.findAll().size();

        // Create the SiteChatgroupToLanguage

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSiteChatgroupToLanguageMockMvc.perform(put("/api/site-chatgroup-to-languages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupToLanguage)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupToLanguage in the database
        List<SiteChatgroupToLanguage> siteChatgroupToLanguageList = siteChatgroupToLanguageRepository.findAll();
        assertThat(siteChatgroupToLanguageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSiteChatgroupToLanguage() throws Exception {
        // Initialize the database
        siteChatgroupToLanguageService.save(siteChatgroupToLanguage);

        int databaseSizeBeforeDelete = siteChatgroupToLanguageRepository.findAll().size();

        // Get the siteChatgroupToLanguage
        restSiteChatgroupToLanguageMockMvc.perform(delete("/api/site-chatgroup-to-languages/{id}", siteChatgroupToLanguage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean siteChatgroupToLanguageExistsInEs = siteChatgroupToLanguageSearchRepository.exists(siteChatgroupToLanguage.getId());
        assertThat(siteChatgroupToLanguageExistsInEs).isFalse();

        // Validate the database is empty
        List<SiteChatgroupToLanguage> siteChatgroupToLanguageList = siteChatgroupToLanguageRepository.findAll();
        assertThat(siteChatgroupToLanguageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSiteChatgroupToLanguage() throws Exception {
        // Initialize the database
        siteChatgroupToLanguageService.save(siteChatgroupToLanguage);

        // Search the siteChatgroupToLanguage
        restSiteChatgroupToLanguageMockMvc.perform(get("/api/_search/site-chatgroup-to-languages?query=id:" + siteChatgroupToLanguage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupToLanguage.getId().intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SiteChatgroupToLanguage.class);
        SiteChatgroupToLanguage siteChatgroupToLanguage1 = new SiteChatgroupToLanguage();
        siteChatgroupToLanguage1.setId(1L);
        SiteChatgroupToLanguage siteChatgroupToLanguage2 = new SiteChatgroupToLanguage();
        siteChatgroupToLanguage2.setId(siteChatgroupToLanguage1.getId());
        assertThat(siteChatgroupToLanguage1).isEqualTo(siteChatgroupToLanguage2);
        siteChatgroupToLanguage2.setId(2L);
        assertThat(siteChatgroupToLanguage1).isNotEqualTo(siteChatgroupToLanguage2);
        siteChatgroupToLanguage1.setId(null);
        assertThat(siteChatgroupToLanguage1).isNotEqualTo(siteChatgroupToLanguage2);
    }
}
