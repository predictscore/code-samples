package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsSummaryComment;
import biz.newparadigm.meridian.domain.NewsSummary;
import biz.newparadigm.meridian.domain.NewsSummaryComment;
import biz.newparadigm.meridian.repository.NewsSummaryCommentRepository;
import biz.newparadigm.meridian.service.NewsSummaryCommentService;
import biz.newparadigm.meridian.repository.search.NewsSummaryCommentSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsSummaryCommentCriteria;
import biz.newparadigm.meridian.service.NewsSummaryCommentQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsSummaryCommentResource REST controller.
 *
 * @see NewsSummaryCommentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsSummaryCommentResourceIntTest {

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    @Autowired
    private NewsSummaryCommentRepository newsSummaryCommentRepository;

    @Autowired
    private NewsSummaryCommentService newsSummaryCommentService;

    @Autowired
    private NewsSummaryCommentSearchRepository newsSummaryCommentSearchRepository;

    @Autowired
    private NewsSummaryCommentQueryService newsSummaryCommentQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsSummaryCommentMockMvc;

    private NewsSummaryComment newsSummaryComment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsSummaryCommentResource newsSummaryCommentResource = new NewsSummaryCommentResource(newsSummaryCommentService, newsSummaryCommentQueryService);
        this.restNewsSummaryCommentMockMvc = MockMvcBuilders.standaloneSetup(newsSummaryCommentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsSummaryComment createEntity(EntityManager em) {
        NewsSummaryComment newsSummaryComment = new NewsSummaryComment()
            .userId(DEFAULT_USER_ID)
            .comment(DEFAULT_COMMENT);
        // Add required entity
        NewsSummary newsSummary = NewsSummaryResourceIntTest.createEntity(em);
        em.persist(newsSummary);
        em.flush();
        newsSummaryComment.setNewsSummary(newsSummary);
        return newsSummaryComment;
    }

    @Before
    public void initTest() {
        newsSummaryCommentSearchRepository.deleteAll();
        newsSummaryComment = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsSummaryComment() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryCommentRepository.findAll().size();

        // Create the NewsSummaryComment
        restNewsSummaryCommentMockMvc.perform(post("/api/news-summary-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryComment)))
            .andExpect(status().isCreated());

        // Validate the NewsSummaryComment in the database
        List<NewsSummaryComment> newsSummaryCommentList = newsSummaryCommentRepository.findAll();
        assertThat(newsSummaryCommentList).hasSize(databaseSizeBeforeCreate + 1);
        NewsSummaryComment testNewsSummaryComment = newsSummaryCommentList.get(newsSummaryCommentList.size() - 1);
        assertThat(testNewsSummaryComment.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testNewsSummaryComment.getComment()).isEqualTo(DEFAULT_COMMENT);

        // Validate the NewsSummaryComment in Elasticsearch
        NewsSummaryComment newsSummaryCommentEs = newsSummaryCommentSearchRepository.findOne(testNewsSummaryComment.getId());
        assertThat(newsSummaryCommentEs).isEqualToComparingFieldByField(testNewsSummaryComment);
    }

    @Test
    @Transactional
    public void createNewsSummaryCommentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryCommentRepository.findAll().size();

        // Create the NewsSummaryComment with an existing ID
        newsSummaryComment.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsSummaryCommentMockMvc.perform(post("/api/news-summary-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryComment)))
            .andExpect(status().isBadRequest());

        // Validate the NewsSummaryComment in the database
        List<NewsSummaryComment> newsSummaryCommentList = newsSummaryCommentRepository.findAll();
        assertThat(newsSummaryCommentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsSummaryCommentRepository.findAll().size();
        // set the field null
        newsSummaryComment.setUserId(null);

        // Create the NewsSummaryComment, which fails.

        restNewsSummaryCommentMockMvc.perform(post("/api/news-summary-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryComment)))
            .andExpect(status().isBadRequest());

        List<NewsSummaryComment> newsSummaryCommentList = newsSummaryCommentRepository.findAll();
        assertThat(newsSummaryCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCommentIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsSummaryCommentRepository.findAll().size();
        // set the field null
        newsSummaryComment.setComment(null);

        // Create the NewsSummaryComment, which fails.

        restNewsSummaryCommentMockMvc.perform(post("/api/news-summary-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryComment)))
            .andExpect(status().isBadRequest());

        List<NewsSummaryComment> newsSummaryCommentList = newsSummaryCommentRepository.findAll();
        assertThat(newsSummaryCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryComments() throws Exception {
        // Initialize the database
        newsSummaryCommentRepository.saveAndFlush(newsSummaryComment);

        // Get all the newsSummaryCommentList
        restNewsSummaryCommentMockMvc.perform(get("/api/news-summary-comments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())));
    }

    @Test
    @Transactional
    public void getNewsSummaryComment() throws Exception {
        // Initialize the database
        newsSummaryCommentRepository.saveAndFlush(newsSummaryComment);

        // Get the newsSummaryComment
        restNewsSummaryCommentMockMvc.perform(get("/api/news-summary-comments/{id}", newsSummaryComment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsSummaryComment.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.toString()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()));
    }

    @Test
    @Transactional
    public void getAllNewsSummaryCommentsByUserIdIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryCommentRepository.saveAndFlush(newsSummaryComment);

        // Get all the newsSummaryCommentList where userId equals to DEFAULT_USER_ID
        defaultNewsSummaryCommentShouldBeFound("userId.equals=" + DEFAULT_USER_ID);

        // Get all the newsSummaryCommentList where userId equals to UPDATED_USER_ID
        defaultNewsSummaryCommentShouldNotBeFound("userId.equals=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryCommentsByUserIdIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryCommentRepository.saveAndFlush(newsSummaryComment);

        // Get all the newsSummaryCommentList where userId in DEFAULT_USER_ID or UPDATED_USER_ID
        defaultNewsSummaryCommentShouldBeFound("userId.in=" + DEFAULT_USER_ID + "," + UPDATED_USER_ID);

        // Get all the newsSummaryCommentList where userId equals to UPDATED_USER_ID
        defaultNewsSummaryCommentShouldNotBeFound("userId.in=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryCommentsByUserIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryCommentRepository.saveAndFlush(newsSummaryComment);

        // Get all the newsSummaryCommentList where userId is not null
        defaultNewsSummaryCommentShouldBeFound("userId.specified=true");

        // Get all the newsSummaryCommentList where userId is null
        defaultNewsSummaryCommentShouldNotBeFound("userId.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryCommentsByCommentIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryCommentRepository.saveAndFlush(newsSummaryComment);

        // Get all the newsSummaryCommentList where comment equals to DEFAULT_COMMENT
        defaultNewsSummaryCommentShouldBeFound("comment.equals=" + DEFAULT_COMMENT);

        // Get all the newsSummaryCommentList where comment equals to UPDATED_COMMENT
        defaultNewsSummaryCommentShouldNotBeFound("comment.equals=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryCommentsByCommentIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryCommentRepository.saveAndFlush(newsSummaryComment);

        // Get all the newsSummaryCommentList where comment in DEFAULT_COMMENT or UPDATED_COMMENT
        defaultNewsSummaryCommentShouldBeFound("comment.in=" + DEFAULT_COMMENT + "," + UPDATED_COMMENT);

        // Get all the newsSummaryCommentList where comment equals to UPDATED_COMMENT
        defaultNewsSummaryCommentShouldNotBeFound("comment.in=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryCommentsByCommentIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryCommentRepository.saveAndFlush(newsSummaryComment);

        // Get all the newsSummaryCommentList where comment is not null
        defaultNewsSummaryCommentShouldBeFound("comment.specified=true");

        // Get all the newsSummaryCommentList where comment is null
        defaultNewsSummaryCommentShouldNotBeFound("comment.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryCommentsByNewsSummaryIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsSummary newsSummary = NewsSummaryResourceIntTest.createEntity(em);
        em.persist(newsSummary);
        em.flush();
        newsSummaryComment.setNewsSummary(newsSummary);
        newsSummaryCommentRepository.saveAndFlush(newsSummaryComment);
        Long newsSummaryId = newsSummary.getId();

        // Get all the newsSummaryCommentList where newsSummary equals to newsSummaryId
        defaultNewsSummaryCommentShouldBeFound("newsSummaryId.equals=" + newsSummaryId);

        // Get all the newsSummaryCommentList where newsSummary equals to newsSummaryId + 1
        defaultNewsSummaryCommentShouldNotBeFound("newsSummaryId.equals=" + (newsSummaryId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsSummaryCommentsByParentCommentIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsSummaryComment parentComment = NewsSummaryCommentResourceIntTest.createEntity(em);
        em.persist(parentComment);
        em.flush();
        newsSummaryComment.setParentComment(parentComment);
        newsSummaryCommentRepository.saveAndFlush(newsSummaryComment);
        Long parentCommentId = parentComment.getId();

        // Get all the newsSummaryCommentList where parentComment equals to parentCommentId
        defaultNewsSummaryCommentShouldBeFound("parentCommentId.equals=" + parentCommentId);

        // Get all the newsSummaryCommentList where parentComment equals to parentCommentId + 1
        defaultNewsSummaryCommentShouldNotBeFound("parentCommentId.equals=" + (parentCommentId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsSummaryCommentShouldBeFound(String filter) throws Exception {
        restNewsSummaryCommentMockMvc.perform(get("/api/news-summary-comments?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsSummaryCommentShouldNotBeFound(String filter) throws Exception {
        restNewsSummaryCommentMockMvc.perform(get("/api/news-summary-comments?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsSummaryComment() throws Exception {
        // Get the newsSummaryComment
        restNewsSummaryCommentMockMvc.perform(get("/api/news-summary-comments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsSummaryComment() throws Exception {
        // Initialize the database
        newsSummaryCommentService.save(newsSummaryComment);

        int databaseSizeBeforeUpdate = newsSummaryCommentRepository.findAll().size();

        // Update the newsSummaryComment
        NewsSummaryComment updatedNewsSummaryComment = newsSummaryCommentRepository.findOne(newsSummaryComment.getId());
        updatedNewsSummaryComment
            .userId(UPDATED_USER_ID)
            .comment(UPDATED_COMMENT);

        restNewsSummaryCommentMockMvc.perform(put("/api/news-summary-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsSummaryComment)))
            .andExpect(status().isOk());

        // Validate the NewsSummaryComment in the database
        List<NewsSummaryComment> newsSummaryCommentList = newsSummaryCommentRepository.findAll();
        assertThat(newsSummaryCommentList).hasSize(databaseSizeBeforeUpdate);
        NewsSummaryComment testNewsSummaryComment = newsSummaryCommentList.get(newsSummaryCommentList.size() - 1);
        assertThat(testNewsSummaryComment.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testNewsSummaryComment.getComment()).isEqualTo(UPDATED_COMMENT);

        // Validate the NewsSummaryComment in Elasticsearch
        NewsSummaryComment newsSummaryCommentEs = newsSummaryCommentSearchRepository.findOne(testNewsSummaryComment.getId());
        assertThat(newsSummaryCommentEs).isEqualToComparingFieldByField(testNewsSummaryComment);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsSummaryComment() throws Exception {
        int databaseSizeBeforeUpdate = newsSummaryCommentRepository.findAll().size();

        // Create the NewsSummaryComment

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsSummaryCommentMockMvc.perform(put("/api/news-summary-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryComment)))
            .andExpect(status().isCreated());

        // Validate the NewsSummaryComment in the database
        List<NewsSummaryComment> newsSummaryCommentList = newsSummaryCommentRepository.findAll();
        assertThat(newsSummaryCommentList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsSummaryComment() throws Exception {
        // Initialize the database
        newsSummaryCommentService.save(newsSummaryComment);

        int databaseSizeBeforeDelete = newsSummaryCommentRepository.findAll().size();

        // Get the newsSummaryComment
        restNewsSummaryCommentMockMvc.perform(delete("/api/news-summary-comments/{id}", newsSummaryComment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsSummaryCommentExistsInEs = newsSummaryCommentSearchRepository.exists(newsSummaryComment.getId());
        assertThat(newsSummaryCommentExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsSummaryComment> newsSummaryCommentList = newsSummaryCommentRepository.findAll();
        assertThat(newsSummaryCommentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsSummaryComment() throws Exception {
        // Initialize the database
        newsSummaryCommentService.save(newsSummaryComment);

        // Search the newsSummaryComment
        restNewsSummaryCommentMockMvc.perform(get("/api/_search/news-summary-comments?query=id:" + newsSummaryComment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsSummaryComment.class);
        NewsSummaryComment newsSummaryComment1 = new NewsSummaryComment();
        newsSummaryComment1.setId(1L);
        NewsSummaryComment newsSummaryComment2 = new NewsSummaryComment();
        newsSummaryComment2.setId(newsSummaryComment1.getId());
        assertThat(newsSummaryComment1).isEqualTo(newsSummaryComment2);
        newsSummaryComment2.setId(2L);
        assertThat(newsSummaryComment1).isNotEqualTo(newsSummaryComment2);
        newsSummaryComment1.setId(null);
        assertThat(newsSummaryComment1).isNotEqualTo(newsSummaryComment2);
    }
}
