package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsDownloadableContentUrl;
import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.repository.NewsDownloadableContentUrlRepository;
import biz.newparadigm.meridian.service.NewsDownloadableContentUrlService;
import biz.newparadigm.meridian.repository.search.NewsDownloadableContentUrlSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsDownloadableContentUrlCriteria;
import biz.newparadigm.meridian.service.NewsDownloadableContentUrlQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsDownloadableContentUrlResource REST controller.
 *
 * @see NewsDownloadableContentUrlResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsDownloadableContentUrlResourceIntTest {

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private NewsDownloadableContentUrlRepository newsDownloadableContentUrlRepository;

    @Autowired
    private NewsDownloadableContentUrlService newsDownloadableContentUrlService;

    @Autowired
    private NewsDownloadableContentUrlSearchRepository newsDownloadableContentUrlSearchRepository;

    @Autowired
    private NewsDownloadableContentUrlQueryService newsDownloadableContentUrlQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsDownloadableContentUrlMockMvc;

    private NewsDownloadableContentUrl newsDownloadableContentUrl;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsDownloadableContentUrlResource newsDownloadableContentUrlResource = new NewsDownloadableContentUrlResource(newsDownloadableContentUrlService, newsDownloadableContentUrlQueryService);
        this.restNewsDownloadableContentUrlMockMvc = MockMvcBuilders.standaloneSetup(newsDownloadableContentUrlResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsDownloadableContentUrl createEntity(EntityManager em) {
        NewsDownloadableContentUrl newsDownloadableContentUrl = new NewsDownloadableContentUrl()
            .url(DEFAULT_URL)
            .description(DEFAULT_DESCRIPTION);
        // Add required entity
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsDownloadableContentUrl.setNews(news);
        return newsDownloadableContentUrl;
    }

    @Before
    public void initTest() {
        newsDownloadableContentUrlSearchRepository.deleteAll();
        newsDownloadableContentUrl = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsDownloadableContentUrl() throws Exception {
        int databaseSizeBeforeCreate = newsDownloadableContentUrlRepository.findAll().size();

        // Create the NewsDownloadableContentUrl
        restNewsDownloadableContentUrlMockMvc.perform(post("/api/news-downloadable-content-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsDownloadableContentUrl)))
            .andExpect(status().isCreated());

        // Validate the NewsDownloadableContentUrl in the database
        List<NewsDownloadableContentUrl> newsDownloadableContentUrlList = newsDownloadableContentUrlRepository.findAll();
        assertThat(newsDownloadableContentUrlList).hasSize(databaseSizeBeforeCreate + 1);
        NewsDownloadableContentUrl testNewsDownloadableContentUrl = newsDownloadableContentUrlList.get(newsDownloadableContentUrlList.size() - 1);
        assertThat(testNewsDownloadableContentUrl.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testNewsDownloadableContentUrl.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the NewsDownloadableContentUrl in Elasticsearch
        NewsDownloadableContentUrl newsDownloadableContentUrlEs = newsDownloadableContentUrlSearchRepository.findOne(testNewsDownloadableContentUrl.getId());
        assertThat(newsDownloadableContentUrlEs).isEqualToComparingFieldByField(testNewsDownloadableContentUrl);
    }

    @Test
    @Transactional
    public void createNewsDownloadableContentUrlWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsDownloadableContentUrlRepository.findAll().size();

        // Create the NewsDownloadableContentUrl with an existing ID
        newsDownloadableContentUrl.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsDownloadableContentUrlMockMvc.perform(post("/api/news-downloadable-content-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsDownloadableContentUrl)))
            .andExpect(status().isBadRequest());

        // Validate the NewsDownloadableContentUrl in the database
        List<NewsDownloadableContentUrl> newsDownloadableContentUrlList = newsDownloadableContentUrlRepository.findAll();
        assertThat(newsDownloadableContentUrlList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUrlIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsDownloadableContentUrlRepository.findAll().size();
        // set the field null
        newsDownloadableContentUrl.setUrl(null);

        // Create the NewsDownloadableContentUrl, which fails.

        restNewsDownloadableContentUrlMockMvc.perform(post("/api/news-downloadable-content-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsDownloadableContentUrl)))
            .andExpect(status().isBadRequest());

        List<NewsDownloadableContentUrl> newsDownloadableContentUrlList = newsDownloadableContentUrlRepository.findAll();
        assertThat(newsDownloadableContentUrlList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNewsDownloadableContentUrls() throws Exception {
        // Initialize the database
        newsDownloadableContentUrlRepository.saveAndFlush(newsDownloadableContentUrl);

        // Get all the newsDownloadableContentUrlList
        restNewsDownloadableContentUrlMockMvc.perform(get("/api/news-downloadable-content-urls?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsDownloadableContentUrl.getId().intValue())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getNewsDownloadableContentUrl() throws Exception {
        // Initialize the database
        newsDownloadableContentUrlRepository.saveAndFlush(newsDownloadableContentUrl);

        // Get the newsDownloadableContentUrl
        restNewsDownloadableContentUrlMockMvc.perform(get("/api/news-downloadable-content-urls/{id}", newsDownloadableContentUrl.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsDownloadableContentUrl.getId().intValue()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getAllNewsDownloadableContentUrlsByUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        newsDownloadableContentUrlRepository.saveAndFlush(newsDownloadableContentUrl);

        // Get all the newsDownloadableContentUrlList where url equals to DEFAULT_URL
        defaultNewsDownloadableContentUrlShouldBeFound("url.equals=" + DEFAULT_URL);

        // Get all the newsDownloadableContentUrlList where url equals to UPDATED_URL
        defaultNewsDownloadableContentUrlShouldNotBeFound("url.equals=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllNewsDownloadableContentUrlsByUrlIsInShouldWork() throws Exception {
        // Initialize the database
        newsDownloadableContentUrlRepository.saveAndFlush(newsDownloadableContentUrl);

        // Get all the newsDownloadableContentUrlList where url in DEFAULT_URL or UPDATED_URL
        defaultNewsDownloadableContentUrlShouldBeFound("url.in=" + DEFAULT_URL + "," + UPDATED_URL);

        // Get all the newsDownloadableContentUrlList where url equals to UPDATED_URL
        defaultNewsDownloadableContentUrlShouldNotBeFound("url.in=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllNewsDownloadableContentUrlsByUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsDownloadableContentUrlRepository.saveAndFlush(newsDownloadableContentUrl);

        // Get all the newsDownloadableContentUrlList where url is not null
        defaultNewsDownloadableContentUrlShouldBeFound("url.specified=true");

        // Get all the newsDownloadableContentUrlList where url is null
        defaultNewsDownloadableContentUrlShouldNotBeFound("url.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsDownloadableContentUrlsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        newsDownloadableContentUrlRepository.saveAndFlush(newsDownloadableContentUrl);

        // Get all the newsDownloadableContentUrlList where description equals to DEFAULT_DESCRIPTION
        defaultNewsDownloadableContentUrlShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the newsDownloadableContentUrlList where description equals to UPDATED_DESCRIPTION
        defaultNewsDownloadableContentUrlShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsDownloadableContentUrlsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        newsDownloadableContentUrlRepository.saveAndFlush(newsDownloadableContentUrl);

        // Get all the newsDownloadableContentUrlList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultNewsDownloadableContentUrlShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the newsDownloadableContentUrlList where description equals to UPDATED_DESCRIPTION
        defaultNewsDownloadableContentUrlShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsDownloadableContentUrlsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsDownloadableContentUrlRepository.saveAndFlush(newsDownloadableContentUrl);

        // Get all the newsDownloadableContentUrlList where description is not null
        defaultNewsDownloadableContentUrlShouldBeFound("description.specified=true");

        // Get all the newsDownloadableContentUrlList where description is null
        defaultNewsDownloadableContentUrlShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsDownloadableContentUrlsByNewsIsEqualToSomething() throws Exception {
        // Initialize the database
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsDownloadableContentUrl.setNews(news);
        newsDownloadableContentUrlRepository.saveAndFlush(newsDownloadableContentUrl);
        Long newsId = news.getId();

        // Get all the newsDownloadableContentUrlList where news equals to newsId
        defaultNewsDownloadableContentUrlShouldBeFound("newsId.equals=" + newsId);

        // Get all the newsDownloadableContentUrlList where news equals to newsId + 1
        defaultNewsDownloadableContentUrlShouldNotBeFound("newsId.equals=" + (newsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsDownloadableContentUrlShouldBeFound(String filter) throws Exception {
        restNewsDownloadableContentUrlMockMvc.perform(get("/api/news-downloadable-content-urls?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsDownloadableContentUrl.getId().intValue())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsDownloadableContentUrlShouldNotBeFound(String filter) throws Exception {
        restNewsDownloadableContentUrlMockMvc.perform(get("/api/news-downloadable-content-urls?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsDownloadableContentUrl() throws Exception {
        // Get the newsDownloadableContentUrl
        restNewsDownloadableContentUrlMockMvc.perform(get("/api/news-downloadable-content-urls/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsDownloadableContentUrl() throws Exception {
        // Initialize the database
        newsDownloadableContentUrlService.save(newsDownloadableContentUrl);

        int databaseSizeBeforeUpdate = newsDownloadableContentUrlRepository.findAll().size();

        // Update the newsDownloadableContentUrl
        NewsDownloadableContentUrl updatedNewsDownloadableContentUrl = newsDownloadableContentUrlRepository.findOne(newsDownloadableContentUrl.getId());
        updatedNewsDownloadableContentUrl
            .url(UPDATED_URL)
            .description(UPDATED_DESCRIPTION);

        restNewsDownloadableContentUrlMockMvc.perform(put("/api/news-downloadable-content-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsDownloadableContentUrl)))
            .andExpect(status().isOk());

        // Validate the NewsDownloadableContentUrl in the database
        List<NewsDownloadableContentUrl> newsDownloadableContentUrlList = newsDownloadableContentUrlRepository.findAll();
        assertThat(newsDownloadableContentUrlList).hasSize(databaseSizeBeforeUpdate);
        NewsDownloadableContentUrl testNewsDownloadableContentUrl = newsDownloadableContentUrlList.get(newsDownloadableContentUrlList.size() - 1);
        assertThat(testNewsDownloadableContentUrl.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testNewsDownloadableContentUrl.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the NewsDownloadableContentUrl in Elasticsearch
        NewsDownloadableContentUrl newsDownloadableContentUrlEs = newsDownloadableContentUrlSearchRepository.findOne(testNewsDownloadableContentUrl.getId());
        assertThat(newsDownloadableContentUrlEs).isEqualToComparingFieldByField(testNewsDownloadableContentUrl);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsDownloadableContentUrl() throws Exception {
        int databaseSizeBeforeUpdate = newsDownloadableContentUrlRepository.findAll().size();

        // Create the NewsDownloadableContentUrl

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsDownloadableContentUrlMockMvc.perform(put("/api/news-downloadable-content-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsDownloadableContentUrl)))
            .andExpect(status().isCreated());

        // Validate the NewsDownloadableContentUrl in the database
        List<NewsDownloadableContentUrl> newsDownloadableContentUrlList = newsDownloadableContentUrlRepository.findAll();
        assertThat(newsDownloadableContentUrlList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsDownloadableContentUrl() throws Exception {
        // Initialize the database
        newsDownloadableContentUrlService.save(newsDownloadableContentUrl);

        int databaseSizeBeforeDelete = newsDownloadableContentUrlRepository.findAll().size();

        // Get the newsDownloadableContentUrl
        restNewsDownloadableContentUrlMockMvc.perform(delete("/api/news-downloadable-content-urls/{id}", newsDownloadableContentUrl.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsDownloadableContentUrlExistsInEs = newsDownloadableContentUrlSearchRepository.exists(newsDownloadableContentUrl.getId());
        assertThat(newsDownloadableContentUrlExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsDownloadableContentUrl> newsDownloadableContentUrlList = newsDownloadableContentUrlRepository.findAll();
        assertThat(newsDownloadableContentUrlList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsDownloadableContentUrl() throws Exception {
        // Initialize the database
        newsDownloadableContentUrlService.save(newsDownloadableContentUrl);

        // Search the newsDownloadableContentUrl
        restNewsDownloadableContentUrlMockMvc.perform(get("/api/_search/news-downloadable-content-urls?query=id:" + newsDownloadableContentUrl.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsDownloadableContentUrl.getId().intValue())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsDownloadableContentUrl.class);
        NewsDownloadableContentUrl newsDownloadableContentUrl1 = new NewsDownloadableContentUrl();
        newsDownloadableContentUrl1.setId(1L);
        NewsDownloadableContentUrl newsDownloadableContentUrl2 = new NewsDownloadableContentUrl();
        newsDownloadableContentUrl2.setId(newsDownloadableContentUrl1.getId());
        assertThat(newsDownloadableContentUrl1).isEqualTo(newsDownloadableContentUrl2);
        newsDownloadableContentUrl2.setId(2L);
        assertThat(newsDownloadableContentUrl1).isNotEqualTo(newsDownloadableContentUrl2);
        newsDownloadableContentUrl1.setId(null);
        assertThat(newsDownloadableContentUrl1).isNotEqualTo(newsDownloadableContentUrl2);
    }
}
