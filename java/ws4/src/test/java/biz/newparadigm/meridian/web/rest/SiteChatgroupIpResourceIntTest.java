package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.SiteChatgroupIp;
import biz.newparadigm.meridian.domain.SiteChatgroup;
import biz.newparadigm.meridian.repository.SiteChatgroupIpRepository;
import biz.newparadigm.meridian.service.SiteChatgroupIpService;
import biz.newparadigm.meridian.repository.search.SiteChatgroupIpSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.SiteChatgroupIpCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupIpQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.sameInstant;
import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SiteChatgroupIpResource REST controller.
 *
 * @see SiteChatgroupIpResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class SiteChatgroupIpResourceIntTest {

    private static final String DEFAULT_IP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_IP_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_IP_WHOIS = "AAAAAAAAAA";
    private static final String UPDATED_IP_WHOIS = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_FIRST_USED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FIRST_USED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_LAST_USED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_USED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private SiteChatgroupIpRepository siteChatgroupIpRepository;

    @Autowired
    private SiteChatgroupIpService siteChatgroupIpService;

    @Autowired
    private SiteChatgroupIpSearchRepository siteChatgroupIpSearchRepository;

    @Autowired
    private SiteChatgroupIpQueryService siteChatgroupIpQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSiteChatgroupIpMockMvc;

    private SiteChatgroupIp siteChatgroupIp;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SiteChatgroupIpResource siteChatgroupIpResource = new SiteChatgroupIpResource(siteChatgroupIpService, siteChatgroupIpQueryService);
        this.restSiteChatgroupIpMockMvc = MockMvcBuilders.standaloneSetup(siteChatgroupIpResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SiteChatgroupIp createEntity(EntityManager em) {
        SiteChatgroupIp siteChatgroupIp = new SiteChatgroupIp()
            //.ipName(DEFAULT_IP_NAME)
            .ipWhois(DEFAULT_IP_WHOIS)
            .firstUsedDate(DEFAULT_FIRST_USED_DATE)
            .lastUsedDate(DEFAULT_LAST_USED_DATE);
        // Add required entity
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupIp.setSiteChatgroup(siteChatgroup);
        return siteChatgroupIp;
    }

    @Before
    public void initTest() {
        siteChatgroupIpSearchRepository.deleteAll();
        siteChatgroupIp = createEntity(em);
    }

    @Test
    @Transactional
    public void createSiteChatgroupIp() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupIpRepository.findAll().size();

        // Create the SiteChatgroupIp
        restSiteChatgroupIpMockMvc.perform(post("/api/site-chatgroup-ips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupIp)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupIp in the database
        List<SiteChatgroupIp> siteChatgroupIpList = siteChatgroupIpRepository.findAll();
        assertThat(siteChatgroupIpList).hasSize(databaseSizeBeforeCreate + 1);
        SiteChatgroupIp testSiteChatgroupIp = siteChatgroupIpList.get(siteChatgroupIpList.size() - 1);
        assertThat(testSiteChatgroupIp.getIpName()).isEqualTo(DEFAULT_IP_NAME);
        assertThat(testSiteChatgroupIp.getIpWhois()).isEqualTo(DEFAULT_IP_WHOIS);
        assertThat(testSiteChatgroupIp.getFirstUsedDate()).isEqualTo(DEFAULT_FIRST_USED_DATE);
        assertThat(testSiteChatgroupIp.getLastUsedDate()).isEqualTo(DEFAULT_LAST_USED_DATE);

        // Validate the SiteChatgroupIp in Elasticsearch
        SiteChatgroupIp siteChatgroupIpEs = siteChatgroupIpSearchRepository.findOne(testSiteChatgroupIp.getId());
        assertThat(siteChatgroupIpEs).isEqualToComparingFieldByField(testSiteChatgroupIp);
    }

    @Test
    @Transactional
    public void createSiteChatgroupIpWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupIpRepository.findAll().size();

        // Create the SiteChatgroupIp with an existing ID
        siteChatgroupIp.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSiteChatgroupIpMockMvc.perform(post("/api/site-chatgroup-ips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupIp)))
            .andExpect(status().isBadRequest());

        // Validate the SiteChatgroupIp in the database
        List<SiteChatgroupIp> siteChatgroupIpList = siteChatgroupIpRepository.findAll();
        assertThat(siteChatgroupIpList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkIpNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupIpRepository.findAll().size();
        // set the field null
        siteChatgroupIp.setIpName(null);

        // Create the SiteChatgroupIp, which fails.

        restSiteChatgroupIpMockMvc.perform(post("/api/site-chatgroup-ips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupIp)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupIp> siteChatgroupIpList = siteChatgroupIpRepository.findAll();
        assertThat(siteChatgroupIpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIpWhoisIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupIpRepository.findAll().size();
        // set the field null
        siteChatgroupIp.setIpWhois(null);

        // Create the SiteChatgroupIp, which fails.

        restSiteChatgroupIpMockMvc.perform(post("/api/site-chatgroup-ips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupIp)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupIp> siteChatgroupIpList = siteChatgroupIpRepository.findAll();
        assertThat(siteChatgroupIpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIps() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList
        restSiteChatgroupIpMockMvc.perform(get("/api/site-chatgroup-ips?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupIp.getId().intValue())))
            .andExpect(jsonPath("$.[*].ipName").value(hasItem(DEFAULT_IP_NAME.toString())))
            .andExpect(jsonPath("$.[*].ipWhois").value(hasItem(DEFAULT_IP_WHOIS.toString())))
            .andExpect(jsonPath("$.[*].firstUsedDate").value(hasItem(sameInstant(DEFAULT_FIRST_USED_DATE))))
            .andExpect(jsonPath("$.[*].lastUsedDate").value(hasItem(sameInstant(DEFAULT_LAST_USED_DATE))));
    }

    @Test
    @Transactional
    public void getSiteChatgroupIp() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get the siteChatgroupIp
        restSiteChatgroupIpMockMvc.perform(get("/api/site-chatgroup-ips/{id}", siteChatgroupIp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(siteChatgroupIp.getId().intValue()))
            .andExpect(jsonPath("$.ipName").value(DEFAULT_IP_NAME.toString()))
            .andExpect(jsonPath("$.ipWhois").value(DEFAULT_IP_WHOIS.toString()))
            .andExpect(jsonPath("$.firstUsedDate").value(sameInstant(DEFAULT_FIRST_USED_DATE)))
            .andExpect(jsonPath("$.lastUsedDate").value(sameInstant(DEFAULT_LAST_USED_DATE)));
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByIpNameIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where ipName equals to DEFAULT_IP_NAME
        defaultSiteChatgroupIpShouldBeFound("ipName.equals=" + DEFAULT_IP_NAME);

        // Get all the siteChatgroupIpList where ipName equals to UPDATED_IP_NAME
        defaultSiteChatgroupIpShouldNotBeFound("ipName.equals=" + UPDATED_IP_NAME);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByIpNameIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where ipName in DEFAULT_IP_NAME or UPDATED_IP_NAME
        defaultSiteChatgroupIpShouldBeFound("ipName.in=" + DEFAULT_IP_NAME + "," + UPDATED_IP_NAME);

        // Get all the siteChatgroupIpList where ipName equals to UPDATED_IP_NAME
        defaultSiteChatgroupIpShouldNotBeFound("ipName.in=" + UPDATED_IP_NAME);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByIpNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where ipName is not null
        defaultSiteChatgroupIpShouldBeFound("ipName.specified=true");

        // Get all the siteChatgroupIpList where ipName is null
        defaultSiteChatgroupIpShouldNotBeFound("ipName.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByIpWhoisIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where ipWhois equals to DEFAULT_IP_WHOIS
        defaultSiteChatgroupIpShouldBeFound("ipWhois.equals=" + DEFAULT_IP_WHOIS);

        // Get all the siteChatgroupIpList where ipWhois equals to UPDATED_IP_WHOIS
        defaultSiteChatgroupIpShouldNotBeFound("ipWhois.equals=" + UPDATED_IP_WHOIS);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByIpWhoisIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where ipWhois in DEFAULT_IP_WHOIS or UPDATED_IP_WHOIS
        defaultSiteChatgroupIpShouldBeFound("ipWhois.in=" + DEFAULT_IP_WHOIS + "," + UPDATED_IP_WHOIS);

        // Get all the siteChatgroupIpList where ipWhois equals to UPDATED_IP_WHOIS
        defaultSiteChatgroupIpShouldNotBeFound("ipWhois.in=" + UPDATED_IP_WHOIS);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByIpWhoisIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where ipWhois is not null
        defaultSiteChatgroupIpShouldBeFound("ipWhois.specified=true");

        // Get all the siteChatgroupIpList where ipWhois is null
        defaultSiteChatgroupIpShouldNotBeFound("ipWhois.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByFirstUsedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where firstUsedDate equals to DEFAULT_FIRST_USED_DATE
        defaultSiteChatgroupIpShouldBeFound("firstUsedDate.equals=" + DEFAULT_FIRST_USED_DATE);

        // Get all the siteChatgroupIpList where firstUsedDate equals to UPDATED_FIRST_USED_DATE
        defaultSiteChatgroupIpShouldNotBeFound("firstUsedDate.equals=" + UPDATED_FIRST_USED_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByFirstUsedDateIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where firstUsedDate in DEFAULT_FIRST_USED_DATE or UPDATED_FIRST_USED_DATE
        defaultSiteChatgroupIpShouldBeFound("firstUsedDate.in=" + DEFAULT_FIRST_USED_DATE + "," + UPDATED_FIRST_USED_DATE);

        // Get all the siteChatgroupIpList where firstUsedDate equals to UPDATED_FIRST_USED_DATE
        defaultSiteChatgroupIpShouldNotBeFound("firstUsedDate.in=" + UPDATED_FIRST_USED_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByFirstUsedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where firstUsedDate is not null
        defaultSiteChatgroupIpShouldBeFound("firstUsedDate.specified=true");

        // Get all the siteChatgroupIpList where firstUsedDate is null
        defaultSiteChatgroupIpShouldNotBeFound("firstUsedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByFirstUsedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where firstUsedDate greater than or equals to DEFAULT_FIRST_USED_DATE
        defaultSiteChatgroupIpShouldBeFound("firstUsedDate.greaterOrEqualThan=" + DEFAULT_FIRST_USED_DATE);

        // Get all the siteChatgroupIpList where firstUsedDate greater than or equals to UPDATED_FIRST_USED_DATE
        defaultSiteChatgroupIpShouldNotBeFound("firstUsedDate.greaterOrEqualThan=" + UPDATED_FIRST_USED_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByFirstUsedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where firstUsedDate less than or equals to DEFAULT_FIRST_USED_DATE
        defaultSiteChatgroupIpShouldNotBeFound("firstUsedDate.lessThan=" + DEFAULT_FIRST_USED_DATE);

        // Get all the siteChatgroupIpList where firstUsedDate less than or equals to UPDATED_FIRST_USED_DATE
        defaultSiteChatgroupIpShouldBeFound("firstUsedDate.lessThan=" + UPDATED_FIRST_USED_DATE);
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByLastUsedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where lastUsedDate equals to DEFAULT_LAST_USED_DATE
        defaultSiteChatgroupIpShouldBeFound("lastUsedDate.equals=" + DEFAULT_LAST_USED_DATE);

        // Get all the siteChatgroupIpList where lastUsedDate equals to UPDATED_LAST_USED_DATE
        defaultSiteChatgroupIpShouldNotBeFound("lastUsedDate.equals=" + UPDATED_LAST_USED_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByLastUsedDateIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where lastUsedDate in DEFAULT_LAST_USED_DATE or UPDATED_LAST_USED_DATE
        defaultSiteChatgroupIpShouldBeFound("lastUsedDate.in=" + DEFAULT_LAST_USED_DATE + "," + UPDATED_LAST_USED_DATE);

        // Get all the siteChatgroupIpList where lastUsedDate equals to UPDATED_LAST_USED_DATE
        defaultSiteChatgroupIpShouldNotBeFound("lastUsedDate.in=" + UPDATED_LAST_USED_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByLastUsedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where lastUsedDate is not null
        defaultSiteChatgroupIpShouldBeFound("lastUsedDate.specified=true");

        // Get all the siteChatgroupIpList where lastUsedDate is null
        defaultSiteChatgroupIpShouldNotBeFound("lastUsedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByLastUsedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where lastUsedDate greater than or equals to DEFAULT_LAST_USED_DATE
        defaultSiteChatgroupIpShouldBeFound("lastUsedDate.greaterOrEqualThan=" + DEFAULT_LAST_USED_DATE);

        // Get all the siteChatgroupIpList where lastUsedDate greater than or equals to UPDATED_LAST_USED_DATE
        defaultSiteChatgroupIpShouldNotBeFound("lastUsedDate.greaterOrEqualThan=" + UPDATED_LAST_USED_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupIpsByLastUsedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);

        // Get all the siteChatgroupIpList where lastUsedDate less than or equals to DEFAULT_LAST_USED_DATE
        defaultSiteChatgroupIpShouldNotBeFound("lastUsedDate.lessThan=" + DEFAULT_LAST_USED_DATE);

        // Get all the siteChatgroupIpList where lastUsedDate less than or equals to UPDATED_LAST_USED_DATE
        defaultSiteChatgroupIpShouldBeFound("lastUsedDate.lessThan=" + UPDATED_LAST_USED_DATE);
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupIpsBySiteChatgroupIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupIp.setSiteChatgroup(siteChatgroup);
        siteChatgroupIpRepository.saveAndFlush(siteChatgroupIp);
        Long siteChatgroupId = siteChatgroup.getId();

        // Get all the siteChatgroupIpList where siteChatgroup equals to siteChatgroupId
        defaultSiteChatgroupIpShouldBeFound("siteChatgroupId.equals=" + siteChatgroupId);

        // Get all the siteChatgroupIpList where siteChatgroup equals to siteChatgroupId + 1
        defaultSiteChatgroupIpShouldNotBeFound("siteChatgroupId.equals=" + (siteChatgroupId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSiteChatgroupIpShouldBeFound(String filter) throws Exception {
        restSiteChatgroupIpMockMvc.perform(get("/api/site-chatgroup-ips?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupIp.getId().intValue())))
            .andExpect(jsonPath("$.[*].ipName").value(hasItem(DEFAULT_IP_NAME.toString())))
            .andExpect(jsonPath("$.[*].ipWhois").value(hasItem(DEFAULT_IP_WHOIS.toString())))
            .andExpect(jsonPath("$.[*].firstUsedDate").value(hasItem(sameInstant(DEFAULT_FIRST_USED_DATE))))
            .andExpect(jsonPath("$.[*].lastUsedDate").value(hasItem(sameInstant(DEFAULT_LAST_USED_DATE))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSiteChatgroupIpShouldNotBeFound(String filter) throws Exception {
        restSiteChatgroupIpMockMvc.perform(get("/api/site-chatgroup-ips?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingSiteChatgroupIp() throws Exception {
        // Get the siteChatgroupIp
        restSiteChatgroupIpMockMvc.perform(get("/api/site-chatgroup-ips/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSiteChatgroupIp() throws Exception {
        // Initialize the database
        siteChatgroupIpService.save(siteChatgroupIp);

        int databaseSizeBeforeUpdate = siteChatgroupIpRepository.findAll().size();

        // Update the siteChatgroupIp
        SiteChatgroupIp updatedSiteChatgroupIp = siteChatgroupIpRepository.findOne(siteChatgroupIp.getId());
        updatedSiteChatgroupIp
            //.ipName(UPDATED_IP_NAME)
            .ipWhois(UPDATED_IP_WHOIS)
            .firstUsedDate(UPDATED_FIRST_USED_DATE)
            .lastUsedDate(UPDATED_LAST_USED_DATE);

        restSiteChatgroupIpMockMvc.perform(put("/api/site-chatgroup-ips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSiteChatgroupIp)))
            .andExpect(status().isOk());

        // Validate the SiteChatgroupIp in the database
        List<SiteChatgroupIp> siteChatgroupIpList = siteChatgroupIpRepository.findAll();
        assertThat(siteChatgroupIpList).hasSize(databaseSizeBeforeUpdate);
        SiteChatgroupIp testSiteChatgroupIp = siteChatgroupIpList.get(siteChatgroupIpList.size() - 1);
        assertThat(testSiteChatgroupIp.getIpName()).isEqualTo(UPDATED_IP_NAME);
        assertThat(testSiteChatgroupIp.getIpWhois()).isEqualTo(UPDATED_IP_WHOIS);
        assertThat(testSiteChatgroupIp.getFirstUsedDate()).isEqualTo(UPDATED_FIRST_USED_DATE);
        assertThat(testSiteChatgroupIp.getLastUsedDate()).isEqualTo(UPDATED_LAST_USED_DATE);

        // Validate the SiteChatgroupIp in Elasticsearch
        SiteChatgroupIp siteChatgroupIpEs = siteChatgroupIpSearchRepository.findOne(testSiteChatgroupIp.getId());
        assertThat(siteChatgroupIpEs).isEqualToComparingFieldByField(testSiteChatgroupIp);
    }

    @Test
    @Transactional
    public void updateNonExistingSiteChatgroupIp() throws Exception {
        int databaseSizeBeforeUpdate = siteChatgroupIpRepository.findAll().size();

        // Create the SiteChatgroupIp

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSiteChatgroupIpMockMvc.perform(put("/api/site-chatgroup-ips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupIp)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupIp in the database
        List<SiteChatgroupIp> siteChatgroupIpList = siteChatgroupIpRepository.findAll();
        assertThat(siteChatgroupIpList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSiteChatgroupIp() throws Exception {
        // Initialize the database
        siteChatgroupIpService.save(siteChatgroupIp);

        int databaseSizeBeforeDelete = siteChatgroupIpRepository.findAll().size();

        // Get the siteChatgroupIp
        restSiteChatgroupIpMockMvc.perform(delete("/api/site-chatgroup-ips/{id}", siteChatgroupIp.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean siteChatgroupIpExistsInEs = siteChatgroupIpSearchRepository.exists(siteChatgroupIp.getId());
        assertThat(siteChatgroupIpExistsInEs).isFalse();

        // Validate the database is empty
        List<SiteChatgroupIp> siteChatgroupIpList = siteChatgroupIpRepository.findAll();
        assertThat(siteChatgroupIpList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSiteChatgroupIp() throws Exception {
        // Initialize the database
        siteChatgroupIpService.save(siteChatgroupIp);

        // Search the siteChatgroupIp
        restSiteChatgroupIpMockMvc.perform(get("/api/_search/site-chatgroup-ips?query=id:" + siteChatgroupIp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupIp.getId().intValue())))
            .andExpect(jsonPath("$.[*].ipName").value(hasItem(DEFAULT_IP_NAME.toString())))
            .andExpect(jsonPath("$.[*].ipWhois").value(hasItem(DEFAULT_IP_WHOIS.toString())))
            .andExpect(jsonPath("$.[*].firstUsedDate").value(hasItem(sameInstant(DEFAULT_FIRST_USED_DATE))))
            .andExpect(jsonPath("$.[*].lastUsedDate").value(hasItem(sameInstant(DEFAULT_LAST_USED_DATE))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SiteChatgroupIp.class);
        SiteChatgroupIp siteChatgroupIp1 = new SiteChatgroupIp();
        siteChatgroupIp1.setId(1L);
        SiteChatgroupIp siteChatgroupIp2 = new SiteChatgroupIp();
        siteChatgroupIp2.setId(siteChatgroupIp1.getId());
        assertThat(siteChatgroupIp1).isEqualTo(siteChatgroupIp2);
        siteChatgroupIp2.setId(2L);
        assertThat(siteChatgroupIp1).isNotEqualTo(siteChatgroupIp2);
        siteChatgroupIp1.setId(null);
        assertThat(siteChatgroupIp1).isNotEqualTo(siteChatgroupIp2);
    }
}
