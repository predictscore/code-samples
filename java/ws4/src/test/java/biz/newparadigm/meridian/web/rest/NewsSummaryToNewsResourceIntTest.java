package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsSummaryToNews;
import biz.newparadigm.meridian.domain.NewsSummary;
import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.repository.NewsSummaryToNewsRepository;
import biz.newparadigm.meridian.service.NewsSummaryToNewsService;
import biz.newparadigm.meridian.repository.search.NewsSummaryToNewsSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsSummaryToNewsCriteria;
import biz.newparadigm.meridian.service.NewsSummaryToNewsQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsSummaryToNewsResource REST controller.
 *
 * @see NewsSummaryToNewsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsSummaryToNewsResourceIntTest {

    @Autowired
    private NewsSummaryToNewsRepository newsSummaryToNewsRepository;

    @Autowired
    private NewsSummaryToNewsService newsSummaryToNewsService;

    @Autowired
    private NewsSummaryToNewsSearchRepository newsSummaryToNewsSearchRepository;

    @Autowired
    private NewsSummaryToNewsQueryService newsSummaryToNewsQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsSummaryToNewsMockMvc;

    private NewsSummaryToNews newsSummaryToNews;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsSummaryToNewsResource newsSummaryToNewsResource = new NewsSummaryToNewsResource(newsSummaryToNewsService, newsSummaryToNewsQueryService);
        this.restNewsSummaryToNewsMockMvc = MockMvcBuilders.standaloneSetup(newsSummaryToNewsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsSummaryToNews createEntity(EntityManager em) {
        NewsSummaryToNews newsSummaryToNews = new NewsSummaryToNews();
        // Add required entity
        NewsSummary newsSummary = NewsSummaryResourceIntTest.createEntity(em);
        em.persist(newsSummary);
        em.flush();
        newsSummaryToNews.setNewsSummary(newsSummary);
        // Add required entity
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsSummaryToNews.setNews(news);
        return newsSummaryToNews;
    }

    @Before
    public void initTest() {
        newsSummaryToNewsSearchRepository.deleteAll();
        newsSummaryToNews = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsSummaryToNews() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryToNewsRepository.findAll().size();

        // Create the NewsSummaryToNews
        restNewsSummaryToNewsMockMvc.perform(post("/api/news-summary-to-news")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryToNews)))
            .andExpect(status().isCreated());

        // Validate the NewsSummaryToNews in the database
        List<NewsSummaryToNews> newsSummaryToNewsList = newsSummaryToNewsRepository.findAll();
        assertThat(newsSummaryToNewsList).hasSize(databaseSizeBeforeCreate + 1);
        NewsSummaryToNews testNewsSummaryToNews = newsSummaryToNewsList.get(newsSummaryToNewsList.size() - 1);

        // Validate the NewsSummaryToNews in Elasticsearch
        NewsSummaryToNews newsSummaryToNewsEs = newsSummaryToNewsSearchRepository.findOne(testNewsSummaryToNews.getId());
        assertThat(newsSummaryToNewsEs).isEqualToComparingFieldByField(testNewsSummaryToNews);
    }

    @Test
    @Transactional
    public void createNewsSummaryToNewsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryToNewsRepository.findAll().size();

        // Create the NewsSummaryToNews with an existing ID
        newsSummaryToNews.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsSummaryToNewsMockMvc.perform(post("/api/news-summary-to-news")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryToNews)))
            .andExpect(status().isBadRequest());

        // Validate the NewsSummaryToNews in the database
        List<NewsSummaryToNews> newsSummaryToNewsList = newsSummaryToNewsRepository.findAll();
        assertThat(newsSummaryToNewsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryToNews() throws Exception {
        // Initialize the database
        newsSummaryToNewsRepository.saveAndFlush(newsSummaryToNews);

        // Get all the newsSummaryToNewsList
        restNewsSummaryToNewsMockMvc.perform(get("/api/news-summary-to-news?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryToNews.getId().intValue())));
    }

    @Test
    @Transactional
    public void getNewsSummaryToNews() throws Exception {
        // Initialize the database
        newsSummaryToNewsRepository.saveAndFlush(newsSummaryToNews);

        // Get the newsSummaryToNews
        restNewsSummaryToNewsMockMvc.perform(get("/api/news-summary-to-news/{id}", newsSummaryToNews.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsSummaryToNews.getId().intValue()));
    }

    @Test
    @Transactional
    public void getAllNewsSummaryToNewsByNewsSummaryIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsSummary newsSummary = NewsSummaryResourceIntTest.createEntity(em);
        em.persist(newsSummary);
        em.flush();
        newsSummaryToNews.setNewsSummary(newsSummary);
        newsSummaryToNewsRepository.saveAndFlush(newsSummaryToNews);
        Long newsSummaryId = newsSummary.getId();

        // Get all the newsSummaryToNewsList where newsSummary equals to newsSummaryId
        defaultNewsSummaryToNewsShouldBeFound("newsSummaryId.equals=" + newsSummaryId);

        // Get all the newsSummaryToNewsList where newsSummary equals to newsSummaryId + 1
        defaultNewsSummaryToNewsShouldNotBeFound("newsSummaryId.equals=" + (newsSummaryId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsSummaryToNewsByNewsIsEqualToSomething() throws Exception {
        // Initialize the database
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsSummaryToNews.setNews(news);
        newsSummaryToNewsRepository.saveAndFlush(newsSummaryToNews);
        Long newsId = news.getId();

        // Get all the newsSummaryToNewsList where news equals to newsId
        defaultNewsSummaryToNewsShouldBeFound("newsId.equals=" + newsId);

        // Get all the newsSummaryToNewsList where news equals to newsId + 1
        defaultNewsSummaryToNewsShouldNotBeFound("newsId.equals=" + (newsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsSummaryToNewsShouldBeFound(String filter) throws Exception {
        restNewsSummaryToNewsMockMvc.perform(get("/api/news-summary-to-news?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryToNews.getId().intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsSummaryToNewsShouldNotBeFound(String filter) throws Exception {
        restNewsSummaryToNewsMockMvc.perform(get("/api/news-summary-to-news?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsSummaryToNews() throws Exception {
        // Get the newsSummaryToNews
        restNewsSummaryToNewsMockMvc.perform(get("/api/news-summary-to-news/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsSummaryToNews() throws Exception {
        // Initialize the database
        newsSummaryToNewsService.save(newsSummaryToNews);

        int databaseSizeBeforeUpdate = newsSummaryToNewsRepository.findAll().size();

        // Update the newsSummaryToNews
        NewsSummaryToNews updatedNewsSummaryToNews = newsSummaryToNewsRepository.findOne(newsSummaryToNews.getId());

        restNewsSummaryToNewsMockMvc.perform(put("/api/news-summary-to-news")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsSummaryToNews)))
            .andExpect(status().isOk());

        // Validate the NewsSummaryToNews in the database
        List<NewsSummaryToNews> newsSummaryToNewsList = newsSummaryToNewsRepository.findAll();
        assertThat(newsSummaryToNewsList).hasSize(databaseSizeBeforeUpdate);
        NewsSummaryToNews testNewsSummaryToNews = newsSummaryToNewsList.get(newsSummaryToNewsList.size() - 1);

        // Validate the NewsSummaryToNews in Elasticsearch
        NewsSummaryToNews newsSummaryToNewsEs = newsSummaryToNewsSearchRepository.findOne(testNewsSummaryToNews.getId());
        assertThat(newsSummaryToNewsEs).isEqualToComparingFieldByField(testNewsSummaryToNews);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsSummaryToNews() throws Exception {
        int databaseSizeBeforeUpdate = newsSummaryToNewsRepository.findAll().size();

        // Create the NewsSummaryToNews

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsSummaryToNewsMockMvc.perform(put("/api/news-summary-to-news")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryToNews)))
            .andExpect(status().isCreated());

        // Validate the NewsSummaryToNews in the database
        List<NewsSummaryToNews> newsSummaryToNewsList = newsSummaryToNewsRepository.findAll();
        assertThat(newsSummaryToNewsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsSummaryToNews() throws Exception {
        // Initialize the database
        newsSummaryToNewsService.save(newsSummaryToNews);

        int databaseSizeBeforeDelete = newsSummaryToNewsRepository.findAll().size();

        // Get the newsSummaryToNews
        restNewsSummaryToNewsMockMvc.perform(delete("/api/news-summary-to-news/{id}", newsSummaryToNews.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsSummaryToNewsExistsInEs = newsSummaryToNewsSearchRepository.exists(newsSummaryToNews.getId());
        assertThat(newsSummaryToNewsExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsSummaryToNews> newsSummaryToNewsList = newsSummaryToNewsRepository.findAll();
        assertThat(newsSummaryToNewsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsSummaryToNews() throws Exception {
        // Initialize the database
        newsSummaryToNewsService.save(newsSummaryToNews);

        // Search the newsSummaryToNews
        restNewsSummaryToNewsMockMvc.perform(get("/api/_search/news-summary-to-news?query=id:" + newsSummaryToNews.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryToNews.getId().intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsSummaryToNews.class);
        NewsSummaryToNews newsSummaryToNews1 = new NewsSummaryToNews();
        newsSummaryToNews1.setId(1L);
        NewsSummaryToNews newsSummaryToNews2 = new NewsSummaryToNews();
        newsSummaryToNews2.setId(newsSummaryToNews1.getId());
        assertThat(newsSummaryToNews1).isEqualTo(newsSummaryToNews2);
        newsSummaryToNews2.setId(2L);
        assertThat(newsSummaryToNews1).isNotEqualTo(newsSummaryToNews2);
        newsSummaryToNews1.setId(null);
        assertThat(newsSummaryToNews1).isNotEqualTo(newsSummaryToNews2);
    }
}
