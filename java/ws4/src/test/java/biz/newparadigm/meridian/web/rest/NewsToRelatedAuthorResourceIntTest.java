package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsToRelatedAuthor;
import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.domain.Author;
import biz.newparadigm.meridian.repository.NewsToRelatedAuthorRepository;
import biz.newparadigm.meridian.service.NewsToRelatedAuthorService;
import biz.newparadigm.meridian.repository.search.NewsToRelatedAuthorSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsToRelatedAuthorResource REST controller.
 *
 * @see NewsToRelatedAuthorResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsToRelatedAuthorResourceIntTest {

    @Autowired
    private NewsToRelatedAuthorRepository newsToRelatedAuthorRepository;

    @Autowired
    private NewsToRelatedAuthorService newsToRelatedAuthorService;

    @Autowired
    private NewsToRelatedAuthorSearchRepository newsToRelatedAuthorSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsToRelatedAuthorMockMvc;

    private NewsToRelatedAuthor newsToRelatedAuthor;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsToRelatedAuthorResource newsToRelatedAuthorResource = new NewsToRelatedAuthorResource(newsToRelatedAuthorService);
        this.restNewsToRelatedAuthorMockMvc = MockMvcBuilders.standaloneSetup(newsToRelatedAuthorResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsToRelatedAuthor createEntity(EntityManager em) {
        NewsToRelatedAuthor newsToRelatedAuthor = new NewsToRelatedAuthor();
        // Add required entity
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsToRelatedAuthor.setNews(news);
        // Add required entity
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        newsToRelatedAuthor.setAuthor(author);
        return newsToRelatedAuthor;
    }

    @Before
    public void initTest() {
        newsToRelatedAuthorSearchRepository.deleteAll();
        newsToRelatedAuthor = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsToRelatedAuthor() throws Exception {
        int databaseSizeBeforeCreate = newsToRelatedAuthorRepository.findAll().size();

        // Create the NewsToRelatedAuthor
        restNewsToRelatedAuthorMockMvc.perform(post("/api/news-to-related-authors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsToRelatedAuthor)))
            .andExpect(status().isCreated());

        // Validate the NewsToRelatedAuthor in the database
        List<NewsToRelatedAuthor> newsToRelatedAuthorList = newsToRelatedAuthorRepository.findAll();
        assertThat(newsToRelatedAuthorList).hasSize(databaseSizeBeforeCreate + 1);
        NewsToRelatedAuthor testNewsToRelatedAuthor = newsToRelatedAuthorList.get(newsToRelatedAuthorList.size() - 1);

        // Validate the NewsToRelatedAuthor in Elasticsearch
        NewsToRelatedAuthor newsToRelatedAuthorEs = newsToRelatedAuthorSearchRepository.findOne(testNewsToRelatedAuthor.getId());
        assertThat(newsToRelatedAuthorEs).isEqualToComparingFieldByField(testNewsToRelatedAuthor);
    }

    @Test
    @Transactional
    public void createNewsToRelatedAuthorWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsToRelatedAuthorRepository.findAll().size();

        // Create the NewsToRelatedAuthor with an existing ID
        newsToRelatedAuthor.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsToRelatedAuthorMockMvc.perform(post("/api/news-to-related-authors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsToRelatedAuthor)))
            .andExpect(status().isBadRequest());

        // Validate the NewsToRelatedAuthor in the database
        List<NewsToRelatedAuthor> newsToRelatedAuthorList = newsToRelatedAuthorRepository.findAll();
        assertThat(newsToRelatedAuthorList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllNewsToRelatedAuthors() throws Exception {
        // Initialize the database
        newsToRelatedAuthorRepository.saveAndFlush(newsToRelatedAuthor);

        // Get all the newsToRelatedAuthorList
        restNewsToRelatedAuthorMockMvc.perform(get("/api/news-to-related-authors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsToRelatedAuthor.getId().intValue())));
    }

    @Test
    @Transactional
    public void getNewsToRelatedAuthor() throws Exception {
        // Initialize the database
        newsToRelatedAuthorRepository.saveAndFlush(newsToRelatedAuthor);

        // Get the newsToRelatedAuthor
        restNewsToRelatedAuthorMockMvc.perform(get("/api/news-to-related-authors/{id}", newsToRelatedAuthor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsToRelatedAuthor.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingNewsToRelatedAuthor() throws Exception {
        // Get the newsToRelatedAuthor
        restNewsToRelatedAuthorMockMvc.perform(get("/api/news-to-related-authors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsToRelatedAuthor() throws Exception {
        // Initialize the database
        newsToRelatedAuthorService.save(newsToRelatedAuthor);

        int databaseSizeBeforeUpdate = newsToRelatedAuthorRepository.findAll().size();

        // Update the newsToRelatedAuthor
        NewsToRelatedAuthor updatedNewsToRelatedAuthor = newsToRelatedAuthorRepository.findOne(newsToRelatedAuthor.getId());

        restNewsToRelatedAuthorMockMvc.perform(put("/api/news-to-related-authors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsToRelatedAuthor)))
            .andExpect(status().isOk());

        // Validate the NewsToRelatedAuthor in the database
        List<NewsToRelatedAuthor> newsToRelatedAuthorList = newsToRelatedAuthorRepository.findAll();
        assertThat(newsToRelatedAuthorList).hasSize(databaseSizeBeforeUpdate);
        NewsToRelatedAuthor testNewsToRelatedAuthor = newsToRelatedAuthorList.get(newsToRelatedAuthorList.size() - 1);

        // Validate the NewsToRelatedAuthor in Elasticsearch
        NewsToRelatedAuthor newsToRelatedAuthorEs = newsToRelatedAuthorSearchRepository.findOne(testNewsToRelatedAuthor.getId());
        assertThat(newsToRelatedAuthorEs).isEqualToComparingFieldByField(testNewsToRelatedAuthor);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsToRelatedAuthor() throws Exception {
        int databaseSizeBeforeUpdate = newsToRelatedAuthorRepository.findAll().size();

        // Create the NewsToRelatedAuthor

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsToRelatedAuthorMockMvc.perform(put("/api/news-to-related-authors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsToRelatedAuthor)))
            .andExpect(status().isCreated());

        // Validate the NewsToRelatedAuthor in the database
        List<NewsToRelatedAuthor> newsToRelatedAuthorList = newsToRelatedAuthorRepository.findAll();
        assertThat(newsToRelatedAuthorList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsToRelatedAuthor() throws Exception {
        // Initialize the database
        newsToRelatedAuthorService.save(newsToRelatedAuthor);

        int databaseSizeBeforeDelete = newsToRelatedAuthorRepository.findAll().size();

        // Get the newsToRelatedAuthor
        restNewsToRelatedAuthorMockMvc.perform(delete("/api/news-to-related-authors/{id}", newsToRelatedAuthor.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsToRelatedAuthorExistsInEs = newsToRelatedAuthorSearchRepository.exists(newsToRelatedAuthor.getId());
        assertThat(newsToRelatedAuthorExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsToRelatedAuthor> newsToRelatedAuthorList = newsToRelatedAuthorRepository.findAll();
        assertThat(newsToRelatedAuthorList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsToRelatedAuthor() throws Exception {
        // Initialize the database
        newsToRelatedAuthorService.save(newsToRelatedAuthor);

        // Search the newsToRelatedAuthor
        restNewsToRelatedAuthorMockMvc.perform(get("/api/_search/news-to-related-authors?query=id:" + newsToRelatedAuthor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsToRelatedAuthor.getId().intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsToRelatedAuthor.class);
        NewsToRelatedAuthor newsToRelatedAuthor1 = new NewsToRelatedAuthor();
        newsToRelatedAuthor1.setId(1L);
        NewsToRelatedAuthor newsToRelatedAuthor2 = new NewsToRelatedAuthor();
        newsToRelatedAuthor2.setId(newsToRelatedAuthor1.getId());
        assertThat(newsToRelatedAuthor1).isEqualTo(newsToRelatedAuthor2);
        newsToRelatedAuthor2.setId(2L);
        assertThat(newsToRelatedAuthor1).isNotEqualTo(newsToRelatedAuthor2);
        newsToRelatedAuthor1.setId(null);
        assertThat(newsToRelatedAuthor1).isNotEqualTo(newsToRelatedAuthor2);
    }
}
