package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsPublicImage;
import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.repository.NewsPublicImageRepository;
import biz.newparadigm.meridian.service.NewsPublicImageService;
import biz.newparadigm.meridian.repository.search.NewsPublicImageSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsPublicImageCriteria;
import biz.newparadigm.meridian.service.NewsPublicImageQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsPublicImageResource REST controller.
 *
 * @see NewsPublicImageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsPublicImageResourceIntTest {

    private static final String DEFAULT_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_FILENAME = "BBBBBBBBBB";

    private static final byte[] DEFAULT_BLOB = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_BLOB = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_BLOB_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_BLOB_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_TRANSLATION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_TRANSLATION = "BBBBBBBBBB";

    private static final Integer DEFAULT_IMAGE_ORDER = 1;
    private static final Integer UPDATED_IMAGE_ORDER = 2;

    @Autowired
    private NewsPublicImageRepository newsPublicImageRepository;

    @Autowired
    private NewsPublicImageService newsPublicImageService;

    @Autowired
    private NewsPublicImageSearchRepository newsPublicImageSearchRepository;

    @Autowired
    private NewsPublicImageQueryService newsPublicImageQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsPublicImageMockMvc;

    private NewsPublicImage newsPublicImage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsPublicImageResource newsPublicImageResource = new NewsPublicImageResource(newsPublicImageService, newsPublicImageQueryService);
        this.restNewsPublicImageMockMvc = MockMvcBuilders.standaloneSetup(newsPublicImageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsPublicImage createEntity(EntityManager em) {
        NewsPublicImage newsPublicImage = new NewsPublicImage()
            .filename(DEFAULT_FILENAME)
            .blob(DEFAULT_BLOB)
            .blobContentType(DEFAULT_BLOB_CONTENT_TYPE)
            .description(DEFAULT_DESCRIPTION)
            .descriptionTranslation(DEFAULT_DESCRIPTION_TRANSLATION)
            .imageOrder(DEFAULT_IMAGE_ORDER);
        // Add required entity
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsPublicImage.setNews(news);
        return newsPublicImage;
    }

    @Before
    public void initTest() {
        newsPublicImageSearchRepository.deleteAll();
        newsPublicImage = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsPublicImage() throws Exception {
        int databaseSizeBeforeCreate = newsPublicImageRepository.findAll().size();

        // Create the NewsPublicImage
        restNewsPublicImageMockMvc.perform(post("/api/news-public-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsPublicImage)))
            .andExpect(status().isCreated());

        // Validate the NewsPublicImage in the database
        List<NewsPublicImage> newsPublicImageList = newsPublicImageRepository.findAll();
        assertThat(newsPublicImageList).hasSize(databaseSizeBeforeCreate + 1);
        NewsPublicImage testNewsPublicImage = newsPublicImageList.get(newsPublicImageList.size() - 1);
        assertThat(testNewsPublicImage.getFilename()).isEqualTo(DEFAULT_FILENAME);
        assertThat(testNewsPublicImage.getBlob()).isEqualTo(DEFAULT_BLOB);
        assertThat(testNewsPublicImage.getBlobContentType()).isEqualTo(DEFAULT_BLOB_CONTENT_TYPE);
        assertThat(testNewsPublicImage.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testNewsPublicImage.getDescriptionTranslation()).isEqualTo(DEFAULT_DESCRIPTION_TRANSLATION);
        assertThat(testNewsPublicImage.getImageOrder()).isEqualTo(DEFAULT_IMAGE_ORDER);

        // Validate the NewsPublicImage in Elasticsearch
        NewsPublicImage newsPublicImageEs = newsPublicImageSearchRepository.findOne(testNewsPublicImage.getId());
        assertThat(newsPublicImageEs).isEqualToComparingFieldByField(testNewsPublicImage);
    }

    @Test
    @Transactional
    public void createNewsPublicImageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsPublicImageRepository.findAll().size();

        // Create the NewsPublicImage with an existing ID
        newsPublicImage.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsPublicImageMockMvc.perform(post("/api/news-public-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsPublicImage)))
            .andExpect(status().isBadRequest());

        // Validate the NewsPublicImage in the database
        List<NewsPublicImage> newsPublicImageList = newsPublicImageRepository.findAll();
        assertThat(newsPublicImageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFilenameIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsPublicImageRepository.findAll().size();
        // set the field null
        newsPublicImage.setFilename(null);

        // Create the NewsPublicImage, which fails.

        restNewsPublicImageMockMvc.perform(post("/api/news-public-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsPublicImage)))
            .andExpect(status().isBadRequest());

        List<NewsPublicImage> newsPublicImageList = newsPublicImageRepository.findAll();
        assertThat(newsPublicImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBlobIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsPublicImageRepository.findAll().size();
        // set the field null
        newsPublicImage.setBlob(null);

        // Create the NewsPublicImage, which fails.

        restNewsPublicImageMockMvc.perform(post("/api/news-public-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsPublicImage)))
            .andExpect(status().isBadRequest());

        List<NewsPublicImage> newsPublicImageList = newsPublicImageRepository.findAll();
        assertThat(newsPublicImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNewsPublicImages() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get all the newsPublicImageList
        restNewsPublicImageMockMvc.perform(get("/api/news-public-images?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsPublicImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionTranslation").value(hasItem(DEFAULT_DESCRIPTION_TRANSLATION.toString())))
            .andExpect(jsonPath("$.[*].imageOrder").value(hasItem(DEFAULT_IMAGE_ORDER)));
    }

    @Test
    @Transactional
    public void getNewsPublicImage() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get the newsPublicImage
        restNewsPublicImageMockMvc.perform(get("/api/news-public-images/{id}", newsPublicImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsPublicImage.getId().intValue()))
            .andExpect(jsonPath("$.filename").value(DEFAULT_FILENAME.toString()))
            .andExpect(jsonPath("$.blobContentType").value(DEFAULT_BLOB_CONTENT_TYPE))
            .andExpect(jsonPath("$.blob").value(Base64Utils.encodeToString(DEFAULT_BLOB)))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.descriptionTranslation").value(DEFAULT_DESCRIPTION_TRANSLATION.toString()))
            .andExpect(jsonPath("$.imageOrder").value(DEFAULT_IMAGE_ORDER));
    }

    @Test
    @Transactional
    public void getAllNewsPublicImagesByFilenameIsEqualToSomething() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get all the newsPublicImageList where filename equals to DEFAULT_FILENAME
        defaultNewsPublicImageShouldBeFound("filename.equals=" + DEFAULT_FILENAME);

        // Get all the newsPublicImageList where filename equals to UPDATED_FILENAME
        defaultNewsPublicImageShouldNotBeFound("filename.equals=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllNewsPublicImagesByFilenameIsInShouldWork() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get all the newsPublicImageList where filename in DEFAULT_FILENAME or UPDATED_FILENAME
        defaultNewsPublicImageShouldBeFound("filename.in=" + DEFAULT_FILENAME + "," + UPDATED_FILENAME);

        // Get all the newsPublicImageList where filename equals to UPDATED_FILENAME
        defaultNewsPublicImageShouldNotBeFound("filename.in=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllNewsPublicImagesByFilenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get all the newsPublicImageList where filename is not null
        defaultNewsPublicImageShouldBeFound("filename.specified=true");

        // Get all the newsPublicImageList where filename is null
        defaultNewsPublicImageShouldNotBeFound("filename.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsPublicImagesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get all the newsPublicImageList where description equals to DEFAULT_DESCRIPTION
        defaultNewsPublicImageShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the newsPublicImageList where description equals to UPDATED_DESCRIPTION
        defaultNewsPublicImageShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsPublicImagesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get all the newsPublicImageList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultNewsPublicImageShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the newsPublicImageList where description equals to UPDATED_DESCRIPTION
        defaultNewsPublicImageShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsPublicImagesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get all the newsPublicImageList where description is not null
        defaultNewsPublicImageShouldBeFound("description.specified=true");

        // Get all the newsPublicImageList where description is null
        defaultNewsPublicImageShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsPublicImagesByDescriptionTranslationIsEqualToSomething() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get all the newsPublicImageList where descriptionTranslation equals to DEFAULT_DESCRIPTION_TRANSLATION
        defaultNewsPublicImageShouldBeFound("descriptionTranslation.equals=" + DEFAULT_DESCRIPTION_TRANSLATION);

        // Get all the newsPublicImageList where descriptionTranslation equals to UPDATED_DESCRIPTION_TRANSLATION
        defaultNewsPublicImageShouldNotBeFound("descriptionTranslation.equals=" + UPDATED_DESCRIPTION_TRANSLATION);
    }

    @Test
    @Transactional
    public void getAllNewsPublicImagesByDescriptionTranslationIsInShouldWork() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get all the newsPublicImageList where descriptionTranslation in DEFAULT_DESCRIPTION_TRANSLATION or UPDATED_DESCRIPTION_TRANSLATION
        defaultNewsPublicImageShouldBeFound("descriptionTranslation.in=" + DEFAULT_DESCRIPTION_TRANSLATION + "," + UPDATED_DESCRIPTION_TRANSLATION);

        // Get all the newsPublicImageList where descriptionTranslation equals to UPDATED_DESCRIPTION_TRANSLATION
        defaultNewsPublicImageShouldNotBeFound("descriptionTranslation.in=" + UPDATED_DESCRIPTION_TRANSLATION);
    }

    @Test
    @Transactional
    public void getAllNewsPublicImagesByDescriptionTranslationIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get all the newsPublicImageList where descriptionTranslation is not null
        defaultNewsPublicImageShouldBeFound("descriptionTranslation.specified=true");

        // Get all the newsPublicImageList where descriptionTranslation is null
        defaultNewsPublicImageShouldNotBeFound("descriptionTranslation.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsPublicImagesByImageOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get all the newsPublicImageList where imageOrder equals to DEFAULT_IMAGE_ORDER
        defaultNewsPublicImageShouldBeFound("imageOrder.equals=" + DEFAULT_IMAGE_ORDER);

        // Get all the newsPublicImageList where imageOrder equals to UPDATED_IMAGE_ORDER
        defaultNewsPublicImageShouldNotBeFound("imageOrder.equals=" + UPDATED_IMAGE_ORDER);
    }

    @Test
    @Transactional
    public void getAllNewsPublicImagesByImageOrderIsInShouldWork() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get all the newsPublicImageList where imageOrder in DEFAULT_IMAGE_ORDER or UPDATED_IMAGE_ORDER
        defaultNewsPublicImageShouldBeFound("imageOrder.in=" + DEFAULT_IMAGE_ORDER + "," + UPDATED_IMAGE_ORDER);

        // Get all the newsPublicImageList where imageOrder equals to UPDATED_IMAGE_ORDER
        defaultNewsPublicImageShouldNotBeFound("imageOrder.in=" + UPDATED_IMAGE_ORDER);
    }

    @Test
    @Transactional
    public void getAllNewsPublicImagesByImageOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get all the newsPublicImageList where imageOrder is not null
        defaultNewsPublicImageShouldBeFound("imageOrder.specified=true");

        // Get all the newsPublicImageList where imageOrder is null
        defaultNewsPublicImageShouldNotBeFound("imageOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsPublicImagesByImageOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get all the newsPublicImageList where imageOrder greater than or equals to DEFAULT_IMAGE_ORDER
        defaultNewsPublicImageShouldBeFound("imageOrder.greaterOrEqualThan=" + DEFAULT_IMAGE_ORDER);

        // Get all the newsPublicImageList where imageOrder greater than or equals to UPDATED_IMAGE_ORDER
        defaultNewsPublicImageShouldNotBeFound("imageOrder.greaterOrEqualThan=" + UPDATED_IMAGE_ORDER);
    }

    @Test
    @Transactional
    public void getAllNewsPublicImagesByImageOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        newsPublicImageRepository.saveAndFlush(newsPublicImage);

        // Get all the newsPublicImageList where imageOrder less than or equals to DEFAULT_IMAGE_ORDER
        defaultNewsPublicImageShouldNotBeFound("imageOrder.lessThan=" + DEFAULT_IMAGE_ORDER);

        // Get all the newsPublicImageList where imageOrder less than or equals to UPDATED_IMAGE_ORDER
        defaultNewsPublicImageShouldBeFound("imageOrder.lessThan=" + UPDATED_IMAGE_ORDER);
    }


    @Test
    @Transactional
    public void getAllNewsPublicImagesByNewsIsEqualToSomething() throws Exception {
        // Initialize the database
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsPublicImage.setNews(news);
        newsPublicImageRepository.saveAndFlush(newsPublicImage);
        Long newsId = news.getId();

        // Get all the newsPublicImageList where news equals to newsId
        defaultNewsPublicImageShouldBeFound("newsId.equals=" + newsId);

        // Get all the newsPublicImageList where news equals to newsId + 1
        defaultNewsPublicImageShouldNotBeFound("newsId.equals=" + (newsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsPublicImageShouldBeFound(String filter) throws Exception {
        restNewsPublicImageMockMvc.perform(get("/api/news-public-images?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsPublicImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionTranslation").value(hasItem(DEFAULT_DESCRIPTION_TRANSLATION.toString())))
            .andExpect(jsonPath("$.[*].imageOrder").value(hasItem(DEFAULT_IMAGE_ORDER)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsPublicImageShouldNotBeFound(String filter) throws Exception {
        restNewsPublicImageMockMvc.perform(get("/api/news-public-images?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsPublicImage() throws Exception {
        // Get the newsPublicImage
        restNewsPublicImageMockMvc.perform(get("/api/news-public-images/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsPublicImage() throws Exception {
        // Initialize the database
        newsPublicImageService.save(newsPublicImage);

        int databaseSizeBeforeUpdate = newsPublicImageRepository.findAll().size();

        // Update the newsPublicImage
        NewsPublicImage updatedNewsPublicImage = newsPublicImageRepository.findOne(newsPublicImage.getId());
        updatedNewsPublicImage
            .filename(UPDATED_FILENAME)
            .blob(UPDATED_BLOB)
            .blobContentType(UPDATED_BLOB_CONTENT_TYPE)
            .description(UPDATED_DESCRIPTION)
            .descriptionTranslation(UPDATED_DESCRIPTION_TRANSLATION)
            .imageOrder(UPDATED_IMAGE_ORDER);

        restNewsPublicImageMockMvc.perform(put("/api/news-public-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsPublicImage)))
            .andExpect(status().isOk());

        // Validate the NewsPublicImage in the database
        List<NewsPublicImage> newsPublicImageList = newsPublicImageRepository.findAll();
        assertThat(newsPublicImageList).hasSize(databaseSizeBeforeUpdate);
        NewsPublicImage testNewsPublicImage = newsPublicImageList.get(newsPublicImageList.size() - 1);
        assertThat(testNewsPublicImage.getFilename()).isEqualTo(UPDATED_FILENAME);
        assertThat(testNewsPublicImage.getBlob()).isEqualTo(UPDATED_BLOB);
        assertThat(testNewsPublicImage.getBlobContentType()).isEqualTo(UPDATED_BLOB_CONTENT_TYPE);
        assertThat(testNewsPublicImage.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testNewsPublicImage.getDescriptionTranslation()).isEqualTo(UPDATED_DESCRIPTION_TRANSLATION);
        assertThat(testNewsPublicImage.getImageOrder()).isEqualTo(UPDATED_IMAGE_ORDER);

        // Validate the NewsPublicImage in Elasticsearch
        NewsPublicImage newsPublicImageEs = newsPublicImageSearchRepository.findOne(testNewsPublicImage.getId());
        assertThat(newsPublicImageEs).isEqualToComparingFieldByField(testNewsPublicImage);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsPublicImage() throws Exception {
        int databaseSizeBeforeUpdate = newsPublicImageRepository.findAll().size();

        // Create the NewsPublicImage

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsPublicImageMockMvc.perform(put("/api/news-public-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsPublicImage)))
            .andExpect(status().isCreated());

        // Validate the NewsPublicImage in the database
        List<NewsPublicImage> newsPublicImageList = newsPublicImageRepository.findAll();
        assertThat(newsPublicImageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsPublicImage() throws Exception {
        // Initialize the database
        newsPublicImageService.save(newsPublicImage);

        int databaseSizeBeforeDelete = newsPublicImageRepository.findAll().size();

        // Get the newsPublicImage
        restNewsPublicImageMockMvc.perform(delete("/api/news-public-images/{id}", newsPublicImage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsPublicImageExistsInEs = newsPublicImageSearchRepository.exists(newsPublicImage.getId());
        assertThat(newsPublicImageExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsPublicImage> newsPublicImageList = newsPublicImageRepository.findAll();
        assertThat(newsPublicImageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsPublicImage() throws Exception {
        // Initialize the database
        newsPublicImageService.save(newsPublicImage);

        // Search the newsPublicImage
        restNewsPublicImageMockMvc.perform(get("/api/_search/news-public-images?query=id:" + newsPublicImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsPublicImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionTranslation").value(hasItem(DEFAULT_DESCRIPTION_TRANSLATION.toString())))
            .andExpect(jsonPath("$.[*].imageOrder").value(hasItem(DEFAULT_IMAGE_ORDER)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsPublicImage.class);
        NewsPublicImage newsPublicImage1 = new NewsPublicImage();
        newsPublicImage1.setId(1L);
        NewsPublicImage newsPublicImage2 = new NewsPublicImage();
        newsPublicImage2.setId(newsPublicImage1.getId());
        assertThat(newsPublicImage1).isEqualTo(newsPublicImage2);
        newsPublicImage2.setId(2L);
        assertThat(newsPublicImage1).isNotEqualTo(newsPublicImage2);
        newsPublicImage1.setId(null);
        assertThat(newsPublicImage1).isNotEqualTo(newsPublicImage2);
    }
}
