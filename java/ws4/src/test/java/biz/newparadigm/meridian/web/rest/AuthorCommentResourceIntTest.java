package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.AuthorComment;
import biz.newparadigm.meridian.domain.Author;
import biz.newparadigm.meridian.domain.AuthorComment;
import biz.newparadigm.meridian.repository.AuthorCommentRepository;
import biz.newparadigm.meridian.service.AuthorCommentService;
import biz.newparadigm.meridian.repository.search.AuthorCommentSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.AuthorCommentCriteria;
import biz.newparadigm.meridian.service.AuthorCommentQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.sameInstant;
import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AuthorCommentResource REST controller.
 *
 * @see AuthorCommentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class AuthorCommentResourceIntTest {

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATETIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATETIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private AuthorCommentRepository authorCommentRepository;

    @Autowired
    private AuthorCommentService authorCommentService;

    @Autowired
    private AuthorCommentSearchRepository authorCommentSearchRepository;

    @Autowired
    private AuthorCommentQueryService authorCommentQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAuthorCommentMockMvc;

    private AuthorComment authorComment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AuthorCommentResource authorCommentResource = new AuthorCommentResource(authorCommentService, authorCommentQueryService);
        this.restAuthorCommentMockMvc = MockMvcBuilders.standaloneSetup(authorCommentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AuthorComment createEntity(EntityManager em) {
        AuthorComment authorComment = new AuthorComment()
            .userId(DEFAULT_USER_ID)
            .comment(DEFAULT_COMMENT)
            .datetime(DEFAULT_DATETIME);
        // Add required entity
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        authorComment.setAuthor(author);
        return authorComment;
    }

    @Before
    public void initTest() {
        authorCommentSearchRepository.deleteAll();
        authorComment = createEntity(em);
    }

    @Test
    @Transactional
    public void createAuthorComment() throws Exception {
        int databaseSizeBeforeCreate = authorCommentRepository.findAll().size();

        // Create the AuthorComment
        restAuthorCommentMockMvc.perform(post("/api/author-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorComment)))
            .andExpect(status().isCreated());

        // Validate the AuthorComment in the database
        List<AuthorComment> authorCommentList = authorCommentRepository.findAll();
        assertThat(authorCommentList).hasSize(databaseSizeBeforeCreate + 1);
        AuthorComment testAuthorComment = authorCommentList.get(authorCommentList.size() - 1);
        assertThat(testAuthorComment.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testAuthorComment.getComment()).isEqualTo(DEFAULT_COMMENT);
        assertThat(testAuthorComment.getDatetime()).isEqualTo(DEFAULT_DATETIME);

        // Validate the AuthorComment in Elasticsearch
        AuthorComment authorCommentEs = authorCommentSearchRepository.findOne(testAuthorComment.getId());
        assertThat(authorCommentEs).isEqualToComparingFieldByField(testAuthorComment);
    }

    @Test
    @Transactional
    public void createAuthorCommentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = authorCommentRepository.findAll().size();

        // Create the AuthorComment with an existing ID
        authorComment.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAuthorCommentMockMvc.perform(post("/api/author-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorComment)))
            .andExpect(status().isBadRequest());

        // Validate the AuthorComment in the database
        List<AuthorComment> authorCommentList = authorCommentRepository.findAll();
        assertThat(authorCommentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorCommentRepository.findAll().size();
        // set the field null
        authorComment.setUserId(null);

        // Create the AuthorComment, which fails.

        restAuthorCommentMockMvc.perform(post("/api/author-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorComment)))
            .andExpect(status().isBadRequest());

        List<AuthorComment> authorCommentList = authorCommentRepository.findAll();
        assertThat(authorCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCommentIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorCommentRepository.findAll().size();
        // set the field null
        authorComment.setComment(null);

        // Create the AuthorComment, which fails.

        restAuthorCommentMockMvc.perform(post("/api/author-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorComment)))
            .andExpect(status().isBadRequest());

        List<AuthorComment> authorCommentList = authorCommentRepository.findAll();
        assertThat(authorCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatetimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorCommentRepository.findAll().size();
        // set the field null
        authorComment.setDatetime(null);

        // Create the AuthorComment, which fails.

        restAuthorCommentMockMvc.perform(post("/api/author-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorComment)))
            .andExpect(status().isBadRequest());

        List<AuthorComment> authorCommentList = authorCommentRepository.findAll();
        assertThat(authorCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAuthorComments() throws Exception {
        // Initialize the database
        authorCommentRepository.saveAndFlush(authorComment);

        // Get all the authorCommentList
        restAuthorCommentMockMvc.perform(get("/api/author-comments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
            .andExpect(jsonPath("$.[*].datetime").value(hasItem(sameInstant(DEFAULT_DATETIME))));
    }

    @Test
    @Transactional
    public void getAuthorComment() throws Exception {
        // Initialize the database
        authorCommentRepository.saveAndFlush(authorComment);

        // Get the authorComment
        restAuthorCommentMockMvc.perform(get("/api/author-comments/{id}", authorComment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(authorComment.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.toString()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()))
            .andExpect(jsonPath("$.datetime").value(sameInstant(DEFAULT_DATETIME)));
    }

    @Test
    @Transactional
    public void getAllAuthorCommentsByUserIdIsEqualToSomething() throws Exception {
        // Initialize the database
        authorCommentRepository.saveAndFlush(authorComment);

        // Get all the authorCommentList where userId equals to DEFAULT_USER_ID
        defaultAuthorCommentShouldBeFound("userId.equals=" + DEFAULT_USER_ID);

        // Get all the authorCommentList where userId equals to UPDATED_USER_ID
        defaultAuthorCommentShouldNotBeFound("userId.equals=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllAuthorCommentsByUserIdIsInShouldWork() throws Exception {
        // Initialize the database
        authorCommentRepository.saveAndFlush(authorComment);

        // Get all the authorCommentList where userId in DEFAULT_USER_ID or UPDATED_USER_ID
        defaultAuthorCommentShouldBeFound("userId.in=" + DEFAULT_USER_ID + "," + UPDATED_USER_ID);

        // Get all the authorCommentList where userId equals to UPDATED_USER_ID
        defaultAuthorCommentShouldNotBeFound("userId.in=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllAuthorCommentsByUserIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorCommentRepository.saveAndFlush(authorComment);

        // Get all the authorCommentList where userId is not null
        defaultAuthorCommentShouldBeFound("userId.specified=true");

        // Get all the authorCommentList where userId is null
        defaultAuthorCommentShouldNotBeFound("userId.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorCommentsByCommentIsEqualToSomething() throws Exception {
        // Initialize the database
        authorCommentRepository.saveAndFlush(authorComment);

        // Get all the authorCommentList where comment equals to DEFAULT_COMMENT
        defaultAuthorCommentShouldBeFound("comment.equals=" + DEFAULT_COMMENT);

        // Get all the authorCommentList where comment equals to UPDATED_COMMENT
        defaultAuthorCommentShouldNotBeFound("comment.equals=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    public void getAllAuthorCommentsByCommentIsInShouldWork() throws Exception {
        // Initialize the database
        authorCommentRepository.saveAndFlush(authorComment);

        // Get all the authorCommentList where comment in DEFAULT_COMMENT or UPDATED_COMMENT
        defaultAuthorCommentShouldBeFound("comment.in=" + DEFAULT_COMMENT + "," + UPDATED_COMMENT);

        // Get all the authorCommentList where comment equals to UPDATED_COMMENT
        defaultAuthorCommentShouldNotBeFound("comment.in=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    public void getAllAuthorCommentsByCommentIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorCommentRepository.saveAndFlush(authorComment);

        // Get all the authorCommentList where comment is not null
        defaultAuthorCommentShouldBeFound("comment.specified=true");

        // Get all the authorCommentList where comment is null
        defaultAuthorCommentShouldNotBeFound("comment.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorCommentsByDatetimeIsEqualToSomething() throws Exception {
        // Initialize the database
        authorCommentRepository.saveAndFlush(authorComment);

        // Get all the authorCommentList where datetime equals to DEFAULT_DATETIME
        defaultAuthorCommentShouldBeFound("datetime.equals=" + DEFAULT_DATETIME);

        // Get all the authorCommentList where datetime equals to UPDATED_DATETIME
        defaultAuthorCommentShouldNotBeFound("datetime.equals=" + UPDATED_DATETIME);
    }

    @Test
    @Transactional
    public void getAllAuthorCommentsByDatetimeIsInShouldWork() throws Exception {
        // Initialize the database
        authorCommentRepository.saveAndFlush(authorComment);

        // Get all the authorCommentList where datetime in DEFAULT_DATETIME or UPDATED_DATETIME
        defaultAuthorCommentShouldBeFound("datetime.in=" + DEFAULT_DATETIME + "," + UPDATED_DATETIME);

        // Get all the authorCommentList where datetime equals to UPDATED_DATETIME
        defaultAuthorCommentShouldNotBeFound("datetime.in=" + UPDATED_DATETIME);
    }

    @Test
    @Transactional
    public void getAllAuthorCommentsByDatetimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorCommentRepository.saveAndFlush(authorComment);

        // Get all the authorCommentList where datetime is not null
        defaultAuthorCommentShouldBeFound("datetime.specified=true");

        // Get all the authorCommentList where datetime is null
        defaultAuthorCommentShouldNotBeFound("datetime.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorCommentsByDatetimeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        authorCommentRepository.saveAndFlush(authorComment);

        // Get all the authorCommentList where datetime greater than or equals to DEFAULT_DATETIME
        defaultAuthorCommentShouldBeFound("datetime.greaterOrEqualThan=" + DEFAULT_DATETIME);

        // Get all the authorCommentList where datetime greater than or equals to UPDATED_DATETIME
        defaultAuthorCommentShouldNotBeFound("datetime.greaterOrEqualThan=" + UPDATED_DATETIME);
    }

    @Test
    @Transactional
    public void getAllAuthorCommentsByDatetimeIsLessThanSomething() throws Exception {
        // Initialize the database
        authorCommentRepository.saveAndFlush(authorComment);

        // Get all the authorCommentList where datetime less than or equals to DEFAULT_DATETIME
        defaultAuthorCommentShouldNotBeFound("datetime.lessThan=" + DEFAULT_DATETIME);

        // Get all the authorCommentList where datetime less than or equals to UPDATED_DATETIME
        defaultAuthorCommentShouldBeFound("datetime.lessThan=" + UPDATED_DATETIME);
    }


    @Test
    @Transactional
    public void getAllAuthorCommentsByAuthorIsEqualToSomething() throws Exception {
        // Initialize the database
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        authorComment.setAuthor(author);
        authorCommentRepository.saveAndFlush(authorComment);
        Long authorId = author.getId();

        // Get all the authorCommentList where author equals to authorId
        defaultAuthorCommentShouldBeFound("authorId.equals=" + authorId);

        // Get all the authorCommentList where author equals to authorId + 1
        defaultAuthorCommentShouldNotBeFound("authorId.equals=" + (authorId + 1));
    }


    @Test
    @Transactional
    public void getAllAuthorCommentsByParentCommentIsEqualToSomething() throws Exception {
        // Initialize the database
        AuthorComment parentComment = AuthorCommentResourceIntTest.createEntity(em);
        em.persist(parentComment);
        em.flush();
        authorComment.setParentComment(parentComment);
        authorCommentRepository.saveAndFlush(authorComment);
        Long parentCommentId = parentComment.getId();

        // Get all the authorCommentList where parentComment equals to parentCommentId
        defaultAuthorCommentShouldBeFound("parentCommentId.equals=" + parentCommentId);

        // Get all the authorCommentList where parentComment equals to parentCommentId + 1
        defaultAuthorCommentShouldNotBeFound("parentCommentId.equals=" + (parentCommentId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultAuthorCommentShouldBeFound(String filter) throws Exception {
        restAuthorCommentMockMvc.perform(get("/api/author-comments?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
            .andExpect(jsonPath("$.[*].datetime").value(hasItem(sameInstant(DEFAULT_DATETIME))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultAuthorCommentShouldNotBeFound(String filter) throws Exception {
        restAuthorCommentMockMvc.perform(get("/api/author-comments?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingAuthorComment() throws Exception {
        // Get the authorComment
        restAuthorCommentMockMvc.perform(get("/api/author-comments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAuthorComment() throws Exception {
        // Initialize the database
        authorCommentService.save(authorComment);

        int databaseSizeBeforeUpdate = authorCommentRepository.findAll().size();

        // Update the authorComment
        AuthorComment updatedAuthorComment = authorCommentRepository.findOne(authorComment.getId());
        updatedAuthorComment
            .userId(UPDATED_USER_ID)
            .comment(UPDATED_COMMENT)
            .datetime(UPDATED_DATETIME);

        restAuthorCommentMockMvc.perform(put("/api/author-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAuthorComment)))
            .andExpect(status().isOk());

        // Validate the AuthorComment in the database
        List<AuthorComment> authorCommentList = authorCommentRepository.findAll();
        assertThat(authorCommentList).hasSize(databaseSizeBeforeUpdate);
        AuthorComment testAuthorComment = authorCommentList.get(authorCommentList.size() - 1);
        assertThat(testAuthorComment.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testAuthorComment.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testAuthorComment.getDatetime()).isEqualTo(UPDATED_DATETIME);

        // Validate the AuthorComment in Elasticsearch
        AuthorComment authorCommentEs = authorCommentSearchRepository.findOne(testAuthorComment.getId());
        assertThat(authorCommentEs).isEqualToComparingFieldByField(testAuthorComment);
    }

    @Test
    @Transactional
    public void updateNonExistingAuthorComment() throws Exception {
        int databaseSizeBeforeUpdate = authorCommentRepository.findAll().size();

        // Create the AuthorComment

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAuthorCommentMockMvc.perform(put("/api/author-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorComment)))
            .andExpect(status().isCreated());

        // Validate the AuthorComment in the database
        List<AuthorComment> authorCommentList = authorCommentRepository.findAll();
        assertThat(authorCommentList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAuthorComment() throws Exception {
        // Initialize the database
        authorCommentService.save(authorComment);

        int databaseSizeBeforeDelete = authorCommentRepository.findAll().size();

        // Get the authorComment
        restAuthorCommentMockMvc.perform(delete("/api/author-comments/{id}", authorComment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean authorCommentExistsInEs = authorCommentSearchRepository.exists(authorComment.getId());
        assertThat(authorCommentExistsInEs).isFalse();

        // Validate the database is empty
        List<AuthorComment> authorCommentList = authorCommentRepository.findAll();
        assertThat(authorCommentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAuthorComment() throws Exception {
        // Initialize the database
        authorCommentService.save(authorComment);

        // Search the authorComment
        restAuthorCommentMockMvc.perform(get("/api/_search/author-comments?query=id:" + authorComment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
            .andExpect(jsonPath("$.[*].datetime").value(hasItem(sameInstant(DEFAULT_DATETIME))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AuthorComment.class);
        AuthorComment authorComment1 = new AuthorComment();
        authorComment1.setId(1L);
        AuthorComment authorComment2 = new AuthorComment();
        authorComment2.setId(authorComment1.getId());
        assertThat(authorComment1).isEqualTo(authorComment2);
        authorComment2.setId(2L);
        assertThat(authorComment1).isNotEqualTo(authorComment2);
        authorComment1.setId(null);
        assertThat(authorComment1).isNotEqualTo(authorComment2);
    }
}
