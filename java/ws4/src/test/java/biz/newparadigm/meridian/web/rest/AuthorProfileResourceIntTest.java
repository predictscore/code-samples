package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.AuthorProfile;
import biz.newparadigm.meridian.domain.Author;
import biz.newparadigm.meridian.repository.AuthorProfileRepository;
import biz.newparadigm.meridian.service.AuthorProfileService;
import biz.newparadigm.meridian.repository.search.AuthorProfileSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.AuthorProfileCriteria;
import biz.newparadigm.meridian.service.AuthorProfileQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.sameInstant;
import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AuthorProfileResource REST controller.
 *
 * @see AuthorProfileResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class AuthorProfileResourceIntTest {

    private static final String DEFAULT_INFO = "AAAAAAAAAA";
    private static final String UPDATED_INFO = "BBBBBBBBBB";

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_SUBMISSION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_SUBMISSION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private AuthorProfileRepository authorProfileRepository;

    @Autowired
    private AuthorProfileService authorProfileService;

    @Autowired
    private AuthorProfileSearchRepository authorProfileSearchRepository;

    @Autowired
    private AuthorProfileQueryService authorProfileQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAuthorProfileMockMvc;

    private AuthorProfile authorProfile;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AuthorProfileResource authorProfileResource = new AuthorProfileResource(authorProfileService, authorProfileQueryService);
        this.restAuthorProfileMockMvc = MockMvcBuilders.standaloneSetup(authorProfileResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AuthorProfile createEntity(EntityManager em) {
        AuthorProfile authorProfile = new AuthorProfile()
            .info(DEFAULT_INFO)
            .username(DEFAULT_USERNAME)
            .submissionDate(DEFAULT_SUBMISSION_DATE);
        // Add required entity
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        authorProfile.setAuthor(author);
        return authorProfile;
    }

    @Before
    public void initTest() {
        authorProfileSearchRepository.deleteAll();
        authorProfile = createEntity(em);
    }

    @Test
    @Transactional
    public void createAuthorProfile() throws Exception {
        int databaseSizeBeforeCreate = authorProfileRepository.findAll().size();

        // Create the AuthorProfile
        restAuthorProfileMockMvc.perform(post("/api/author-profiles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorProfile)))
            .andExpect(status().isCreated());

        // Validate the AuthorProfile in the database
        List<AuthorProfile> authorProfileList = authorProfileRepository.findAll();
        assertThat(authorProfileList).hasSize(databaseSizeBeforeCreate + 1);
        AuthorProfile testAuthorProfile = authorProfileList.get(authorProfileList.size() - 1);
        assertThat(testAuthorProfile.getInfo()).isEqualTo(DEFAULT_INFO);
        assertThat(testAuthorProfile.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testAuthorProfile.getSubmissionDate()).isEqualTo(DEFAULT_SUBMISSION_DATE);

        // Validate the AuthorProfile in Elasticsearch
        AuthorProfile authorProfileEs = authorProfileSearchRepository.findOne(testAuthorProfile.getId());
        assertThat(authorProfileEs).isEqualToComparingFieldByField(testAuthorProfile);
    }

    @Test
    @Transactional
    public void createAuthorProfileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = authorProfileRepository.findAll().size();

        // Create the AuthorProfile with an existing ID
        authorProfile.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAuthorProfileMockMvc.perform(post("/api/author-profiles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorProfile)))
            .andExpect(status().isBadRequest());

        // Validate the AuthorProfile in the database
        List<AuthorProfile> authorProfileList = authorProfileRepository.findAll();
        assertThat(authorProfileList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkInfoIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorProfileRepository.findAll().size();
        // set the field null
        authorProfile.setInfo(null);

        // Create the AuthorProfile, which fails.

        restAuthorProfileMockMvc.perform(post("/api/author-profiles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorProfile)))
            .andExpect(status().isBadRequest());

        List<AuthorProfile> authorProfileList = authorProfileRepository.findAll();
        assertThat(authorProfileList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUsernameIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorProfileRepository.findAll().size();
        // set the field null
        authorProfile.setUsername(null);

        // Create the AuthorProfile, which fails.

        restAuthorProfileMockMvc.perform(post("/api/author-profiles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorProfile)))
            .andExpect(status().isBadRequest());

        List<AuthorProfile> authorProfileList = authorProfileRepository.findAll();
        assertThat(authorProfileList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSubmissionDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorProfileRepository.findAll().size();
        // set the field null
        authorProfile.setSubmissionDate(null);

        // Create the AuthorProfile, which fails.

        restAuthorProfileMockMvc.perform(post("/api/author-profiles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorProfile)))
            .andExpect(status().isBadRequest());

        List<AuthorProfile> authorProfileList = authorProfileRepository.findAll();
        assertThat(authorProfileList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAuthorProfiles() throws Exception {
        // Initialize the database
        authorProfileRepository.saveAndFlush(authorProfile);

        // Get all the authorProfileList
        restAuthorProfileMockMvc.perform(get("/api/author-profiles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorProfile.getId().intValue())))
            .andExpect(jsonPath("$.[*].info").value(hasItem(DEFAULT_INFO.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].submissionDate").value(hasItem(sameInstant(DEFAULT_SUBMISSION_DATE))));
    }

    @Test
    @Transactional
    public void getAuthorProfile() throws Exception {
        // Initialize the database
        authorProfileRepository.saveAndFlush(authorProfile);

        // Get the authorProfile
        restAuthorProfileMockMvc.perform(get("/api/author-profiles/{id}", authorProfile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(authorProfile.getId().intValue()))
            .andExpect(jsonPath("$.info").value(DEFAULT_INFO.toString()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()))
            .andExpect(jsonPath("$.submissionDate").value(sameInstant(DEFAULT_SUBMISSION_DATE)));
    }

    @Test
    @Transactional
    public void getAllAuthorProfilesByInfoIsEqualToSomething() throws Exception {
        // Initialize the database
        authorProfileRepository.saveAndFlush(authorProfile);

        // Get all the authorProfileList where info equals to DEFAULT_INFO
        defaultAuthorProfileShouldBeFound("info.equals=" + DEFAULT_INFO);

        // Get all the authorProfileList where info equals to UPDATED_INFO
        defaultAuthorProfileShouldNotBeFound("info.equals=" + UPDATED_INFO);
    }

    @Test
    @Transactional
    public void getAllAuthorProfilesByInfoIsInShouldWork() throws Exception {
        // Initialize the database
        authorProfileRepository.saveAndFlush(authorProfile);

        // Get all the authorProfileList where info in DEFAULT_INFO or UPDATED_INFO
        defaultAuthorProfileShouldBeFound("info.in=" + DEFAULT_INFO + "," + UPDATED_INFO);

        // Get all the authorProfileList where info equals to UPDATED_INFO
        defaultAuthorProfileShouldNotBeFound("info.in=" + UPDATED_INFO);
    }

    @Test
    @Transactional
    public void getAllAuthorProfilesByInfoIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorProfileRepository.saveAndFlush(authorProfile);

        // Get all the authorProfileList where info is not null
        defaultAuthorProfileShouldBeFound("info.specified=true");

        // Get all the authorProfileList where info is null
        defaultAuthorProfileShouldNotBeFound("info.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorProfilesByUsernameIsEqualToSomething() throws Exception {
        // Initialize the database
        authorProfileRepository.saveAndFlush(authorProfile);

        // Get all the authorProfileList where username equals to DEFAULT_USERNAME
        defaultAuthorProfileShouldBeFound("username.equals=" + DEFAULT_USERNAME);

        // Get all the authorProfileList where username equals to UPDATED_USERNAME
        defaultAuthorProfileShouldNotBeFound("username.equals=" + UPDATED_USERNAME);
    }

    @Test
    @Transactional
    public void getAllAuthorProfilesByUsernameIsInShouldWork() throws Exception {
        // Initialize the database
        authorProfileRepository.saveAndFlush(authorProfile);

        // Get all the authorProfileList where username in DEFAULT_USERNAME or UPDATED_USERNAME
        defaultAuthorProfileShouldBeFound("username.in=" + DEFAULT_USERNAME + "," + UPDATED_USERNAME);

        // Get all the authorProfileList where username equals to UPDATED_USERNAME
        defaultAuthorProfileShouldNotBeFound("username.in=" + UPDATED_USERNAME);
    }

    @Test
    @Transactional
    public void getAllAuthorProfilesByUsernameIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorProfileRepository.saveAndFlush(authorProfile);

        // Get all the authorProfileList where username is not null
        defaultAuthorProfileShouldBeFound("username.specified=true");

        // Get all the authorProfileList where username is null
        defaultAuthorProfileShouldNotBeFound("username.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorProfilesBySubmissionDateIsEqualToSomething() throws Exception {
        // Initialize the database
        authorProfileRepository.saveAndFlush(authorProfile);

        // Get all the authorProfileList where submissionDate equals to DEFAULT_SUBMISSION_DATE
        defaultAuthorProfileShouldBeFound("submissionDate.equals=" + DEFAULT_SUBMISSION_DATE);

        // Get all the authorProfileList where submissionDate equals to UPDATED_SUBMISSION_DATE
        defaultAuthorProfileShouldNotBeFound("submissionDate.equals=" + UPDATED_SUBMISSION_DATE);
    }

    @Test
    @Transactional
    public void getAllAuthorProfilesBySubmissionDateIsInShouldWork() throws Exception {
        // Initialize the database
        authorProfileRepository.saveAndFlush(authorProfile);

        // Get all the authorProfileList where submissionDate in DEFAULT_SUBMISSION_DATE or UPDATED_SUBMISSION_DATE
        defaultAuthorProfileShouldBeFound("submissionDate.in=" + DEFAULT_SUBMISSION_DATE + "," + UPDATED_SUBMISSION_DATE);

        // Get all the authorProfileList where submissionDate equals to UPDATED_SUBMISSION_DATE
        defaultAuthorProfileShouldNotBeFound("submissionDate.in=" + UPDATED_SUBMISSION_DATE);
    }

    @Test
    @Transactional
    public void getAllAuthorProfilesBySubmissionDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorProfileRepository.saveAndFlush(authorProfile);

        // Get all the authorProfileList where submissionDate is not null
        defaultAuthorProfileShouldBeFound("submissionDate.specified=true");

        // Get all the authorProfileList where submissionDate is null
        defaultAuthorProfileShouldNotBeFound("submissionDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorProfilesBySubmissionDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        authorProfileRepository.saveAndFlush(authorProfile);

        // Get all the authorProfileList where submissionDate greater than or equals to DEFAULT_SUBMISSION_DATE
        defaultAuthorProfileShouldBeFound("submissionDate.greaterOrEqualThan=" + DEFAULT_SUBMISSION_DATE);

        // Get all the authorProfileList where submissionDate greater than or equals to UPDATED_SUBMISSION_DATE
        defaultAuthorProfileShouldNotBeFound("submissionDate.greaterOrEqualThan=" + UPDATED_SUBMISSION_DATE);
    }

    @Test
    @Transactional
    public void getAllAuthorProfilesBySubmissionDateIsLessThanSomething() throws Exception {
        // Initialize the database
        authorProfileRepository.saveAndFlush(authorProfile);

        // Get all the authorProfileList where submissionDate less than or equals to DEFAULT_SUBMISSION_DATE
        defaultAuthorProfileShouldNotBeFound("submissionDate.lessThan=" + DEFAULT_SUBMISSION_DATE);

        // Get all the authorProfileList where submissionDate less than or equals to UPDATED_SUBMISSION_DATE
        defaultAuthorProfileShouldBeFound("submissionDate.lessThan=" + UPDATED_SUBMISSION_DATE);
    }


    @Test
    @Transactional
    public void getAllAuthorProfilesByAuthorIsEqualToSomething() throws Exception {
        // Initialize the database
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        authorProfile.setAuthor(author);
        authorProfileRepository.saveAndFlush(authorProfile);
        Long authorId = author.getId();

        // Get all the authorProfileList where author equals to authorId
        defaultAuthorProfileShouldBeFound("authorId.equals=" + authorId);

        // Get all the authorProfileList where author equals to authorId + 1
        defaultAuthorProfileShouldNotBeFound("authorId.equals=" + (authorId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultAuthorProfileShouldBeFound(String filter) throws Exception {
        restAuthorProfileMockMvc.perform(get("/api/author-profiles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorProfile.getId().intValue())))
            .andExpect(jsonPath("$.[*].info").value(hasItem(DEFAULT_INFO.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].submissionDate").value(hasItem(sameInstant(DEFAULT_SUBMISSION_DATE))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultAuthorProfileShouldNotBeFound(String filter) throws Exception {
        restAuthorProfileMockMvc.perform(get("/api/author-profiles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingAuthorProfile() throws Exception {
        // Get the authorProfile
        restAuthorProfileMockMvc.perform(get("/api/author-profiles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAuthorProfile() throws Exception {
        // Initialize the database
        authorProfileService.save(authorProfile);

        int databaseSizeBeforeUpdate = authorProfileRepository.findAll().size();

        // Update the authorProfile
        AuthorProfile updatedAuthorProfile = authorProfileRepository.findOne(authorProfile.getId());
        updatedAuthorProfile
            .info(UPDATED_INFO)
            .username(UPDATED_USERNAME)
            .submissionDate(UPDATED_SUBMISSION_DATE);

        restAuthorProfileMockMvc.perform(put("/api/author-profiles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAuthorProfile)))
            .andExpect(status().isOk());

        // Validate the AuthorProfile in the database
        List<AuthorProfile> authorProfileList = authorProfileRepository.findAll();
        assertThat(authorProfileList).hasSize(databaseSizeBeforeUpdate);
        AuthorProfile testAuthorProfile = authorProfileList.get(authorProfileList.size() - 1);
        assertThat(testAuthorProfile.getInfo()).isEqualTo(UPDATED_INFO);
        assertThat(testAuthorProfile.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testAuthorProfile.getSubmissionDate()).isEqualTo(UPDATED_SUBMISSION_DATE);

        // Validate the AuthorProfile in Elasticsearch
        AuthorProfile authorProfileEs = authorProfileSearchRepository.findOne(testAuthorProfile.getId());
        assertThat(authorProfileEs).isEqualToComparingFieldByField(testAuthorProfile);
    }

    @Test
    @Transactional
    public void updateNonExistingAuthorProfile() throws Exception {
        int databaseSizeBeforeUpdate = authorProfileRepository.findAll().size();

        // Create the AuthorProfile

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAuthorProfileMockMvc.perform(put("/api/author-profiles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorProfile)))
            .andExpect(status().isCreated());

        // Validate the AuthorProfile in the database
        List<AuthorProfile> authorProfileList = authorProfileRepository.findAll();
        assertThat(authorProfileList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAuthorProfile() throws Exception {
        // Initialize the database
        authorProfileService.save(authorProfile);

        int databaseSizeBeforeDelete = authorProfileRepository.findAll().size();

        // Get the authorProfile
        restAuthorProfileMockMvc.perform(delete("/api/author-profiles/{id}", authorProfile.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean authorProfileExistsInEs = authorProfileSearchRepository.exists(authorProfile.getId());
        assertThat(authorProfileExistsInEs).isFalse();

        // Validate the database is empty
        List<AuthorProfile> authorProfileList = authorProfileRepository.findAll();
        assertThat(authorProfileList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAuthorProfile() throws Exception {
        // Initialize the database
        authorProfileService.save(authorProfile);

        // Search the authorProfile
        restAuthorProfileMockMvc.perform(get("/api/_search/author-profiles?query=id:" + authorProfile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorProfile.getId().intValue())))
            .andExpect(jsonPath("$.[*].info").value(hasItem(DEFAULT_INFO.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].submissionDate").value(hasItem(sameInstant(DEFAULT_SUBMISSION_DATE))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AuthorProfile.class);
        AuthorProfile authorProfile1 = new AuthorProfile();
        authorProfile1.setId(1L);
        AuthorProfile authorProfile2 = new AuthorProfile();
        authorProfile2.setId(authorProfile1.getId());
        assertThat(authorProfile1).isEqualTo(authorProfile2);
        authorProfile2.setId(2L);
        assertThat(authorProfile1).isNotEqualTo(authorProfile2);
        authorProfile1.setId(null);
        assertThat(authorProfile1).isNotEqualTo(authorProfile2);
    }
}
