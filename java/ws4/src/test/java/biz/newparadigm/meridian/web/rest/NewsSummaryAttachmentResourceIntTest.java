package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsSummaryAttachment;
import biz.newparadigm.meridian.domain.NewsSummary;
import biz.newparadigm.meridian.repository.NewsSummaryAttachmentRepository;
import biz.newparadigm.meridian.service.NewsSummaryAttachmentService;
import biz.newparadigm.meridian.repository.search.NewsSummaryAttachmentSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsSummaryAttachmentCriteria;
import biz.newparadigm.meridian.service.NewsSummaryAttachmentQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsSummaryAttachmentResource REST controller.
 *
 * @see NewsSummaryAttachmentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsSummaryAttachmentResourceIntTest {

    private static final String DEFAULT_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_FILENAME = "BBBBBBBBBB";

    private static final byte[] DEFAULT_BLOB = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_BLOB = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_BLOB_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_BLOB_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_TRANSLATION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_TRANSLATION = "BBBBBBBBBB";

    @Autowired
    private NewsSummaryAttachmentRepository newsSummaryAttachmentRepository;

    @Autowired
    private NewsSummaryAttachmentService newsSummaryAttachmentService;

    @Autowired
    private NewsSummaryAttachmentSearchRepository newsSummaryAttachmentSearchRepository;

    @Autowired
    private NewsSummaryAttachmentQueryService newsSummaryAttachmentQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsSummaryAttachmentMockMvc;

    private NewsSummaryAttachment newsSummaryAttachment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsSummaryAttachmentResource newsSummaryAttachmentResource = new NewsSummaryAttachmentResource(newsSummaryAttachmentService, newsSummaryAttachmentQueryService);
        this.restNewsSummaryAttachmentMockMvc = MockMvcBuilders.standaloneSetup(newsSummaryAttachmentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsSummaryAttachment createEntity(EntityManager em) {
        NewsSummaryAttachment newsSummaryAttachment = new NewsSummaryAttachment()
            .filename(DEFAULT_FILENAME)
            .blob(DEFAULT_BLOB)
            .blobContentType(DEFAULT_BLOB_CONTENT_TYPE)
            .description(DEFAULT_DESCRIPTION)
            .descriptionTranslation(DEFAULT_DESCRIPTION_TRANSLATION);
        // Add required entity
        NewsSummary newsSummary = NewsSummaryResourceIntTest.createEntity(em);
        em.persist(newsSummary);
        em.flush();
        newsSummaryAttachment.setNewsSummary(newsSummary);
        return newsSummaryAttachment;
    }

    @Before
    public void initTest() {
        newsSummaryAttachmentSearchRepository.deleteAll();
        newsSummaryAttachment = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsSummaryAttachment() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryAttachmentRepository.findAll().size();

        // Create the NewsSummaryAttachment
        restNewsSummaryAttachmentMockMvc.perform(post("/api/news-summary-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryAttachment)))
            .andExpect(status().isCreated());

        // Validate the NewsSummaryAttachment in the database
        List<NewsSummaryAttachment> newsSummaryAttachmentList = newsSummaryAttachmentRepository.findAll();
        assertThat(newsSummaryAttachmentList).hasSize(databaseSizeBeforeCreate + 1);
        NewsSummaryAttachment testNewsSummaryAttachment = newsSummaryAttachmentList.get(newsSummaryAttachmentList.size() - 1);
        assertThat(testNewsSummaryAttachment.getFilename()).isEqualTo(DEFAULT_FILENAME);
        assertThat(testNewsSummaryAttachment.getBlob()).isEqualTo(DEFAULT_BLOB);
        assertThat(testNewsSummaryAttachment.getBlobContentType()).isEqualTo(DEFAULT_BLOB_CONTENT_TYPE);
        assertThat(testNewsSummaryAttachment.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testNewsSummaryAttachment.getDescriptionTranslation()).isEqualTo(DEFAULT_DESCRIPTION_TRANSLATION);

        // Validate the NewsSummaryAttachment in Elasticsearch
        NewsSummaryAttachment newsSummaryAttachmentEs = newsSummaryAttachmentSearchRepository.findOne(testNewsSummaryAttachment.getId());
        assertThat(newsSummaryAttachmentEs).isEqualToComparingFieldByField(testNewsSummaryAttachment);
    }

    @Test
    @Transactional
    public void createNewsSummaryAttachmentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryAttachmentRepository.findAll().size();

        // Create the NewsSummaryAttachment with an existing ID
        newsSummaryAttachment.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsSummaryAttachmentMockMvc.perform(post("/api/news-summary-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryAttachment)))
            .andExpect(status().isBadRequest());

        // Validate the NewsSummaryAttachment in the database
        List<NewsSummaryAttachment> newsSummaryAttachmentList = newsSummaryAttachmentRepository.findAll();
        assertThat(newsSummaryAttachmentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFilenameIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsSummaryAttachmentRepository.findAll().size();
        // set the field null
        newsSummaryAttachment.setFilename(null);

        // Create the NewsSummaryAttachment, which fails.

        restNewsSummaryAttachmentMockMvc.perform(post("/api/news-summary-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryAttachment)))
            .andExpect(status().isBadRequest());

        List<NewsSummaryAttachment> newsSummaryAttachmentList = newsSummaryAttachmentRepository.findAll();
        assertThat(newsSummaryAttachmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBlobIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsSummaryAttachmentRepository.findAll().size();
        // set the field null
        newsSummaryAttachment.setBlob(null);

        // Create the NewsSummaryAttachment, which fails.

        restNewsSummaryAttachmentMockMvc.perform(post("/api/news-summary-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryAttachment)))
            .andExpect(status().isBadRequest());

        List<NewsSummaryAttachment> newsSummaryAttachmentList = newsSummaryAttachmentRepository.findAll();
        assertThat(newsSummaryAttachmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryAttachments() throws Exception {
        // Initialize the database
        newsSummaryAttachmentRepository.saveAndFlush(newsSummaryAttachment);

        // Get all the newsSummaryAttachmentList
        restNewsSummaryAttachmentMockMvc.perform(get("/api/news-summary-attachments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryAttachment.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionTranslation").value(hasItem(DEFAULT_DESCRIPTION_TRANSLATION.toString())));
    }

    @Test
    @Transactional
    public void getNewsSummaryAttachment() throws Exception {
        // Initialize the database
        newsSummaryAttachmentRepository.saveAndFlush(newsSummaryAttachment);

        // Get the newsSummaryAttachment
        restNewsSummaryAttachmentMockMvc.perform(get("/api/news-summary-attachments/{id}", newsSummaryAttachment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsSummaryAttachment.getId().intValue()))
            .andExpect(jsonPath("$.filename").value(DEFAULT_FILENAME.toString()))
            .andExpect(jsonPath("$.blobContentType").value(DEFAULT_BLOB_CONTENT_TYPE))
            .andExpect(jsonPath("$.blob").value(Base64Utils.encodeToString(DEFAULT_BLOB)))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.descriptionTranslation").value(DEFAULT_DESCRIPTION_TRANSLATION.toString()));
    }

    @Test
    @Transactional
    public void getAllNewsSummaryAttachmentsByFilenameIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryAttachmentRepository.saveAndFlush(newsSummaryAttachment);

        // Get all the newsSummaryAttachmentList where filename equals to DEFAULT_FILENAME
        defaultNewsSummaryAttachmentShouldBeFound("filename.equals=" + DEFAULT_FILENAME);

        // Get all the newsSummaryAttachmentList where filename equals to UPDATED_FILENAME
        defaultNewsSummaryAttachmentShouldNotBeFound("filename.equals=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryAttachmentsByFilenameIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryAttachmentRepository.saveAndFlush(newsSummaryAttachment);

        // Get all the newsSummaryAttachmentList where filename in DEFAULT_FILENAME or UPDATED_FILENAME
        defaultNewsSummaryAttachmentShouldBeFound("filename.in=" + DEFAULT_FILENAME + "," + UPDATED_FILENAME);

        // Get all the newsSummaryAttachmentList where filename equals to UPDATED_FILENAME
        defaultNewsSummaryAttachmentShouldNotBeFound("filename.in=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryAttachmentsByFilenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryAttachmentRepository.saveAndFlush(newsSummaryAttachment);

        // Get all the newsSummaryAttachmentList where filename is not null
        defaultNewsSummaryAttachmentShouldBeFound("filename.specified=true");

        // Get all the newsSummaryAttachmentList where filename is null
        defaultNewsSummaryAttachmentShouldNotBeFound("filename.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryAttachmentsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryAttachmentRepository.saveAndFlush(newsSummaryAttachment);

        // Get all the newsSummaryAttachmentList where description equals to DEFAULT_DESCRIPTION
        defaultNewsSummaryAttachmentShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the newsSummaryAttachmentList where description equals to UPDATED_DESCRIPTION
        defaultNewsSummaryAttachmentShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryAttachmentsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryAttachmentRepository.saveAndFlush(newsSummaryAttachment);

        // Get all the newsSummaryAttachmentList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultNewsSummaryAttachmentShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the newsSummaryAttachmentList where description equals to UPDATED_DESCRIPTION
        defaultNewsSummaryAttachmentShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryAttachmentsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryAttachmentRepository.saveAndFlush(newsSummaryAttachment);

        // Get all the newsSummaryAttachmentList where description is not null
        defaultNewsSummaryAttachmentShouldBeFound("description.specified=true");

        // Get all the newsSummaryAttachmentList where description is null
        defaultNewsSummaryAttachmentShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryAttachmentsByDescriptionTranslationIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryAttachmentRepository.saveAndFlush(newsSummaryAttachment);

        // Get all the newsSummaryAttachmentList where descriptionTranslation equals to DEFAULT_DESCRIPTION_TRANSLATION
        defaultNewsSummaryAttachmentShouldBeFound("descriptionTranslation.equals=" + DEFAULT_DESCRIPTION_TRANSLATION);

        // Get all the newsSummaryAttachmentList where descriptionTranslation equals to UPDATED_DESCRIPTION_TRANSLATION
        defaultNewsSummaryAttachmentShouldNotBeFound("descriptionTranslation.equals=" + UPDATED_DESCRIPTION_TRANSLATION);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryAttachmentsByDescriptionTranslationIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryAttachmentRepository.saveAndFlush(newsSummaryAttachment);

        // Get all the newsSummaryAttachmentList where descriptionTranslation in DEFAULT_DESCRIPTION_TRANSLATION or UPDATED_DESCRIPTION_TRANSLATION
        defaultNewsSummaryAttachmentShouldBeFound("descriptionTranslation.in=" + DEFAULT_DESCRIPTION_TRANSLATION + "," + UPDATED_DESCRIPTION_TRANSLATION);

        // Get all the newsSummaryAttachmentList where descriptionTranslation equals to UPDATED_DESCRIPTION_TRANSLATION
        defaultNewsSummaryAttachmentShouldNotBeFound("descriptionTranslation.in=" + UPDATED_DESCRIPTION_TRANSLATION);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryAttachmentsByDescriptionTranslationIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryAttachmentRepository.saveAndFlush(newsSummaryAttachment);

        // Get all the newsSummaryAttachmentList where descriptionTranslation is not null
        defaultNewsSummaryAttachmentShouldBeFound("descriptionTranslation.specified=true");

        // Get all the newsSummaryAttachmentList where descriptionTranslation is null
        defaultNewsSummaryAttachmentShouldNotBeFound("descriptionTranslation.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryAttachmentsByNewsSummaryIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsSummary newsSummary = NewsSummaryResourceIntTest.createEntity(em);
        em.persist(newsSummary);
        em.flush();
        newsSummaryAttachment.setNewsSummary(newsSummary);
        newsSummaryAttachmentRepository.saveAndFlush(newsSummaryAttachment);
        Long newsSummaryId = newsSummary.getId();

        // Get all the newsSummaryAttachmentList where newsSummary equals to newsSummaryId
        defaultNewsSummaryAttachmentShouldBeFound("newsSummaryId.equals=" + newsSummaryId);

        // Get all the newsSummaryAttachmentList where newsSummary equals to newsSummaryId + 1
        defaultNewsSummaryAttachmentShouldNotBeFound("newsSummaryId.equals=" + (newsSummaryId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsSummaryAttachmentShouldBeFound(String filter) throws Exception {
        restNewsSummaryAttachmentMockMvc.perform(get("/api/news-summary-attachments?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryAttachment.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionTranslation").value(hasItem(DEFAULT_DESCRIPTION_TRANSLATION.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsSummaryAttachmentShouldNotBeFound(String filter) throws Exception {
        restNewsSummaryAttachmentMockMvc.perform(get("/api/news-summary-attachments?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsSummaryAttachment() throws Exception {
        // Get the newsSummaryAttachment
        restNewsSummaryAttachmentMockMvc.perform(get("/api/news-summary-attachments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsSummaryAttachment() throws Exception {
        // Initialize the database
        newsSummaryAttachmentService.save(newsSummaryAttachment);

        int databaseSizeBeforeUpdate = newsSummaryAttachmentRepository.findAll().size();

        // Update the newsSummaryAttachment
        NewsSummaryAttachment updatedNewsSummaryAttachment = newsSummaryAttachmentRepository.findOne(newsSummaryAttachment.getId());
        updatedNewsSummaryAttachment
            .filename(UPDATED_FILENAME)
            .blob(UPDATED_BLOB)
            .blobContentType(UPDATED_BLOB_CONTENT_TYPE)
            .description(UPDATED_DESCRIPTION)
            .descriptionTranslation(UPDATED_DESCRIPTION_TRANSLATION);

        restNewsSummaryAttachmentMockMvc.perform(put("/api/news-summary-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsSummaryAttachment)))
            .andExpect(status().isOk());

        // Validate the NewsSummaryAttachment in the database
        List<NewsSummaryAttachment> newsSummaryAttachmentList = newsSummaryAttachmentRepository.findAll();
        assertThat(newsSummaryAttachmentList).hasSize(databaseSizeBeforeUpdate);
        NewsSummaryAttachment testNewsSummaryAttachment = newsSummaryAttachmentList.get(newsSummaryAttachmentList.size() - 1);
        assertThat(testNewsSummaryAttachment.getFilename()).isEqualTo(UPDATED_FILENAME);
        assertThat(testNewsSummaryAttachment.getBlob()).isEqualTo(UPDATED_BLOB);
        assertThat(testNewsSummaryAttachment.getBlobContentType()).isEqualTo(UPDATED_BLOB_CONTENT_TYPE);
        assertThat(testNewsSummaryAttachment.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testNewsSummaryAttachment.getDescriptionTranslation()).isEqualTo(UPDATED_DESCRIPTION_TRANSLATION);

        // Validate the NewsSummaryAttachment in Elasticsearch
        NewsSummaryAttachment newsSummaryAttachmentEs = newsSummaryAttachmentSearchRepository.findOne(testNewsSummaryAttachment.getId());
        assertThat(newsSummaryAttachmentEs).isEqualToComparingFieldByField(testNewsSummaryAttachment);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsSummaryAttachment() throws Exception {
        int databaseSizeBeforeUpdate = newsSummaryAttachmentRepository.findAll().size();

        // Create the NewsSummaryAttachment

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsSummaryAttachmentMockMvc.perform(put("/api/news-summary-attachments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryAttachment)))
            .andExpect(status().isCreated());

        // Validate the NewsSummaryAttachment in the database
        List<NewsSummaryAttachment> newsSummaryAttachmentList = newsSummaryAttachmentRepository.findAll();
        assertThat(newsSummaryAttachmentList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsSummaryAttachment() throws Exception {
        // Initialize the database
        newsSummaryAttachmentService.save(newsSummaryAttachment);

        int databaseSizeBeforeDelete = newsSummaryAttachmentRepository.findAll().size();

        // Get the newsSummaryAttachment
        restNewsSummaryAttachmentMockMvc.perform(delete("/api/news-summary-attachments/{id}", newsSummaryAttachment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsSummaryAttachmentExistsInEs = newsSummaryAttachmentSearchRepository.exists(newsSummaryAttachment.getId());
        assertThat(newsSummaryAttachmentExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsSummaryAttachment> newsSummaryAttachmentList = newsSummaryAttachmentRepository.findAll();
        assertThat(newsSummaryAttachmentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsSummaryAttachment() throws Exception {
        // Initialize the database
        newsSummaryAttachmentService.save(newsSummaryAttachment);

        // Search the newsSummaryAttachment
        restNewsSummaryAttachmentMockMvc.perform(get("/api/_search/news-summary-attachments?query=id:" + newsSummaryAttachment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryAttachment.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionTranslation").value(hasItem(DEFAULT_DESCRIPTION_TRANSLATION.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsSummaryAttachment.class);
        NewsSummaryAttachment newsSummaryAttachment1 = new NewsSummaryAttachment();
        newsSummaryAttachment1.setId(1L);
        NewsSummaryAttachment newsSummaryAttachment2 = new NewsSummaryAttachment();
        newsSummaryAttachment2.setId(newsSummaryAttachment1.getId());
        assertThat(newsSummaryAttachment1).isEqualTo(newsSummaryAttachment2);
        newsSummaryAttachment2.setId(2L);
        assertThat(newsSummaryAttachment1).isNotEqualTo(newsSummaryAttachment2);
        newsSummaryAttachment1.setId(null);
        assertThat(newsSummaryAttachment1).isNotEqualTo(newsSummaryAttachment2);
    }
}
