package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsConfidentialImage;
import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.repository.NewsConfidentialImageRepository;
import biz.newparadigm.meridian.service.NewsConfidentialImageService;
import biz.newparadigm.meridian.repository.search.NewsConfidentialImageSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsConfidentialImageCriteria;
import biz.newparadigm.meridian.service.NewsConfidentialImageQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsConfidentialImageResource REST controller.
 *
 * @see NewsConfidentialImageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsConfidentialImageResourceIntTest {

    private static final String DEFAULT_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_FILENAME = "BBBBBBBBBB";

    private static final byte[] DEFAULT_BLOB = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_BLOB = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_BLOB_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_BLOB_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_TRANSLATION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_TRANSLATION = "BBBBBBBBBB";

    private static final Integer DEFAULT_IMAGE_ORDER = 1;
    private static final Integer UPDATED_IMAGE_ORDER = 2;

    @Autowired
    private NewsConfidentialImageRepository newsConfidentialImageRepository;

    @Autowired
    private NewsConfidentialImageService newsConfidentialImageService;

    @Autowired
    private NewsConfidentialImageSearchRepository newsConfidentialImageSearchRepository;

    @Autowired
    private NewsConfidentialImageQueryService newsConfidentialImageQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsConfidentialImageMockMvc;

    private NewsConfidentialImage newsConfidentialImage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsConfidentialImageResource newsConfidentialImageResource = new NewsConfidentialImageResource(newsConfidentialImageService, newsConfidentialImageQueryService);
        this.restNewsConfidentialImageMockMvc = MockMvcBuilders.standaloneSetup(newsConfidentialImageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsConfidentialImage createEntity(EntityManager em) {
        NewsConfidentialImage newsConfidentialImage = new NewsConfidentialImage()
            .filename(DEFAULT_FILENAME)
            .blob(DEFAULT_BLOB)
            .blobContentType(DEFAULT_BLOB_CONTENT_TYPE)
            .description(DEFAULT_DESCRIPTION)
            .descriptionTranslation(DEFAULT_DESCRIPTION_TRANSLATION)
            .imageOrder(DEFAULT_IMAGE_ORDER);
        // Add required entity
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsConfidentialImage.setNews(news);
        return newsConfidentialImage;
    }

    @Before
    public void initTest() {
        newsConfidentialImageSearchRepository.deleteAll();
        newsConfidentialImage = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsConfidentialImage() throws Exception {
        int databaseSizeBeforeCreate = newsConfidentialImageRepository.findAll().size();

        // Create the NewsConfidentialImage
        restNewsConfidentialImageMockMvc.perform(post("/api/news-confidential-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsConfidentialImage)))
            .andExpect(status().isCreated());

        // Validate the NewsConfidentialImage in the database
        List<NewsConfidentialImage> newsConfidentialImageList = newsConfidentialImageRepository.findAll();
        assertThat(newsConfidentialImageList).hasSize(databaseSizeBeforeCreate + 1);
        NewsConfidentialImage testNewsConfidentialImage = newsConfidentialImageList.get(newsConfidentialImageList.size() - 1);
        assertThat(testNewsConfidentialImage.getFilename()).isEqualTo(DEFAULT_FILENAME);
        assertThat(testNewsConfidentialImage.getBlob()).isEqualTo(DEFAULT_BLOB);
        assertThat(testNewsConfidentialImage.getBlobContentType()).isEqualTo(DEFAULT_BLOB_CONTENT_TYPE);
        assertThat(testNewsConfidentialImage.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testNewsConfidentialImage.getDescriptionTranslation()).isEqualTo(DEFAULT_DESCRIPTION_TRANSLATION);
        assertThat(testNewsConfidentialImage.getImageOrder()).isEqualTo(DEFAULT_IMAGE_ORDER);

        // Validate the NewsConfidentialImage in Elasticsearch
        NewsConfidentialImage newsConfidentialImageEs = newsConfidentialImageSearchRepository.findOne(testNewsConfidentialImage.getId());
        assertThat(newsConfidentialImageEs).isEqualToComparingFieldByField(testNewsConfidentialImage);
    }

    @Test
    @Transactional
    public void createNewsConfidentialImageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsConfidentialImageRepository.findAll().size();

        // Create the NewsConfidentialImage with an existing ID
        newsConfidentialImage.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsConfidentialImageMockMvc.perform(post("/api/news-confidential-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsConfidentialImage)))
            .andExpect(status().isBadRequest());

        // Validate the NewsConfidentialImage in the database
        List<NewsConfidentialImage> newsConfidentialImageList = newsConfidentialImageRepository.findAll();
        assertThat(newsConfidentialImageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFilenameIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsConfidentialImageRepository.findAll().size();
        // set the field null
        newsConfidentialImage.setFilename(null);

        // Create the NewsConfidentialImage, which fails.

        restNewsConfidentialImageMockMvc.perform(post("/api/news-confidential-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsConfidentialImage)))
            .andExpect(status().isBadRequest());

        List<NewsConfidentialImage> newsConfidentialImageList = newsConfidentialImageRepository.findAll();
        assertThat(newsConfidentialImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBlobIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsConfidentialImageRepository.findAll().size();
        // set the field null
        newsConfidentialImage.setBlob(null);

        // Create the NewsConfidentialImage, which fails.

        restNewsConfidentialImageMockMvc.perform(post("/api/news-confidential-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsConfidentialImage)))
            .andExpect(status().isBadRequest());

        List<NewsConfidentialImage> newsConfidentialImageList = newsConfidentialImageRepository.findAll();
        assertThat(newsConfidentialImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNewsConfidentialImages() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get all the newsConfidentialImageList
        restNewsConfidentialImageMockMvc.perform(get("/api/news-confidential-images?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsConfidentialImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionTranslation").value(hasItem(DEFAULT_DESCRIPTION_TRANSLATION.toString())))
            .andExpect(jsonPath("$.[*].imageOrder").value(hasItem(DEFAULT_IMAGE_ORDER)));
    }

    @Test
    @Transactional
    public void getNewsConfidentialImage() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get the newsConfidentialImage
        restNewsConfidentialImageMockMvc.perform(get("/api/news-confidential-images/{id}", newsConfidentialImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsConfidentialImage.getId().intValue()))
            .andExpect(jsonPath("$.filename").value(DEFAULT_FILENAME.toString()))
            .andExpect(jsonPath("$.blobContentType").value(DEFAULT_BLOB_CONTENT_TYPE))
            .andExpect(jsonPath("$.blob").value(Base64Utils.encodeToString(DEFAULT_BLOB)))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.descriptionTranslation").value(DEFAULT_DESCRIPTION_TRANSLATION.toString()))
            .andExpect(jsonPath("$.imageOrder").value(DEFAULT_IMAGE_ORDER));
    }

    @Test
    @Transactional
    public void getAllNewsConfidentialImagesByFilenameIsEqualToSomething() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get all the newsConfidentialImageList where filename equals to DEFAULT_FILENAME
        defaultNewsConfidentialImageShouldBeFound("filename.equals=" + DEFAULT_FILENAME);

        // Get all the newsConfidentialImageList where filename equals to UPDATED_FILENAME
        defaultNewsConfidentialImageShouldNotBeFound("filename.equals=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllNewsConfidentialImagesByFilenameIsInShouldWork() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get all the newsConfidentialImageList where filename in DEFAULT_FILENAME or UPDATED_FILENAME
        defaultNewsConfidentialImageShouldBeFound("filename.in=" + DEFAULT_FILENAME + "," + UPDATED_FILENAME);

        // Get all the newsConfidentialImageList where filename equals to UPDATED_FILENAME
        defaultNewsConfidentialImageShouldNotBeFound("filename.in=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllNewsConfidentialImagesByFilenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get all the newsConfidentialImageList where filename is not null
        defaultNewsConfidentialImageShouldBeFound("filename.specified=true");

        // Get all the newsConfidentialImageList where filename is null
        defaultNewsConfidentialImageShouldNotBeFound("filename.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsConfidentialImagesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get all the newsConfidentialImageList where description equals to DEFAULT_DESCRIPTION
        defaultNewsConfidentialImageShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the newsConfidentialImageList where description equals to UPDATED_DESCRIPTION
        defaultNewsConfidentialImageShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsConfidentialImagesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get all the newsConfidentialImageList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultNewsConfidentialImageShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the newsConfidentialImageList where description equals to UPDATED_DESCRIPTION
        defaultNewsConfidentialImageShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsConfidentialImagesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get all the newsConfidentialImageList where description is not null
        defaultNewsConfidentialImageShouldBeFound("description.specified=true");

        // Get all the newsConfidentialImageList where description is null
        defaultNewsConfidentialImageShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsConfidentialImagesByDescriptionTranslationIsEqualToSomething() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get all the newsConfidentialImageList where descriptionTranslation equals to DEFAULT_DESCRIPTION_TRANSLATION
        defaultNewsConfidentialImageShouldBeFound("descriptionTranslation.equals=" + DEFAULT_DESCRIPTION_TRANSLATION);

        // Get all the newsConfidentialImageList where descriptionTranslation equals to UPDATED_DESCRIPTION_TRANSLATION
        defaultNewsConfidentialImageShouldNotBeFound("descriptionTranslation.equals=" + UPDATED_DESCRIPTION_TRANSLATION);
    }

    @Test
    @Transactional
    public void getAllNewsConfidentialImagesByDescriptionTranslationIsInShouldWork() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get all the newsConfidentialImageList where descriptionTranslation in DEFAULT_DESCRIPTION_TRANSLATION or UPDATED_DESCRIPTION_TRANSLATION
        defaultNewsConfidentialImageShouldBeFound("descriptionTranslation.in=" + DEFAULT_DESCRIPTION_TRANSLATION + "," + UPDATED_DESCRIPTION_TRANSLATION);

        // Get all the newsConfidentialImageList where descriptionTranslation equals to UPDATED_DESCRIPTION_TRANSLATION
        defaultNewsConfidentialImageShouldNotBeFound("descriptionTranslation.in=" + UPDATED_DESCRIPTION_TRANSLATION);
    }

    @Test
    @Transactional
    public void getAllNewsConfidentialImagesByDescriptionTranslationIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get all the newsConfidentialImageList where descriptionTranslation is not null
        defaultNewsConfidentialImageShouldBeFound("descriptionTranslation.specified=true");

        // Get all the newsConfidentialImageList where descriptionTranslation is null
        defaultNewsConfidentialImageShouldNotBeFound("descriptionTranslation.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsConfidentialImagesByImageOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get all the newsConfidentialImageList where imageOrder equals to DEFAULT_IMAGE_ORDER
        defaultNewsConfidentialImageShouldBeFound("imageOrder.equals=" + DEFAULT_IMAGE_ORDER);

        // Get all the newsConfidentialImageList where imageOrder equals to UPDATED_IMAGE_ORDER
        defaultNewsConfidentialImageShouldNotBeFound("imageOrder.equals=" + UPDATED_IMAGE_ORDER);
    }

    @Test
    @Transactional
    public void getAllNewsConfidentialImagesByImageOrderIsInShouldWork() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get all the newsConfidentialImageList where imageOrder in DEFAULT_IMAGE_ORDER or UPDATED_IMAGE_ORDER
        defaultNewsConfidentialImageShouldBeFound("imageOrder.in=" + DEFAULT_IMAGE_ORDER + "," + UPDATED_IMAGE_ORDER);

        // Get all the newsConfidentialImageList where imageOrder equals to UPDATED_IMAGE_ORDER
        defaultNewsConfidentialImageShouldNotBeFound("imageOrder.in=" + UPDATED_IMAGE_ORDER);
    }

    @Test
    @Transactional
    public void getAllNewsConfidentialImagesByImageOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get all the newsConfidentialImageList where imageOrder is not null
        defaultNewsConfidentialImageShouldBeFound("imageOrder.specified=true");

        // Get all the newsConfidentialImageList where imageOrder is null
        defaultNewsConfidentialImageShouldNotBeFound("imageOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsConfidentialImagesByImageOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get all the newsConfidentialImageList where imageOrder greater than or equals to DEFAULT_IMAGE_ORDER
        defaultNewsConfidentialImageShouldBeFound("imageOrder.greaterOrEqualThan=" + DEFAULT_IMAGE_ORDER);

        // Get all the newsConfidentialImageList where imageOrder greater than or equals to UPDATED_IMAGE_ORDER
        defaultNewsConfidentialImageShouldNotBeFound("imageOrder.greaterOrEqualThan=" + UPDATED_IMAGE_ORDER);
    }

    @Test
    @Transactional
    public void getAllNewsConfidentialImagesByImageOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);

        // Get all the newsConfidentialImageList where imageOrder less than or equals to DEFAULT_IMAGE_ORDER
        defaultNewsConfidentialImageShouldNotBeFound("imageOrder.lessThan=" + DEFAULT_IMAGE_ORDER);

        // Get all the newsConfidentialImageList where imageOrder less than or equals to UPDATED_IMAGE_ORDER
        defaultNewsConfidentialImageShouldBeFound("imageOrder.lessThan=" + UPDATED_IMAGE_ORDER);
    }


    @Test
    @Transactional
    public void getAllNewsConfidentialImagesByNewsIsEqualToSomething() throws Exception {
        // Initialize the database
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsConfidentialImage.setNews(news);
        newsConfidentialImageRepository.saveAndFlush(newsConfidentialImage);
        Long newsId = news.getId();

        // Get all the newsConfidentialImageList where news equals to newsId
        defaultNewsConfidentialImageShouldBeFound("newsId.equals=" + newsId);

        // Get all the newsConfidentialImageList where news equals to newsId + 1
        defaultNewsConfidentialImageShouldNotBeFound("newsId.equals=" + (newsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsConfidentialImageShouldBeFound(String filter) throws Exception {
        restNewsConfidentialImageMockMvc.perform(get("/api/news-confidential-images?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsConfidentialImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionTranslation").value(hasItem(DEFAULT_DESCRIPTION_TRANSLATION.toString())))
            .andExpect(jsonPath("$.[*].imageOrder").value(hasItem(DEFAULT_IMAGE_ORDER)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsConfidentialImageShouldNotBeFound(String filter) throws Exception {
        restNewsConfidentialImageMockMvc.perform(get("/api/news-confidential-images?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsConfidentialImage() throws Exception {
        // Get the newsConfidentialImage
        restNewsConfidentialImageMockMvc.perform(get("/api/news-confidential-images/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsConfidentialImage() throws Exception {
        // Initialize the database
        newsConfidentialImageService.save(newsConfidentialImage);

        int databaseSizeBeforeUpdate = newsConfidentialImageRepository.findAll().size();

        // Update the newsConfidentialImage
        NewsConfidentialImage updatedNewsConfidentialImage = newsConfidentialImageRepository.findOne(newsConfidentialImage.getId());
        updatedNewsConfidentialImage
            .filename(UPDATED_FILENAME)
            .blob(UPDATED_BLOB)
            .blobContentType(UPDATED_BLOB_CONTENT_TYPE)
            .description(UPDATED_DESCRIPTION)
            .descriptionTranslation(UPDATED_DESCRIPTION_TRANSLATION)
            .imageOrder(UPDATED_IMAGE_ORDER);

        restNewsConfidentialImageMockMvc.perform(put("/api/news-confidential-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsConfidentialImage)))
            .andExpect(status().isOk());

        // Validate the NewsConfidentialImage in the database
        List<NewsConfidentialImage> newsConfidentialImageList = newsConfidentialImageRepository.findAll();
        assertThat(newsConfidentialImageList).hasSize(databaseSizeBeforeUpdate);
        NewsConfidentialImage testNewsConfidentialImage = newsConfidentialImageList.get(newsConfidentialImageList.size() - 1);
        assertThat(testNewsConfidentialImage.getFilename()).isEqualTo(UPDATED_FILENAME);
        assertThat(testNewsConfidentialImage.getBlob()).isEqualTo(UPDATED_BLOB);
        assertThat(testNewsConfidentialImage.getBlobContentType()).isEqualTo(UPDATED_BLOB_CONTENT_TYPE);
        assertThat(testNewsConfidentialImage.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testNewsConfidentialImage.getDescriptionTranslation()).isEqualTo(UPDATED_DESCRIPTION_TRANSLATION);
        assertThat(testNewsConfidentialImage.getImageOrder()).isEqualTo(UPDATED_IMAGE_ORDER);

        // Validate the NewsConfidentialImage in Elasticsearch
        NewsConfidentialImage newsConfidentialImageEs = newsConfidentialImageSearchRepository.findOne(testNewsConfidentialImage.getId());
        assertThat(newsConfidentialImageEs).isEqualToComparingFieldByField(testNewsConfidentialImage);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsConfidentialImage() throws Exception {
        int databaseSizeBeforeUpdate = newsConfidentialImageRepository.findAll().size();

        // Create the NewsConfidentialImage

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsConfidentialImageMockMvc.perform(put("/api/news-confidential-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsConfidentialImage)))
            .andExpect(status().isCreated());

        // Validate the NewsConfidentialImage in the database
        List<NewsConfidentialImage> newsConfidentialImageList = newsConfidentialImageRepository.findAll();
        assertThat(newsConfidentialImageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsConfidentialImage() throws Exception {
        // Initialize the database
        newsConfidentialImageService.save(newsConfidentialImage);

        int databaseSizeBeforeDelete = newsConfidentialImageRepository.findAll().size();

        // Get the newsConfidentialImage
        restNewsConfidentialImageMockMvc.perform(delete("/api/news-confidential-images/{id}", newsConfidentialImage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsConfidentialImageExistsInEs = newsConfidentialImageSearchRepository.exists(newsConfidentialImage.getId());
        assertThat(newsConfidentialImageExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsConfidentialImage> newsConfidentialImageList = newsConfidentialImageRepository.findAll();
        assertThat(newsConfidentialImageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsConfidentialImage() throws Exception {
        // Initialize the database
        newsConfidentialImageService.save(newsConfidentialImage);

        // Search the newsConfidentialImage
        restNewsConfidentialImageMockMvc.perform(get("/api/_search/news-confidential-images?query=id:" + newsConfidentialImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsConfidentialImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionTranslation").value(hasItem(DEFAULT_DESCRIPTION_TRANSLATION.toString())))
            .andExpect(jsonPath("$.[*].imageOrder").value(hasItem(DEFAULT_IMAGE_ORDER)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsConfidentialImage.class);
        NewsConfidentialImage newsConfidentialImage1 = new NewsConfidentialImage();
        newsConfidentialImage1.setId(1L);
        NewsConfidentialImage newsConfidentialImage2 = new NewsConfidentialImage();
        newsConfidentialImage2.setId(newsConfidentialImage1.getId());
        assertThat(newsConfidentialImage1).isEqualTo(newsConfidentialImage2);
        newsConfidentialImage2.setId(2L);
        assertThat(newsConfidentialImage1).isNotEqualTo(newsConfidentialImage2);
        newsConfidentialImage1.setId(null);
        assertThat(newsConfidentialImage1).isNotEqualTo(newsConfidentialImage2);
    }
}
