package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsComment;
import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.domain.NewsComment;
import biz.newparadigm.meridian.repository.NewsCommentRepository;
import biz.newparadigm.meridian.service.NewsCommentService;
import biz.newparadigm.meridian.repository.search.NewsCommentSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsCommentCriteria;
import biz.newparadigm.meridian.service.NewsCommentQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.sameInstant;
import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsCommentResource REST controller.
 *
 * @see NewsCommentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsCommentResourceIntTest {

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATETIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATETIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private NewsCommentRepository newsCommentRepository;

    @Autowired
    private NewsCommentService newsCommentService;

    @Autowired
    private NewsCommentSearchRepository newsCommentSearchRepository;

    @Autowired
    private NewsCommentQueryService newsCommentQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsCommentMockMvc;

    private NewsComment newsComment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsCommentResource newsCommentResource = new NewsCommentResource(newsCommentService, newsCommentQueryService);
        this.restNewsCommentMockMvc = MockMvcBuilders.standaloneSetup(newsCommentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsComment createEntity(EntityManager em) {
        NewsComment newsComment = new NewsComment()
            .userId(DEFAULT_USER_ID)
            .comment(DEFAULT_COMMENT)
            .datetime(DEFAULT_DATETIME);
        // Add required entity
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsComment.setNews(news);
        return newsComment;
    }

    @Before
    public void initTest() {
        newsCommentSearchRepository.deleteAll();
        newsComment = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsComment() throws Exception {
        int databaseSizeBeforeCreate = newsCommentRepository.findAll().size();

        // Create the NewsComment
        restNewsCommentMockMvc.perform(post("/api/news-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsComment)))
            .andExpect(status().isCreated());

        // Validate the NewsComment in the database
        List<NewsComment> newsCommentList = newsCommentRepository.findAll();
        assertThat(newsCommentList).hasSize(databaseSizeBeforeCreate + 1);
        NewsComment testNewsComment = newsCommentList.get(newsCommentList.size() - 1);
        assertThat(testNewsComment.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testNewsComment.getComment()).isEqualTo(DEFAULT_COMMENT);
        assertThat(testNewsComment.getDatetime()).isEqualTo(DEFAULT_DATETIME);

        // Validate the NewsComment in Elasticsearch
        NewsComment newsCommentEs = newsCommentSearchRepository.findOne(testNewsComment.getId());
        assertThat(newsCommentEs).isEqualToComparingFieldByField(testNewsComment);
    }

    @Test
    @Transactional
    public void createNewsCommentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsCommentRepository.findAll().size();

        // Create the NewsComment with an existing ID
        newsComment.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsCommentMockMvc.perform(post("/api/news-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsComment)))
            .andExpect(status().isBadRequest());

        // Validate the NewsComment in the database
        List<NewsComment> newsCommentList = newsCommentRepository.findAll();
        assertThat(newsCommentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsCommentRepository.findAll().size();
        // set the field null
        newsComment.setUserId(null);

        // Create the NewsComment, which fails.

        restNewsCommentMockMvc.perform(post("/api/news-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsComment)))
            .andExpect(status().isBadRequest());

        List<NewsComment> newsCommentList = newsCommentRepository.findAll();
        assertThat(newsCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCommentIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsCommentRepository.findAll().size();
        // set the field null
        newsComment.setComment(null);

        // Create the NewsComment, which fails.

        restNewsCommentMockMvc.perform(post("/api/news-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsComment)))
            .andExpect(status().isBadRequest());

        List<NewsComment> newsCommentList = newsCommentRepository.findAll();
        assertThat(newsCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDatetimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsCommentRepository.findAll().size();
        // set the field null
        newsComment.setDatetime(null);

        // Create the NewsComment, which fails.

        restNewsCommentMockMvc.perform(post("/api/news-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsComment)))
            .andExpect(status().isBadRequest());

        List<NewsComment> newsCommentList = newsCommentRepository.findAll();
        assertThat(newsCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNewsComments() throws Exception {
        // Initialize the database
        newsCommentRepository.saveAndFlush(newsComment);

        // Get all the newsCommentList
        restNewsCommentMockMvc.perform(get("/api/news-comments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
            .andExpect(jsonPath("$.[*].datetime").value(hasItem(sameInstant(DEFAULT_DATETIME))));
    }

    @Test
    @Transactional
    public void getNewsComment() throws Exception {
        // Initialize the database
        newsCommentRepository.saveAndFlush(newsComment);

        // Get the newsComment
        restNewsCommentMockMvc.perform(get("/api/news-comments/{id}", newsComment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsComment.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.toString()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()))
            .andExpect(jsonPath("$.datetime").value(sameInstant(DEFAULT_DATETIME)));
    }

    @Test
    @Transactional
    public void getAllNewsCommentsByUserIdIsEqualToSomething() throws Exception {
        // Initialize the database
        newsCommentRepository.saveAndFlush(newsComment);

        // Get all the newsCommentList where userId equals to DEFAULT_USER_ID
        defaultNewsCommentShouldBeFound("userId.equals=" + DEFAULT_USER_ID);

        // Get all the newsCommentList where userId equals to UPDATED_USER_ID
        defaultNewsCommentShouldNotBeFound("userId.equals=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllNewsCommentsByUserIdIsInShouldWork() throws Exception {
        // Initialize the database
        newsCommentRepository.saveAndFlush(newsComment);

        // Get all the newsCommentList where userId in DEFAULT_USER_ID or UPDATED_USER_ID
        defaultNewsCommentShouldBeFound("userId.in=" + DEFAULT_USER_ID + "," + UPDATED_USER_ID);

        // Get all the newsCommentList where userId equals to UPDATED_USER_ID
        defaultNewsCommentShouldNotBeFound("userId.in=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllNewsCommentsByUserIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsCommentRepository.saveAndFlush(newsComment);

        // Get all the newsCommentList where userId is not null
        defaultNewsCommentShouldBeFound("userId.specified=true");

        // Get all the newsCommentList where userId is null
        defaultNewsCommentShouldNotBeFound("userId.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsCommentsByCommentIsEqualToSomething() throws Exception {
        // Initialize the database
        newsCommentRepository.saveAndFlush(newsComment);

        // Get all the newsCommentList where comment equals to DEFAULT_COMMENT
        defaultNewsCommentShouldBeFound("comment.equals=" + DEFAULT_COMMENT);

        // Get all the newsCommentList where comment equals to UPDATED_COMMENT
        defaultNewsCommentShouldNotBeFound("comment.equals=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    public void getAllNewsCommentsByCommentIsInShouldWork() throws Exception {
        // Initialize the database
        newsCommentRepository.saveAndFlush(newsComment);

        // Get all the newsCommentList where comment in DEFAULT_COMMENT or UPDATED_COMMENT
        defaultNewsCommentShouldBeFound("comment.in=" + DEFAULT_COMMENT + "," + UPDATED_COMMENT);

        // Get all the newsCommentList where comment equals to UPDATED_COMMENT
        defaultNewsCommentShouldNotBeFound("comment.in=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    public void getAllNewsCommentsByCommentIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsCommentRepository.saveAndFlush(newsComment);

        // Get all the newsCommentList where comment is not null
        defaultNewsCommentShouldBeFound("comment.specified=true");

        // Get all the newsCommentList where comment is null
        defaultNewsCommentShouldNotBeFound("comment.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsCommentsByDatetimeIsEqualToSomething() throws Exception {
        // Initialize the database
        newsCommentRepository.saveAndFlush(newsComment);

        // Get all the newsCommentList where datetime equals to DEFAULT_DATETIME
        defaultNewsCommentShouldBeFound("datetime.equals=" + DEFAULT_DATETIME);

        // Get all the newsCommentList where datetime equals to UPDATED_DATETIME
        defaultNewsCommentShouldNotBeFound("datetime.equals=" + UPDATED_DATETIME);
    }

    @Test
    @Transactional
    public void getAllNewsCommentsByDatetimeIsInShouldWork() throws Exception {
        // Initialize the database
        newsCommentRepository.saveAndFlush(newsComment);

        // Get all the newsCommentList where datetime in DEFAULT_DATETIME or UPDATED_DATETIME
        defaultNewsCommentShouldBeFound("datetime.in=" + DEFAULT_DATETIME + "," + UPDATED_DATETIME);

        // Get all the newsCommentList where datetime equals to UPDATED_DATETIME
        defaultNewsCommentShouldNotBeFound("datetime.in=" + UPDATED_DATETIME);
    }

    @Test
    @Transactional
    public void getAllNewsCommentsByDatetimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsCommentRepository.saveAndFlush(newsComment);

        // Get all the newsCommentList where datetime is not null
        defaultNewsCommentShouldBeFound("datetime.specified=true");

        // Get all the newsCommentList where datetime is null
        defaultNewsCommentShouldNotBeFound("datetime.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsCommentsByDatetimeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        newsCommentRepository.saveAndFlush(newsComment);

        // Get all the newsCommentList where datetime greater than or equals to DEFAULT_DATETIME
        defaultNewsCommentShouldBeFound("datetime.greaterOrEqualThan=" + DEFAULT_DATETIME);

        // Get all the newsCommentList where datetime greater than or equals to UPDATED_DATETIME
        defaultNewsCommentShouldNotBeFound("datetime.greaterOrEqualThan=" + UPDATED_DATETIME);
    }

    @Test
    @Transactional
    public void getAllNewsCommentsByDatetimeIsLessThanSomething() throws Exception {
        // Initialize the database
        newsCommentRepository.saveAndFlush(newsComment);

        // Get all the newsCommentList where datetime less than or equals to DEFAULT_DATETIME
        defaultNewsCommentShouldNotBeFound("datetime.lessThan=" + DEFAULT_DATETIME);

        // Get all the newsCommentList where datetime less than or equals to UPDATED_DATETIME
        defaultNewsCommentShouldBeFound("datetime.lessThan=" + UPDATED_DATETIME);
    }


    @Test
    @Transactional
    public void getAllNewsCommentsByNewsIsEqualToSomething() throws Exception {
        // Initialize the database
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsComment.setNews(news);
        newsCommentRepository.saveAndFlush(newsComment);
        Long newsId = news.getId();

        // Get all the newsCommentList where news equals to newsId
        defaultNewsCommentShouldBeFound("newsId.equals=" + newsId);

        // Get all the newsCommentList where news equals to newsId + 1
        defaultNewsCommentShouldNotBeFound("newsId.equals=" + (newsId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsCommentsByParentCommentIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsComment parentComment = NewsCommentResourceIntTest.createEntity(em);
        em.persist(parentComment);
        em.flush();
        newsComment.setParentComment(parentComment);
        newsCommentRepository.saveAndFlush(newsComment);
        Long parentCommentId = parentComment.getId();

        // Get all the newsCommentList where parentComment equals to parentCommentId
        defaultNewsCommentShouldBeFound("parentCommentId.equals=" + parentCommentId);

        // Get all the newsCommentList where parentComment equals to parentCommentId + 1
        defaultNewsCommentShouldNotBeFound("parentCommentId.equals=" + (parentCommentId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsCommentShouldBeFound(String filter) throws Exception {
        restNewsCommentMockMvc.perform(get("/api/news-comments?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
            .andExpect(jsonPath("$.[*].datetime").value(hasItem(sameInstant(DEFAULT_DATETIME))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsCommentShouldNotBeFound(String filter) throws Exception {
        restNewsCommentMockMvc.perform(get("/api/news-comments?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsComment() throws Exception {
        // Get the newsComment
        restNewsCommentMockMvc.perform(get("/api/news-comments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsComment() throws Exception {
        // Initialize the database
        newsCommentService.save(newsComment);

        int databaseSizeBeforeUpdate = newsCommentRepository.findAll().size();

        // Update the newsComment
        NewsComment updatedNewsComment = newsCommentRepository.findOne(newsComment.getId());
        updatedNewsComment
            .userId(UPDATED_USER_ID)
            .comment(UPDATED_COMMENT)
            .datetime(UPDATED_DATETIME);

        restNewsCommentMockMvc.perform(put("/api/news-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsComment)))
            .andExpect(status().isOk());

        // Validate the NewsComment in the database
        List<NewsComment> newsCommentList = newsCommentRepository.findAll();
        assertThat(newsCommentList).hasSize(databaseSizeBeforeUpdate);
        NewsComment testNewsComment = newsCommentList.get(newsCommentList.size() - 1);
        assertThat(testNewsComment.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testNewsComment.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testNewsComment.getDatetime()).isEqualTo(UPDATED_DATETIME);

        // Validate the NewsComment in Elasticsearch
        NewsComment newsCommentEs = newsCommentSearchRepository.findOne(testNewsComment.getId());
        assertThat(newsCommentEs).isEqualToComparingFieldByField(testNewsComment);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsComment() throws Exception {
        int databaseSizeBeforeUpdate = newsCommentRepository.findAll().size();

        // Create the NewsComment

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsCommentMockMvc.perform(put("/api/news-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsComment)))
            .andExpect(status().isCreated());

        // Validate the NewsComment in the database
        List<NewsComment> newsCommentList = newsCommentRepository.findAll();
        assertThat(newsCommentList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsComment() throws Exception {
        // Initialize the database
        newsCommentService.save(newsComment);

        int databaseSizeBeforeDelete = newsCommentRepository.findAll().size();

        // Get the newsComment
        restNewsCommentMockMvc.perform(delete("/api/news-comments/{id}", newsComment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsCommentExistsInEs = newsCommentSearchRepository.exists(newsComment.getId());
        assertThat(newsCommentExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsComment> newsCommentList = newsCommentRepository.findAll();
        assertThat(newsCommentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsComment() throws Exception {
        // Initialize the database
        newsCommentService.save(newsComment);

        // Search the newsComment
        restNewsCommentMockMvc.perform(get("/api/_search/news-comments?query=id:" + newsComment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
            .andExpect(jsonPath("$.[*].datetime").value(hasItem(sameInstant(DEFAULT_DATETIME))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsComment.class);
        NewsComment newsComment1 = new NewsComment();
        newsComment1.setId(1L);
        NewsComment newsComment2 = new NewsComment();
        newsComment2.setId(newsComment1.getId());
        assertThat(newsComment1).isEqualTo(newsComment2);
        newsComment2.setId(2L);
        assertThat(newsComment1).isNotEqualTo(newsComment2);
        newsComment1.setId(null);
        assertThat(newsComment1).isNotEqualTo(newsComment2);
    }
}
