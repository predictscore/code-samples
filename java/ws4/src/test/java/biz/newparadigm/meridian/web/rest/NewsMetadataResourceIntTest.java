package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsMetadata;
import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.domain.Metagroup;
import biz.newparadigm.meridian.domain.Metafield;
import biz.newparadigm.meridian.repository.NewsMetadataRepository;
import biz.newparadigm.meridian.service.NewsMetadataService;
import biz.newparadigm.meridian.repository.search.NewsMetadataSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsMetadataCriteria;
import biz.newparadigm.meridian.service.NewsMetadataQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.sameInstant;
import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsMetadataResource REST controller.
 *
 * @see NewsMetadataResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsMetadataResourceIntTest {

    private static final String DEFAULT_VALUE_STRING = "AAAAAAAAAA";
    private static final String UPDATED_VALUE_STRING = "BBBBBBBBBB";

    private static final Double DEFAULT_VALUE_NUMBER = 1D;
    private static final Double UPDATED_VALUE_NUMBER = 2D;

    private static final ZonedDateTime DEFAULT_VALUE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_VALUE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private NewsMetadataRepository newsMetadataRepository;

    @Autowired
    private NewsMetadataService newsMetadataService;

    @Autowired
    private NewsMetadataSearchRepository newsMetadataSearchRepository;

    @Autowired
    private NewsMetadataQueryService newsMetadataQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsMetadataMockMvc;

    private NewsMetadata newsMetadata;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsMetadataResource newsMetadataResource = new NewsMetadataResource(newsMetadataService, newsMetadataQueryService);
        this.restNewsMetadataMockMvc = MockMvcBuilders.standaloneSetup(newsMetadataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsMetadata createEntity(EntityManager em) {
        NewsMetadata newsMetadata = new NewsMetadata()
            .valueString(DEFAULT_VALUE_STRING)
            .valueNumber(DEFAULT_VALUE_NUMBER)
            .valueDate(DEFAULT_VALUE_DATE);
        // Add required entity
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsMetadata.setNews(news);
        // Add required entity
        Metagroup metagroup = MetagroupResourceIntTest.createEntity(em);
        em.persist(metagroup);
        em.flush();
        newsMetadata.setMetagroup(metagroup);
        // Add required entity
        Metafield metafield = MetafieldResourceIntTest.createEntity(em);
        em.persist(metafield);
        em.flush();
        newsMetadata.setMetafield(metafield);
        return newsMetadata;
    }

    @Before
    public void initTest() {
        newsMetadataSearchRepository.deleteAll();
        newsMetadata = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsMetadata() throws Exception {
        int databaseSizeBeforeCreate = newsMetadataRepository.findAll().size();

        // Create the NewsMetadata
        restNewsMetadataMockMvc.perform(post("/api/news-metadata")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsMetadata)))
            .andExpect(status().isCreated());

        // Validate the NewsMetadata in the database
        List<NewsMetadata> newsMetadataList = newsMetadataRepository.findAll();
        assertThat(newsMetadataList).hasSize(databaseSizeBeforeCreate + 1);
        NewsMetadata testNewsMetadata = newsMetadataList.get(newsMetadataList.size() - 1);
        assertThat(testNewsMetadata.getValueString()).isEqualTo(DEFAULT_VALUE_STRING);
        assertThat(testNewsMetadata.getValueNumber()).isEqualTo(DEFAULT_VALUE_NUMBER);
        assertThat(testNewsMetadata.getValueDate()).isEqualTo(DEFAULT_VALUE_DATE);

        // Validate the NewsMetadata in Elasticsearch
        NewsMetadata newsMetadataEs = newsMetadataSearchRepository.findOne(testNewsMetadata.getId());
        assertThat(newsMetadataEs).isEqualToComparingFieldByField(testNewsMetadata);
    }

    @Test
    @Transactional
    public void createNewsMetadataWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsMetadataRepository.findAll().size();

        // Create the NewsMetadata with an existing ID
        newsMetadata.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsMetadataMockMvc.perform(post("/api/news-metadata")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsMetadata)))
            .andExpect(status().isBadRequest());

        // Validate the NewsMetadata in the database
        List<NewsMetadata> newsMetadataList = newsMetadataRepository.findAll();
        assertThat(newsMetadataList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllNewsMetadata() throws Exception {
        // Initialize the database
        newsMetadataRepository.saveAndFlush(newsMetadata);

        // Get all the newsMetadataList
        restNewsMetadataMockMvc.perform(get("/api/news-metadata?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsMetadata.getId().intValue())))
            .andExpect(jsonPath("$.[*].valueString").value(hasItem(DEFAULT_VALUE_STRING.toString())))
            .andExpect(jsonPath("$.[*].valueNumber").value(hasItem(DEFAULT_VALUE_NUMBER.doubleValue())))
            .andExpect(jsonPath("$.[*].valueDate").value(hasItem(sameInstant(DEFAULT_VALUE_DATE))));
    }

    @Test
    @Transactional
    public void getNewsMetadata() throws Exception {
        // Initialize the database
        newsMetadataRepository.saveAndFlush(newsMetadata);

        // Get the newsMetadata
        restNewsMetadataMockMvc.perform(get("/api/news-metadata/{id}", newsMetadata.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsMetadata.getId().intValue()))
            .andExpect(jsonPath("$.valueString").value(DEFAULT_VALUE_STRING.toString()))
            .andExpect(jsonPath("$.valueNumber").value(DEFAULT_VALUE_NUMBER.doubleValue()))
            .andExpect(jsonPath("$.valueDate").value(sameInstant(DEFAULT_VALUE_DATE)));
    }

    @Test
    @Transactional
    public void getAllNewsMetadataByValueStringIsEqualToSomething() throws Exception {
        // Initialize the database
        newsMetadataRepository.saveAndFlush(newsMetadata);

        // Get all the newsMetadataList where valueString equals to DEFAULT_VALUE_STRING
        defaultNewsMetadataShouldBeFound("valueString.equals=" + DEFAULT_VALUE_STRING);

        // Get all the newsMetadataList where valueString equals to UPDATED_VALUE_STRING
        defaultNewsMetadataShouldNotBeFound("valueString.equals=" + UPDATED_VALUE_STRING);
    }

    @Test
    @Transactional
    public void getAllNewsMetadataByValueStringIsInShouldWork() throws Exception {
        // Initialize the database
        newsMetadataRepository.saveAndFlush(newsMetadata);

        // Get all the newsMetadataList where valueString in DEFAULT_VALUE_STRING or UPDATED_VALUE_STRING
        defaultNewsMetadataShouldBeFound("valueString.in=" + DEFAULT_VALUE_STRING + "," + UPDATED_VALUE_STRING);

        // Get all the newsMetadataList where valueString equals to UPDATED_VALUE_STRING
        defaultNewsMetadataShouldNotBeFound("valueString.in=" + UPDATED_VALUE_STRING);
    }

    @Test
    @Transactional
    public void getAllNewsMetadataByValueStringIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsMetadataRepository.saveAndFlush(newsMetadata);

        // Get all the newsMetadataList where valueString is not null
        defaultNewsMetadataShouldBeFound("valueString.specified=true");

        // Get all the newsMetadataList where valueString is null
        defaultNewsMetadataShouldNotBeFound("valueString.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsMetadataByValueNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        newsMetadataRepository.saveAndFlush(newsMetadata);

        // Get all the newsMetadataList where valueNumber equals to DEFAULT_VALUE_NUMBER
        defaultNewsMetadataShouldBeFound("valueNumber.equals=" + DEFAULT_VALUE_NUMBER);

        // Get all the newsMetadataList where valueNumber equals to UPDATED_VALUE_NUMBER
        defaultNewsMetadataShouldNotBeFound("valueNumber.equals=" + UPDATED_VALUE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllNewsMetadataByValueNumberIsInShouldWork() throws Exception {
        // Initialize the database
        newsMetadataRepository.saveAndFlush(newsMetadata);

        // Get all the newsMetadataList where valueNumber in DEFAULT_VALUE_NUMBER or UPDATED_VALUE_NUMBER
        defaultNewsMetadataShouldBeFound("valueNumber.in=" + DEFAULT_VALUE_NUMBER + "," + UPDATED_VALUE_NUMBER);

        // Get all the newsMetadataList where valueNumber equals to UPDATED_VALUE_NUMBER
        defaultNewsMetadataShouldNotBeFound("valueNumber.in=" + UPDATED_VALUE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllNewsMetadataByValueNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsMetadataRepository.saveAndFlush(newsMetadata);

        // Get all the newsMetadataList where valueNumber is not null
        defaultNewsMetadataShouldBeFound("valueNumber.specified=true");

        // Get all the newsMetadataList where valueNumber is null
        defaultNewsMetadataShouldNotBeFound("valueNumber.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsMetadataByValueDateIsEqualToSomething() throws Exception {
        // Initialize the database
        newsMetadataRepository.saveAndFlush(newsMetadata);

        // Get all the newsMetadataList where valueDate equals to DEFAULT_VALUE_DATE
        defaultNewsMetadataShouldBeFound("valueDate.equals=" + DEFAULT_VALUE_DATE);

        // Get all the newsMetadataList where valueDate equals to UPDATED_VALUE_DATE
        defaultNewsMetadataShouldNotBeFound("valueDate.equals=" + UPDATED_VALUE_DATE);
    }

    @Test
    @Transactional
    public void getAllNewsMetadataByValueDateIsInShouldWork() throws Exception {
        // Initialize the database
        newsMetadataRepository.saveAndFlush(newsMetadata);

        // Get all the newsMetadataList where valueDate in DEFAULT_VALUE_DATE or UPDATED_VALUE_DATE
        defaultNewsMetadataShouldBeFound("valueDate.in=" + DEFAULT_VALUE_DATE + "," + UPDATED_VALUE_DATE);

        // Get all the newsMetadataList where valueDate equals to UPDATED_VALUE_DATE
        defaultNewsMetadataShouldNotBeFound("valueDate.in=" + UPDATED_VALUE_DATE);
    }

    @Test
    @Transactional
    public void getAllNewsMetadataByValueDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsMetadataRepository.saveAndFlush(newsMetadata);

        // Get all the newsMetadataList where valueDate is not null
        defaultNewsMetadataShouldBeFound("valueDate.specified=true");

        // Get all the newsMetadataList where valueDate is null
        defaultNewsMetadataShouldNotBeFound("valueDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsMetadataByValueDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        newsMetadataRepository.saveAndFlush(newsMetadata);

        // Get all the newsMetadataList where valueDate greater than or equals to DEFAULT_VALUE_DATE
        defaultNewsMetadataShouldBeFound("valueDate.greaterOrEqualThan=" + DEFAULT_VALUE_DATE);

        // Get all the newsMetadataList where valueDate greater than or equals to UPDATED_VALUE_DATE
        defaultNewsMetadataShouldNotBeFound("valueDate.greaterOrEqualThan=" + UPDATED_VALUE_DATE);
    }

    @Test
    @Transactional
    public void getAllNewsMetadataByValueDateIsLessThanSomething() throws Exception {
        // Initialize the database
        newsMetadataRepository.saveAndFlush(newsMetadata);

        // Get all the newsMetadataList where valueDate less than or equals to DEFAULT_VALUE_DATE
        defaultNewsMetadataShouldNotBeFound("valueDate.lessThan=" + DEFAULT_VALUE_DATE);

        // Get all the newsMetadataList where valueDate less than or equals to UPDATED_VALUE_DATE
        defaultNewsMetadataShouldBeFound("valueDate.lessThan=" + UPDATED_VALUE_DATE);
    }


    @Test
    @Transactional
    public void getAllNewsMetadataByNewsIsEqualToSomething() throws Exception {
        // Initialize the database
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsMetadata.setNews(news);
        newsMetadataRepository.saveAndFlush(newsMetadata);
        Long newsId = news.getId();

        // Get all the newsMetadataList where news equals to newsId
        defaultNewsMetadataShouldBeFound("newsId.equals=" + newsId);

        // Get all the newsMetadataList where news equals to newsId + 1
        defaultNewsMetadataShouldNotBeFound("newsId.equals=" + (newsId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsMetadataByMetagroupIsEqualToSomething() throws Exception {
        // Initialize the database
        Metagroup metagroup = MetagroupResourceIntTest.createEntity(em);
        em.persist(metagroup);
        em.flush();
        newsMetadata.setMetagroup(metagroup);
        newsMetadataRepository.saveAndFlush(newsMetadata);
        Long metagroupId = metagroup.getId();

        // Get all the newsMetadataList where metagroup equals to metagroupId
        defaultNewsMetadataShouldBeFound("metagroupId.equals=" + metagroupId);

        // Get all the newsMetadataList where metagroup equals to metagroupId + 1
        defaultNewsMetadataShouldNotBeFound("metagroupId.equals=" + (metagroupId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsMetadataByMetafieldIsEqualToSomething() throws Exception {
        // Initialize the database
        Metafield metafield = MetafieldResourceIntTest.createEntity(em);
        em.persist(metafield);
        em.flush();
        newsMetadata.setMetafield(metafield);
        newsMetadataRepository.saveAndFlush(newsMetadata);
        Long metafieldId = metafield.getId();

        // Get all the newsMetadataList where metafield equals to metafieldId
        defaultNewsMetadataShouldBeFound("metafieldId.equals=" + metafieldId);

        // Get all the newsMetadataList where metafield equals to metafieldId + 1
        defaultNewsMetadataShouldNotBeFound("metafieldId.equals=" + (metafieldId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsMetadataShouldBeFound(String filter) throws Exception {
        restNewsMetadataMockMvc.perform(get("/api/news-metadata?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsMetadata.getId().intValue())))
            .andExpect(jsonPath("$.[*].valueString").value(hasItem(DEFAULT_VALUE_STRING.toString())))
            .andExpect(jsonPath("$.[*].valueNumber").value(hasItem(DEFAULT_VALUE_NUMBER.doubleValue())))
            .andExpect(jsonPath("$.[*].valueDate").value(hasItem(sameInstant(DEFAULT_VALUE_DATE))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsMetadataShouldNotBeFound(String filter) throws Exception {
        restNewsMetadataMockMvc.perform(get("/api/news-metadata?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsMetadata() throws Exception {
        // Get the newsMetadata
        restNewsMetadataMockMvc.perform(get("/api/news-metadata/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsMetadata() throws Exception {
        // Initialize the database
        newsMetadataService.save(newsMetadata);

        int databaseSizeBeforeUpdate = newsMetadataRepository.findAll().size();

        // Update the newsMetadata
        NewsMetadata updatedNewsMetadata = newsMetadataRepository.findOne(newsMetadata.getId());
        updatedNewsMetadata
            .valueString(UPDATED_VALUE_STRING)
            .valueNumber(UPDATED_VALUE_NUMBER)
            .valueDate(UPDATED_VALUE_DATE);

        restNewsMetadataMockMvc.perform(put("/api/news-metadata")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsMetadata)))
            .andExpect(status().isOk());

        // Validate the NewsMetadata in the database
        List<NewsMetadata> newsMetadataList = newsMetadataRepository.findAll();
        assertThat(newsMetadataList).hasSize(databaseSizeBeforeUpdate);
        NewsMetadata testNewsMetadata = newsMetadataList.get(newsMetadataList.size() - 1);
        assertThat(testNewsMetadata.getValueString()).isEqualTo(UPDATED_VALUE_STRING);
        assertThat(testNewsMetadata.getValueNumber()).isEqualTo(UPDATED_VALUE_NUMBER);
        assertThat(testNewsMetadata.getValueDate()).isEqualTo(UPDATED_VALUE_DATE);

        // Validate the NewsMetadata in Elasticsearch
        NewsMetadata newsMetadataEs = newsMetadataSearchRepository.findOne(testNewsMetadata.getId());
        assertThat(newsMetadataEs).isEqualToComparingFieldByField(testNewsMetadata);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsMetadata() throws Exception {
        int databaseSizeBeforeUpdate = newsMetadataRepository.findAll().size();

        // Create the NewsMetadata

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsMetadataMockMvc.perform(put("/api/news-metadata")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsMetadata)))
            .andExpect(status().isCreated());

        // Validate the NewsMetadata in the database
        List<NewsMetadata> newsMetadataList = newsMetadataRepository.findAll();
        assertThat(newsMetadataList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsMetadata() throws Exception {
        // Initialize the database
        newsMetadataService.save(newsMetadata);

        int databaseSizeBeforeDelete = newsMetadataRepository.findAll().size();

        // Get the newsMetadata
        restNewsMetadataMockMvc.perform(delete("/api/news-metadata/{id}", newsMetadata.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsMetadataExistsInEs = newsMetadataSearchRepository.exists(newsMetadata.getId());
        assertThat(newsMetadataExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsMetadata> newsMetadataList = newsMetadataRepository.findAll();
        assertThat(newsMetadataList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsMetadata() throws Exception {
        // Initialize the database
        newsMetadataService.save(newsMetadata);

        // Search the newsMetadata
        restNewsMetadataMockMvc.perform(get("/api/_search/news-metadata?query=id:" + newsMetadata.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsMetadata.getId().intValue())))
            .andExpect(jsonPath("$.[*].valueString").value(hasItem(DEFAULT_VALUE_STRING.toString())))
            .andExpect(jsonPath("$.[*].valueNumber").value(hasItem(DEFAULT_VALUE_NUMBER.doubleValue())))
            .andExpect(jsonPath("$.[*].valueDate").value(hasItem(sameInstant(DEFAULT_VALUE_DATE))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsMetadata.class);
        NewsMetadata newsMetadata1 = new NewsMetadata();
        newsMetadata1.setId(1L);
        NewsMetadata newsMetadata2 = new NewsMetadata();
        newsMetadata2.setId(newsMetadata1.getId());
        assertThat(newsMetadata1).isEqualTo(newsMetadata2);
        newsMetadata2.setId(2L);
        assertThat(newsMetadata1).isNotEqualTo(newsMetadata2);
        newsMetadata1.setId(null);
        assertThat(newsMetadata1).isNotEqualTo(newsMetadata2);
    }
}
