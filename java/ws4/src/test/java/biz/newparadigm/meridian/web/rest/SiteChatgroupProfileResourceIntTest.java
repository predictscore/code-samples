package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.SiteChatgroup;
import biz.newparadigm.meridian.domain.SiteChatgroupProfile;
import biz.newparadigm.meridian.repository.SiteChatgroupProfileRepository;
import biz.newparadigm.meridian.service.SiteChatgroupProfileService;
import biz.newparadigm.meridian.repository.search.SiteChatgroupProfileSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.SiteChatgroupProfileCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupProfileQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.sameInstant;
import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SiteChatgroupProfileResource REST controller.
 *
 * @see SiteChatgroupProfileResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class SiteChatgroupProfileResourceIntTest {

    private static final String DEFAULT_INFO = "AAAAAAAAAA";
    private static final String UPDATED_INFO = "BBBBBBBBBB";

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_SUBMISSION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_SUBMISSION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private SiteChatgroupProfileRepository siteChatgroupProfileRepository;

    @Autowired
    private SiteChatgroupProfileService siteChatgroupProfileService;

    @Autowired
    private SiteChatgroupProfileSearchRepository siteChatgroupProfileSearchRepository;

    @Autowired
    private SiteChatgroupProfileQueryService siteChatgroupProfileQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSiteChatgroupProfileMockMvc;

    private SiteChatgroupProfile siteChatgroupProfile;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SiteChatgroupProfileResource siteChatgroupProfileResource = new SiteChatgroupProfileResource(siteChatgroupProfileService, siteChatgroupProfileQueryService);
        this.restSiteChatgroupProfileMockMvc = MockMvcBuilders.standaloneSetup(siteChatgroupProfileResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SiteChatgroupProfile createEntity(EntityManager em) {
        SiteChatgroupProfile siteChatgroupProfile = new SiteChatgroupProfile()
            .info(DEFAULT_INFO)
            .username(DEFAULT_USERNAME)
            .submissionDate(DEFAULT_SUBMISSION_DATE);
        // Add required entity
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupProfile.setSiteChatgroup(siteChatgroup);
        return siteChatgroupProfile;
    }

    @Before
    public void initTest() {
        siteChatgroupProfileSearchRepository.deleteAll();
        siteChatgroupProfile = createEntity(em);
    }

    @Test
    @Transactional
    public void createSiteChatgroupProfile() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupProfileRepository.findAll().size();

        // Create the SiteChatgroupProfile
        restSiteChatgroupProfileMockMvc.perform(post("/api/site-chatgroup-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupProfile)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupProfile in the database
        List<SiteChatgroupProfile> siteChatgroupProfileList = siteChatgroupProfileRepository.findAll();
        assertThat(siteChatgroupProfileList).hasSize(databaseSizeBeforeCreate + 1);
        SiteChatgroupProfile testSiteChatgroupProfile = siteChatgroupProfileList.get(siteChatgroupProfileList.size() - 1);
        assertThat(testSiteChatgroupProfile.getInfo()).isEqualTo(DEFAULT_INFO);
        assertThat(testSiteChatgroupProfile.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testSiteChatgroupProfile.getSubmissionDate()).isEqualTo(DEFAULT_SUBMISSION_DATE);

        // Validate the SiteChatgroupProfile in Elasticsearch
        SiteChatgroupProfile siteChatgroupProfileEs = siteChatgroupProfileSearchRepository.findOne(testSiteChatgroupProfile.getId());
        assertThat(siteChatgroupProfileEs).isEqualToComparingFieldByField(testSiteChatgroupProfile);
    }

    @Test
    @Transactional
    public void createSiteChatgroupProfileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupProfileRepository.findAll().size();

        // Create the SiteChatgroupProfile with an existing ID
        siteChatgroupProfile.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSiteChatgroupProfileMockMvc.perform(post("/api/site-chatgroup-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupProfile)))
            .andExpect(status().isBadRequest());

        // Validate the SiteChatgroupProfile in the database
        List<SiteChatgroupProfile> siteChatgroupProfileList = siteChatgroupProfileRepository.findAll();
        assertThat(siteChatgroupProfileList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkInfoIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupProfileRepository.findAll().size();
        // set the field null
        siteChatgroupProfile.setInfo(null);

        // Create the SiteChatgroupProfile, which fails.

        restSiteChatgroupProfileMockMvc.perform(post("/api/site-chatgroup-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupProfile)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupProfile> siteChatgroupProfileList = siteChatgroupProfileRepository.findAll();
        assertThat(siteChatgroupProfileList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUsernameIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupProfileRepository.findAll().size();
        // set the field null
        siteChatgroupProfile.setUsername(null);

        // Create the SiteChatgroupProfile, which fails.

        restSiteChatgroupProfileMockMvc.perform(post("/api/site-chatgroup-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupProfile)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupProfile> siteChatgroupProfileList = siteChatgroupProfileRepository.findAll();
        assertThat(siteChatgroupProfileList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSubmissionDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = siteChatgroupProfileRepository.findAll().size();
        // set the field null
        siteChatgroupProfile.setSubmissionDate(null);

        // Create the SiteChatgroupProfile, which fails.

        restSiteChatgroupProfileMockMvc.perform(post("/api/site-chatgroup-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupProfile)))
            .andExpect(status().isBadRequest());

        List<SiteChatgroupProfile> siteChatgroupProfileList = siteChatgroupProfileRepository.findAll();
        assertThat(siteChatgroupProfileList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupProfiles() throws Exception {
        // Initialize the database
        siteChatgroupProfileRepository.saveAndFlush(siteChatgroupProfile);

        // Get all the siteChatgroupProfileList
        restSiteChatgroupProfileMockMvc.perform(get("/api/site-chatgroup-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupProfile.getId().intValue())))
            .andExpect(jsonPath("$.[*].info").value(hasItem(DEFAULT_INFO.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].submissionDate").value(hasItem(sameInstant(DEFAULT_SUBMISSION_DATE))));
    }

    @Test
    @Transactional
    public void getSiteChatgroupProfile() throws Exception {
        // Initialize the database
        siteChatgroupProfileRepository.saveAndFlush(siteChatgroupProfile);

        // Get the siteChatgroupProfile
        restSiteChatgroupProfileMockMvc.perform(get("/api/site-chatgroup-infos/{id}", siteChatgroupProfile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(siteChatgroupProfile.getId().intValue()))
            .andExpect(jsonPath("$.info").value(DEFAULT_INFO.toString()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()))
            .andExpect(jsonPath("$.submissionDate").value(sameInstant(DEFAULT_SUBMISSION_DATE)));
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupProfilesByInfoIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupProfileRepository.saveAndFlush(siteChatgroupProfile);

        // Get all the siteChatgroupProfileList where info equals to DEFAULT_INFO
        defaultSiteChatgroupProfileShouldBeFound("info.equals=" + DEFAULT_INFO);

        // Get all the siteChatgroupProfileList where info equals to UPDATED_INFO
        defaultSiteChatgroupProfileShouldNotBeFound("info.equals=" + UPDATED_INFO);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupProfilesByInfoIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupProfileRepository.saveAndFlush(siteChatgroupProfile);

        // Get all the siteChatgroupProfileList where info in DEFAULT_INFO or UPDATED_INFO
        defaultSiteChatgroupProfileShouldBeFound("info.in=" + DEFAULT_INFO + "," + UPDATED_INFO);

        // Get all the siteChatgroupProfileList where info equals to UPDATED_INFO
        defaultSiteChatgroupProfileShouldNotBeFound("info.in=" + UPDATED_INFO);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupProfilesByInfoIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupProfileRepository.saveAndFlush(siteChatgroupProfile);

        // Get all the siteChatgroupProfileList where info is not null
        defaultSiteChatgroupProfileShouldBeFound("info.specified=true");

        // Get all the siteChatgroupProfileList where info is null
        defaultSiteChatgroupProfileShouldNotBeFound("info.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupProfilesByUsernameIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupProfileRepository.saveAndFlush(siteChatgroupProfile);

        // Get all the siteChatgroupProfileList where username equals to DEFAULT_USERNAME
        defaultSiteChatgroupProfileShouldBeFound("username.equals=" + DEFAULT_USERNAME);

        // Get all the siteChatgroupProfileList where username equals to UPDATED_USERNAME
        defaultSiteChatgroupProfileShouldNotBeFound("username.equals=" + UPDATED_USERNAME);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupProfilesByUsernameIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupProfileRepository.saveAndFlush(siteChatgroupProfile);

        // Get all the siteChatgroupProfileList where username in DEFAULT_USERNAME or UPDATED_USERNAME
        defaultSiteChatgroupProfileShouldBeFound("username.in=" + DEFAULT_USERNAME + "," + UPDATED_USERNAME);

        // Get all the siteChatgroupProfileList where username equals to UPDATED_USERNAME
        defaultSiteChatgroupProfileShouldNotBeFound("username.in=" + UPDATED_USERNAME);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupProfilesByUsernameIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupProfileRepository.saveAndFlush(siteChatgroupProfile);

        // Get all the siteChatgroupProfileList where username is not null
        defaultSiteChatgroupProfileShouldBeFound("username.specified=true");

        // Get all the siteChatgroupProfileList where username is null
        defaultSiteChatgroupProfileShouldNotBeFound("username.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupProfilesBySubmissionDateIsEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupProfileRepository.saveAndFlush(siteChatgroupProfile);

        // Get all the siteChatgroupProfileList where submissionDate equals to DEFAULT_SUBMISSION_DATE
        defaultSiteChatgroupProfileShouldBeFound("submissionDate.equals=" + DEFAULT_SUBMISSION_DATE);

        // Get all the siteChatgroupProfileList where submissionDate equals to UPDATED_SUBMISSION_DATE
        defaultSiteChatgroupProfileShouldNotBeFound("submissionDate.equals=" + UPDATED_SUBMISSION_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupProfilesBySubmissionDateIsInShouldWork() throws Exception {
        // Initialize the database
        siteChatgroupProfileRepository.saveAndFlush(siteChatgroupProfile);

        // Get all the siteChatgroupProfileList where submissionDate in DEFAULT_SUBMISSION_DATE or UPDATED_SUBMISSION_DATE
        defaultSiteChatgroupProfileShouldBeFound("submissionDate.in=" + DEFAULT_SUBMISSION_DATE + "," + UPDATED_SUBMISSION_DATE);

        // Get all the siteChatgroupProfileList where submissionDate equals to UPDATED_SUBMISSION_DATE
        defaultSiteChatgroupProfileShouldNotBeFound("submissionDate.in=" + UPDATED_SUBMISSION_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupProfilesBySubmissionDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        siteChatgroupProfileRepository.saveAndFlush(siteChatgroupProfile);

        // Get all the siteChatgroupProfileList where submissionDate is not null
        defaultSiteChatgroupProfileShouldBeFound("submissionDate.specified=true");

        // Get all the siteChatgroupProfileList where submissionDate is null
        defaultSiteChatgroupProfileShouldNotBeFound("submissionDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupProfilesBySubmissionDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        siteChatgroupProfileRepository.saveAndFlush(siteChatgroupProfile);

        // Get all the siteChatgroupProfileList where submissionDate greater than or equals to DEFAULT_SUBMISSION_DATE
        defaultSiteChatgroupProfileShouldBeFound("submissionDate.greaterOrEqualThan=" + DEFAULT_SUBMISSION_DATE);

        // Get all the siteChatgroupProfileList where submissionDate greater than or equals to UPDATED_SUBMISSION_DATE
        defaultSiteChatgroupProfileShouldNotBeFound("submissionDate.greaterOrEqualThan=" + UPDATED_SUBMISSION_DATE);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupProfilesBySubmissionDateIsLessThanSomething() throws Exception {
        // Initialize the database
        siteChatgroupProfileRepository.saveAndFlush(siteChatgroupProfile);

        // Get all the siteChatgroupProfileList where submissionDate less than or equals to DEFAULT_SUBMISSION_DATE
        defaultSiteChatgroupProfileShouldNotBeFound("submissionDate.lessThan=" + DEFAULT_SUBMISSION_DATE);

        // Get all the siteChatgroupProfileList where submissionDate less than or equals to UPDATED_SUBMISSION_DATE
        defaultSiteChatgroupProfileShouldBeFound("submissionDate.lessThan=" + UPDATED_SUBMISSION_DATE);
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupProfilesBySiteChatgroupIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupProfile.setSiteChatgroup(siteChatgroup);
        siteChatgroupProfileRepository.saveAndFlush(siteChatgroupProfile);
        Long siteChatgroupId = siteChatgroup.getId();

        // Get all the siteChatgroupProfileList where siteChatgroup equals to siteChatgroupId
        defaultSiteChatgroupProfileShouldBeFound("siteChatgroupId.equals=" + siteChatgroupId);

        // Get all the siteChatgroupProfileList where siteChatgroup equals to siteChatgroupId + 1
        defaultSiteChatgroupProfileShouldNotBeFound("siteChatgroupId.equals=" + (siteChatgroupId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSiteChatgroupProfileShouldBeFound(String filter) throws Exception {
        restSiteChatgroupProfileMockMvc.perform(get("/api/site-chatgroup-infos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupProfile.getId().intValue())))
            .andExpect(jsonPath("$.[*].info").value(hasItem(DEFAULT_INFO.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].submissionDate").value(hasItem(sameInstant(DEFAULT_SUBMISSION_DATE))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSiteChatgroupProfileShouldNotBeFound(String filter) throws Exception {
        restSiteChatgroupProfileMockMvc.perform(get("/api/site-chatgroup-infos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingSiteChatgroupProfile() throws Exception {
        // Get the siteChatgroupProfile
        restSiteChatgroupProfileMockMvc.perform(get("/api/site-chatgroup-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSiteChatgroupProfile() throws Exception {
        // Initialize the database
        siteChatgroupProfileService.save(siteChatgroupProfile);

        int databaseSizeBeforeUpdate = siteChatgroupProfileRepository.findAll().size();

        // Update the siteChatgroupProfile
        SiteChatgroupProfile updatedSiteChatgroupProfile = siteChatgroupProfileRepository.findOne(siteChatgroupProfile.getId());
        updatedSiteChatgroupProfile
            .info(UPDATED_INFO)
            .username(UPDATED_USERNAME)
            .submissionDate(UPDATED_SUBMISSION_DATE);

        restSiteChatgroupProfileMockMvc.perform(put("/api/site-chatgroup-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSiteChatgroupProfile)))
            .andExpect(status().isOk());

        // Validate the SiteChatgroupProfile in the database
        List<SiteChatgroupProfile> siteChatgroupProfileList = siteChatgroupProfileRepository.findAll();
        assertThat(siteChatgroupProfileList).hasSize(databaseSizeBeforeUpdate);
        SiteChatgroupProfile testSiteChatgroupProfile = siteChatgroupProfileList.get(siteChatgroupProfileList.size() - 1);
        assertThat(testSiteChatgroupProfile.getInfo()).isEqualTo(UPDATED_INFO);
        assertThat(testSiteChatgroupProfile.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testSiteChatgroupProfile.getSubmissionDate()).isEqualTo(UPDATED_SUBMISSION_DATE);

        // Validate the SiteChatgroupProfile in Elasticsearch
        SiteChatgroupProfile siteChatgroupProfileEs = siteChatgroupProfileSearchRepository.findOne(testSiteChatgroupProfile.getId());
        assertThat(siteChatgroupProfileEs).isEqualToComparingFieldByField(testSiteChatgroupProfile);
    }

    @Test
    @Transactional
    public void updateNonExistingSiteChatgroupProfile() throws Exception {
        int databaseSizeBeforeUpdate = siteChatgroupProfileRepository.findAll().size();

        // Create the SiteChatgroupProfile

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSiteChatgroupProfileMockMvc.perform(put("/api/site-chatgroup-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupProfile)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupProfile in the database
        List<SiteChatgroupProfile> siteChatgroupProfileList = siteChatgroupProfileRepository.findAll();
        assertThat(siteChatgroupProfileList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSiteChatgroupProfile() throws Exception {
        // Initialize the database
        siteChatgroupProfileService.save(siteChatgroupProfile);

        int databaseSizeBeforeDelete = siteChatgroupProfileRepository.findAll().size();

        // Get the siteChatgroupProfile
        restSiteChatgroupProfileMockMvc.perform(delete("/api/site-chatgroup-infos/{id}", siteChatgroupProfile.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean siteChatgroupProfileExistsInEs = siteChatgroupProfileSearchRepository.exists(siteChatgroupProfile.getId());
        assertThat(siteChatgroupProfileExistsInEs).isFalse();

        // Validate the database is empty
        List<SiteChatgroupProfile> siteChatgroupProfileList = siteChatgroupProfileRepository.findAll();
        assertThat(siteChatgroupProfileList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSiteChatgroupProfile() throws Exception {
        // Initialize the database
        siteChatgroupProfileService.save(siteChatgroupProfile);

        // Search the siteChatgroupProfile
        restSiteChatgroupProfileMockMvc.perform(get("/api/_search/site-chatgroup-infos?query=id:" + siteChatgroupProfile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupProfile.getId().intValue())))
            .andExpect(jsonPath("$.[*].info").value(hasItem(DEFAULT_INFO.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].submissionDate").value(hasItem(sameInstant(DEFAULT_SUBMISSION_DATE))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SiteChatgroupProfile.class);
        SiteChatgroupProfile siteChatgroupProfile1 = new SiteChatgroupProfile();
        siteChatgroupProfile1.setId(1L);
        SiteChatgroupProfile siteChatgroupProfile2 = new SiteChatgroupProfile();
        siteChatgroupProfile2.setId(siteChatgroupProfile1.getId());
        assertThat(siteChatgroupProfile1).isEqualTo(siteChatgroupProfile2);
        siteChatgroupProfile2.setId(2L);
        assertThat(siteChatgroupProfile1).isNotEqualTo(siteChatgroupProfile2);
        siteChatgroupProfile1.setId(null);
        assertThat(siteChatgroupProfile1).isNotEqualTo(siteChatgroupProfile2);
    }
}
