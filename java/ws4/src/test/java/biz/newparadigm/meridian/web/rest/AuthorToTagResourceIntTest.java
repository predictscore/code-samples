package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.AuthorToTag;
import biz.newparadigm.meridian.domain.Author;
import biz.newparadigm.meridian.domain.Tag;
import biz.newparadigm.meridian.repository.AuthorToTagRepository;
import biz.newparadigm.meridian.service.AuthorToTagService;
import biz.newparadigm.meridian.repository.search.AuthorToTagSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.AuthorToTagCriteria;
import biz.newparadigm.meridian.service.AuthorToTagQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AuthorToTagResource REST controller.
 *
 * @see AuthorToTagResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class AuthorToTagResourceIntTest {

    @Autowired
    private AuthorToTagRepository authorToTagRepository;

    @Autowired
    private AuthorToTagService authorToTagService;

    @Autowired
    private AuthorToTagSearchRepository authorToTagSearchRepository;

    @Autowired
    private AuthorToTagQueryService authorToTagQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAuthorToTagMockMvc;

    private AuthorToTag authorToTag;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AuthorToTagResource authorToTagResource = new AuthorToTagResource(authorToTagService, authorToTagQueryService);
        this.restAuthorToTagMockMvc = MockMvcBuilders.standaloneSetup(authorToTagResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AuthorToTag createEntity(EntityManager em) {
        AuthorToTag authorToTag = new AuthorToTag();
        // Add required entity
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        authorToTag.setAuthor(author);
        // Add required entity
        Tag tag = TagResourceIntTest.createEntity(em);
        em.persist(tag);
        em.flush();
        authorToTag.setTag(tag);
        return authorToTag;
    }

    @Before
    public void initTest() {
        authorToTagSearchRepository.deleteAll();
        authorToTag = createEntity(em);
    }

    @Test
    @Transactional
    public void createAuthorToTag() throws Exception {
        int databaseSizeBeforeCreate = authorToTagRepository.findAll().size();

        // Create the AuthorToTag
        restAuthorToTagMockMvc.perform(post("/api/author-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorToTag)))
            .andExpect(status().isCreated());

        // Validate the AuthorToTag in the database
        List<AuthorToTag> authorToTagList = authorToTagRepository.findAll();
        assertThat(authorToTagList).hasSize(databaseSizeBeforeCreate + 1);
        AuthorToTag testAuthorToTag = authorToTagList.get(authorToTagList.size() - 1);

        // Validate the AuthorToTag in Elasticsearch
        AuthorToTag authorToTagEs = authorToTagSearchRepository.findOne(testAuthorToTag.getId());
        assertThat(authorToTagEs).isEqualToComparingFieldByField(testAuthorToTag);
    }

    @Test
    @Transactional
    public void createAuthorToTagWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = authorToTagRepository.findAll().size();

        // Create the AuthorToTag with an existing ID
        authorToTag.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAuthorToTagMockMvc.perform(post("/api/author-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorToTag)))
            .andExpect(status().isBadRequest());

        // Validate the AuthorToTag in the database
        List<AuthorToTag> authorToTagList = authorToTagRepository.findAll();
        assertThat(authorToTagList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAuthorToTags() throws Exception {
        // Initialize the database
        authorToTagRepository.saveAndFlush(authorToTag);

        // Get all the authorToTagList
        restAuthorToTagMockMvc.perform(get("/api/author-to-tags?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorToTag.getId().intValue())));
    }

    @Test
    @Transactional
    public void getAuthorToTag() throws Exception {
        // Initialize the database
        authorToTagRepository.saveAndFlush(authorToTag);

        // Get the authorToTag
        restAuthorToTagMockMvc.perform(get("/api/author-to-tags/{id}", authorToTag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(authorToTag.getId().intValue()));
    }

    @Test
    @Transactional
    public void getAllAuthorToTagsByAuthorIsEqualToSomething() throws Exception {
        // Initialize the database
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        authorToTag.setAuthor(author);
        authorToTagRepository.saveAndFlush(authorToTag);
        Long authorId = author.getId();

        // Get all the authorToTagList where author equals to authorId
        defaultAuthorToTagShouldBeFound("authorId.equals=" + authorId);

        // Get all the authorToTagList where author equals to authorId + 1
        defaultAuthorToTagShouldNotBeFound("authorId.equals=" + (authorId + 1));
    }


    @Test
    @Transactional
    public void getAllAuthorToTagsByTagIsEqualToSomething() throws Exception {
        // Initialize the database
        Tag tag = TagResourceIntTest.createEntity(em);
        em.persist(tag);
        em.flush();
        authorToTag.setTag(tag);
        authorToTagRepository.saveAndFlush(authorToTag);
        Long tagId = tag.getId();

        // Get all the authorToTagList where tag equals to tagId
        defaultAuthorToTagShouldBeFound("tagId.equals=" + tagId);

        // Get all the authorToTagList where tag equals to tagId + 1
        defaultAuthorToTagShouldNotBeFound("tagId.equals=" + (tagId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultAuthorToTagShouldBeFound(String filter) throws Exception {
        restAuthorToTagMockMvc.perform(get("/api/author-to-tags?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorToTag.getId().intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultAuthorToTagShouldNotBeFound(String filter) throws Exception {
        restAuthorToTagMockMvc.perform(get("/api/author-to-tags?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingAuthorToTag() throws Exception {
        // Get the authorToTag
        restAuthorToTagMockMvc.perform(get("/api/author-to-tags/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAuthorToTag() throws Exception {
        // Initialize the database
        authorToTagService.save(authorToTag);

        int databaseSizeBeforeUpdate = authorToTagRepository.findAll().size();

        // Update the authorToTag
        AuthorToTag updatedAuthorToTag = authorToTagRepository.findOne(authorToTag.getId());

        restAuthorToTagMockMvc.perform(put("/api/author-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAuthorToTag)))
            .andExpect(status().isOk());

        // Validate the AuthorToTag in the database
        List<AuthorToTag> authorToTagList = authorToTagRepository.findAll();
        assertThat(authorToTagList).hasSize(databaseSizeBeforeUpdate);
        AuthorToTag testAuthorToTag = authorToTagList.get(authorToTagList.size() - 1);

        // Validate the AuthorToTag in Elasticsearch
        AuthorToTag authorToTagEs = authorToTagSearchRepository.findOne(testAuthorToTag.getId());
        assertThat(authorToTagEs).isEqualToComparingFieldByField(testAuthorToTag);
    }

    @Test
    @Transactional
    public void updateNonExistingAuthorToTag() throws Exception {
        int databaseSizeBeforeUpdate = authorToTagRepository.findAll().size();

        // Create the AuthorToTag

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAuthorToTagMockMvc.perform(put("/api/author-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorToTag)))
            .andExpect(status().isCreated());

        // Validate the AuthorToTag in the database
        List<AuthorToTag> authorToTagList = authorToTagRepository.findAll();
        assertThat(authorToTagList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAuthorToTag() throws Exception {
        // Initialize the database
        authorToTagService.save(authorToTag);

        int databaseSizeBeforeDelete = authorToTagRepository.findAll().size();

        // Get the authorToTag
        restAuthorToTagMockMvc.perform(delete("/api/author-to-tags/{id}", authorToTag.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean authorToTagExistsInEs = authorToTagSearchRepository.exists(authorToTag.getId());
        assertThat(authorToTagExistsInEs).isFalse();

        // Validate the database is empty
        List<AuthorToTag> authorToTagList = authorToTagRepository.findAll();
        assertThat(authorToTagList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAuthorToTag() throws Exception {
        // Initialize the database
        authorToTagService.save(authorToTag);

        // Search the authorToTag
        restAuthorToTagMockMvc.perform(get("/api/_search/author-to-tags?query=id:" + authorToTag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorToTag.getId().intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AuthorToTag.class);
        AuthorToTag authorToTag1 = new AuthorToTag();
        authorToTag1.setId(1L);
        AuthorToTag authorToTag2 = new AuthorToTag();
        authorToTag2.setId(authorToTag1.getId());
        assertThat(authorToTag1).isEqualTo(authorToTag2);
        authorToTag2.setId(2L);
        assertThat(authorToTag1).isNotEqualTo(authorToTag2);
        authorToTag1.setId(null);
        assertThat(authorToTag1).isNotEqualTo(authorToTag2);
    }
}
