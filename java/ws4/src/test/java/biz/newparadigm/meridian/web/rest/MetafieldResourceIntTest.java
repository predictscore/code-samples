package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.Metafield;
import biz.newparadigm.meridian.domain.Metagroup;
import biz.newparadigm.meridian.repository.MetafieldRepository;
import biz.newparadigm.meridian.service.MetafieldService;
import biz.newparadigm.meridian.repository.search.MetafieldSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.MetafieldCriteria;
import biz.newparadigm.meridian.service.MetafieldQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MetafieldResource REST controller.
 *
 * @see MetafieldResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class MetafieldResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private MetafieldRepository metafieldRepository;

    @Autowired
    private MetafieldService metafieldService;

    @Autowired
    private MetafieldSearchRepository metafieldSearchRepository;

    @Autowired
    private MetafieldQueryService metafieldQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMetafieldMockMvc;

    private Metafield metafield;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MetafieldResource metafieldResource = new MetafieldResource(metafieldService, metafieldQueryService);
        this.restMetafieldMockMvc = MockMvcBuilders.standaloneSetup(metafieldResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Metafield createEntity(EntityManager em) {
        Metafield metafield = new Metafield()
            .name(DEFAULT_NAME);
        // Add required entity
        Metagroup metagroup = MetagroupResourceIntTest.createEntity(em);
        em.persist(metagroup);
        em.flush();
        metafield.setMetagroup(metagroup);
        return metafield;
    }

    @Before
    public void initTest() {
        metafieldSearchRepository.deleteAll();
        metafield = createEntity(em);
    }

    @Test
    @Transactional
    public void createMetafield() throws Exception {
        int databaseSizeBeforeCreate = metafieldRepository.findAll().size();

        // Create the Metafield
        restMetafieldMockMvc.perform(post("/api/metafields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(metafield)))
            .andExpect(status().isCreated());

        // Validate the Metafield in the database
        List<Metafield> metafieldList = metafieldRepository.findAll();
        assertThat(metafieldList).hasSize(databaseSizeBeforeCreate + 1);
        Metafield testMetafield = metafieldList.get(metafieldList.size() - 1);
        assertThat(testMetafield.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the Metafield in Elasticsearch
        Metafield metafieldEs = metafieldSearchRepository.findOne(testMetafield.getId());
        assertThat(metafieldEs).isEqualToComparingFieldByField(testMetafield);
    }

    @Test
    @Transactional
    public void createMetafieldWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = metafieldRepository.findAll().size();

        // Create the Metafield with an existing ID
        metafield.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMetafieldMockMvc.perform(post("/api/metafields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(metafield)))
            .andExpect(status().isBadRequest());

        // Validate the Metafield in the database
        List<Metafield> metafieldList = metafieldRepository.findAll();
        assertThat(metafieldList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = metafieldRepository.findAll().size();
        // set the field null
        metafield.setName(null);

        // Create the Metafield, which fails.

        restMetafieldMockMvc.perform(post("/api/metafields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(metafield)))
            .andExpect(status().isBadRequest());

        List<Metafield> metafieldList = metafieldRepository.findAll();
        assertThat(metafieldList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMetafields() throws Exception {
        // Initialize the database
        metafieldRepository.saveAndFlush(metafield);

        // Get all the metafieldList
        restMetafieldMockMvc.perform(get("/api/metafields?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(metafield.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getMetafield() throws Exception {
        // Initialize the database
        metafieldRepository.saveAndFlush(metafield);

        // Get the metafield
        restMetafieldMockMvc.perform(get("/api/metafields/{id}", metafield.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(metafield.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllMetafieldsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        metafieldRepository.saveAndFlush(metafield);

        // Get all the metafieldList where name equals to DEFAULT_NAME
        defaultMetafieldShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the metafieldList where name equals to UPDATED_NAME
        defaultMetafieldShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllMetafieldsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        metafieldRepository.saveAndFlush(metafield);

        // Get all the metafieldList where name in DEFAULT_NAME or UPDATED_NAME
        defaultMetafieldShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the metafieldList where name equals to UPDATED_NAME
        defaultMetafieldShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllMetafieldsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        metafieldRepository.saveAndFlush(metafield);

        // Get all the metafieldList where name is not null
        defaultMetafieldShouldBeFound("name.specified=true");

        // Get all the metafieldList where name is null
        defaultMetafieldShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllMetafieldsByMetagroupIsEqualToSomething() throws Exception {
        // Initialize the database
        Metagroup metagroup = MetagroupResourceIntTest.createEntity(em);
        em.persist(metagroup);
        em.flush();
        metafield.setMetagroup(metagroup);
        metafieldRepository.saveAndFlush(metafield);
        Long metagroupId = metagroup.getId();

        // Get all the metafieldList where metagroup equals to metagroupId
        defaultMetafieldShouldBeFound("metagroupId.equals=" + metagroupId);

        // Get all the metafieldList where metagroup equals to metagroupId + 1
        defaultMetafieldShouldNotBeFound("metagroupId.equals=" + (metagroupId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultMetafieldShouldBeFound(String filter) throws Exception {
        restMetafieldMockMvc.perform(get("/api/metafields?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(metafield.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultMetafieldShouldNotBeFound(String filter) throws Exception {
        restMetafieldMockMvc.perform(get("/api/metafields?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingMetafield() throws Exception {
        // Get the metafield
        restMetafieldMockMvc.perform(get("/api/metafields/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMetafield() throws Exception {
        // Initialize the database
        metafieldService.save(metafield);

        int databaseSizeBeforeUpdate = metafieldRepository.findAll().size();

        // Update the metafield
        Metafield updatedMetafield = metafieldRepository.findOne(metafield.getId());
        updatedMetafield
            .name(UPDATED_NAME);

        restMetafieldMockMvc.perform(put("/api/metafields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMetafield)))
            .andExpect(status().isOk());

        // Validate the Metafield in the database
        List<Metafield> metafieldList = metafieldRepository.findAll();
        assertThat(metafieldList).hasSize(databaseSizeBeforeUpdate);
        Metafield testMetafield = metafieldList.get(metafieldList.size() - 1);
        assertThat(testMetafield.getName()).isEqualTo(UPDATED_NAME);

        // Validate the Metafield in Elasticsearch
        Metafield metafieldEs = metafieldSearchRepository.findOne(testMetafield.getId());
        assertThat(metafieldEs).isEqualToComparingFieldByField(testMetafield);
    }

    @Test
    @Transactional
    public void updateNonExistingMetafield() throws Exception {
        int databaseSizeBeforeUpdate = metafieldRepository.findAll().size();

        // Create the Metafield

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMetafieldMockMvc.perform(put("/api/metafields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(metafield)))
            .andExpect(status().isCreated());

        // Validate the Metafield in the database
        List<Metafield> metafieldList = metafieldRepository.findAll();
        assertThat(metafieldList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMetafield() throws Exception {
        // Initialize the database
        metafieldService.save(metafield);

        int databaseSizeBeforeDelete = metafieldRepository.findAll().size();

        // Get the metafield
        restMetafieldMockMvc.perform(delete("/api/metafields/{id}", metafield.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean metafieldExistsInEs = metafieldSearchRepository.exists(metafield.getId());
        assertThat(metafieldExistsInEs).isFalse();

        // Validate the database is empty
        List<Metafield> metafieldList = metafieldRepository.findAll();
        assertThat(metafieldList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMetafield() throws Exception {
        // Initialize the database
        metafieldService.save(metafield);

        // Search the metafield
        restMetafieldMockMvc.perform(get("/api/_search/metafields?query=id:" + metafield.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(metafield.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Metafield.class);
        Metafield metafield1 = new Metafield();
        metafield1.setId(1L);
        Metafield metafield2 = new Metafield();
        metafield2.setId(metafield1.getId());
        assertThat(metafield1).isEqualTo(metafield2);
        metafield2.setId(2L);
        assertThat(metafield1).isNotEqualTo(metafield2);
        metafield1.setId(null);
        assertThat(metafield1).isNotEqualTo(metafield2);
    }
}
