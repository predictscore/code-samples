package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.AuthorToLanguage;
import biz.newparadigm.meridian.domain.Author;
import biz.newparadigm.meridian.domain.Language;
import biz.newparadigm.meridian.repository.AuthorToLanguageRepository;
import biz.newparadigm.meridian.service.AuthorToLanguageService;
import biz.newparadigm.meridian.repository.search.AuthorToLanguageSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.AuthorToLanguageCriteria;
import biz.newparadigm.meridian.service.AuthorToLanguageQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AuthorToLanguageResource REST controller.
 *
 * @see AuthorToLanguageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class AuthorToLanguageResourceIntTest {

    @Autowired
    private AuthorToLanguageRepository authorToLanguageRepository;

    @Autowired
    private AuthorToLanguageService authorToLanguageService;

    @Autowired
    private AuthorToLanguageSearchRepository authorToLanguageSearchRepository;

    @Autowired
    private AuthorToLanguageQueryService authorToLanguageQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAuthorToLanguageMockMvc;

    private AuthorToLanguage authorToLanguage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AuthorToLanguageResource authorToLanguageResource = new AuthorToLanguageResource(authorToLanguageService, authorToLanguageQueryService);
        this.restAuthorToLanguageMockMvc = MockMvcBuilders.standaloneSetup(authorToLanguageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AuthorToLanguage createEntity(EntityManager em) {
        AuthorToLanguage authorToLanguage = new AuthorToLanguage();
        // Add required entity
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        authorToLanguage.setAuthor(author);
        // Add required entity
        Language language = LanguageResourceIntTest.createEntity(em);
        em.persist(language);
        em.flush();
        authorToLanguage.setLanguage(language);
        return authorToLanguage;
    }

    @Before
    public void initTest() {
        authorToLanguageSearchRepository.deleteAll();
        authorToLanguage = createEntity(em);
    }

    @Test
    @Transactional
    public void createAuthorToLanguage() throws Exception {
        int databaseSizeBeforeCreate = authorToLanguageRepository.findAll().size();

        // Create the AuthorToLanguage
        restAuthorToLanguageMockMvc.perform(post("/api/author-to-languages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorToLanguage)))
            .andExpect(status().isCreated());

        // Validate the AuthorToLanguage in the database
        List<AuthorToLanguage> authorToLanguageList = authorToLanguageRepository.findAll();
        assertThat(authorToLanguageList).hasSize(databaseSizeBeforeCreate + 1);
        AuthorToLanguage testAuthorToLanguage = authorToLanguageList.get(authorToLanguageList.size() - 1);

        // Validate the AuthorToLanguage in Elasticsearch
        AuthorToLanguage authorToLanguageEs = authorToLanguageSearchRepository.findOne(testAuthorToLanguage.getId());
        assertThat(authorToLanguageEs).isEqualToComparingFieldByField(testAuthorToLanguage);
    }

    @Test
    @Transactional
    public void createAuthorToLanguageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = authorToLanguageRepository.findAll().size();

        // Create the AuthorToLanguage with an existing ID
        authorToLanguage.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAuthorToLanguageMockMvc.perform(post("/api/author-to-languages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorToLanguage)))
            .andExpect(status().isBadRequest());

        // Validate the AuthorToLanguage in the database
        List<AuthorToLanguage> authorToLanguageList = authorToLanguageRepository.findAll();
        assertThat(authorToLanguageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAuthorToLanguages() throws Exception {
        // Initialize the database
        authorToLanguageRepository.saveAndFlush(authorToLanguage);

        // Get all the authorToLanguageList
        restAuthorToLanguageMockMvc.perform(get("/api/author-to-languages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorToLanguage.getId().intValue())));
    }

    @Test
    @Transactional
    public void getAuthorToLanguage() throws Exception {
        // Initialize the database
        authorToLanguageRepository.saveAndFlush(authorToLanguage);

        // Get the authorToLanguage
        restAuthorToLanguageMockMvc.perform(get("/api/author-to-languages/{id}", authorToLanguage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(authorToLanguage.getId().intValue()));
    }

    @Test
    @Transactional
    public void getAllAuthorToLanguagesByAuthorIsEqualToSomething() throws Exception {
        // Initialize the database
        Author author = AuthorResourceIntTest.createEntity(em);
        em.persist(author);
        em.flush();
        authorToLanguage.setAuthor(author);
        authorToLanguageRepository.saveAndFlush(authorToLanguage);
        Long authorId = author.getId();

        // Get all the authorToLanguageList where author equals to authorId
        defaultAuthorToLanguageShouldBeFound("authorId.equals=" + authorId);

        // Get all the authorToLanguageList where author equals to authorId + 1
        defaultAuthorToLanguageShouldNotBeFound("authorId.equals=" + (authorId + 1));
    }


    @Test
    @Transactional
    public void getAllAuthorToLanguagesByLanguageIsEqualToSomething() throws Exception {
        // Initialize the database
        Language language = LanguageResourceIntTest.createEntity(em);
        em.persist(language);
        em.flush();
        authorToLanguage.setLanguage(language);
        authorToLanguageRepository.saveAndFlush(authorToLanguage);
        Long languageId = language.getId();

        // Get all the authorToLanguageList where language equals to languageId
        defaultAuthorToLanguageShouldBeFound("languageId.equals=" + languageId);

        // Get all the authorToLanguageList where language equals to languageId + 1
        defaultAuthorToLanguageShouldNotBeFound("languageId.equals=" + (languageId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultAuthorToLanguageShouldBeFound(String filter) throws Exception {
        restAuthorToLanguageMockMvc.perform(get("/api/author-to-languages?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorToLanguage.getId().intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultAuthorToLanguageShouldNotBeFound(String filter) throws Exception {
        restAuthorToLanguageMockMvc.perform(get("/api/author-to-languages?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingAuthorToLanguage() throws Exception {
        // Get the authorToLanguage
        restAuthorToLanguageMockMvc.perform(get("/api/author-to-languages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAuthorToLanguage() throws Exception {
        // Initialize the database
        authorToLanguageService.save(authorToLanguage);

        int databaseSizeBeforeUpdate = authorToLanguageRepository.findAll().size();

        // Update the authorToLanguage
        AuthorToLanguage updatedAuthorToLanguage = authorToLanguageRepository.findOne(authorToLanguage.getId());

        restAuthorToLanguageMockMvc.perform(put("/api/author-to-languages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAuthorToLanguage)))
            .andExpect(status().isOk());

        // Validate the AuthorToLanguage in the database
        List<AuthorToLanguage> authorToLanguageList = authorToLanguageRepository.findAll();
        assertThat(authorToLanguageList).hasSize(databaseSizeBeforeUpdate);
        AuthorToLanguage testAuthorToLanguage = authorToLanguageList.get(authorToLanguageList.size() - 1);

        // Validate the AuthorToLanguage in Elasticsearch
        AuthorToLanguage authorToLanguageEs = authorToLanguageSearchRepository.findOne(testAuthorToLanguage.getId());
        assertThat(authorToLanguageEs).isEqualToComparingFieldByField(testAuthorToLanguage);
    }

    @Test
    @Transactional
    public void updateNonExistingAuthorToLanguage() throws Exception {
        int databaseSizeBeforeUpdate = authorToLanguageRepository.findAll().size();

        // Create the AuthorToLanguage

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAuthorToLanguageMockMvc.perform(put("/api/author-to-languages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(authorToLanguage)))
            .andExpect(status().isCreated());

        // Validate the AuthorToLanguage in the database
        List<AuthorToLanguage> authorToLanguageList = authorToLanguageRepository.findAll();
        assertThat(authorToLanguageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAuthorToLanguage() throws Exception {
        // Initialize the database
        authorToLanguageService.save(authorToLanguage);

        int databaseSizeBeforeDelete = authorToLanguageRepository.findAll().size();

        // Get the authorToLanguage
        restAuthorToLanguageMockMvc.perform(delete("/api/author-to-languages/{id}", authorToLanguage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean authorToLanguageExistsInEs = authorToLanguageSearchRepository.exists(authorToLanguage.getId());
        assertThat(authorToLanguageExistsInEs).isFalse();

        // Validate the database is empty
        List<AuthorToLanguage> authorToLanguageList = authorToLanguageRepository.findAll();
        assertThat(authorToLanguageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAuthorToLanguage() throws Exception {
        // Initialize the database
        authorToLanguageService.save(authorToLanguage);

        // Search the authorToLanguage
        restAuthorToLanguageMockMvc.perform(get("/api/_search/author-to-languages?query=id:" + authorToLanguage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorToLanguage.getId().intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AuthorToLanguage.class);
        AuthorToLanguage authorToLanguage1 = new AuthorToLanguage();
        authorToLanguage1.setId(1L);
        AuthorToLanguage authorToLanguage2 = new AuthorToLanguage();
        authorToLanguage2.setId(authorToLanguage1.getId());
        assertThat(authorToLanguage1).isEqualTo(authorToLanguage2);
        authorToLanguage2.setId(2L);
        assertThat(authorToLanguage1).isNotEqualTo(authorToLanguage2);
        authorToLanguage1.setId(null);
        assertThat(authorToLanguage1).isNotEqualTo(authorToLanguage2);
    }
}
