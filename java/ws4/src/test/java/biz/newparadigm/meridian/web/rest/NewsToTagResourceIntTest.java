package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsToTag;
import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.domain.Tag;
import biz.newparadigm.meridian.repository.NewsToTagRepository;
import biz.newparadigm.meridian.service.NewsToTagService;
import biz.newparadigm.meridian.repository.search.NewsToTagSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsToTagCriteria;
import biz.newparadigm.meridian.service.NewsToTagQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsToTagResource REST controller.
 *
 * @see NewsToTagResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsToTagResourceIntTest {

    @Autowired
    private NewsToTagRepository newsToTagRepository;

    @Autowired
    private NewsToTagService newsToTagService;

    @Autowired
    private NewsToTagSearchRepository newsToTagSearchRepository;

    @Autowired
    private NewsToTagQueryService newsToTagQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsToTagMockMvc;

    private NewsToTag newsToTag;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsToTagResource newsToTagResource = new NewsToTagResource(newsToTagService, newsToTagQueryService);
        this.restNewsToTagMockMvc = MockMvcBuilders.standaloneSetup(newsToTagResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsToTag createEntity(EntityManager em) {
        NewsToTag newsToTag = new NewsToTag();
        // Add required entity
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsToTag.setNews(news);
        // Add required entity
        Tag tag = TagResourceIntTest.createEntity(em);
        em.persist(tag);
        em.flush();
        newsToTag.setTag(tag);
        return newsToTag;
    }

    @Before
    public void initTest() {
        newsToTagSearchRepository.deleteAll();
        newsToTag = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsToTag() throws Exception {
        int databaseSizeBeforeCreate = newsToTagRepository.findAll().size();

        // Create the NewsToTag
        restNewsToTagMockMvc.perform(post("/api/news-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsToTag)))
            .andExpect(status().isCreated());

        // Validate the NewsToTag in the database
        List<NewsToTag> newsToTagList = newsToTagRepository.findAll();
        assertThat(newsToTagList).hasSize(databaseSizeBeforeCreate + 1);
        NewsToTag testNewsToTag = newsToTagList.get(newsToTagList.size() - 1);

        // Validate the NewsToTag in Elasticsearch
        NewsToTag newsToTagEs = newsToTagSearchRepository.findOne(testNewsToTag.getId());
        assertThat(newsToTagEs).isEqualToComparingFieldByField(testNewsToTag);
    }

    @Test
    @Transactional
    public void createNewsToTagWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsToTagRepository.findAll().size();

        // Create the NewsToTag with an existing ID
        newsToTag.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsToTagMockMvc.perform(post("/api/news-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsToTag)))
            .andExpect(status().isBadRequest());

        // Validate the NewsToTag in the database
        List<NewsToTag> newsToTagList = newsToTagRepository.findAll();
        assertThat(newsToTagList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllNewsToTags() throws Exception {
        // Initialize the database
        newsToTagRepository.saveAndFlush(newsToTag);

        // Get all the newsToTagList
        restNewsToTagMockMvc.perform(get("/api/news-to-tags?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsToTag.getId().intValue())));
    }

    @Test
    @Transactional
    public void getNewsToTag() throws Exception {
        // Initialize the database
        newsToTagRepository.saveAndFlush(newsToTag);

        // Get the newsToTag
        restNewsToTagMockMvc.perform(get("/api/news-to-tags/{id}", newsToTag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsToTag.getId().intValue()));
    }

    @Test
    @Transactional
    public void getAllNewsToTagsByNewsIsEqualToSomething() throws Exception {
        // Initialize the database
        News news = NewsResourceIntTest.createEntity(em);
        em.persist(news);
        em.flush();
        newsToTag.setNews(news);
        newsToTagRepository.saveAndFlush(newsToTag);
        Long newsId = news.getId();

        // Get all the newsToTagList where news equals to newsId
        defaultNewsToTagShouldBeFound("newsId.equals=" + newsId);

        // Get all the newsToTagList where news equals to newsId + 1
        defaultNewsToTagShouldNotBeFound("newsId.equals=" + (newsId + 1));
    }


    @Test
    @Transactional
    public void getAllNewsToTagsByTagIsEqualToSomething() throws Exception {
        // Initialize the database
        Tag tag = TagResourceIntTest.createEntity(em);
        em.persist(tag);
        em.flush();
        newsToTag.setTag(tag);
        newsToTagRepository.saveAndFlush(newsToTag);
        Long tagId = tag.getId();

        // Get all the newsToTagList where tag equals to tagId
        defaultNewsToTagShouldBeFound("tagId.equals=" + tagId);

        // Get all the newsToTagList where tag equals to tagId + 1
        defaultNewsToTagShouldNotBeFound("tagId.equals=" + (tagId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsToTagShouldBeFound(String filter) throws Exception {
        restNewsToTagMockMvc.perform(get("/api/news-to-tags?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsToTag.getId().intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsToTagShouldNotBeFound(String filter) throws Exception {
        restNewsToTagMockMvc.perform(get("/api/news-to-tags?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsToTag() throws Exception {
        // Get the newsToTag
        restNewsToTagMockMvc.perform(get("/api/news-to-tags/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsToTag() throws Exception {
        // Initialize the database
        newsToTagService.save(newsToTag);

        int databaseSizeBeforeUpdate = newsToTagRepository.findAll().size();

        // Update the newsToTag
        NewsToTag updatedNewsToTag = newsToTagRepository.findOne(newsToTag.getId());

        restNewsToTagMockMvc.perform(put("/api/news-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsToTag)))
            .andExpect(status().isOk());

        // Validate the NewsToTag in the database
        List<NewsToTag> newsToTagList = newsToTagRepository.findAll();
        assertThat(newsToTagList).hasSize(databaseSizeBeforeUpdate);
        NewsToTag testNewsToTag = newsToTagList.get(newsToTagList.size() - 1);

        // Validate the NewsToTag in Elasticsearch
        NewsToTag newsToTagEs = newsToTagSearchRepository.findOne(testNewsToTag.getId());
        assertThat(newsToTagEs).isEqualToComparingFieldByField(testNewsToTag);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsToTag() throws Exception {
        int databaseSizeBeforeUpdate = newsToTagRepository.findAll().size();

        // Create the NewsToTag

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsToTagMockMvc.perform(put("/api/news-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsToTag)))
            .andExpect(status().isCreated());

        // Validate the NewsToTag in the database
        List<NewsToTag> newsToTagList = newsToTagRepository.findAll();
        assertThat(newsToTagList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsToTag() throws Exception {
        // Initialize the database
        newsToTagService.save(newsToTag);

        int databaseSizeBeforeDelete = newsToTagRepository.findAll().size();

        // Get the newsToTag
        restNewsToTagMockMvc.perform(delete("/api/news-to-tags/{id}", newsToTag.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsToTagExistsInEs = newsToTagSearchRepository.exists(newsToTag.getId());
        assertThat(newsToTagExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsToTag> newsToTagList = newsToTagRepository.findAll();
        assertThat(newsToTagList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsToTag() throws Exception {
        // Initialize the database
        newsToTagService.save(newsToTag);

        // Search the newsToTag
        restNewsToTagMockMvc.perform(get("/api/_search/news-to-tags?query=id:" + newsToTag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsToTag.getId().intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsToTag.class);
        NewsToTag newsToTag1 = new NewsToTag();
        newsToTag1.setId(1L);
        NewsToTag newsToTag2 = new NewsToTag();
        newsToTag2.setId(newsToTag1.getId());
        assertThat(newsToTag1).isEqualTo(newsToTag2);
        newsToTag2.setId(2L);
        assertThat(newsToTag1).isNotEqualTo(newsToTag2);
        newsToTag1.setId(null);
        assertThat(newsToTag1).isNotEqualTo(newsToTag2);
    }
}
