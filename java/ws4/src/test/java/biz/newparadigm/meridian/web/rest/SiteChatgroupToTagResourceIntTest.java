package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.SiteChatgroupToTag;
import biz.newparadigm.meridian.domain.SiteChatgroup;
import biz.newparadigm.meridian.domain.Tag;
import biz.newparadigm.meridian.repository.SiteChatgroupToTagRepository;
import biz.newparadigm.meridian.service.SiteChatgroupToTagService;
import biz.newparadigm.meridian.repository.search.SiteChatgroupToTagSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.SiteChatgroupToTagCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupToTagQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SiteChatgroupToTagResource REST controller.
 *
 * @see SiteChatgroupToTagResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class SiteChatgroupToTagResourceIntTest {

    @Autowired
    private SiteChatgroupToTagRepository siteChatgroupToTagRepository;

    @Autowired
    private SiteChatgroupToTagService siteChatgroupToTagService;

    @Autowired
    private SiteChatgroupToTagSearchRepository siteChatgroupToTagSearchRepository;

    @Autowired
    private SiteChatgroupToTagQueryService siteChatgroupToTagQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSiteChatgroupToTagMockMvc;

    private SiteChatgroupToTag siteChatgroupToTag;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SiteChatgroupToTagResource siteChatgroupToTagResource = new SiteChatgroupToTagResource(siteChatgroupToTagService, siteChatgroupToTagQueryService);
        this.restSiteChatgroupToTagMockMvc = MockMvcBuilders.standaloneSetup(siteChatgroupToTagResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SiteChatgroupToTag createEntity(EntityManager em) {
        SiteChatgroupToTag siteChatgroupToTag = new SiteChatgroupToTag();
        // Add required entity
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupToTag.setSiteChatgroup(siteChatgroup);
        // Add required entity
        Tag tag = TagResourceIntTest.createEntity(em);
        em.persist(tag);
        em.flush();
        siteChatgroupToTag.setTag(tag);
        return siteChatgroupToTag;
    }

    @Before
    public void initTest() {
        siteChatgroupToTagSearchRepository.deleteAll();
        siteChatgroupToTag = createEntity(em);
    }

    @Test
    @Transactional
    public void createSiteChatgroupToTag() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupToTagRepository.findAll().size();

        // Create the SiteChatgroupToTag
        restSiteChatgroupToTagMockMvc.perform(post("/api/site-chatgroup-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupToTag)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupToTag in the database
        List<SiteChatgroupToTag> siteChatgroupToTagList = siteChatgroupToTagRepository.findAll();
        assertThat(siteChatgroupToTagList).hasSize(databaseSizeBeforeCreate + 1);
        SiteChatgroupToTag testSiteChatgroupToTag = siteChatgroupToTagList.get(siteChatgroupToTagList.size() - 1);

        // Validate the SiteChatgroupToTag in Elasticsearch
        SiteChatgroupToTag siteChatgroupToTagEs = siteChatgroupToTagSearchRepository.findOne(testSiteChatgroupToTag.getId());
        assertThat(siteChatgroupToTagEs).isEqualToComparingFieldByField(testSiteChatgroupToTag);
    }

    @Test
    @Transactional
    public void createSiteChatgroupToTagWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = siteChatgroupToTagRepository.findAll().size();

        // Create the SiteChatgroupToTag with an existing ID
        siteChatgroupToTag.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSiteChatgroupToTagMockMvc.perform(post("/api/site-chatgroup-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupToTag)))
            .andExpect(status().isBadRequest());

        // Validate the SiteChatgroupToTag in the database
        List<SiteChatgroupToTag> siteChatgroupToTagList = siteChatgroupToTagRepository.findAll();
        assertThat(siteChatgroupToTagList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupToTags() throws Exception {
        // Initialize the database
        siteChatgroupToTagRepository.saveAndFlush(siteChatgroupToTag);

        // Get all the siteChatgroupToTagList
        restSiteChatgroupToTagMockMvc.perform(get("/api/site-chatgroup-to-tags?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupToTag.getId().intValue())));
    }

    @Test
    @Transactional
    public void getSiteChatgroupToTag() throws Exception {
        // Initialize the database
        siteChatgroupToTagRepository.saveAndFlush(siteChatgroupToTag);

        // Get the siteChatgroupToTag
        restSiteChatgroupToTagMockMvc.perform(get("/api/site-chatgroup-to-tags/{id}", siteChatgroupToTag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(siteChatgroupToTag.getId().intValue()));
    }

    @Test
    @Transactional
    public void getAllSiteChatgroupToTagsBySiteChatgroupIsEqualToSomething() throws Exception {
        // Initialize the database
        SiteChatgroup siteChatgroup = SiteChatgroupResourceIntTest.createEntity(em);
        em.persist(siteChatgroup);
        em.flush();
        siteChatgroupToTag.setSiteChatgroup(siteChatgroup);
        siteChatgroupToTagRepository.saveAndFlush(siteChatgroupToTag);
        Long siteChatgroupId = siteChatgroup.getId();

        // Get all the siteChatgroupToTagList where siteChatgroup equals to siteChatgroupId
        defaultSiteChatgroupToTagShouldBeFound("siteChatgroupId.equals=" + siteChatgroupId);

        // Get all the siteChatgroupToTagList where siteChatgroup equals to siteChatgroupId + 1
        defaultSiteChatgroupToTagShouldNotBeFound("siteChatgroupId.equals=" + (siteChatgroupId + 1));
    }


    @Test
    @Transactional
    public void getAllSiteChatgroupToTagsByTagIsEqualToSomething() throws Exception {
        // Initialize the database
        Tag tag = TagResourceIntTest.createEntity(em);
        em.persist(tag);
        em.flush();
        siteChatgroupToTag.setTag(tag);
        siteChatgroupToTagRepository.saveAndFlush(siteChatgroupToTag);
        Long tagId = tag.getId();

        // Get all the siteChatgroupToTagList where tag equals to tagId
        defaultSiteChatgroupToTagShouldBeFound("tagId.equals=" + tagId);

        // Get all the siteChatgroupToTagList where tag equals to tagId + 1
        defaultSiteChatgroupToTagShouldNotBeFound("tagId.equals=" + (tagId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultSiteChatgroupToTagShouldBeFound(String filter) throws Exception {
        restSiteChatgroupToTagMockMvc.perform(get("/api/site-chatgroup-to-tags?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupToTag.getId().intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultSiteChatgroupToTagShouldNotBeFound(String filter) throws Exception {
        restSiteChatgroupToTagMockMvc.perform(get("/api/site-chatgroup-to-tags?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingSiteChatgroupToTag() throws Exception {
        // Get the siteChatgroupToTag
        restSiteChatgroupToTagMockMvc.perform(get("/api/site-chatgroup-to-tags/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSiteChatgroupToTag() throws Exception {
        // Initialize the database
        siteChatgroupToTagService.save(siteChatgroupToTag);

        int databaseSizeBeforeUpdate = siteChatgroupToTagRepository.findAll().size();

        // Update the siteChatgroupToTag
        SiteChatgroupToTag updatedSiteChatgroupToTag = siteChatgroupToTagRepository.findOne(siteChatgroupToTag.getId());

        restSiteChatgroupToTagMockMvc.perform(put("/api/site-chatgroup-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSiteChatgroupToTag)))
            .andExpect(status().isOk());

        // Validate the SiteChatgroupToTag in the database
        List<SiteChatgroupToTag> siteChatgroupToTagList = siteChatgroupToTagRepository.findAll();
        assertThat(siteChatgroupToTagList).hasSize(databaseSizeBeforeUpdate);
        SiteChatgroupToTag testSiteChatgroupToTag = siteChatgroupToTagList.get(siteChatgroupToTagList.size() - 1);

        // Validate the SiteChatgroupToTag in Elasticsearch
        SiteChatgroupToTag siteChatgroupToTagEs = siteChatgroupToTagSearchRepository.findOne(testSiteChatgroupToTag.getId());
        assertThat(siteChatgroupToTagEs).isEqualToComparingFieldByField(testSiteChatgroupToTag);
    }

    @Test
    @Transactional
    public void updateNonExistingSiteChatgroupToTag() throws Exception {
        int databaseSizeBeforeUpdate = siteChatgroupToTagRepository.findAll().size();

        // Create the SiteChatgroupToTag

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSiteChatgroupToTagMockMvc.perform(put("/api/site-chatgroup-to-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteChatgroupToTag)))
            .andExpect(status().isCreated());

        // Validate the SiteChatgroupToTag in the database
        List<SiteChatgroupToTag> siteChatgroupToTagList = siteChatgroupToTagRepository.findAll();
        assertThat(siteChatgroupToTagList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSiteChatgroupToTag() throws Exception {
        // Initialize the database
        siteChatgroupToTagService.save(siteChatgroupToTag);

        int databaseSizeBeforeDelete = siteChatgroupToTagRepository.findAll().size();

        // Get the siteChatgroupToTag
        restSiteChatgroupToTagMockMvc.perform(delete("/api/site-chatgroup-to-tags/{id}", siteChatgroupToTag.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean siteChatgroupToTagExistsInEs = siteChatgroupToTagSearchRepository.exists(siteChatgroupToTag.getId());
        assertThat(siteChatgroupToTagExistsInEs).isFalse();

        // Validate the database is empty
        List<SiteChatgroupToTag> siteChatgroupToTagList = siteChatgroupToTagRepository.findAll();
        assertThat(siteChatgroupToTagList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchSiteChatgroupToTag() throws Exception {
        // Initialize the database
        siteChatgroupToTagService.save(siteChatgroupToTag);

        // Search the siteChatgroupToTag
        restSiteChatgroupToTagMockMvc.perform(get("/api/_search/site-chatgroup-to-tags?query=id:" + siteChatgroupToTag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteChatgroupToTag.getId().intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SiteChatgroupToTag.class);
        SiteChatgroupToTag siteChatgroupToTag1 = new SiteChatgroupToTag();
        siteChatgroupToTag1.setId(1L);
        SiteChatgroupToTag siteChatgroupToTag2 = new SiteChatgroupToTag();
        siteChatgroupToTag2.setId(siteChatgroupToTag1.getId());
        assertThat(siteChatgroupToTag1).isEqualTo(siteChatgroupToTag2);
        siteChatgroupToTag2.setId(2L);
        assertThat(siteChatgroupToTag1).isNotEqualTo(siteChatgroupToTag2);
        siteChatgroupToTag1.setId(null);
        assertThat(siteChatgroupToTag1).isNotEqualTo(siteChatgroupToTag2);
    }
}
