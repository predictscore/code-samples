package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.NewmeridianApp;

import biz.newparadigm.meridian.domain.NewsSummaryPublicImage;
import biz.newparadigm.meridian.domain.NewsSummary;
import biz.newparadigm.meridian.repository.NewsSummaryPublicImageRepository;
import biz.newparadigm.meridian.service.NewsSummaryPublicImageService;
import biz.newparadigm.meridian.repository.search.NewsSummaryPublicImageSearchRepository;
import biz.newparadigm.meridian.web.rest.errors.ExceptionTranslator;
import biz.newparadigm.meridian.service.dto.NewsSummaryPublicImageCriteria;
import biz.newparadigm.meridian.service.NewsSummaryPublicImageQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static biz.newparadigm.meridian.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsSummaryPublicImageResource REST controller.
 *
 * @see NewsSummaryPublicImageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NewmeridianApp.class)
public class NewsSummaryPublicImageResourceIntTest {

    private static final String DEFAULT_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_FILENAME = "BBBBBBBBBB";

    private static final byte[] DEFAULT_BLOB = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_BLOB = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_BLOB_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_BLOB_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_TRANSLATION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_TRANSLATION = "BBBBBBBBBB";

    private static final Integer DEFAULT_IMAGE_ORDER = 1;
    private static final Integer UPDATED_IMAGE_ORDER = 2;

    @Autowired
    private NewsSummaryPublicImageRepository newsSummaryPublicImageRepository;

    @Autowired
    private NewsSummaryPublicImageService newsSummaryPublicImageService;

    @Autowired
    private NewsSummaryPublicImageSearchRepository newsSummaryPublicImageSearchRepository;

    @Autowired
    private NewsSummaryPublicImageQueryService newsSummaryPublicImageQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsSummaryPublicImageMockMvc;

    private NewsSummaryPublicImage newsSummaryPublicImage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NewsSummaryPublicImageResource newsSummaryPublicImageResource = new NewsSummaryPublicImageResource(newsSummaryPublicImageService, newsSummaryPublicImageQueryService);
        this.restNewsSummaryPublicImageMockMvc = MockMvcBuilders.standaloneSetup(newsSummaryPublicImageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsSummaryPublicImage createEntity(EntityManager em) {
        NewsSummaryPublicImage newsSummaryPublicImage = new NewsSummaryPublicImage()
            .filename(DEFAULT_FILENAME)
            .blob(DEFAULT_BLOB)
            .blobContentType(DEFAULT_BLOB_CONTENT_TYPE)
            .description(DEFAULT_DESCRIPTION)
            .descriptionTranslation(DEFAULT_DESCRIPTION_TRANSLATION)
            .imageOrder(DEFAULT_IMAGE_ORDER);
        // Add required entity
        NewsSummary newsSummary = NewsSummaryResourceIntTest.createEntity(em);
        em.persist(newsSummary);
        em.flush();
        newsSummaryPublicImage.setNewsSummary(newsSummary);
        return newsSummaryPublicImage;
    }

    @Before
    public void initTest() {
        newsSummaryPublicImageSearchRepository.deleteAll();
        newsSummaryPublicImage = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsSummaryPublicImage() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryPublicImageRepository.findAll().size();

        // Create the NewsSummaryPublicImage
        restNewsSummaryPublicImageMockMvc.perform(post("/api/news-summary-public-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryPublicImage)))
            .andExpect(status().isCreated());

        // Validate the NewsSummaryPublicImage in the database
        List<NewsSummaryPublicImage> newsSummaryPublicImageList = newsSummaryPublicImageRepository.findAll();
        assertThat(newsSummaryPublicImageList).hasSize(databaseSizeBeforeCreate + 1);
        NewsSummaryPublicImage testNewsSummaryPublicImage = newsSummaryPublicImageList.get(newsSummaryPublicImageList.size() - 1);
        assertThat(testNewsSummaryPublicImage.getFilename()).isEqualTo(DEFAULT_FILENAME);
        assertThat(testNewsSummaryPublicImage.getBlob()).isEqualTo(DEFAULT_BLOB);
        assertThat(testNewsSummaryPublicImage.getBlobContentType()).isEqualTo(DEFAULT_BLOB_CONTENT_TYPE);
        assertThat(testNewsSummaryPublicImage.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testNewsSummaryPublicImage.getDescriptionTranslation()).isEqualTo(DEFAULT_DESCRIPTION_TRANSLATION);
        assertThat(testNewsSummaryPublicImage.getImageOrder()).isEqualTo(DEFAULT_IMAGE_ORDER);

        // Validate the NewsSummaryPublicImage in Elasticsearch
        NewsSummaryPublicImage newsSummaryPublicImageEs = newsSummaryPublicImageSearchRepository.findOne(testNewsSummaryPublicImage.getId());
        assertThat(newsSummaryPublicImageEs).isEqualToComparingFieldByField(testNewsSummaryPublicImage);
    }

    @Test
    @Transactional
    public void createNewsSummaryPublicImageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsSummaryPublicImageRepository.findAll().size();

        // Create the NewsSummaryPublicImage with an existing ID
        newsSummaryPublicImage.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsSummaryPublicImageMockMvc.perform(post("/api/news-summary-public-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryPublicImage)))
            .andExpect(status().isBadRequest());

        // Validate the NewsSummaryPublicImage in the database
        List<NewsSummaryPublicImage> newsSummaryPublicImageList = newsSummaryPublicImageRepository.findAll();
        assertThat(newsSummaryPublicImageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFilenameIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsSummaryPublicImageRepository.findAll().size();
        // set the field null
        newsSummaryPublicImage.setFilename(null);

        // Create the NewsSummaryPublicImage, which fails.

        restNewsSummaryPublicImageMockMvc.perform(post("/api/news-summary-public-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryPublicImage)))
            .andExpect(status().isBadRequest());

        List<NewsSummaryPublicImage> newsSummaryPublicImageList = newsSummaryPublicImageRepository.findAll();
        assertThat(newsSummaryPublicImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBlobIsRequired() throws Exception {
        int databaseSizeBeforeTest = newsSummaryPublicImageRepository.findAll().size();
        // set the field null
        newsSummaryPublicImage.setBlob(null);

        // Create the NewsSummaryPublicImage, which fails.

        restNewsSummaryPublicImageMockMvc.perform(post("/api/news-summary-public-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryPublicImage)))
            .andExpect(status().isBadRequest());

        List<NewsSummaryPublicImage> newsSummaryPublicImageList = newsSummaryPublicImageRepository.findAll();
        assertThat(newsSummaryPublicImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryPublicImages() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get all the newsSummaryPublicImageList
        restNewsSummaryPublicImageMockMvc.perform(get("/api/news-summary-public-images?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryPublicImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionTranslation").value(hasItem(DEFAULT_DESCRIPTION_TRANSLATION.toString())))
            .andExpect(jsonPath("$.[*].imageOrder").value(hasItem(DEFAULT_IMAGE_ORDER)));
    }

    @Test
    @Transactional
    public void getNewsSummaryPublicImage() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get the newsSummaryPublicImage
        restNewsSummaryPublicImageMockMvc.perform(get("/api/news-summary-public-images/{id}", newsSummaryPublicImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsSummaryPublicImage.getId().intValue()))
            .andExpect(jsonPath("$.filename").value(DEFAULT_FILENAME.toString()))
            .andExpect(jsonPath("$.blobContentType").value(DEFAULT_BLOB_CONTENT_TYPE))
            .andExpect(jsonPath("$.blob").value(Base64Utils.encodeToString(DEFAULT_BLOB)))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.descriptionTranslation").value(DEFAULT_DESCRIPTION_TRANSLATION.toString()))
            .andExpect(jsonPath("$.imageOrder").value(DEFAULT_IMAGE_ORDER));
    }

    @Test
    @Transactional
    public void getAllNewsSummaryPublicImagesByFilenameIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get all the newsSummaryPublicImageList where filename equals to DEFAULT_FILENAME
        defaultNewsSummaryPublicImageShouldBeFound("filename.equals=" + DEFAULT_FILENAME);

        // Get all the newsSummaryPublicImageList where filename equals to UPDATED_FILENAME
        defaultNewsSummaryPublicImageShouldNotBeFound("filename.equals=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryPublicImagesByFilenameIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get all the newsSummaryPublicImageList where filename in DEFAULT_FILENAME or UPDATED_FILENAME
        defaultNewsSummaryPublicImageShouldBeFound("filename.in=" + DEFAULT_FILENAME + "," + UPDATED_FILENAME);

        // Get all the newsSummaryPublicImageList where filename equals to UPDATED_FILENAME
        defaultNewsSummaryPublicImageShouldNotBeFound("filename.in=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryPublicImagesByFilenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get all the newsSummaryPublicImageList where filename is not null
        defaultNewsSummaryPublicImageShouldBeFound("filename.specified=true");

        // Get all the newsSummaryPublicImageList where filename is null
        defaultNewsSummaryPublicImageShouldNotBeFound("filename.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryPublicImagesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get all the newsSummaryPublicImageList where description equals to DEFAULT_DESCRIPTION
        defaultNewsSummaryPublicImageShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the newsSummaryPublicImageList where description equals to UPDATED_DESCRIPTION
        defaultNewsSummaryPublicImageShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryPublicImagesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get all the newsSummaryPublicImageList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultNewsSummaryPublicImageShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the newsSummaryPublicImageList where description equals to UPDATED_DESCRIPTION
        defaultNewsSummaryPublicImageShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryPublicImagesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get all the newsSummaryPublicImageList where description is not null
        defaultNewsSummaryPublicImageShouldBeFound("description.specified=true");

        // Get all the newsSummaryPublicImageList where description is null
        defaultNewsSummaryPublicImageShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryPublicImagesByDescriptionTranslationIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get all the newsSummaryPublicImageList where descriptionTranslation equals to DEFAULT_DESCRIPTION_TRANSLATION
        defaultNewsSummaryPublicImageShouldBeFound("descriptionTranslation.equals=" + DEFAULT_DESCRIPTION_TRANSLATION);

        // Get all the newsSummaryPublicImageList where descriptionTranslation equals to UPDATED_DESCRIPTION_TRANSLATION
        defaultNewsSummaryPublicImageShouldNotBeFound("descriptionTranslation.equals=" + UPDATED_DESCRIPTION_TRANSLATION);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryPublicImagesByDescriptionTranslationIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get all the newsSummaryPublicImageList where descriptionTranslation in DEFAULT_DESCRIPTION_TRANSLATION or UPDATED_DESCRIPTION_TRANSLATION
        defaultNewsSummaryPublicImageShouldBeFound("descriptionTranslation.in=" + DEFAULT_DESCRIPTION_TRANSLATION + "," + UPDATED_DESCRIPTION_TRANSLATION);

        // Get all the newsSummaryPublicImageList where descriptionTranslation equals to UPDATED_DESCRIPTION_TRANSLATION
        defaultNewsSummaryPublicImageShouldNotBeFound("descriptionTranslation.in=" + UPDATED_DESCRIPTION_TRANSLATION);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryPublicImagesByDescriptionTranslationIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get all the newsSummaryPublicImageList where descriptionTranslation is not null
        defaultNewsSummaryPublicImageShouldBeFound("descriptionTranslation.specified=true");

        // Get all the newsSummaryPublicImageList where descriptionTranslation is null
        defaultNewsSummaryPublicImageShouldNotBeFound("descriptionTranslation.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryPublicImagesByImageOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get all the newsSummaryPublicImageList where imageOrder equals to DEFAULT_IMAGE_ORDER
        defaultNewsSummaryPublicImageShouldBeFound("imageOrder.equals=" + DEFAULT_IMAGE_ORDER);

        // Get all the newsSummaryPublicImageList where imageOrder equals to UPDATED_IMAGE_ORDER
        defaultNewsSummaryPublicImageShouldNotBeFound("imageOrder.equals=" + UPDATED_IMAGE_ORDER);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryPublicImagesByImageOrderIsInShouldWork() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get all the newsSummaryPublicImageList where imageOrder in DEFAULT_IMAGE_ORDER or UPDATED_IMAGE_ORDER
        defaultNewsSummaryPublicImageShouldBeFound("imageOrder.in=" + DEFAULT_IMAGE_ORDER + "," + UPDATED_IMAGE_ORDER);

        // Get all the newsSummaryPublicImageList where imageOrder equals to UPDATED_IMAGE_ORDER
        defaultNewsSummaryPublicImageShouldNotBeFound("imageOrder.in=" + UPDATED_IMAGE_ORDER);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryPublicImagesByImageOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get all the newsSummaryPublicImageList where imageOrder is not null
        defaultNewsSummaryPublicImageShouldBeFound("imageOrder.specified=true");

        // Get all the newsSummaryPublicImageList where imageOrder is null
        defaultNewsSummaryPublicImageShouldNotBeFound("imageOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllNewsSummaryPublicImagesByImageOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get all the newsSummaryPublicImageList where imageOrder greater than or equals to DEFAULT_IMAGE_ORDER
        defaultNewsSummaryPublicImageShouldBeFound("imageOrder.greaterOrEqualThan=" + DEFAULT_IMAGE_ORDER);

        // Get all the newsSummaryPublicImageList where imageOrder greater than or equals to UPDATED_IMAGE_ORDER
        defaultNewsSummaryPublicImageShouldNotBeFound("imageOrder.greaterOrEqualThan=" + UPDATED_IMAGE_ORDER);
    }

    @Test
    @Transactional
    public void getAllNewsSummaryPublicImagesByImageOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);

        // Get all the newsSummaryPublicImageList where imageOrder less than or equals to DEFAULT_IMAGE_ORDER
        defaultNewsSummaryPublicImageShouldNotBeFound("imageOrder.lessThan=" + DEFAULT_IMAGE_ORDER);

        // Get all the newsSummaryPublicImageList where imageOrder less than or equals to UPDATED_IMAGE_ORDER
        defaultNewsSummaryPublicImageShouldBeFound("imageOrder.lessThan=" + UPDATED_IMAGE_ORDER);
    }


    @Test
    @Transactional
    public void getAllNewsSummaryPublicImagesByNewsSummaryIsEqualToSomething() throws Exception {
        // Initialize the database
        NewsSummary newsSummary = NewsSummaryResourceIntTest.createEntity(em);
        em.persist(newsSummary);
        em.flush();
        newsSummaryPublicImage.setNewsSummary(newsSummary);
        newsSummaryPublicImageRepository.saveAndFlush(newsSummaryPublicImage);
        Long newsSummaryId = newsSummary.getId();

        // Get all the newsSummaryPublicImageList where newsSummary equals to newsSummaryId
        defaultNewsSummaryPublicImageShouldBeFound("newsSummaryId.equals=" + newsSummaryId);

        // Get all the newsSummaryPublicImageList where newsSummary equals to newsSummaryId + 1
        defaultNewsSummaryPublicImageShouldNotBeFound("newsSummaryId.equals=" + (newsSummaryId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultNewsSummaryPublicImageShouldBeFound(String filter) throws Exception {
        restNewsSummaryPublicImageMockMvc.perform(get("/api/news-summary-public-images?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryPublicImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionTranslation").value(hasItem(DEFAULT_DESCRIPTION_TRANSLATION.toString())))
            .andExpect(jsonPath("$.[*].imageOrder").value(hasItem(DEFAULT_IMAGE_ORDER)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultNewsSummaryPublicImageShouldNotBeFound(String filter) throws Exception {
        restNewsSummaryPublicImageMockMvc.perform(get("/api/news-summary-public-images?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingNewsSummaryPublicImage() throws Exception {
        // Get the newsSummaryPublicImage
        restNewsSummaryPublicImageMockMvc.perform(get("/api/news-summary-public-images/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsSummaryPublicImage() throws Exception {
        // Initialize the database
        newsSummaryPublicImageService.save(newsSummaryPublicImage);

        int databaseSizeBeforeUpdate = newsSummaryPublicImageRepository.findAll().size();

        // Update the newsSummaryPublicImage
        NewsSummaryPublicImage updatedNewsSummaryPublicImage = newsSummaryPublicImageRepository.findOne(newsSummaryPublicImage.getId());
        updatedNewsSummaryPublicImage
            .filename(UPDATED_FILENAME)
            .blob(UPDATED_BLOB)
            .blobContentType(UPDATED_BLOB_CONTENT_TYPE)
            .description(UPDATED_DESCRIPTION)
            .descriptionTranslation(UPDATED_DESCRIPTION_TRANSLATION)
            .imageOrder(UPDATED_IMAGE_ORDER);

        restNewsSummaryPublicImageMockMvc.perform(put("/api/news-summary-public-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsSummaryPublicImage)))
            .andExpect(status().isOk());

        // Validate the NewsSummaryPublicImage in the database
        List<NewsSummaryPublicImage> newsSummaryPublicImageList = newsSummaryPublicImageRepository.findAll();
        assertThat(newsSummaryPublicImageList).hasSize(databaseSizeBeforeUpdate);
        NewsSummaryPublicImage testNewsSummaryPublicImage = newsSummaryPublicImageList.get(newsSummaryPublicImageList.size() - 1);
        assertThat(testNewsSummaryPublicImage.getFilename()).isEqualTo(UPDATED_FILENAME);
        assertThat(testNewsSummaryPublicImage.getBlob()).isEqualTo(UPDATED_BLOB);
        assertThat(testNewsSummaryPublicImage.getBlobContentType()).isEqualTo(UPDATED_BLOB_CONTENT_TYPE);
        assertThat(testNewsSummaryPublicImage.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testNewsSummaryPublicImage.getDescriptionTranslation()).isEqualTo(UPDATED_DESCRIPTION_TRANSLATION);
        assertThat(testNewsSummaryPublicImage.getImageOrder()).isEqualTo(UPDATED_IMAGE_ORDER);

        // Validate the NewsSummaryPublicImage in Elasticsearch
        NewsSummaryPublicImage newsSummaryPublicImageEs = newsSummaryPublicImageSearchRepository.findOne(testNewsSummaryPublicImage.getId());
        assertThat(newsSummaryPublicImageEs).isEqualToComparingFieldByField(testNewsSummaryPublicImage);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsSummaryPublicImage() throws Exception {
        int databaseSizeBeforeUpdate = newsSummaryPublicImageRepository.findAll().size();

        // Create the NewsSummaryPublicImage

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsSummaryPublicImageMockMvc.perform(put("/api/news-summary-public-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsSummaryPublicImage)))
            .andExpect(status().isCreated());

        // Validate the NewsSummaryPublicImage in the database
        List<NewsSummaryPublicImage> newsSummaryPublicImageList = newsSummaryPublicImageRepository.findAll();
        assertThat(newsSummaryPublicImageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsSummaryPublicImage() throws Exception {
        // Initialize the database
        newsSummaryPublicImageService.save(newsSummaryPublicImage);

        int databaseSizeBeforeDelete = newsSummaryPublicImageRepository.findAll().size();

        // Get the newsSummaryPublicImage
        restNewsSummaryPublicImageMockMvc.perform(delete("/api/news-summary-public-images/{id}", newsSummaryPublicImage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean newsSummaryPublicImageExistsInEs = newsSummaryPublicImageSearchRepository.exists(newsSummaryPublicImage.getId());
        assertThat(newsSummaryPublicImageExistsInEs).isFalse();

        // Validate the database is empty
        List<NewsSummaryPublicImage> newsSummaryPublicImageList = newsSummaryPublicImageRepository.findAll();
        assertThat(newsSummaryPublicImageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNewsSummaryPublicImage() throws Exception {
        // Initialize the database
        newsSummaryPublicImageService.save(newsSummaryPublicImage);

        // Search the newsSummaryPublicImage
        restNewsSummaryPublicImageMockMvc.perform(get("/api/_search/news-summary-public-images?query=id:" + newsSummaryPublicImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsSummaryPublicImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME.toString())))
            .andExpect(jsonPath("$.[*].blobContentType").value(hasItem(DEFAULT_BLOB_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].blob").value(hasItem(Base64Utils.encodeToString(DEFAULT_BLOB))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionTranslation").value(hasItem(DEFAULT_DESCRIPTION_TRANSLATION.toString())))
            .andExpect(jsonPath("$.[*].imageOrder").value(hasItem(DEFAULT_IMAGE_ORDER)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsSummaryPublicImage.class);
        NewsSummaryPublicImage newsSummaryPublicImage1 = new NewsSummaryPublicImage();
        newsSummaryPublicImage1.setId(1L);
        NewsSummaryPublicImage newsSummaryPublicImage2 = new NewsSummaryPublicImage();
        newsSummaryPublicImage2.setId(newsSummaryPublicImage1.getId());
        assertThat(newsSummaryPublicImage1).isEqualTo(newsSummaryPublicImage2);
        newsSummaryPublicImage2.setId(2L);
        assertThat(newsSummaryPublicImage1).isNotEqualTo(newsSummaryPublicImage2);
        newsSummaryPublicImage1.setId(null);
        assertThat(newsSummaryPublicImage1).isNotEqualTo(newsSummaryPublicImage2);
    }
}
