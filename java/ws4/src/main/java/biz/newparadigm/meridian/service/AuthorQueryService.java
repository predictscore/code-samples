package biz.newparadigm.meridian.service;

import java.util.List;

import biz.newparadigm.meridian.service.filter.SpecificationBuilder;
import biz.newparadigm.meridian.service.odata.Expression;
import biz.newparadigm.meridian.service.odata.ExpressionBuilder;
import biz.newparadigm.meridian.service.query.MeridianQueryService;
import biz.newparadigm.meridian.service.util.FieldsUtil;
import biz.newparadigm.meridian.service.util.ServiceUtil;
import biz.newparadigm.meridian.web.rest.AuthorResource;
import io.github.jhipster.service.filter.StringFilter;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.Author;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.AuthorRepository;
import biz.newparadigm.meridian.service.dto.AuthorCriteria;

import javax.persistence.criteria.*;
import javax.persistence.metamodel.ListAttribute;

/**
 * Service for executing complex queries for Author entities in the database.
 * The main input is a {@link AuthorCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link Author} or a {@link Page} of {%link Author} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class AuthorQueryService extends QueryService<Author> {
    private final Logger log = LoggerFactory.getLogger(AuthorQueryService.class);

    @Autowired
    private AuthorRepository authorRepository;

    /**
     * Return a {@link Page} of {%link Author} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Author> findByCriteria(String filter, AuthorCriteria criteria, Pageable page, boolean filterByLocation, List<String> locationNames) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        Expression filterExpression = ExpressionBuilder.build(filter);
        Specifications<Author> specification;
        if (filterExpression == null) {
            specification = createSpecification(criteria, filterByLocation, locationNames);
        } else {
            MeridianQueryService<Author> meridianQueryService = new MeridianQueryService<>(AuthorResource.ENTITY_NAME);
            specification = meridianQueryService.createFilterSpecification(filterExpression, filterByLocation, locationNames);
        }
        Page<Author> result = authorRepository.findAll(specification, page);
        if (result != null && result.getContent() != null && !result.getContent().isEmpty()) {
            for (Author author : result) {
                if (ServiceUtil.checkContains(filter, ServiceUtil.AUTHOR_FILTER_NAMES)) {
                    Hibernate.initialize(author.getNames());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.AUTHOR_FILTER_CONTACTS)) {
                    Hibernate.initialize(author.getContacts());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.AUTHOR_FILTER_TAGS)) {
                    Hibernate.initialize(author.getTags());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.AUTHOR_FILTER_LANGUAGES)) {
                    Hibernate.initialize(author.getLanguages());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.AUTHOR_FILTER_SITECHATGROUPS)) {
                    Hibernate.initialize(author.getSiteChatGroups());
                }
            }
        }
        return result;
    }

    private void makeEncoding(StringFilter stringFilter) {
        if (stringFilter.getContains() != null) {
            stringFilter.setContains(MeridianQueryService.encodeForQuery(stringFilter.getContains()));
        }
        if (stringFilter.getEquals() != null) {
            stringFilter.setEquals(MeridianQueryService.encodeForQuery(stringFilter.getEquals()));
        }
    }

    /**
     * Function to convert AuthorCriteria to a {@link Specifications}
     */
    private Specifications<Author> createSpecification(AuthorCriteria criteria, boolean filterByLocation, List<String> locationNames) {
        SpecificationBuilder<Author> builder = new SpecificationBuilder<>();
        Specifications<Author> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Author_.id));
            }
            if (criteria.getOriginalName() != null) {
                makeEncoding(criteria.getOriginalName());
                specification = specification.and(buildStringSpecification(criteria.getOriginalName(), Author_.originalName));
            }
            if (criteria.getEnglishName() != null) {
                makeEncoding(criteria.getEnglishName());
                specification = specification.and(buildStringSpecification(criteria.getEnglishName(), Author_.englishName));
            }
            if (criteria.getLocationId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getLocationId(), Author_.location, Location_.id));
            }
            if (criteria.getLocation() != null) {
                makeEncoding(criteria.getLocation());
                specification = specification.and(buildReferringEntitySpecification(criteria.getLocation(), Author_.location, Location_.name));
            }
            if (criteria.getContact() != null) {
                makeEncoding(criteria.getContact());
                specification = specification.and(buildAuthorToContactEntitySpecification(criteria.getContact(), Author_.contacts, AuthorToContact_.contactValue.getJavaMember().getName()));
            }
            if (filterByLocation) {
                if (locationNames == null || locationNames.isEmpty()) {
                    specification = specification.and(builder.buildNoResultSpecification(Author_.id));
                } else {
                    specification = specification.and(builder.buildLocationsSpecification(locationNames, Author.class));
                }
            }
        }
        return specification;
    }

    private Specification<Author> buildAuthorToContactEntitySpecification(StringFilter filter, ListAttribute<? super Author, AuthorToContact> reference, String valueField) {
        return (root, query, builder) -> {
            final Subquery<Long> authorQuery = query.subquery(Long.class);
            final Root<Author> authorRoot = authorQuery.from(Author.class);
            final Join<Author, AuthorToContact> contacts = authorRoot.join(reference, JoinType.INNER);
            authorQuery.select(authorRoot.get("id"));
            if (filter.getEquals() != null) {
                authorQuery.where(builder.equal(contacts.get(valueField), filter.getEquals()));
            } else if (filter.getContains() != null) {
                String txt = MeridianQueryService.encodeForQuery(filter.getContains().toUpperCase());
                authorQuery.where(builder.like(builder.upper(contacts.get(valueField)),"%"+txt+"%"));
            }
            return builder.in(root.get("id")).value(authorQuery);
        };
    }
}
