package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.*;
import biz.newparadigm.meridian.domain.audit.AuditChange;
import biz.newparadigm.meridian.domain.audit.AuditChangeType;
import biz.newparadigm.meridian.domain.audit.AuditEventType;
import biz.newparadigm.meridian.domain.audit.AuditUtil;
import biz.newparadigm.meridian.repository.AuthorRepository;
import biz.newparadigm.meridian.repository.AuthorToSiteChatgroupRepository;
import biz.newparadigm.meridian.repository.search.AuthorSearchRepository;
import biz.newparadigm.meridian.security.SecurityUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.Hibernate;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Change;
import org.javers.core.diff.Diff;
import org.javers.core.diff.changetype.NewObject;
import org.javers.core.diff.changetype.ObjectRemoved;
import org.javers.core.diff.changetype.ReferenceChange;
import org.javers.core.diff.changetype.ValueChange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

@Service
@Transactional
public class AuthorService {

    private final Logger log = LoggerFactory.getLogger(AuthorService.class);

    private final AuthorRepository authorRepository;

    private final AuthorSearchRepository authorSearchRepository;

    @Autowired
    private EntityAuditService entityAuditService;

    @Autowired
    private AuthorNameService authorNameService;

    @Autowired
    private AuthorToContactService authorToContactService;

    @Autowired
    private AuthorToTagService authorToTagService;

    @Autowired
    private AuthorToSiteChatgroupService authorToSiteChatgroupService;

    @Autowired
    private AuthorToLanguageService authorToLanguageService;

    public AuthorService(AuthorRepository authorRepository, AuthorSearchRepository authorSearchRepository) {
        this.authorRepository = authorRepository;
        this.authorSearchRepository = authorSearchRepository;
    }

    /**
     * Save a author.
     *
     * @param author the entity to save
     * @return the persisted entity
     */
    public Author save(Author author) {
        log.debug("Request to save Author : {}", author);
        Author originalAuthor;
        String originalAuthorJson = "{}";
        ObjectMapper mapper = AuditUtil.getMapper();
        boolean insert = true;
        Author result;
        // PUT
        if (author.getId() != null && author.getId() > 0L) {
            insert = false;
            Author originalAuthor1 = authorRepository.findOne(author.getId());
            originalAuthor = AuditUtil.prepareAuthor(originalAuthor1);
            try {
                originalAuthorJson = mapper.writeValueAsString(originalAuthor);
            } catch (Exception e) {
                originalAuthorJson = "{}";
            }
            if (author.getNames() != null && !author.getNames().isEmpty()) {
                author.getNames().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0L) {
                        item.setAuthor(author.toJPASimpleObject());
                        authorNameService.save(item);
                    }
                });
            }
            if (author.getContacts() != null && !author.getContacts().isEmpty()) {
                author.getContacts().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0L) {
                        item.setAuthor(author.toJPASimpleObject());
                        authorToContactService.save(item);
                    }
                });
            }
            if (author.getTags() != null && !author.getTags().isEmpty()) {
                author.getTags().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0L) {
                        item.setAuthor(author.toJPASimpleObject());
                        authorToTagService.save(item);
                    }
                });
            }
            if (author.getSiteChatGroups() != null && !author.getSiteChatGroups().isEmpty()) {
                author.getSiteChatGroups().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0L) {
                        item.setAuthor(author.toJPASimpleObject());
                        authorToSiteChatgroupService.save(item);
                    }
                });
            }
            if (author.getLanguages() != null && !author.getLanguages().isEmpty()) {
                author.getLanguages().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0L) {
                        item.setAuthor(author.toJPASimpleObject());
                        authorToLanguageService.save(item);
                    }
                });
            }
            author.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
            author.setLastModifiedDate(ZonedDateTime.now(ZoneId.of("UTC")));
            result = authorRepository.save(author);
        } else {
            List<AuthorName> names = new ArrayList<>(author.getNames());
            List<AuthorToContact> contacts = new ArrayList<>(author.getContacts());
            List<AuthorToTag> tags = new ArrayList<>(author.getTags());
            List<AuthorToSiteChatgroup> siteChatgroups = new ArrayList<>(author.getSiteChatGroups());
            List<AuthorToLanguage> languages = new ArrayList<>(author.getLanguages());

            author.getNames().clear();
            author.getContacts().clear();
            author.getTags().clear();
            author.getSiteChatGroups().clear();
            author.getLanguages().clear();

            author.setCreatedBy(SecurityUtils.getCurrentUserLogin());
            author.setCreatedDate(ZonedDateTime.now(ZoneId.of("UTC")));
            result = authorRepository.save(author);

            if (!names.isEmpty()) {
                names.forEach(item -> {
                    item.setAuthor(result);
                    authorNameService.save(item);
                    author.getNames().add(item);
                });
            }
            if (!contacts.isEmpty()) {
                contacts.forEach(item -> {
                    item.setAuthor(result);
                    authorToContactService.save(item);
                    author.getContacts().add(item);
                });
            }
            if (!tags.isEmpty()) {
                tags.forEach(item -> {
                    item.setAuthor(result);
                    authorToTagService.save(item);
                    author.getTags().add(item);
                });
            }
            if (!siteChatgroups.isEmpty()) {
                siteChatgroups.forEach(item -> {
                    item.setAuthor(result);
                    authorToSiteChatgroupService.save(item);
                    author.getSiteChatGroups().add(item);
                });
            }
            if (!languages.isEmpty()) {
                languages.forEach(item -> {
                    item.setAuthor(result);
                    authorToLanguageService.save(item);
                    author.getLanguages().add(item);
                });
            }
        }

        Author updatedAuthor1 = findOne(result.getId(), true);
        Author updatedAuthor = AuditUtil.prepareAuthor(updatedAuthor1);

        String updatedAuthorJson;
        try {
            updatedAuthorJson = mapper.writeValueAsString(updatedAuthor);
        } catch (Exception e) {
            updatedAuthorJson = "{}";
        }

        try {
            Author beforeNode = mapper.readValue(originalAuthorJson, Author.class);
            Author afterNode = mapper.readValue(updatedAuthorJson, Author.class);
            Javers j = JaversBuilder.javers().build();
            Diff diff = j.compare(beforeNode, afterNode);
            EntityAudit audit = AuditUtil.getAuditEntity(mapper, insert, updatedAuthor1.getId(), diff, originalAuthorJson, updatedAuthorJson, Author.class.getName());
            if (audit != null) {
                entityAuditService.save(audit);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     *  Get all the authors.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Author> findAll(Pageable pageable) {
        log.debug("Request to get all Authors");
        return authorRepository.findAll(pageable);
    }

    /**
     *  Get one author by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Author findOne(Long id, boolean loadLazyItems) {
        log.debug("Request to get Author : {}", id);
        Author author = authorRepository.findOne(id);
        if (loadLazyItems && author != null) {
         //   Hibernate.initialize(author.getProfiles());
            Hibernate.initialize(author.getNames());
         //   Hibernate.initialize(author.getComments());
            Hibernate.initialize(author.getContacts());
            Hibernate.initialize(author.getSiteChatGroups());
            Hibernate.initialize(author.getTags());
            Hibernate.initialize(author.getLanguages());
        }
        return author;
    }

    /**
     *  Delete the  author by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Author : {}", id);
        authorRepository.delete(id);
        authorSearchRepository.delete(id);
    }

    /**
     * Search for the author corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Author> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Authors for query {}", query);
        Page<Author> result = authorSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
