package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.SiteChatgroupName;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SiteChatgroupName entity.
 */
public interface SiteChatgroupNameSearchRepository extends ElasticsearchRepository<SiteChatgroupName, Long> {
}
