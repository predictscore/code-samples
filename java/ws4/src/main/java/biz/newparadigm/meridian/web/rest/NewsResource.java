package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.config.ApplicationProperties;
import biz.newparadigm.meridian.security.SecurityUtils;
import biz.newparadigm.meridian.service.CrowdService;
import biz.newparadigm.meridian.service.LocationService;
import biz.newparadigm.meridian.service.dto.export.NewsExport;
import biz.newparadigm.meridian.service.mapper.NewsExportMapper;
import biz.newparadigm.meridian.service.util.UserUtil;
import biz.newparadigm.meridian.web.rest.util.AccessUtil;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.service.NewsService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsCriteria;
import biz.newparadigm.meridian.service.NewsQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.*;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

@RestController
@RequestMapping("/api")
public class NewsResource {

    private final Logger log = LoggerFactory.getLogger(NewsResource.class);

    public static final String ENTITY_NAME = "news";

    @Autowired
    private NewsService newsService;

    @Autowired
    private NewsQueryService newsQueryService;

    @Autowired
    private NewsExportMapper newsExportMapper;

    @Autowired
    private ApplicationProperties properties;

    @Autowired
    private CrowdService crowdService;

    @Autowired
    private LocationService locationService;

    /**
     * POST  /news : Create a new news.
     *
     * @param news the news to create
     * @return the ResponseEntity with status 201 (Created) and with body the new news, or with status 400 (Bad Request) if the news has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news")
    @Timed
    public ResponseEntity<News> createNews(@Valid @RequestBody News news) throws URISyntaxException {
        log.debug("REST request to save News : {}", news);
        if (news.getId() != null) {
            throw new BadRequestAlertException("A new news cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (properties.isCrowd() && !isAdmin()) {
            boolean hasAccess = hasAccess(news);
            if (!hasAccess) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        }
        News result = newsService.save(news);
        return ResponseEntity.created(new URI("/api/news/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news : Updates an existing news.
     *
     * @param news the news to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated news,
     * or with status 400 (Bad Request) if the news is not valid,
     * or with status 500 (Internal Server Error) if the news couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news")
    @Timed
    public ResponseEntity<News> updateNews(@Valid @RequestBody News news) throws URISyntaxException {
        log.debug("REST request to update News : {}", news);
        if (properties.isCrowd() && !isAdmin()) {
            boolean hasAccess = hasAccess(news);
            if (!hasAccess) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        }
        if (news.getId() == null) {
            return createNews(news);
        }
        News result = newsService.save(news);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, news.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news : get all the news.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of news in body
     */
    @GetMapping("/news")
    @Timed
    public ResponseEntity<List<News>> getAllNews(@RequestParam(name = "filter", required = false) String filter,
                                                 @RequestParam(name = "select", required = false) String select,
                                                 @RequestParam(name = "ignore", required = false) String ignore,
                                                 @ApiParam NewsCriteria criteria,
                                                 @ApiParam Pageable pageable) {
        log.debug("REST request to get News by criteria: {}", criteria);
        boolean filterByLocation = false;
        List<String> locationNames = new ArrayList<>();
        if (properties.isCrowd() && !isAdmin()) {
            filterByLocation = true;
            locationNames = UserUtil.getUserLocations(crowdService, SecurityUtils.getCurrentUserLogin());
        }
        Page<News> page = newsQueryService.findByCriteria(filter, select, ignore, criteria, pageable, filterByLocation, locationNames);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news/:id : get the "id" news.
     *
     * @param id the id of the news to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the news, or with status 404 (Not Found)
     */
    @GetMapping("/news/{id}")
    @Timed
    public ResponseEntity<News> getNews(@PathVariable Long id) {
        log.debug("REST request to get News : {}", id);
        News news = newsService.findOne(id, true);
        if (properties.isCrowd() && !isAdmin()) {
            boolean hasAccess = hasAccess(news);
            if (!hasAccess) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        }
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(news));
    }

    /**
     * DELETE  /news/:id : delete the "id" news.
     *
     * @param id the id of the news to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news/{id}")
    @Timed
    public ResponseEntity<Void> deleteNews(@PathVariable Long id) {
        log.debug("REST request to delete News : {}", id);
        if (properties.isCrowd() && !isAdmin()) {
            News news = newsService.findOne(id, false);
            boolean hasAccess = hasAccess(news);
            if (!hasAccess) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        }
        newsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news?query=:query : search for the news corresponding
     * to the query.
     *
     * @param query the query of the news search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news")
    @Timed
    public ResponseEntity<List<News>> searchNews(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of News for query {}", query);
        Page<News> page = newsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/newsExport")
    @Timed
    public ResponseEntity<List<NewsExport>> getNewsByNewsDate(@RequestParam(value="fromDate") String fromDate,
                                                            @RequestParam(value="toDate", required = false) String toDate,
                                                            @RequestParam(value="tags", required = false) String tags,
                                                            @RequestParam(value="limit", required = false) Integer limit) {
        List<News> newstList = newsService.findAllForExport(fromDate, toDate, tags, limit);
        List<NewsExport> newsExportList = newsExportMapper.toNewsExport(newstList);
        return new ResponseEntity<>(newsExportList, HttpStatus.OK);
    }

    private boolean isAdmin() {
        CrowdUserDetails user = crowdService.getUserByUsername(SecurityUtils.getCurrentUserLogin());
        return UserUtil.isAdmin(user);
    }

    private boolean hasAccess(News news) {
        return AccessUtil.hasAccess(crowdService, locationService, news.getLocation());
    }
}
