package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsAttachment;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsAttachmentRepository;
import biz.newparadigm.meridian.repository.search.NewsAttachmentSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsAttachmentCriteria;


/**
 * Service for executing complex queries for NewsAttachment entities in the database.
 * The main input is a {@link NewsAttachmentCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsAttachment} or a {@link Page} of {%link NewsAttachment} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsAttachmentQueryService extends QueryService<NewsAttachment> {

    private final Logger log = LoggerFactory.getLogger(NewsAttachmentQueryService.class);


    private final NewsAttachmentRepository newsAttachmentRepository;

    private final NewsAttachmentSearchRepository newsAttachmentSearchRepository;

    public NewsAttachmentQueryService(NewsAttachmentRepository newsAttachmentRepository, NewsAttachmentSearchRepository newsAttachmentSearchRepository) {
        this.newsAttachmentRepository = newsAttachmentRepository;
        this.newsAttachmentSearchRepository = newsAttachmentSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsAttachment} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsAttachment> findByCriteria(NewsAttachmentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsAttachment> specification = createSpecification(criteria);
        return newsAttachmentRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsAttachment} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsAttachment> findByCriteria(NewsAttachmentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsAttachment> specification = createSpecification(criteria);
        return newsAttachmentRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsAttachmentCriteria to a {@link Specifications}
     */
    private Specifications<NewsAttachment> createSpecification(NewsAttachmentCriteria criteria) {
        Specifications<NewsAttachment> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsAttachment_.id));
            }
            if (criteria.getFilename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFilename(), NewsAttachment_.filename));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), NewsAttachment_.description));
            }
            if (criteria.getNewsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsId(), NewsAttachment_.news, News_.id));
            }
        }
        return specification;
    }

}
