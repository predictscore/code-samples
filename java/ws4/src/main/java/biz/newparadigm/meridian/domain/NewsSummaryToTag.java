package biz.newparadigm.meridian.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A NewsSummaryToTag.
 */
@Entity
@Table(name = "news_summary_to_tags")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "newssummarytotag")
public class NewsSummaryToTag implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="news_summary_to_tags_id_seq")
    @SequenceGenerator(name="news_summary_to_tags_id_seq", sequenceName="news_summary_to_tags_id_seq", allocationSize=1)
    private Long id;

    @ManyToOne(optional = false)
    @NotNull
    private NewsSummary newsSummary;

    @ManyToOne(optional = false)
    @NotNull
    private Tag tag;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NewsSummary getNewsSummary() {
        return newsSummary;
    }

    public NewsSummaryToTag newsSummary(NewsSummary newsSummary) {
        this.newsSummary = newsSummary;
        return this;
    }

    public void setNewsSummary(NewsSummary newsSummary) {
        this.newsSummary = newsSummary;
    }

    public Tag getTag() {
        return tag;
    }

    public NewsSummaryToTag tag(Tag tag) {
        this.tag = tag;
        return this;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NewsSummaryToTag newsSummaryToTag = (NewsSummaryToTag) o;
        if (newsSummaryToTag.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), newsSummaryToTag.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NewsSummaryToTag{" +
            "id=" + getId() +
            "}";
    }
}
