package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.SiteChatgroupNumUsers;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SiteChatgroupNumUsers entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SiteChatgroupNumUsersRepository extends JpaRepository<SiteChatgroupNumUsers, Long>, JpaSpecificationExecutor<SiteChatgroupNumUsers> {

}
