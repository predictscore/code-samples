package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsToRelatedAuthor;
import biz.newparadigm.meridian.repository.NewsToRelatedAuthorRepository;
import biz.newparadigm.meridian.repository.search.NewsToRelatedAuthorSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsToRelatedAuthor.
 */
@Service
@Transactional
public class NewsToRelatedAuthorService {

    private final Logger log = LoggerFactory.getLogger(NewsToRelatedAuthorService.class);

    private final NewsToRelatedAuthorRepository newsToRelatedAuthorRepository;

    private final NewsToRelatedAuthorSearchRepository newsToRelatedAuthorSearchRepository;

    public NewsToRelatedAuthorService(NewsToRelatedAuthorRepository newsToRelatedAuthorRepository, NewsToRelatedAuthorSearchRepository newsToRelatedAuthorSearchRepository) {
        this.newsToRelatedAuthorRepository = newsToRelatedAuthorRepository;
        this.newsToRelatedAuthorSearchRepository = newsToRelatedAuthorSearchRepository;
    }

    /**
     * Save a newsToRelatedAuthor.
     *
     * @param newsToRelatedAuthor the entity to save
     * @return the persisted entity
     */
    public NewsToRelatedAuthor save(NewsToRelatedAuthor newsToRelatedAuthor) {
        log.debug("Request to save NewsToRelatedAuthor : {}", newsToRelatedAuthor);
        NewsToRelatedAuthor result = newsToRelatedAuthorRepository.save(newsToRelatedAuthor);
        newsToRelatedAuthorSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsToRelatedAuthors.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<NewsToRelatedAuthor> findAll() {
        log.debug("Request to get all NewsToRelatedAuthors");
        return newsToRelatedAuthorRepository.findAll();
    }

    /**
     *  Get one newsToRelatedAuthor by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsToRelatedAuthor findOne(Long id) {
        log.debug("Request to get NewsToRelatedAuthor : {}", id);
        return newsToRelatedAuthorRepository.findOne(id);
    }

    /**
     *  Delete the  newsToRelatedAuthor by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsToRelatedAuthor : {}", id);
        newsToRelatedAuthorRepository.delete(id);
        newsToRelatedAuthorSearchRepository.delete(id);
    }

    /**
     * Search for the newsToRelatedAuthor corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<NewsToRelatedAuthor> search(String query) {
        log.debug("Request to search NewsToRelatedAuthors for query {}", query);
        return StreamSupport
            .stream(newsToRelatedAuthorSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
