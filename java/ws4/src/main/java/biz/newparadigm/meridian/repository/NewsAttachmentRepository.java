package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsAttachment;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsAttachment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsAttachmentRepository extends JpaRepository<NewsAttachment, Long>, JpaSpecificationExecutor<NewsAttachment> {

}
