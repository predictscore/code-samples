package biz.newparadigm.meridian.domain;

import biz.newparadigm.meridian.domain.listeners.AuthorAuditListener;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "authors")
@Document(indexName = "author")
@EntityListeners(AuthorAuditListener.class)
public class Author extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="authors_id_seq")
    @SequenceGenerator(name="authors_id_seq", sequenceName="authors_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "original_name", nullable = false)
    private String originalName;

    @NotNull
    @Column(name = "english_name", nullable = false)
    private String englishName;

    @ManyToOne(optional = false)
    @NotNull
    private Location location;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<AuthorName> names = new ArrayList<>();

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<AuthorToContact> contacts = new ArrayList<>();

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<AuthorToTag> tags = new ArrayList<>();

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<AuthorToLanguage> languages = new ArrayList<>();

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<AuthorToSiteChatgroup> siteChatGroups = new ArrayList<>();

    @Version
    private long version = 0L;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public Author originalName(String originalName) {
        this.originalName = originalName;
        return this;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public Author englishName(String englishName) {
        this.englishName = englishName;
        return this;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public Location getLocation() {
        return location;
    }

    public Author location(Location location) {
        this.location = location;
        return this;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

//    public List<AuthorProfile> getProfiles() {
//        return profiles;
//    }
//
//    public Author profiles(List<AuthorProfile> authorProfiles) {
//        this.profiles = authorProfiles;
//        return this;
//    }
//
//    public Author addProfiles(AuthorProfile authorProfile) {
//        this.profiles.add(authorProfile);
//        authorProfile.setAuthor(this);
//        return this;
//    }
//
//    public Author removeProfiles(AuthorProfile authorProfile) {
//        this.profiles.remove(authorProfile);
//        authorProfile.setAuthor(null);
//        return this;
//    }

//    public void setProfiles(List<AuthorProfile> authorProfiles) {
//        this.profiles = authorProfiles;
//    }

    public List<AuthorName> getNames() {
        return names;
    }

    public Author names(List<AuthorName> authorNames) {
        this.names = authorNames;
        return this;
    }

    public Author addNames(AuthorName authorName) {
        this.names.add(authorName);
        authorName.setAuthor(this);
        return this;
    }

    public Author removeNames(AuthorName authorName) {
        this.names.remove(authorName);
        authorName.setAuthor(null);
        return this;
    }

    public void setNames(List<AuthorName> authorNames) {
        this.names = authorNames;
    }

//    public List<AuthorComment> getComments() {
//        return comments;
//    }
//
//    public Author comments(List<AuthorComment> authorComments) {
//        this.comments = authorComments;
//        return this;
//    }
//
//    public Author addComments(AuthorComment authorComment) {
//        this.comments.add(authorComment);
//        authorComment.setAuthor(this);
//        return this;
//    }
//
//    public Author removeComments(AuthorComment authorComment) {
//        this.comments.remove(authorComment);
//        authorComment.setAuthor(null);
//        return this;
//    }
//
//    public void setComments(List<AuthorComment> authorComments) {
//        this.comments = authorComments;
//    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public List<AuthorToContact> getContacts() {
        return contacts;
    }

    public void setContacts(List<AuthorToContact> contacts) {
        this.contacts = contacts;
    }

    public List<AuthorToSiteChatgroup> getSiteChatGroups() {
        return siteChatGroups;
    }

    public void setSiteChatGroups(List<AuthorToSiteChatgroup> siteChatGroups) {
        this.siteChatGroups = siteChatGroups;
    }

    public List<AuthorToTag> getTags() {
        return tags;
    }

    public void setTags(List<AuthorToTag> tags) {
        this.tags = tags;
    }

    public List<AuthorToLanguage> getLanguages() {
        return languages;
    }

    public void setLanguages(List<AuthorToLanguage> languages) {
        this.languages = languages;
    }

    public Author toJPASimpleObject() {
        Author author = new Author();
        author.setId(this.getId());
        return author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Author author = (Author) o;
        if (author.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), author.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Author{" +
            "id=" + getId() +
            ", originalName='" + getOriginalName() + "'" +
            ", englishName='" + getEnglishName() + "'" +
            "}";
    }
}
