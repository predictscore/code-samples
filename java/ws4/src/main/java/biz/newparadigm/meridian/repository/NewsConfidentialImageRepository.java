package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsConfidentialImage;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsConfidentialImage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsConfidentialImageRepository extends JpaRepository<NewsConfidentialImage, Long>, JpaSpecificationExecutor<NewsConfidentialImage> {

}
