package biz.newparadigm.meridian.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.SiteChatgroupComment;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.SiteChatgroupCommentRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupCommentSearchRepository;
import biz.newparadigm.meridian.service.dto.SiteChatgroupCommentCriteria;


/**
 * Service for executing complex queries for SiteChatgroupComment entities in the database.
 * The main input is a {@link SiteChatgroupCommentCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link SiteChatgroupComment} or a {@link Page} of {%link SiteChatgroupComment} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class SiteChatgroupCommentQueryService extends QueryService<SiteChatgroupComment> {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupCommentQueryService.class);


    private final SiteChatgroupCommentRepository siteChatgroupCommentRepository;

    private final SiteChatgroupCommentSearchRepository siteChatgroupCommentSearchRepository;

    public SiteChatgroupCommentQueryService(SiteChatgroupCommentRepository siteChatgroupCommentRepository, SiteChatgroupCommentSearchRepository siteChatgroupCommentSearchRepository) {
        this.siteChatgroupCommentRepository = siteChatgroupCommentRepository;
        this.siteChatgroupCommentSearchRepository = siteChatgroupCommentSearchRepository;
    }

    /**
     * Return a {@link List} of {%link SiteChatgroupComment} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SiteChatgroupComment> findByCriteria(SiteChatgroupCommentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<SiteChatgroupComment> specification = createSpecification(criteria);
        return siteChatgroupCommentRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link SiteChatgroupComment} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupComment> findByCriteria(SiteChatgroupCommentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<SiteChatgroupComment> specification = createSpecification(criteria);
        return siteChatgroupCommentRepository.findAll(specification, page);
    }

    /**
     * Function to convert SiteChatgroupCommentCriteria to a {@link Specifications}
     */
    private Specifications<SiteChatgroupComment> createSpecification(SiteChatgroupCommentCriteria criteria) {
        Specifications<SiteChatgroupComment> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SiteChatgroupComment_.id));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUserId(), SiteChatgroupComment_.userId));
            }
            if (criteria.getComment() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComment(), SiteChatgroupComment_.comment));
            }
            if (criteria.getDatetime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDatetime(), SiteChatgroupComment_.datetime));
            }
            if (criteria.getSiteChatgroupId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSiteChatgroupId(), SiteChatgroupComment_.siteChatgroup, SiteChatgroup_.id));
            }
            if (criteria.getParentCommentId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getParentCommentId(), SiteChatgroupComment_.parentComment, SiteChatgroupComment_.id));
            }
        }
        return specification;
    }

}
