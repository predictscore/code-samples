package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsSummaryComment;
import biz.newparadigm.meridian.repository.NewsSummaryCommentRepository;
import biz.newparadigm.meridian.repository.search.NewsSummaryCommentSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsSummaryComment.
 */
@Service
@Transactional
public class NewsSummaryCommentService {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryCommentService.class);

    private final NewsSummaryCommentRepository newsSummaryCommentRepository;

    private final NewsSummaryCommentSearchRepository newsSummaryCommentSearchRepository;

    public NewsSummaryCommentService(NewsSummaryCommentRepository newsSummaryCommentRepository, NewsSummaryCommentSearchRepository newsSummaryCommentSearchRepository) {
        this.newsSummaryCommentRepository = newsSummaryCommentRepository;
        this.newsSummaryCommentSearchRepository = newsSummaryCommentSearchRepository;
    }

    /**
     * Save a newsSummaryComment.
     *
     * @param newsSummaryComment the entity to save
     * @return the persisted entity
     */
    public NewsSummaryComment save(NewsSummaryComment newsSummaryComment) {
        log.debug("Request to save NewsSummaryComment : {}", newsSummaryComment);
        NewsSummaryComment result = newsSummaryCommentRepository.save(newsSummaryComment);
        newsSummaryCommentSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsSummaryComments.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryComment> findAll(Pageable pageable) {
        log.debug("Request to get all NewsSummaryComments");
        return newsSummaryCommentRepository.findAll(pageable);
    }

    /**
     *  Get one newsSummaryComment by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsSummaryComment findOne(Long id) {
        log.debug("Request to get NewsSummaryComment : {}", id);
        return newsSummaryCommentRepository.findOne(id);
    }

    /**
     *  Delete the  newsSummaryComment by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsSummaryComment : {}", id);
        newsSummaryCommentRepository.delete(id);
        newsSummaryCommentSearchRepository.delete(id);
    }

    /**
     * Search for the newsSummaryComment corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryComment> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsSummaryComments for query {}", query);
        Page<NewsSummaryComment> result = newsSummaryCommentSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
