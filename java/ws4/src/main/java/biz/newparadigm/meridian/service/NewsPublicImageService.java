package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsPublicImage;
import biz.newparadigm.meridian.repository.NewsPublicImageRepository;
import biz.newparadigm.meridian.repository.search.NewsPublicImageSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsPublicImage.
 */
@Service
@Transactional
public class NewsPublicImageService {

    private final Logger log = LoggerFactory.getLogger(NewsPublicImageService.class);

    private final NewsPublicImageRepository newsPublicImageRepository;

    private final NewsPublicImageSearchRepository newsPublicImageSearchRepository;

    public NewsPublicImageService(NewsPublicImageRepository newsPublicImageRepository, NewsPublicImageSearchRepository newsPublicImageSearchRepository) {
        this.newsPublicImageRepository = newsPublicImageRepository;
        this.newsPublicImageSearchRepository = newsPublicImageSearchRepository;
    }

    /**
     * Save a newsPublicImage.
     *
     * @param newsPublicImage the entity to save
     * @return the persisted entity
     */
    public NewsPublicImage save(NewsPublicImage newsPublicImage) {
        log.debug("Request to save NewsPublicImage : {}", newsPublicImage);
        NewsPublicImage result = newsPublicImageRepository.save(newsPublicImage);
        newsPublicImageSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsPublicImages.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsPublicImage> findAll(Pageable pageable) {
        log.debug("Request to get all NewsPublicImages");
        return newsPublicImageRepository.findAll(pageable);
    }

    /**
     *  Get one newsPublicImage by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsPublicImage findOne(Long id) {
        log.debug("Request to get NewsPublicImage : {}", id);
        return newsPublicImageRepository.findOne(id);
    }

    /**
     *  Delete the  newsPublicImage by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsPublicImage : {}", id);
        newsPublicImageRepository.delete(id);
        newsPublicImageSearchRepository.delete(id);
    }

    /**
     * Search for the newsPublicImage corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsPublicImage> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsPublicImages for query {}", query);
        Page<NewsPublicImage> result = newsPublicImageSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
