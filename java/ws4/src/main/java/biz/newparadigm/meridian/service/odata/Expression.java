package biz.newparadigm.meridian.service.odata;

public class Expression {
    public static class Num extends Expression {
        private final long value;

        public Num(long x) {
            value = x;
        }

        public long getValue() {
            return value;
        }
    }

    public static class Str extends Expression {
        private final String value;

        public Str(String x) {
            value = x;
        }

        public String getValue() {
            return value;
        }
    }

    public static class Var extends Expression {
        private final String name;

        public Var(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public static Object getObjectValue(Expression ex) {
        if (ex instanceof Str) {
            return ((Str) ex).getValue();
        } else if (ex instanceof Var) {
            return ((Var) ex).getName();
        } else if (ex instanceof Num) {
            return ((Num) ex).getValue();
        }
        return "";
    }

    public static String getStringValue(Expression ex) {
        if (ex instanceof Str) {
            return ((Str) ex).getValue();
        } else if (ex instanceof Var) {
            return ((Var) ex).getName();
        } else if (ex instanceof Num) {
            return String.valueOf(((Num) ex).getValue());
        }
        return "";
    }

    public static class Unary extends Expression {
        private final Expression expr;
        private final boolean not;

        public Unary(Expression e, String oper) {
            expr = e;
            not = "not".equalsIgnoreCase(oper);
        }

        public Expression getExpr() {
            return expr;
        }

        public boolean isNot() {
            return not;
        }
    }

    public static class Binary extends Expression {
        private final Expression x1;
        private final Expression x2;
        private final String op;

        public Binary(Expression x1, Expression x2, String op) {
            this.x1 = x1;
            this.x2 = x2;
            this.op = op;
        }

        public Expression getX1() {
            return x1;
        }

        public Expression getX2() {
            return x2;
        }

        public String getOp() {
            return op;
        }
    }
}
