package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsSummaryPublicImage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsSummaryPublicImage entity.
 */
public interface NewsSummaryPublicImageSearchRepository extends ElasticsearchRepository<NewsSummaryPublicImage, Long> {
}
