package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.SiteChatgroupIp;
import biz.newparadigm.meridian.service.SiteChatgroupIpService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.SiteChatgroupIpCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupIpQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SiteChatgroupIp.
 */
@RestController
@RequestMapping("/api")
public class SiteChatgroupIpResource {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupIpResource.class);

    private static final String ENTITY_NAME = "siteChatgroupIp";

    private final SiteChatgroupIpService siteChatgroupIpService;

    private final SiteChatgroupIpQueryService siteChatgroupIpQueryService;

    public SiteChatgroupIpResource(SiteChatgroupIpService siteChatgroupIpService, SiteChatgroupIpQueryService siteChatgroupIpQueryService) {
        this.siteChatgroupIpService = siteChatgroupIpService;
        this.siteChatgroupIpQueryService = siteChatgroupIpQueryService;
    }

    /**
     * POST  /site-chatgroup-ips : Create a new siteChatgroupIp.
     *
     * @param siteChatgroupIp the siteChatgroupIp to create
     * @return the ResponseEntity with status 201 (Created) and with body the new siteChatgroupIp, or with status 400 (Bad Request) if the siteChatgroupIp has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/site-chatgroup-ips")
    @Timed
    public ResponseEntity<SiteChatgroupIp> createSiteChatgroupIp(@Valid @RequestBody SiteChatgroupIp siteChatgroupIp) throws URISyntaxException {
        log.debug("REST request to save SiteChatgroupIp : {}", siteChatgroupIp);
        if (siteChatgroupIp.getId() != null) {
            throw new BadRequestAlertException("A new siteChatgroupIp cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SiteChatgroupIp result = siteChatgroupIpService.save(siteChatgroupIp);
        return ResponseEntity.created(new URI("/api/site-chatgroup-ips/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /site-chatgroup-ips : Updates an existing siteChatgroupIp.
     *
     * @param siteChatgroupIp the siteChatgroupIp to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated siteChatgroupIp,
     * or with status 400 (Bad Request) if the siteChatgroupIp is not valid,
     * or with status 500 (Internal Server Error) if the siteChatgroupIp couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/site-chatgroup-ips")
    @Timed
    public ResponseEntity<SiteChatgroupIp> updateSiteChatgroupIp(@Valid @RequestBody SiteChatgroupIp siteChatgroupIp) throws URISyntaxException {
        log.debug("REST request to update SiteChatgroupIp : {}", siteChatgroupIp);
        if (siteChatgroupIp.getId() == null) {
            return createSiteChatgroupIp(siteChatgroupIp);
        }
        SiteChatgroupIp result = siteChatgroupIpService.save(siteChatgroupIp);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, siteChatgroupIp.getId().toString()))
            .body(result);
    }

    /**
     * GET  /site-chatgroup-ips : get all the siteChatgroupIps.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of siteChatgroupIps in body
     */
    @GetMapping("/site-chatgroup-ips")
    @Timed
    public ResponseEntity<List<SiteChatgroupIp>> getAllSiteChatgroupIps(SiteChatgroupIpCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get SiteChatgroupIps by criteria: {}", criteria);
        Page<SiteChatgroupIp> page = siteChatgroupIpQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/site-chatgroup-ips");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /site-chatgroup-ips/:id : get the "id" siteChatgroupIp.
     *
     * @param id the id of the siteChatgroupIp to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the siteChatgroupIp, or with status 404 (Not Found)
     */
    @GetMapping("/site-chatgroup-ips/{id}")
    @Timed
    public ResponseEntity<SiteChatgroupIp> getSiteChatgroupIp(@PathVariable Long id) {
        log.debug("REST request to get SiteChatgroupIp : {}", id);
        SiteChatgroupIp siteChatgroupIp = siteChatgroupIpService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(siteChatgroupIp));
    }

    /**
     * DELETE  /site-chatgroup-ips/:id : delete the "id" siteChatgroupIp.
     *
     * @param id the id of the siteChatgroupIp to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/site-chatgroup-ips/{id}")
    @Timed
    public ResponseEntity<Void> deleteSiteChatgroupIp(@PathVariable Long id) {
        log.debug("REST request to delete SiteChatgroupIp : {}", id);
        siteChatgroupIpService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/site-chatgroup-ips?query=:query : search for the siteChatgroupIp corresponding
     * to the query.
     *
     * @param query the query of the siteChatgroupIp search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/site-chatgroup-ips")
    @Timed
    public ResponseEntity<List<SiteChatgroupIp>> searchSiteChatgroupIps(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SiteChatgroupIps for query {}", query);
        Page<SiteChatgroupIp> page = siteChatgroupIpService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/site-chatgroup-ips");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
