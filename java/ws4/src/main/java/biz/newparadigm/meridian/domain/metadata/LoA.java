package biz.newparadigm.meridian.domain.metadata;

public class LoA {
    private String levelOfActivity;

    public String getLevelOfActivity() {
        return levelOfActivity;
    }

    public void setLevelOfActivity(String levelOfActivity) {
        this.levelOfActivity = levelOfActivity;
    }
}
