package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsSummaryToTag;
import biz.newparadigm.meridian.repository.NewsSummaryToTagRepository;
import biz.newparadigm.meridian.repository.search.NewsSummaryToTagSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsSummaryToTag.
 */
@Service
@Transactional
public class NewsSummaryToTagService {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryToTagService.class);

    private final NewsSummaryToTagRepository newsSummaryToTagRepository;

    private final NewsSummaryToTagSearchRepository newsSummaryToTagSearchRepository;

    public NewsSummaryToTagService(NewsSummaryToTagRepository newsSummaryToTagRepository, NewsSummaryToTagSearchRepository newsSummaryToTagSearchRepository) {
        this.newsSummaryToTagRepository = newsSummaryToTagRepository;
        this.newsSummaryToTagSearchRepository = newsSummaryToTagSearchRepository;
    }

    /**
     * Save a newsSummaryToTag.
     *
     * @param newsSummaryToTag the entity to save
     * @return the persisted entity
     */
    public NewsSummaryToTag save(NewsSummaryToTag newsSummaryToTag) {
        log.debug("Request to save NewsSummaryToTag : {}", newsSummaryToTag);
        NewsSummaryToTag result = newsSummaryToTagRepository.save(newsSummaryToTag);
        newsSummaryToTagSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsSummaryToTags.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryToTag> findAll(Pageable pageable) {
        log.debug("Request to get all NewsSummaryToTags");
        return newsSummaryToTagRepository.findAll(pageable);
    }

    /**
     *  Get one newsSummaryToTag by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsSummaryToTag findOne(Long id) {
        log.debug("Request to get NewsSummaryToTag : {}", id);
        return newsSummaryToTagRepository.findOne(id);
    }

    /**
     *  Delete the  newsSummaryToTag by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsSummaryToTag : {}", id);
        newsSummaryToTagRepository.delete(id);
        newsSummaryToTagSearchRepository.delete(id);
    }

    /**
     * Search for the newsSummaryToTag corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryToTag> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsSummaryToTags for query {}", query);
        Page<NewsSummaryToTag> result = newsSummaryToTagSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
