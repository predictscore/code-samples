package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsSummary;
import biz.newparadigm.meridian.repository.NewsSummaryRepository;
import biz.newparadigm.meridian.repository.search.NewsSummarySearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsSummary.
 */
@Service
@Transactional
public class NewsSummaryService {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryService.class);

    private final NewsSummaryRepository newsSummaryRepository;

    private final NewsSummarySearchRepository newsSummarySearchRepository;

    public NewsSummaryService(NewsSummaryRepository newsSummaryRepository, NewsSummarySearchRepository newsSummarySearchRepository) {
        this.newsSummaryRepository = newsSummaryRepository;
        this.newsSummarySearchRepository = newsSummarySearchRepository;
    }

    /**
     * Save a newsSummary.
     *
     * @param newsSummary the entity to save
     * @return the persisted entity
     */
    public NewsSummary save(NewsSummary newsSummary) {
        log.debug("Request to save NewsSummary : {}", newsSummary);
        NewsSummary result = newsSummaryRepository.save(newsSummary);
        newsSummarySearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsSummaries.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummary> findAll(Pageable pageable) {
        log.debug("Request to get all NewsSummaries");
        return newsSummaryRepository.findAll(pageable);
    }

    /**
     *  Get one newsSummary by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsSummary findOne(Long id) {
        log.debug("Request to get NewsSummary : {}", id);
        return newsSummaryRepository.findOne(id);
    }

    /**
     *  Delete the  newsSummary by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsSummary : {}", id);
        newsSummaryRepository.delete(id);
        newsSummarySearchRepository.delete(id);
    }

    /**
     * Search for the newsSummary corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummary> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsSummaries for query {}", query);
        Page<NewsSummary> result = newsSummarySearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
