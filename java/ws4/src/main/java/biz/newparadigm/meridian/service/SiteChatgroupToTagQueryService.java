package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.SiteChatgroupToTag;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.SiteChatgroupToTagRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupToTagSearchRepository;
import biz.newparadigm.meridian.service.dto.SiteChatgroupToTagCriteria;


/**
 * Service for executing complex queries for SiteChatgroupToTag entities in the database.
 * The main input is a {@link SiteChatgroupToTagCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link SiteChatgroupToTag} or a {@link Page} of {%link SiteChatgroupToTag} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class SiteChatgroupToTagQueryService extends QueryService<SiteChatgroupToTag> {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupToTagQueryService.class);


    private final SiteChatgroupToTagRepository siteChatgroupToTagRepository;

    private final SiteChatgroupToTagSearchRepository siteChatgroupToTagSearchRepository;

    public SiteChatgroupToTagQueryService(SiteChatgroupToTagRepository siteChatgroupToTagRepository, SiteChatgroupToTagSearchRepository siteChatgroupToTagSearchRepository) {
        this.siteChatgroupToTagRepository = siteChatgroupToTagRepository;
        this.siteChatgroupToTagSearchRepository = siteChatgroupToTagSearchRepository;
    }

    /**
     * Return a {@link List} of {%link SiteChatgroupToTag} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SiteChatgroupToTag> findByCriteria(SiteChatgroupToTagCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<SiteChatgroupToTag> specification = createSpecification(criteria);
        return siteChatgroupToTagRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link SiteChatgroupToTag} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupToTag> findByCriteria(SiteChatgroupToTagCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<SiteChatgroupToTag> specification = createSpecification(criteria);
        return siteChatgroupToTagRepository.findAll(specification, page);
    }

    /**
     * Function to convert SiteChatgroupToTagCriteria to a {@link Specifications}
     */
    private Specifications<SiteChatgroupToTag> createSpecification(SiteChatgroupToTagCriteria criteria) {
        Specifications<SiteChatgroupToTag> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SiteChatgroupToTag_.id));
            }
            if (criteria.getSiteChatgroupId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSiteChatgroupId(), SiteChatgroupToTag_.siteChatgroup, SiteChatgroup_.id));
            }
            if (criteria.getTagId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getTagId(), SiteChatgroupToTag_.tag, Tag_.id));
            }
        }
        return specification;
    }

}
