package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsOriginalImage;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsOriginalImageRepository;
import biz.newparadigm.meridian.repository.search.NewsOriginalImageSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsOriginalImageCriteria;


/**
 * Service for executing complex queries for NewsOriginalImage entities in the database.
 * The main input is a {@link NewsOriginalImageCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsOriginalImage} or a {@link Page} of {%link NewsOriginalImage} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsOriginalImageQueryService extends QueryService<NewsOriginalImage> {

    private final Logger log = LoggerFactory.getLogger(NewsOriginalImageQueryService.class);


    private final NewsOriginalImageRepository newsOriginalImageRepository;

    private final NewsOriginalImageSearchRepository newsOriginalImageSearchRepository;

    public NewsOriginalImageQueryService(NewsOriginalImageRepository newsOriginalImageRepository, NewsOriginalImageSearchRepository newsOriginalImageSearchRepository) {
        this.newsOriginalImageRepository = newsOriginalImageRepository;
        this.newsOriginalImageSearchRepository = newsOriginalImageSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsOriginalImage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsOriginalImage> findByCriteria(NewsOriginalImageCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsOriginalImage> specification = createSpecification(criteria);
        return newsOriginalImageRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsOriginalImage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsOriginalImage> findByCriteria(NewsOriginalImageCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsOriginalImage> specification = createSpecification(criteria);
        return newsOriginalImageRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsOriginalImageCriteria to a {@link Specifications}
     */
    private Specifications<NewsOriginalImage> createSpecification(NewsOriginalImageCriteria criteria) {
        Specifications<NewsOriginalImage> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsOriginalImage_.id));
            }
            if (criteria.getFilename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFilename(), NewsOriginalImage_.filename));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), NewsOriginalImage_.description));
            }
            if (criteria.getImageOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getImageOrder(), NewsOriginalImage_.imageOrder));
            }
            if (criteria.getNewsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsId(), NewsOriginalImage_.news, News_.id));
            }
        }
        return specification;
    }

}
