package biz.newparadigm.meridian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the NewsSummaryToNews entity. This class is used in NewsSummaryToNewsResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /news-summary-to-news?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NewsSummaryToNewsCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private LongFilter newsSummaryId;

    private LongFilter newsId;

    public NewsSummaryToNewsCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getNewsSummaryId() {
        return newsSummaryId;
    }

    public void setNewsSummaryId(LongFilter newsSummaryId) {
        this.newsSummaryId = newsSummaryId;
    }

    public LongFilter getNewsId() {
        return newsId;
    }

    public void setNewsId(LongFilter newsId) {
        this.newsId = newsId;
    }

    @Override
    public String toString() {
        return "NewsSummaryToNewsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (newsSummaryId != null ? "newsSummaryId=" + newsSummaryId + ", " : "") +
                (newsId != null ? "newsId=" + newsId + ", " : "") +
            "}";
    }

}
