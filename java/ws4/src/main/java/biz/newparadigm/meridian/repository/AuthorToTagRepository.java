package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.AuthorToTag;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

@SuppressWarnings("unused")
@Repository
public interface AuthorToTagRepository extends JpaRepository<AuthorToTag, Long>, JpaSpecificationExecutor<AuthorToTag> {
    @Modifying
    @Query("DELETE FROM AuthorToTag WHERE author.id = :authorId")
    public void deleteAllByAuthorId(@Param("authorId") Long authorId);

    @Modifying
    @Query("DELETE FROM AuthorToTag WHERE tag.id = :tagId")
    public void deleteAllByTagId(@Param("tagId") Long tagId);
}
