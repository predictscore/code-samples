package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.SiteChatgroupToTag;
import biz.newparadigm.meridian.service.SiteChatgroupToTagService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.SiteChatgroupToTagCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupToTagQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SiteChatgroupToTag.
 */
@RestController
@RequestMapping("/api")
public class SiteChatgroupToTagResource {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupToTagResource.class);

    private static final String ENTITY_NAME = "siteChatgroupToTag";

    private final SiteChatgroupToTagService siteChatgroupToTagService;

    private final SiteChatgroupToTagQueryService siteChatgroupToTagQueryService;

    public SiteChatgroupToTagResource(SiteChatgroupToTagService siteChatgroupToTagService, SiteChatgroupToTagQueryService siteChatgroupToTagQueryService) {
        this.siteChatgroupToTagService = siteChatgroupToTagService;
        this.siteChatgroupToTagQueryService = siteChatgroupToTagQueryService;
    }

    /**
     * POST  /site-chatgroup-to-tags : Create a new siteChatgroupToTag.
     *
     * @param siteChatgroupToTag the siteChatgroupToTag to create
     * @return the ResponseEntity with status 201 (Created) and with body the new siteChatgroupToTag, or with status 400 (Bad Request) if the siteChatgroupToTag has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/site-chatgroup-to-tags")
    @Timed
    public ResponseEntity<SiteChatgroupToTag> createSiteChatgroupToTag(@Valid @RequestBody SiteChatgroupToTag siteChatgroupToTag) throws URISyntaxException {
        log.debug("REST request to save SiteChatgroupToTag : {}", siteChatgroupToTag);
        if (siteChatgroupToTag.getId() != null) {
            throw new BadRequestAlertException("A new siteChatgroupToTag cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SiteChatgroupToTag result = siteChatgroupToTagService.save(siteChatgroupToTag);
        return ResponseEntity.created(new URI("/api/site-chatgroup-to-tags/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /site-chatgroup-to-tags : Updates an existing siteChatgroupToTag.
     *
     * @param siteChatgroupToTag the siteChatgroupToTag to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated siteChatgroupToTag,
     * or with status 400 (Bad Request) if the siteChatgroupToTag is not valid,
     * or with status 500 (Internal Server Error) if the siteChatgroupToTag couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/site-chatgroup-to-tags")
    @Timed
    public ResponseEntity<SiteChatgroupToTag> updateSiteChatgroupToTag(@Valid @RequestBody SiteChatgroupToTag siteChatgroupToTag) throws URISyntaxException {
        log.debug("REST request to update SiteChatgroupToTag : {}", siteChatgroupToTag);
        if (siteChatgroupToTag.getId() == null) {
            return createSiteChatgroupToTag(siteChatgroupToTag);
        }
        SiteChatgroupToTag result = siteChatgroupToTagService.save(siteChatgroupToTag);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, siteChatgroupToTag.getId().toString()))
            .body(result);
    }

    /**
     * GET  /site-chatgroup-to-tags : get all the siteChatgroupToTags.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of siteChatgroupToTags in body
     */
    @GetMapping("/site-chatgroup-to-tags")
    @Timed
    public ResponseEntity<List<SiteChatgroupToTag>> getAllSiteChatgroupToTags(SiteChatgroupToTagCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get SiteChatgroupToTags by criteria: {}", criteria);
        Page<SiteChatgroupToTag> page = siteChatgroupToTagQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/site-chatgroup-to-tags");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /site-chatgroup-to-tags/:id : get the "id" siteChatgroupToTag.
     *
     * @param id the id of the siteChatgroupToTag to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the siteChatgroupToTag, or with status 404 (Not Found)
     */
    @GetMapping("/site-chatgroup-to-tags/{id}")
    @Timed
    public ResponseEntity<SiteChatgroupToTag> getSiteChatgroupToTag(@PathVariable Long id) {
        log.debug("REST request to get SiteChatgroupToTag : {}", id);
        SiteChatgroupToTag siteChatgroupToTag = siteChatgroupToTagService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(siteChatgroupToTag));
    }

    /**
     * DELETE  /site-chatgroup-to-tags/:id : delete the "id" siteChatgroupToTag.
     *
     * @param id the id of the siteChatgroupToTag to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/site-chatgroup-to-tags/{id}")
    @Timed
    public ResponseEntity<Void> deleteSiteChatgroupToTag(@PathVariable Long id) {
        log.debug("REST request to delete SiteChatgroupToTag : {}", id);
        siteChatgroupToTagService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/site-chatgroup-to-tags?query=:query : search for the siteChatgroupToTag corresponding
     * to the query.
     *
     * @param query the query of the siteChatgroupToTag search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/site-chatgroup-to-tags")
    @Timed
    public ResponseEntity<List<SiteChatgroupToTag>> searchSiteChatgroupToTags(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SiteChatgroupToTags for query {}", query);
        Page<SiteChatgroupToTag> page = siteChatgroupToTagService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/site-chatgroup-to-tags");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
