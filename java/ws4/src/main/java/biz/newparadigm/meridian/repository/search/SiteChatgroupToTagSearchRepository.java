package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.SiteChatgroupToTag;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SiteChatgroupToTag entity.
 */
public interface SiteChatgroupToTagSearchRepository extends ElasticsearchRepository<SiteChatgroupToTag, Long> {
}
