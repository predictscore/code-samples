package biz.newparadigm.meridian.domain;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A NewsDownloadableContentUrl.
 */
@Entity
@Table(name = "news_downloadable_content_urls")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "newsdownloadablecontenturl")
public class NewsDownloadableContentUrl implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="news_downloadable_content_urls_id_seq")
    @SequenceGenerator(name="news_downloadable_content_urls_id_seq", sequenceName="news_downloadable_content_urls_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "url", nullable = false)
    private String url;

    @Column(name = "description")
    private String description;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"comments", "attachments", "originalImages", "publicImages", "confidentialImages", "urls",
        "downloadableContentUrls", "metadata", "tags", "languages", "newsSummaries", "metagroups", "metagroupsMap", "author", "siteChatgroup"})
    private News news;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public NewsDownloadableContentUrl url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public NewsDownloadableContentUrl description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public News getNews() {
        return news;
    }

    public NewsDownloadableContentUrl news(News news) {
        this.news = news;
        return this;
    }

    public void setNews(News news) {
        this.news = news;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NewsDownloadableContentUrl newsDownloadableContentUrl = (NewsDownloadableContentUrl) o;
        if (newsDownloadableContentUrl.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), newsDownloadableContentUrl.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NewsDownloadableContentUrl{" +
            "id=" + getId() +
            ", url='" + getUrl() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
