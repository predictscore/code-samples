package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.AuthorProfile;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AuthorProfile entity.
 */
public interface AuthorProfileSearchRepository extends ElasticsearchRepository<AuthorProfile, Long> {
}
