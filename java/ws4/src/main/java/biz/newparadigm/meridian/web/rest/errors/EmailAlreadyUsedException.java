package biz.newparadigm.meridian.web.rest.errors;

public class EmailAlreadyUsedException extends BadRequestAlertException {

    public EmailAlreadyUsedException() {
        super("Email address already in use", "userManagement", "emailexists");
    }
}
