package biz.newparadigm.meridian.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A NewsSummaryPublicImage.
 */
@Entity
@Table(name = "news_summary_public_images")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "newssummarypublicimage")
public class NewsSummaryPublicImage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="news_summary_public_images_id_seq")
    @SequenceGenerator(name="news_summary_public_images_id_seq", sequenceName="news_summary_public_images_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "filename", nullable = false)
    private String filename;

    @NotNull
    @Lob
    @Column(name = "blob", nullable = false)
    private byte[] blob;

    @Transient
//    @Column(name = "jhi_blob_content_type", nullable = false)
    private String blobContentType;

    @Column(name = "description")
    private String description;

    @Column(name = "description_translation")
    private String descriptionTranslation;

    @Column(name = "image_order")
    private Integer imageOrder;

    @ManyToOne(optional = false)
    @NotNull
    private NewsSummary newsSummary;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public NewsSummaryPublicImage filename(String filename) {
        this.filename = filename;
        return this;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public byte[] getBlob() {
        return blob;
    }

    public NewsSummaryPublicImage blob(byte[] blob) {
        this.blob = blob;
        return this;
    }

    public void setBlob(byte[] blob) {
        this.blob = blob;
    }

    public String getBlobContentType() {
        return blobContentType;
    }

    public NewsSummaryPublicImage blobContentType(String blobContentType) {
        this.blobContentType = blobContentType;
        return this;
    }

    public void setBlobContentType(String blobContentType) {
        this.blobContentType = blobContentType;
    }

    public String getDescription() {
        return description;
    }

    public NewsSummaryPublicImage description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslation() {
        return descriptionTranslation;
    }

    public NewsSummaryPublicImage descriptionTranslation(String descriptionTranslation) {
        this.descriptionTranslation = descriptionTranslation;
        return this;
    }

    public void setDescriptionTranslation(String descriptionTranslation) {
        this.descriptionTranslation = descriptionTranslation;
    }

    public Integer getImageOrder() {
        return imageOrder;
    }

    public NewsSummaryPublicImage imageOrder(Integer imageOrder) {
        this.imageOrder = imageOrder;
        return this;
    }

    public void setImageOrder(Integer imageOrder) {
        this.imageOrder = imageOrder;
    }

    public NewsSummary getNewsSummary() {
        return newsSummary;
    }

    public NewsSummaryPublicImage newsSummary(NewsSummary newsSummary) {
        this.newsSummary = newsSummary;
        return this;
    }

    public void setNewsSummary(NewsSummary newsSummary) {
        this.newsSummary = newsSummary;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NewsSummaryPublicImage newsSummaryPublicImage = (NewsSummaryPublicImage) o;
        if (newsSummaryPublicImage.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), newsSummaryPublicImage.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NewsSummaryPublicImage{" +
            "id=" + getId() +
            ", filename='" + getFilename() + "'" +
            ", blob='" + getBlob() + "'" +
            ", blobContentType='" + blobContentType + "'" +
            ", description='" + getDescription() + "'" +
            ", descriptionTranslation='" + getDescriptionTranslation() + "'" +
            ", imageOrder='" + getImageOrder() + "'" +
            "}";
    }
}
