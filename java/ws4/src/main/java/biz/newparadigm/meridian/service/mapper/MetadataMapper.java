package biz.newparadigm.meridian.service.mapper;

import biz.newparadigm.meridian.domain.Metafield;
import biz.newparadigm.meridian.domain.Metagroup;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MetadataMapper {
    public Metagroup getMetagroupByName(List<Metagroup> metagroups, String name) {
        if (metagroups != null && !metagroups.isEmpty() && name != null) {
            List<Metagroup> filtered = metagroups.stream().filter(g -> {
                String formattedName = g.getName().toLowerCase().replaceAll(" ", "");
                return formattedName.equals(name.toLowerCase());
            }).collect(Collectors.toList());
            if (filtered != null && !filtered.isEmpty()) {
                return filtered.get(0);
            }
        }
        return null;
    }

    public Metafield getMetafieldByName(List<Metafield> metafields, String name) {
        if (metafields != null && !metafields.isEmpty() && name != null) {
            List<Metafield> filtered = metafields.stream().filter(f -> {
                String formattedName = f.getName().toLowerCase().replaceAll(" ", "");
                return formattedName.equals(name.toLowerCase());
            }).collect(Collectors.toList());
            if (filtered != null && !filtered.isEmpty()) {
                return filtered.get(0);
            }
        }
        return null;
    }
}
