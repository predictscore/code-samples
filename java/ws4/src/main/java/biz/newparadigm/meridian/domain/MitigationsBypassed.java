package biz.newparadigm.meridian.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "mitigations_bypassed")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "mitigationsbypassed")
public class MitigationsBypassed implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="mitigations_bypassed_id_seq")
    @SequenceGenerator(name="mitigations_bypassed_id_seq", sequenceName="mitigations_bypassed_id_seq", allocationSize=1)
    private Long id;

    @Column(name = "description")
    private String description;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public MitigationsBypassed description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MitigationsBypassed mitigationsBypassed = (MitigationsBypassed) o;
        if (mitigationsBypassed.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), mitigationsBypassed.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MitigationsBypassed{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
