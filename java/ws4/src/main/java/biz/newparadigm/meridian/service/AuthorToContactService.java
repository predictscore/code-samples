package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.AuthorToContact;
import biz.newparadigm.meridian.repository.AuthorToContactRepository;
import biz.newparadigm.meridian.repository.search.AuthorToContactSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing AuthorToContact.
 */
@Service
@Transactional
public class AuthorToContactService {

    private final Logger log = LoggerFactory.getLogger(AuthorToContactService.class);

    private final AuthorToContactRepository authorToContactRepository;

    private final AuthorToContactSearchRepository authorToContactSearchRepository;

    public AuthorToContactService(AuthorToContactRepository authorToContactRepository, AuthorToContactSearchRepository authorToContactSearchRepository) {
        this.authorToContactRepository = authorToContactRepository;
        this.authorToContactSearchRepository = authorToContactSearchRepository;
    }

    /**
     * Save a authorToContact.
     *
     * @param authorToContact the entity to save
     * @return the persisted entity
     */
    public AuthorToContact save(AuthorToContact authorToContact) {
        log.debug("Request to save AuthorToContact : {}", authorToContact);
        AuthorToContact result = authorToContactRepository.save(authorToContact);
        authorToContactSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the authorToContacts.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuthorToContact> findAll(Pageable pageable) {
        log.debug("Request to get all AuthorToContacts");
        return authorToContactRepository.findAll(pageable);
    }

    /**
     *  Get one authorToContact by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AuthorToContact findOne(Long id) {
        log.debug("Request to get AuthorToContact : {}", id);
        return authorToContactRepository.findOne(id);
    }

    /**
     *  Delete the  authorToContact by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AuthorToContact : {}", id);
        authorToContactRepository.delete(id);
        //authorToContactSearchRepository.delete(id);
    }

    public void deleteByAuthorId(Long id) {
        authorToContactRepository.deleteAllByAuthorId(id);
        //authorToContactSearchRepository.delete(id);
    }

    /**
     * Search for the authorToContact corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuthorToContact> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of AuthorToContacts for query {}", query);
        Page<AuthorToContact> result = authorToContactSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
