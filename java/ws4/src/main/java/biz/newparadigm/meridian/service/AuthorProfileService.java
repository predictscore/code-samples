package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.AuthorProfile;
import biz.newparadigm.meridian.repository.AuthorProfileRepository;
import biz.newparadigm.meridian.repository.search.AuthorProfileSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing AuthorProfile.
 */
@Service
@Transactional
public class AuthorProfileService {

    private final Logger log = LoggerFactory.getLogger(AuthorProfileService.class);

    private final AuthorProfileRepository authorProfileRepository;

    private final AuthorProfileSearchRepository authorProfileSearchRepository;

    public AuthorProfileService(AuthorProfileRepository authorProfileRepository, AuthorProfileSearchRepository authorProfileSearchRepository) {
        this.authorProfileRepository = authorProfileRepository;
        this.authorProfileSearchRepository = authorProfileSearchRepository;
    }

    /**
     * Save a authorProfile.
     *
     * @param authorProfile the entity to save
     * @return the persisted entity
     */
    public AuthorProfile save(AuthorProfile authorProfile) {
        log.debug("Request to save AuthorProfile : {}", authorProfile);
        AuthorProfile result = authorProfileRepository.save(authorProfile);
        authorProfileSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the authorProfiles.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuthorProfile> findAll(Pageable pageable) {
        log.debug("Request to get all AuthorProfiles");
        return authorProfileRepository.findAll(pageable);
    }

    /**
     *  Get one authorProfile by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AuthorProfile findOne(Long id) {
        log.debug("Request to get AuthorProfile : {}", id);
        return authorProfileRepository.findOne(id);
    }

    /**
     *  Delete the  authorProfile by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AuthorProfile : {}", id);
        authorProfileRepository.delete(id);
        authorProfileSearchRepository.delete(id);
    }

    /**
     * Search for the authorProfile corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuthorProfile> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of AuthorProfiles for query {}", query);
        Page<AuthorProfile> result = authorProfileSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
