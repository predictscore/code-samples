package biz.newparadigm.meridian.web.rest.errors.problem;

import com.google.gag.annotation.remark.Hack;
import com.google.gag.annotation.remark.OhNoYouDidnt;
import org.zalando.problem.StatusType;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

@Immutable // TODO kind of a lie until we remove set(String, Object)
public abstract class MeridianAbstractThrowableProblem extends MeridianThrowableProblem {

    private final String type;
    private final String title;
    private final StatusType status;
    private final String detail;
    private final URI instance;
    private final Map<String, Object> parameters;

    protected MeridianAbstractThrowableProblem() {
        this(null);
    }

    protected MeridianAbstractThrowableProblem(@Nullable final String type) {
        this(type, null);
    }

    protected MeridianAbstractThrowableProblem(@Nullable final String type,
                                               @Nullable final String title) {
        this(type, title, null);
    }

    protected MeridianAbstractThrowableProblem(@Nullable final String type,
                                               @Nullable final String title,
                                               @Nullable final StatusType status) {
        this(type, title, status, null);
    }

    protected MeridianAbstractThrowableProblem(@Nullable final String type,
                                               @Nullable final String title,
                                               @Nullable final StatusType status,
                                               @Nullable final String detail) {
        this(type, title, status, detail, null);
    }

    protected MeridianAbstractThrowableProblem(@Nullable final String type,
                                               @Nullable final String title,
                                               @Nullable final StatusType status,
                                               @Nullable final String detail,
                                               @Nullable final URI instance) {
        this(type, title, status, detail, instance, null);
    }

    protected MeridianAbstractThrowableProblem(@Nullable final String type,
                                               @Nullable final String title,
                                               @Nullable final StatusType status,
                                               @Nullable final String detail,
                                               @Nullable final URI instance,
                                               @Nullable final MeridianThrowableProblem cause) {
        this(type, title, status, detail, instance, cause, null);
    }

    protected MeridianAbstractThrowableProblem(@Nullable final String type,
                                               @Nullable final String title,
                                               @Nullable final StatusType status,
                                               @Nullable final String detail,
                                               @Nullable final URI instance,
                                               @Nullable final MeridianThrowableProblem cause,
                                               @Nullable final Map<String, Object> parameters) {
        super(cause);
        this.type = Optional.ofNullable(type).orElse("Meridian WS error");
        this.title = title;
        this.status = status;
        this.detail = detail;
        this.instance = instance;
        this.parameters = Optional.ofNullable(parameters).orElseGet(LinkedHashMap::new);
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public StatusType getStatus() {
        return status;
    }

    @Override
    public String getDetail() {
        return detail;
    }

    @Override
    public URI getInstance() {
        return instance;
    }

    @Override
    public Map<String, Object> getParameters() {
        return Collections.unmodifiableMap(parameters);
    }

    /**
     * This is required to workaround missing support for {@link com.fasterxml.jackson.annotation.JsonAnySetter} on
     * constructors annotated with {@link com.fasterxml.jackson.annotation.JsonCreator}.
     *
     * @param key   the custom key
     * @param value the custom value
     * @see <a href="https://github.com/FasterXML/jackson-databind/issues/562">Jackson Issue 562</a>
     */
    @Hack
    @OhNoYouDidnt
    void set(final String key, final Object value) {
        parameters.put(key, value);
    }

}
