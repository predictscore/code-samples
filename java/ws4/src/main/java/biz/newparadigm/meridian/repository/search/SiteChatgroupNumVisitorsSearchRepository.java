package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.SiteChatgroupNumVisitors;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SiteChatgroupNumVisitors entity.
 */
public interface SiteChatgroupNumVisitorsSearchRepository extends ElasticsearchRepository<SiteChatgroupNumVisitors, Long> {
}
