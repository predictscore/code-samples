package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsSummaryComment;
import biz.newparadigm.meridian.service.NewsSummaryCommentService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsSummaryCommentCriteria;
import biz.newparadigm.meridian.service.NewsSummaryCommentQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsSummaryComment.
 */
@RestController
@RequestMapping("/api")
public class NewsSummaryCommentResource {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryCommentResource.class);

    private static final String ENTITY_NAME = "newsSummaryComment";

    private final NewsSummaryCommentService newsSummaryCommentService;

    private final NewsSummaryCommentQueryService newsSummaryCommentQueryService;

    public NewsSummaryCommentResource(NewsSummaryCommentService newsSummaryCommentService, NewsSummaryCommentQueryService newsSummaryCommentQueryService) {
        this.newsSummaryCommentService = newsSummaryCommentService;
        this.newsSummaryCommentQueryService = newsSummaryCommentQueryService;
    }

    /**
     * POST  /news-summary-comments : Create a new newsSummaryComment.
     *
     * @param newsSummaryComment the newsSummaryComment to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsSummaryComment, or with status 400 (Bad Request) if the newsSummaryComment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-summary-comments")
    @Timed
    public ResponseEntity<NewsSummaryComment> createNewsSummaryComment(@Valid @RequestBody NewsSummaryComment newsSummaryComment) throws URISyntaxException {
        log.debug("REST request to save NewsSummaryComment : {}", newsSummaryComment);
        if (newsSummaryComment.getId() != null) {
            throw new BadRequestAlertException("A new newsSummaryComment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsSummaryComment result = newsSummaryCommentService.save(newsSummaryComment);
        return ResponseEntity.created(new URI("/api/news-summary-comments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-summary-comments : Updates an existing newsSummaryComment.
     *
     * @param newsSummaryComment the newsSummaryComment to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsSummaryComment,
     * or with status 400 (Bad Request) if the newsSummaryComment is not valid,
     * or with status 500 (Internal Server Error) if the newsSummaryComment couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-summary-comments")
    @Timed
    public ResponseEntity<NewsSummaryComment> updateNewsSummaryComment(@Valid @RequestBody NewsSummaryComment newsSummaryComment) throws URISyntaxException {
        log.debug("REST request to update NewsSummaryComment : {}", newsSummaryComment);
        if (newsSummaryComment.getId() == null) {
            return createNewsSummaryComment(newsSummaryComment);
        }
        NewsSummaryComment result = newsSummaryCommentService.save(newsSummaryComment);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsSummaryComment.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-summary-comments : get all the newsSummaryComments.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsSummaryComments in body
     */
    @GetMapping("/news-summary-comments")
    @Timed
    public ResponseEntity<List<NewsSummaryComment>> getAllNewsSummaryComments(NewsSummaryCommentCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsSummaryComments by criteria: {}", criteria);
        Page<NewsSummaryComment> page = newsSummaryCommentQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-summary-comments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-summary-comments/:id : get the "id" newsSummaryComment.
     *
     * @param id the id of the newsSummaryComment to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsSummaryComment, or with status 404 (Not Found)
     */
    @GetMapping("/news-summary-comments/{id}")
    @Timed
    public ResponseEntity<NewsSummaryComment> getNewsSummaryComment(@PathVariable Long id) {
        log.debug("REST request to get NewsSummaryComment : {}", id);
        NewsSummaryComment newsSummaryComment = newsSummaryCommentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsSummaryComment));
    }

    /**
     * DELETE  /news-summary-comments/:id : delete the "id" newsSummaryComment.
     *
     * @param id the id of the newsSummaryComment to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-summary-comments/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsSummaryComment(@PathVariable Long id) {
        log.debug("REST request to delete NewsSummaryComment : {}", id);
        newsSummaryCommentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-summary-comments?query=:query : search for the newsSummaryComment corresponding
     * to the query.
     *
     * @param query the query of the newsSummaryComment search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-summary-comments")
    @Timed
    public ResponseEntity<List<NewsSummaryComment>> searchNewsSummaryComments(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsSummaryComments for query {}", query);
        Page<NewsSummaryComment> page = newsSummaryCommentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-summary-comments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
