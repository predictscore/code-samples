package biz.newparadigm.meridian.service.dto.old;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class SiteChatgroupNumVisitorDTO {
    private Long siteChatGroupId = -1L;
    private int numVisitors;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.dateWithTimeFormat)
    private Date numVisitorDate;

    private String numVisitorDateString;

    public String getNumVisitorDateString() {
        return numVisitorDateString;
    }

    public void setNumVisitorDateString(String numVisitorDateString) {
        this.numVisitorDateString = numVisitorDateString;
    }

    public Long getSiteChatGroupId() {
        return siteChatGroupId;
    }

    public void setSiteChatGroupId(Long siteChatGroupId) {
        this.siteChatGroupId = siteChatGroupId;
    }

    public int getNumVisitors() {
        return numVisitors;
    }

    public void setNumVisitors(int numVisitors) {
        this.numVisitors = numVisitors;
    }

    public Date getNumVisitorDate() {
        return numVisitorDate;
    }

    public void setNumVisitorDate(Date numVisitorDate) {
        this.numVisitorDate = numVisitorDate;
    }
}
