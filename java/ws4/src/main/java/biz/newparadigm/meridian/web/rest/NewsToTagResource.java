package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsToTag;
import biz.newparadigm.meridian.service.NewsToTagService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsToTagCriteria;
import biz.newparadigm.meridian.service.NewsToTagQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsToTag.
 */
@RestController
@RequestMapping("/api")
public class NewsToTagResource {

    private final Logger log = LoggerFactory.getLogger(NewsToTagResource.class);

    private static final String ENTITY_NAME = "newsToTag";

    private final NewsToTagService newsToTagService;

    private final NewsToTagQueryService newsToTagQueryService;

    public NewsToTagResource(NewsToTagService newsToTagService, NewsToTagQueryService newsToTagQueryService) {
        this.newsToTagService = newsToTagService;
        this.newsToTagQueryService = newsToTagQueryService;
    }

    /**
     * POST  /news-to-tags : Create a new newsToTag.
     *
     * @param newsToTag the newsToTag to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsToTag, or with status 400 (Bad Request) if the newsToTag has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-to-tags")
    @Timed
    public ResponseEntity<NewsToTag> createNewsToTag(@Valid @RequestBody NewsToTag newsToTag) throws URISyntaxException {
        log.debug("REST request to save NewsToTag : {}", newsToTag);
        if (newsToTag.getId() != null) {
            throw new BadRequestAlertException("A new newsToTag cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsToTag result = newsToTagService.save(newsToTag);
        return ResponseEntity.created(new URI("/api/news-to-tags/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-to-tags : Updates an existing newsToTag.
     *
     * @param newsToTag the newsToTag to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsToTag,
     * or with status 400 (Bad Request) if the newsToTag is not valid,
     * or with status 500 (Internal Server Error) if the newsToTag couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-to-tags")
    @Timed
    public ResponseEntity<NewsToTag> updateNewsToTag(@Valid @RequestBody NewsToTag newsToTag) throws URISyntaxException {
        log.debug("REST request to update NewsToTag : {}", newsToTag);
        if (newsToTag.getId() == null) {
            return createNewsToTag(newsToTag);
        }
        NewsToTag result = newsToTagService.save(newsToTag);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsToTag.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-to-tags : get all the newsToTags.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsToTags in body
     */
    @GetMapping("/news-to-tags")
    @Timed
    public ResponseEntity<List<NewsToTag>> getAllNewsToTags(NewsToTagCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsToTags by criteria: {}", criteria);
        Page<NewsToTag> page = newsToTagQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-to-tags");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-to-tags/:id : get the "id" newsToTag.
     *
     * @param id the id of the newsToTag to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsToTag, or with status 404 (Not Found)
     */
    @GetMapping("/news-to-tags/{id}")
    @Timed
    public ResponseEntity<NewsToTag> getNewsToTag(@PathVariable Long id) {
        log.debug("REST request to get NewsToTag : {}", id);
        NewsToTag newsToTag = newsToTagService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsToTag));
    }

    /**
     * DELETE  /news-to-tags/:id : delete the "id" newsToTag.
     *
     * @param id the id of the newsToTag to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-to-tags/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsToTag(@PathVariable Long id) {
        log.debug("REST request to delete NewsToTag : {}", id);
        newsToTagService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-to-tags?query=:query : search for the newsToTag corresponding
     * to the query.
     *
     * @param query the query of the newsToTag search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-to-tags")
    @Timed
    public ResponseEntity<List<NewsToTag>> searchNewsToTags(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsToTags for query {}", query);
        Page<NewsToTag> page = newsToTagService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-to-tags");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
