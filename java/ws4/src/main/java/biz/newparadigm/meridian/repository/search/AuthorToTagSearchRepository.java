package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.AuthorToTag;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AuthorToTag entity.
 */
public interface AuthorToTagSearchRepository extends ElasticsearchRepository<AuthorToTag, Long> {
}
