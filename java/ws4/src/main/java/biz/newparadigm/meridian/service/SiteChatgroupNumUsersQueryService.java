package biz.newparadigm.meridian.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.SiteChatgroupNumUsers;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.SiteChatgroupNumUsersRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupNumUsersSearchRepository;
import biz.newparadigm.meridian.service.dto.SiteChatgroupNumUsersCriteria;


/**
 * Service for executing complex queries for SiteChatgroupNumUsers entities in the database.
 * The main input is a {@link SiteChatgroupNumUsersCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link SiteChatgroupNumUsers} or a {@link Page} of {%link SiteChatgroupNumUsers} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class SiteChatgroupNumUsersQueryService extends QueryService<SiteChatgroupNumUsers> {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupNumUsersQueryService.class);


    private final SiteChatgroupNumUsersRepository siteChatgroupNumUsersRepository;

    private final SiteChatgroupNumUsersSearchRepository siteChatgroupNumUsersSearchRepository;

    public SiteChatgroupNumUsersQueryService(SiteChatgroupNumUsersRepository siteChatgroupNumUsersRepository, SiteChatgroupNumUsersSearchRepository siteChatgroupNumUsersSearchRepository) {
        this.siteChatgroupNumUsersRepository = siteChatgroupNumUsersRepository;
        this.siteChatgroupNumUsersSearchRepository = siteChatgroupNumUsersSearchRepository;
    }

    /**
     * Return a {@link List} of {%link SiteChatgroupNumUsers} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SiteChatgroupNumUsers> findByCriteria(SiteChatgroupNumUsersCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<SiteChatgroupNumUsers> specification = createSpecification(criteria);
        return siteChatgroupNumUsersRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link SiteChatgroupNumUsers} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupNumUsers> findByCriteria(SiteChatgroupNumUsersCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<SiteChatgroupNumUsers> specification = createSpecification(criteria);
        return siteChatgroupNumUsersRepository.findAll(specification, page);
    }

    /**
     * Function to convert SiteChatgroupNumUsersCriteria to a {@link Specifications}
     */
    private Specifications<SiteChatgroupNumUsers> createSpecification(SiteChatgroupNumUsersCriteria criteria) {
        Specifications<SiteChatgroupNumUsers> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SiteChatgroupNumUsers_.id));
            }
            if (criteria.getNumUsers() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumUsers(), SiteChatgroupNumUsers_.numUsers));
            }
            if (criteria.getNumUserDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumUserDate(), SiteChatgroupNumUsers_.numUserDate));
            }
            if (criteria.getSiteChatgroupId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSiteChatgroupId(), SiteChatgroupNumUsers_.siteChatgroup, SiteChatgroup_.id));
            }
        }
        return specification;
    }

}
