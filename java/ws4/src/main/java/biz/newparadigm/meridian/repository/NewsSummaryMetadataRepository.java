package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsSummaryMetadata;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsSummaryMetadata entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsSummaryMetadataRepository extends JpaRepository<NewsSummaryMetadata, Long>, JpaSpecificationExecutor<NewsSummaryMetadata> {

}
