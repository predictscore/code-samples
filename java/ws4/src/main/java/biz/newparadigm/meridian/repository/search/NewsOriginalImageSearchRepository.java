package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsOriginalImage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsOriginalImage entity.
 */
public interface NewsOriginalImageSearchRepository extends ElasticsearchRepository<NewsOriginalImage, Long> {
}
