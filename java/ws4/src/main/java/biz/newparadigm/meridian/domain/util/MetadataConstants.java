package biz.newparadigm.meridian.domain.util;

public class MetadataConstants {
    public static final String METAGROUP_FOR_PROFIT = "ForProfit";
    public static final String METAGROUP_EVENT = "Event";
    public static final String METAGROUP_COMPANY = "Company";
    public static final String METAGROUP_LOA = "LoA";
    public static final String METAGROUP_MMVAR = "MMvar";
    public static final String METAGROUP_PROD_OR_SERV = "ProdOrServ";
    public static final String METAGROUP_VULNERABILITY = "Vulnerability";
    public static final String METAGROUP_FF = "FF";

    public static final String METAFIELD_TYPICAL_USERS = "TypicalUsers";
    public static final String METAFIELD_PRICE_ORIGINAL = "PriceOriginal";
    public static final String METAFIELD_PRICE_ENGLISH = "PriceEnglish";
    public static final String METAFIELD_EVENT = "Event";
    public static final String METAFIELD_COMPANY = "Company";
    public static final String METAFIELD_LEVEL_OF_ACTIVITY = "LevelofActivity";
    public static final String METAFIELD_ASSOCIATES_PROFILES_NAMES = "AssociatesProfilesNames";
    public static final String METAFIELD_OFFER_TYPE = "OfferType";
    public static final String METAFIELD_ANALYSIS = "Analysis";
    public static final String METAFIELD_RFI_RESPONSE = "RFIResponse";
    public static final String METAFIELD_CATEGORY = "Category";
    public static final String METAFIELD_CONFIDENCE_LEVEL = "ConfidenceLevel";
    public static final String METAFIELD_CONFIDENCE_LEVEL_EXPLANATION = "ConfidenceLevelExplanation";
    public static final String METAFIELD_LEVEL_OF_IMPACT = "LevelofImpact";
    public static final String METAFIELD_LEVEL_OF_IMPACT_EXPLANATION = "LevelofImpactExplanation";
    public static final String METAFIELD_PRODUCT_SERVICE_NAME_ORIGINAL = "Product(Service)NameOriginal";
    public static final String METAFIELD_PRODUCT_SERVICE_NAME_ENGLISH = "Product(Service)NameEnglish";
    public static final String METAFIELD_PRODUCT_SERVICE_VERSION_ORIGINAL = "Product(Service)VersionOriginal";
    public static final String METAFIELD_PRODUCT_SERVICE_VERSION_ENGLISH = "Product(Service)VersionEnglish";
    public static final String METAFIELD_PRODUCT_SERVICE_LANGUAGE = "Product(Service)Language";
    public static final String METAFIELD_MITIGAATIOMS_BYPASSED = "MitigationsBypassed";
    public static final String METAFIELD_CVE = "CVE";
    public static final String METAFIELD_FF_CATEGOTY = "FF Category";
    public static final String METAFIELD_SIZE = "Size";
    public static final String METAFIELD_RATE = "Rate";
    public static final String METAFIELD_REGION = "Region";
    public static final String METAFIELD_DATA_TYPE = "DataType";
}
