package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsComment;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsComment entity.
 */
public interface NewsCommentSearchRepository extends ElasticsearchRepository<NewsComment, Long> {
}
