package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsSummaryComment;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsSummaryCommentRepository;
import biz.newparadigm.meridian.repository.search.NewsSummaryCommentSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsSummaryCommentCriteria;


/**
 * Service for executing complex queries for NewsSummaryComment entities in the database.
 * The main input is a {@link NewsSummaryCommentCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsSummaryComment} or a {@link Page} of {%link NewsSummaryComment} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsSummaryCommentQueryService extends QueryService<NewsSummaryComment> {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryCommentQueryService.class);


    private final NewsSummaryCommentRepository newsSummaryCommentRepository;

    private final NewsSummaryCommentSearchRepository newsSummaryCommentSearchRepository;

    public NewsSummaryCommentQueryService(NewsSummaryCommentRepository newsSummaryCommentRepository, NewsSummaryCommentSearchRepository newsSummaryCommentSearchRepository) {
        this.newsSummaryCommentRepository = newsSummaryCommentRepository;
        this.newsSummaryCommentSearchRepository = newsSummaryCommentSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsSummaryComment} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsSummaryComment> findByCriteria(NewsSummaryCommentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsSummaryComment> specification = createSpecification(criteria);
        return newsSummaryCommentRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsSummaryComment} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryComment> findByCriteria(NewsSummaryCommentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsSummaryComment> specification = createSpecification(criteria);
        return newsSummaryCommentRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsSummaryCommentCriteria to a {@link Specifications}
     */
    private Specifications<NewsSummaryComment> createSpecification(NewsSummaryCommentCriteria criteria) {
        Specifications<NewsSummaryComment> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsSummaryComment_.id));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUserId(), NewsSummaryComment_.userId));
            }
            if (criteria.getComment() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComment(), NewsSummaryComment_.comment));
            }
            if (criteria.getNewsSummaryId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsSummaryId(), NewsSummaryComment_.newsSummary, NewsSummary_.id));
            }
            if (criteria.getParentCommentId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getParentCommentId(), NewsSummaryComment_.parentComment, NewsSummaryComment_.id));
            }
        }
        return specification;
    }

}
