package biz.newparadigm.meridian.service;


import java.util.List;

import biz.newparadigm.meridian.service.filter.SpecificationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.Location;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.LocationRepository;
import biz.newparadigm.meridian.repository.search.LocationSearchRepository;
import biz.newparadigm.meridian.service.dto.LocationCriteria;


/**
 * Service for executing complex queries for Location entities in the database.
 * The main input is a {@link LocationCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link Location} or a {@link Page} of {%link Location} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class LocationQueryService extends QueryService<Location> {

    private final Logger log = LoggerFactory.getLogger(LocationQueryService.class);


    private final LocationRepository locationRepository;

    private final LocationSearchRepository locationSearchRepository;

    public LocationQueryService(LocationRepository locationRepository, LocationSearchRepository locationSearchRepository) {
        this.locationRepository = locationRepository;
        this.locationSearchRepository = locationSearchRepository;
    }

    /**
     * Return a {@link List} of {%link Location} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Location> findByCriteria(LocationCriteria criteria, boolean filterByLocation, List<String> locationNames) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Location> specification = createSpecification(criteria, filterByLocation, locationNames);
        return locationRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link Location} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Location> findByCriteria(LocationCriteria criteria, Pageable page, boolean filterByLocation, List<String> locationNames) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Location> specification = createSpecification(criteria, filterByLocation, locationNames);
        return locationRepository.findAll(specification, page);
    }

    /**
     * Function to convert LocationCriteria to a {@link Specifications}
     */
    private Specifications<Location> createSpecification(LocationCriteria criteria, boolean filterByLocation, List<String> locationNames) {
        SpecificationBuilder<Location> builder = new SpecificationBuilder<>();
        Specifications<Location> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Location_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Location_.name));
            }
            if (filterByLocation) {
                if (locationNames == null || locationNames.isEmpty()) {
                    specification = specification.and(builder.buildNoResultSpecification(Location_.id));
                } else {
                    specification = specification.and(builder.buildLocationsSpecificationForLocation(locationNames));
                }
            }
        }
        return specification;
    }

}
