package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.RFIResponse;
import biz.newparadigm.meridian.repository.RFIResponseRepository;
import biz.newparadigm.meridian.repository.search.RFIResponseSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing RFIResponse.
 */
@Service
@Transactional
public class RFIResponseService {

    private final Logger log = LoggerFactory.getLogger(RFIResponseService.class);

    private final RFIResponseRepository rFIResponseRepository;

    private final RFIResponseSearchRepository rFIResponseSearchRepository;

    public RFIResponseService(RFIResponseRepository rFIResponseRepository, RFIResponseSearchRepository rFIResponseSearchRepository) {
        this.rFIResponseRepository = rFIResponseRepository;
        this.rFIResponseSearchRepository = rFIResponseSearchRepository;
    }

    /**
     * Save a rFIResponse.
     *
     * @param rFIResponse the entity to save
     * @return the persisted entity
     */
    public RFIResponse save(RFIResponse rFIResponse) {
        log.debug("Request to save RFIResponse : {}", rFIResponse);
        RFIResponse result = rFIResponseRepository.save(rFIResponse);
        rFIResponseSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the rFIResponses.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RFIResponse> findAll(Pageable pageable) {
        log.debug("Request to get all RFIResponses");
        return rFIResponseRepository.findAll(pageable);
    }

    /**
     *  Get one rFIResponse by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public RFIResponse findOne(Long id) {
        log.debug("Request to get RFIResponse : {}", id);
        return rFIResponseRepository.findOne(id);
    }

    /**
     *  Delete the  rFIResponse by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete RFIResponse : {}", id);
        rFIResponseRepository.delete(id);
        rFIResponseSearchRepository.delete(id);
    }

    /**
     * Search for the rFIResponse corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RFIResponse> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of RFIResponses for query {}", query);
        Page<RFIResponse> result = rFIResponseSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
