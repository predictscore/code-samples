package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.SiteChatgroupProfile;
import biz.newparadigm.meridian.repository.SiteChatgroupProfileRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupProfileSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SiteChatgroupProfile.
 */
@Service
@Transactional
public class SiteChatgroupProfileService {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupProfileService.class);

    private final SiteChatgroupProfileRepository siteChatgroupProfileRepository;

    private final SiteChatgroupProfileSearchRepository siteChatgroupProfileSearchRepository;

    public SiteChatgroupProfileService(SiteChatgroupProfileRepository siteChatgroupProfileRepository, SiteChatgroupProfileSearchRepository siteChatgroupProfileSearchRepository) {
        this.siteChatgroupProfileRepository = siteChatgroupProfileRepository;
        this.siteChatgroupProfileSearchRepository = siteChatgroupProfileSearchRepository;
    }

    /**
     * Save a siteChatgroupProfile.
     *
     * @param siteChatgroupProfile the entity to save
     * @return the persisted entity
     */
    public SiteChatgroupProfile save(SiteChatgroupProfile siteChatgroupProfile) {
        log.debug("Request to save SiteChatgroupProfile : {}", siteChatgroupProfile);
        SiteChatgroupProfile result = siteChatgroupProfileRepository.save(siteChatgroupProfile);
        siteChatgroupProfileSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the siteChatgroupProfiles.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupProfile> findAll(Pageable pageable) {
        log.debug("Request to get all SiteChatgroupProfiles");
        return siteChatgroupProfileRepository.findAll(pageable);
    }

    /**
     *  Get one siteChatgroupProfile by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SiteChatgroupProfile findOne(Long id) {
        log.debug("Request to get SiteChatgroupProfile : {}", id);
        return siteChatgroupProfileRepository.findOne(id);
    }

    /**
     *  Delete the  siteChatgroupProfile by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SiteChatgroupProfile : {}", id);
        siteChatgroupProfileRepository.delete(id);
        siteChatgroupProfileSearchRepository.delete(id);
    }

    /**
     * Search for the siteChatgroupProfile corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupProfile> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SiteChatgroupProfiles for query {}", query);
        Page<SiteChatgroupProfile> result = siteChatgroupProfileSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
