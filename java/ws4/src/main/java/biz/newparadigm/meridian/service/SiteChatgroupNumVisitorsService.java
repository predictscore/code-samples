package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.SiteChatgroupNumVisitors;
import biz.newparadigm.meridian.repository.SiteChatgroupNumVisitorsRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupNumVisitorsSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SiteChatgroupNumVisitors.
 */
@Service
@Transactional
public class SiteChatgroupNumVisitorsService {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupNumVisitorsService.class);

    private final SiteChatgroupNumVisitorsRepository siteChatgroupNumVisitorsRepository;

    private final SiteChatgroupNumVisitorsSearchRepository siteChatgroupNumVisitorsSearchRepository;

    public SiteChatgroupNumVisitorsService(SiteChatgroupNumVisitorsRepository siteChatgroupNumVisitorsRepository, SiteChatgroupNumVisitorsSearchRepository siteChatgroupNumVisitorsSearchRepository) {
        this.siteChatgroupNumVisitorsRepository = siteChatgroupNumVisitorsRepository;
        this.siteChatgroupNumVisitorsSearchRepository = siteChatgroupNumVisitorsSearchRepository;
    }

    /**
     * Save a siteChatgroupNumVisitors.
     *
     * @param siteChatgroupNumVisitors the entity to save
     * @return the persisted entity
     */
    public SiteChatgroupNumVisitors save(SiteChatgroupNumVisitors siteChatgroupNumVisitors) {
        log.debug("Request to save SiteChatgroupNumVisitors : {}", siteChatgroupNumVisitors);
        SiteChatgroupNumVisitors result = siteChatgroupNumVisitorsRepository.save(siteChatgroupNumVisitors);
        siteChatgroupNumVisitorsSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the siteChatgroupNumVisitors.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupNumVisitors> findAll(Pageable pageable) {
        log.debug("Request to get all SiteChatgroupNumVisitors");
        return siteChatgroupNumVisitorsRepository.findAll(pageable);
    }

    /**
     *  Get one siteChatgroupNumVisitors by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SiteChatgroupNumVisitors findOne(Long id) {
        log.debug("Request to get SiteChatgroupNumVisitors : {}", id);
        return siteChatgroupNumVisitorsRepository.findOne(id);
    }

    /**
     *  Delete the  siteChatgroupNumVisitors by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SiteChatgroupNumVisitors : {}", id);
        siteChatgroupNumVisitorsRepository.delete(id);
        siteChatgroupNumVisitorsSearchRepository.delete(id);
    }

    /**
     * Search for the siteChatgroupNumVisitors corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupNumVisitors> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SiteChatgroupNumVisitors for query {}", query);
        Page<SiteChatgroupNumVisitors> result = siteChatgroupNumVisitorsSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
