package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.AuthorToTag;
import biz.newparadigm.meridian.repository.AuthorToTagRepository;
import biz.newparadigm.meridian.repository.search.AuthorToTagSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

@Service
@Transactional
public class AuthorToTagService {

    private final Logger log = LoggerFactory.getLogger(AuthorToTagService.class);

    private final AuthorToTagRepository authorToTagRepository;

    private final AuthorToTagSearchRepository authorToTagSearchRepository;

    public AuthorToTagService(AuthorToTagRepository authorToTagRepository, AuthorToTagSearchRepository authorToTagSearchRepository) {
        this.authorToTagRepository = authorToTagRepository;
        this.authorToTagSearchRepository = authorToTagSearchRepository;
    }

    /**
     * Save a authorToTag.
     *
     * @param authorToTag the entity to save
     * @return the persisted entity
     */
    public AuthorToTag save(AuthorToTag authorToTag) {
        log.debug("Request to save AuthorToTag : {}", authorToTag);
        AuthorToTag result = authorToTagRepository.save(authorToTag);
        authorToTagSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the authorToTags.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuthorToTag> findAll(Pageable pageable) {
        log.debug("Request to get all AuthorToTags");
        return authorToTagRepository.findAll(pageable);
    }

    /**
     *  Get one authorToTag by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AuthorToTag findOne(Long id) {
        log.debug("Request to get AuthorToTag : {}", id);
        return authorToTagRepository.findOne(id);
    }

    /**
     *  Delete the  authorToTag by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AuthorToTag : {}", id);
        authorToTagRepository.delete(id);
        authorToTagSearchRepository.delete(id);
    }

    public void deleteByAuthorId(Long authorId) {
        log.debug("Request to delete AuthorToTag by author id : {}", authorId);
        authorToTagRepository.deleteAllByAuthorId(authorId);
    }

    public void deleteByTagId(Long tagId) {
        log.debug("Request to delete AuthorToTag by tag id : {}", tagId);
        authorToTagRepository.deleteAllByTagId(tagId);
    }

    /**
     * Search for the authorToTag corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuthorToTag> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of AuthorToTags for query {}", query);
        Page<AuthorToTag> result = authorToTagSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
