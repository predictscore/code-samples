package biz.newparadigm.meridian.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A NewsSummaryToNews.
 */
@Entity
@Table(name = "news_summary_to_news")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "newssummarytonews")
public class NewsSummaryToNews implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="news_summary_to_news_id_seq")
    @SequenceGenerator(name="news_summary_to_news_id_seq", sequenceName="news_summary_to_news_id_seq", allocationSize=1)
    private Long id;

    @ManyToOne(optional = false)
    @NotNull
    private NewsSummary newsSummary;

    @ManyToOne(optional = false)
    @NotNull
//    @JsonBackReference(value = "news-newsSummaries")
    @JsonIgnoreProperties({"comments", "attachments", "originalImages", "publicImages", "confidentialImages", "urls",
        "downloadableContentUrls", "metadata", "tags", "languages", "newsSummaries", "metagroups", "author", "siteChatgroup"})
    private News news;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NewsSummary getNewsSummary() {
        return newsSummary;
    }

    public NewsSummaryToNews newsSummary(NewsSummary newsSummary) {
        this.newsSummary = newsSummary;
        return this;
    }

    public void setNewsSummary(NewsSummary newsSummary) {
        this.newsSummary = newsSummary;
    }

    public News getNews() {
        return news;
    }

    public NewsSummaryToNews news(News news) {
        this.news = news;
        return this;
    }

    public void setNews(News news) {
        this.news = news;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NewsSummaryToNews newsSummaryToNews = (NewsSummaryToNews) o;
        if (newsSummaryToNews.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), newsSummaryToNews.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NewsSummaryToNews{" +
            "id=" + getId() +
            "}";
    }
}
