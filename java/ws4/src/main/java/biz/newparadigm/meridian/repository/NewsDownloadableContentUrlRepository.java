package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsDownloadableContentUrl;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsDownloadableContentUrl entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsDownloadableContentUrlRepository extends JpaRepository<NewsDownloadableContentUrl, Long>, JpaSpecificationExecutor<NewsDownloadableContentUrl> {

}
