package biz.newparadigm.meridian.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FieldsUtil {
    private static final Logger log = LoggerFactory.getLogger(FieldsUtil.class);

    public static void selectFields(String fieldsToSelect, Object obj) {
        try {
            if (fieldsToSelect != null && !fieldsToSelect.isEmpty()) {
                List<String> fieldsList = getFieldsList(fieldsToSelect);
                Field[] fields = obj.getClass().getDeclaredFields();
                for (Field f : fields) {
                    if (!fieldsList.contains(f.getName().trim()) && !f.getName().equalsIgnoreCase("serialVersionUID")) {
                        cleanFieldValue(f, obj);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error selectFields", e);
        }
    }

    public static void ignoreFields(String fieldsToIgnore, Object obj) {
        try {
            if (fieldsToIgnore != null && !fieldsToIgnore.isEmpty()) {
                List<String> fieldsList = getFieldsList(fieldsToIgnore);
                Field[] fields = obj.getClass().getDeclaredFields();
                for (Field f : fields) {
                    if (fieldsList.contains(f.getName().trim())) {
                        cleanFieldValue(f, obj);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error ignoreFields", e);
        }
    }

    private static List<String> getFieldsList(String fieldsStr) {
        List<String> result = new ArrayList<>();
        try {
            result = Arrays.asList(fieldsStr.split(","));
            result.forEach(s -> s = s.trim());
        } catch (Exception e) {
            log.error("Error converting string to list", e);
        }
        return result;
    }

    private static void cleanFieldValue(Field f, Object obj) {
        try {
            f.setAccessible(true);
            f.set(obj, f.getType().isPrimitive() ? 0 : null);
        } catch (Exception e) {
            log.error("Error cleaning field {" + f.getName() + "}", e);
        }
    }
}
