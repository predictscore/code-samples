package biz.newparadigm.meridian.service.util;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DateTimeUtil {
    public static final String DATE_FORMAT_JAVA_DATE = "EEE MMM dd HH:mm:ss zzz yyyy";
    public static final String DATE_FORMAT_PURE_POSTGRES = "yyyy-MM-dd HH:mm:ss";
    public static final Locale DEFAULT_LOCALE = Locale.ENGLISH;

    public static String toString(ZonedDateTime dateTime, String format, String appender) {
        try {
            ZonedDateTime utc = dateTime.withZoneSameInstant(ZoneId.of("UTC"));
            return utc.format(DateTimeFormatter.ofPattern(format, DEFAULT_LOCALE)) + (appender == null ? "" : appender);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
