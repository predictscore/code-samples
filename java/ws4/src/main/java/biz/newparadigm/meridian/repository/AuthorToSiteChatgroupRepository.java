package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.AuthorToSiteChatgroup;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AuthorToSiteChatgroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuthorToSiteChatgroupRepository extends JpaRepository<AuthorToSiteChatgroup, Long>, JpaSpecificationExecutor<AuthorToSiteChatgroup> {

}
