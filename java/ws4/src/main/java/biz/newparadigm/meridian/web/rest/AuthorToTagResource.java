package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.AuthorToTag;
import biz.newparadigm.meridian.service.AuthorToTagService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.AuthorToTagCriteria;
import biz.newparadigm.meridian.service.AuthorToTagQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AuthorToTag.
 */
@RestController
@RequestMapping("/api")
public class AuthorToTagResource {

    private final Logger log = LoggerFactory.getLogger(AuthorToTagResource.class);

    private static final String ENTITY_NAME = "authorToTag";

    private final AuthorToTagService authorToTagService;

    private final AuthorToTagQueryService authorToTagQueryService;

    public AuthorToTagResource(AuthorToTagService authorToTagService, AuthorToTagQueryService authorToTagQueryService) {
        this.authorToTagService = authorToTagService;
        this.authorToTagQueryService = authorToTagQueryService;
    }

    /**
     * POST  /author-to-tags : Create a new authorToTag.
     *
     * @param authorToTag the authorToTag to create
     * @return the ResponseEntity with status 201 (Created) and with body the new authorToTag, or with status 400 (Bad Request) if the authorToTag has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/author-to-tags")
    @Timed
    public ResponseEntity<AuthorToTag> createAuthorToTag(@Valid @RequestBody AuthorToTag authorToTag) throws URISyntaxException {
        log.debug("REST request to save AuthorToTag : {}", authorToTag);
        if (authorToTag.getId() != null) {
            throw new BadRequestAlertException("A new authorToTag cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuthorToTag result = authorToTagService.save(authorToTag);
        return ResponseEntity.created(new URI("/api/author-to-tags/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /author-to-tags : Updates an existing authorToTag.
     *
     * @param authorToTag the authorToTag to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated authorToTag,
     * or with status 400 (Bad Request) if the authorToTag is not valid,
     * or with status 500 (Internal Server Error) if the authorToTag couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/author-to-tags")
    @Timed
    public ResponseEntity<AuthorToTag> updateAuthorToTag(@Valid @RequestBody AuthorToTag authorToTag) throws URISyntaxException {
        log.debug("REST request to update AuthorToTag : {}", authorToTag);
        if (authorToTag.getId() == null) {
            return createAuthorToTag(authorToTag);
        }
        AuthorToTag result = authorToTagService.save(authorToTag);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, authorToTag.getId().toString()))
            .body(result);
    }

    /**
     * GET  /author-to-tags : get all the authorToTags.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of authorToTags in body
     */
    @GetMapping("/author-to-tags")
    @Timed
    public ResponseEntity<List<AuthorToTag>> getAllAuthorToTags(AuthorToTagCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get AuthorToTags by criteria: {}", criteria);
        Page<AuthorToTag> page = authorToTagQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/author-to-tags");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /author-to-tags/:id : get the "id" authorToTag.
     *
     * @param id the id of the authorToTag to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the authorToTag, or with status 404 (Not Found)
     */
    @GetMapping("/author-to-tags/{id}")
    @Timed
    public ResponseEntity<AuthorToTag> getAuthorToTag(@PathVariable Long id) {
        log.debug("REST request to get AuthorToTag : {}", id);
        AuthorToTag authorToTag = authorToTagService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(authorToTag));
    }

    /**
     * DELETE  /author-to-tags/:id : delete the "id" authorToTag.
     *
     * @param id the id of the authorToTag to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/author-to-tags/{id}")
    @Timed
    public ResponseEntity<Void> deleteAuthorToTag(@PathVariable Long id) {
        log.debug("REST request to delete AuthorToTag : {}", id);
        authorToTagService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/author-to-tags?query=:query : search for the authorToTag corresponding
     * to the query.
     *
     * @param query the query of the authorToTag search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/author-to-tags")
    @Timed
    public ResponseEntity<List<AuthorToTag>> searchAuthorToTags(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of AuthorToTags for query {}", query);
        Page<AuthorToTag> page = authorToTagService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/author-to-tags");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
