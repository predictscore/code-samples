package biz.newparadigm.meridian.domain;

import biz.newparadigm.meridian.domain.util.MeridianDateSerializer;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

@Entity
@Table(name = "site_chatgroup_ips")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "sitechatgroupip")
public class SiteChatgroupIp implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="site_chatgroup_ips_id_seq")
    @SequenceGenerator(name="site_chatgroup_ips_id_seq", sequenceName="site_chatgroup_ips_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name="ip_name", nullable=false)
    private String ipName;

    @Column(name = "ip_whois")
    private String ipWhois;

    @NotNull
    @Column(name = "first_used_date", nullable = false)
    @JsonSerialize(using = MeridianDateSerializer.class)
    private ZonedDateTime firstUsedDate;

    @Column(name = "last_used_date")
    @JsonSerialize(using = MeridianDateSerializer.class)
    private ZonedDateTime lastUsedDate;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"comments", "domains", "ips", "names", "infos", "numVisitors", "numUsers", "languages", "tags", "authors"})
    private SiteChatgroup siteChatgroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIpName() {
        return ipName;
    }

    public SiteChatgroupIp ipName(String ipName) {
        this.ipName = ipName;
        return this;
    }

    public void setIpName(String ipName) {
        this.ipName = ipName;
    }

    public String getIpWhois() {
        return ipWhois;
    }

    public SiteChatgroupIp ipWhois(String ipWhois) {
        this.ipWhois = ipWhois;
        return this;
    }

    public void setIpWhois(String ipWhois) {
        this.ipWhois = ipWhois;
    }

    public ZonedDateTime getFirstUsedDate() {
        return firstUsedDate;
    }

    public SiteChatgroupIp firstUsedDate(ZonedDateTime firstUsedDate) {
        this.firstUsedDate = firstUsedDate;
        return this;
    }

    public void setFirstUsedDate(ZonedDateTime firstUsedDate) {
        this.firstUsedDate = firstUsedDate;
    }

    public ZonedDateTime getLastUsedDate() {
        return lastUsedDate;
    }

    public SiteChatgroupIp lastUsedDate(ZonedDateTime lastUsedDate) {
        this.lastUsedDate = lastUsedDate;
        return this;
    }

    public void setLastUsedDate(ZonedDateTime lastUsedDate) {
        this.lastUsedDate = lastUsedDate;
    }

    public SiteChatgroup getSiteChatgroup() {
        return siteChatgroup;
    }

    public SiteChatgroupIp siteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
        return this;
    }

    public void setSiteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SiteChatgroupIp siteChatgroupIp = (SiteChatgroupIp) o;
        if (siteChatgroupIp.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), siteChatgroupIp.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SiteChatgroupIp{" +
            "id=" + getId() +
            ", ipName='" + getIpName() + "'" +
            ", ipWhois='" + getIpWhois() + "'" +
            ", firstUsedDate='" + getFirstUsedDate() + "'" +
            ", lastUsedDate='" + getLastUsedDate() + "'" +
            "}";
    }
}
