package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.SiteChatgroupNumUsers;
import biz.newparadigm.meridian.service.SiteChatgroupNumUsersService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.SiteChatgroupNumUsersCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupNumUsersQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SiteChatgroupNumUsers.
 */
@RestController
@RequestMapping("/api")
public class SiteChatgroupNumUsersResource {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupNumUsersResource.class);

    private static final String ENTITY_NAME = "siteChatgroupNumUsers";

    private final SiteChatgroupNumUsersService siteChatgroupNumUsersService;

    private final SiteChatgroupNumUsersQueryService siteChatgroupNumUsersQueryService;

    public SiteChatgroupNumUsersResource(SiteChatgroupNumUsersService siteChatgroupNumUsersService, SiteChatgroupNumUsersQueryService siteChatgroupNumUsersQueryService) {
        this.siteChatgroupNumUsersService = siteChatgroupNumUsersService;
        this.siteChatgroupNumUsersQueryService = siteChatgroupNumUsersQueryService;
    }

    /**
     * POST  /site-chatgroup-num-users : Create a new siteChatgroupNumUsers.
     *
     * @param siteChatgroupNumUsers the siteChatgroupNumUsers to create
     * @return the ResponseEntity with status 201 (Created) and with body the new siteChatgroupNumUsers, or with status 400 (Bad Request) if the siteChatgroupNumUsers has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/site-chatgroup-num-users")
    @Timed
    public ResponseEntity<SiteChatgroupNumUsers> createSiteChatgroupNumUsers(@Valid @RequestBody SiteChatgroupNumUsers siteChatgroupNumUsers) throws URISyntaxException {
        log.debug("REST request to save SiteChatgroupNumUsers : {}", siteChatgroupNumUsers);
        if (siteChatgroupNumUsers.getId() != null) {
            throw new BadRequestAlertException("A new siteChatgroupNumUsers cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SiteChatgroupNumUsers result = siteChatgroupNumUsersService.save(siteChatgroupNumUsers);
        return ResponseEntity.created(new URI("/api/site-chatgroup-num-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /site-chatgroup-num-users : Updates an existing siteChatgroupNumUsers.
     *
     * @param siteChatgroupNumUsers the siteChatgroupNumUsers to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated siteChatgroupNumUsers,
     * or with status 400 (Bad Request) if the siteChatgroupNumUsers is not valid,
     * or with status 500 (Internal Server Error) if the siteChatgroupNumUsers couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/site-chatgroup-num-users")
    @Timed
    public ResponseEntity<SiteChatgroupNumUsers> updateSiteChatgroupNumUsers(@Valid @RequestBody SiteChatgroupNumUsers siteChatgroupNumUsers) throws URISyntaxException {
        log.debug("REST request to update SiteChatgroupNumUsers : {}", siteChatgroupNumUsers);
        if (siteChatgroupNumUsers.getId() == null) {
            return createSiteChatgroupNumUsers(siteChatgroupNumUsers);
        }
        SiteChatgroupNumUsers result = siteChatgroupNumUsersService.save(siteChatgroupNumUsers);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, siteChatgroupNumUsers.getId().toString()))
            .body(result);
    }

    /**
     * GET  /site-chatgroup-num-users : get all the siteChatgroupNumUsers.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of siteChatgroupNumUsers in body
     */
    @GetMapping("/site-chatgroup-num-users")
    @Timed
    public ResponseEntity<List<SiteChatgroupNumUsers>> getAllSiteChatgroupNumUsers(SiteChatgroupNumUsersCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get SiteChatgroupNumUsers by criteria: {}", criteria);
        Page<SiteChatgroupNumUsers> page = siteChatgroupNumUsersQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/site-chatgroup-num-users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /site-chatgroup-num-users/:id : get the "id" siteChatgroupNumUsers.
     *
     * @param id the id of the siteChatgroupNumUsers to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the siteChatgroupNumUsers, or with status 404 (Not Found)
     */
    @GetMapping("/site-chatgroup-num-users/{id}")
    @Timed
    public ResponseEntity<SiteChatgroupNumUsers> getSiteChatgroupNumUsers(@PathVariable Long id) {
        log.debug("REST request to get SiteChatgroupNumUsers : {}", id);
        SiteChatgroupNumUsers siteChatgroupNumUsers = siteChatgroupNumUsersService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(siteChatgroupNumUsers));
    }

    /**
     * DELETE  /site-chatgroup-num-users/:id : delete the "id" siteChatgroupNumUsers.
     *
     * @param id the id of the siteChatgroupNumUsers to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/site-chatgroup-num-users/{id}")
    @Timed
    public ResponseEntity<Void> deleteSiteChatgroupNumUsers(@PathVariable Long id) {
        log.debug("REST request to delete SiteChatgroupNumUsers : {}", id);
        siteChatgroupNumUsersService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/site-chatgroup-num-users?query=:query : search for the siteChatgroupNumUsers corresponding
     * to the query.
     *
     * @param query the query of the siteChatgroupNumUsers search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/site-chatgroup-num-users")
    @Timed
    public ResponseEntity<List<SiteChatgroupNumUsers>> searchSiteChatgroupNumUsers(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SiteChatgroupNumUsers for query {}", query);
        Page<SiteChatgroupNumUsers> page = siteChatgroupNumUsersService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/site-chatgroup-num-users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
