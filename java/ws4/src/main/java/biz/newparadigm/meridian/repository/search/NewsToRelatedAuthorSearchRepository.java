package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsToRelatedAuthor;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsToRelatedAuthor entity.
 */
public interface NewsToRelatedAuthorSearchRepository extends ElasticsearchRepository<NewsToRelatedAuthor, Long> {
}
