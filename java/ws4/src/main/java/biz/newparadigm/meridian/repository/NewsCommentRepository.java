package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsComment;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsComment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsCommentRepository extends JpaRepository<NewsComment, Long>, JpaSpecificationExecutor<NewsComment> {

}
