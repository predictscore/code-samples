package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.SiteChatgroupToLanguage;
import biz.newparadigm.meridian.repository.SiteChatgroupToLanguageRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupToLanguageSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SiteChatgroupToLanguage.
 */
@Service
@Transactional
public class SiteChatgroupToLanguageService {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupToLanguageService.class);

    private final SiteChatgroupToLanguageRepository siteChatgroupToLanguageRepository;

    private final SiteChatgroupToLanguageSearchRepository siteChatgroupToLanguageSearchRepository;

    public SiteChatgroupToLanguageService(SiteChatgroupToLanguageRepository siteChatgroupToLanguageRepository, SiteChatgroupToLanguageSearchRepository siteChatgroupToLanguageSearchRepository) {
        this.siteChatgroupToLanguageRepository = siteChatgroupToLanguageRepository;
        this.siteChatgroupToLanguageSearchRepository = siteChatgroupToLanguageSearchRepository;
    }

    /**
     * Save a siteChatgroupToLanguage.
     *
     * @param siteChatgroupToLanguage the entity to save
     * @return the persisted entity
     */
    public SiteChatgroupToLanguage save(SiteChatgroupToLanguage siteChatgroupToLanguage) {
        log.debug("Request to save SiteChatgroupToLanguage : {}", siteChatgroupToLanguage);
        SiteChatgroupToLanguage result = siteChatgroupToLanguageRepository.save(siteChatgroupToLanguage);
        siteChatgroupToLanguageSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the siteChatgroupToLanguages.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupToLanguage> findAll(Pageable pageable) {
        log.debug("Request to get all SiteChatgroupToLanguages");
        return siteChatgroupToLanguageRepository.findAll(pageable);
    }

    /**
     *  Get one siteChatgroupToLanguage by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SiteChatgroupToLanguage findOne(Long id) {
        log.debug("Request to get SiteChatgroupToLanguage : {}", id);
        return siteChatgroupToLanguageRepository.findOne(id);
    }

    /**
     *  Delete the  siteChatgroupToLanguage by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SiteChatgroupToLanguage : {}", id);
        siteChatgroupToLanguageRepository.delete(id);
        siteChatgroupToLanguageSearchRepository.delete(id);
    }

    /**
     * Search for the siteChatgroupToLanguage corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupToLanguage> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SiteChatgroupToLanguages for query {}", query);
        Page<SiteChatgroupToLanguage> result = siteChatgroupToLanguageSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
