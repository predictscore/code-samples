package biz.newparadigm.meridian.web.rest.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.DefaultProblem;
import org.zalando.problem.Problem;
import org.zalando.problem.ProblemBuilder;
import org.zalando.problem.Status;
import org.zalando.problem.spring.web.advice.ProblemHandling;
import org.zalando.problem.spring.web.advice.validation.ConstraintViolationProblem;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionTranslator implements ProblemHandling {
    private final Logger log = LoggerFactory.getLogger(ExceptionTranslator.class);

    @Override
    public ResponseEntity<Problem> process(@Nullable ResponseEntity<Problem> entity, NativeWebRequest request) {
        if (entity == null || entity.getBody() == null) {
            return entity;
        }
        Problem problem = entity.getBody();
        log.debug(problem.toString());
        if (!(problem instanceof ConstraintViolationProblem || problem instanceof DefaultProblem)) {
            return entity;
        }
        ProblemBuilder builder = Problem.builder()
            .withStatus(problem.getStatus())
            .withTitle(problem.getTitle())
            .with("path", request.getNativeRequest(HttpServletRequest.class).getRequestURI());

        if (problem instanceof ConstraintViolationProblem) {
            builder
                .with("violations", ((ConstraintViolationProblem) problem).getViolations())
                .with("message", ErrorConstants.ERR_VALIDATION);
            return new ResponseEntity<>(builder.build(), entity.getHeaders(), entity.getStatusCode());
        } else {
            builder
                .withCause(((DefaultProblem) problem).getCause())
                .withDetail(problem.getDetail())
                .withInstance(problem.getInstance());
            problem.getParameters().forEach(builder::with);
            return new ResponseEntity<>(builder.build(), entity.getHeaders(), entity.getStatusCode());
        }
    }

    @Override
    public ResponseEntity<Problem> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, @Nonnull NativeWebRequest request) {
        BindingResult result = ex.getBindingResult();
        List<FieldErrorVM> fieldErrors = result.getFieldErrors().stream()
            .map(f -> new FieldErrorVM(f.getObjectName(), f.getField(), f.getDefaultMessage()))
            .collect(Collectors.toList());

        Problem problem = Problem.builder()
            .withTitle("Request body or query param is not valid")
            .withStatus(defaultConstraintViolationStatus())
            .with("message", ErrorConstants.ERR_VALIDATION)
            .with("fieldErrors", fieldErrors)
            .build();
        return create(ex, problem, request);
    }

    @ExceptionHandler(BadRequestAlertException.class)
    public ResponseEntity<Problem> handleBadRequestAlertException(BadRequestAlertException ex, NativeWebRequest request) {
        Problem problem = Problem.builder()
            .withStatus(Status.BAD_REQUEST)
            .withTitle(ErrorConstants.ERR_BAD_REQUEST)
            .with("message", ex.getLocalizedMessage())
            .build();
        return create(ex, problem, request);
    }

    @ExceptionHandler(ConcurrencyFailureException.class)
    public ResponseEntity<Problem> handleConcurrencyFailure(ConcurrencyFailureException ex, NativeWebRequest request) {
        Problem problem = Problem.builder()
            .withStatus(Status.CONFLICT)
            .withTitle(ErrorConstants.ERR_CONCURRENCY_FAILURE)
            .with("message", ex.getLocalizedMessage())
            .build();
        return create(ex, problem, request);
    }

    @ExceptionHandler(TransactionSystemException.class)
    public ResponseEntity<Problem> handleConstraintViolationFailure(TransactionSystemException ex, NativeWebRequest request) {
        System.out.println(ex.getClass());
        Problem problem = Problem.builder()
            .withStatus(Status.INTERNAL_SERVER_ERROR)
            .withTitle(ErrorConstants.ERR_CONSTRAINT_VIOLATION)
            .with("message", getExceptionMessageChainString(ex))
            .build();
        return create(ex, problem, request);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Problem> handleFailure(Exception ex, NativeWebRequest request) {
        System.out.println(ex.getClass());
        Problem problem = Problem.builder()
            .withStatus(Status.INTERNAL_SERVER_ERROR)
            .with("message", getExceptionMessageChainString(ex))
            .build();
        return create(ex, problem, request);
    }

    private String getExceptionMessageChainString(Throwable throwable) {
        try {
            List<String> list = getExceptionMessageChain(throwable);
            return String.join(";", list);
        } catch (Exception e) {
            return "";
        }
    }

    private List<String> getExceptionMessageChain(Throwable throwable) {
        try {
            List<String> result = new ArrayList<>();
            while (throwable != null) {
                result.add(throwable.getMessage());
                throwable = throwable.getCause();
            }
            return result;
        } catch (Exception e) {
            return Collections.singletonList(throwable.getMessage());
        }
    }
}
