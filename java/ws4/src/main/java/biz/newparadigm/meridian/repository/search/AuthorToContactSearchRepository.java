package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.AuthorToContact;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AuthorToContact entity.
 */
public interface AuthorToContactSearchRepository extends ElasticsearchRepository<AuthorToContact, Long> {
}
