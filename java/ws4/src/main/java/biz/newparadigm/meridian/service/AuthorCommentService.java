package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.AuthorComment;
import biz.newparadigm.meridian.repository.AuthorCommentRepository;
import biz.newparadigm.meridian.repository.search.AuthorCommentSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing AuthorComment.
 */
@Service
@Transactional
public class AuthorCommentService {

    private final Logger log = LoggerFactory.getLogger(AuthorCommentService.class);

    private final AuthorCommentRepository authorCommentRepository;

    private final AuthorCommentSearchRepository authorCommentSearchRepository;

    public AuthorCommentService(AuthorCommentRepository authorCommentRepository, AuthorCommentSearchRepository authorCommentSearchRepository) {
        this.authorCommentRepository = authorCommentRepository;
        this.authorCommentSearchRepository = authorCommentSearchRepository;
    }

    /**
     * Save a authorComment.
     *
     * @param authorComment the entity to save
     * @return the persisted entity
     */
    public AuthorComment save(AuthorComment authorComment) {
        log.debug("Request to save AuthorComment : {}", authorComment);
        AuthorComment result = authorCommentRepository.save(authorComment);
        authorCommentSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the authorComments.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuthorComment> findAll(Pageable pageable) {
        log.debug("Request to get all AuthorComments");
        return authorCommentRepository.findAll(pageable);
    }

    /**
     *  Get one authorComment by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AuthorComment findOne(Long id) {
        log.debug("Request to get AuthorComment : {}", id);
        return authorCommentRepository.findOne(id);
    }

    /**
     *  Delete the  authorComment by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AuthorComment : {}", id);
        authorCommentRepository.delete(id);
        authorCommentSearchRepository.delete(id);
    }

    /**
     * Search for the authorComment corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuthorComment> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of AuthorComments for query {}", query);
        Page<AuthorComment> result = authorCommentSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
