package biz.newparadigm.meridian.service.util;

import biz.newparadigm.meridian.domain.News;

import java.util.ArrayList;

public class ServiceUtil {
    public static final String AUTHOR_FILTER_NAMES = "names.";
    public static final String AUTHOR_FILTER_CONTACTS = "contacts.";
    public static final String AUTHOR_FILTER_TAGS = "tags.";
    public static final String AUTHOR_FILTER_LANGUAGES = "languages.";
    public static final String AUTHOR_FILTER_SITECHATGROUPS = "siteChatGroups.";

    public static final String SITECHATGROUP_FILTER_DOMAINS = "domains.";
    public static final String SITECHATGROUP_FILTER_IPS = "ips.";
    public static final String SITECHATGROUP_FILTER_NAMES = "names.";
    public static final String SITECHATGROUP_FILTER_NUMVISITORS = "numVisitors.";
    public static final String SITECHATGROUP_FILTER_NUMUSERS = "numUsers.";
    public static final String SITECHATGROUP_FILTER_LANGUAGES = "languages.";
    public static final String SITECHATGROUP_FILTER_TAGS = "tags.";
    public static final String SITECHATGROUP_FILTER_AUTHORS = "authors.";

    public static final String NEWS_FILTER_ATTACHMENTS = "attachments.";
    public static final String NEWS_FILTER_ORIGINAL_IMAGES = "originalImages.";
    public static final String NEWS_FILTER_PUBLIC_IMAGES = "publicImages.";
    public static final String NEWS_FILTER_CONFIDENTIAL_IMAGES = "confidentialImages.";
    public static final String NEWS_FILTER_URLS = "urls.";
    public static final String NEWS_FILTER_DOWNLOADABLE_CONTENTURLS = "downloadableContentUrls.";
    public static final String NEWS_AUTHOR_FILTER = "author.names.";
    public static final String NEWS_AUTHOR_SITECHATGROUP_FILTER = "author.siteChatGroups.";
    public static final String NEWS_SITECHATGROUP_FILTER = "siteChatgroup.names.";
    public static final String NEWS_CATEGORY_FILTER = "metagroups.mmvar.category.";

    public static void setEmptyListIfNullNews(News news) {
        if (news.getAttachments() == null) {
            news.setAttachments(new ArrayList<>());
        }
        if (news.getPublicImages() == null) {
            news.setPublicImages(new ArrayList<>());
        }
        if (news.getConfidentialImages() == null) {
            news.setConfidentialImages(new ArrayList<>());
        }
        if (news.getOriginalImages() == null) {
            news.setOriginalImages(new ArrayList<>());
        }
        if (news.getUrls() == null) {
            news.setUrls(new ArrayList<>());
        }
        if (news.getDownloadableContentUrls() == null) {
            news.setDownloadableContentUrls(new ArrayList<>());
        }
        if (news.getTags() == null) {
            news.setTags(new ArrayList<>());
        }
        if (news.getLanguages() == null) {
            news.setLanguages(new ArrayList<>());
        }
        if (news.getNewsSummaries() == null) {
            news.setNewsSummaries(new ArrayList<>());
        }
        if (news.getMetadata() == null) {
            news.setMetadata(new ArrayList<>());
        }
    }

    public static boolean checkContains(String filter, String filterPart) {
        return filter != null && filter.contains(filterPart);
    }
}
