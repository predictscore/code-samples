package biz.newparadigm.meridian.service.dto.export;

import java.util.List;

public class NewsExport {
    private int recordId;
    private String intelCategory = "";
    private String intelSubCategory = "";
    private String intelMicroCategory = "";
    private String name = "";
    private String version = "";
    private List<String> cves;
    private List<String> mitigationsByPassed;
    private List<String> offerLanguages;
    private List<String> uiLanguages;
    private List<NewsExportImgDescTrans> offerRestrictedImages;
    private List<NewsExportImgDescTrans> nonSensitiveOfferImages;
    private List<NewsExportImgDescTrans> attachments;
    private String intelRegion = "";
    private String intelSource="Alfa";          //HC'd
    private String intelForum = "";                 //SiteChatGroup englishName
    private String intelForumAlias = "";             //SiteChatGroup otherNames
    private String intelForumLanguage = "";
    private String offerClaim = "";
    private String offerDate = "";
    private String offerPrice = "";
    private String offerType = "";
    private String structuredPrice = "";
    private String actor = "";
    private String actorAlias = "";
    private String actorProfile = "";
    private String actorRegion = "";
    private String associateProfile = "";
    private String purchaserType = "";
    private String observedDate = "";
    private String observedMonthYear = "";
    private String reportedDate = "";
    private String recordedDate = "";
    private String vendorNotes = "";
    private String claimValidity = "";
    private String clientNotes = "";
    private String threatRisk = "";
    private String attachmentHyperLink = "";
    private String none = "";
    private String attachmentLbi = "";
    private String attachmentMbi = "";
    private String attachmentHbi = "";
    private String levelOfActivity = "";
    private String regionOfActivity = "";
    private String securityRole = "";
    private String itRelevance = "";

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public String getIntelCategory() {
        return intelCategory;
    }

    public void setIntelCategory(String intelCategory) {
        this.intelCategory = intelCategory;
    }

    public String getIntelSubCategory() {
        return intelSubCategory;
    }

    public void setIntelSubCategory(String intelSubCategory) {
        this.intelSubCategory = intelSubCategory;
    }

    public String getIntelMicroCategory() {
        return intelMicroCategory;
    }

    public void setIntelMicroCategory(String intelMicroCategory) {
        this.intelMicroCategory = intelMicroCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<String> getCves() {
        return cves;
    }

    public void setCves(List<String> cves) {
        this.cves = cves;
    }

    public List<String> getMitigationsByPassed() {
        return mitigationsByPassed;
    }

    public void setMitigationsByPassed(List<String> mitigationsByPassed) {
        this.mitigationsByPassed = mitigationsByPassed;
    }

    public List<String> getOfferLanguages() {
        return offerLanguages;
    }

    public void setOfferLanguages(List<String> offerLanguages) {
        this.offerLanguages = offerLanguages;
    }

    public List<String> getUiLanguages() {
        return uiLanguages;
    }

    public void setUiLanguages(List<String> uiLanguages) {
        this.uiLanguages = uiLanguages;
    }

    public List<NewsExportImgDescTrans> getOfferRestrictedImages() {
        return offerRestrictedImages;
    }

    public void setOfferRestrictedImages(List<NewsExportImgDescTrans> offerRestrictedImages) {
        this.offerRestrictedImages = offerRestrictedImages;
    }

    public List<NewsExportImgDescTrans> getNonSensitiveOfferImages() {
        return nonSensitiveOfferImages;
    }

    public void setNonSensitiveOfferImages(List<NewsExportImgDescTrans> nonSensitiveOfferImages) {
        this.nonSensitiveOfferImages = nonSensitiveOfferImages;
    }

    public List<NewsExportImgDescTrans> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<NewsExportImgDescTrans> attachments) {
        this.attachments = attachments;
    }

    public String getIntelRegion() {
        return intelRegion;
    }

    public void setIntelRegion(String intelRegion) {
        this.intelRegion = intelRegion;
    }

    public String getIntelSource() {
        return intelSource;
    }

    public void setIntelSource(String intelSource) {
        this.intelSource = intelSource;
    }

    public String getIntelForum() {
        return intelForum;
    }

    public void setIntelForum(String intelForum) {
        this.intelForum = intelForum;
    }

    public String getIntelForumAlias() {
        return intelForumAlias;
    }

    public void setIntelForumAlias(String intelForumAlias) {
        this.intelForumAlias = intelForumAlias;
    }

    public String getIntelForumLanguage() {
        return intelForumLanguage;
    }

    public void setIntelForumLanguage(String intelForumLanguage) {
        this.intelForumLanguage = intelForumLanguage;
    }

    public String getOfferClaim() {
        return offerClaim;
    }

    public void setOfferClaim(String offerClaim) {
        this.offerClaim = offerClaim;
    }

    public String getOfferDate() {
        return offerDate;
    }

    public void setOfferDate(String offerDate) {
        this.offerDate = offerDate;
    }

    public String getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(String offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getStructuredPrice() {
        return structuredPrice;
    }

    public void setStructuredPrice(String structuredPrice) {
        this.structuredPrice = structuredPrice;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getActorAlias() {
        return actorAlias;
    }

    public void setActorAlias(String actorAlias) {
        this.actorAlias = actorAlias;
    }

    public String getActorProfile() {
        return actorProfile;
    }

    public void setActorProfile(String actorProfile) {
        this.actorProfile = actorProfile;
    }

    public String getActorRegion() {
        return actorRegion;
    }

    public void setActorRegion(String actorRegion) {
        this.actorRegion = actorRegion;
    }

    public String getAssociateProfile() {
        return associateProfile;
    }

    public void setAssociateProfile(String associateProfile) {
        this.associateProfile = associateProfile;
    }

    public String getPurchaserType() {
        return purchaserType;
    }

    public void setPurchaserType(String purchaserType) {
        this.purchaserType = purchaserType;
    }

    public String getObservedDate() {
        return observedDate;
    }

    public void setObservedDate(String observedDate) {
        this.observedDate = observedDate;
    }

    public String getObservedMonthYear() {
        return observedMonthYear;
    }

    public void setObservedMonthYear(String observedMonthYear) {
        this.observedMonthYear = observedMonthYear;
    }

    public String getReportedDate() {
        return reportedDate;
    }

    public void setReportedDate(String reportedDate) {
        this.reportedDate = reportedDate;
    }

    public String getRecordedDate() {
        return recordedDate;
    }

    public void setRecordedDate(String recordedDate) {
        this.recordedDate = recordedDate;
    }

    public String getVendorNotes() {
        return vendorNotes;
    }

    public void setVendorNotes(String vendorNotes) {
        this.vendorNotes = vendorNotes;
    }

    public String getClaimValidity() {
        return claimValidity;
    }

    public void setClaimValidity(String claimValidity) {
        this.claimValidity = claimValidity;
    }

    public String getClientNotes() {
        return clientNotes;
    }

    public void setClientNotes(String clientNotes) {
        this.clientNotes = clientNotes;
    }

    public String getThreatRisk() {
        return threatRisk;
    }

    public void setThreatRisk(String threatRisk) {
        this.threatRisk = threatRisk;
    }

    public String getAttachmentHyperLink() {
        return attachmentHyperLink;
    }

    public void setAttachmentHyperLink(String attachmentHyperLink) {
        this.attachmentHyperLink = attachmentHyperLink;
    }

    public String getNone() {
        return none;
    }

    public void setNone(String none) {
        this.none = none;
    }

    public String getAttachmentLbi() {
        return attachmentLbi;
    }

    public void setAttachmentLbi(String attachmentLbi) {
        this.attachmentLbi = attachmentLbi;
    }

    public String getAttachmentMbi() {
        return attachmentMbi;
    }

    public void setAttachmentMbi(String attachmentMbi) {
        this.attachmentMbi = attachmentMbi;
    }

    public String getAttachmentHbi() {
        return attachmentHbi;
    }

    public void setAttachmentHbi(String attachmentHbi) {
        this.attachmentHbi = attachmentHbi;
    }

    public String getLevelOfActivity() {
        return levelOfActivity;
    }

    public void setLevelOfActivity(String levelOfActivity) {
        this.levelOfActivity = levelOfActivity;
    }

    public String getRegionOfActivity() {
        return regionOfActivity;
    }

    public void setRegionOfActivity(String regionOfActivity) {
        this.regionOfActivity = regionOfActivity;
    }

    public String getSecurityRole() {
        return securityRole;
    }

    public void setSecurityRole(String securityRole) {
        this.securityRole = securityRole;
    }

    public String getItRelevance() {
        return itRelevance;
    }

    public void setItRelevance(String itRelevance) {
        this.itRelevance = itRelevance;
    }
}
