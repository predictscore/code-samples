package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.AuthorName;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AuthorName entity.
 */
public interface AuthorNameSearchRepository extends ElasticsearchRepository<AuthorName, Long> {
}
