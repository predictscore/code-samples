package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsComment;
import biz.newparadigm.meridian.service.NewsCommentService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsCommentCriteria;
import biz.newparadigm.meridian.service.NewsCommentQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsComment.
 */
@RestController
@RequestMapping("/api")
public class NewsCommentResource {

    private final Logger log = LoggerFactory.getLogger(NewsCommentResource.class);

    private static final String ENTITY_NAME = "newsComment";

    private final NewsCommentService newsCommentService;

    private final NewsCommentQueryService newsCommentQueryService;

    public NewsCommentResource(NewsCommentService newsCommentService, NewsCommentQueryService newsCommentQueryService) {
        this.newsCommentService = newsCommentService;
        this.newsCommentQueryService = newsCommentQueryService;
    }

    /**
     * POST  /news-comments : Create a new newsComment.
     *
     * @param newsComment the newsComment to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsComment, or with status 400 (Bad Request) if the newsComment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-comments")
    @Timed
    public ResponseEntity<NewsComment> createNewsComment(@Valid @RequestBody NewsComment newsComment) throws URISyntaxException {
        log.debug("REST request to save NewsComment : {}", newsComment);
        if (newsComment.getId() != null) {
            throw new BadRequestAlertException("A new newsComment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsComment result = newsCommentService.save(newsComment);
        return ResponseEntity.created(new URI("/api/news-comments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-comments : Updates an existing newsComment.
     *
     * @param newsComment the newsComment to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsComment,
     * or with status 400 (Bad Request) if the newsComment is not valid,
     * or with status 500 (Internal Server Error) if the newsComment couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-comments")
    @Timed
    public ResponseEntity<NewsComment> updateNewsComment(@Valid @RequestBody NewsComment newsComment) throws URISyntaxException {
        log.debug("REST request to update NewsComment : {}", newsComment);
        if (newsComment.getId() == null) {
            return createNewsComment(newsComment);
        }
        NewsComment result = newsCommentService.save(newsComment);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsComment.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-comments : get all the newsComments.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsComments in body
     */
    @GetMapping("/news-comments")
    @Timed
    public ResponseEntity<List<NewsComment>> getAllNewsComments(NewsCommentCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsComments by criteria: {}", criteria);
        Page<NewsComment> page = newsCommentQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-comments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-comments/:id : get the "id" newsComment.
     *
     * @param id the id of the newsComment to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsComment, or with status 404 (Not Found)
     */
    @GetMapping("/news-comments/{id}")
    @Timed
    public ResponseEntity<NewsComment> getNewsComment(@PathVariable Long id) {
        log.debug("REST request to get NewsComment : {}", id);
        NewsComment newsComment = newsCommentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsComment));
    }

    /**
     * DELETE  /news-comments/:id : delete the "id" newsComment.
     *
     * @param id the id of the newsComment to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-comments/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsComment(@PathVariable Long id) {
        log.debug("REST request to delete NewsComment : {}", id);
        newsCommentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-comments?query=:query : search for the newsComment corresponding
     * to the query.
     *
     * @param query the query of the newsComment search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-comments")
    @Timed
    public ResponseEntity<List<NewsComment>> searchNewsComments(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsComments for query {}", query);
        Page<NewsComment> page = newsCommentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-comments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
