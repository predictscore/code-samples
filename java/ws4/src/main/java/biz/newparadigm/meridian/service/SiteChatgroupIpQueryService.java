package biz.newparadigm.meridian.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.SiteChatgroupIp;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.SiteChatgroupIpRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupIpSearchRepository;
import biz.newparadigm.meridian.service.dto.SiteChatgroupIpCriteria;


/**
 * Service for executing complex queries for SiteChatgroupIp entities in the database.
 * The main input is a {@link SiteChatgroupIpCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link SiteChatgroupIp} or a {@link Page} of {%link SiteChatgroupIp} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class SiteChatgroupIpQueryService extends QueryService<SiteChatgroupIp> {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupIpQueryService.class);


    private final SiteChatgroupIpRepository siteChatgroupIpRepository;

    private final SiteChatgroupIpSearchRepository siteChatgroupIpSearchRepository;

    public SiteChatgroupIpQueryService(SiteChatgroupIpRepository siteChatgroupIpRepository, SiteChatgroupIpSearchRepository siteChatgroupIpSearchRepository) {
        this.siteChatgroupIpRepository = siteChatgroupIpRepository;
        this.siteChatgroupIpSearchRepository = siteChatgroupIpSearchRepository;
    }

    /**
     * Return a {@link List} of {%link SiteChatgroupIp} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SiteChatgroupIp> findByCriteria(SiteChatgroupIpCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<SiteChatgroupIp> specification = createSpecification(criteria);
        return siteChatgroupIpRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link SiteChatgroupIp} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupIp> findByCriteria(SiteChatgroupIpCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<SiteChatgroupIp> specification = createSpecification(criteria);
        return siteChatgroupIpRepository.findAll(specification, page);
    }

    /**
     * Function to convert SiteChatgroupIpCriteria to a {@link Specifications}
     */
    private Specifications<SiteChatgroupIp> createSpecification(SiteChatgroupIpCriteria criteria) {
        Specifications<SiteChatgroupIp> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SiteChatgroupIp_.id));
            }
            if (criteria.getIpName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIpName(), SiteChatgroupIp_.ipName));
            }
            if (criteria.getIpWhois() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIpWhois(), SiteChatgroupIp_.ipWhois));
            }
            if (criteria.getFirstUsedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFirstUsedDate(), SiteChatgroupIp_.firstUsedDate));
            }
            if (criteria.getLastUsedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUsedDate(), SiteChatgroupIp_.lastUsedDate));
            }
            if (criteria.getSiteChatgroupId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSiteChatgroupId(), SiteChatgroupIp_.siteChatgroup, SiteChatgroup_.id));
            }
        }
        return specification;
    }

}
