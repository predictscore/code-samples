package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsSummaryAttachment;
import biz.newparadigm.meridian.service.NewsSummaryAttachmentService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsSummaryAttachmentCriteria;
import biz.newparadigm.meridian.service.NewsSummaryAttachmentQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsSummaryAttachment.
 */
@RestController
@RequestMapping("/api")
public class NewsSummaryAttachmentResource {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryAttachmentResource.class);

    private static final String ENTITY_NAME = "newsSummaryAttachment";

    private final NewsSummaryAttachmentService newsSummaryAttachmentService;

    private final NewsSummaryAttachmentQueryService newsSummaryAttachmentQueryService;

    public NewsSummaryAttachmentResource(NewsSummaryAttachmentService newsSummaryAttachmentService, NewsSummaryAttachmentQueryService newsSummaryAttachmentQueryService) {
        this.newsSummaryAttachmentService = newsSummaryAttachmentService;
        this.newsSummaryAttachmentQueryService = newsSummaryAttachmentQueryService;
    }

    /**
     * POST  /news-summary-attachments : Create a new newsSummaryAttachment.
     *
     * @param newsSummaryAttachment the newsSummaryAttachment to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsSummaryAttachment, or with status 400 (Bad Request) if the newsSummaryAttachment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-summary-attachments")
    @Timed
    public ResponseEntity<NewsSummaryAttachment> createNewsSummaryAttachment(@Valid @RequestBody NewsSummaryAttachment newsSummaryAttachment) throws URISyntaxException {
        log.debug("REST request to save NewsSummaryAttachment : {}", newsSummaryAttachment);
        if (newsSummaryAttachment.getId() != null) {
            throw new BadRequestAlertException("A new newsSummaryAttachment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsSummaryAttachment result = newsSummaryAttachmentService.save(newsSummaryAttachment);
        return ResponseEntity.created(new URI("/api/news-summary-attachments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-summary-attachments : Updates an existing newsSummaryAttachment.
     *
     * @param newsSummaryAttachment the newsSummaryAttachment to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsSummaryAttachment,
     * or with status 400 (Bad Request) if the newsSummaryAttachment is not valid,
     * or with status 500 (Internal Server Error) if the newsSummaryAttachment couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-summary-attachments")
    @Timed
    public ResponseEntity<NewsSummaryAttachment> updateNewsSummaryAttachment(@Valid @RequestBody NewsSummaryAttachment newsSummaryAttachment) throws URISyntaxException {
        log.debug("REST request to update NewsSummaryAttachment : {}", newsSummaryAttachment);
        if (newsSummaryAttachment.getId() == null) {
            return createNewsSummaryAttachment(newsSummaryAttachment);
        }
        NewsSummaryAttachment result = newsSummaryAttachmentService.save(newsSummaryAttachment);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsSummaryAttachment.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-summary-attachments : get all the newsSummaryAttachments.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsSummaryAttachments in body
     */
    @GetMapping("/news-summary-attachments")
    @Timed
    public ResponseEntity<List<NewsSummaryAttachment>> getAllNewsSummaryAttachments(NewsSummaryAttachmentCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsSummaryAttachments by criteria: {}", criteria);
        Page<NewsSummaryAttachment> page = newsSummaryAttachmentQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-summary-attachments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-summary-attachments/:id : get the "id" newsSummaryAttachment.
     *
     * @param id the id of the newsSummaryAttachment to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsSummaryAttachment, or with status 404 (Not Found)
     */
    @GetMapping("/news-summary-attachments/{id}")
    @Timed
    public ResponseEntity<NewsSummaryAttachment> getNewsSummaryAttachment(@PathVariable Long id) {
        log.debug("REST request to get NewsSummaryAttachment : {}", id);
        NewsSummaryAttachment newsSummaryAttachment = newsSummaryAttachmentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsSummaryAttachment));
    }

    /**
     * DELETE  /news-summary-attachments/:id : delete the "id" newsSummaryAttachment.
     *
     * @param id the id of the newsSummaryAttachment to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-summary-attachments/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsSummaryAttachment(@PathVariable Long id) {
        log.debug("REST request to delete NewsSummaryAttachment : {}", id);
        newsSummaryAttachmentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-summary-attachments?query=:query : search for the newsSummaryAttachment corresponding
     * to the query.
     *
     * @param query the query of the newsSummaryAttachment search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-summary-attachments")
    @Timed
    public ResponseEntity<List<NewsSummaryAttachment>> searchNewsSummaryAttachments(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsSummaryAttachments for query {}", query);
        Page<NewsSummaryAttachment> page = newsSummaryAttachmentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-summary-attachments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
