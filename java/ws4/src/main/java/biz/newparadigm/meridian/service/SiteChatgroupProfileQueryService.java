package biz.newparadigm.meridian.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.SiteChatgroupProfile;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.SiteChatgroupProfileRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupProfileSearchRepository;
import biz.newparadigm.meridian.service.dto.SiteChatgroupProfileCriteria;


/**
 * Service for executing complex queries for SiteChatgroupProfile entities in the database.
 * The main input is a {@link SiteChatgroupProfileCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link SiteChatgroupProfile} or a {@link Page} of {%link SiteChatgroupProfile} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class SiteChatgroupProfileQueryService extends QueryService<SiteChatgroupProfile> {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupProfileQueryService.class);


    private final SiteChatgroupProfileRepository siteChatgroupProfileRepository;

    private final SiteChatgroupProfileSearchRepository siteChatgroupProfileSearchRepository;

    public SiteChatgroupProfileQueryService(SiteChatgroupProfileRepository siteChatgroupProfileRepository, SiteChatgroupProfileSearchRepository siteChatgroupProfileSearchRepository) {
        this.siteChatgroupProfileRepository = siteChatgroupProfileRepository;
        this.siteChatgroupProfileSearchRepository = siteChatgroupProfileSearchRepository;
    }

    /**
     * Return a {@link List} of {%link SiteChatgroupProfile} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SiteChatgroupProfile> findByCriteria(SiteChatgroupProfileCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<SiteChatgroupProfile> specification = createSpecification(criteria);
        return siteChatgroupProfileRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link SiteChatgroupProfile} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupProfile> findByCriteria(SiteChatgroupProfileCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<SiteChatgroupProfile> specification = createSpecification(criteria);
        return siteChatgroupProfileRepository.findAll(specification, page);
    }

    /**
     * Function to convert SiteChatgroupProfileCriteria to a {@link Specifications}
     */
    private Specifications<SiteChatgroupProfile> createSpecification(SiteChatgroupProfileCriteria criteria) {
        Specifications<SiteChatgroupProfile> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SiteChatgroupProfile_.id));
            }
            if (criteria.getInfo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInfo(), SiteChatgroupProfile_.info));
            }
            if (criteria.getUsername() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUsername(), SiteChatgroupProfile_.username));
            }
            if (criteria.getSubmissionDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSubmissionDate(), SiteChatgroupProfile_.submissionDate));
            }
            if (criteria.getSiteChatgroupId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSiteChatgroupId(), SiteChatgroupProfile_.siteChatgroup, SiteChatgroup_.id));
            }
        }
        return specification;
    }

}
