package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.Metafield;
import biz.newparadigm.meridian.repository.MetafieldRepository;
import biz.newparadigm.meridian.repository.search.MetafieldSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Metafield.
 */
@Service
@Transactional
public class MetafieldService {

    private final Logger log = LoggerFactory.getLogger(MetafieldService.class);

    private final MetafieldRepository metafieldRepository;

    private final MetafieldSearchRepository metafieldSearchRepository;

    public MetafieldService(MetafieldRepository metafieldRepository, MetafieldSearchRepository metafieldSearchRepository) {
        this.metafieldRepository = metafieldRepository;
        this.metafieldSearchRepository = metafieldSearchRepository;
    }

    /**
     * Save a metafield.
     *
     * @param metafield the entity to save
     * @return the persisted entity
     */
    public Metafield save(Metafield metafield) {
        log.debug("Request to save Metafield : {}", metafield);
        Metafield result = metafieldRepository.save(metafield);
        metafieldSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the metafields.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Metafield> findAll(Pageable pageable) {
        log.debug("Request to get all Metafields");
        return metafieldRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public List<Metafield> findAll() {
        log.debug("Request to get all Metafields");
        return metafieldRepository.findAll();
    }

    /**
     *  Get one metafield by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Metafield findOne(Long id) {
        log.debug("Request to get Metafield : {}", id);
        return metafieldRepository.findOne(id);
    }

    /**
     *  Delete the  metafield by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Metafield : {}", id);
        metafieldRepository.delete(id);
        metafieldSearchRepository.delete(id);
    }

    /**
     * Search for the metafield corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Metafield> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Metafields for query {}", query);
        Page<Metafield> result = metafieldSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
