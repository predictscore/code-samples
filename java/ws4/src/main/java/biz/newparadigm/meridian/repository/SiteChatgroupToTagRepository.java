package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.SiteChatgroupToTag;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SiteChatgroupToTag entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SiteChatgroupToTagRepository extends JpaRepository<SiteChatgroupToTag, Long>, JpaSpecificationExecutor<SiteChatgroupToTag> {

}
