package biz.newparadigm.meridian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the NewsSummaryComment entity. This class is used in NewsSummaryCommentResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /news-summary-comments?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NewsSummaryCommentCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter userId;

    private StringFilter comment;

    private LongFilter newsSummaryId;

    private LongFilter parentCommentId;

    public NewsSummaryCommentCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getUserId() {
        return userId;
    }

    public void setUserId(StringFilter userId) {
        this.userId = userId;
    }

    public StringFilter getComment() {
        return comment;
    }

    public void setComment(StringFilter comment) {
        this.comment = comment;
    }

    public LongFilter getNewsSummaryId() {
        return newsSummaryId;
    }

    public void setNewsSummaryId(LongFilter newsSummaryId) {
        this.newsSummaryId = newsSummaryId;
    }

    public LongFilter getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(LongFilter parentCommentId) {
        this.parentCommentId = parentCommentId;
    }

    @Override
    public String toString() {
        return "NewsSummaryCommentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (comment != null ? "comment=" + comment + ", " : "") +
                (newsSummaryId != null ? "newsSummaryId=" + newsSummaryId + ", " : "") +
                (parentCommentId != null ? "parentCommentId=" + parentCommentId + ", " : "") +
            "}";
    }

}
