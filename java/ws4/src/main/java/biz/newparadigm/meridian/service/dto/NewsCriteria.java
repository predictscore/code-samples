package biz.newparadigm.meridian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the News entity. This class is used in NewsResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /news?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NewsCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private ZonedDateTimeFilter datetime;

    private StringFilter originalSubject;

    private StringFilter englishSubject;

    private StringFilter originalPost;

    private StringFilter englishPost;

    private ZonedDateTimeFilter creationDate;

    private LongFilter authorId;

    private LongFilter siteChatgroupId;

    private LongFilter locationId;

    public NewsCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public ZonedDateTimeFilter getDatetime() {
        return datetime;
    }

    public void setDatetime(ZonedDateTimeFilter datetime) {
        this.datetime = datetime;
    }

    public StringFilter getOriginalSubject() {
        return originalSubject;
    }

    public void setOriginalSubject(StringFilter originalSubject) {
        this.originalSubject = originalSubject;
    }

    public StringFilter getEnglishSubject() {
        return englishSubject;
    }

    public void setEnglishSubject(StringFilter englishSubject) {
        this.englishSubject = englishSubject;
    }

    public StringFilter getOriginalPost() {
        return originalPost;
    }

    public void setOriginalPost(StringFilter originalPost) {
        this.originalPost = originalPost;
    }

    public StringFilter getEnglishPost() {
        return englishPost;
    }

    public void setEnglishPost(StringFilter englishPost) {
        this.englishPost = englishPost;
    }

    public ZonedDateTimeFilter getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(ZonedDateTimeFilter creationDate) {
        this.creationDate = creationDate;
    }

    public LongFilter getAuthorId() {
        return authorId;
    }

    public void setAuthorId(LongFilter authorId) {
        this.authorId = authorId;
    }

    public LongFilter getSiteChatgroupId() {
        return siteChatgroupId;
    }

    public void setSiteChatgroupId(LongFilter siteChatgroupId) {
        this.siteChatgroupId = siteChatgroupId;
    }

    public LongFilter getLocationId() {
        return locationId;
    }

    public void setLocationId(LongFilter locationId) {
        this.locationId = locationId;
    }


    @Override
    public String toString() {
        return "NewsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (datetime != null ? "datetime=" + datetime + ", " : "") +
                (originalSubject != null ? "originalSubject=" + originalSubject + ", " : "") +
                (englishSubject != null ? "englishSubject=" + englishSubject + ", " : "") +
                (originalPost != null ? "originalPost=" + originalPost + ", " : "") +
                (englishPost != null ? "englishPost=" + englishPost + ", " : "") +
                (creationDate != null ? "creationDate=" + creationDate + ", " : "") +
                (authorId != null ? "authorId=" + authorId + ", " : "") +
                (siteChatgroupId != null ? "siteChatgroupId=" + siteChatgroupId + ", " : "") +
                (locationId != null ? "locationId=" + locationId + ", " : "") +
            "}";
    }

}
