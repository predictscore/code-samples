package biz.newparadigm.meridian.service.filter;

import java.util.List;

public class MeridianFilter<FIELD_TYPE> {
    private FIELD_TYPE eq;
    private FIELD_TYPE ne;
    private Boolean specified;
    private List<FIELD_TYPE> in;

    public FIELD_TYPE getEq() {
        return eq;
    }

    public void setEq(FIELD_TYPE eq) {
        this.eq = eq;
    }

    public FIELD_TYPE getNe() {
        return ne;
    }

    public void setNe(FIELD_TYPE ne) {
        this.ne = ne;
    }

    public Boolean getSpecified() {
        return specified;
    }

    public void setSpecified(Boolean specified) {
        this.specified = specified;
    }

    public List<FIELD_TYPE> getIn() {
        return in;
    }

    public void setIn(List<FIELD_TYPE> in) {
        this.in = in;
    }

    @Override
    public String toString() {
        return "MeridianFilter ["
            + (getEq() != null ? "equals=" + getEq() + ", " : "")
            + (getNe() != null ? "not equals=" + getNe() + ", " : "")
            + (getIn() != null ? "in=" + getIn() : "")
            + (getSpecified() != null ? "specified=" + getSpecified() : "")
            + "]";
    }
}
