package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsAttachment;
import biz.newparadigm.meridian.repository.NewsAttachmentRepository;
import biz.newparadigm.meridian.repository.search.NewsAttachmentSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsAttachment.
 */
@Service
@Transactional
public class NewsAttachmentService {

    private final Logger log = LoggerFactory.getLogger(NewsAttachmentService.class);

    private final NewsAttachmentRepository newsAttachmentRepository;

    private final NewsAttachmentSearchRepository newsAttachmentSearchRepository;

    public NewsAttachmentService(NewsAttachmentRepository newsAttachmentRepository, NewsAttachmentSearchRepository newsAttachmentSearchRepository) {
        this.newsAttachmentRepository = newsAttachmentRepository;
        this.newsAttachmentSearchRepository = newsAttachmentSearchRepository;
    }

    /**
     * Save a newsAttachment.
     *
     * @param newsAttachment the entity to save
     * @return the persisted entity
     */
    public NewsAttachment save(NewsAttachment newsAttachment) {
        log.debug("Request to save NewsAttachment : {}", newsAttachment);
        NewsAttachment result = newsAttachmentRepository.save(newsAttachment);
        newsAttachmentSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsAttachments.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsAttachment> findAll(Pageable pageable) {
        log.debug("Request to get all NewsAttachments");
        return newsAttachmentRepository.findAll(pageable);
    }

    /**
     *  Get one newsAttachment by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsAttachment findOne(Long id) {
        log.debug("Request to get NewsAttachment : {}", id);
        return newsAttachmentRepository.findOne(id);
    }

    /**
     *  Delete the  newsAttachment by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsAttachment : {}", id);
        newsAttachmentRepository.delete(id);
        newsAttachmentSearchRepository.delete(id);
    }

    /**
     * Search for the newsAttachment corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsAttachment> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsAttachments for query {}", query);
        Page<NewsAttachment> result = newsAttachmentSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
