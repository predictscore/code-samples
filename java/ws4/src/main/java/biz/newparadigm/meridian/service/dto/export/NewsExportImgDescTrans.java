package biz.newparadigm.meridian.service.dto.export;

public class NewsExportImgDescTrans {
    private String filename;
    private String image;
    private String description;
    private String translation;


    public NewsExportImgDescTrans() {
    }

    public NewsExportImgDescTrans(String filename, String image, String description, String translation) {
        this.filename = filename;
        this.image = image;
        this.description = description;
        this.translation = translation;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }
}
