package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.SiteChatgroupComment;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SiteChatgroupComment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SiteChatgroupCommentRepository extends JpaRepository<SiteChatgroupComment, Long>, JpaSpecificationExecutor<SiteChatgroupComment> {

}
