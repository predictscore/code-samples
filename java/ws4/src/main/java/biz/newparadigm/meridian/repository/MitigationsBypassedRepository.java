package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.MitigationsBypassed;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the MitigationsBypassed entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MitigationsBypassedRepository extends JpaRepository<MitigationsBypassed, Long>, JpaSpecificationExecutor<MitigationsBypassed> {

}
