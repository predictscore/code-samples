package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsSummaryToNews;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsSummaryToNewsRepository;
import biz.newparadigm.meridian.repository.search.NewsSummaryToNewsSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsSummaryToNewsCriteria;


/**
 * Service for executing complex queries for NewsSummaryToNews entities in the database.
 * The main input is a {@link NewsSummaryToNewsCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsSummaryToNews} or a {@link Page} of {%link NewsSummaryToNews} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsSummaryToNewsQueryService extends QueryService<NewsSummaryToNews> {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryToNewsQueryService.class);


    private final NewsSummaryToNewsRepository newsSummaryToNewsRepository;

    private final NewsSummaryToNewsSearchRepository newsSummaryToNewsSearchRepository;

    public NewsSummaryToNewsQueryService(NewsSummaryToNewsRepository newsSummaryToNewsRepository, NewsSummaryToNewsSearchRepository newsSummaryToNewsSearchRepository) {
        this.newsSummaryToNewsRepository = newsSummaryToNewsRepository;
        this.newsSummaryToNewsSearchRepository = newsSummaryToNewsSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsSummaryToNews} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsSummaryToNews> findByCriteria(NewsSummaryToNewsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsSummaryToNews> specification = createSpecification(criteria);
        return newsSummaryToNewsRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsSummaryToNews} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryToNews> findByCriteria(NewsSummaryToNewsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsSummaryToNews> specification = createSpecification(criteria);
        return newsSummaryToNewsRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsSummaryToNewsCriteria to a {@link Specifications}
     */
    private Specifications<NewsSummaryToNews> createSpecification(NewsSummaryToNewsCriteria criteria) {
        Specifications<NewsSummaryToNews> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsSummaryToNews_.id));
            }
            if (criteria.getNewsSummaryId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsSummaryId(), NewsSummaryToNews_.newsSummary, NewsSummary_.id));
            }
            if (criteria.getNewsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsId(), NewsSummaryToNews_.news, News_.id));
            }
        }
        return specification;
    }

}
