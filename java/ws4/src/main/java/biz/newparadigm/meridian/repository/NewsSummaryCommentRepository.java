package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsSummaryComment;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsSummaryComment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsSummaryCommentRepository extends JpaRepository<NewsSummaryComment, Long>, JpaSpecificationExecutor<NewsSummaryComment> {

}
