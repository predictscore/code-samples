package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.MitigationsBypassed;
import biz.newparadigm.meridian.service.MitigationsBypassedService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.MitigationsBypassedCriteria;
import biz.newparadigm.meridian.service.MitigationsBypassedQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MitigationsBypassed.
 */
@RestController
@RequestMapping("/api")
public class MitigationsBypassedResource {

    private final Logger log = LoggerFactory.getLogger(MitigationsBypassedResource.class);

    private static final String ENTITY_NAME = "mitigationsBypassed";

    private final MitigationsBypassedService mitigationsBypassedService;

    private final MitigationsBypassedQueryService mitigationsBypassedQueryService;

    public MitigationsBypassedResource(MitigationsBypassedService mitigationsBypassedService, MitigationsBypassedQueryService mitigationsBypassedQueryService) {
        this.mitigationsBypassedService = mitigationsBypassedService;
        this.mitigationsBypassedQueryService = mitigationsBypassedQueryService;
    }

    /**
     * POST  /mitigations-bypasseds : Create a new mitigationsBypassed.
     *
     * @param mitigationsBypassed the mitigationsBypassed to create
     * @return the ResponseEntity with status 201 (Created) and with body the new mitigationsBypassed, or with status 400 (Bad Request) if the mitigationsBypassed has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/mitigations-bypasseds")
    @Timed
    public ResponseEntity<MitigationsBypassed> createMitigationsBypassed(@RequestBody MitigationsBypassed mitigationsBypassed) throws URISyntaxException {
        log.debug("REST request to save MitigationsBypassed : {}", mitigationsBypassed);
        if (mitigationsBypassed.getId() != null) {
            throw new BadRequestAlertException("A new mitigationsBypassed cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MitigationsBypassed result = mitigationsBypassedService.save(mitigationsBypassed);
        return ResponseEntity.created(new URI("/api/mitigations-bypasseds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /mitigations-bypasseds : Updates an existing mitigationsBypassed.
     *
     * @param mitigationsBypassed the mitigationsBypassed to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated mitigationsBypassed,
     * or with status 400 (Bad Request) if the mitigationsBypassed is not valid,
     * or with status 500 (Internal Server Error) if the mitigationsBypassed couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/mitigations-bypasseds")
    @Timed
    public ResponseEntity<MitigationsBypassed> updateMitigationsBypassed(@RequestBody MitigationsBypassed mitigationsBypassed) throws URISyntaxException {
        log.debug("REST request to update MitigationsBypassed : {}", mitigationsBypassed);
        if (mitigationsBypassed.getId() == null) {
            return createMitigationsBypassed(mitigationsBypassed);
        }
        MitigationsBypassed result = mitigationsBypassedService.save(mitigationsBypassed);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, mitigationsBypassed.getId().toString()))
            .body(result);
    }

    /**
     * GET  /mitigations-bypasseds : get all the mitigationsBypasseds.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of mitigationsBypasseds in body
     */
    @GetMapping("/mitigations-bypasseds")
    @Timed
    public ResponseEntity<List<MitigationsBypassed>> getAllMitigationsBypasseds(MitigationsBypassedCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get MitigationsBypasseds by criteria: {}", criteria);
        Page<MitigationsBypassed> page = mitigationsBypassedQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/mitigations-bypasseds");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /mitigations-bypasseds/:id : get the "id" mitigationsBypassed.
     *
     * @param id the id of the mitigationsBypassed to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the mitigationsBypassed, or with status 404 (Not Found)
     */
    @GetMapping("/mitigations-bypasseds/{id}")
    @Timed
    public ResponseEntity<MitigationsBypassed> getMitigationsBypassed(@PathVariable Long id) {
        log.debug("REST request to get MitigationsBypassed : {}", id);
        MitigationsBypassed mitigationsBypassed = mitigationsBypassedService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(mitigationsBypassed));
    }

    /**
     * DELETE  /mitigations-bypasseds/:id : delete the "id" mitigationsBypassed.
     *
     * @param id the id of the mitigationsBypassed to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/mitigations-bypasseds/{id}")
    @Timed
    public ResponseEntity<Void> deleteMitigationsBypassed(@PathVariable Long id) {
        log.debug("REST request to delete MitigationsBypassed : {}", id);
        mitigationsBypassedService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/mitigations-bypasseds?query=:query : search for the mitigationsBypassed corresponding
     * to the query.
     *
     * @param query the query of the mitigationsBypassed search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/mitigations-bypasseds")
    @Timed
    public ResponseEntity<List<MitigationsBypassed>> searchMitigationsBypasseds(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of MitigationsBypasseds for query {}", query);
        Page<MitigationsBypassed> page = mitigationsBypassedService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/mitigations-bypasseds");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
