package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsAttachment;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsAttachment entity.
 */
public interface NewsAttachmentSearchRepository extends ElasticsearchRepository<NewsAttachment, Long> {
}
