package biz.newparadigm.meridian.service.filter;

public class MeridianRangeFilter<FIELD_TYPE extends Comparable<? super FIELD_TYPE>> extends MeridianFilter<FIELD_TYPE> {
    private FIELD_TYPE gt;
    private FIELD_TYPE lt;
    private FIELD_TYPE ge;
    private FIELD_TYPE le;

    public FIELD_TYPE getGt() {
        return gt;
    }

    public void setGt(FIELD_TYPE gt) {
        this.gt = gt;
    }

    public FIELD_TYPE getLt() {
        return lt;
    }

    public void setLt(FIELD_TYPE lt) {
        this.lt = lt;
    }

    public FIELD_TYPE getGe() {
        return ge;
    }

    public void setGe(FIELD_TYPE ge) {
        this.ge = ge;
    }

    public FIELD_TYPE getLe() {
        return le;
    }

    public void setLe(FIELD_TYPE le) {
        this.le = le;
    }

    @Override
    public String toString() {
        return "MeridianRangeFilter [" + (getGt() != null ? "greaterThan=" + getGt() + ", " : "")
            + (getGe() != null ? "greaterOrEqualThan=" + getGe() + ", " : "")
            + (getLt() != null ? "lessThan=" + getLt() + ", " : "")
            + (getLe() != null ? "lessOrEqualThan=" + getLe() + ", " : "")
            + (getEq() != null ? "equals=" + getEq() + ", " : "")
            + (getNe() != null ? "not equals=" + getNe() + ", " : "")
            + (getSpecified() != null ? "specified=" + getSpecified() : "")
            + (getIn() != null ? "in=" + getIn() : "")
            + "]";
    }

}
