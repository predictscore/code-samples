package biz.newparadigm.meridian.service.odata;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExpressionBuilder {
    private String expression; // initial expression
    private int p = 0; // current position
    private final Logger log = LoggerFactory.getLogger(ExpressionBuilder.class);

    public static void main1(String...args) {
        ExpressionBuilder.build("name eq 'HUIDS f f fhgdherh*GY#' AND location ge her");
    }

    public static Expression build(String expression) {
        try {
            if (expression == null || expression.trim().isEmpty()) {
                return null;
            }
            ExpressionBuilder builder = new ExpressionBuilder(expression.trim());
            builder.skip(" ");
            Expression expr = builder.build(0);
            Gson gson = new Gson();
            System.out.println(gson.toJson(expr));
            return expr;
        } catch (Exception e) {
            return null;
        }
    }

    private ExpressionBuilder(String expression) {
        this.expression = expression;
    }

    Expression build(int state) {
        if (lastState(state)) {
            Expression ex = null;
//            boolean isMinus = startWith("-");
//            if (isMinus) {
//                skip("-");
//            }

            if (startWith("(")) {
                skip("(");
                ex = build(0);
                skip(")");
            } else {
                ex = readSingle();
            }

//            if (isMinus) {
//                ex = new Expression.Unary(ex, "-");
//            }

            return ex;
        }

        boolean unarNot = state == 2 && (startWith("not") || startWith("NOT"));
        if (unarNot) {
            skip("not");
        }

        Expression a1 = build(state + 1);
        if (unarNot) {
            a1 = new Expression.Unary(a1, "not");
        }

        String op = null;
        while ((op = readStateOperator(state)) != null) {
            Expression a2 = build(state + 1);
            a1 = new Expression.Binary(a1, a2, op);
        }
        return a1;
    }

    private static String[][] states = new String[][]{
        {"OR", "or"},
        {"AND", "and"},
        {"NOT", "not"},
        {"LE", "GE", "EQ", "NE", "LT", "GT", "le", "ge", "eq", "ne", "lt", "gt", "startswith", "endswith", "contains"},
//        {"+", "-"},
//        {"*", "/"},
        null
    };

    private boolean lastState(int s) {
        return s + 1 >= states.length;
    }

    private boolean startWith(String s) {
        return (expression.startsWith(s, p) || expression.startsWith(s.toUpperCase(), p));
    }

    private void skip(String s) {
        if (startWith(s)) {
            p += s.length();
        }
        while (p < expression.length() && expression.charAt(p) == ' ') {
            p++;
        }
    }

    private String readStateOperator(int state) {
        String[] ops = states[state];
        for (String s : ops) {
            if (startWith(s)) {
                skip(s);
                return s;
            }
        }
        return null;
    }

    private Expression readSingle() {
        int p0 = p;
        if (startWith("'") || startWith("\"")) {
            boolean q2 = startWith("\"");
            p = expression.indexOf(q2 ? '"' : '\'', p + 1);
            Expression ex = new Expression.Str(expression.substring(p0 + 1, p));
            skip(q2 ? "\"" : "'");
            return ex;
        }

        while (p < expression.length()) {
//            if (!(Character.isLetterOrDigit(expression.charAt(p)))
//                && expression.charAt(p) != "-".charAt(0) && expression.charAt(p) != ":".charAt(0)
//                && expression.charAt(p) != "+".charAt(0) && expression.charAt(p) != ".".charAt(0)) {
//                break;
//            }
            if (Character.isSpaceChar(expression.charAt(p)) || expression.charAt(p) == ")".charAt(0)) {
                break;
            }
            p++;
        }

        Expression ex = null;
        if (p > p0) {
            String s = expression.substring(p0, p);
            skip(" ");
            try {
                long x = Long.parseLong(s);
                return new Expression.Num(x);
            } catch (Exception e) {
            }

            if ("null".equals(s)) {
                return new Expression.Str(null);
            }

            return new Expression.Var(s);
        }
        return null;
    }
}
