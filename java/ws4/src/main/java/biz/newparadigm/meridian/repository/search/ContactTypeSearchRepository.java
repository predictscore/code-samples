package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.ContactType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ContactType entity.
 */
public interface ContactTypeSearchRepository extends ElasticsearchRepository<ContactType, Long> {
}
