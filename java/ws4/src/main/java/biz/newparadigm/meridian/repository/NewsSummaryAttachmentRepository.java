package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsSummaryAttachment;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsSummaryAttachment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsSummaryAttachmentRepository extends JpaRepository<NewsSummaryAttachment, Long>, JpaSpecificationExecutor<NewsSummaryAttachment> {

}
