package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsSummaryToTag;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsSummaryToTag entity.
 */
public interface NewsSummaryToTagSearchRepository extends ElasticsearchRepository<NewsSummaryToTag, Long> {
}
