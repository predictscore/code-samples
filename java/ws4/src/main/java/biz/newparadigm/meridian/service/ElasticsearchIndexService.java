package biz.newparadigm.meridian.service;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonIgnore;
import biz.newparadigm.meridian.domain.*;
import biz.newparadigm.meridian.repository.*;
import biz.newparadigm.meridian.repository.search.*;
import org.elasticsearch.indices.IndexAlreadyExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.ManyToMany;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class ElasticsearchIndexService {

    private final Logger log = LoggerFactory.getLogger(ElasticsearchIndexService.class);

    private final AuthorRepository authorRepository;

    private final AuthorSearchRepository authorSearchRepository;

    private final AuthorCommentRepository authorCommentRepository;

    private final AuthorCommentSearchRepository authorCommentSearchRepository;

    private final AuthorNameRepository authorNameRepository;

    private final AuthorNameSearchRepository authorNameSearchRepository;

    private final AuthorProfileRepository authorProfileRepository;

    private final AuthorProfileSearchRepository authorProfileSearchRepository;

    private final AuthorToContactRepository authorToContactRepository;

    private final AuthorToContactSearchRepository authorToContactSearchRepository;

    private final AuthorToLanguageRepository authorToLanguageRepository;

    private final AuthorToLanguageSearchRepository authorToLanguageSearchRepository;

    private final AuthorToSiteChatgroupRepository authorToSiteChatgroupRepository;

    private final AuthorToSiteChatgroupSearchRepository authorToSiteChatgroupSearchRepository;

    private final AuthorToTagRepository authorToTagRepository;

    private final AuthorToTagSearchRepository authorToTagSearchRepository;

    private final CategoryRepository categoryRepository;

    private final CategorySearchRepository categorySearchRepository;

    private final ContactTypeRepository contactTypeRepository;

    private final ContactTypeSearchRepository contactTypeSearchRepository;

    private final LanguageRepository languageRepository;

    private final LanguageSearchRepository languageSearchRepository;

    private final LocationRepository locationRepository;

    private final LocationSearchRepository locationSearchRepository;

    private final MetafieldRepository metafieldRepository;

    private final MetafieldSearchRepository metafieldSearchRepository;

    private final MetagroupRepository metagroupRepository;

    private final MetagroupSearchRepository metagroupSearchRepository;

    private final MitigationsBypassedRepository mitigationsBypassedRepository;

    private final MitigationsBypassedSearchRepository mitigationsBypassedSearchRepository;

    private final NewsRepository newsRepository;

    private final NewsSearchRepository newsSearchRepository;

    private final NewsAttachmentRepository newsAttachmentRepository;

    private final NewsAttachmentSearchRepository newsAttachmentSearchRepository;

    private final NewsCommentRepository newsCommentRepository;

    private final NewsCommentSearchRepository newsCommentSearchRepository;

    private final NewsConfidentialImageRepository newsConfidentialImageRepository;

    private final NewsConfidentialImageSearchRepository newsConfidentialImageSearchRepository;

    private final NewsDownloadableContentUrlRepository newsDownloadableContentUrlRepository;

    private final NewsDownloadableContentUrlSearchRepository newsDownloadableContentUrlSearchRepository;

    private final NewsMetadataRepository newsMetadataRepository;

    private final NewsMetadataSearchRepository newsMetadataSearchRepository;

    private final NewsOriginalImageRepository newsOriginalImageRepository;

    private final NewsOriginalImageSearchRepository newsOriginalImageSearchRepository;

    private final NewsPublicImageRepository newsPublicImageRepository;

    private final NewsPublicImageSearchRepository newsPublicImageSearchRepository;

    private final NewsSummaryRepository newsSummaryRepository;

    private final NewsSummarySearchRepository newsSummarySearchRepository;

    private final NewsSummaryAttachmentRepository newsSummaryAttachmentRepository;

    private final NewsSummaryAttachmentSearchRepository newsSummaryAttachmentSearchRepository;

    private final NewsSummaryCommentRepository newsSummaryCommentRepository;

    private final NewsSummaryCommentSearchRepository newsSummaryCommentSearchRepository;

    private final NewsSummaryConfidentialImageRepository newsSummaryConfidentialImageRepository;

    private final NewsSummaryConfidentialImageSearchRepository newsSummaryConfidentialImageSearchRepository;

    private final NewsSummaryMetadataRepository newsSummaryMetadataRepository;

    private final NewsSummaryMetadataSearchRepository newsSummaryMetadataSearchRepository;

    private final NewsSummaryPublicImageRepository newsSummaryPublicImageRepository;

    private final NewsSummaryPublicImageSearchRepository newsSummaryPublicImageSearchRepository;

    private final NewsSummaryToNewsRepository newsSummaryToNewsRepository;

    private final NewsSummaryToNewsSearchRepository newsSummaryToNewsSearchRepository;

    private final NewsSummaryToTagRepository newsSummaryToTagRepository;

    private final NewsSummaryToTagSearchRepository newsSummaryToTagSearchRepository;

    private final NewsToLanguageRepository newsToLanguageRepository;

    private final NewsToLanguageSearchRepository newsToLanguageSearchRepository;

  //  private final NewsToRelatedAuthorRepository newsToRelatedAuthorRepository;

  //  private final NewsToRelatedAuthorSearchRepository newsToRelatedAuthorSearchRepository;

    private final NewsToTagRepository newsToTagRepository;

    private final NewsToTagSearchRepository newsToTagSearchRepository;

    private final NewsUrlRepository newsUrlRepository;

    private final NewsUrlSearchRepository newsUrlSearchRepository;

    private final RFIResponseRepository rFIResponseRepository;

    private final RFIResponseSearchRepository rFIResponseSearchRepository;

    private final SiteChatgroupRepository siteChatgroupRepository;

    private final SiteChatgroupSearchRepository siteChatgroupSearchRepository;

    private final SiteChatgroupCommentRepository siteChatgroupCommentRepository;

    private final SiteChatgroupCommentSearchRepository siteChatgroupCommentSearchRepository;

    private final SiteChatgroupDomainRepository siteChatgroupDomainRepository;

    private final SiteChatgroupDomainSearchRepository siteChatgroupDomainSearchRepository;

    private final SiteChatgroupProfileRepository siteChatgroupInfoRepository;

    private final SiteChatgroupProfileSearchRepository siteChatgroupInfoSearchRepository;

    private final SiteChatgroupIpRepository siteChatgroupIpRepository;

    private final SiteChatgroupIpSearchRepository siteChatgroupIpSearchRepository;

    private final SiteChatgroupNameRepository siteChatgroupNameRepository;

    private final SiteChatgroupNameSearchRepository siteChatgroupNameSearchRepository;

    private final SiteChatgroupNumUsersRepository siteChatgroupNumUsersRepository;

    private final SiteChatgroupNumUsersSearchRepository siteChatgroupNumUsersSearchRepository;

    private final SiteChatgroupNumVisitorsRepository siteChatgroupNumVisitorsRepository;

    private final SiteChatgroupNumVisitorsSearchRepository siteChatgroupNumVisitorsSearchRepository;

    private final SiteChatgroupToLanguageRepository siteChatgroupToLanguageRepository;

    private final SiteChatgroupToLanguageSearchRepository siteChatgroupToLanguageSearchRepository;

    private final SiteChatgroupToTagRepository siteChatgroupToTagRepository;

    private final SiteChatgroupToTagSearchRepository siteChatgroupToTagSearchRepository;

    private final TagRepository tagRepository;

    private final TagSearchRepository tagSearchRepository;

    private final UserRepository userRepository;

    private final UserSearchRepository userSearchRepository;

    private final ElasticsearchTemplate elasticsearchTemplate;

    private static final Lock reindexLock = new ReentrantLock();

    public ElasticsearchIndexService(
        UserRepository userRepository,
        UserSearchRepository userSearchRepository,
        AuthorRepository authorRepository,
        AuthorSearchRepository authorSearchRepository,
        AuthorCommentRepository authorCommentRepository,
        AuthorCommentSearchRepository authorCommentSearchRepository,
        AuthorNameRepository authorNameRepository,
        AuthorNameSearchRepository authorNameSearchRepository,
        AuthorProfileRepository authorProfileRepository,
        AuthorProfileSearchRepository authorProfileSearchRepository,
        AuthorToContactRepository authorToContactRepository,
        AuthorToContactSearchRepository authorToContactSearchRepository,
        AuthorToLanguageRepository authorToLanguageRepository,
        AuthorToLanguageSearchRepository authorToLanguageSearchRepository,
        AuthorToSiteChatgroupRepository authorToSiteChatgroupRepository,
        AuthorToSiteChatgroupSearchRepository authorToSiteChatgroupSearchRepository,
        AuthorToTagRepository authorToTagRepository,
        AuthorToTagSearchRepository authorToTagSearchRepository,
        CategoryRepository categoryRepository,
        CategorySearchRepository categorySearchRepository,
        ContactTypeRepository contactTypeRepository,
        ContactTypeSearchRepository contactTypeSearchRepository,
        LanguageRepository languageRepository,
        LanguageSearchRepository languageSearchRepository,
        LocationRepository locationRepository,
        LocationSearchRepository locationSearchRepository,
        MetafieldRepository metafieldRepository,
        MetafieldSearchRepository metafieldSearchRepository,
        MetagroupRepository metagroupRepository,
        MetagroupSearchRepository metagroupSearchRepository,
        MitigationsBypassedRepository mitigationsBypassedRepository,
        MitigationsBypassedSearchRepository mitigationsBypassedSearchRepository,
        NewsRepository newsRepository,
        NewsSearchRepository newsSearchRepository,
        NewsAttachmentRepository newsAttachmentRepository,
        NewsAttachmentSearchRepository newsAttachmentSearchRepository,
        NewsCommentRepository newsCommentRepository,
        NewsCommentSearchRepository newsCommentSearchRepository,
        NewsConfidentialImageRepository newsConfidentialImageRepository,
        NewsConfidentialImageSearchRepository newsConfidentialImageSearchRepository,
        NewsDownloadableContentUrlRepository newsDownloadableContentUrlRepository,
        NewsDownloadableContentUrlSearchRepository newsDownloadableContentUrlSearchRepository,
        NewsMetadataRepository newsMetadataRepository,
        NewsMetadataSearchRepository newsMetadataSearchRepository,
        NewsOriginalImageRepository newsOriginalImageRepository,
        NewsOriginalImageSearchRepository newsOriginalImageSearchRepository,
        NewsPublicImageRepository newsPublicImageRepository,
        NewsPublicImageSearchRepository newsPublicImageSearchRepository,
        NewsSummaryRepository newsSummaryRepository,
        NewsSummarySearchRepository newsSummarySearchRepository,
        NewsSummaryAttachmentRepository newsSummaryAttachmentRepository,
        NewsSummaryAttachmentSearchRepository newsSummaryAttachmentSearchRepository,
        NewsSummaryCommentRepository newsSummaryCommentRepository,
        NewsSummaryCommentSearchRepository newsSummaryCommentSearchRepository,
        NewsSummaryConfidentialImageRepository newsSummaryConfidentialImageRepository,
        NewsSummaryConfidentialImageSearchRepository newsSummaryConfidentialImageSearchRepository,
        NewsSummaryMetadataRepository newsSummaryMetadataRepository,
        NewsSummaryMetadataSearchRepository newsSummaryMetadataSearchRepository,
        NewsSummaryPublicImageRepository newsSummaryPublicImageRepository,
        NewsSummaryPublicImageSearchRepository newsSummaryPublicImageSearchRepository,
        NewsSummaryToNewsRepository newsSummaryToNewsRepository,
        NewsSummaryToNewsSearchRepository newsSummaryToNewsSearchRepository,
        NewsSummaryToTagRepository newsSummaryToTagRepository,
        NewsSummaryToTagSearchRepository newsSummaryToTagSearchRepository,
        NewsToLanguageRepository newsToLanguageRepository,
        NewsToLanguageSearchRepository newsToLanguageSearchRepository,
    //    NewsToRelatedAuthorRepository newsToRelatedAuthorRepository,
    //    NewsToRelatedAuthorSearchRepository newsToRelatedAuthorSearchRepository,
        NewsToTagRepository newsToTagRepository,
        NewsToTagSearchRepository newsToTagSearchRepository,
        NewsUrlRepository newsUrlRepository,
        NewsUrlSearchRepository newsUrlSearchRepository,
        RFIResponseRepository rFIResponseRepository,
        RFIResponseSearchRepository rFIResponseSearchRepository,
        SiteChatgroupRepository siteChatgroupRepository,
        SiteChatgroupSearchRepository siteChatgroupSearchRepository,
        SiteChatgroupCommentRepository siteChatgroupCommentRepository,
        SiteChatgroupCommentSearchRepository siteChatgroupCommentSearchRepository,
        SiteChatgroupDomainRepository siteChatgroupDomainRepository,
        SiteChatgroupDomainSearchRepository siteChatgroupDomainSearchRepository,
        SiteChatgroupProfileRepository siteChatgroupInfoRepository,
        SiteChatgroupProfileSearchRepository siteChatgroupInfoSearchRepository,
        SiteChatgroupIpRepository siteChatgroupIpRepository,
        SiteChatgroupIpSearchRepository siteChatgroupIpSearchRepository,
        SiteChatgroupNameRepository siteChatgroupNameRepository,
        SiteChatgroupNameSearchRepository siteChatgroupNameSearchRepository,
        SiteChatgroupNumUsersRepository siteChatgroupNumUsersRepository,
        SiteChatgroupNumUsersSearchRepository siteChatgroupNumUsersSearchRepository,
        SiteChatgroupNumVisitorsRepository siteChatgroupNumVisitorsRepository,
        SiteChatgroupNumVisitorsSearchRepository siteChatgroupNumVisitorsSearchRepository,
        SiteChatgroupToLanguageRepository siteChatgroupToLanguageRepository,
        SiteChatgroupToLanguageSearchRepository siteChatgroupToLanguageSearchRepository,
        SiteChatgroupToTagRepository siteChatgroupToTagRepository,
        SiteChatgroupToTagSearchRepository siteChatgroupToTagSearchRepository,
        TagRepository tagRepository,
        TagSearchRepository tagSearchRepository,
        ElasticsearchTemplate elasticsearchTemplate) {
        this.userRepository = userRepository;
        this.userSearchRepository = userSearchRepository;
        this.authorRepository = authorRepository;
        this.authorSearchRepository = authorSearchRepository;
        this.authorCommentRepository = authorCommentRepository;
        this.authorCommentSearchRepository = authorCommentSearchRepository;
        this.authorNameRepository = authorNameRepository;
        this.authorNameSearchRepository = authorNameSearchRepository;
        this.authorProfileRepository = authorProfileRepository;
        this.authorProfileSearchRepository = authorProfileSearchRepository;
        this.authorToContactRepository = authorToContactRepository;
        this.authorToContactSearchRepository = authorToContactSearchRepository;
        this.authorToLanguageRepository = authorToLanguageRepository;
        this.authorToLanguageSearchRepository = authorToLanguageSearchRepository;
        this.authorToSiteChatgroupRepository = authorToSiteChatgroupRepository;
        this.authorToSiteChatgroupSearchRepository = authorToSiteChatgroupSearchRepository;
        this.authorToTagRepository = authorToTagRepository;
        this.authorToTagSearchRepository = authorToTagSearchRepository;
        this.categoryRepository = categoryRepository;
        this.categorySearchRepository = categorySearchRepository;
        this.contactTypeRepository = contactTypeRepository;
        this.contactTypeSearchRepository = contactTypeSearchRepository;
        this.languageRepository = languageRepository;
        this.languageSearchRepository = languageSearchRepository;
        this.locationRepository = locationRepository;
        this.locationSearchRepository = locationSearchRepository;
        this.metafieldRepository = metafieldRepository;
        this.metafieldSearchRepository = metafieldSearchRepository;
        this.metagroupRepository = metagroupRepository;
        this.metagroupSearchRepository = metagroupSearchRepository;
        this.mitigationsBypassedRepository = mitigationsBypassedRepository;
        this.mitigationsBypassedSearchRepository = mitigationsBypassedSearchRepository;
        this.newsRepository = newsRepository;
        this.newsSearchRepository = newsSearchRepository;
        this.newsAttachmentRepository = newsAttachmentRepository;
        this.newsAttachmentSearchRepository = newsAttachmentSearchRepository;
        this.newsCommentRepository = newsCommentRepository;
        this.newsCommentSearchRepository = newsCommentSearchRepository;
        this.newsConfidentialImageRepository = newsConfidentialImageRepository;
        this.newsConfidentialImageSearchRepository = newsConfidentialImageSearchRepository;
        this.newsDownloadableContentUrlRepository = newsDownloadableContentUrlRepository;
        this.newsDownloadableContentUrlSearchRepository = newsDownloadableContentUrlSearchRepository;
        this.newsMetadataRepository = newsMetadataRepository;
        this.newsMetadataSearchRepository = newsMetadataSearchRepository;
        this.newsOriginalImageRepository = newsOriginalImageRepository;
        this.newsOriginalImageSearchRepository = newsOriginalImageSearchRepository;
        this.newsPublicImageRepository = newsPublicImageRepository;
        this.newsPublicImageSearchRepository = newsPublicImageSearchRepository;
        this.newsSummaryRepository = newsSummaryRepository;
        this.newsSummarySearchRepository = newsSummarySearchRepository;
        this.newsSummaryAttachmentRepository = newsSummaryAttachmentRepository;
        this.newsSummaryAttachmentSearchRepository = newsSummaryAttachmentSearchRepository;
        this.newsSummaryCommentRepository = newsSummaryCommentRepository;
        this.newsSummaryCommentSearchRepository = newsSummaryCommentSearchRepository;
        this.newsSummaryConfidentialImageRepository = newsSummaryConfidentialImageRepository;
        this.newsSummaryConfidentialImageSearchRepository = newsSummaryConfidentialImageSearchRepository;
        this.newsSummaryMetadataRepository = newsSummaryMetadataRepository;
        this.newsSummaryMetadataSearchRepository = newsSummaryMetadataSearchRepository;
        this.newsSummaryPublicImageRepository = newsSummaryPublicImageRepository;
        this.newsSummaryPublicImageSearchRepository = newsSummaryPublicImageSearchRepository;
        this.newsSummaryToNewsRepository = newsSummaryToNewsRepository;
        this.newsSummaryToNewsSearchRepository = newsSummaryToNewsSearchRepository;
        this.newsSummaryToTagRepository = newsSummaryToTagRepository;
        this.newsSummaryToTagSearchRepository = newsSummaryToTagSearchRepository;
        this.newsToLanguageRepository = newsToLanguageRepository;
        this.newsToLanguageSearchRepository = newsToLanguageSearchRepository;
     //   this.newsToRelatedAuthorRepository = newsToRelatedAuthorRepository;
    //    this.newsToRelatedAuthorSearchRepository = newsToRelatedAuthorSearchRepository;
        this.newsToTagRepository = newsToTagRepository;
        this.newsToTagSearchRepository = newsToTagSearchRepository;
        this.newsUrlRepository = newsUrlRepository;
        this.newsUrlSearchRepository = newsUrlSearchRepository;
        this.rFIResponseRepository = rFIResponseRepository;
        this.rFIResponseSearchRepository = rFIResponseSearchRepository;
        this.siteChatgroupRepository = siteChatgroupRepository;
        this.siteChatgroupSearchRepository = siteChatgroupSearchRepository;
        this.siteChatgroupCommentRepository = siteChatgroupCommentRepository;
        this.siteChatgroupCommentSearchRepository = siteChatgroupCommentSearchRepository;
        this.siteChatgroupDomainRepository = siteChatgroupDomainRepository;
        this.siteChatgroupDomainSearchRepository = siteChatgroupDomainSearchRepository;
        this.siteChatgroupInfoRepository = siteChatgroupInfoRepository;
        this.siteChatgroupInfoSearchRepository = siteChatgroupInfoSearchRepository;
        this.siteChatgroupIpRepository = siteChatgroupIpRepository;
        this.siteChatgroupIpSearchRepository = siteChatgroupIpSearchRepository;
        this.siteChatgroupNameRepository = siteChatgroupNameRepository;
        this.siteChatgroupNameSearchRepository = siteChatgroupNameSearchRepository;
        this.siteChatgroupNumUsersRepository = siteChatgroupNumUsersRepository;
        this.siteChatgroupNumUsersSearchRepository = siteChatgroupNumUsersSearchRepository;
        this.siteChatgroupNumVisitorsRepository = siteChatgroupNumVisitorsRepository;
        this.siteChatgroupNumVisitorsSearchRepository = siteChatgroupNumVisitorsSearchRepository;
        this.siteChatgroupToLanguageRepository = siteChatgroupToLanguageRepository;
        this.siteChatgroupToLanguageSearchRepository = siteChatgroupToLanguageSearchRepository;
        this.siteChatgroupToTagRepository = siteChatgroupToTagRepository;
        this.siteChatgroupToTagSearchRepository = siteChatgroupToTagSearchRepository;
        this.tagRepository = tagRepository;
        this.tagSearchRepository = tagSearchRepository;
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @Async
    @Timed
    public void reindexAll() {
        if(reindexLock.tryLock()) {
            try {
                reindexForClass(Author.class, authorRepository, authorSearchRepository);
                reindexForClass(AuthorComment.class, authorCommentRepository, authorCommentSearchRepository);
                reindexForClass(AuthorName.class, authorNameRepository, authorNameSearchRepository);
                reindexForClass(AuthorProfile.class, authorProfileRepository, authorProfileSearchRepository);
                reindexForClass(AuthorToContact.class, authorToContactRepository, authorToContactSearchRepository);
                reindexForClass(AuthorToLanguage.class, authorToLanguageRepository, authorToLanguageSearchRepository);
                reindexForClass(AuthorToSiteChatgroup.class, authorToSiteChatgroupRepository, authorToSiteChatgroupSearchRepository);
                reindexForClass(AuthorToTag.class, authorToTagRepository, authorToTagSearchRepository);
                reindexForClass(Category.class, categoryRepository, categorySearchRepository);
                reindexForClass(ContactType.class, contactTypeRepository, contactTypeSearchRepository);
                reindexForClass(Language.class, languageRepository, languageSearchRepository);
                reindexForClass(Location.class, locationRepository, locationSearchRepository);
                reindexForClass(Metafield.class, metafieldRepository, metafieldSearchRepository);
                reindexForClass(Metagroup.class, metagroupRepository, metagroupSearchRepository);
                reindexForClass(MitigationsBypassed.class, mitigationsBypassedRepository, mitigationsBypassedSearchRepository);
                reindexForClass(News.class, newsRepository, newsSearchRepository);
                reindexForClass(NewsAttachment.class, newsAttachmentRepository, newsAttachmentSearchRepository);
                reindexForClass(NewsComment.class, newsCommentRepository, newsCommentSearchRepository);
                reindexForClass(NewsConfidentialImage.class, newsConfidentialImageRepository, newsConfidentialImageSearchRepository);
                reindexForClass(NewsDownloadableContentUrl.class, newsDownloadableContentUrlRepository, newsDownloadableContentUrlSearchRepository);
                reindexForClass(NewsMetadata.class, newsMetadataRepository, newsMetadataSearchRepository);
                reindexForClass(NewsOriginalImage.class, newsOriginalImageRepository, newsOriginalImageSearchRepository);
                reindexForClass(NewsPublicImage.class, newsPublicImageRepository, newsPublicImageSearchRepository);
                reindexForClass(NewsSummary.class, newsSummaryRepository, newsSummarySearchRepository);
                reindexForClass(NewsSummaryAttachment.class, newsSummaryAttachmentRepository, newsSummaryAttachmentSearchRepository);
                reindexForClass(NewsSummaryComment.class, newsSummaryCommentRepository, newsSummaryCommentSearchRepository);
                reindexForClass(NewsSummaryConfidentialImage.class, newsSummaryConfidentialImageRepository, newsSummaryConfidentialImageSearchRepository);
                reindexForClass(NewsSummaryMetadata.class, newsSummaryMetadataRepository, newsSummaryMetadataSearchRepository);
                reindexForClass(NewsSummaryPublicImage.class, newsSummaryPublicImageRepository, newsSummaryPublicImageSearchRepository);
                reindexForClass(NewsSummaryToNews.class, newsSummaryToNewsRepository, newsSummaryToNewsSearchRepository);
                reindexForClass(NewsSummaryToTag.class, newsSummaryToTagRepository, newsSummaryToTagSearchRepository);
                reindexForClass(NewsToLanguage.class, newsToLanguageRepository, newsToLanguageSearchRepository);
               // reindexForClass(NewsToRelatedAuthor.class, newsToRelatedAuthorRepository, newsToRelatedAuthorSearchRepository);
                reindexForClass(NewsToTag.class, newsToTagRepository, newsToTagSearchRepository);
                reindexForClass(NewsUrl.class, newsUrlRepository, newsUrlSearchRepository);
                reindexForClass(RFIResponse.class, rFIResponseRepository, rFIResponseSearchRepository);
                reindexForClass(SiteChatgroup.class, siteChatgroupRepository, siteChatgroupSearchRepository);
                reindexForClass(SiteChatgroupComment.class, siteChatgroupCommentRepository, siteChatgroupCommentSearchRepository);
                reindexForClass(SiteChatgroupDomain.class, siteChatgroupDomainRepository, siteChatgroupDomainSearchRepository);
                reindexForClass(SiteChatgroupProfile.class, siteChatgroupInfoRepository, siteChatgroupInfoSearchRepository);
                reindexForClass(SiteChatgroupIp.class, siteChatgroupIpRepository, siteChatgroupIpSearchRepository);
                reindexForClass(SiteChatgroupName.class, siteChatgroupNameRepository, siteChatgroupNameSearchRepository);
                reindexForClass(SiteChatgroupNumUsers.class, siteChatgroupNumUsersRepository, siteChatgroupNumUsersSearchRepository);
                reindexForClass(SiteChatgroupNumVisitors.class, siteChatgroupNumVisitorsRepository, siteChatgroupNumVisitorsSearchRepository);
                reindexForClass(SiteChatgroupToLanguage.class, siteChatgroupToLanguageRepository, siteChatgroupToLanguageSearchRepository);
                reindexForClass(SiteChatgroupToTag.class, siteChatgroupToTagRepository, siteChatgroupToTagSearchRepository);
                reindexForClass(Tag.class, tagRepository, tagSearchRepository);
                reindexForClass(User.class, userRepository, userSearchRepository);

                log.info("Elasticsearch: Successfully performed reindexing");
            } finally {
                reindexLock.unlock();
            }
        } else {
            log.info("Elasticsearch: concurrent reindexing attempt");
        }
    }

    @SuppressWarnings("unchecked")
    private <T, ID extends Serializable> void reindexForClass(Class<T> entityClass, JpaRepository<T, ID> jpaRepository,
                                                              ElasticsearchRepository<T, ID> elasticsearchRepository) {
        elasticsearchTemplate.deleteIndex(entityClass);
        try {
            elasticsearchTemplate.createIndex(entityClass);
        } catch (IndexAlreadyExistsException e) {
            // Do nothing. Index was already concurrently recreated by some other service.
        }
        elasticsearchTemplate.putMapping(entityClass);
        if (jpaRepository.count() > 0) {
            // if a JHipster entity field is the owner side of a many-to-many relationship, it should be loaded manually
            List<Method> relationshipGetters = Arrays.stream(entityClass.getDeclaredFields())
                .filter(field -> field.getType().equals(Set.class))
                .filter(field -> field.getAnnotation(ManyToMany.class) != null)
                .filter(field -> field.getAnnotation(ManyToMany.class).mappedBy().isEmpty())
                .filter(field -> field.getAnnotation(JsonIgnore.class) == null)
                .map(field -> {
                    try {
                        return new PropertyDescriptor(field.getName(), entityClass).getReadMethod();
                    } catch (IntrospectionException e) {
                        log.error("Error retrieving getter for class {}, field {}. Field will NOT be indexed",
                            entityClass.getSimpleName(), field.getName(), e);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

            int size = 100;
            for (int i = 0; i <= jpaRepository.count() / size; i++) {
                Pageable page = new PageRequest(i, size);
                log.info("Indexing page {} of {}, size {}", i, jpaRepository.count() / size, size);
                Page<T> results = jpaRepository.findAll(page);
                results.map(result -> {
                    // if there are any relationships to load, do it now
                    relationshipGetters.forEach(method -> {
                        try {
                            // eagerly load the relationship set
                            ((Set) method.invoke(result)).size();
                        } catch (Exception ex) {
                            log.error(ex.getMessage());
                        }
                    });
                    return result;
                });
                elasticsearchRepository.save(results.getContent());
            }
        }
        log.info("Elasticsearch: Indexed all rows for {}", entityClass.getSimpleName());
    }
}
