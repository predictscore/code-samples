package biz.newparadigm.meridian.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.AuthorComment;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.AuthorCommentRepository;
import biz.newparadigm.meridian.repository.search.AuthorCommentSearchRepository;
import biz.newparadigm.meridian.service.dto.AuthorCommentCriteria;


/**
 * Service for executing complex queries for AuthorComment entities in the database.
 * The main input is a {@link AuthorCommentCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link AuthorComment} or a {@link Page} of {%link AuthorComment} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class AuthorCommentQueryService extends QueryService<AuthorComment> {

    private final Logger log = LoggerFactory.getLogger(AuthorCommentQueryService.class);


    private final AuthorCommentRepository authorCommentRepository;

    private final AuthorCommentSearchRepository authorCommentSearchRepository;

    public AuthorCommentQueryService(AuthorCommentRepository authorCommentRepository, AuthorCommentSearchRepository authorCommentSearchRepository) {
        this.authorCommentRepository = authorCommentRepository;
        this.authorCommentSearchRepository = authorCommentSearchRepository;
    }

    /**
     * Return a {@link List} of {%link AuthorComment} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuthorComment> findByCriteria(AuthorCommentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<AuthorComment> specification = createSpecification(criteria);
        return authorCommentRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link AuthorComment} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuthorComment> findByCriteria(AuthorCommentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<AuthorComment> specification = createSpecification(criteria);
        return authorCommentRepository.findAll(specification, page);
    }

    /**
     * Function to convert AuthorCommentCriteria to a {@link Specifications}
     */
    private Specifications<AuthorComment> createSpecification(AuthorCommentCriteria criteria) {
        Specifications<AuthorComment> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AuthorComment_.id));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUserId(), AuthorComment_.userId));
            }
            if (criteria.getComment() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComment(), AuthorComment_.comment));
            }
            if (criteria.getDatetime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDatetime(), AuthorComment_.datetime));
            }
            if (criteria.getAuthorId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getAuthorId(), AuthorComment_.author, Author_.id));
            }
            if (criteria.getParentCommentId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getParentCommentId(), AuthorComment_.parentComment, AuthorComment_.id));
            }
        }
        return specification;
    }

}
