package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.SiteChatgroupIp;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SiteChatgroupIp entity.
 */
public interface SiteChatgroupIpSearchRepository extends ElasticsearchRepository<SiteChatgroupIp, Long> {
}
