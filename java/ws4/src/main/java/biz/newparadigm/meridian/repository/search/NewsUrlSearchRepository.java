package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsUrl;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsUrl entity.
 */
public interface NewsUrlSearchRepository extends ElasticsearchRepository<NewsUrl, Long> {
}
