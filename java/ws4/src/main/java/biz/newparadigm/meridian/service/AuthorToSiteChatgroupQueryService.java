package biz.newparadigm.meridian.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.AuthorToSiteChatgroup;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.AuthorToSiteChatgroupRepository;
import biz.newparadigm.meridian.service.dto.AuthorToSiteChatgroupCriteria;


/**
 * Service for executing complex queries for AuthorToSiteChatgroup entities in the database.
 * The main input is a {@link AuthorToSiteChatgroupCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link AuthorToSiteChatgroup} or a {@link Page} of {%link AuthorToSiteChatgroup} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class AuthorToSiteChatgroupQueryService extends QueryService<AuthorToSiteChatgroup> {

    private final Logger log = LoggerFactory.getLogger(AuthorToSiteChatgroupQueryService.class);

    private final AuthorToSiteChatgroupRepository authorToSiteChatgroupRepository;

    @Autowired
    public AuthorToSiteChatgroupQueryService(AuthorToSiteChatgroupRepository authorToSiteChatgroupRepository) {
        this.authorToSiteChatgroupRepository = authorToSiteChatgroupRepository;
    }

    /**
     * Return a {@link List} of {%link AuthorToSiteChatgroup} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuthorToSiteChatgroup> findByCriteria(AuthorToSiteChatgroupCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<AuthorToSiteChatgroup> specification = createSpecification(criteria);
        return authorToSiteChatgroupRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link AuthorToSiteChatgroup} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuthorToSiteChatgroup> findByCriteria(AuthorToSiteChatgroupCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<AuthorToSiteChatgroup> specification = createSpecification(criteria);
        return authorToSiteChatgroupRepository.findAll(specification, page);
    }

    /**
     * Function to convert AuthorToSiteChatgroupCriteria to a {@link Specifications}
     */
    private Specifications<AuthorToSiteChatgroup> createSpecification(AuthorToSiteChatgroupCriteria criteria) {
        Specifications<AuthorToSiteChatgroup> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AuthorToSiteChatgroup_.id));
            }
            if (criteria.getFromDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFromDate(), AuthorToSiteChatgroup_.fromDate));
            }
            if (criteria.getOriginalName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOriginalName(), AuthorToSiteChatgroup_.originalName));
            }
            if (criteria.getEnglishName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEnglishName(), AuthorToSiteChatgroup_.englishName));
            }
            if (criteria.getProfileLink() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProfileLink(), AuthorToSiteChatgroup_.profileLink));
            }
            if (criteria.getToDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getToDate(), AuthorToSiteChatgroup_.toDate));
            }
            if (criteria.getStaffFunction() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStaffFunction(), AuthorToSiteChatgroup_.staffFunction));
            }
            if (criteria.getAuthorId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getAuthorId(), AuthorToSiteChatgroup_.author, Author_.id));
            }
            if (criteria.getSiteChatgroupId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSiteChatgroupId(), AuthorToSiteChatgroup_.siteChatgroup, SiteChatgroup_.id));
            }
        }
        return specification;
    }

}
