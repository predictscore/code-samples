package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.MitigationsBypassed;
import biz.newparadigm.meridian.repository.MitigationsBypassedRepository;
import biz.newparadigm.meridian.repository.search.MitigationsBypassedSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing MitigationsBypassed.
 */
@Service
@Transactional
public class MitigationsBypassedService {

    private final Logger log = LoggerFactory.getLogger(MitigationsBypassedService.class);

    private final MitigationsBypassedRepository mitigationsBypassedRepository;

    private final MitigationsBypassedSearchRepository mitigationsBypassedSearchRepository;

    public MitigationsBypassedService(MitigationsBypassedRepository mitigationsBypassedRepository, MitigationsBypassedSearchRepository mitigationsBypassedSearchRepository) {
        this.mitigationsBypassedRepository = mitigationsBypassedRepository;
        this.mitigationsBypassedSearchRepository = mitigationsBypassedSearchRepository;
    }

    /**
     * Save a mitigationsBypassed.
     *
     * @param mitigationsBypassed the entity to save
     * @return the persisted entity
     */
    public MitigationsBypassed save(MitigationsBypassed mitigationsBypassed) {
        log.debug("Request to save MitigationsBypassed : {}", mitigationsBypassed);
        MitigationsBypassed result = mitigationsBypassedRepository.save(mitigationsBypassed);
        mitigationsBypassedSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the mitigationsBypasseds.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MitigationsBypassed> findAll(Pageable pageable) {
        log.debug("Request to get all MitigationsBypasseds");
        return mitigationsBypassedRepository.findAll(pageable);
    }

    /**
     *  Get one mitigationsBypassed by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public MitigationsBypassed findOne(Long id) {
        log.debug("Request to get MitigationsBypassed : {}", id);
        return mitigationsBypassedRepository.findOne(id);
    }

    /**
     *  Delete the  mitigationsBypassed by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MitigationsBypassed : {}", id);
        mitigationsBypassedRepository.delete(id);
        mitigationsBypassedSearchRepository.delete(id);
    }

    /**
     * Search for the mitigationsBypassed corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MitigationsBypassed> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MitigationsBypasseds for query {}", query);
        Page<MitigationsBypassed> result = mitigationsBypassedSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
