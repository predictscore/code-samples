package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsSummaryPublicImage;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsSummaryPublicImageRepository;
import biz.newparadigm.meridian.repository.search.NewsSummaryPublicImageSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsSummaryPublicImageCriteria;


/**
 * Service for executing complex queries for NewsSummaryPublicImage entities in the database.
 * The main input is a {@link NewsSummaryPublicImageCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsSummaryPublicImage} or a {@link Page} of {%link NewsSummaryPublicImage} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsSummaryPublicImageQueryService extends QueryService<NewsSummaryPublicImage> {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryPublicImageQueryService.class);


    private final NewsSummaryPublicImageRepository newsSummaryPublicImageRepository;

    private final NewsSummaryPublicImageSearchRepository newsSummaryPublicImageSearchRepository;

    public NewsSummaryPublicImageQueryService(NewsSummaryPublicImageRepository newsSummaryPublicImageRepository, NewsSummaryPublicImageSearchRepository newsSummaryPublicImageSearchRepository) {
        this.newsSummaryPublicImageRepository = newsSummaryPublicImageRepository;
        this.newsSummaryPublicImageSearchRepository = newsSummaryPublicImageSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsSummaryPublicImage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsSummaryPublicImage> findByCriteria(NewsSummaryPublicImageCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsSummaryPublicImage> specification = createSpecification(criteria);
        return newsSummaryPublicImageRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsSummaryPublicImage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryPublicImage> findByCriteria(NewsSummaryPublicImageCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsSummaryPublicImage> specification = createSpecification(criteria);
        return newsSummaryPublicImageRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsSummaryPublicImageCriteria to a {@link Specifications}
     */
    private Specifications<NewsSummaryPublicImage> createSpecification(NewsSummaryPublicImageCriteria criteria) {
        Specifications<NewsSummaryPublicImage> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsSummaryPublicImage_.id));
            }
            if (criteria.getFilename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFilename(), NewsSummaryPublicImage_.filename));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), NewsSummaryPublicImage_.description));
            }
            if (criteria.getDescriptionTranslation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescriptionTranslation(), NewsSummaryPublicImage_.descriptionTranslation));
            }
            if (criteria.getImageOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getImageOrder(), NewsSummaryPublicImage_.imageOrder));
            }
            if (criteria.getNewsSummaryId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsSummaryId(), NewsSummaryPublicImage_.newsSummary, NewsSummary_.id));
            }
        }
        return specification;
    }

}
