package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsSummaryToTag;
import biz.newparadigm.meridian.service.NewsSummaryToTagService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsSummaryToTagCriteria;
import biz.newparadigm.meridian.service.NewsSummaryToTagQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsSummaryToTag.
 */
@RestController
@RequestMapping("/api")
public class NewsSummaryToTagResource {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryToTagResource.class);

    private static final String ENTITY_NAME = "newsSummaryToTag";

    private final NewsSummaryToTagService newsSummaryToTagService;

    private final NewsSummaryToTagQueryService newsSummaryToTagQueryService;

    public NewsSummaryToTagResource(NewsSummaryToTagService newsSummaryToTagService, NewsSummaryToTagQueryService newsSummaryToTagQueryService) {
        this.newsSummaryToTagService = newsSummaryToTagService;
        this.newsSummaryToTagQueryService = newsSummaryToTagQueryService;
    }

    /**
     * POST  /news-summary-to-tags : Create a new newsSummaryToTag.
     *
     * @param newsSummaryToTag the newsSummaryToTag to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsSummaryToTag, or with status 400 (Bad Request) if the newsSummaryToTag has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-summary-to-tags")
    @Timed
    public ResponseEntity<NewsSummaryToTag> createNewsSummaryToTag(@Valid @RequestBody NewsSummaryToTag newsSummaryToTag) throws URISyntaxException {
        log.debug("REST request to save NewsSummaryToTag : {}", newsSummaryToTag);
        if (newsSummaryToTag.getId() != null) {
            throw new BadRequestAlertException("A new newsSummaryToTag cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsSummaryToTag result = newsSummaryToTagService.save(newsSummaryToTag);
        return ResponseEntity.created(new URI("/api/news-summary-to-tags/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-summary-to-tags : Updates an existing newsSummaryToTag.
     *
     * @param newsSummaryToTag the newsSummaryToTag to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsSummaryToTag,
     * or with status 400 (Bad Request) if the newsSummaryToTag is not valid,
     * or with status 500 (Internal Server Error) if the newsSummaryToTag couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-summary-to-tags")
    @Timed
    public ResponseEntity<NewsSummaryToTag> updateNewsSummaryToTag(@Valid @RequestBody NewsSummaryToTag newsSummaryToTag) throws URISyntaxException {
        log.debug("REST request to update NewsSummaryToTag : {}", newsSummaryToTag);
        if (newsSummaryToTag.getId() == null) {
            return createNewsSummaryToTag(newsSummaryToTag);
        }
        NewsSummaryToTag result = newsSummaryToTagService.save(newsSummaryToTag);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsSummaryToTag.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-summary-to-tags : get all the newsSummaryToTags.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsSummaryToTags in body
     */
    @GetMapping("/news-summary-to-tags")
    @Timed
    public ResponseEntity<List<NewsSummaryToTag>> getAllNewsSummaryToTags(NewsSummaryToTagCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsSummaryToTags by criteria: {}", criteria);
        Page<NewsSummaryToTag> page = newsSummaryToTagQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-summary-to-tags");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-summary-to-tags/:id : get the "id" newsSummaryToTag.
     *
     * @param id the id of the newsSummaryToTag to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsSummaryToTag, or with status 404 (Not Found)
     */
    @GetMapping("/news-summary-to-tags/{id}")
    @Timed
    public ResponseEntity<NewsSummaryToTag> getNewsSummaryToTag(@PathVariable Long id) {
        log.debug("REST request to get NewsSummaryToTag : {}", id);
        NewsSummaryToTag newsSummaryToTag = newsSummaryToTagService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsSummaryToTag));
    }

    /**
     * DELETE  /news-summary-to-tags/:id : delete the "id" newsSummaryToTag.
     *
     * @param id the id of the newsSummaryToTag to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-summary-to-tags/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsSummaryToTag(@PathVariable Long id) {
        log.debug("REST request to delete NewsSummaryToTag : {}", id);
        newsSummaryToTagService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-summary-to-tags?query=:query : search for the newsSummaryToTag corresponding
     * to the query.
     *
     * @param query the query of the newsSummaryToTag search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-summary-to-tags")
    @Timed
    public ResponseEntity<List<NewsSummaryToTag>> searchNewsSummaryToTags(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsSummaryToTags for query {}", query);
        Page<NewsSummaryToTag> page = newsSummaryToTagService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-summary-to-tags");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
