package biz.newparadigm.meridian.service.dto.old;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class SiteChatgroupIpDTO {
    private Long siteChatGroupId = -1L;
    private String ipName;
    private String ipWhois;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.dateWithTimeFormat)
    private Date firstUsedDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.dateWithTimeFormat)
    private Date lastUsedDate;

    public Long getSiteChatGroupId() {
        return siteChatGroupId;
    }

    public void setSiteChatGroupId(Long siteChatGroupId) {
        this.siteChatGroupId = siteChatGroupId;
    }

    public String getIpName() {
        return ipName;
    }

    public void setIpName(String ipName) {
        this.ipName = ipName;
    }

    public String getIpWhois() {
        return ipWhois;
    }

    public void setIpWhois(String ipWhois) {
        this.ipWhois = ipWhois;
    }

    public Date getFirstUsedDate() {
        return firstUsedDate;
    }

    public void setFirstUsedDate(Date firstUsedDate) {
        this.firstUsedDate = firstUsedDate;
    }

    public Date getLastUsedDate() {
        return lastUsedDate;
    }

    public void setLastUsedDate(Date lastUsedDate) {
        this.lastUsedDate = lastUsedDate;
    }
}
