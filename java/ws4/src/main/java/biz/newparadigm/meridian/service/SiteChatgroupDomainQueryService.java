package biz.newparadigm.meridian.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.SiteChatgroupDomain;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.SiteChatgroupDomainRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupDomainSearchRepository;
import biz.newparadigm.meridian.service.dto.SiteChatgroupDomainCriteria;


/**
 * Service for executing complex queries for SiteChatgroupDomain entities in the database.
 * The main input is a {@link SiteChatgroupDomainCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link SiteChatgroupDomain} or a {@link Page} of {%link SiteChatgroupDomain} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class SiteChatgroupDomainQueryService extends QueryService<SiteChatgroupDomain> {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupDomainQueryService.class);


    private final SiteChatgroupDomainRepository siteChatgroupDomainRepository;

    private final SiteChatgroupDomainSearchRepository siteChatgroupDomainSearchRepository;

    public SiteChatgroupDomainQueryService(SiteChatgroupDomainRepository siteChatgroupDomainRepository, SiteChatgroupDomainSearchRepository siteChatgroupDomainSearchRepository) {
        this.siteChatgroupDomainRepository = siteChatgroupDomainRepository;
        this.siteChatgroupDomainSearchRepository = siteChatgroupDomainSearchRepository;
    }

    /**
     * Return a {@link List} of {%link SiteChatgroupDomain} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SiteChatgroupDomain> findByCriteria(SiteChatgroupDomainCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<SiteChatgroupDomain> specification = createSpecification(criteria);
        return siteChatgroupDomainRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link SiteChatgroupDomain} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupDomain> findByCriteria(SiteChatgroupDomainCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<SiteChatgroupDomain> specification = createSpecification(criteria);
        return siteChatgroupDomainRepository.findAll(specification, page);
    }

    /**
     * Function to convert SiteChatgroupDomainCriteria to a {@link Specifications}
     */
    private Specifications<SiteChatgroupDomain> createSpecification(SiteChatgroupDomainCriteria criteria) {
        Specifications<SiteChatgroupDomain> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SiteChatgroupDomain_.id));
            }
            if (criteria.getDomainId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDomainId(), SiteChatgroupDomain_.domainId));
            }
            if (criteria.getDomainInfo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDomainInfo(), SiteChatgroupDomain_.domainInfo));
            }
            if (criteria.getFirstUsedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFirstUsedDate(), SiteChatgroupDomain_.firstUsedDate));
            }
            if (criteria.getLastUsedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUsedDate(), SiteChatgroupDomain_.lastUsedDate));
            }
            if (criteria.getSiteChatgroupId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSiteChatgroupId(), SiteChatgroupDomain_.siteChatgroup, SiteChatgroup_.id));
            }
        }
        return specification;
    }

}
