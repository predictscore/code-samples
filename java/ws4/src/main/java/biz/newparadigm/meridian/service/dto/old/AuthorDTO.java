package biz.newparadigm.meridian.service.dto.old;

import java.util.ArrayList;
import java.util.List;

public class AuthorDTO {
    private Long authorId=-1L;
    private String originalName;
    private String englishName;
    private LocationDTO location;
    private List authorContacts = new ArrayList();
    private List authorTags = new ArrayList();
    private List authorLanguages = new ArrayList();
    private List authorSites = new ArrayList();
    private List authorChatGroups = new ArrayList();
    private List authorNames = new ArrayList();
    private List authorInfoList = new ArrayList();

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }

    public List getAuthorContacts() {
        return authorContacts;
    }

    public void setAuthorContacts(List authorContacts) {
        this.authorContacts = authorContacts;
    }

    public List getAuthorTags() {
        return authorTags;
    }

    public void setAuthorTags(List authorTags) {
        this.authorTags = authorTags;
    }

    public List getAuthorLanguages() {
        return authorLanguages;
    }

    public void setAuthorLanguages(List authorLanguages) {
        this.authorLanguages = authorLanguages;
    }

    public List getAuthorSites() {
        return authorSites;
    }

    public void setAuthorSites(List authorSites) {
        this.authorSites = authorSites;
    }

    public List getAuthorChatGroups() {
        return authorChatGroups;
    }

    public void setAuthorChatGroups(List authorChatGroups) {
        this.authorChatGroups = authorChatGroups;
    }

    public List getAuthorNames() {
        return authorNames;
    }

    public void setAuthorNames(List authorNames) {
        this.authorNames = authorNames;
    }

    public List getAuthorInfoList() {
        return authorInfoList;
    }

    public void setAuthorInfoList(List authorInfoList) {
        this.authorInfoList = authorInfoList;
    }
}
