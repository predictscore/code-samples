package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsMetadata;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsMetadata entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsMetadataRepository extends JpaRepository<NewsMetadata, Long>, JpaSpecificationExecutor<NewsMetadata> {

}
