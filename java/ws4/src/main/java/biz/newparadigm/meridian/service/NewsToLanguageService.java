package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsToLanguage;
import biz.newparadigm.meridian.repository.NewsToLanguageRepository;
import biz.newparadigm.meridian.repository.search.NewsToLanguageSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsToLanguage.
 */
@Service
@Transactional
public class NewsToLanguageService {

    private final Logger log = LoggerFactory.getLogger(NewsToLanguageService.class);

    private final NewsToLanguageRepository newsToLanguageRepository;

    private final NewsToLanguageSearchRepository newsToLanguageSearchRepository;

    public NewsToLanguageService(NewsToLanguageRepository newsToLanguageRepository, NewsToLanguageSearchRepository newsToLanguageSearchRepository) {
        this.newsToLanguageRepository = newsToLanguageRepository;
        this.newsToLanguageSearchRepository = newsToLanguageSearchRepository;
    }

    /**
     * Save a newsToLanguage.
     *
     * @param newsToLanguage the entity to save
     * @return the persisted entity
     */
    public NewsToLanguage save(NewsToLanguage newsToLanguage) {
        log.debug("Request to save NewsToLanguage : {}", newsToLanguage);
        NewsToLanguage result = newsToLanguageRepository.save(newsToLanguage);
        newsToLanguageSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsToLanguages.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsToLanguage> findAll(Pageable pageable) {
        log.debug("Request to get all NewsToLanguages");
        return newsToLanguageRepository.findAll(pageable);
    }

    /**
     *  Get one newsToLanguage by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsToLanguage findOne(Long id) {
        log.debug("Request to get NewsToLanguage : {}", id);
        return newsToLanguageRepository.findOne(id);
    }

    /**
     *  Delete the  newsToLanguage by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsToLanguage : {}", id);
        newsToLanguageRepository.delete(id);
        newsToLanguageSearchRepository.delete(id);
    }

    /**
     * Search for the newsToLanguage corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsToLanguage> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsToLanguages for query {}", query);
        Page<NewsToLanguage> result = newsToLanguageSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
