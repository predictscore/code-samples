package biz.newparadigm.meridian.service.mapper;

import biz.newparadigm.meridian.domain.*;
import biz.newparadigm.meridian.service.dto.old.*;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

@Service
public class SiteChatgroupMapper {
    public SiteChatgroupDTO toDTO(SiteChatgroup siteChatgroup, boolean full) {
        SiteChatgroupDTO dto = new SiteChatgroupDTO();
        if (siteChatgroup != null) {
            dto.setSiteChatGroupId(siteChatgroup.getId());
            dto.setIsSite(siteChatgroup.isIsSite());
            dto.setEnglishName(siteChatgroup.getEnglishName());
            dto.setOriginalName(siteChatgroup.getOriginalName());

            dto.setSiteChatGroupLanguages(new ArrayList<>());
            dto.setSiteChatGroupDomains(new ArrayList<>());
            dto.setSiteIps(new ArrayList<>());
            dto.setSiteChatGroupNames(new ArrayList<>());
            dto.setSiteChatGroupNumUsers(new ArrayList<>());
            dto.setSiteChatGroupNumVisitors(new ArrayList<>());
            dto.setSiteChatGroupTags(new ArrayList<>());
            dto.setSiteChatGroupAuthors(new ArrayList<>());

            if (full) {
                if (siteChatgroup.getLocation() != null) {
                    dto.setLocation(toLocationDTO(siteChatgroup.getLocation()));
                }

                if (siteChatgroup.getLanguages() != null && !siteChatgroup.getLanguages().isEmpty()) {
                    siteChatgroup.getLanguages().forEach(l -> dto.getSiteChatGroupLanguages().add(toLanguageDTO(l)));
                }
                if (siteChatgroup.getDomains() != null && !siteChatgroup.getDomains().isEmpty()) {
                    siteChatgroup.getDomains().forEach(d -> dto.getSiteChatGroupDomains().add(toDomainDTO(d, dto.getSiteChatGroupId())));
                }
                if (siteChatgroup.getIps() != null && !siteChatgroup.getIps().isEmpty()) {
                    siteChatgroup.getIps().forEach(i -> dto.getSiteIps().add(toIpDTO(i, dto.getSiteChatGroupId())));
                }
                if (siteChatgroup.getNames() != null && !siteChatgroup.getNames().isEmpty()) {
                    siteChatgroup.getNames().forEach(n -> dto.getSiteChatGroupNames().add(toNameDTO(n, dto.getSiteChatGroupId())));
                }
                if (siteChatgroup.getNumUsers() != null && !siteChatgroup.getNumUsers().isEmpty()) {
                    siteChatgroup.getNumUsers().forEach(n -> dto.getSiteChatGroupNumUsers().add(toNumUserDTO(n, dto.getSiteChatGroupId())));
                }
                if (siteChatgroup.getNumVisitors() != null && !siteChatgroup.getNumVisitors().isEmpty()) {
                    siteChatgroup.getNumVisitors().forEach(n -> dto.getSiteChatGroupNumVisitors().add(toNumVisitorDTO(n, dto.getSiteChatGroupId())));
                }
                if (siteChatgroup.getTags() != null && !siteChatgroup.getTags().isEmpty()) {
                    siteChatgroup.getTags().forEach(t -> dto.getSiteChatGroupTags().add(toTagDTO(t)));
                }
                if (siteChatgroup.getAuthors() != null && !siteChatgroup.getAuthors().isEmpty()) {
                    siteChatgroup.getAuthors().forEach(a -> {
                        if (a.getStaffFunction() != null && !a.getStaffFunction().isEmpty()) {
                            dto.getSiteChatGroupAuthors().add(toAuthorDTO(a, dto.getSiteChatGroupId()));
                        }
                    });
                }
            }
        }
        return dto;
    }

    private LocationDTO toLocationDTO(Location location) {
        LocationDTO dto = new LocationDTO();
        if (location != null) {
            dto.setLocationId(location.getId());
            dto.setLocationName(location.getName());
        }
        return dto;
    }

    private LanguageDTO toLanguageDTO(SiteChatgroupToLanguage language) {
        LanguageDTO dto = new LanguageDTO();
        if (language != null && language.getLanguage() != null) {
            dto.setLanguageId(language.getLanguage().getId());
            dto.setLanguageName(language.getLanguage().getName());
        }
        return dto;
    }

    private SiteChatgroupDomainDTO toDomainDTO(SiteChatgroupDomain domain, Long siteChatgroupId) {
        SiteChatgroupDomainDTO dto = new SiteChatgroupDomainDTO();
        if (domain != null) {
            dto.setDomainId(domain.getDomainId());
            dto.setDomainInfo(domain.getDomainInfo());
            dto.setFirstUsedDate(new Date(domain.getFirstUsedDate().toInstant().toEpochMilli()));
            dto.setLastUsedDate(new Date(domain.getLastUsedDate().toInstant().toEpochMilli()));
            dto.setSiteChatGroupId(siteChatgroupId);
        }
        return dto;
    }

    private SiteChatgroupIpDTO toIpDTO(SiteChatgroupIp ip, Long siteChatgroupId) {
        SiteChatgroupIpDTO dto = new SiteChatgroupIpDTO();
        if (ip != null) {
           // dto.setIpName((PgInet)ip.getIpName());
            dto.setIpWhois(ip.getIpWhois());
            dto.setFirstUsedDate(new Date(ip.getFirstUsedDate().toInstant().toEpochMilli()));
            dto.setLastUsedDate(new Date(ip.getLastUsedDate().toInstant().toEpochMilli()));
            dto.setSiteChatGroupId(siteChatgroupId);
        }
        return dto;
    }

    private SiteChatgroupNameDTO toNameDTO(SiteChatgroupName name, Long siteChatgroupId) {
        SiteChatgroupNameDTO dto = new SiteChatgroupNameDTO();
        if (name != null) {
            dto.setEnglishName(name.getEnglishName());
            dto.setOriginalName(name.getOriginalName());
            dto.setSiteChatGroupId(siteChatgroupId);
        }
        return dto;
    }

    private SiteChatgroupNumUserDTO toNumUserDTO(SiteChatgroupNumUsers numUser, Long siteChatgroupId) {
        SiteChatgroupNumUserDTO dto = new SiteChatgroupNumUserDTO();
        if (numUser != null) {
            dto.setNumUserDate(new Date(numUser.getNumUserDate().toInstant().toEpochMilli()));
            dto.setNumUsers(numUser.getNumUsers());
            dto.setSiteChatGroupId(siteChatgroupId);
        }
        return dto;
    }

    private SiteChatgroupNumVisitorDTO toNumVisitorDTO(SiteChatgroupNumVisitors numVisitor, Long siteChatgroupId) {
        SiteChatgroupNumVisitorDTO dto = new SiteChatgroupNumVisitorDTO();
        if (numVisitor != null) {
            dto.setNumVisitorDate(new Date(numVisitor.getNumVisitorDate().toInstant().toEpochMilli()));
            dto.setNumVisitors(numVisitor.getNumVisitors());
            dto.setSiteChatGroupId(siteChatgroupId);
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.dateFormat);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            dto.setNumVisitorDateString(dto.getNumVisitorDate() != null ? sdf.format(dto.getNumVisitorDate()) : null);
        }
        return dto;
    }

    private TagDTO toTagDTO(SiteChatgroupToTag tag) {
        TagDTO dto = new TagDTO();
        if (tag != null && tag.getTag() != null) {
            dto.setTagId(tag.getTag().getId());
            dto.setTagDescription(tag.getTag().getDescription());
            dto.setTagName(tag.getTag().getName());
        }
        return dto;
    }

    private SiteChatgroupAuthorDTO toAuthorDTO(AuthorToSiteChatgroup author, Long siteChatgroupId) {
        SiteChatgroupAuthorDTO dto = new SiteChatgroupAuthorDTO();
        if (author != null) {
            dto.setSiteChatGroupId(siteChatgroupId);
            dto.setStaffFunction(author.getStaffFunction());
            if (author.getAuthor() != null) {
                AuthorDTO adto = new AuthorDTO();
                adto.setAuthorId (author.getAuthor().getId());
                adto.setEnglishName(author.getAuthor().getEnglishName());
                adto.setOriginalName(author.getAuthor().getOriginalName());
                dto.setAuthor(adto);
            }
        }
        return dto;
    }
}
