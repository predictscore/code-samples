package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.SiteChatgroupDomain;
import biz.newparadigm.meridian.repository.SiteChatgroupDomainRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupDomainSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SiteChatgroupDomain.
 */
@Service
@Transactional
public class SiteChatgroupDomainService {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupDomainService.class);

    private final SiteChatgroupDomainRepository siteChatgroupDomainRepository;

    private final SiteChatgroupDomainSearchRepository siteChatgroupDomainSearchRepository;

    public SiteChatgroupDomainService(SiteChatgroupDomainRepository siteChatgroupDomainRepository, SiteChatgroupDomainSearchRepository siteChatgroupDomainSearchRepository) {
        this.siteChatgroupDomainRepository = siteChatgroupDomainRepository;
        this.siteChatgroupDomainSearchRepository = siteChatgroupDomainSearchRepository;
    }

    /**
     * Save a siteChatgroupDomain.
     *
     * @param siteChatgroupDomain the entity to save
     * @return the persisted entity
     */
    public SiteChatgroupDomain save(SiteChatgroupDomain siteChatgroupDomain) {
        log.debug("Request to save SiteChatgroupDomain : {}", siteChatgroupDomain);
        SiteChatgroupDomain result = siteChatgroupDomainRepository.save(siteChatgroupDomain);
        siteChatgroupDomainSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the siteChatgroupDomains.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupDomain> findAll(Pageable pageable) {
        log.debug("Request to get all SiteChatgroupDomains");
        return siteChatgroupDomainRepository.findAll(pageable);
    }

    /**
     *  Get one siteChatgroupDomain by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SiteChatgroupDomain findOne(Long id) {
        log.debug("Request to get SiteChatgroupDomain : {}", id);
        return siteChatgroupDomainRepository.findOne(id);
    }

    /**
     *  Delete the  siteChatgroupDomain by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SiteChatgroupDomain : {}", id);
        siteChatgroupDomainRepository.delete(id);
        siteChatgroupDomainSearchRepository.delete(id);
    }

    /**
     * Search for the siteChatgroupDomain corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupDomain> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SiteChatgroupDomains for query {}", query);
        Page<SiteChatgroupDomain> result = siteChatgroupDomainSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
