package biz.newparadigm.meridian.domain;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A AuthorToContact.
 */
@Entity
@Table(name = "author_to_contacts")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "authortocontact")
public class AuthorToContact implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="author_to_contacts_id_seq")
    @SequenceGenerator(name="author_to_contacts_id_seq", sequenceName="author_to_contacts_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "contact_value", nullable = false)
    private String contactValue;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"profiles", "names", "comments", "contacts", "siteChatGroups", "tags", "languages"})
    private Author author;

    @ManyToOne(optional = false)
    @NotNull
    private ContactType contactType;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContactValue() {
        return contactValue;
    }

    public AuthorToContact contactValue(String contactValue) {
        this.contactValue = contactValue;
        return this;
    }

    public void setContactValue(String contactValue) {
        this.contactValue = contactValue;
    }

    public Author getAuthor() {
        return author;
    }

    public AuthorToContact author(Author author) {
        this.author = author;
        return this;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public ContactType getContactType() {
        return contactType;
    }

    public AuthorToContact contactType(ContactType contactType) {
        this.contactType = contactType;
        return this;
    }

    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AuthorToContact authorToContact = (AuthorToContact) o;
        if (authorToContact.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), authorToContact.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AuthorToContact{" +
            "id=" + getId() +
            ", contactValue='" + getContactValue() + "'" +
            "}";
    }
}
