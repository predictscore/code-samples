package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.service.view.TestView;
import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.SiteChatgroupName;
import biz.newparadigm.meridian.service.SiteChatgroupNameService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.SiteChatgroupNameCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupNameQueryService;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SiteChatgroupName.
 */
@RestController
@RequestMapping("/api")
public class SiteChatgroupNameResource {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupNameResource.class);

    private static final String ENTITY_NAME = "siteChatgroupName";

    private final SiteChatgroupNameService siteChatgroupNameService;

    private final SiteChatgroupNameQueryService siteChatgroupNameQueryService;

    public SiteChatgroupNameResource(SiteChatgroupNameService siteChatgroupNameService, SiteChatgroupNameQueryService siteChatgroupNameQueryService) {
        this.siteChatgroupNameService = siteChatgroupNameService;
        this.siteChatgroupNameQueryService = siteChatgroupNameQueryService;
    }

    /**
     * POST  /site-chatgroup-names : Create a new siteChatgroupName.
     *
     * @param siteChatgroupName the siteChatgroupName to create
     * @return the ResponseEntity with status 201 (Created) and with body the new siteChatgroupName, or with status 400 (Bad Request) if the siteChatgroupName has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/site-chatgroup-names")
    @Timed
    public ResponseEntity<SiteChatgroupName> createSiteChatgroupName(@Valid @RequestBody SiteChatgroupName siteChatgroupName) throws URISyntaxException {
        log.debug("REST request to save SiteChatgroupName : {}", siteChatgroupName);
        if (siteChatgroupName.getId() != null) {
            throw new BadRequestAlertException("A new siteChatgroupName cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SiteChatgroupName result = siteChatgroupNameService.save(siteChatgroupName);
        return ResponseEntity.created(new URI("/api/site-chatgroup-names/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /site-chatgroup-names : Updates an existing siteChatgroupName.
     *
     * @param siteChatgroupName the siteChatgroupName to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated siteChatgroupName,
     * or with status 400 (Bad Request) if the siteChatgroupName is not valid,
     * or with status 500 (Internal Server Error) if the siteChatgroupName couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/site-chatgroup-names")
    @Timed
    public ResponseEntity<SiteChatgroupName> updateSiteChatgroupName(@Valid @RequestBody SiteChatgroupName siteChatgroupName) throws URISyntaxException {
        log.debug("REST request to update SiteChatgroupName : {}", siteChatgroupName);
        if (siteChatgroupName.getId() == null) {
            return createSiteChatgroupName(siteChatgroupName);
        }
        SiteChatgroupName result = siteChatgroupNameService.save(siteChatgroupName);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, siteChatgroupName.getId().toString()))
            .body(result);
    }

    /**
     * GET  /site-chatgroup-names : get all the siteChatgroupNames.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of siteChatgroupNames in body
     */
    @GetMapping("/site-chatgroup-names")
    @Timed
    public ResponseEntity<List<SiteChatgroupName>> getAllSiteChatgroupNames(SiteChatgroupNameCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get SiteChatgroupNames by criteria: {}", criteria);
        Page<SiteChatgroupName> page = siteChatgroupNameQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/site-chatgroup-names");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /site-chatgroup-names/:id : get the "id" siteChatgroupName.
     *
     * @param id the id of the siteChatgroupName to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the siteChatgroupName, or with status 404 (Not Found)
     */
    @GetMapping("/site-chatgroup-names/{id}")
    @Timed
    public ResponseEntity<SiteChatgroupName> getSiteChatgroupName(@PathVariable Long id) {
        log.debug("REST request to get SiteChatgroupName : {}", id);
        SiteChatgroupName siteChatgroupName = siteChatgroupNameService.findOne(id);
//        siteChatgroupName.setSiteChatgroupId(siteChatgroupName.getSiteChatgroup().getId());
//        siteChatgroupName.setSiteChatgroup(null);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(siteChatgroupName));
    }

//    @GetMapping("/site-chatgroup-names/{id}/extended")
//    @Timed
//    public ResponseEntity<SiteChatgroupName> getSiteChatgroupNameExtended(@PathVariable Long id) {
//        log.debug("REST request to get SiteChatgroupName : {}", id);
//        SiteChatgroupName siteChatgroupName = siteChatgroupNameService.findOne(id);
//        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(siteChatgroupName));
//    }

    /**
     * DELETE  /site-chatgroup-names/:id : delete the "id" siteChatgroupName.
     *
     * @param id the id of the siteChatgroupName to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/site-chatgroup-names/{id}")
    @Timed
    public ResponseEntity<Void> deleteSiteChatgroupName(@PathVariable Long id) {
        log.debug("REST request to delete SiteChatgroupName : {}", id);
        siteChatgroupNameService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/site-chatgroup-names?query=:query : search for the siteChatgroupName corresponding
     * to the query.
     *
     * @param query the query of the siteChatgroupName search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/site-chatgroup-names")
    @Timed
    public ResponseEntity<List<SiteChatgroupName>> searchSiteChatgroupNames(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SiteChatgroupNames for query {}", query);
        Page<SiteChatgroupName> page = siteChatgroupNameService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/site-chatgroup-names");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
