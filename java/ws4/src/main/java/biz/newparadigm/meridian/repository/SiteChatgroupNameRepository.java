package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.SiteChatgroupName;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SiteChatgroupName entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SiteChatgroupNameRepository extends JpaRepository<SiteChatgroupName, Long>, JpaSpecificationExecutor<SiteChatgroupName> {

}
