package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.AuthorToLanguage;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AuthorToLanguage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuthorToLanguageRepository extends JpaRepository<AuthorToLanguage, Long>, JpaSpecificationExecutor<AuthorToLanguage> {
    @Modifying
    @Query("DELETE FROM AuthorToLanguage WHERE author.id = :authorId")
    public void deleteAllByAuthorId(@Param("authorId") Long authorId);

    @Modifying
    @Query("DELETE FROM AuthorToLanguage WHERE language.id = :languageId")
    public void deleteAllByLanguageId(@Param("languageId") Long languageId);
}
