package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsToTag;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsToTag entity.
 */
public interface NewsToTagSearchRepository extends ElasticsearchRepository<NewsToTag, Long> {
}
