package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.RFIResponse;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the RFIResponse entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RFIResponseRepository extends JpaRepository<RFIResponse, Long>, JpaSpecificationExecutor<RFIResponse> {

}
