package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsSummaryPublicImage;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsSummaryPublicImage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsSummaryPublicImageRepository extends JpaRepository<NewsSummaryPublicImage, Long>, JpaSpecificationExecutor<NewsSummaryPublicImage> {

}
