package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.*;
import biz.newparadigm.meridian.domain.audit.AuditChange;
import biz.newparadigm.meridian.domain.audit.AuditChangeType;
import biz.newparadigm.meridian.domain.audit.AuditEventType;
import biz.newparadigm.meridian.domain.audit.AuditUtil;
import biz.newparadigm.meridian.domain.metadata.Metagroups;
import biz.newparadigm.meridian.domain.util.MetadataConstants;
import biz.newparadigm.meridian.repository.*;
import biz.newparadigm.meridian.repository.search.NewsSearchRepository;
import biz.newparadigm.meridian.security.SecurityUtils;
import biz.newparadigm.meridian.service.filter.SpecificationBuilder;
import biz.newparadigm.meridian.service.mapper.MetadataMapper;
import biz.newparadigm.meridian.service.util.ServiceUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.hibernate.Hibernate;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Change;
import org.javers.core.diff.Diff;
import org.javers.core.diff.changetype.NewObject;
import org.javers.core.diff.changetype.ObjectRemoved;
import org.javers.core.diff.changetype.ReferenceChange;
import org.javers.core.diff.changetype.ValueChange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing News.
 */
@Service
@Transactional
public class NewsService {

    private final Logger log = LoggerFactory.getLogger(NewsService.class);

    private final NewsRepository newsRepository;

    private final NewsSearchRepository newsSearchRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private LanguageRepository languageRepository;

    @Autowired
    private MitigationsBypassedRepository mitigationsBypassedRepository;

    @Autowired
    private RFIResponseRepository RFIResponseRepository;

    @Autowired
    private NewsCommentService commentService;

    @Autowired
    private NewsAttachmentService attachmentService;

    @Autowired
    private NewsOriginalImageService originalImageService;

    @Autowired
    private NewsConfidentialImageService confidentialImageService;

    @Autowired
    private NewsPublicImageService publicImageService;

    @Autowired
    private NewsUrlService urlService;

    @Autowired
    private NewsDownloadableContentUrlService downloadableContentUrlService;

    @Autowired
    private NewsToTagService tagService;

    @Autowired
    private NewsToLanguageService languageService;

    @Autowired
    private NewsSummaryToNewsService summaryToNewsService;

    @Autowired
    private NewsMetadataService metadataService;

    @Autowired
    private MetagroupService metagroupService;

    @Autowired
    private MetafieldService metafieldService;

    @Autowired
    private MetadataMapper metadataMapper;

    @Autowired
    private RFIResponseService rfiResponseService;

    @Autowired
    private EntityAuditService entityAuditService;

    public NewsService(NewsRepository newsRepository, NewsSearchRepository newsSearchRepository) {
        this.newsRepository = newsRepository;
        this.newsSearchRepository = newsSearchRepository;
    }

    /**
     * Save a news.
     *
     * @param news the entity to save
     * @return the persisted entity
     */
    public News save(News news) {
        log.debug("Request to save News : {}", news);
        News originalNews;
        String originalNewsJson = "{}";
        News result;
        ObjectMapper mapper = AuditUtil.getMapper();
        boolean insert = true;
        if (news.getId() != null && news.getId() > 0L) {
            insert = false;
            News originalNews1 = newsRepository.findOne(news.getId());
            originalNews = AuditUtil.prepareNews(originalNews1);
            try {
                originalNewsJson = mapper.writeValueAsString(originalNews);
            } catch (Exception e) {
                originalNewsJson = "{}";
            }
            ServiceUtil.setEmptyListIfNullNews(news);

            if (news.getAttachments() != null && !news.getAttachments().isEmpty()) {
                news.getAttachments().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0L) {
                        item.setNews(news.toJPASimpleObject());
                        attachmentService.save(item);
                    }
                });
            }

            if (news.getOriginalImages() != null && !news.getOriginalImages().isEmpty()) {
                news.getOriginalImages().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0L) {
                        item.setNews(news.toJPASimpleObject());
                        originalImageService.save(item);
                    }
                });
            }

            if (news.getPublicImages() != null && !news.getPublicImages().isEmpty()) {
                news.getPublicImages().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0L) {
                        item.setNews(news.toJPASimpleObject());
                        publicImageService.save(item);
                    }
                });
            }

            if (news.getConfidentialImages() != null && !news.getConfidentialImages().isEmpty()) {
                news.getConfidentialImages().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0L) {
                        item.setNews(news.toJPASimpleObject());
                        confidentialImageService.save(item);
                    }
                });
            }

            if (news.getUrls() != null && !news.getUrls().isEmpty()) {
                news.getUrls().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0L) {
                        item.setNews(news.toJPASimpleObject());
                        urlService.save(item);
                    }
                });
            }

            if (news.getDownloadableContentUrls() != null && !news.getDownloadableContentUrls().isEmpty()) {
                news.getDownloadableContentUrls().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0L) {
                        item.setNews(news.toJPASimpleObject());
                        downloadableContentUrlService.save(item);
                    }
                });
            }

            if (news.getTags() != null && !news.getTags().isEmpty()) {
                news.getTags().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0L) {
                        item.setNews(news.toJPASimpleObject());
                        tagService.save(item);
                    }
                });
            }

            if (news.getLanguages() != null && !news.getLanguages().isEmpty()) {
                news.getLanguages().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0L) {
                        item.setNews(news.toJPASimpleObject());
                        languageService.save(item);
                    }
                });
            }

            if (news.getNewsSummaries() != null && !news.getNewsSummaries().isEmpty()) {
                news.getNewsSummaries().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0L) {
                        item.setNews(news.toJPASimpleObject());
                        summaryToNewsService.save(item);
                    }
                });
            }
            if (news.getMetadata() != null) {
                news.getMetadata().clear();
            }
            List<NewsMetadata> metadata = toDatabaseMetadata(news.getMetagroups());
            if (metadata != null && !metadata.isEmpty()) {
                metadata.forEach(item -> {
                    item.setNews(news.toJPASimpleObject());
                    metadataService.save(item);
                    news.getMetadata().add(item);
                });
            }
            news.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
            news.setLastModifiedDate(ZonedDateTime.now(ZoneId.of("UTC")));
            result = newsRepository.save(news);
        } else {
            ServiceUtil.setEmptyListIfNullNews(news);
            List<NewsAttachment> attachments = new ArrayList<>(news.getAttachments());
            List<NewsOriginalImage> originalImages = new ArrayList<>(news.getOriginalImages());
            List<NewsConfidentialImage> confidentialImages = new ArrayList<>(news.getConfidentialImages());
            List<NewsPublicImage> publicImages = new ArrayList<>(news.getPublicImages());
            List<NewsUrl> urls = new ArrayList<>(news.getUrls());
            List<NewsDownloadableContentUrl> downloadableContentUrls = new ArrayList<>(news.getDownloadableContentUrls());
            List<NewsToTag> tags = new ArrayList<>(news.getTags());
            List<NewsToLanguage> languages = new ArrayList<>(news.getLanguages());
            List<NewsSummaryToNews> newsSummaries = new ArrayList<>(news.getNewsSummaries());
            Metagroups metagroups = news.getMetagroups();
            List<NewsMetadata> metadata;

            news.getAttachments().clear();
            news.getOriginalImages().clear();
            news.getConfidentialImages().clear();
            news.getPublicImages().clear();
            news.getUrls().clear();
            news.getDownloadableContentUrls().clear();
            news.getTags().clear();
            news.getLanguages().clear();
            news.getNewsSummaries().clear();
            news.getMetadata().clear();

            news.setCreatedBy(SecurityUtils.getCurrentUserLogin());
            news.setCreatedDate(ZonedDateTime.now(ZoneId.of("UTC")));
            result = newsRepository.save(news);

            if (!attachments.isEmpty()) {
                attachments.forEach(item -> {
                    item.setNews(result);
                    attachmentService.save(item);
                    news.getAttachments().add(item);
                });
            }
            if (!publicImages.isEmpty()) {
                publicImages.forEach(item -> {
                    item.setNews(result);
                    publicImageService.save(item);
                    news.getPublicImages().add(item);
                });
            }
            if (!confidentialImages.isEmpty()) {
                confidentialImages.forEach(item -> {
                    item.setNews(result);
                    confidentialImageService.save(item);
                    news.getConfidentialImages().add(item);
                });
            }
            if (!originalImages.isEmpty()) {
                originalImages.forEach(item -> {
                    item.setNews(result);
                    originalImageService.save(item);
                    news.getOriginalImages().add(item);
                });
            }
            if (!urls.isEmpty()) {
                urls.forEach(item -> {
                    item.setNews(result);
                    urlService.save(item);
                    news.getUrls().add(item);
                });
            }
            if (!downloadableContentUrls.isEmpty()) {
                downloadableContentUrls.forEach(item -> {
                    item.setNews(result);
                    downloadableContentUrlService.save(item);
                    news.getDownloadableContentUrls().add(item);
                });
            }
            if (!tags.isEmpty()) {
                tags.forEach(item -> {
                    item.setNews(result);
                    tagService.save(item);
                    news.getTags().add(item);
                });
            }
            if (!languages.isEmpty()) {
                languages.forEach(item -> {
                    item.setNews(result);
                    languageService.save(item);
                    news.getLanguages().add(item);
                });
            }
            if (!newsSummaries.isEmpty()) {
                newsSummaries.forEach(item -> {
                    item.setNews(result);
                    summaryToNewsService.save(item);
                    news.getNewsSummaries().add(item);
                });
            }
            metadata = toDatabaseMetadata(metagroups);
            if (!metadata.isEmpty()) {
                metadata.forEach(item -> {
                    item.setNews(result);
                    metadataService.save(item);
                    news.getMetadata().add(item);
                });
            }
        }

        News updatedNews1 = findOne(result.getId(), true);
        News updatedNews = AuditUtil.prepareNews(updatedNews1);

        newsSearchRepository.save(result);

        String updatedNewsJson;
        try {
            updatedNewsJson = mapper.writeValueAsString(updatedNews);
        } catch (Exception e) {
            updatedNewsJson = "{}";
        }

        try {
            News beforeNode = mapper.readValue(originalNewsJson, News.class);
            News afterNode = mapper.readValue(updatedNewsJson, News.class);

            Javers j = JaversBuilder.javers().build();
            Diff diff = j.compare(beforeNode, afterNode);
            EntityAudit audit = AuditUtil.getAuditEntity(mapper, insert, updatedNews1.getId(), diff, originalNewsJson, updatedNewsJson, News.class.getName());
            if (audit != null) {
                entityAuditService.save(audit);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return updatedNews1;
    }

    /**
     *  Get all the news.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<News> findAll(Pageable pageable) {
        log.debug("Request to get all News");
        return newsRepository.findAll(pageable);
    }

    /**
     *  Get one news by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public News findOne(Long id, boolean loadLazyItems) {
        log.debug("Request to get News : {}", id);
        News news = newsRepository.findOne(id);
        if (loadLazyItems && news != null) {
            initializeLazyItems(news);
        }
        return news;
    }

    private void initializeLazyItems(News news) {
        Hibernate.initialize(news.getAttachments());
        Hibernate.initialize(news.getPublicImages());
        Hibernate.initialize(news.getConfidentialImages());
        Hibernate.initialize(news.getOriginalImages());
        Hibernate.initialize(news.getUrls());
        Hibernate.initialize(news.getDownloadableContentUrls());
        Hibernate.initialize(news.getMetadata());
        Hibernate.initialize(news.getTags());
        Hibernate.initialize(news.getLanguages());
        Hibernate.initialize(news.getNewsSummaries());
        initializeMetagroups(news);
    }

    public void initializeMetagroups(News news) {
        Metagroups metagroups = new Metagroups();
        if (news.getMetadata() != null && !news.getMetadata().isEmpty()) {
            int incValue = 0;
            long metafieldId = news.getMetadata().get(0).getMetafield().getId();
            if (metafieldId > 100000 && metafieldId < 200000) {
                incValue += 100000;
            } else if (metafieldId > 200000 && metafieldId < 300000) {
                incValue += 200000;
            }
            for (NewsMetadata m : news.getMetadata()) {
                switch (m.getMetafield().getId().intValue() - incValue) {
                    case 1:
                        metagroups.getForProfit().setTypicalUsers(m.getValueString());
                        break;
                    case 2:
                        metagroups.getForProfit().setPriceOriginal(m.getValueString());
                        break;
                    case 3:
                        metagroups.getForProfit().setPriceEnglish(m.getValueString());
                        break;
                    case 6:
                        metagroups.getLoa().setLevelOfActivity(m.getValueString());
                        break;
                    case 8:
                        metagroups.getMmvar().setOfferType(m.getValueString());
                        break;
                    case 9:
                        metagroups.getMmvar().setAnalysis(m.getValueString());
                        break;
                    case 10:
                        RFIResponse rfi = null;
                        if (m.getValueNumber() != null) {
                            rfi = RFIResponseRepository.findOne(m.getValueNumber().longValue());
                        }
                        if (rfi != null) {
                            metagroups.getMmvar().getRfiResponses().add(rfi);
                        }
                        break;
                    case 11:
                        Category category = null;
                        if (m.getValueNumber() != null) {
                            category = categoryRepository.findOne(m.getValueNumber().longValue());
                        }
                        if (category != null) {
                            metagroups.getMmvar().setCategory(category);
                        }
                        break;
                    case 12:
                        metagroups.getMmvar().setConfidenceLevel(m.getValueString());
                        break;
                    case 13:
                        metagroups.getMmvar().setConfidenceLevelExplanation(m.getValueString());
                        break;
                    case 14:
                        metagroups.getMmvar().setLevelOfImpact(m.getValueString());
                        break;
                    case 15:
                        metagroups.getMmvar().setLevelOfImpactExplanation(m.getValueString());
                        break;
                    case 16:
                        metagroups.getProdOrServ().setProductServiceNameOriginal(m.getValueString());
                        break;
                    case 17:
                        metagroups.getProdOrServ().setProductServiceNameEnglish(m.getValueString());
                        break;
                    case 18:
                        metagroups.getProdOrServ().setProductServiceVersionOriginal(m.getValueString());
                        break;
                    case 19:
                        metagroups.getProdOrServ().setProductServiceVersionEnglish(m.getValueString());
                        break;
                    case 20:
                        Language lang = null;
                        if (m.getValueNumber() != null) {
                            lang = languageRepository.findOne(m.getValueNumber().longValue());
                        }
                        if (lang != null) {
                            metagroups.getProdOrServ().getProductServiceLanguages().add(lang);
                        }
                        break;
                    case 21:
                        if (m.getValueString() != null) {
                            metagroups.getVulnerability().getCve().add(m.getValueString());
                        }
                        break;
                    case 22:
                        MitigationsBypassed mb = null;
                        if (m.getValueNumber() != null) {
                            mb = mitigationsBypassedRepository.findOne(m.getValueNumber().longValue());
                        }
                        if (mb != null) {
                            metagroups.getVulnerability().getMitigationsBypassed().add(mb);
                        }
                        break;
                    case 23:
                        metagroups.getFf().setFfCategory(m.getValueString());
                        break;
                    case 24:
                        metagroups.getFf().setSize(m.getValueNumber());
                        break;
                    case 25:
                        metagroups.getFf().setRate(m.getValueNumber());
                        break;
                    case 26:
                        metagroups.getFf().setRegion(m.getValueString());
                        break;
                    case 27:
                        metagroups.getFf().setDataType(m.getValueString());
                        break;
                }
            }
        }
        news.setMetagroups(metagroups);
    }

    private List<NewsMetadata> toDatabaseMetadata(Metagroups metagroups) {
        List<NewsMetadata> result = new ArrayList<>();

        if (metagroups != null) {
            List<Metagroup> metagroupsList = metagroupService.findAll();
            List<Metafield> metafieldsList = metafieldService.findAll();

            if (metagroups.getProdOrServ() != null) {
                Metagroup mg = metadataMapper.getMetagroupByName(metagroupsList, MetadataConstants.METAGROUP_PROD_OR_SERV);
                if (metagroups.getProdOrServ().getProductServiceNameOriginal() != null && !metagroups.getProdOrServ().getProductServiceNameOriginal().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_PRODUCT_SERVICE_NAME_ORIGINAL));
                    m.setValueString(metagroups.getProdOrServ().getProductServiceNameOriginal());
                    result.add(m);
                }
                if (metagroups.getProdOrServ().getProductServiceNameEnglish() != null && !metagroups.getProdOrServ().getProductServiceNameEnglish().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_PRODUCT_SERVICE_NAME_ENGLISH));
                    m.setValueString(metagroups.getProdOrServ().getProductServiceNameEnglish());
                    result.add(m);
                }
                if (metagroups.getProdOrServ().getProductServiceVersionOriginal() != null && !metagroups.getProdOrServ().getProductServiceVersionOriginal().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_PRODUCT_SERVICE_VERSION_ORIGINAL));
                    m.setValueString(metagroups.getProdOrServ().getProductServiceVersionOriginal());
                    result.add(m);
                }
                if (metagroups.getProdOrServ().getProductServiceVersionEnglish() != null && !metagroups.getProdOrServ().getProductServiceVersionEnglish().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_PRODUCT_SERVICE_VERSION_ENGLISH));
                    m.setValueString(metagroups.getProdOrServ().getProductServiceVersionEnglish());
                    result.add(m);
                }
                if (metagroups.getProdOrServ().getProductServiceLanguages() != null && !metagroups.getProdOrServ().getProductServiceLanguages().isEmpty()) {
                    metagroups.getProdOrServ().getProductServiceLanguages().stream().filter(Objects::nonNull).forEach(value -> {
                        NewsMetadata m = new NewsMetadata();
                        m.setMetagroup(mg);
                        m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_PRODUCT_SERVICE_LANGUAGE));
                        m.setValueNumber(value.getId().doubleValue());
                        result.add(m);
                    });
                }
            }
            if (metagroups.getForProfit() != null) {
                Metagroup mg = metadataMapper.getMetagroupByName(metagroupsList, MetadataConstants.METAGROUP_FOR_PROFIT);
                if (metagroups.getForProfit().getPriceOriginal() != null && !metagroups.getForProfit().getPriceOriginal().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_PRICE_ORIGINAL));
                    m.setValueString(metagroups.getForProfit().getPriceOriginal());
                    result.add(m);
                }
                if (metagroups.getForProfit().getPriceEnglish() != null && !metagroups.getForProfit().getPriceEnglish().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_PRICE_ENGLISH));
                    m.setValueString(metagroups.getForProfit().getPriceEnglish());
                    result.add(m);
                }
                if (metagroups.getForProfit().getTypicalUsers() != null && !metagroups.getForProfit().getTypicalUsers().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_TYPICAL_USERS));
                    m.setValueString(metagroups.getForProfit().getTypicalUsers());
                    result.add(m);
                }
            }
            if (metagroups.getLoa() != null) {
                Metagroup mg = metadataMapper.getMetagroupByName(metagroupsList, MetadataConstants.METAGROUP_LOA);
                if (metagroups.getLoa().getLevelOfActivity() != null && !metagroups.getLoa().getLevelOfActivity().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_LEVEL_OF_ACTIVITY));
                    m.setValueString(metagroups.getLoa().getLevelOfActivity());
                    result.add(m);
                }
            }
            if (metagroups.getVulnerability() != null) {
                Metagroup mg = metadataMapper.getMetagroupByName(metagroupsList, MetadataConstants.METAGROUP_VULNERABILITY);
                if (metagroups.getVulnerability().getCve() != null && !metagroups.getVulnerability().getCve().isEmpty()) {
                    metagroups.getVulnerability().getCve().stream().filter(Objects::nonNull).forEach(value -> {
                        NewsMetadata m = new NewsMetadata();
                        m.setMetagroup(mg);
                        m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_CVE));
                        m.setValueString(value);
                        result.add(m);
                    });
                }
                if (metagroups.getVulnerability().getMitigationsBypassed() != null && !metagroups.getVulnerability().getMitigationsBypassed().isEmpty()) {
                    metagroups.getVulnerability().getMitigationsBypassed().stream().filter(Objects::nonNull).forEach(value -> {
                        NewsMetadata m = new NewsMetadata();
                        m.setMetagroup(mg);
                        m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_MITIGAATIOMS_BYPASSED));
                        m.setValueNumber(value.getId().doubleValue());
                        result.add(m);
                    });
                }
            }
            if (metagroups.getMmvar() != null) {
                Metagroup mg = metadataMapper.getMetagroupByName(metagroupsList, MetadataConstants.METAGROUP_MMVAR);
                if (metagroups.getMmvar().getRfiResponses() != null && !metagroups.getMmvar().getRfiResponses().isEmpty()) {
                    metagroups.getMmvar().getRfiResponses().stream().filter(Objects::nonNull).forEach(value -> {
                        if (value.getId() == null || value.getId() == 0L) {
                            value.setUsername(SecurityUtils.getCurrentUserLogin());
                            value.setSubmissionDate(ZonedDateTime.now(ZoneId.of("UTC")));
                            rfiResponseService.save(value);
                        }
                        if (value.getId() != null) {
                            NewsMetadata m = new NewsMetadata();
                            m.setMetagroup(mg);
                            m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_RFI_RESPONSE));
                            m.setValueNumber(value.getId().doubleValue());
                            result.add(m);
                        }
                    });
                }
                if (metagroups.getMmvar().getAnalysis() != null && !metagroups.getMmvar().getAnalysis().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_ANALYSIS));
                    m.setValueString(metagroups.getMmvar().getAnalysis());
                    result.add(m);
                }
                if (metagroups.getMmvar().getCategory() != null) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_CATEGORY));
                    m.setValueNumber(metagroups.getMmvar().getCategory().getId().doubleValue());
                    result.add(m);
                }
                if (metagroups.getMmvar().getConfidenceLevel() != null && !metagroups.getMmvar().getConfidenceLevel().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_CONFIDENCE_LEVEL));
                    m.setValueString(metagroups.getMmvar().getConfidenceLevel());
                    result.add(m);
                }
                if (metagroups.getMmvar().getConfidenceLevelExplanation() != null && !metagroups.getMmvar().getConfidenceLevelExplanation().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_CONFIDENCE_LEVEL_EXPLANATION));
                    m.setValueString(metagroups.getMmvar().getConfidenceLevelExplanation());
                    result.add(m);
                }
                if (metagroups.getMmvar().getLevelOfImpact() != null && !metagroups.getMmvar().getLevelOfImpact().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_LEVEL_OF_IMPACT));
                    m.setValueString(metagroups.getMmvar().getLevelOfImpact());
                    result.add(m);
                }
                if (metagroups.getMmvar().getLevelOfImpactExplanation() != null && !metagroups.getMmvar().getLevelOfImpactExplanation().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_LEVEL_OF_IMPACT_EXPLANATION));
                    m.setValueString(metagroups.getMmvar().getLevelOfImpactExplanation());
                    result.add(m);
                }
                if (metagroups.getMmvar().getOfferType() != null && !metagroups.getMmvar().getOfferType().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_OFFER_TYPE));
                    m.setValueString(metagroups.getMmvar().getOfferType());
                    result.add(m);
                }
            }
            if (metagroups.getFf() != null) {
                Metagroup mg = metadataMapper.getMetagroupByName(metagroupsList, MetadataConstants.METAGROUP_FF);
                if (metagroups.getFf().getFfCategory() != null && !metagroups.getFf().getFfCategory().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_FF_CATEGOTY));
                    m.setValueString(metagroups.getFf().getFfCategory());
                    result.add(m);
                }
                if (metagroups.getFf().getDataType() != null && !metagroups.getFf().getDataType().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_DATA_TYPE));
                    m.setValueString(metagroups.getFf().getDataType());
                    result.add(m);
                }
                if (metagroups.getFf().getRegion() != null && !metagroups.getFf().getRegion().isEmpty()) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_REGION));
                    m.setValueString(metagroups.getFf().getRegion());
                    result.add(m);
                }
                if (metagroups.getFf().getRate() != null) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_RATE));
                    m.setValueNumber(metagroups.getFf().getRate());
                    result.add(m);
                }
                if (metagroups.getFf().getSize() != null) {
                    NewsMetadata m = new NewsMetadata();
                    m.setMetagroup(mg);
                    m.setMetafield(metadataMapper.getMetafieldByName(metafieldsList, MetadataConstants.METAFIELD_SIZE));
                    m.setValueNumber(metagroups.getFf().getSize());
                    result.add(m);
                }
            }
        }
        return result;
    }

    private void initializeInternalLazyItems(News news) {
        if (news.getAuthor() != null) {
            Hibernate.initialize(news.getAuthor().getNames());
        }
        if (news.getSiteChatgroup() != null) {
            Hibernate.initialize(news.getSiteChatgroup().getNames());
            Hibernate.initialize(news.getSiteChatgroup().getLanguages());
        }
    }

    public List<News> findAllForExport(String fromDate, String toDate, String tags, Integer limit) {
        List<News> result;
        SpecificationBuilder<News> builder = new SpecificationBuilder<>();
        Sort sort = new Sort(Sort.Direction.ASC, "datetime");
        Pageable pageable = new PageRequest(0, (limit != null && limit >= 0) ? limit : Integer.MAX_VALUE, sort);
        Specifications<News> specification = Specifications.where(null);
        ZonedDateTime fromZonedDate = ZonedDateTime.parse(fromDate + " 00:00:00", DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss").withZone(ZoneId.of("UTC")));
        specification = specification.and(builder.greaterThanOrEqualTo("datetime", fromZonedDate));
        if (toDate != null) {
            ZonedDateTime toZonedDate = ZonedDateTime.parse(toDate + " 23:59:59", DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss").withZone(ZoneId.of("UTC")));
            specification = specification.and(builder.lessThanOrEqualTo("datetime", toZonedDate));
        }
        if (tags != null && !tags.isEmpty()) {
            String[] tagArray = tags.split(",");
            tagArray = trimStringArray(tagArray);
            specification = specification.and(builder.buildExportTagsSpecification(Arrays.asList(tagArray)));
        }
        Page<News> page = newsRepository.findAll(specification, pageable);
        result = page.getContent();
        result.forEach(this::initializeLazyItems);
        result.forEach(this::initializeInternalLazyItems);
        return result;
    }

    private String[] trimStringArray(String[] tagArray) {
        String[] trimmedTagArray = new String[tagArray.length];
        for (int i = 0; i < tagArray.length; i++) {
            trimmedTagArray[i] = tagArray[i].trim();
        }
        return trimmedTagArray;
    }

    /**
     *  Delete the  news by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete News : {}", id);
        newsRepository.delete(id);
        newsSearchRepository.delete(id);
    }

    /**
     * Search for the news corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<News> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of News for query {}", query);
        Page<News> result = newsSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
