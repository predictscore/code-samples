package biz.newparadigm.meridian.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.RFIResponse;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.RFIResponseRepository;
import biz.newparadigm.meridian.repository.search.RFIResponseSearchRepository;
import biz.newparadigm.meridian.service.dto.RFIResponseCriteria;


/**
 * Service for executing complex queries for RFIResponse entities in the database.
 * The main input is a {@link RFIResponseCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link RFIResponse} or a {@link Page} of {%link RFIResponse} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class RFIResponseQueryService extends QueryService<RFIResponse> {

    private final Logger log = LoggerFactory.getLogger(RFIResponseQueryService.class);


    private final RFIResponseRepository rFIResponseRepository;

    private final RFIResponseSearchRepository rFIResponseSearchRepository;

    public RFIResponseQueryService(RFIResponseRepository rFIResponseRepository, RFIResponseSearchRepository rFIResponseSearchRepository) {
        this.rFIResponseRepository = rFIResponseRepository;
        this.rFIResponseSearchRepository = rFIResponseSearchRepository;
    }

    /**
     * Return a {@link List} of {%link RFIResponse} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RFIResponse> findByCriteria(RFIResponseCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<RFIResponse> specification = createSpecification(criteria);
        return rFIResponseRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link RFIResponse} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RFIResponse> findByCriteria(RFIResponseCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<RFIResponse> specification = createSpecification(criteria);
        return rFIResponseRepository.findAll(specification, page);
    }

    /**
     * Function to convert RFIResponseCriteria to a {@link Specifications}
     */
    private Specifications<RFIResponse> createSpecification(RFIResponseCriteria criteria) {
        Specifications<RFIResponse> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), RFIResponse_.id));
            }
            if (criteria.getNote() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNote(), RFIResponse_.note));
            }
            if (criteria.getUsername() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUsername(), RFIResponse_.username));
            }
            if (criteria.getSubmissionDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSubmissionDate(), RFIResponse_.submissionDate));
            }
        }
        return specification;
    }

}
