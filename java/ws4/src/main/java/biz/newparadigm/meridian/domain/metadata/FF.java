package biz.newparadigm.meridian.domain.metadata;

public class FF {
    private String ffCategory;
    private Double size;
    private Double rate;
    private String region;
    private String dataType;

    public String getFfCategory() {
        return ffCategory;
    }

    public void setFfCategory(String ffCategory) {
        this.ffCategory = ffCategory;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }
}
