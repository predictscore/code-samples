package biz.newparadigm.meridian.service.dto.old;

public class Constants {
    public static final String dateFormat = "yyyy-MM-dd";
    public static final String dateWithTimeFormat = "yyyy-MM-dd'T'HH:mm:ssZZ";
}
