package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsDownloadableContentUrl;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsDownloadableContentUrl entity.
 */
public interface NewsDownloadableContentUrlSearchRepository extends ElasticsearchRepository<NewsDownloadableContentUrl, Long> {
}
