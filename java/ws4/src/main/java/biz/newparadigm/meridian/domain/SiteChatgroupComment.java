package biz.newparadigm.meridian.domain;

import biz.newparadigm.meridian.domain.util.MeridianDateSerializer;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A SiteChatgroupComment.
 */
@Entity
@Table(name = "site_chatgroup_comments")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "sitechatgroupcomment")
public class SiteChatgroupComment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="site_chatgroup_comments_id_seq")
    @SequenceGenerator(name="site_chatgroup_comments_id_seq", sequenceName="site_chatgroup_comments_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "user_id", nullable = false)
    private String userId;

    @NotNull
    @Size(max = 2000)
    @Column(name = "comment", length = 2000, nullable = false)
    private String comment;

    @NotNull
    @Column(name = "datetime", nullable = false)
    @JsonSerialize(using = MeridianDateSerializer.class)
    private ZonedDateTime datetime;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"comments", "domains", "ips", "names", "infos", "numVisitors", "numUsers", "languages", "tags", "authors"})
    private SiteChatgroup siteChatgroup;

    @ManyToOne
    private SiteChatgroupComment parentComment;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public SiteChatgroupComment userId(String userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public SiteChatgroupComment comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ZonedDateTime getDatetime() {
        return datetime;
    }

    public SiteChatgroupComment datetime(ZonedDateTime datetime) {
        this.datetime = datetime;
        return this;
    }

    public void setDatetime(ZonedDateTime datetime) {
        this.datetime = datetime;
    }

    public SiteChatgroup getSiteChatgroup() {
        return siteChatgroup;
    }

    public SiteChatgroupComment siteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
        return this;
    }

    public void setSiteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
    }

    public SiteChatgroupComment getParentComment() {
        return parentComment;
    }

    public SiteChatgroupComment parentComment(SiteChatgroupComment siteChatgroupComment) {
        this.parentComment = siteChatgroupComment;
        return this;
    }

    public void setParentComment(SiteChatgroupComment siteChatgroupComment) {
        this.parentComment = siteChatgroupComment;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SiteChatgroupComment siteChatgroupComment = (SiteChatgroupComment) o;
        if (siteChatgroupComment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), siteChatgroupComment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SiteChatgroupComment{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", comment='" + getComment() + "'" +
            ", datetime='" + getDatetime() + "'" +
            "}";
    }
}
