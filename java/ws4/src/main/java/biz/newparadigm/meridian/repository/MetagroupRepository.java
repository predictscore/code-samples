package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.Metagroup;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Metagroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MetagroupRepository extends JpaRepository<Metagroup, Long>, JpaSpecificationExecutor<Metagroup> {

}
