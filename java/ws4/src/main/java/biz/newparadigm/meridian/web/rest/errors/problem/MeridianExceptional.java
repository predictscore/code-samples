package biz.newparadigm.meridian.web.rest.errors.problem;

public interface MeridianExceptional extends MeridianProblem {
    MeridianExceptional getCause();

    default MeridianExceptional propagate() throws Exception {
        throw propagateAs(Exception.class);
    }

    default <X extends Throwable> X propagateAs(final Class<X> type) throws X {
        throw type.cast(this);
    }

}
