package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsSummary;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsSummary entity.
 */
public interface NewsSummarySearchRepository extends ElasticsearchRepository<NewsSummary, Long> {
}
