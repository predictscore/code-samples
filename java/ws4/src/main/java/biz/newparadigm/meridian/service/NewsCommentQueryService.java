package biz.newparadigm.meridian.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsComment;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsCommentRepository;
import biz.newparadigm.meridian.repository.search.NewsCommentSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsCommentCriteria;


/**
 * Service for executing complex queries for NewsComment entities in the database.
 * The main input is a {@link NewsCommentCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsComment} or a {@link Page} of {%link NewsComment} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsCommentQueryService extends QueryService<NewsComment> {

    private final Logger log = LoggerFactory.getLogger(NewsCommentQueryService.class);


    private final NewsCommentRepository newsCommentRepository;

    private final NewsCommentSearchRepository newsCommentSearchRepository;

    public NewsCommentQueryService(NewsCommentRepository newsCommentRepository, NewsCommentSearchRepository newsCommentSearchRepository) {
        this.newsCommentRepository = newsCommentRepository;
        this.newsCommentSearchRepository = newsCommentSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsComment} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsComment> findByCriteria(NewsCommentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsComment> specification = createSpecification(criteria);
        return newsCommentRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsComment} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsComment> findByCriteria(NewsCommentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsComment> specification = createSpecification(criteria);
        return newsCommentRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsCommentCriteria to a {@link Specifications}
     */
    private Specifications<NewsComment> createSpecification(NewsCommentCriteria criteria) {
        Specifications<NewsComment> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsComment_.id));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUserId(), NewsComment_.userId));
            }
            if (criteria.getComment() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComment(), NewsComment_.comment));
            }
            if (criteria.getDatetime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDatetime(), NewsComment_.datetime));
            }
            if (criteria.getNewsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsId(), NewsComment_.news, News_.id));
            }
            if (criteria.getParentCommentId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getParentCommentId(), NewsComment_.parentComment, NewsComment_.id));
            }
        }
        return specification;
    }

}
