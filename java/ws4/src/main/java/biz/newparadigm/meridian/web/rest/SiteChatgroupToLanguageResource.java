package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.SiteChatgroupToLanguage;
import biz.newparadigm.meridian.service.SiteChatgroupToLanguageService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.SiteChatgroupToLanguageCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupToLanguageQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SiteChatgroupToLanguage.
 */
@RestController
@RequestMapping("/api")
public class SiteChatgroupToLanguageResource {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupToLanguageResource.class);

    private static final String ENTITY_NAME = "siteChatgroupToLanguage";

    private final SiteChatgroupToLanguageService siteChatgroupToLanguageService;

    private final SiteChatgroupToLanguageQueryService siteChatgroupToLanguageQueryService;

    public SiteChatgroupToLanguageResource(SiteChatgroupToLanguageService siteChatgroupToLanguageService, SiteChatgroupToLanguageQueryService siteChatgroupToLanguageQueryService) {
        this.siteChatgroupToLanguageService = siteChatgroupToLanguageService;
        this.siteChatgroupToLanguageQueryService = siteChatgroupToLanguageQueryService;
    }

    /**
     * POST  /site-chatgroup-to-languages : Create a new siteChatgroupToLanguage.
     *
     * @param siteChatgroupToLanguage the siteChatgroupToLanguage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new siteChatgroupToLanguage, or with status 400 (Bad Request) if the siteChatgroupToLanguage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/site-chatgroup-to-languages")
    @Timed
    public ResponseEntity<SiteChatgroupToLanguage> createSiteChatgroupToLanguage(@Valid @RequestBody SiteChatgroupToLanguage siteChatgroupToLanguage) throws URISyntaxException {
        log.debug("REST request to save SiteChatgroupToLanguage : {}", siteChatgroupToLanguage);
        if (siteChatgroupToLanguage.getId() != null) {
            throw new BadRequestAlertException("A new siteChatgroupToLanguage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SiteChatgroupToLanguage result = siteChatgroupToLanguageService.save(siteChatgroupToLanguage);
        return ResponseEntity.created(new URI("/api/site-chatgroup-to-languages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /site-chatgroup-to-languages : Updates an existing siteChatgroupToLanguage.
     *
     * @param siteChatgroupToLanguage the siteChatgroupToLanguage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated siteChatgroupToLanguage,
     * or with status 400 (Bad Request) if the siteChatgroupToLanguage is not valid,
     * or with status 500 (Internal Server Error) if the siteChatgroupToLanguage couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/site-chatgroup-to-languages")
    @Timed
    public ResponseEntity<SiteChatgroupToLanguage> updateSiteChatgroupToLanguage(@Valid @RequestBody SiteChatgroupToLanguage siteChatgroupToLanguage) throws URISyntaxException {
        log.debug("REST request to update SiteChatgroupToLanguage : {}", siteChatgroupToLanguage);
        if (siteChatgroupToLanguage.getId() == null) {
            return createSiteChatgroupToLanguage(siteChatgroupToLanguage);
        }
        SiteChatgroupToLanguage result = siteChatgroupToLanguageService.save(siteChatgroupToLanguage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, siteChatgroupToLanguage.getId().toString()))
            .body(result);
    }

    /**
     * GET  /site-chatgroup-to-languages : get all the siteChatgroupToLanguages.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of siteChatgroupToLanguages in body
     */
    @GetMapping("/site-chatgroup-to-languages")
    @Timed
    public ResponseEntity<List<SiteChatgroupToLanguage>> getAllSiteChatgroupToLanguages(SiteChatgroupToLanguageCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get SiteChatgroupToLanguages by criteria: {}", criteria);
        Page<SiteChatgroupToLanguage> page = siteChatgroupToLanguageQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/site-chatgroup-to-languages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /site-chatgroup-to-languages/:id : get the "id" siteChatgroupToLanguage.
     *
     * @param id the id of the siteChatgroupToLanguage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the siteChatgroupToLanguage, or with status 404 (Not Found)
     */
    @GetMapping("/site-chatgroup-to-languages/{id}")
    @Timed
    public ResponseEntity<SiteChatgroupToLanguage> getSiteChatgroupToLanguage(@PathVariable Long id) {
        log.debug("REST request to get SiteChatgroupToLanguage : {}", id);
        SiteChatgroupToLanguage siteChatgroupToLanguage = siteChatgroupToLanguageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(siteChatgroupToLanguage));
    }

    /**
     * DELETE  /site-chatgroup-to-languages/:id : delete the "id" siteChatgroupToLanguage.
     *
     * @param id the id of the siteChatgroupToLanguage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/site-chatgroup-to-languages/{id}")
    @Timed
    public ResponseEntity<Void> deleteSiteChatgroupToLanguage(@PathVariable Long id) {
        log.debug("REST request to delete SiteChatgroupToLanguage : {}", id);
        siteChatgroupToLanguageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/site-chatgroup-to-languages?query=:query : search for the siteChatgroupToLanguage corresponding
     * to the query.
     *
     * @param query the query of the siteChatgroupToLanguage search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/site-chatgroup-to-languages")
    @Timed
    public ResponseEntity<List<SiteChatgroupToLanguage>> searchSiteChatgroupToLanguages(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SiteChatgroupToLanguages for query {}", query);
        Page<SiteChatgroupToLanguage> page = siteChatgroupToLanguageService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/site-chatgroup-to-languages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
