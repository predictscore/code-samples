package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsConfidentialImage;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsConfidentialImageRepository;
import biz.newparadigm.meridian.repository.search.NewsConfidentialImageSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsConfidentialImageCriteria;


/**
 * Service for executing complex queries for NewsConfidentialImage entities in the database.
 * The main input is a {@link NewsConfidentialImageCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsConfidentialImage} or a {@link Page} of {%link NewsConfidentialImage} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsConfidentialImageQueryService extends QueryService<NewsConfidentialImage> {

    private final Logger log = LoggerFactory.getLogger(NewsConfidentialImageQueryService.class);


    private final NewsConfidentialImageRepository newsConfidentialImageRepository;

    private final NewsConfidentialImageSearchRepository newsConfidentialImageSearchRepository;

    public NewsConfidentialImageQueryService(NewsConfidentialImageRepository newsConfidentialImageRepository, NewsConfidentialImageSearchRepository newsConfidentialImageSearchRepository) {
        this.newsConfidentialImageRepository = newsConfidentialImageRepository;
        this.newsConfidentialImageSearchRepository = newsConfidentialImageSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsConfidentialImage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsConfidentialImage> findByCriteria(NewsConfidentialImageCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsConfidentialImage> specification = createSpecification(criteria);
        return newsConfidentialImageRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsConfidentialImage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsConfidentialImage> findByCriteria(NewsConfidentialImageCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsConfidentialImage> specification = createSpecification(criteria);
        return newsConfidentialImageRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsConfidentialImageCriteria to a {@link Specifications}
     */
    private Specifications<NewsConfidentialImage> createSpecification(NewsConfidentialImageCriteria criteria) {
        Specifications<NewsConfidentialImage> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsConfidentialImage_.id));
            }
            if (criteria.getFilename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFilename(), NewsConfidentialImage_.filename));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), NewsConfidentialImage_.description));
            }
            if (criteria.getDescriptionTranslation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescriptionTranslation(), NewsConfidentialImage_.descriptionTranslation));
            }
            if (criteria.getImageOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getImageOrder(), NewsConfidentialImage_.imageOrder));
            }
            if (criteria.getNewsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsId(), NewsConfidentialImage_.news, News_.id));
            }
        }
        return specification;
    }

}
