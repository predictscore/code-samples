package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsSummaryToNews;
import biz.newparadigm.meridian.repository.NewsSummaryToNewsRepository;
import biz.newparadigm.meridian.repository.search.NewsSummaryToNewsSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsSummaryToNews.
 */
@Service
@Transactional
public class NewsSummaryToNewsService {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryToNewsService.class);

    private final NewsSummaryToNewsRepository newsSummaryToNewsRepository;

    private final NewsSummaryToNewsSearchRepository newsSummaryToNewsSearchRepository;

    public NewsSummaryToNewsService(NewsSummaryToNewsRepository newsSummaryToNewsRepository, NewsSummaryToNewsSearchRepository newsSummaryToNewsSearchRepository) {
        this.newsSummaryToNewsRepository = newsSummaryToNewsRepository;
        this.newsSummaryToNewsSearchRepository = newsSummaryToNewsSearchRepository;
    }

    /**
     * Save a newsSummaryToNews.
     *
     * @param newsSummaryToNews the entity to save
     * @return the persisted entity
     */
    public NewsSummaryToNews save(NewsSummaryToNews newsSummaryToNews) {
        log.debug("Request to save NewsSummaryToNews : {}", newsSummaryToNews);
        NewsSummaryToNews result = newsSummaryToNewsRepository.save(newsSummaryToNews);
        newsSummaryToNewsSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsSummaryToNews.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryToNews> findAll(Pageable pageable) {
        log.debug("Request to get all NewsSummaryToNews");
        return newsSummaryToNewsRepository.findAll(pageable);
    }

    /**
     *  Get one newsSummaryToNews by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsSummaryToNews findOne(Long id) {
        log.debug("Request to get NewsSummaryToNews : {}", id);
        return newsSummaryToNewsRepository.findOne(id);
    }

    /**
     *  Delete the  newsSummaryToNews by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsSummaryToNews : {}", id);
        newsSummaryToNewsRepository.delete(id);
        newsSummaryToNewsSearchRepository.delete(id);
    }

    /**
     * Search for the newsSummaryToNews corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryToNews> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsSummaryToNews for query {}", query);
        Page<NewsSummaryToNews> result = newsSummaryToNewsSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
