package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.AuthorToLanguage;
import biz.newparadigm.meridian.repository.AuthorToLanguageRepository;
import biz.newparadigm.meridian.repository.search.AuthorToLanguageSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing AuthorToLanguage.
 */
@Service
@Transactional
public class AuthorToLanguageService {

    private final Logger log = LoggerFactory.getLogger(AuthorToLanguageService.class);

    private final AuthorToLanguageRepository authorToLanguageRepository;

    private final AuthorToLanguageSearchRepository authorToLanguageSearchRepository;

    public AuthorToLanguageService(AuthorToLanguageRepository authorToLanguageRepository, AuthorToLanguageSearchRepository authorToLanguageSearchRepository) {
        this.authorToLanguageRepository = authorToLanguageRepository;
        this.authorToLanguageSearchRepository = authorToLanguageSearchRepository;
    }

    /**
     * Save a authorToLanguage.
     *
     * @param authorToLanguage the entity to save
     * @return the persisted entity
     */
    public AuthorToLanguage save(AuthorToLanguage authorToLanguage) {
        log.debug("Request to save AuthorToLanguage : {}", authorToLanguage);
        AuthorToLanguage result = authorToLanguageRepository.save(authorToLanguage);
        authorToLanguageSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the authorToLanguages.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuthorToLanguage> findAll(Pageable pageable) {
        log.debug("Request to get all AuthorToLanguages");
        return authorToLanguageRepository.findAll(pageable);
    }

    /**
     *  Get one authorToLanguage by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AuthorToLanguage findOne(Long id) {
        log.debug("Request to get AuthorToLanguage : {}", id);
        return authorToLanguageRepository.findOne(id);
    }

    /**
     *  Delete the  authorToLanguage by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AuthorToLanguage : {}", id);
        authorToLanguageRepository.delete(id);
        authorToLanguageSearchRepository.delete(id);
    }

    public void deleteByAuthorId(Long authorId) {
        log.debug("Request to delete AuthorToLanguage by author id : {}", authorId);
        authorToLanguageRepository.deleteAllByAuthorId(authorId);
    }

    public void deleteByLanguageId(Long languageId) {
        log.debug("Request to delete AuthorToLanguage by language id : {}", languageId);
        authorToLanguageRepository.deleteAllByLanguageId(languageId);
    }

    /**
     * Search for the authorToLanguage corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuthorToLanguage> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of AuthorToLanguages for query {}", query);
        Page<AuthorToLanguage> result = authorToLanguageSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
