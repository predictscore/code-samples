package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsAttachment;
import biz.newparadigm.meridian.service.NewsAttachmentService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsAttachmentCriteria;
import biz.newparadigm.meridian.service.NewsAttachmentQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsAttachment.
 */
@RestController
@RequestMapping("/api")
public class NewsAttachmentResource {

    private final Logger log = LoggerFactory.getLogger(NewsAttachmentResource.class);

    private static final String ENTITY_NAME = "newsAttachment";

    private final NewsAttachmentService newsAttachmentService;

    private final NewsAttachmentQueryService newsAttachmentQueryService;

    public NewsAttachmentResource(NewsAttachmentService newsAttachmentService, NewsAttachmentQueryService newsAttachmentQueryService) {
        this.newsAttachmentService = newsAttachmentService;
        this.newsAttachmentQueryService = newsAttachmentQueryService;
    }

    /**
     * POST  /news-attachments : Create a new newsAttachment.
     *
     * @param newsAttachment the newsAttachment to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsAttachment, or with status 400 (Bad Request) if the newsAttachment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-attachments")
    @Timed
    public ResponseEntity<NewsAttachment> createNewsAttachment(@Valid @RequestBody NewsAttachment newsAttachment) throws URISyntaxException {
        log.debug("REST request to save NewsAttachment : {}", newsAttachment);
        if (newsAttachment.getId() != null) {
            throw new BadRequestAlertException("A new newsAttachment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsAttachment result = newsAttachmentService.save(newsAttachment);
        return ResponseEntity.created(new URI("/api/news-attachments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-attachments : Updates an existing newsAttachment.
     *
     * @param newsAttachment the newsAttachment to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsAttachment,
     * or with status 400 (Bad Request) if the newsAttachment is not valid,
     * or with status 500 (Internal Server Error) if the newsAttachment couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-attachments")
    @Timed
    public ResponseEntity<NewsAttachment> updateNewsAttachment(@Valid @RequestBody NewsAttachment newsAttachment) throws URISyntaxException {
        log.debug("REST request to update NewsAttachment : {}", newsAttachment);
        if (newsAttachment.getId() == null) {
            return createNewsAttachment(newsAttachment);
        }
        NewsAttachment result = newsAttachmentService.save(newsAttachment);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsAttachment.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-attachments : get all the newsAttachments.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsAttachments in body
     */
    @GetMapping("/news-attachments")
    @Timed
    public ResponseEntity<List<NewsAttachment>> getAllNewsAttachments(NewsAttachmentCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsAttachments by criteria: {}", criteria);
        Page<NewsAttachment> page = newsAttachmentQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-attachments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-attachments/:id : get the "id" newsAttachment.
     *
     * @param id the id of the newsAttachment to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsAttachment, or with status 404 (Not Found)
     */
    @GetMapping("/news-attachments/{id}")
    @Timed
    public ResponseEntity<NewsAttachment> getNewsAttachment(@PathVariable Long id) {
        log.debug("REST request to get NewsAttachment : {}", id);
        NewsAttachment newsAttachment = newsAttachmentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsAttachment));
    }

    /**
     * DELETE  /news-attachments/:id : delete the "id" newsAttachment.
     *
     * @param id the id of the newsAttachment to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-attachments/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsAttachment(@PathVariable Long id) {
        log.debug("REST request to delete NewsAttachment : {}", id);
        newsAttachmentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-attachments?query=:query : search for the newsAttachment corresponding
     * to the query.
     *
     * @param query the query of the newsAttachment search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-attachments")
    @Timed
    public ResponseEntity<List<NewsAttachment>> searchNewsAttachments(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsAttachments for query {}", query);
        Page<NewsAttachment> page = newsAttachmentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-attachments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
