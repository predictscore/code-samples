package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsSummaryPublicImage;
import biz.newparadigm.meridian.repository.NewsSummaryPublicImageRepository;
import biz.newparadigm.meridian.repository.search.NewsSummaryPublicImageSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsSummaryPublicImage.
 */
@Service
@Transactional
public class NewsSummaryPublicImageService {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryPublicImageService.class);

    private final NewsSummaryPublicImageRepository newsSummaryPublicImageRepository;

    private final NewsSummaryPublicImageSearchRepository newsSummaryPublicImageSearchRepository;

    public NewsSummaryPublicImageService(NewsSummaryPublicImageRepository newsSummaryPublicImageRepository, NewsSummaryPublicImageSearchRepository newsSummaryPublicImageSearchRepository) {
        this.newsSummaryPublicImageRepository = newsSummaryPublicImageRepository;
        this.newsSummaryPublicImageSearchRepository = newsSummaryPublicImageSearchRepository;
    }

    /**
     * Save a newsSummaryPublicImage.
     *
     * @param newsSummaryPublicImage the entity to save
     * @return the persisted entity
     */
    public NewsSummaryPublicImage save(NewsSummaryPublicImage newsSummaryPublicImage) {
        log.debug("Request to save NewsSummaryPublicImage : {}", newsSummaryPublicImage);
        NewsSummaryPublicImage result = newsSummaryPublicImageRepository.save(newsSummaryPublicImage);
        newsSummaryPublicImageSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsSummaryPublicImages.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryPublicImage> findAll(Pageable pageable) {
        log.debug("Request to get all NewsSummaryPublicImages");
        return newsSummaryPublicImageRepository.findAll(pageable);
    }

    /**
     *  Get one newsSummaryPublicImage by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsSummaryPublicImage findOne(Long id) {
        log.debug("Request to get NewsSummaryPublicImage : {}", id);
        return newsSummaryPublicImageRepository.findOne(id);
    }

    /**
     *  Delete the  newsSummaryPublicImage by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsSummaryPublicImage : {}", id);
        newsSummaryPublicImageRepository.delete(id);
        newsSummaryPublicImageSearchRepository.delete(id);
    }

    /**
     * Search for the newsSummaryPublicImage corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryPublicImage> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsSummaryPublicImages for query {}", query);
        Page<NewsSummaryPublicImage> result = newsSummaryPublicImageSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
