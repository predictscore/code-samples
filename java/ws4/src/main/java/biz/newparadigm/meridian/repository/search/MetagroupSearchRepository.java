package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.Metagroup;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Metagroup entity.
 */
public interface MetagroupSearchRepository extends ElasticsearchRepository<Metagroup, Long> {
}
