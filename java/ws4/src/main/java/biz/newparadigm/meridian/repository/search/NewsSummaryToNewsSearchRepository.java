package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsSummaryToNews;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsSummaryToNews entity.
 */
public interface NewsSummaryToNewsSearchRepository extends ElasticsearchRepository<NewsSummaryToNews, Long> {
}
