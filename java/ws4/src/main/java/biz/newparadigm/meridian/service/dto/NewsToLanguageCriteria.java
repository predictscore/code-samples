package biz.newparadigm.meridian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the NewsToLanguage entity. This class is used in NewsToLanguageResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /news-to-languages?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NewsToLanguageCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private LongFilter newsId;

    private LongFilter languageId;

    public NewsToLanguageCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getNewsId() {
        return newsId;
    }

    public void setNewsId(LongFilter newsId) {
        this.newsId = newsId;
    }

    public LongFilter getLanguageId() {
        return languageId;
    }

    public void setLanguageId(LongFilter languageId) {
        this.languageId = languageId;
    }

    @Override
    public String toString() {
        return "NewsToLanguageCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (newsId != null ? "newsId=" + newsId + ", " : "") +
                (languageId != null ? "languageId=" + languageId + ", " : "") +
            "}";
    }

}
