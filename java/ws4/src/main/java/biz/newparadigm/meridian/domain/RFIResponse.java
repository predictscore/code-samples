package biz.newparadigm.meridian.domain;

import biz.newparadigm.meridian.domain.util.MeridianDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A RFIResponse.
 */
@Entity
@Table(name = "rfi_responses")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "rfiresponse")
public class RFIResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="rfi_responses_id_seq")
    @SequenceGenerator(name="rfi_responses_id_seq", sequenceName="rfi_responses_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "note", nullable = false)
    private String note;

    @NotNull
    @Column(name = "username", nullable = false)
    private String username;

    @NotNull
    @Column(name = "submission_date", nullable = false)
    @JsonSerialize(using = MeridianDateSerializer.class)
    private ZonedDateTime submissionDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public RFIResponse note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUsername() {
        return username;
    }

    public RFIResponse username(String username) {
        this.username = username;
        return this;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ZonedDateTime getSubmissionDate() {
        return submissionDate;
    }

    public RFIResponse submissionDate(ZonedDateTime submissionDate) {
        this.submissionDate = submissionDate;
        return this;
    }

    public void setSubmissionDate(ZonedDateTime submissionDate) {
        this.submissionDate = submissionDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RFIResponse rFIResponse = (RFIResponse) o;
        if (rFIResponse.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rFIResponse.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RFIResponse{" +
            "id=" + getId() +
            ", note='" + getNote() + "'" +
            ", username='" + getUsername() + "'" +
            ", submissionDate='" + getSubmissionDate() + "'" +
            "}";
    }
}
