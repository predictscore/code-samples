package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.config.ApplicationProperties;
import biz.newparadigm.meridian.domain.Location;
import biz.newparadigm.meridian.security.SecurityUtils;
import biz.newparadigm.meridian.service.CrowdService;
import biz.newparadigm.meridian.service.LocationService;
import biz.newparadigm.meridian.service.util.UserUtil;
import biz.newparadigm.meridian.web.rest.util.AccessUtil;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.Author;
import biz.newparadigm.meridian.service.AuthorService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.AuthorCriteria;
import biz.newparadigm.meridian.service.AuthorQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class AuthorResource {

    private final Logger log = LoggerFactory.getLogger(AuthorResource.class);

    public static final String ENTITY_NAME = "author";

    private final AuthorService authorService;

    private final AuthorQueryService authorQueryService;

    @Autowired
    private ApplicationProperties properties;

    @Autowired
    private CrowdService crowdService;

    @Autowired
    private LocationService locationService;

    public AuthorResource(AuthorService authorService, AuthorQueryService authorQueryService) {
        this.authorService = authorService;
        this.authorQueryService = authorQueryService;
    }

    /**
     * POST  /authors : Create a new author.
     *
     * @param author the author to create
     * @return the ResponseEntity with status 201 (Created) and with body the new author, or with status 400 (Bad Request) if the author has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/authors")
    @Timed
    public ResponseEntity<Author> createAuthor(@RequestBody Author author) throws URISyntaxException {
        log.debug("REST request to save Author : {}", author);
        if (author.getId() != null) {
            throw new BadRequestAlertException("A new author cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (properties.isCrowd() && isNotAdmin()) {
            boolean hasAccess = hasAccess(author);
            if (!hasAccess) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }
        }
        Author result = authorService.save(author);
        return ResponseEntity.created(new URI("/api/authors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /authors : Updates an existing author.
     *
     * @param author the author to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated author,
     * or with status 400 (Bad Request) if the author is not valid,
     * or with status 500 (Internal Server Error) if the author couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping(value = "/authors")
    @Timed
    public ResponseEntity<Author> updateAuthor(@Valid @RequestBody Author author) throws URISyntaxException {
        log.debug("REST request to update Author : {}", author);
        if (properties.isCrowd() && isNotAdmin()) {
            boolean hasAccess = hasAccess(author);
            if (!hasAccess) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }
        }
        if (author.getId() == null) {
            return createAuthor(author);
        }
        Author result = authorService.save(author);
        Author ret = authorService.findOne(result.getId(), true);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, author.getId().toString()))
            .body(ret);
    }

    /**
     * GET  /authors : get all the authors.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of authors in body
     */
    @GetMapping("/authors")
    @Timed
    public ResponseEntity<List<Author>> getAllAuthors(@RequestParam(name = "filter", required = false) String filter,
                                                      @ApiParam AuthorCriteria criteria,
                                                      @ApiParam Pageable pageable) {
        log.debug("REST request to get Authors by criteria: {}", criteria);
        boolean filterByLocation = false;
        List<String> locationNames = new ArrayList<>();
        if (properties.isCrowd() && isNotAdmin()) {
            filterByLocation = true;
            locationNames = UserUtil.getUserLocations(crowdService, SecurityUtils.getCurrentUserLogin());
        }
        Page<Author> page = authorQueryService.findByCriteria(filter, criteria, pageable, filterByLocation, locationNames);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/authors");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /authors/:id : get the "id" author.
     *
     * @param id the id of the author to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the author, or with status 404 (Not Found)
     */
    @GetMapping("/authors/{id}")
    @Timed
    public ResponseEntity<Author> getAuthor(@PathVariable Long id) {
        log.debug("REST request to get Author : {}", id);
        Author author = authorService.findOne(id, true);
        if (properties.isCrowd() && isNotAdmin()) {
            boolean hasAccess = hasAccess(author);
            if (!hasAccess) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        }
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(author));
    }

    /**
     * DELETE  /authors/:id : delete the "id" author.
     *
     * @param id the id of the author to delete
     * @return the ResponseEntity with status 200 (OK), or with status 404 (Not Found)
     */
    @DeleteMapping("/authors/{id}")
    @Timed
    public ResponseEntity<Void> deleteAuthor(@PathVariable Long id) {
        log.debug("REST request to delete Author : {}", id);
        if (properties.isCrowd() && isNotAdmin()) {
            Author author = authorService.findOne(id, false);
            boolean hasAccess = hasAccess(author);
            if (!hasAccess) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        }
        authorService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/authors?query=:query : search for the author corresponding
     * to the query.
     *
     * @param query the query of the author search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/authors")
    @Timed
    public ResponseEntity<List<Author>> searchAuthors(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Authors for query {}", query);
        Page<Author> page = authorService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/authors");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    private boolean isNotAdmin() {
        CrowdUserDetails user = crowdService.getUserByUsername(SecurityUtils.getCurrentUserLogin());
        return !UserUtil.isAdmin(user);
    }

    private boolean hasAccess(Author author) {
        return AccessUtil.hasAccess(crowdService, locationService, author.getLocation());
    }
}
