package biz.newparadigm.meridian.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.AuthorProfile;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.AuthorProfileRepository;
import biz.newparadigm.meridian.repository.search.AuthorProfileSearchRepository;
import biz.newparadigm.meridian.service.dto.AuthorProfileCriteria;


/**
 * Service for executing complex queries for AuthorProfile entities in the database.
 * The main input is a {@link AuthorProfileCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link AuthorProfile} or a {@link Page} of {%link AuthorProfile} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class AuthorProfileQueryService extends QueryService<AuthorProfile> {

    private final Logger log = LoggerFactory.getLogger(AuthorProfileQueryService.class);


    private final AuthorProfileRepository authorProfileRepository;

    private final AuthorProfileSearchRepository authorProfileSearchRepository;

    public AuthorProfileQueryService(AuthorProfileRepository authorProfileRepository, AuthorProfileSearchRepository authorProfileSearchRepository) {
        this.authorProfileRepository = authorProfileRepository;
        this.authorProfileSearchRepository = authorProfileSearchRepository;
    }

    /**
     * Return a {@link List} of {%link AuthorProfile} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuthorProfile> findByCriteria(AuthorProfileCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<AuthorProfile> specification = createSpecification(criteria);
        return authorProfileRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link AuthorProfile} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuthorProfile> findByCriteria(AuthorProfileCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<AuthorProfile> specification = createSpecification(criteria);
        return authorProfileRepository.findAll(specification, page);
    }

    /**
     * Function to convert AuthorProfileCriteria to a {@link Specifications}
     */
    private Specifications<AuthorProfile> createSpecification(AuthorProfileCriteria criteria) {
        Specifications<AuthorProfile> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AuthorProfile_.id));
            }
            if (criteria.getInfo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInfo(), AuthorProfile_.info));
            }
            if (criteria.getUsername() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUsername(), AuthorProfile_.username));
            }
            if (criteria.getSubmissionDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSubmissionDate(), AuthorProfile_.submissionDate));
            }
            if (criteria.getAuthorId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getAuthorId(), AuthorProfile_.author, Author_.id));
            }
        }
        return specification;
    }

}
