package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsSummaryConfidentialImage;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsSummaryConfidentialImage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsSummaryConfidentialImageRepository extends JpaRepository<NewsSummaryConfidentialImage, Long>, JpaSpecificationExecutor<NewsSummaryConfidentialImage> {

}
