package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsOriginalImage;
import biz.newparadigm.meridian.repository.NewsOriginalImageRepository;
import biz.newparadigm.meridian.repository.search.NewsOriginalImageSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsOriginalImage.
 */
@Service
@Transactional
public class NewsOriginalImageService {

    private final Logger log = LoggerFactory.getLogger(NewsOriginalImageService.class);

    private final NewsOriginalImageRepository newsOriginalImageRepository;

    private final NewsOriginalImageSearchRepository newsOriginalImageSearchRepository;

    public NewsOriginalImageService(NewsOriginalImageRepository newsOriginalImageRepository, NewsOriginalImageSearchRepository newsOriginalImageSearchRepository) {
        this.newsOriginalImageRepository = newsOriginalImageRepository;
        this.newsOriginalImageSearchRepository = newsOriginalImageSearchRepository;
    }

    /**
     * Save a newsOriginalImage.
     *
     * @param newsOriginalImage the entity to save
     * @return the persisted entity
     */
    public NewsOriginalImage save(NewsOriginalImage newsOriginalImage) {
        log.debug("Request to save NewsOriginalImage : {}", newsOriginalImage);
        NewsOriginalImage result = newsOriginalImageRepository.save(newsOriginalImage);
        newsOriginalImageSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsOriginalImages.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsOriginalImage> findAll(Pageable pageable) {
        log.debug("Request to get all NewsOriginalImages");
        return newsOriginalImageRepository.findAll(pageable);
    }

    /**
     *  Get one newsOriginalImage by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsOriginalImage findOne(Long id) {
        log.debug("Request to get NewsOriginalImage : {}", id);
        return newsOriginalImageRepository.findOne(id);
    }

    /**
     *  Delete the  newsOriginalImage by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsOriginalImage : {}", id);
        newsOriginalImageRepository.delete(id);
        newsOriginalImageSearchRepository.delete(id);
    }

    /**
     * Search for the newsOriginalImage corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsOriginalImage> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsOriginalImages for query {}", query);
        Page<NewsOriginalImage> result = newsOriginalImageSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
