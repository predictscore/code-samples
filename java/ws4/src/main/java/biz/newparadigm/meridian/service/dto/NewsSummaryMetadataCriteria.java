package biz.newparadigm.meridian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the NewsSummaryMetadata entity. This class is used in NewsSummaryMetadataResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /news-summary-metadata?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NewsSummaryMetadataCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter valueString;

    private FloatFilter valueNumber;

    private ZonedDateTimeFilter valueDate;

    private LongFilter newsSummaryId;

    private LongFilter metagroupId;

    private LongFilter metafieldId;

    public NewsSummaryMetadataCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getValueString() {
        return valueString;
    }

    public void setValueString(StringFilter valueString) {
        this.valueString = valueString;
    }

    public FloatFilter getValueNumber() {
        return valueNumber;
    }

    public void setValueNumber(FloatFilter valueNumber) {
        this.valueNumber = valueNumber;
    }

    public ZonedDateTimeFilter getValueDate() {
        return valueDate;
    }

    public void setValueDate(ZonedDateTimeFilter valueDate) {
        this.valueDate = valueDate;
    }

    public LongFilter getNewsSummaryId() {
        return newsSummaryId;
    }

    public void setNewsSummaryId(LongFilter newsSummaryId) {
        this.newsSummaryId = newsSummaryId;
    }

    public LongFilter getMetagroupId() {
        return metagroupId;
    }

    public void setMetagroupId(LongFilter metagroupId) {
        this.metagroupId = metagroupId;
    }

    public LongFilter getMetafieldId() {
        return metafieldId;
    }

    public void setMetafieldId(LongFilter metafieldId) {
        this.metafieldId = metafieldId;
    }

    @Override
    public String toString() {
        return "NewsSummaryMetadataCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (valueString != null ? "valueString=" + valueString + ", " : "") +
                (valueNumber != null ? "valueNumber=" + valueNumber + ", " : "") +
                (valueDate != null ? "valueDate=" + valueDate + ", " : "") +
                (newsSummaryId != null ? "newsSummaryId=" + newsSummaryId + ", " : "") +
                (metagroupId != null ? "metagroupId=" + metagroupId + ", " : "") +
                (metafieldId != null ? "metafieldId=" + metafieldId + ", " : "") +
            "}";
    }

}
