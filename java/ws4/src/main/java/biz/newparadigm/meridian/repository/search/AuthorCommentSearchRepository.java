package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.AuthorComment;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AuthorComment entity.
 */
public interface AuthorCommentSearchRepository extends ElasticsearchRepository<AuthorComment, Long> {
}
