package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.AuthorToSiteChatgroup;
import biz.newparadigm.meridian.repository.AuthorToSiteChatgroupRepository;
import biz.newparadigm.meridian.repository.search.AuthorToSiteChatgroupSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing AuthorToSiteChatgroup.
 */
@Service
@Transactional
public class AuthorToSiteChatgroupService {

    private final Logger log = LoggerFactory.getLogger(AuthorToSiteChatgroupService.class);

    private final AuthorToSiteChatgroupRepository authorToSiteChatgroupRepository;

    private final AuthorToSiteChatgroupSearchRepository authorToSiteChatgroupSearchRepository;

    public AuthorToSiteChatgroupService(AuthorToSiteChatgroupRepository authorToSiteChatgroupRepository, AuthorToSiteChatgroupSearchRepository authorToSiteChatgroupSearchRepository) {
        this.authorToSiteChatgroupRepository = authorToSiteChatgroupRepository;
        this.authorToSiteChatgroupSearchRepository = authorToSiteChatgroupSearchRepository;
    }

    /**
     * Save a authorToSiteChatgroup.
     *
     * @param authorToSiteChatgroup the entity to save
     * @return the persisted entity
     */
    public AuthorToSiteChatgroup save(AuthorToSiteChatgroup authorToSiteChatgroup) {
        log.debug("Request to save AuthorToSiteChatgroup : {}", authorToSiteChatgroup);
        AuthorToSiteChatgroup result = authorToSiteChatgroupRepository.save(authorToSiteChatgroup);
        authorToSiteChatgroupSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the authorToSiteChatgroups.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuthorToSiteChatgroup> findAll(Pageable pageable) {
        log.debug("Request to get all AuthorToSiteChatgroups");
        return authorToSiteChatgroupRepository.findAll(pageable);
    }

    /**
     *  Get one authorToSiteChatgroup by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AuthorToSiteChatgroup findOne(Long id) {
        log.debug("Request to get AuthorToSiteChatgroup : {}", id);
        return authorToSiteChatgroupRepository.findOne(id);
    }

    /**
     *  Delete the  authorToSiteChatgroup by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AuthorToSiteChatgroup : {}", id);
        authorToSiteChatgroupRepository.delete(id);
        authorToSiteChatgroupSearchRepository.delete(id);
    }

    /**
     * Search for the authorToSiteChatgroup corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuthorToSiteChatgroup> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of AuthorToSiteChatgroups for query {}", query);
        Page<AuthorToSiteChatgroup> result = authorToSiteChatgroupSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
