package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.SiteChatgroup;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SiteChatgroup entity.
 */
public interface SiteChatgroupSearchRepository extends ElasticsearchRepository<SiteChatgroup, Long> {
}
