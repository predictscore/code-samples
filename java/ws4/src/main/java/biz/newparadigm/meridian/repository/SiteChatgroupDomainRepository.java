package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.SiteChatgroupDomain;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SiteChatgroupDomain entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SiteChatgroupDomainRepository extends JpaRepository<SiteChatgroupDomain, Long>, JpaSpecificationExecutor<SiteChatgroupDomain> {

}
