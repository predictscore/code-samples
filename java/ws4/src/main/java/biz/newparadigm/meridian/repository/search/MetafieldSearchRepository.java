package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.Metafield;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Metafield entity.
 */
public interface MetafieldSearchRepository extends ElasticsearchRepository<Metafield, Long> {
}
