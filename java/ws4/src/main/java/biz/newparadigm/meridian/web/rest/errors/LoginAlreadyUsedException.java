package biz.newparadigm.meridian.web.rest.errors;

public class LoginAlreadyUsedException extends BadRequestAlertException {

    public LoginAlreadyUsedException() {
        super("Login already in use", "userManagement", "userexists");
    }
}
