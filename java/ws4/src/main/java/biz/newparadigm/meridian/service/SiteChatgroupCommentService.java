package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.SiteChatgroupComment;
import biz.newparadigm.meridian.repository.SiteChatgroupCommentRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupCommentSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SiteChatgroupComment.
 */
@Service
@Transactional
public class SiteChatgroupCommentService {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupCommentService.class);

    private final SiteChatgroupCommentRepository siteChatgroupCommentRepository;

    private final SiteChatgroupCommentSearchRepository siteChatgroupCommentSearchRepository;

    public SiteChatgroupCommentService(SiteChatgroupCommentRepository siteChatgroupCommentRepository, SiteChatgroupCommentSearchRepository siteChatgroupCommentSearchRepository) {
        this.siteChatgroupCommentRepository = siteChatgroupCommentRepository;
        this.siteChatgroupCommentSearchRepository = siteChatgroupCommentSearchRepository;
    }

    /**
     * Save a siteChatgroupComment.
     *
     * @param siteChatgroupComment the entity to save
     * @return the persisted entity
     */
    public SiteChatgroupComment save(SiteChatgroupComment siteChatgroupComment) {
        log.debug("Request to save SiteChatgroupComment : {}", siteChatgroupComment);
        SiteChatgroupComment result = siteChatgroupCommentRepository.save(siteChatgroupComment);
        siteChatgroupCommentSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the siteChatgroupComments.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupComment> findAll(Pageable pageable) {
        log.debug("Request to get all SiteChatgroupComments");
        return siteChatgroupCommentRepository.findAll(pageable);
    }

    /**
     *  Get one siteChatgroupComment by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SiteChatgroupComment findOne(Long id) {
        log.debug("Request to get SiteChatgroupComment : {}", id);
        return siteChatgroupCommentRepository.findOne(id);
    }

    /**
     *  Delete the  siteChatgroupComment by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SiteChatgroupComment : {}", id);
        siteChatgroupCommentRepository.delete(id);
        siteChatgroupCommentSearchRepository.delete(id);
    }

    /**
     * Search for the siteChatgroupComment corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupComment> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SiteChatgroupComments for query {}", query);
        Page<SiteChatgroupComment> result = siteChatgroupCommentSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
