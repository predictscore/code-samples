package biz.newparadigm.meridian.web.rest.errors.problem;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.joining;
import static org.zalando.problem.spi.StackTraceProcessor.COMPOUND;

@Immutable
public abstract class MeridianThrowableProblem extends RuntimeException implements MeridianProblem, MeridianExceptional {
    protected MeridianThrowableProblem() {
        this(null);
    }

    protected MeridianThrowableProblem(@Nullable final MeridianThrowableProblem cause) {
        super(cause);

        final Collection<StackTraceElement> stackTrace = COMPOUND.process(asList(getStackTrace()));
        setStackTrace(stackTrace.toArray(new StackTraceElement[stackTrace.size()]));
    }

    @Override
    public String getMessage() {
        return Stream.of(getTitle(), getDetail())
            .filter(Objects::nonNull)
            .collect(joining(": "));
    }

    @Override
    public MeridianThrowableProblem getCause() {
        // cast is safe, since the only way to set this is our constructor
        return (MeridianThrowableProblem) super.getCause();
    }

    @Override
    public String toString() {
        return MeridianProblem.toString(this);
    }
}
