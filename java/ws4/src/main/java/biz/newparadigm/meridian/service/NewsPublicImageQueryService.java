package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsPublicImage;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsPublicImageRepository;
import biz.newparadigm.meridian.repository.search.NewsPublicImageSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsPublicImageCriteria;


/**
 * Service for executing complex queries for NewsPublicImage entities in the database.
 * The main input is a {@link NewsPublicImageCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsPublicImage} or a {@link Page} of {%link NewsPublicImage} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsPublicImageQueryService extends QueryService<NewsPublicImage> {

    private final Logger log = LoggerFactory.getLogger(NewsPublicImageQueryService.class);


    private final NewsPublicImageRepository newsPublicImageRepository;

    private final NewsPublicImageSearchRepository newsPublicImageSearchRepository;

    public NewsPublicImageQueryService(NewsPublicImageRepository newsPublicImageRepository, NewsPublicImageSearchRepository newsPublicImageSearchRepository) {
        this.newsPublicImageRepository = newsPublicImageRepository;
        this.newsPublicImageSearchRepository = newsPublicImageSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsPublicImage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsPublicImage> findByCriteria(NewsPublicImageCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsPublicImage> specification = createSpecification(criteria);
        return newsPublicImageRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsPublicImage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsPublicImage> findByCriteria(NewsPublicImageCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsPublicImage> specification = createSpecification(criteria);
        return newsPublicImageRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsPublicImageCriteria to a {@link Specifications}
     */
    private Specifications<NewsPublicImage> createSpecification(NewsPublicImageCriteria criteria) {
        Specifications<NewsPublicImage> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsPublicImage_.id));
            }
            if (criteria.getFilename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFilename(), NewsPublicImage_.filename));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), NewsPublicImage_.description));
            }
            if (criteria.getDescriptionTranslation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescriptionTranslation(), NewsPublicImage_.descriptionTranslation));
            }
            if (criteria.getImageOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getImageOrder(), NewsPublicImage_.imageOrder));
            }
            if (criteria.getNewsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsId(), NewsPublicImage_.news, News_.id));
            }
        }
        return specification;
    }

}
