package biz.newparadigm.meridian.service.dto.old;

import java.util.ArrayList;
import java.util.List;

public class SiteChatgroupDTO {
    private Long siteChatGroupId = -1L;
    private boolean isSite;
    private String originalName;
    private String englishName;
    private LocationDTO location;
    private List<SiteChatgroupNameDTO> siteChatGroupNames = new ArrayList<>();
    private List<SiteChatgroupDomainDTO> siteChatGroupDomains = new ArrayList<>();
    private List<SiteChatgroupAuthorDTO> siteChatGroupAuthors= new ArrayList<>();
    private List<SiteChatgroupIpDTO> siteIps= new ArrayList<>();
    private List<SiteChatgroupNumUserDTO> siteChatGroupNumUsers = new ArrayList<>();
    private List<SiteChatgroupNumVisitorDTO> siteChatGroupNumVisitors = new ArrayList<>();
    private List<TagDTO> siteChatGroupTags = new ArrayList<>();
    private List<LanguageDTO> siteChatGroupLanguages = new ArrayList<>();

    public Long getSiteChatGroupId() {
        return siteChatGroupId;
    }

    public void setSiteChatGroupId(Long siteChatGroupId) {
        this.siteChatGroupId = siteChatGroupId;
    }

    public boolean isIsSite() {
        return isSite;
    }

    public void setIsSite(boolean isSite) {
        this.isSite = isSite;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }

    public List<SiteChatgroupNameDTO> getSiteChatGroupNames() {
        return siteChatGroupNames;
    }

    public void setSiteChatGroupNames(List<SiteChatgroupNameDTO> siteChatGroupNames) {
        this.siteChatGroupNames = siteChatGroupNames;
    }

    public List<SiteChatgroupDomainDTO> getSiteChatGroupDomains() {
        return siteChatGroupDomains;
    }

    public void setSiteChatGroupDomains(List<SiteChatgroupDomainDTO> siteChatGroupDomains) {
        this.siteChatGroupDomains = siteChatGroupDomains;
    }

    public List<SiteChatgroupAuthorDTO> getSiteChatGroupAuthors() {
        return siteChatGroupAuthors;
    }

    public void setSiteChatGroupAuthors(List<SiteChatgroupAuthorDTO> siteChatGroupAuthors) {
        this.siteChatGroupAuthors = siteChatGroupAuthors;
    }

    public List<SiteChatgroupIpDTO> getSiteIps() {
        return siteIps;
    }

    public void setSiteIps(List<SiteChatgroupIpDTO> siteIps) {
        this.siteIps = siteIps;
    }

    public List<SiteChatgroupNumUserDTO> getSiteChatGroupNumUsers() {
        return siteChatGroupNumUsers;
    }

    public void setSiteChatGroupNumUsers(List<SiteChatgroupNumUserDTO> siteChatGroupNumUsers) {
        this.siteChatGroupNumUsers = siteChatGroupNumUsers;
    }

    public List<SiteChatgroupNumVisitorDTO> getSiteChatGroupNumVisitors() {
        return siteChatGroupNumVisitors;
    }

    public void setSiteChatGroupNumVisitors(List<SiteChatgroupNumVisitorDTO> siteChatGroupNumVisitors) {
        this.siteChatGroupNumVisitors = siteChatGroupNumVisitors;
    }

    public List<TagDTO> getSiteChatGroupTags() {
        return siteChatGroupTags;
    }

    public void setSiteChatGroupTags(List<TagDTO> siteChatGroupTags) {
        this.siteChatGroupTags = siteChatGroupTags;
    }

    public List<LanguageDTO> getSiteChatGroupLanguages() {
        return siteChatGroupLanguages;
    }

    public void setSiteChatGroupLanguages(List<LanguageDTO> siteChatGroupLanguages) {
        this.siteChatGroupLanguages = siteChatGroupLanguages;
    }
}
