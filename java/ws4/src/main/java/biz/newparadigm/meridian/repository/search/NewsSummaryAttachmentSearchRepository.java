package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsSummaryAttachment;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsSummaryAttachment entity.
 */
public interface NewsSummaryAttachmentSearchRepository extends ElasticsearchRepository<NewsSummaryAttachment, Long> {
}
