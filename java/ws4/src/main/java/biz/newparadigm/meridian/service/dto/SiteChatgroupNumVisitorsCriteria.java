package biz.newparadigm.meridian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the SiteChatgroupNumVisitors entity. This class is used in SiteChatgroupNumVisitorsResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /site-chatgroup-num-visitors?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SiteChatgroupNumVisitorsCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private IntegerFilter numVisitors;

    private ZonedDateTimeFilter numVisitorDate;

    private LongFilter siteChatgroupId;

    public SiteChatgroupNumVisitorsCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getNumVisitors() {
        return numVisitors;
    }

    public void setNumVisitors(IntegerFilter numVisitors) {
        this.numVisitors = numVisitors;
    }

    public ZonedDateTimeFilter getNumVisitorDate() {
        return numVisitorDate;
    }

    public void setNumVisitorDate(ZonedDateTimeFilter numVisitorDate) {
        this.numVisitorDate = numVisitorDate;
    }

    public LongFilter getSiteChatgroupId() {
        return siteChatgroupId;
    }

    public void setSiteChatgroupId(LongFilter siteChatgroupId) {
        this.siteChatgroupId = siteChatgroupId;
    }

    @Override
    public String toString() {
        return "SiteChatgroupNumVisitorsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (numVisitors != null ? "numVisitors=" + numVisitors + ", " : "") +
                (numVisitorDate != null ? "numVisitorDate=" + numVisitorDate + ", " : "") +
                (siteChatgroupId != null ? "siteChatgroupId=" + siteChatgroupId + ", " : "") +
            "}";
    }

}
