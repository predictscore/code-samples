package biz.newparadigm.meridian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the Author entity. This class is used in AuthorResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /authors?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AuthorCriteria implements Serializable {
    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter originalName;

    private StringFilter englishName;

    private LongFilter locationId;

    private StringFilter location;

    private StringFilter contact;

    public AuthorCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getOriginalName() {
        return originalName;
    }

    public void setOriginalName(StringFilter originalName) {
        this.originalName = originalName;
    }

    public StringFilter getEnglishName() {
        return englishName;
    }

    public void setEnglishName(StringFilter englishName) {
        this.englishName = englishName;
    }

    public LongFilter getLocationId() {
        return locationId;
    }

    public void setLocationId(LongFilter locationId) {
        this.locationId = locationId;
    }

    public StringFilter getLocation() {
        return location;
    }

    public void setLocation(StringFilter location) {
        this.location = location;
    }

    public StringFilter getContact() {
        return contact;
    }

    public void setContact(StringFilter contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return "AuthorCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (originalName != null ? "originalName=" + originalName + ", " : "") +
                (englishName != null ? "englishName=" + englishName + ", " : "") +
                (locationId != null ? "locationId=" + locationId + ", " : "") +
            "}";
    }

}
