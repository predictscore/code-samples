package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.RFIResponse;
import biz.newparadigm.meridian.service.RFIResponseService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.RFIResponseCriteria;
import biz.newparadigm.meridian.service.RFIResponseQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RFIResponse.
 */
@RestController
@RequestMapping("/api")
public class RFIResponseResource {

    private final Logger log = LoggerFactory.getLogger(RFIResponseResource.class);

    private static final String ENTITY_NAME = "rFIResponse";

    private final RFIResponseService rFIResponseService;

    private final RFIResponseQueryService rFIResponseQueryService;

    public RFIResponseResource(RFIResponseService rFIResponseService, RFIResponseQueryService rFIResponseQueryService) {
        this.rFIResponseService = rFIResponseService;
        this.rFIResponseQueryService = rFIResponseQueryService;
    }

    /**
     * POST  /r-fi-responses : Create a new rFIResponse.
     *
     * @param rFIResponse the rFIResponse to create
     * @return the ResponseEntity with status 201 (Created) and with body the new rFIResponse, or with status 400 (Bad Request) if the rFIResponse has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/r-fi-responses")
    @Timed
    public ResponseEntity<RFIResponse> createRFIResponse(@Valid @RequestBody RFIResponse rFIResponse) throws URISyntaxException {
        log.debug("REST request to save RFIResponse : {}", rFIResponse);
        if (rFIResponse.getId() != null) {
            throw new BadRequestAlertException("A new rFIResponse cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RFIResponse result = rFIResponseService.save(rFIResponse);
        return ResponseEntity.created(new URI("/api/r-fi-responses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /r-fi-responses : Updates an existing rFIResponse.
     *
     * @param rFIResponse the rFIResponse to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated rFIResponse,
     * or with status 400 (Bad Request) if the rFIResponse is not valid,
     * or with status 500 (Internal Server Error) if the rFIResponse couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/r-fi-responses")
    @Timed
    public ResponseEntity<RFIResponse> updateRFIResponse(@Valid @RequestBody RFIResponse rFIResponse) throws URISyntaxException {
        log.debug("REST request to update RFIResponse : {}", rFIResponse);
        if (rFIResponse.getId() == null) {
            return createRFIResponse(rFIResponse);
        }
        RFIResponse result = rFIResponseService.save(rFIResponse);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, rFIResponse.getId().toString()))
            .body(result);
    }

    /**
     * GET  /r-fi-responses : get all the rFIResponses.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of rFIResponses in body
     */
    @GetMapping("/r-fi-responses")
    @Timed
    public ResponseEntity<List<RFIResponse>> getAllRFIResponses(RFIResponseCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get RFIResponses by criteria: {}", criteria);
        Page<RFIResponse> page = rFIResponseQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/r-fi-responses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /r-fi-responses/:id : get the "id" rFIResponse.
     *
     * @param id the id of the rFIResponse to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the rFIResponse, or with status 404 (Not Found)
     */
    @GetMapping("/r-fi-responses/{id}")
    @Timed
    public ResponseEntity<RFIResponse> getRFIResponse(@PathVariable Long id) {
        log.debug("REST request to get RFIResponse : {}", id);
        RFIResponse rFIResponse = rFIResponseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(rFIResponse));
    }

    /**
     * DELETE  /r-fi-responses/:id : delete the "id" rFIResponse.
     *
     * @param id the id of the rFIResponse to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/r-fi-responses/{id}")
    @Timed
    public ResponseEntity<Void> deleteRFIResponse(@PathVariable Long id) {
        log.debug("REST request to delete RFIResponse : {}", id);
        rFIResponseService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/r-fi-responses?query=:query : search for the rFIResponse corresponding
     * to the query.
     *
     * @param query the query of the rFIResponse search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/r-fi-responses")
    @Timed
    public ResponseEntity<List<RFIResponse>> searchRFIResponses(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of RFIResponses for query {}", query);
        Page<RFIResponse> page = rFIResponseService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/r-fi-responses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
