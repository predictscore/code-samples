package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.*;
import biz.newparadigm.meridian.domain.audit.AuditUtil;
import biz.newparadigm.meridian.repository.AuthorToSiteChatgroupRepository;
import biz.newparadigm.meridian.repository.SiteChatgroupRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupSearchRepository;
import biz.newparadigm.meridian.security.SecurityUtils;
import biz.newparadigm.meridian.service.filter.SpecificationBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.Hibernate;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Diff;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;


import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

@Service
@Transactional
public class SiteChatgroupService {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupService.class);

    private final SiteChatgroupRepository siteChatgroupRepository;

    private final SiteChatgroupSearchRepository siteChatgroupSearchRepository;

    @Autowired
    private SiteChatgroupCommentService commentService;

    @Autowired
    private SiteChatgroupDomainService domainService;

    @Autowired
    private SiteChatgroupIpService ipService;

    @Autowired
    private SiteChatgroupNameService nameService;

    @Autowired
    private SiteChatgroupProfileService profileService;

    @Autowired
    private SiteChatgroupNumUsersService numUsersService;

    @Autowired
    private SiteChatgroupNumVisitorsService numVisitorsService;

    @Autowired
    private SiteChatgroupToTagService tagService;

    @Autowired
    private AuthorToSiteChatgroupService authorToSiteChatgroupService;

    @Autowired
    private SiteChatgroupToLanguageService languageService;

    @Autowired
    private EntityAuditService entityAuditService;

    public SiteChatgroupService(SiteChatgroupRepository siteChatgroupRepository, SiteChatgroupSearchRepository siteChatgroupSearchRepository) {
        this.siteChatgroupRepository = siteChatgroupRepository;
        this.siteChatgroupSearchRepository = siteChatgroupSearchRepository;
    }

    /**
     * Save a siteChatgroup.
     *
     * @param siteChatgroup the entity to save
     * @return the persisted entity
     */
    public SiteChatgroup save(SiteChatgroup siteChatgroup) {
        log.debug("Request to save SiteChatgroup : {}", siteChatgroup);
        SiteChatgroup originalSiteChatgroup;
        String originalSiteChatgroupJson = "{}";
        ObjectMapper mapper = AuditUtil.getMapper();
        boolean insert = true;
        SiteChatgroup result;
        if (siteChatgroup.getId() != null && siteChatgroup.getId() > 0L) {
            insert = false;
            SiteChatgroup originalSiteChatgroup1 = siteChatgroupRepository.findOne(siteChatgroup.getId());
            originalSiteChatgroup = AuditUtil.prepareSiteChatgroup(originalSiteChatgroup1);
            try {
                originalSiteChatgroupJson = mapper.writeValueAsString(originalSiteChatgroup);
            } catch (Exception e) {
                originalSiteChatgroupJson = "{}";
            }

            if (siteChatgroup.getDomains() != null && !siteChatgroup.getDomains().isEmpty()) {
                siteChatgroup.getDomains().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0) {
                        item.setSiteChatgroup(siteChatgroup.toJPASimpleObject());
                        domainService.save(item);
                    }
                });
            }
            if (siteChatgroup.getIps() != null && !siteChatgroup.getIps().isEmpty()) {
                siteChatgroup.getIps().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0) {
                        item.setSiteChatgroup(siteChatgroup.toJPASimpleObject());
                        ipService.save(item);
                    }
                });
            }
            if (siteChatgroup.getNames() != null && !siteChatgroup.getNames().isEmpty()) {
                siteChatgroup.getNames().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0) {
                        item.setSiteChatgroup(siteChatgroup.toJPASimpleObject());
                        nameService.save(item);
                    }
                });
            }
            if (siteChatgroup.getNumUsers() != null && !siteChatgroup.getNumUsers().isEmpty()) {
                siteChatgroup.getNumUsers().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0) {
                        item.setSiteChatgroup(siteChatgroup.toJPASimpleObject());
                        item.setSiteChatgroup(siteChatgroup.toJPASimpleObject());
                        numUsersService.save(item);
                    }
                });
            }
            if (siteChatgroup.getNumVisitors() != null && !siteChatgroup.getNumVisitors().isEmpty()) {
                siteChatgroup.getNumVisitors().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0) {
                        item.setSiteChatgroup(siteChatgroup.toJPASimpleObject());
                        numVisitorsService.save(item);
                    }
                });
            }
            if (siteChatgroup.getTags() != null && !siteChatgroup.getTags().isEmpty()) {
                siteChatgroup.getTags().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0) {
                        item.setSiteChatgroup(siteChatgroup.toJPASimpleObject());
                        tagService.save(item);
                    }
                });
            }
            if (siteChatgroup.getAuthors() != null && !siteChatgroup.getAuthors().isEmpty()) {
                siteChatgroup.getAuthors().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0) {
                        item.setSiteChatgroup(siteChatgroup.toJPASimpleObject());
                        authorToSiteChatgroupService.save(item);
                    }
                });
            }
            if (siteChatgroup.getLanguages() != null && !siteChatgroup.getLanguages().isEmpty()) {
                siteChatgroup.getLanguages().forEach(item -> {
                    if (item.getId() == null || item.getId() == 0) {
                        item.setSiteChatgroup(siteChatgroup.toJPASimpleObject());
                        languageService.save(item);
                    }
                });
            }
            siteChatgroup.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
            siteChatgroup.setLastModifiedDate(ZonedDateTime.now(ZoneId.of("UTC")));
            result = siteChatgroupRepository.save(siteChatgroup);
        } else {
            List<SiteChatgroupDomain> domains = new ArrayList<>(siteChatgroup.getDomains());
            List<SiteChatgroupIp> ips = new ArrayList<>(siteChatgroup.getIps());
            List<SiteChatgroupName> names = new ArrayList<>(siteChatgroup.getNames());
            List<SiteChatgroupNumUsers> numUsers = new ArrayList<>(siteChatgroup.getNumUsers());
            List<SiteChatgroupNumVisitors> numVisitors = new ArrayList<>(siteChatgroup.getNumVisitors());
            List<SiteChatgroupToLanguage> languages = new ArrayList<>(siteChatgroup.getLanguages());
            List<SiteChatgroupToTag> tags = new ArrayList<>(siteChatgroup.getTags());
            List<AuthorToSiteChatgroup> authors = new ArrayList<>(siteChatgroup.getAuthors());

            siteChatgroup.getDomains().clear();
            siteChatgroup.getIps().clear();
            siteChatgroup.getNames().clear();
            siteChatgroup.getNumUsers().clear();
            siteChatgroup.getNumVisitors().clear();
            siteChatgroup.getLanguages().clear();
            siteChatgroup.getTags().clear();
            siteChatgroup.getAuthors().clear();

            siteChatgroup.setCreatedBy(SecurityUtils.getCurrentUserLogin());
            siteChatgroup.setCreatedDate(ZonedDateTime.now(ZoneId.of("UTC")));
            result = siteChatgroupRepository.save(siteChatgroup);

            if (!domains.isEmpty()) {
                domains.forEach(item -> {
                    item.setSiteChatgroup(result);
                    domainService.save(item);
                    siteChatgroup.getDomains().add(item);
                });
            }
            if (!ips.isEmpty()) {
                ips.forEach(item -> {
                    item.setSiteChatgroup(result);
                    ipService.save(item);
                    siteChatgroup.getIps().add(item);
                });
            }
            if (!names.isEmpty()) {
                names.forEach(item -> {
                    item.setSiteChatgroup(result);
                    nameService.save(item);
                    siteChatgroup.getNames().add(item);
                });
            }
            if (!numUsers.isEmpty()) {
                numUsers.forEach(item -> {
                    item.setSiteChatgroup(result);
                    numUsersService.save(item);
                    siteChatgroup.getNumUsers().add(item);
                });
            }
            if (!numVisitors.isEmpty()) {
                numVisitors.forEach(item -> {
                    item.setSiteChatgroup(result);
                    numVisitorsService.save(item);
                    siteChatgroup.getNumVisitors().add(item);
                });
            }
            if (!tags.isEmpty()) {
                tags.forEach(item -> {
                    item.setSiteChatgroup(result);
                    tagService.save(item);
                    siteChatgroup.getTags().add(item);
                });
            }
            if (!authors.isEmpty()) {
                authors.forEach(item -> {
                    item.setSiteChatgroup(result);
                    authorToSiteChatgroupService.save(item);
                    siteChatgroup.getAuthors().add(item);
                });
            }
            if (!languages.isEmpty()) {
                languages.forEach(item -> {
                    item.setSiteChatgroup(result);
                    languageService.save(item);
                    siteChatgroup.getLanguages().add(item);
                });
            }
        }

        SiteChatgroup updatedSiteChatgroup1 = findOne(result.getId(), true);
        SiteChatgroup updatedSiteChatgroup = AuditUtil.prepareSiteChatgroup(updatedSiteChatgroup1);

        String updatedSiteChatgroupJson;
        try {
            updatedSiteChatgroupJson = mapper.writeValueAsString(updatedSiteChatgroup);
        } catch (Exception e) {
            updatedSiteChatgroupJson = "{}";
        }

        try {
            SiteChatgroup beforeNode = mapper.readValue(originalSiteChatgroupJson, SiteChatgroup.class);
            SiteChatgroup afterNode = mapper.readValue(updatedSiteChatgroupJson, SiteChatgroup.class);
            Javers j = JaversBuilder.javers().build();
            Diff diff = j.compare(beforeNode, afterNode);
            EntityAudit audit = AuditUtil.getAuditEntity(mapper, insert, updatedSiteChatgroup1.getId(), diff, originalSiteChatgroupJson, updatedSiteChatgroupJson, SiteChatgroup.class.getName());
            if (audit != null) {
                entityAuditService.save(audit);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     *  Get all the siteChatgroups.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroup> findAll(Pageable pageable) {
        log.debug("Request to get all SiteChatgroups");
        return siteChatgroupRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public List<SiteChatgroup> findAllOld(String originalName, String englishName, String domainName, boolean isExact, String locationName) {
        log.debug("Request to get all SiteChatgroups");
        if (domainName != null && !domainName.isEmpty()) {
            if (locationName != null && !locationName.isEmpty()) {
                return isExact ? siteChatgroupRepository.findEqDomainIdLocation(domainName, locationName) : siteChatgroupRepository.findLikeDomainIdLocation(domainName, locationName);
            } else {
                return isExact ? siteChatgroupRepository.findEqDomainId(domainName) : siteChatgroupRepository.findLikeDomainId(domainName);
            }
        } else {
            SpecificationBuilder<SiteChatgroup> builder = new SpecificationBuilder<>();
            Specifications<SiteChatgroup> specification = Specifications.where(null);
            specification = specification.and(builder.greaterThanOrEqualTo("id", 0));
            if (originalName != null) {
                specification = specification.and(isExact
                    ? builder.equalsSpecification("originalName", originalName)
                    : builder.likeUpperSpecification("originalName", originalName));
            } else if (englishName != null) {
                specification = specification.and(isExact
                    ? builder.equalsSpecification("englishName", englishName)
                    : builder.likeUpperSpecification("englishName", englishName));
            }
            if (locationName != null) {
                specification = specification.and(builder.buildReferringEntitySpecification("location", "name", locationName));
            }
            List<SiteChatgroup> result = siteChatgroupRepository.findAll(specification);
            return result;
        }
    }


    /**
     *  Get one siteChatgroup by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SiteChatgroup findOne(Long id, boolean loadLazyItems) {
        log.debug("Request to get SiteChatgroup : {}", id);
        SiteChatgroup siteChatgroup = siteChatgroupRepository.findOne(id);
        if (loadLazyItems && siteChatgroup != null) {
            Hibernate.initialize(siteChatgroup.getDomains());
            Hibernate.initialize(siteChatgroup.getIps());
            Hibernate.initialize(siteChatgroup.getNames());
            Hibernate.initialize(siteChatgroup.getNumUsers());
            Hibernate.initialize(siteChatgroup.getNumVisitors());
            Hibernate.initialize(siteChatgroup.getLanguages());
            Hibernate.initialize(siteChatgroup.getTags());
            Hibernate.initialize(siteChatgroup.getAuthors());
        }
        return siteChatgroup;
    }

    /**
     *  Delete the  siteChatgroup by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SiteChatgroup : {}", id);
        siteChatgroupRepository.delete(id);
        siteChatgroupSearchRepository.delete(id);
    }

    /**
     * Search for the siteChatgroup corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroup> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SiteChatgroups for query {}", query);
        Page<SiteChatgroup> result = siteChatgroupSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
