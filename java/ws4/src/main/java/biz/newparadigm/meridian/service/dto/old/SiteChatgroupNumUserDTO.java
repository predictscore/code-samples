package biz.newparadigm.meridian.service.dto.old;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class SiteChatgroupNumUserDTO {
    private Long siteChatGroupId = -1L;
    private int numUsers;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.dateWithTimeFormat)
    private Date numUserDate;

    public Long getSiteChatGroupId() {
        return siteChatGroupId;
    }

    public void setSiteChatGroupId(Long siteChatGroupId) {
        this.siteChatGroupId = siteChatGroupId;
    }

    public int getNumUsers() {
        return numUsers;
    }

    public void setNumUsers(int numUsers) {
        this.numUsers = numUsers;
    }

    public Date getNumUserDate() {
        return numUserDate;
    }

    public void setNumUserDate(Date numUserDate) {
        this.numUserDate = numUserDate;
    }
}
