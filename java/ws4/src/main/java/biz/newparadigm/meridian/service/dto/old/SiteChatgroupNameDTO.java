package biz.newparadigm.meridian.service.dto.old;

public class SiteChatgroupNameDTO {
    private Long siteChatGroupId = -1L;
    private String originalName;
    private String englishName;

    public Long getSiteChatGroupId() {
        return siteChatGroupId;
    }

    public void setSiteChatGroupId(Long siteChatGroupId) {
        this.siteChatGroupId = siteChatGroupId;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }
}
