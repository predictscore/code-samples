package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsToTag;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsToTagRepository;
import biz.newparadigm.meridian.repository.search.NewsToTagSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsToTagCriteria;


/**
 * Service for executing complex queries for NewsToTag entities in the database.
 * The main input is a {@link NewsToTagCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsToTag} or a {@link Page} of {%link NewsToTag} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsToTagQueryService extends QueryService<NewsToTag> {

    private final Logger log = LoggerFactory.getLogger(NewsToTagQueryService.class);


    private final NewsToTagRepository newsToTagRepository;

    private final NewsToTagSearchRepository newsToTagSearchRepository;

    public NewsToTagQueryService(NewsToTagRepository newsToTagRepository, NewsToTagSearchRepository newsToTagSearchRepository) {
        this.newsToTagRepository = newsToTagRepository;
        this.newsToTagSearchRepository = newsToTagSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsToTag} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsToTag> findByCriteria(NewsToTagCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsToTag> specification = createSpecification(criteria);
        return newsToTagRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsToTag} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsToTag> findByCriteria(NewsToTagCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsToTag> specification = createSpecification(criteria);
        return newsToTagRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsToTagCriteria to a {@link Specifications}
     */
    private Specifications<NewsToTag> createSpecification(NewsToTagCriteria criteria) {
        Specifications<NewsToTag> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsToTag_.id));
            }
            if (criteria.getNewsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsId(), NewsToTag_.news, News_.id));
            }
            if (criteria.getTagId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getTagId(), NewsToTag_.tag, Tag_.id));
            }
        }
        return specification;
    }

}
