package biz.newparadigm.meridian.service.util;

import biz.newparadigm.meridian.service.CrowdService;
import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class UserUtil {
    public static List<String> getGroups(Collection<GrantedAuthority> authorities) {
        return authorities.parallelStream().map(a -> UserUtil.fromCrowdRoleToGroup(a.getAuthority())).collect(Collectors.toList());
    }

    public static boolean isAdmin(CrowdUserDetails user) {
        return user.getAuthorities().parallelStream().anyMatch(a-> {
            String roleName = fromCrowdRole(a.getAuthority());
            return roleName.equalsIgnoreCase("ROLE_ADMIN");
        });
    }

    public static String fromCrowdRole(String crowdRole) {
//        if (crowdRole != null && crowdRole.equalsIgnoreCase("ROLE_crowd-administrators")) {
//            return "ROLE_ADMIN";
//        }
//        return "ROLE_USER";
        return crowdRole;
    }

    public static String fromCrowdRoleToGroup(String crowdRole) {
        if (crowdRole != null && crowdRole.startsWith("ROLE_")) {
            return crowdRole.substring(5);
        }
        return crowdRole;
    }

    public static List<String> getUserLocations(CrowdService crowdService, String login) {
        SOAPPrincipal principal = crowdService.getUserWithAttributes(login);
        List<String> locations = new ArrayList<>();
        if (principal != null) {
            try {
                SOAPAttribute attribute = principal.getAttribute("location");
                locations = Arrays.asList(attribute.getValues());
            } catch (Exception e) {
                locations = new ArrayList<>();
            }
        }
        return locations;
    }

//    public static boolean userHasAccessToContainer(ContainerDTO container, String user, List<String> groups) {
//        if (container != null) {
//            if (container.getUsers() != null && container.getUsers().contains(user)) {
//                return true;
//            }
//            if (container.getGroups() != null) {
//                List<String> intersect = container.getGroups().stream().filter(groups::contains).collect(Collectors.toList());
//                if (intersect != null && !intersect.isEmpty()) {
//                    return true;
//                }
//            }
//        }
//        return false;
//    }
}

