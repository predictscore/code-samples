package biz.newparadigm.meridian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the NewsMetadata entity. This class is used in NewsMetadataResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /news-metadata?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NewsMetadataCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter valueString;

    private DoubleFilter valueNumber;

    private ZonedDateTimeFilter valueDate;

    private LongFilter newsId;

    private LongFilter metagroupId;

    private LongFilter metafieldId;

    public NewsMetadataCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getValueString() {
        return valueString;
    }

    public void setValueString(StringFilter valueString) {
        this.valueString = valueString;
    }

    public DoubleFilter getValueNumber() {
        return valueNumber;
    }

    public void setValueNumber(DoubleFilter valueNumber) {
        this.valueNumber = valueNumber;
    }

    public ZonedDateTimeFilter getValueDate() {
        return valueDate;
    }

    public void setValueDate(ZonedDateTimeFilter valueDate) {
        this.valueDate = valueDate;
    }

    public LongFilter getNewsId() {
        return newsId;
    }

    public void setNewsId(LongFilter newsId) {
        this.newsId = newsId;
    }

    public LongFilter getMetagroupId() {
        return metagroupId;
    }

    public void setMetagroupId(LongFilter metagroupId) {
        this.metagroupId = metagroupId;
    }

    public LongFilter getMetafieldId() {
        return metafieldId;
    }

    public void setMetafieldId(LongFilter metafieldId) {
        this.metafieldId = metafieldId;
    }

    @Override
    public String toString() {
        return "NewsMetadataCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (valueString != null ? "valueString=" + valueString + ", " : "") +
                (valueNumber != null ? "valueNumber=" + valueNumber + ", " : "") +
                (valueDate != null ? "valueDate=" + valueDate + ", " : "") +
                (newsId != null ? "newsId=" + newsId + ", " : "") +
                (metagroupId != null ? "metagroupId=" + metagroupId + ", " : "") +
                (metafieldId != null ? "metafieldId=" + metafieldId + ", " : "") +
            "}";
    }

}
