package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsSummaryMetadata;
import biz.newparadigm.meridian.service.NewsSummaryMetadataService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsSummaryMetadataCriteria;
import biz.newparadigm.meridian.service.NewsSummaryMetadataQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsSummaryMetadata.
 */
@RestController
@RequestMapping("/api")
public class NewsSummaryMetadataResource {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryMetadataResource.class);

    private static final String ENTITY_NAME = "newsSummaryMetadata";

    private final NewsSummaryMetadataService newsSummaryMetadataService;

    private final NewsSummaryMetadataQueryService newsSummaryMetadataQueryService;

    public NewsSummaryMetadataResource(NewsSummaryMetadataService newsSummaryMetadataService, NewsSummaryMetadataQueryService newsSummaryMetadataQueryService) {
        this.newsSummaryMetadataService = newsSummaryMetadataService;
        this.newsSummaryMetadataQueryService = newsSummaryMetadataQueryService;
    }

    /**
     * POST  /news-summary-metadata : Create a new newsSummaryMetadata.
     *
     * @param newsSummaryMetadata the newsSummaryMetadata to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsSummaryMetadata, or with status 400 (Bad Request) if the newsSummaryMetadata has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-summary-metadata")
    @Timed
    public ResponseEntity<NewsSummaryMetadata> createNewsSummaryMetadata(@Valid @RequestBody NewsSummaryMetadata newsSummaryMetadata) throws URISyntaxException {
        log.debug("REST request to save NewsSummaryMetadata : {}", newsSummaryMetadata);
        if (newsSummaryMetadata.getId() != null) {
            throw new BadRequestAlertException("A new newsSummaryMetadata cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsSummaryMetadata result = newsSummaryMetadataService.save(newsSummaryMetadata);
        return ResponseEntity.created(new URI("/api/news-summary-metadata/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-summary-metadata : Updates an existing newsSummaryMetadata.
     *
     * @param newsSummaryMetadata the newsSummaryMetadata to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsSummaryMetadata,
     * or with status 400 (Bad Request) if the newsSummaryMetadata is not valid,
     * or with status 500 (Internal Server Error) if the newsSummaryMetadata couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-summary-metadata")
    @Timed
    public ResponseEntity<NewsSummaryMetadata> updateNewsSummaryMetadata(@Valid @RequestBody NewsSummaryMetadata newsSummaryMetadata) throws URISyntaxException {
        log.debug("REST request to update NewsSummaryMetadata : {}", newsSummaryMetadata);
        if (newsSummaryMetadata.getId() == null) {
            return createNewsSummaryMetadata(newsSummaryMetadata);
        }
        NewsSummaryMetadata result = newsSummaryMetadataService.save(newsSummaryMetadata);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsSummaryMetadata.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-summary-metadata : get all the newsSummaryMetadata.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsSummaryMetadata in body
     */
    @GetMapping("/news-summary-metadata")
    @Timed
    public ResponseEntity<List<NewsSummaryMetadata>> getAllNewsSummaryMetadata(NewsSummaryMetadataCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsSummaryMetadata by criteria: {}", criteria);
        Page<NewsSummaryMetadata> page = newsSummaryMetadataQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-summary-metadata");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-summary-metadata/:id : get the "id" newsSummaryMetadata.
     *
     * @param id the id of the newsSummaryMetadata to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsSummaryMetadata, or with status 404 (Not Found)
     */
    @GetMapping("/news-summary-metadata/{id}")
    @Timed
    public ResponseEntity<NewsSummaryMetadata> getNewsSummaryMetadata(@PathVariable Long id) {
        log.debug("REST request to get NewsSummaryMetadata : {}", id);
        NewsSummaryMetadata newsSummaryMetadata = newsSummaryMetadataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsSummaryMetadata));
    }

    /**
     * DELETE  /news-summary-metadata/:id : delete the "id" newsSummaryMetadata.
     *
     * @param id the id of the newsSummaryMetadata to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-summary-metadata/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsSummaryMetadata(@PathVariable Long id) {
        log.debug("REST request to delete NewsSummaryMetadata : {}", id);
        newsSummaryMetadataService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-summary-metadata?query=:query : search for the newsSummaryMetadata corresponding
     * to the query.
     *
     * @param query the query of the newsSummaryMetadata search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-summary-metadata")
    @Timed
    public ResponseEntity<List<NewsSummaryMetadata>> searchNewsSummaryMetadata(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsSummaryMetadata for query {}", query);
        Page<NewsSummaryMetadata> page = newsSummaryMetadataService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-summary-metadata");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
