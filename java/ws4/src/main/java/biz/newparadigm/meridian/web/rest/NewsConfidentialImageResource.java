package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsConfidentialImage;
import biz.newparadigm.meridian.service.NewsConfidentialImageService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsConfidentialImageCriteria;
import biz.newparadigm.meridian.service.NewsConfidentialImageQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsConfidentialImage.
 */
@RestController
@RequestMapping("/api")
public class NewsConfidentialImageResource {

    private final Logger log = LoggerFactory.getLogger(NewsConfidentialImageResource.class);

    private static final String ENTITY_NAME = "newsConfidentialImage";

    private final NewsConfidentialImageService newsConfidentialImageService;

    private final NewsConfidentialImageQueryService newsConfidentialImageQueryService;

    public NewsConfidentialImageResource(NewsConfidentialImageService newsConfidentialImageService, NewsConfidentialImageQueryService newsConfidentialImageQueryService) {
        this.newsConfidentialImageService = newsConfidentialImageService;
        this.newsConfidentialImageQueryService = newsConfidentialImageQueryService;
    }

    /**
     * POST  /news-confidential-images : Create a new newsConfidentialImage.
     *
     * @param newsConfidentialImage the newsConfidentialImage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsConfidentialImage, or with status 400 (Bad Request) if the newsConfidentialImage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-confidential-images")
    @Timed
    public ResponseEntity<NewsConfidentialImage> createNewsConfidentialImage(@Valid @RequestBody NewsConfidentialImage newsConfidentialImage) throws URISyntaxException {
        log.debug("REST request to save NewsConfidentialImage : {}", newsConfidentialImage);
        if (newsConfidentialImage.getId() != null) {
            throw new BadRequestAlertException("A new newsConfidentialImage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsConfidentialImage result = newsConfidentialImageService.save(newsConfidentialImage);
        return ResponseEntity.created(new URI("/api/news-confidential-images/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-confidential-images : Updates an existing newsConfidentialImage.
     *
     * @param newsConfidentialImage the newsConfidentialImage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsConfidentialImage,
     * or with status 400 (Bad Request) if the newsConfidentialImage is not valid,
     * or with status 500 (Internal Server Error) if the newsConfidentialImage couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-confidential-images")
    @Timed
    public ResponseEntity<NewsConfidentialImage> updateNewsConfidentialImage(@Valid @RequestBody NewsConfidentialImage newsConfidentialImage) throws URISyntaxException {
        log.debug("REST request to update NewsConfidentialImage : {}", newsConfidentialImage);
        if (newsConfidentialImage.getId() == null) {
            return createNewsConfidentialImage(newsConfidentialImage);
        }
        NewsConfidentialImage result = newsConfidentialImageService.save(newsConfidentialImage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsConfidentialImage.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-confidential-images : get all the newsConfidentialImages.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsConfidentialImages in body
     */
    @GetMapping("/news-confidential-images")
    @Timed
    public ResponseEntity<List<NewsConfidentialImage>> getAllNewsConfidentialImages(NewsConfidentialImageCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsConfidentialImages by criteria: {}", criteria);
        Page<NewsConfidentialImage> page = newsConfidentialImageQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-confidential-images");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-confidential-images/:id : get the "id" newsConfidentialImage.
     *
     * @param id the id of the newsConfidentialImage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsConfidentialImage, or with status 404 (Not Found)
     */
    @GetMapping("/news-confidential-images/{id}")
    @Timed
    public ResponseEntity<NewsConfidentialImage> getNewsConfidentialImage(@PathVariable Long id) {
        log.debug("REST request to get NewsConfidentialImage : {}", id);
        NewsConfidentialImage newsConfidentialImage = newsConfidentialImageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsConfidentialImage));
    }

    /**
     * DELETE  /news-confidential-images/:id : delete the "id" newsConfidentialImage.
     *
     * @param id the id of the newsConfidentialImage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-confidential-images/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsConfidentialImage(@PathVariable Long id) {
        log.debug("REST request to delete NewsConfidentialImage : {}", id);
        newsConfidentialImageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-confidential-images?query=:query : search for the newsConfidentialImage corresponding
     * to the query.
     *
     * @param query the query of the newsConfidentialImage search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-confidential-images")
    @Timed
    public ResponseEntity<List<NewsConfidentialImage>> searchNewsConfidentialImages(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsConfidentialImages for query {}", query);
        Page<NewsConfidentialImage> page = newsConfidentialImageService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-confidential-images");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
