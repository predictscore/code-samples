package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsToRelatedAuthor;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsToRelatedAuthor entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsToRelatedAuthorRepository extends JpaRepository<NewsToRelatedAuthor, Long> {

}
