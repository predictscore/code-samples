package biz.newparadigm.meridian.domain;

import biz.newparadigm.meridian.domain.util.MeridianDateSerializer;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A NewsComment.
 */
@Entity
@Table(name = "news_comments")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "newscomment")
public class NewsComment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="news_comments_id_seq")
    @SequenceGenerator(name="news_comments_id_seq", sequenceName="news_comments_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "user_id", nullable = false)
    private String userId;

    @NotNull
    @Column(name = "comment", nullable = false)
    private String comment;

    @NotNull
    @Column(name = "datetime", nullable = false)
    @JsonSerialize(using = MeridianDateSerializer.class)
    private ZonedDateTime datetime;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"comments", "attachments", "originalImages", "publicImages", "confidentialImages", "urls",
        "downloadableContentUrls", "metadata", "tags", "languages", "newsSummaries", "metagroups", "metagroupsMap", "author", "siteChatgroup"})
    private News news;

    @ManyToOne
    private NewsComment parentComment;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public NewsComment userId(String userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public NewsComment comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ZonedDateTime getDatetime() {
        return datetime;
    }

    public NewsComment datetime(ZonedDateTime datetime) {
        this.datetime = datetime;
        return this;
    }

    public void setDatetime(ZonedDateTime datetime) {
        this.datetime = datetime;
    }

    public News getNews() {
        return news;
    }

    public NewsComment news(News news) {
        this.news = news;
        return this;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public NewsComment getParentComment() {
        return parentComment;
    }

    public NewsComment parentComment(NewsComment newsComment) {
        this.parentComment = newsComment;
        return this;
    }

    public void setParentComment(NewsComment newsComment) {
        this.parentComment = newsComment;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NewsComment newsComment = (NewsComment) o;
        if (newsComment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), newsComment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NewsComment{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", comment='" + getComment() + "'" +
            ", datetime='" + getDatetime() + "'" +
            "}";
    }
}
