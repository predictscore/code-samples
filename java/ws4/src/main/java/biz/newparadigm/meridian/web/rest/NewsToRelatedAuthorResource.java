package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsToRelatedAuthor;
import biz.newparadigm.meridian.service.NewsToRelatedAuthorService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsToRelatedAuthor.
 */
@RestController
@RequestMapping("/api")
public class NewsToRelatedAuthorResource {

    private final Logger log = LoggerFactory.getLogger(NewsToRelatedAuthorResource.class);

    private static final String ENTITY_NAME = "newsToRelatedAuthor";

    private final NewsToRelatedAuthorService newsToRelatedAuthorService;

    public NewsToRelatedAuthorResource(NewsToRelatedAuthorService newsToRelatedAuthorService) {
        this.newsToRelatedAuthorService = newsToRelatedAuthorService;
    }

    /**
     * POST  /news-to-related-authors : Create a new newsToRelatedAuthor.
     *
     * @param newsToRelatedAuthor the newsToRelatedAuthor to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsToRelatedAuthor, or with status 400 (Bad Request) if the newsToRelatedAuthor has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-to-related-authors")
    @Timed
    public ResponseEntity<NewsToRelatedAuthor> createNewsToRelatedAuthor(@Valid @RequestBody NewsToRelatedAuthor newsToRelatedAuthor) throws URISyntaxException {
        log.debug("REST request to save NewsToRelatedAuthor : {}", newsToRelatedAuthor);
        if (newsToRelatedAuthor.getId() != null) {
            throw new BadRequestAlertException("A new newsToRelatedAuthor cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsToRelatedAuthor result = newsToRelatedAuthorService.save(newsToRelatedAuthor);
        return ResponseEntity.created(new URI("/api/news-to-related-authors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-to-related-authors : Updates an existing newsToRelatedAuthor.
     *
     * @param newsToRelatedAuthor the newsToRelatedAuthor to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsToRelatedAuthor,
     * or with status 400 (Bad Request) if the newsToRelatedAuthor is not valid,
     * or with status 500 (Internal Server Error) if the newsToRelatedAuthor couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-to-related-authors")
    @Timed
    public ResponseEntity<NewsToRelatedAuthor> updateNewsToRelatedAuthor(@Valid @RequestBody NewsToRelatedAuthor newsToRelatedAuthor) throws URISyntaxException {
        log.debug("REST request to update NewsToRelatedAuthor : {}", newsToRelatedAuthor);
        if (newsToRelatedAuthor.getId() == null) {
            return createNewsToRelatedAuthor(newsToRelatedAuthor);
        }
        NewsToRelatedAuthor result = newsToRelatedAuthorService.save(newsToRelatedAuthor);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsToRelatedAuthor.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-to-related-authors : get all the newsToRelatedAuthors.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of newsToRelatedAuthors in body
     */
    @GetMapping("/news-to-related-authors")
    @Timed
    public List<NewsToRelatedAuthor> getAllNewsToRelatedAuthors() {
        log.debug("REST request to get all NewsToRelatedAuthors");
        return newsToRelatedAuthorService.findAll();
        }

    /**
     * GET  /news-to-related-authors/:id : get the "id" newsToRelatedAuthor.
     *
     * @param id the id of the newsToRelatedAuthor to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsToRelatedAuthor, or with status 404 (Not Found)
     */
    @GetMapping("/news-to-related-authors/{id}")
    @Timed
    public ResponseEntity<NewsToRelatedAuthor> getNewsToRelatedAuthor(@PathVariable Long id) {
        log.debug("REST request to get NewsToRelatedAuthor : {}", id);
        NewsToRelatedAuthor newsToRelatedAuthor = newsToRelatedAuthorService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsToRelatedAuthor));
    }

    /**
     * DELETE  /news-to-related-authors/:id : delete the "id" newsToRelatedAuthor.
     *
     * @param id the id of the newsToRelatedAuthor to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-to-related-authors/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsToRelatedAuthor(@PathVariable Long id) {
        log.debug("REST request to delete NewsToRelatedAuthor : {}", id);
        newsToRelatedAuthorService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-to-related-authors?query=:query : search for the newsToRelatedAuthor corresponding
     * to the query.
     *
     * @param query the query of the newsToRelatedAuthor search
     * @return the result of the search
     */
    @GetMapping("/_search/news-to-related-authors")
    @Timed
    public List<NewsToRelatedAuthor> searchNewsToRelatedAuthors(@RequestParam String query) {
        log.debug("REST request to search NewsToRelatedAuthors for query {}", query);
        return newsToRelatedAuthorService.search(query);
    }

}
