package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.SiteChatgroupDomain;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SiteChatgroupDomain entity.
 */
public interface SiteChatgroupDomainSearchRepository extends ElasticsearchRepository<SiteChatgroupDomain, Long> {
}
