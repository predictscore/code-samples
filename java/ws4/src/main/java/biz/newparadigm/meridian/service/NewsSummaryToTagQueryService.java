package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsSummaryToTag;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsSummaryToTagRepository;
import biz.newparadigm.meridian.repository.search.NewsSummaryToTagSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsSummaryToTagCriteria;


/**
 * Service for executing complex queries for NewsSummaryToTag entities in the database.
 * The main input is a {@link NewsSummaryToTagCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsSummaryToTag} or a {@link Page} of {%link NewsSummaryToTag} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsSummaryToTagQueryService extends QueryService<NewsSummaryToTag> {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryToTagQueryService.class);


    private final NewsSummaryToTagRepository newsSummaryToTagRepository;

    private final NewsSummaryToTagSearchRepository newsSummaryToTagSearchRepository;

    public NewsSummaryToTagQueryService(NewsSummaryToTagRepository newsSummaryToTagRepository, NewsSummaryToTagSearchRepository newsSummaryToTagSearchRepository) {
        this.newsSummaryToTagRepository = newsSummaryToTagRepository;
        this.newsSummaryToTagSearchRepository = newsSummaryToTagSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsSummaryToTag} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsSummaryToTag> findByCriteria(NewsSummaryToTagCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsSummaryToTag> specification = createSpecification(criteria);
        return newsSummaryToTagRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsSummaryToTag} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryToTag> findByCriteria(NewsSummaryToTagCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsSummaryToTag> specification = createSpecification(criteria);
        return newsSummaryToTagRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsSummaryToTagCriteria to a {@link Specifications}
     */
    private Specifications<NewsSummaryToTag> createSpecification(NewsSummaryToTagCriteria criteria) {
        Specifications<NewsSummaryToTag> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsSummaryToTag_.id));
            }
            if (criteria.getNewsSummaryId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsSummaryId(), NewsSummaryToTag_.newsSummary, NewsSummary_.id));
            }
            if (criteria.getTagId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getTagId(), NewsSummaryToTag_.tag, Tag_.id));
            }
        }
        return specification;
    }

}
