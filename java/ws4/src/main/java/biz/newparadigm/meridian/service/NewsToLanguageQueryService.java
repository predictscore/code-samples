package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsToLanguage;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsToLanguageRepository;
import biz.newparadigm.meridian.repository.search.NewsToLanguageSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsToLanguageCriteria;


/**
 * Service for executing complex queries for NewsToLanguage entities in the database.
 * The main input is a {@link NewsToLanguageCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsToLanguage} or a {@link Page} of {%link NewsToLanguage} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsToLanguageQueryService extends QueryService<NewsToLanguage> {

    private final Logger log = LoggerFactory.getLogger(NewsToLanguageQueryService.class);


    private final NewsToLanguageRepository newsToLanguageRepository;

    private final NewsToLanguageSearchRepository newsToLanguageSearchRepository;

    public NewsToLanguageQueryService(NewsToLanguageRepository newsToLanguageRepository, NewsToLanguageSearchRepository newsToLanguageSearchRepository) {
        this.newsToLanguageRepository = newsToLanguageRepository;
        this.newsToLanguageSearchRepository = newsToLanguageSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsToLanguage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsToLanguage> findByCriteria(NewsToLanguageCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsToLanguage> specification = createSpecification(criteria);
        return newsToLanguageRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsToLanguage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsToLanguage> findByCriteria(NewsToLanguageCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsToLanguage> specification = createSpecification(criteria);
        return newsToLanguageRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsToLanguageCriteria to a {@link Specifications}
     */
    private Specifications<NewsToLanguage> createSpecification(NewsToLanguageCriteria criteria) {
        Specifications<NewsToLanguage> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsToLanguage_.id));
            }
            if (criteria.getNewsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsId(), NewsToLanguage_.news, News_.id));
            }
            if (criteria.getLanguageId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getLanguageId(), NewsToLanguage_.language, Language_.id));
            }
        }
        return specification;
    }

}
