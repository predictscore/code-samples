package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.AuthorName;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AuthorName entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuthorNameRepository extends JpaRepository<AuthorName, Long>, JpaSpecificationExecutor<AuthorName> {

}
