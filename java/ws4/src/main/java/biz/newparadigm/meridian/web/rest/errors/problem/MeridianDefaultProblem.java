package biz.newparadigm.meridian.web.rest.errors.problem;

import org.zalando.problem.StatusType;

import javax.annotation.Nullable;
import java.net.URI;
import java.util.Map;

public final class MeridianDefaultProblem extends MeridianAbstractThrowableProblem {
    // TODO needed for jackson
    MeridianDefaultProblem(@Nullable final String type,
                   @Nullable final String title,
                   @Nullable final StatusType status,
                   @Nullable final String detail,
                   @Nullable final URI instance,
                   @Nullable final MeridianThrowableProblem cause) {
        super(type, title, status, detail, instance, cause);
    }

    MeridianDefaultProblem(@Nullable final String type,
                   @Nullable final String title,
                   @Nullable final StatusType status,
                   @Nullable final String detail,
                   @Nullable final URI instance,
                   @Nullable final MeridianThrowableProblem cause,
                   @Nullable final Map<String, Object> parameters) {
        super(type, title, status, detail, instance, cause, parameters);
    }
}
