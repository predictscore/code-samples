package biz.newparadigm.meridian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the AuthorToContact entity. This class is used in AuthorToContactResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /author-to-contacts?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AuthorToContactCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter contactValue;

    private LongFilter authorId;

    private LongFilter contactTypeId;

    public AuthorToContactCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getContactValue() {
        return contactValue;
    }

    public void setContactValue(StringFilter contactValue) {
        this.contactValue = contactValue;
    }

    public LongFilter getAuthorId() {
        return authorId;
    }

    public void setAuthorId(LongFilter authorId) {
        this.authorId = authorId;
    }

    public LongFilter getContactTypeId() {
        return contactTypeId;
    }

    public void setContactTypeId(LongFilter contactTypeId) {
        this.contactTypeId = contactTypeId;
    }

    @Override
    public String toString() {
        return "AuthorToContactCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (contactValue != null ? "contactValue=" + contactValue + ", " : "") +
                (authorId != null ? "authorId=" + authorId + ", " : "") +
                (contactTypeId != null ? "contactTypeId=" + contactTypeId + ", " : "") +
            "}";
    }

}
