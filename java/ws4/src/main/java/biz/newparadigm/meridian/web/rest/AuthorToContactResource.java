package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.AuthorToContact;
import biz.newparadigm.meridian.service.AuthorToContactService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.AuthorToContactCriteria;
import biz.newparadigm.meridian.service.AuthorToContactQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AuthorToContact.
 */
@RestController
@RequestMapping("/api")
public class AuthorToContactResource {

    private final Logger log = LoggerFactory.getLogger(AuthorToContactResource.class);

    private static final String ENTITY_NAME = "authorToContact";

    private final AuthorToContactService authorToContactService;

    private final AuthorToContactQueryService authorToContactQueryService;

    public AuthorToContactResource(AuthorToContactService authorToContactService, AuthorToContactQueryService authorToContactQueryService) {
        this.authorToContactService = authorToContactService;
        this.authorToContactQueryService = authorToContactQueryService;
    }

    /**
     * POST  /author-to-contacts : Create a new authorToContact.
     *
     * @param authorToContact the authorToContact to create
     * @return the ResponseEntity with status 201 (Created) and with body the new authorToContact, or with status 400 (Bad Request) if the authorToContact has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/author-to-contacts")
    @Timed
    public ResponseEntity<AuthorToContact> createAuthorToContact(@Valid @RequestBody AuthorToContact authorToContact) throws URISyntaxException {
        log.debug("REST request to save AuthorToContact : {}", authorToContact);
        if (authorToContact.getId() != null) {
            throw new BadRequestAlertException("A new authorToContact cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuthorToContact result = authorToContactService.save(authorToContact);
        return ResponseEntity.created(new URI("/api/author-to-contacts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /author-to-contacts : Updates an existing authorToContact.
     *
     * @param authorToContact the authorToContact to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated authorToContact,
     * or with status 400 (Bad Request) if the authorToContact is not valid,
     * or with status 500 (Internal Server Error) if the authorToContact couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/author-to-contacts")
    @Timed
    public ResponseEntity<AuthorToContact> updateAuthorToContact(@Valid @RequestBody AuthorToContact authorToContact) throws URISyntaxException {
        log.debug("REST request to update AuthorToContact : {}", authorToContact);
        if (authorToContact.getId() == null) {
            return createAuthorToContact(authorToContact);
        }
        AuthorToContact result = authorToContactService.save(authorToContact);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, authorToContact.getId().toString()))
            .body(result);
    }

    /**
     * GET  /author-to-contacts : get all the authorToContacts.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of authorToContacts in body
     */
    @GetMapping("/author-to-contacts")
    @Timed
    public ResponseEntity<List<AuthorToContact>> getAllAuthorToContacts(AuthorToContactCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get AuthorToContacts by criteria: {}", criteria);
        Page<AuthorToContact> page = authorToContactQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/author-to-contacts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /author-to-contacts/:id : get the "id" authorToContact.
     *
     * @param id the id of the authorToContact to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the authorToContact, or with status 404 (Not Found)
     */
    @GetMapping("/author-to-contacts/{id}")
    @Timed
    public ResponseEntity<AuthorToContact> getAuthorToContact(@PathVariable Long id) {
        log.debug("REST request to get AuthorToContact : {}", id);
        AuthorToContact authorToContact = authorToContactService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(authorToContact));
    }

    /**
     * DELETE  /author-to-contacts/:id : delete the "id" authorToContact.
     *
     * @param id the id of the authorToContact to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/author-to-contacts/{id}")
    @Timed
    public ResponseEntity<Void> deleteAuthorToContact(@PathVariable Long id) {
        log.debug("REST request to delete AuthorToContact : {}", id);
        authorToContactService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/author-to-contacts?query=:query : search for the authorToContact corresponding
     * to the query.
     *
     * @param query the query of the authorToContact search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/author-to-contacts")
    @Timed
    public ResponseEntity<List<AuthorToContact>> searchAuthorToContacts(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of AuthorToContacts for query {}", query);
        Page<AuthorToContact> page = authorToContactService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/author-to-contacts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
