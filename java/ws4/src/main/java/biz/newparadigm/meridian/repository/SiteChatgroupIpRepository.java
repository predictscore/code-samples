package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.SiteChatgroupIp;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SiteChatgroupIp entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SiteChatgroupIpRepository extends JpaRepository<SiteChatgroupIp, Long>, JpaSpecificationExecutor<SiteChatgroupIp> {

}
