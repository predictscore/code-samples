package biz.newparadigm.meridian.domain;

import biz.newparadigm.meridian.domain.util.MeridianDateSerializer;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

@Entity
@Table(name = "site_chatgroup_domains")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "sitechatgroupdomain")
public class SiteChatgroupDomain implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="site_chatgroup_domains_id_seq")
    @SequenceGenerator(name="site_chatgroup_domains_id_seq", sequenceName="site_chatgroup_domains_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "domain_id", nullable = false)
    private String domainId;

    @Column(name = "domain_info")
    private String domainInfo;

    @NotNull
    @Column(name = "first_used_date", nullable = false)
    @JsonSerialize(using = MeridianDateSerializer.class)
    private ZonedDateTime firstUsedDate;

    @Column(name = "last_used_date")
    @JsonSerialize(using = MeridianDateSerializer.class)
    private ZonedDateTime lastUsedDate;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"comments", "domains", "ips", "names", "infos", "numVisitors", "numUsers", "languages", "tags", "authors"})
    private SiteChatgroup siteChatgroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDomainId() {
        return domainId;
    }

    public SiteChatgroupDomain domainId(String domainId) {
        this.domainId = domainId;
        return this;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getDomainInfo() {
        return domainInfo;
    }

    public SiteChatgroupDomain domainInfo(String domainInfo) {
        this.domainInfo = domainInfo;
        return this;
    }

    public void setDomainInfo(String domainInfo) {
        this.domainInfo = domainInfo;
    }

    public ZonedDateTime getFirstUsedDate() {
        return firstUsedDate;
    }

    public SiteChatgroupDomain firstUsedDate(ZonedDateTime firstUsedDate) {
        this.firstUsedDate = firstUsedDate;
        return this;
    }

    public void setFirstUsedDate(ZonedDateTime firstUsedDate) {
        this.firstUsedDate = firstUsedDate;
    }

    public ZonedDateTime getLastUsedDate() {
        return lastUsedDate;
    }

    public SiteChatgroupDomain lastUsedDate(ZonedDateTime lastUsedDate) {
        this.lastUsedDate = lastUsedDate;
        return this;
    }

    public void setLastUsedDate(ZonedDateTime lastUsedDate) {
        this.lastUsedDate = lastUsedDate;
    }

    public SiteChatgroup getSiteChatgroup() {
        return siteChatgroup;
    }

    public SiteChatgroupDomain siteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
        return this;
    }

    public void setSiteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SiteChatgroupDomain siteChatgroupDomain = (SiteChatgroupDomain) o;
        if (siteChatgroupDomain.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), siteChatgroupDomain.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SiteChatgroupDomain{" +
            "id=" + getId() +
            ", domainId='" + getDomainId() + "'" +
            ", domainInfo='" + getDomainInfo() + "'" +
            ", firstUsedDate='" + getFirstUsedDate() + "'" +
            ", lastUsedDate='" + getLastUsedDate() + "'" +
            "}";
    }
}
