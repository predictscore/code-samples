package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.SiteChatgroup;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;

@Repository
public interface SiteChatgroupRepository extends JpaRepository<SiteChatgroup, Long>, JpaSpecificationExecutor<SiteChatgroup> {
    @Query("SELECT s FROM SiteChatgroup s JOIN s.domains d WHERE d.domainId = :domainId ")
    public List<SiteChatgroup> findEqDomainId(@Param(value = "domainId") String domainId);

    @Query("SELECT s FROM SiteChatgroup s JOIN s.domains d WHERE d.domainId = :domainId AND s.location.name = :locationName ")
    public List<SiteChatgroup> findEqDomainIdLocation(@Param(value = "domainId") String domainId, @Param(value = "locationName") String locationName);

    @Query("SELECT s FROM SiteChatgroup s JOIN s.domains d WHERE d.domainId LIKE %:domainId% ")
    public List<SiteChatgroup> findLikeDomainId(@Param(value = "domainId") String domainId);

    @Query("SELECT s FROM SiteChatgroup s JOIN s.domains d WHERE d.domainId LIKE %:domainId% AND s.location.name = :locationName ")
    public List<SiteChatgroup> findLikeDomainIdLocation(@Param(value = "domainId") String domainId, @Param(value = "locationName") String locationName);
}
