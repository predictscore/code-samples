package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsOriginalImage;
import biz.newparadigm.meridian.service.NewsOriginalImageService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsOriginalImageCriteria;
import biz.newparadigm.meridian.service.NewsOriginalImageQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsOriginalImage.
 */
@RestController
@RequestMapping("/api")
public class NewsOriginalImageResource {

    private final Logger log = LoggerFactory.getLogger(NewsOriginalImageResource.class);

    private static final String ENTITY_NAME = "newsOriginalImage";

    private final NewsOriginalImageService newsOriginalImageService;

    private final NewsOriginalImageQueryService newsOriginalImageQueryService;

    public NewsOriginalImageResource(NewsOriginalImageService newsOriginalImageService, NewsOriginalImageQueryService newsOriginalImageQueryService) {
        this.newsOriginalImageService = newsOriginalImageService;
        this.newsOriginalImageQueryService = newsOriginalImageQueryService;
    }

    /**
     * POST  /news-original-images : Create a new newsOriginalImage.
     *
     * @param newsOriginalImage the newsOriginalImage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsOriginalImage, or with status 400 (Bad Request) if the newsOriginalImage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-original-images")
    @Timed
    public ResponseEntity<NewsOriginalImage> createNewsOriginalImage(@Valid @RequestBody NewsOriginalImage newsOriginalImage) throws URISyntaxException {
        log.debug("REST request to save NewsOriginalImage : {}", newsOriginalImage);
        if (newsOriginalImage.getId() != null) {
            throw new BadRequestAlertException("A new newsOriginalImage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsOriginalImage result = newsOriginalImageService.save(newsOriginalImage);
        return ResponseEntity.created(new URI("/api/news-original-images/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-original-images : Updates an existing newsOriginalImage.
     *
     * @param newsOriginalImage the newsOriginalImage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsOriginalImage,
     * or with status 400 (Bad Request) if the newsOriginalImage is not valid,
     * or with status 500 (Internal Server Error) if the newsOriginalImage couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-original-images")
    @Timed
    public ResponseEntity<NewsOriginalImage> updateNewsOriginalImage(@Valid @RequestBody NewsOriginalImage newsOriginalImage) throws URISyntaxException {
        log.debug("REST request to update NewsOriginalImage : {}", newsOriginalImage);
        if (newsOriginalImage.getId() == null) {
            return createNewsOriginalImage(newsOriginalImage);
        }
        NewsOriginalImage result = newsOriginalImageService.save(newsOriginalImage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsOriginalImage.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-original-images : get all the newsOriginalImages.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsOriginalImages in body
     */
    @GetMapping("/news-original-images")
    @Timed
    public ResponseEntity<List<NewsOriginalImage>> getAllNewsOriginalImages(NewsOriginalImageCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsOriginalImages by criteria: {}", criteria);
        Page<NewsOriginalImage> page = newsOriginalImageQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-original-images");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-original-images/:id : get the "id" newsOriginalImage.
     *
     * @param id the id of the newsOriginalImage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsOriginalImage, or with status 404 (Not Found)
     */
    @GetMapping("/news-original-images/{id}")
    @Timed
    public ResponseEntity<NewsOriginalImage> getNewsOriginalImage(@PathVariable Long id) {
        log.debug("REST request to get NewsOriginalImage : {}", id);
        NewsOriginalImage newsOriginalImage = newsOriginalImageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsOriginalImage));
    }

    /**
     * DELETE  /news-original-images/:id : delete the "id" newsOriginalImage.
     *
     * @param id the id of the newsOriginalImage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-original-images/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsOriginalImage(@PathVariable Long id) {
        log.debug("REST request to delete NewsOriginalImage : {}", id);
        newsOriginalImageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-original-images?query=:query : search for the newsOriginalImage corresponding
     * to the query.
     *
     * @param query the query of the newsOriginalImage search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-original-images")
    @Timed
    public ResponseEntity<List<NewsOriginalImage>> searchNewsOriginalImages(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsOriginalImages for query {}", query);
        Page<NewsOriginalImage> page = newsOriginalImageService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-original-images");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
