package biz.newparadigm.meridian.domain;

import biz.newparadigm.meridian.domain.util.MeridianDateSerializer;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

@Entity
@Table(name = "authors_to_site_chatgroups")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "authortositechatgroup")
public class AuthorToSiteChatgroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="authors_to_site_chatgroups_id_seq")
    @SequenceGenerator(name="authors_to_site_chatgroups_id_seq", sequenceName="authors_to_site_chatgroups_id_seq", allocationSize=1)
    private Long id;

    @Column(name = "from_date")
    @JsonSerialize(using = MeridianDateSerializer.class)
    private ZonedDateTime fromDate;

    @Column(name = "original_name")
    private String originalName;

    @Column(name = "english_name")
    private String englishName;

    @Column(name = "profile_link")
    private String profileLink;

    @Column(name = "to_date")
    @JsonSerialize(using = MeridianDateSerializer.class)
    private ZonedDateTime toDate;

    @Column(name = "staff_function")
    private String staffFunction;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"profiles", "names", "comments", "contacts", "siteChatGroups", "tags", "languages"})
    private Author author;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"comments", "domains", "ips", "names", "infos", "numVisitors", "numUsers", "languages", "tags", "authors"})
    private SiteChatgroup siteChatgroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getFromDate() {
        return fromDate;
    }

    public AuthorToSiteChatgroup fromDate(ZonedDateTime fromDate) {
        this.fromDate = fromDate;
        return this;
    }

    public void setFromDate(ZonedDateTime fromDate) {
        this.fromDate = fromDate;
    }

    public String getOriginalName() {
        return originalName;
    }

    public AuthorToSiteChatgroup originalName(String originalName) {
        this.originalName = originalName;
        return this;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public AuthorToSiteChatgroup englishName(String englishName) {
        this.englishName = englishName;
        return this;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getProfileLink() {
        return profileLink;
    }

    public AuthorToSiteChatgroup profileLink(String profileLink) {
        this.profileLink = profileLink;
        return this;
    }

    public void setProfileLink(String profileLink) {
        this.profileLink = profileLink;
    }

    public ZonedDateTime getToDate() {
        return toDate;
    }

    public AuthorToSiteChatgroup toDate(ZonedDateTime toDate) {
        this.toDate = toDate;
        return this;
    }

    public void setToDate(ZonedDateTime toDate) {
        this.toDate = toDate;
    }

    public String getStaffFunction() {
        return staffFunction;
    }

    public AuthorToSiteChatgroup staffFunction(String staffFunction) {
        this.staffFunction = staffFunction;
        return this;
    }

    public void setStaffFunction(String staffFunction) {
        this.staffFunction = staffFunction;
    }

    public Author getAuthor() {
        return author;
    }

    public AuthorToSiteChatgroup author(Author author) {
        this.author = author;
        return this;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public SiteChatgroup getSiteChatgroup() {
        return siteChatgroup;
    }

    public AuthorToSiteChatgroup siteChatGroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
        return this;
    }

    public void setSiteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AuthorToSiteChatgroup authorToSiteChatgroup = (AuthorToSiteChatgroup) o;
        if (authorToSiteChatgroup.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), authorToSiteChatgroup.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AuthorToSiteChatgroup{" +
            "id=" + getId() +
            ", fromDate='" + getFromDate() + "'" +
            ", originalName='" + getOriginalName() + "'" +
            ", englishName='" + getEnglishName() + "'" +
            ", profileLink='" + getProfileLink() + "'" +
            ", toDate='" + getToDate() + "'" +
            ", staffFunction='" + getStaffFunction() + "'" +
            "}";
    }
}
