package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.AuthorName;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.AuthorNameRepository;
import biz.newparadigm.meridian.repository.search.AuthorNameSearchRepository;
import biz.newparadigm.meridian.service.dto.AuthorNameCriteria;


/**
 * Service for executing complex queries for AuthorName entities in the database.
 * The main input is a {@link AuthorNameCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link AuthorName} or a {@link Page} of {%link AuthorName} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class AuthorNameQueryService extends QueryService<AuthorName> {

    private final Logger log = LoggerFactory.getLogger(AuthorNameQueryService.class);


    private final AuthorNameRepository authorNameRepository;

    private final AuthorNameSearchRepository authorNameSearchRepository;

    public AuthorNameQueryService(AuthorNameRepository authorNameRepository, AuthorNameSearchRepository authorNameSearchRepository) {
        this.authorNameRepository = authorNameRepository;
        this.authorNameSearchRepository = authorNameSearchRepository;
    }

    /**
     * Return a {@link List} of {%link AuthorName} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuthorName> findByCriteria(AuthorNameCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<AuthorName> specification = createSpecification(criteria);
        return authorNameRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link AuthorName} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuthorName> findByCriteria(AuthorNameCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<AuthorName> specification = createSpecification(criteria);
        return authorNameRepository.findAll(specification, page);
    }

    /**
     * Function to convert AuthorNameCriteria to a {@link Specifications}
     */
    private Specifications<AuthorName> createSpecification(AuthorNameCriteria criteria) {
        Specifications<AuthorName> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AuthorName_.id));
            }
            if (criteria.getOriginalName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOriginalName(), AuthorName_.originalName));
            }
            if (criteria.getEnglishName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEnglishName(), AuthorName_.englishName));
            }
            if (criteria.getAuthorId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getAuthorId(), AuthorName_.author, Author_.id));
            }
        }
        return specification;
    }

}
