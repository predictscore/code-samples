package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsToLanguage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsToLanguage entity.
 */
public interface NewsToLanguageSearchRepository extends ElasticsearchRepository<NewsToLanguage, Long> {
}
