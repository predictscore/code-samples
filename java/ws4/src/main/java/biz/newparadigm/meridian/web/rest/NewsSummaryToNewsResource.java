package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsSummaryToNews;
import biz.newparadigm.meridian.service.NewsSummaryToNewsService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsSummaryToNewsCriteria;
import biz.newparadigm.meridian.service.NewsSummaryToNewsQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsSummaryToNews.
 */
@RestController
@RequestMapping("/api")
public class NewsSummaryToNewsResource {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryToNewsResource.class);

    private static final String ENTITY_NAME = "newsSummaryToNews";

    private final NewsSummaryToNewsService newsSummaryToNewsService;

    private final NewsSummaryToNewsQueryService newsSummaryToNewsQueryService;

    public NewsSummaryToNewsResource(NewsSummaryToNewsService newsSummaryToNewsService, NewsSummaryToNewsQueryService newsSummaryToNewsQueryService) {
        this.newsSummaryToNewsService = newsSummaryToNewsService;
        this.newsSummaryToNewsQueryService = newsSummaryToNewsQueryService;
    }

    /**
     * POST  /news-summary-to-news : Create a new newsSummaryToNews.
     *
     * @param newsSummaryToNews the newsSummaryToNews to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsSummaryToNews, or with status 400 (Bad Request) if the newsSummaryToNews has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-summary-to-news")
    @Timed
    public ResponseEntity<NewsSummaryToNews> createNewsSummaryToNews(@Valid @RequestBody NewsSummaryToNews newsSummaryToNews) throws URISyntaxException {
        log.debug("REST request to save NewsSummaryToNews : {}", newsSummaryToNews);
        if (newsSummaryToNews.getId() != null) {
            throw new BadRequestAlertException("A new newsSummaryToNews cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsSummaryToNews result = newsSummaryToNewsService.save(newsSummaryToNews);
        return ResponseEntity.created(new URI("/api/news-summary-to-news/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-summary-to-news : Updates an existing newsSummaryToNews.
     *
     * @param newsSummaryToNews the newsSummaryToNews to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsSummaryToNews,
     * or with status 400 (Bad Request) if the newsSummaryToNews is not valid,
     * or with status 500 (Internal Server Error) if the newsSummaryToNews couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-summary-to-news")
    @Timed
    public ResponseEntity<NewsSummaryToNews> updateNewsSummaryToNews(@Valid @RequestBody NewsSummaryToNews newsSummaryToNews) throws URISyntaxException {
        log.debug("REST request to update NewsSummaryToNews : {}", newsSummaryToNews);
        if (newsSummaryToNews.getId() == null) {
            return createNewsSummaryToNews(newsSummaryToNews);
        }
        NewsSummaryToNews result = newsSummaryToNewsService.save(newsSummaryToNews);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsSummaryToNews.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-summary-to-news : get all the newsSummaryToNews.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsSummaryToNews in body
     */
    @GetMapping("/news-summary-to-news")
    @Timed
    public ResponseEntity<List<NewsSummaryToNews>> getAllNewsSummaryToNews(NewsSummaryToNewsCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsSummaryToNews by criteria: {}", criteria);
        Page<NewsSummaryToNews> page = newsSummaryToNewsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-summary-to-news");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-summary-to-news/:id : get the "id" newsSummaryToNews.
     *
     * @param id the id of the newsSummaryToNews to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsSummaryToNews, or with status 404 (Not Found)
     */
    @GetMapping("/news-summary-to-news/{id}")
    @Timed
    public ResponseEntity<NewsSummaryToNews> getNewsSummaryToNews(@PathVariable Long id) {
        log.debug("REST request to get NewsSummaryToNews : {}", id);
        NewsSummaryToNews newsSummaryToNews = newsSummaryToNewsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsSummaryToNews));
    }

    /**
     * DELETE  /news-summary-to-news/:id : delete the "id" newsSummaryToNews.
     *
     * @param id the id of the newsSummaryToNews to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-summary-to-news/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsSummaryToNews(@PathVariable Long id) {
        log.debug("REST request to delete NewsSummaryToNews : {}", id);
        newsSummaryToNewsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-summary-to-news?query=:query : search for the newsSummaryToNews corresponding
     * to the query.
     *
     * @param query the query of the newsSummaryToNews search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-summary-to-news")
    @Timed
    public ResponseEntity<List<NewsSummaryToNews>> searchNewsSummaryToNews(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsSummaryToNews for query {}", query);
        Page<NewsSummaryToNews> page = newsSummaryToNewsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-summary-to-news");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
