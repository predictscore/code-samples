package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.News;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;

@Repository
public interface NewsRepository extends JpaRepository<News, Long>, JpaSpecificationExecutor<News> {
}
