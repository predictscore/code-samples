package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.SiteChatgroupNumUsers;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SiteChatgroupNumUsers entity.
 */
public interface SiteChatgroupNumUsersSearchRepository extends ElasticsearchRepository<SiteChatgroupNumUsers, Long> {
}
