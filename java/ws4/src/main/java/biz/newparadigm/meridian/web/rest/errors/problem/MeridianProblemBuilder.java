package biz.newparadigm.meridian.web.rest.errors.problem;


import org.zalando.problem.ProblemBuilder;
import org.zalando.problem.StatusType;

import javax.annotation.Nullable;
import java.net.URI;
import java.util.*;

public final class MeridianProblemBuilder {
    private static final Set<String> RESERVED_PROPERTIES = new HashSet<>(Arrays.asList(
        "type", "title", "status", "detail", "instance", "cause"
    ));

    private String type;
    private String title;
    private StatusType status;
    private String detail;
    private URI instance;
    private MeridianThrowableProblem cause;
    private final Map<String, Object> parameters = new LinkedHashMap<>();

    /**
     * @see MeridianProblem#builder()
     */
    MeridianProblemBuilder() {

    }

    public MeridianProblemBuilder withType(@Nullable final String type) {
        this.type = type;
        return this;
    }

    public MeridianProblemBuilder withTitle(@Nullable final String title) {
        this.title = title;
        return this;
    }

    public MeridianProblemBuilder withStatus(@Nullable final StatusType status) {
        this.status = status;
        return this;
    }

    public MeridianProblemBuilder withDetail(@Nullable final String detail) {
        this.detail = detail;
        return this;
    }

    public MeridianProblemBuilder withInstance(@Nullable final URI instance) {
        this.instance = instance;
        return this;
    }

    public MeridianProblemBuilder withCause(@Nullable final MeridianThrowableProblem cause) {
        this.cause = cause;
        return this;
    }

    /**
     *
     * @param key
     * @param value
     * @return
     * @throws IllegalArgumentException if key is any of type, title, status, detail or instance
     */
    public MeridianProblemBuilder with(final String key, final Object value) throws IllegalArgumentException {
        if (RESERVED_PROPERTIES.contains(key)) {
            throw new IllegalArgumentException("Property " + key + " is reserved");
        }
        parameters.put(key, value);
        return this;
    }

    public MeridianThrowableProblem build() {
        return new MeridianDefaultProblem(type, title, status, detail, instance, cause, new LinkedHashMap<>(parameters));
    }
}
