package biz.newparadigm.meridian.service.dto.old;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class SiteChatgroupDomainDTO {
    private Long siteChatGroupId = -1L;
    private String domainId;
    private String domainInfo;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.dateWithTimeFormat)
    private Date firstUsedDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Constants.dateWithTimeFormat)
    private Date lastUsedDate;

    public Long getSiteChatGroupId() {
        return siteChatGroupId;
    }

    public void setSiteChatGroupId(Long siteChatGroupId) {
        this.siteChatGroupId = siteChatGroupId;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getDomainInfo() {
        return domainInfo;
    }

    public void setDomainInfo(String domainInfo) {
        this.domainInfo = domainInfo;
    }

    public Date getFirstUsedDate() {
        return firstUsedDate;
    }

    public void setFirstUsedDate(Date firstUsedDate) {
        this.firstUsedDate = firstUsedDate;
    }

    public Date getLastUsedDate() {
        return lastUsedDate;
    }

    public void setLastUsedDate(Date lastUsedDate) {
        this.lastUsedDate = lastUsedDate;
    }
}
