package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsSummaryAttachment;
import biz.newparadigm.meridian.repository.NewsSummaryAttachmentRepository;
import biz.newparadigm.meridian.repository.search.NewsSummaryAttachmentSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsSummaryAttachment.
 */
@Service
@Transactional
public class NewsSummaryAttachmentService {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryAttachmentService.class);

    private final NewsSummaryAttachmentRepository newsSummaryAttachmentRepository;

    private final NewsSummaryAttachmentSearchRepository newsSummaryAttachmentSearchRepository;

    public NewsSummaryAttachmentService(NewsSummaryAttachmentRepository newsSummaryAttachmentRepository, NewsSummaryAttachmentSearchRepository newsSummaryAttachmentSearchRepository) {
        this.newsSummaryAttachmentRepository = newsSummaryAttachmentRepository;
        this.newsSummaryAttachmentSearchRepository = newsSummaryAttachmentSearchRepository;
    }

    /**
     * Save a newsSummaryAttachment.
     *
     * @param newsSummaryAttachment the entity to save
     * @return the persisted entity
     */
    public NewsSummaryAttachment save(NewsSummaryAttachment newsSummaryAttachment) {
        log.debug("Request to save NewsSummaryAttachment : {}", newsSummaryAttachment);
        NewsSummaryAttachment result = newsSummaryAttachmentRepository.save(newsSummaryAttachment);
        newsSummaryAttachmentSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsSummaryAttachments.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryAttachment> findAll(Pageable pageable) {
        log.debug("Request to get all NewsSummaryAttachments");
        return newsSummaryAttachmentRepository.findAll(pageable);
    }

    /**
     *  Get one newsSummaryAttachment by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsSummaryAttachment findOne(Long id) {
        log.debug("Request to get NewsSummaryAttachment : {}", id);
        return newsSummaryAttachmentRepository.findOne(id);
    }

    /**
     *  Delete the  newsSummaryAttachment by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsSummaryAttachment : {}", id);
        newsSummaryAttachmentRepository.delete(id);
        newsSummaryAttachmentSearchRepository.delete(id);
    }

    /**
     * Search for the newsSummaryAttachment corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryAttachment> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsSummaryAttachments for query {}", query);
        Page<NewsSummaryAttachment> result = newsSummaryAttachmentSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
