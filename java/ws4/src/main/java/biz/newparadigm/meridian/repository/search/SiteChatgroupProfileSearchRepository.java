package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.SiteChatgroupProfile;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SiteChatgroupProfile entity.
 */
public interface SiteChatgroupProfileSearchRepository extends ElasticsearchRepository<SiteChatgroupProfile, Long> {
}
