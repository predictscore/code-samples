package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsSummaryMetadata;
import biz.newparadigm.meridian.repository.NewsSummaryMetadataRepository;
import biz.newparadigm.meridian.repository.search.NewsSummaryMetadataSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsSummaryMetadata.
 */
@Service
@Transactional
public class NewsSummaryMetadataService {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryMetadataService.class);

    private final NewsSummaryMetadataRepository newsSummaryMetadataRepository;

    private final NewsSummaryMetadataSearchRepository newsSummaryMetadataSearchRepository;

    public NewsSummaryMetadataService(NewsSummaryMetadataRepository newsSummaryMetadataRepository, NewsSummaryMetadataSearchRepository newsSummaryMetadataSearchRepository) {
        this.newsSummaryMetadataRepository = newsSummaryMetadataRepository;
        this.newsSummaryMetadataSearchRepository = newsSummaryMetadataSearchRepository;
    }

    /**
     * Save a newsSummaryMetadata.
     *
     * @param newsSummaryMetadata the entity to save
     * @return the persisted entity
     */
    public NewsSummaryMetadata save(NewsSummaryMetadata newsSummaryMetadata) {
        log.debug("Request to save NewsSummaryMetadata : {}", newsSummaryMetadata);
        NewsSummaryMetadata result = newsSummaryMetadataRepository.save(newsSummaryMetadata);
        newsSummaryMetadataSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsSummaryMetadata.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryMetadata> findAll(Pageable pageable) {
        log.debug("Request to get all NewsSummaryMetadata");
        return newsSummaryMetadataRepository.findAll(pageable);
    }

    /**
     *  Get one newsSummaryMetadata by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsSummaryMetadata findOne(Long id) {
        log.debug("Request to get NewsSummaryMetadata : {}", id);
        return newsSummaryMetadataRepository.findOne(id);
    }

    /**
     *  Delete the  newsSummaryMetadata by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsSummaryMetadata : {}", id);
        newsSummaryMetadataRepository.delete(id);
        newsSummaryMetadataSearchRepository.delete(id);
    }

    /**
     * Search for the newsSummaryMetadata corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryMetadata> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsSummaryMetadata for query {}", query);
        Page<NewsSummaryMetadata> result = newsSummaryMetadataSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
