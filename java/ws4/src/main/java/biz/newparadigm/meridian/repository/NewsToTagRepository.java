package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsToTag;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsToTag entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsToTagRepository extends JpaRepository<NewsToTag, Long>, JpaSpecificationExecutor<NewsToTag> {

}
