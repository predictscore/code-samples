package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.SiteChatgroupNumVisitors;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SiteChatgroupNumVisitors entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SiteChatgroupNumVisitorsRepository extends JpaRepository<SiteChatgroupNumVisitors, Long>, JpaSpecificationExecutor<SiteChatgroupNumVisitors> {

}
