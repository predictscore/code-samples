package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.SiteChatgroupNumUsers;
import biz.newparadigm.meridian.repository.SiteChatgroupNumUsersRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupNumUsersSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SiteChatgroupNumUsers.
 */
@Service
@Transactional
public class SiteChatgroupNumUsersService {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupNumUsersService.class);

    private final SiteChatgroupNumUsersRepository siteChatgroupNumUsersRepository;

    private final SiteChatgroupNumUsersSearchRepository siteChatgroupNumUsersSearchRepository;

    public SiteChatgroupNumUsersService(SiteChatgroupNumUsersRepository siteChatgroupNumUsersRepository, SiteChatgroupNumUsersSearchRepository siteChatgroupNumUsersSearchRepository) {
        this.siteChatgroupNumUsersRepository = siteChatgroupNumUsersRepository;
        this.siteChatgroupNumUsersSearchRepository = siteChatgroupNumUsersSearchRepository;
    }

    /**
     * Save a siteChatgroupNumUsers.
     *
     * @param siteChatgroupNumUsers the entity to save
     * @return the persisted entity
     */
    public SiteChatgroupNumUsers save(SiteChatgroupNumUsers siteChatgroupNumUsers) {
        log.debug("Request to save SiteChatgroupNumUsers : {}", siteChatgroupNumUsers);
        SiteChatgroupNumUsers result = siteChatgroupNumUsersRepository.save(siteChatgroupNumUsers);
        siteChatgroupNumUsersSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the siteChatgroupNumUsers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupNumUsers> findAll(Pageable pageable) {
        log.debug("Request to get all SiteChatgroupNumUsers");
        return siteChatgroupNumUsersRepository.findAll(pageable);
    }

    /**
     *  Get one siteChatgroupNumUsers by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SiteChatgroupNumUsers findOne(Long id) {
        log.debug("Request to get SiteChatgroupNumUsers : {}", id);
        return siteChatgroupNumUsersRepository.findOne(id);
    }

    /**
     *  Delete the  siteChatgroupNumUsers by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SiteChatgroupNumUsers : {}", id);
        siteChatgroupNumUsersRepository.delete(id);
        siteChatgroupNumUsersSearchRepository.delete(id);
    }

    /**
     * Search for the siteChatgroupNumUsers corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupNumUsers> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SiteChatgroupNumUsers for query {}", query);
        Page<SiteChatgroupNumUsers> result = siteChatgroupNumUsersSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
