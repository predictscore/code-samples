package biz.newparadigm.meridian.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.SiteChatgroupNumVisitors;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.SiteChatgroupNumVisitorsRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupNumVisitorsSearchRepository;
import biz.newparadigm.meridian.service.dto.SiteChatgroupNumVisitorsCriteria;


/**
 * Service for executing complex queries for SiteChatgroupNumVisitors entities in the database.
 * The main input is a {@link SiteChatgroupNumVisitorsCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link SiteChatgroupNumVisitors} or a {@link Page} of {%link SiteChatgroupNumVisitors} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class SiteChatgroupNumVisitorsQueryService extends QueryService<SiteChatgroupNumVisitors> {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupNumVisitorsQueryService.class);


    private final SiteChatgroupNumVisitorsRepository siteChatgroupNumVisitorsRepository;

    private final SiteChatgroupNumVisitorsSearchRepository siteChatgroupNumVisitorsSearchRepository;

    public SiteChatgroupNumVisitorsQueryService(SiteChatgroupNumVisitorsRepository siteChatgroupNumVisitorsRepository, SiteChatgroupNumVisitorsSearchRepository siteChatgroupNumVisitorsSearchRepository) {
        this.siteChatgroupNumVisitorsRepository = siteChatgroupNumVisitorsRepository;
        this.siteChatgroupNumVisitorsSearchRepository = siteChatgroupNumVisitorsSearchRepository;
    }

    /**
     * Return a {@link List} of {%link SiteChatgroupNumVisitors} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SiteChatgroupNumVisitors> findByCriteria(SiteChatgroupNumVisitorsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<SiteChatgroupNumVisitors> specification = createSpecification(criteria);
        return siteChatgroupNumVisitorsRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link SiteChatgroupNumVisitors} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupNumVisitors> findByCriteria(SiteChatgroupNumVisitorsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<SiteChatgroupNumVisitors> specification = createSpecification(criteria);
        return siteChatgroupNumVisitorsRepository.findAll(specification, page);
    }

    /**
     * Function to convert SiteChatgroupNumVisitorsCriteria to a {@link Specifications}
     */
    private Specifications<SiteChatgroupNumVisitors> createSpecification(SiteChatgroupNumVisitorsCriteria criteria) {
        Specifications<SiteChatgroupNumVisitors> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SiteChatgroupNumVisitors_.id));
            }
            if (criteria.getNumVisitors() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumVisitors(), SiteChatgroupNumVisitors_.numVisitors));
            }
            if (criteria.getNumVisitorDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumVisitorDate(), SiteChatgroupNumVisitors_.numVisitorDate));
            }
            if (criteria.getSiteChatgroupId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSiteChatgroupId(), SiteChatgroupNumVisitors_.siteChatgroup, SiteChatgroup_.id));
            }
        }
        return specification;
    }

}
