package biz.newparadigm.meridian.service;

import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsService;
import com.atlassian.crowd.service.GroupManager;
import com.atlassian.crowd.service.GroupMembershipManager;
import com.atlassian.crowd.service.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CrowdService {
    private final Logger log = LoggerFactory.getLogger(CrowdService.class);

    @Autowired private CrowdUserDetailsService crowdUserService;
    @Autowired private UserManager userManager;
    @Autowired private GroupManager groupManager;
    @Autowired private GroupMembershipManager groupMembershipManager;

    public CrowdService() {
    }

    public CrowdUserDetails getUserByUsername(String username) {
        return crowdUserService.loadUserByUsername(username);
    }

    public SOAPPrincipal getUserWithAttributes(String username) {
        try {
            return userManager.getUserWithAttributes(username);
        } catch (Exception e) {
            return null;
        }
    }

    public List<String> getUserNames() {
        try {
            List<String> users = new ArrayList<>();
            List<String> groupNames = getGroupNames();
            for (String group : groupNames) {
                List<String> usersList = groupMembershipManager.getMembers(group);
                if (usersList != null) {
                    users.addAll(usersList);
                }
            }
            return users;
        } catch (Exception e) {
            log.error("Error loading user names", e);
            return new ArrayList<>();
        }
    }

    public List<String> getGroupNames() {
        try {
            return groupManager.getAllGroupNames();
        } catch (Exception e) {
            log.error("Error loading group names", e);
            return new ArrayList<>();
        }
    }
}

