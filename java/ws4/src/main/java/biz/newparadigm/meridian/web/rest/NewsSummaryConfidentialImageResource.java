package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsSummaryConfidentialImage;
import biz.newparadigm.meridian.service.NewsSummaryConfidentialImageService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsSummaryConfidentialImageCriteria;
import biz.newparadigm.meridian.service.NewsSummaryConfidentialImageQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsSummaryConfidentialImage.
 */
@RestController
@RequestMapping("/api")
public class NewsSummaryConfidentialImageResource {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryConfidentialImageResource.class);

    private static final String ENTITY_NAME = "newsSummaryConfidentialImage";

    private final NewsSummaryConfidentialImageService newsSummaryConfidentialImageService;

    private final NewsSummaryConfidentialImageQueryService newsSummaryConfidentialImageQueryService;

    public NewsSummaryConfidentialImageResource(NewsSummaryConfidentialImageService newsSummaryConfidentialImageService, NewsSummaryConfidentialImageQueryService newsSummaryConfidentialImageQueryService) {
        this.newsSummaryConfidentialImageService = newsSummaryConfidentialImageService;
        this.newsSummaryConfidentialImageQueryService = newsSummaryConfidentialImageQueryService;
    }

    /**
     * POST  /news-summary-confidential-images : Create a new newsSummaryConfidentialImage.
     *
     * @param newsSummaryConfidentialImage the newsSummaryConfidentialImage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsSummaryConfidentialImage, or with status 400 (Bad Request) if the newsSummaryConfidentialImage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-summary-confidential-images")
    @Timed
    public ResponseEntity<NewsSummaryConfidentialImage> createNewsSummaryConfidentialImage(@Valid @RequestBody NewsSummaryConfidentialImage newsSummaryConfidentialImage) throws URISyntaxException {
        log.debug("REST request to save NewsSummaryConfidentialImage : {}", newsSummaryConfidentialImage);
        if (newsSummaryConfidentialImage.getId() != null) {
            throw new BadRequestAlertException("A new newsSummaryConfidentialImage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsSummaryConfidentialImage result = newsSummaryConfidentialImageService.save(newsSummaryConfidentialImage);
        return ResponseEntity.created(new URI("/api/news-summary-confidential-images/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-summary-confidential-images : Updates an existing newsSummaryConfidentialImage.
     *
     * @param newsSummaryConfidentialImage the newsSummaryConfidentialImage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsSummaryConfidentialImage,
     * or with status 400 (Bad Request) if the newsSummaryConfidentialImage is not valid,
     * or with status 500 (Internal Server Error) if the newsSummaryConfidentialImage couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-summary-confidential-images")
    @Timed
    public ResponseEntity<NewsSummaryConfidentialImage> updateNewsSummaryConfidentialImage(@Valid @RequestBody NewsSummaryConfidentialImage newsSummaryConfidentialImage) throws URISyntaxException {
        log.debug("REST request to update NewsSummaryConfidentialImage : {}", newsSummaryConfidentialImage);
        if (newsSummaryConfidentialImage.getId() == null) {
            return createNewsSummaryConfidentialImage(newsSummaryConfidentialImage);
        }
        NewsSummaryConfidentialImage result = newsSummaryConfidentialImageService.save(newsSummaryConfidentialImage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsSummaryConfidentialImage.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-summary-confidential-images : get all the newsSummaryConfidentialImages.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsSummaryConfidentialImages in body
     */
    @GetMapping("/news-summary-confidential-images")
    @Timed
    public ResponseEntity<List<NewsSummaryConfidentialImage>> getAllNewsSummaryConfidentialImages(NewsSummaryConfidentialImageCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsSummaryConfidentialImages by criteria: {}", criteria);
        Page<NewsSummaryConfidentialImage> page = newsSummaryConfidentialImageQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-summary-confidential-images");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-summary-confidential-images/:id : get the "id" newsSummaryConfidentialImage.
     *
     * @param id the id of the newsSummaryConfidentialImage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsSummaryConfidentialImage, or with status 404 (Not Found)
     */
    @GetMapping("/news-summary-confidential-images/{id}")
    @Timed
    public ResponseEntity<NewsSummaryConfidentialImage> getNewsSummaryConfidentialImage(@PathVariable Long id) {
        log.debug("REST request to get NewsSummaryConfidentialImage : {}", id);
        NewsSummaryConfidentialImage newsSummaryConfidentialImage = newsSummaryConfidentialImageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsSummaryConfidentialImage));
    }

    /**
     * DELETE  /news-summary-confidential-images/:id : delete the "id" newsSummaryConfidentialImage.
     *
     * @param id the id of the newsSummaryConfidentialImage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-summary-confidential-images/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsSummaryConfidentialImage(@PathVariable Long id) {
        log.debug("REST request to delete NewsSummaryConfidentialImage : {}", id);
        newsSummaryConfidentialImageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-summary-confidential-images?query=:query : search for the newsSummaryConfidentialImage corresponding
     * to the query.
     *
     * @param query the query of the newsSummaryConfidentialImage search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-summary-confidential-images")
    @Timed
    public ResponseEntity<List<NewsSummaryConfidentialImage>> searchNewsSummaryConfidentialImages(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsSummaryConfidentialImages for query {}", query);
        Page<NewsSummaryConfidentialImage> page = newsSummaryConfidentialImageService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-summary-confidential-images");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
