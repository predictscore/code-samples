package biz.newparadigm.meridian.service;


import java.time.ZonedDateTime;
import java.util.List;

import biz.newparadigm.meridian.service.filter.SpecificationBuilder;
import biz.newparadigm.meridian.service.odata.Expression;
import biz.newparadigm.meridian.service.odata.ExpressionBuilder;
import biz.newparadigm.meridian.service.query.MeridianQueryService;
import biz.newparadigm.meridian.service.util.FieldsUtil;
import biz.newparadigm.meridian.service.util.ServiceUtil;
import biz.newparadigm.meridian.web.rest.NewsResource;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.News;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsRepository;
import biz.newparadigm.meridian.repository.search.NewsSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsCriteria;


/**
 * Service for executing complex queries for News entities in the database.
 * The main input is a {@link NewsCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link News} or a {@link Page} of {%link News} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsQueryService extends QueryService<News> {
    private final Logger log = LoggerFactory.getLogger(NewsQueryService.class);

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private NewsService newsService;

    /**
     * Return a {@link List} of {%link News} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<News> findByCriteria(NewsCriteria criteria, boolean filterByLocation, List<String> locationNames) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<News> specification = createSpecification(criteria, filterByLocation, locationNames);
        return newsRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link News} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<News> findByCriteria(String filter, String select, String ignore, NewsCriteria criteria, Pageable page, boolean filterByLocation, List<String> locationNames) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        Expression filterExpression = ExpressionBuilder.build(filter);
        Specifications<News> specification;
        if (filterExpression == null) {
            specification = createSpecification(criteria, filterByLocation, locationNames);
        } else {
            MeridianQueryService<News> meridianQueryService = new MeridianQueryService<>(NewsResource.ENTITY_NAME);
            specification = meridianQueryService.createFilterSpecification(filterExpression, filterByLocation, locationNames);
        }
        Page<News> result = newsRepository.findAll(specification, page);
        if (result != null && result.getContent() != null && !result.getContent().isEmpty()) {
            for (News news : result) {
                FieldsUtil.selectFields(select, news);
                FieldsUtil.ignoreFields(ignore, news);
                Hibernate.initialize(news.getLanguages());
                Hibernate.initialize(news.getTags());
                if (ServiceUtil.checkContains(filter, ServiceUtil.NEWS_FILTER_ATTACHMENTS)) {
                    Hibernate.initialize(news.getAttachments());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.NEWS_FILTER_PUBLIC_IMAGES)) {
                    Hibernate.initialize(news.getPublicImages());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.NEWS_FILTER_ORIGINAL_IMAGES)) {
                    Hibernate.initialize(news.getOriginalImages());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.NEWS_FILTER_CONFIDENTIAL_IMAGES)) {
                    Hibernate.initialize(news.getConfidentialImages());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.NEWS_FILTER_URLS)) {
                    Hibernate.initialize(news.getUrls());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.NEWS_FILTER_DOWNLOADABLE_CONTENTURLS)) {
                    Hibernate.initialize(news.getDownloadableContentUrls());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.NEWS_CATEGORY_FILTER)) {
                    Hibernate.initialize(news.getMetadata());
                    newsService.initializeMetagroups(news);
                    news.setMetadata(null);
                }
                if (news.getAuthor() != null) {
                    if (ServiceUtil.checkContains(filter, ServiceUtil.NEWS_AUTHOR_FILTER)) {
                        Hibernate.initialize(news.getAuthor().getNames());
                    }
                    if (ServiceUtil.checkContains(filter, ServiceUtil.NEWS_AUTHOR_SITECHATGROUP_FILTER)) {
                        Hibernate.initialize(news.getAuthor().getSiteChatGroups());
                    }
                }
                if (news.getSiteChatgroup() != null) {
                    if (ServiceUtil.checkContains(filter, ServiceUtil.NEWS_SITECHATGROUP_FILTER)) {
                        Hibernate.initialize(news.getSiteChatgroup().getNames());
                    }
                }
            }
        }
        return result;
    }

    /**
     * Function to convert NewsCriteria to a {@link Specifications}
     */
    private Specifications<News> createSpecification(NewsCriteria criteria, boolean filterByLocation, List<String> locationNames) {
        SpecificationBuilder<News> builder = new SpecificationBuilder<>();
        Specifications<News> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), News_.id));
            }
            if (criteria.getDatetime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDatetime(), News_.datetime));
            }
            if (criteria.getOriginalSubject() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOriginalSubject(), News_.originalSubject));
            }
            if (criteria.getEnglishSubject() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEnglishSubject(), News_.englishSubject));
            }
            if (criteria.getOriginalPost() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOriginalPost(), News_.originalPost));
            }
            if (criteria.getEnglishPost() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEnglishPost(), News_.englishPost));
            }
            if (criteria.getCreationDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreationDate(), News_.createdDate));
            }
            if (criteria.getAuthorId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getAuthorId(), News_.author, Author_.id));
            }
            if (criteria.getSiteChatgroupId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSiteChatgroupId(), News_.siteChatgroup, SiteChatgroup_.id));
            }
            if (criteria.getLocationId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getLocationId(), News_.location, Location_.id));
            }
            if (filterByLocation) {
                if (locationNames == null || locationNames.isEmpty()) {
                    specification = specification.and(builder.buildNoResultSpecification(News_.id));
                } else {
                    specification = specification.and(builder.buildLocationsSpecification(locationNames, News.class));
                }
            }
        }
        return specification;
    }

}
