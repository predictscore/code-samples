package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.Metafield;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Metafield entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MetafieldRepository extends JpaRepository<Metafield, Long>, JpaSpecificationExecutor<Metafield> {

}
