package biz.newparadigm.meridian.domain;

import biz.newparadigm.meridian.domain.util.MeridianDateSerializer;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

@Entity
@Table(name = "site_chatgroup_num_visitors")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "sitechatgroupnumvisitors")
public class SiteChatgroupNumVisitors implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="site_chatgroup_num_visitors_id_seq")
    @SequenceGenerator(name="site_chatgroup_num_visitors_id_seq", sequenceName="site_chatgroup_num_visitors_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "num_visitors", nullable = false)
    private Integer numVisitors;

    @NotNull
    @Column(name = "num_visitor_date", nullable = false)
    @JsonSerialize(using = MeridianDateSerializer.class)
    private ZonedDateTime numVisitorDate;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"comments", "domains", "ips", "names", "infos", "numVisitors", "numUsers", "languages", "tags", "authors"})
    private SiteChatgroup siteChatgroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumVisitors() {
        return numVisitors;
    }

    public SiteChatgroupNumVisitors numVisitors(Integer numVisitors) {
        this.numVisitors = numVisitors;
        return this;
    }

    public void setNumVisitors(Integer numVisitors) {
        this.numVisitors = numVisitors;
    }

    public ZonedDateTime getNumVisitorDate() {
        return numVisitorDate;
    }

    public SiteChatgroupNumVisitors numVisitorDate(ZonedDateTime numVisitorDate) {
        this.numVisitorDate = numVisitorDate;
        return this;
    }

    public void setNumVisitorDate(ZonedDateTime numVisitorDate) {
        this.numVisitorDate = numVisitorDate;
    }

    public SiteChatgroup getSiteChatgroup() {
        return siteChatgroup;
    }

    public SiteChatgroupNumVisitors siteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
        return this;
    }

    public void setSiteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SiteChatgroupNumVisitors siteChatgroupNumVisitors = (SiteChatgroupNumVisitors) o;
        if (siteChatgroupNumVisitors.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), siteChatgroupNumVisitors.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SiteChatgroupNumVisitors{" +
            "id=" + getId() +
            ", numVisitors='" + getNumVisitors() + "'" +
            ", numVisitorDate='" + getNumVisitorDate() + "'" +
            "}";
    }
}
