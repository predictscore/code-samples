package biz.newparadigm.meridian.domain.metadata;

public class Metagroups {
    private ForProfit forProfit;
    private LoA loa;
    private FF ff;
    private MMvar mmvar;
    private ProdOrServ prodOrServ;
    private Vulnerability vulnerability;

    public Metagroups() {
        forProfit = new ForProfit();
        loa = new LoA();
        ff = new FF();
        mmvar = new MMvar();
        prodOrServ = new ProdOrServ();
        vulnerability = new Vulnerability();
    }

    public ForProfit getForProfit() {
        return forProfit;
    }

    public void setForProfit(ForProfit forProfit) {
        this.forProfit = forProfit;
    }

    public LoA getLoa() {
        return loa;
    }

    public void setLoa(LoA loa) {
        this.loa = loa;
    }

    public FF getFf() {
        return ff;
    }

    public void setFf(FF ff) {
        this.ff = ff;
    }

    public MMvar getMmvar() {
        return mmvar;
    }

    public void setMmvar(MMvar mmvar) {
        this.mmvar = mmvar;
    }

    public ProdOrServ getProdOrServ() {
        return prodOrServ;
    }

    public void setProdOrServ(ProdOrServ prodOrServ) {
        this.prodOrServ = prodOrServ;
    }

    public Vulnerability getVulnerability() {
        return vulnerability;
    }

    public void setVulnerability(Vulnerability vulnerability) {
        this.vulnerability = vulnerability;
    }
}
