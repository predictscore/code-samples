/**
 * View Models used by Spring MVC REST controllers.
 */
package biz.newparadigm.meridian.web.rest.vm;
