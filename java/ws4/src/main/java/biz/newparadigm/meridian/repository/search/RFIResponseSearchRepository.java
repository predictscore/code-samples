package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.RFIResponse;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RFIResponse entity.
 */
public interface RFIResponseSearchRepository extends ElasticsearchRepository<RFIResponse, Long> {
}
