package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsMetadata;
import biz.newparadigm.meridian.repository.NewsMetadataRepository;
import biz.newparadigm.meridian.repository.search.NewsMetadataSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsMetadata.
 */
@Service
@Transactional
public class NewsMetadataService {

    private final Logger log = LoggerFactory.getLogger(NewsMetadataService.class);

    private final NewsMetadataRepository newsMetadataRepository;

    private final NewsMetadataSearchRepository newsMetadataSearchRepository;

    public NewsMetadataService(NewsMetadataRepository newsMetadataRepository, NewsMetadataSearchRepository newsMetadataSearchRepository) {
        this.newsMetadataRepository = newsMetadataRepository;
        this.newsMetadataSearchRepository = newsMetadataSearchRepository;
    }

    /**
     * Save a newsMetadata.
     *
     * @param newsMetadata the entity to save
     * @return the persisted entity
     */
    public NewsMetadata save(NewsMetadata newsMetadata) {
        log.debug("Request to save NewsMetadata : {}", newsMetadata);
        NewsMetadata result = newsMetadataRepository.save(newsMetadata);
        newsMetadataSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsMetadata.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsMetadata> findAll(Pageable pageable) {
        log.debug("Request to get all NewsMetadata");
        return newsMetadataRepository.findAll(pageable);
    }

    /**
     *  Get one newsMetadata by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsMetadata findOne(Long id) {
        log.debug("Request to get NewsMetadata : {}", id);
        return newsMetadataRepository.findOne(id);
    }

    /**
     *  Delete the  newsMetadata by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsMetadata : {}", id);
        newsMetadataRepository.delete(id);
        newsMetadataSearchRepository.delete(id);
    }

    /**
     * Search for the newsMetadata corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsMetadata> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsMetadata for query {}", query);
        Page<NewsMetadata> result = newsMetadataSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
