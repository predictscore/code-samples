package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsUrl;
import biz.newparadigm.meridian.service.NewsUrlService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsUrlCriteria;
import biz.newparadigm.meridian.service.NewsUrlQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsUrl.
 */
@RestController
@RequestMapping("/api")
public class NewsUrlResource {

    private final Logger log = LoggerFactory.getLogger(NewsUrlResource.class);

    private static final String ENTITY_NAME = "newsUrl";

    private final NewsUrlService newsUrlService;

    private final NewsUrlQueryService newsUrlQueryService;

    public NewsUrlResource(NewsUrlService newsUrlService, NewsUrlQueryService newsUrlQueryService) {
        this.newsUrlService = newsUrlService;
        this.newsUrlQueryService = newsUrlQueryService;
    }

    /**
     * POST  /news-urls : Create a new newsUrl.
     *
     * @param newsUrl the newsUrl to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsUrl, or with status 400 (Bad Request) if the newsUrl has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-urls")
    @Timed
    public ResponseEntity<NewsUrl> createNewsUrl(@Valid @RequestBody NewsUrl newsUrl) throws URISyntaxException {
        log.debug("REST request to save NewsUrl : {}", newsUrl);
        if (newsUrl.getId() != null) {
            throw new BadRequestAlertException("A new newsUrl cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsUrl result = newsUrlService.save(newsUrl);
        return ResponseEntity.created(new URI("/api/news-urls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-urls : Updates an existing newsUrl.
     *
     * @param newsUrl the newsUrl to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsUrl,
     * or with status 400 (Bad Request) if the newsUrl is not valid,
     * or with status 500 (Internal Server Error) if the newsUrl couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-urls")
    @Timed
    public ResponseEntity<NewsUrl> updateNewsUrl(@Valid @RequestBody NewsUrl newsUrl) throws URISyntaxException {
        log.debug("REST request to update NewsUrl : {}", newsUrl);
        if (newsUrl.getId() == null) {
            return createNewsUrl(newsUrl);
        }
        NewsUrl result = newsUrlService.save(newsUrl);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsUrl.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-urls : get all the newsUrls.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsUrls in body
     */
    @GetMapping("/news-urls")
    @Timed
    public ResponseEntity<List<NewsUrl>> getAllNewsUrls(NewsUrlCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsUrls by criteria: {}", criteria);
        Page<NewsUrl> page = newsUrlQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-urls");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-urls/:id : get the "id" newsUrl.
     *
     * @param id the id of the newsUrl to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsUrl, or with status 404 (Not Found)
     */
    @GetMapping("/news-urls/{id}")
    @Timed
    public ResponseEntity<NewsUrl> getNewsUrl(@PathVariable Long id) {
        log.debug("REST request to get NewsUrl : {}", id);
        NewsUrl newsUrl = newsUrlService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsUrl));
    }

    /**
     * DELETE  /news-urls/:id : delete the "id" newsUrl.
     *
     * @param id the id of the newsUrl to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-urls/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsUrl(@PathVariable Long id) {
        log.debug("REST request to delete NewsUrl : {}", id);
        newsUrlService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-urls?query=:query : search for the newsUrl corresponding
     * to the query.
     *
     * @param query the query of the newsUrl search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-urls")
    @Timed
    public ResponseEntity<List<NewsUrl>> searchNewsUrls(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsUrls for query {}", query);
        Page<NewsUrl> page = newsUrlService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-urls");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
