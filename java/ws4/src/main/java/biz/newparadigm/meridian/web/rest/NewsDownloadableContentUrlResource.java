package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsDownloadableContentUrl;
import biz.newparadigm.meridian.service.NewsDownloadableContentUrlService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsDownloadableContentUrlCriteria;
import biz.newparadigm.meridian.service.NewsDownloadableContentUrlQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsDownloadableContentUrl.
 */
@RestController
@RequestMapping("/api")
public class NewsDownloadableContentUrlResource {

    private final Logger log = LoggerFactory.getLogger(NewsDownloadableContentUrlResource.class);

    private static final String ENTITY_NAME = "newsDownloadableContentUrl";

    private final NewsDownloadableContentUrlService newsDownloadableContentUrlService;

    private final NewsDownloadableContentUrlQueryService newsDownloadableContentUrlQueryService;

    public NewsDownloadableContentUrlResource(NewsDownloadableContentUrlService newsDownloadableContentUrlService, NewsDownloadableContentUrlQueryService newsDownloadableContentUrlQueryService) {
        this.newsDownloadableContentUrlService = newsDownloadableContentUrlService;
        this.newsDownloadableContentUrlQueryService = newsDownloadableContentUrlQueryService;
    }

    /**
     * POST  /news-downloadable-content-urls : Create a new newsDownloadableContentUrl.
     *
     * @param newsDownloadableContentUrl the newsDownloadableContentUrl to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsDownloadableContentUrl, or with status 400 (Bad Request) if the newsDownloadableContentUrl has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-downloadable-content-urls")
    @Timed
    public ResponseEntity<NewsDownloadableContentUrl> createNewsDownloadableContentUrl(@Valid @RequestBody NewsDownloadableContentUrl newsDownloadableContentUrl) throws URISyntaxException {
        log.debug("REST request to save NewsDownloadableContentUrl : {}", newsDownloadableContentUrl);
        if (newsDownloadableContentUrl.getId() != null) {
            throw new BadRequestAlertException("A new newsDownloadableContentUrl cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsDownloadableContentUrl result = newsDownloadableContentUrlService.save(newsDownloadableContentUrl);
        return ResponseEntity.created(new URI("/api/news-downloadable-content-urls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-downloadable-content-urls : Updates an existing newsDownloadableContentUrl.
     *
     * @param newsDownloadableContentUrl the newsDownloadableContentUrl to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsDownloadableContentUrl,
     * or with status 400 (Bad Request) if the newsDownloadableContentUrl is not valid,
     * or with status 500 (Internal Server Error) if the newsDownloadableContentUrl couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-downloadable-content-urls")
    @Timed
    public ResponseEntity<NewsDownloadableContentUrl> updateNewsDownloadableContentUrl(@Valid @RequestBody NewsDownloadableContentUrl newsDownloadableContentUrl) throws URISyntaxException {
        log.debug("REST request to update NewsDownloadableContentUrl : {}", newsDownloadableContentUrl);
        if (newsDownloadableContentUrl.getId() == null) {
            return createNewsDownloadableContentUrl(newsDownloadableContentUrl);
        }
        NewsDownloadableContentUrl result = newsDownloadableContentUrlService.save(newsDownloadableContentUrl);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsDownloadableContentUrl.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-downloadable-content-urls : get all the newsDownloadableContentUrls.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsDownloadableContentUrls in body
     */
    @GetMapping("/news-downloadable-content-urls")
    @Timed
    public ResponseEntity<List<NewsDownloadableContentUrl>> getAllNewsDownloadableContentUrls(NewsDownloadableContentUrlCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsDownloadableContentUrls by criteria: {}", criteria);
        Page<NewsDownloadableContentUrl> page = newsDownloadableContentUrlQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-downloadable-content-urls");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-downloadable-content-urls/:id : get the "id" newsDownloadableContentUrl.
     *
     * @param id the id of the newsDownloadableContentUrl to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsDownloadableContentUrl, or with status 404 (Not Found)
     */
    @GetMapping("/news-downloadable-content-urls/{id}")
    @Timed
    public ResponseEntity<NewsDownloadableContentUrl> getNewsDownloadableContentUrl(@PathVariable Long id) {
        log.debug("REST request to get NewsDownloadableContentUrl : {}", id);
        NewsDownloadableContentUrl newsDownloadableContentUrl = newsDownloadableContentUrlService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsDownloadableContentUrl));
    }

    /**
     * DELETE  /news-downloadable-content-urls/:id : delete the "id" newsDownloadableContentUrl.
     *
     * @param id the id of the newsDownloadableContentUrl to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-downloadable-content-urls/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsDownloadableContentUrl(@PathVariable Long id) {
        log.debug("REST request to delete NewsDownloadableContentUrl : {}", id);
        newsDownloadableContentUrlService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-downloadable-content-urls?query=:query : search for the newsDownloadableContentUrl corresponding
     * to the query.
     *
     * @param query the query of the newsDownloadableContentUrl search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-downloadable-content-urls")
    @Timed
    public ResponseEntity<List<NewsDownloadableContentUrl>> searchNewsDownloadableContentUrls(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsDownloadableContentUrls for query {}", query);
        Page<NewsDownloadableContentUrl> page = newsDownloadableContentUrlService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-downloadable-content-urls");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
