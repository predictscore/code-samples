package biz.newparadigm.meridian.domain.audit;

public enum AuditEventType {
    INSERT,UPDATE,DELETE
}
