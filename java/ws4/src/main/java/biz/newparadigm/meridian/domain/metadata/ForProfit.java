package biz.newparadigm.meridian.domain.metadata;

import java.util.ArrayList;
import java.util.List;

public class ForProfit {
    private String priceOriginal;
    private String priceEnglish;
    private String typicalUsers;

    public String getPriceOriginal() {
        return priceOriginal;
    }

    public void setPriceOriginal(String priceOriginal) {
        this.priceOriginal = priceOriginal;
    }

    public String getPriceEnglish() {
        return priceEnglish;
    }

    public void setPriceEnglish(String priceEnglish) {
        this.priceEnglish = priceEnglish;
    }

    public String getTypicalUsers() {
        return typicalUsers;
    }

    public void setTypicalUsers(String typicalUsers) {
        this.typicalUsers = typicalUsers;
    }
}
