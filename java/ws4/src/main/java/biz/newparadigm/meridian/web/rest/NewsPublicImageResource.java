package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsPublicImage;
import biz.newparadigm.meridian.service.NewsPublicImageService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsPublicImageCriteria;
import biz.newparadigm.meridian.service.NewsPublicImageQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsPublicImage.
 */
@RestController
@RequestMapping("/api")
public class NewsPublicImageResource {

    private final Logger log = LoggerFactory.getLogger(NewsPublicImageResource.class);

    private static final String ENTITY_NAME = "newsPublicImage";

    private final NewsPublicImageService newsPublicImageService;

    private final NewsPublicImageQueryService newsPublicImageQueryService;

    public NewsPublicImageResource(NewsPublicImageService newsPublicImageService, NewsPublicImageQueryService newsPublicImageQueryService) {
        this.newsPublicImageService = newsPublicImageService;
        this.newsPublicImageQueryService = newsPublicImageQueryService;
    }

    /**
     * POST  /news-public-images : Create a new newsPublicImage.
     *
     * @param newsPublicImage the newsPublicImage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsPublicImage, or with status 400 (Bad Request) if the newsPublicImage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-public-images")
    @Timed
    public ResponseEntity<NewsPublicImage> createNewsPublicImage(@Valid @RequestBody NewsPublicImage newsPublicImage) throws URISyntaxException {
        log.debug("REST request to save NewsPublicImage : {}", newsPublicImage);
        if (newsPublicImage.getId() != null) {
            throw new BadRequestAlertException("A new newsPublicImage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsPublicImage result = newsPublicImageService.save(newsPublicImage);
        return ResponseEntity.created(new URI("/api/news-public-images/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-public-images : Updates an existing newsPublicImage.
     *
     * @param newsPublicImage the newsPublicImage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsPublicImage,
     * or with status 400 (Bad Request) if the newsPublicImage is not valid,
     * or with status 500 (Internal Server Error) if the newsPublicImage couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-public-images")
    @Timed
    public ResponseEntity<NewsPublicImage> updateNewsPublicImage(@Valid @RequestBody NewsPublicImage newsPublicImage) throws URISyntaxException {
        log.debug("REST request to update NewsPublicImage : {}", newsPublicImage);
        if (newsPublicImage.getId() == null) {
            return createNewsPublicImage(newsPublicImage);
        }
        NewsPublicImage result = newsPublicImageService.save(newsPublicImage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsPublicImage.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-public-images : get all the newsPublicImages.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsPublicImages in body
     */
    @GetMapping("/news-public-images")
    @Timed
    public ResponseEntity<List<NewsPublicImage>> getAllNewsPublicImages(NewsPublicImageCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsPublicImages by criteria: {}", criteria);
        Page<NewsPublicImage> page = newsPublicImageQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-public-images");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-public-images/:id : get the "id" newsPublicImage.
     *
     * @param id the id of the newsPublicImage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsPublicImage, or with status 404 (Not Found)
     */
    @GetMapping("/news-public-images/{id}")
    @Timed
    public ResponseEntity<NewsPublicImage> getNewsPublicImage(@PathVariable Long id) {
        log.debug("REST request to get NewsPublicImage : {}", id);
        NewsPublicImage newsPublicImage = newsPublicImageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsPublicImage));
    }

    /**
     * DELETE  /news-public-images/:id : delete the "id" newsPublicImage.
     *
     * @param id the id of the newsPublicImage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-public-images/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsPublicImage(@PathVariable Long id) {
        log.debug("REST request to delete NewsPublicImage : {}", id);
        newsPublicImageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-public-images?query=:query : search for the newsPublicImage corresponding
     * to the query.
     *
     * @param query the query of the newsPublicImage search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-public-images")
    @Timed
    public ResponseEntity<List<NewsPublicImage>> searchNewsPublicImages(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsPublicImages for query {}", query);
        Page<NewsPublicImage> page = newsPublicImageService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-public-images");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
