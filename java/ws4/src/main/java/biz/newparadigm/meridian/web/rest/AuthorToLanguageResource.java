package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.AuthorToLanguage;
import biz.newparadigm.meridian.service.AuthorToLanguageService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.AuthorToLanguageCriteria;
import biz.newparadigm.meridian.service.AuthorToLanguageQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AuthorToLanguage.
 */
@RestController
@RequestMapping("/api")
public class AuthorToLanguageResource {

    private final Logger log = LoggerFactory.getLogger(AuthorToLanguageResource.class);

    private static final String ENTITY_NAME = "authorToLanguage";

    private final AuthorToLanguageService authorToLanguageService;

    private final AuthorToLanguageQueryService authorToLanguageQueryService;

    public AuthorToLanguageResource(AuthorToLanguageService authorToLanguageService, AuthorToLanguageQueryService authorToLanguageQueryService) {
        this.authorToLanguageService = authorToLanguageService;
        this.authorToLanguageQueryService = authorToLanguageQueryService;
    }

    /**
     * POST  /author-to-languages : Create a new authorToLanguage.
     *
     * @param authorToLanguage the authorToLanguage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new authorToLanguage, or with status 400 (Bad Request) if the authorToLanguage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/author-to-languages")
    @Timed
    public ResponseEntity<AuthorToLanguage> createAuthorToLanguage(@Valid @RequestBody AuthorToLanguage authorToLanguage) throws URISyntaxException {
        log.debug("REST request to save AuthorToLanguage : {}", authorToLanguage);
        if (authorToLanguage.getId() != null) {
            throw new BadRequestAlertException("A new authorToLanguage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuthorToLanguage result = authorToLanguageService.save(authorToLanguage);
        return ResponseEntity.created(new URI("/api/author-to-languages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /author-to-languages : Updates an existing authorToLanguage.
     *
     * @param authorToLanguage the authorToLanguage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated authorToLanguage,
     * or with status 400 (Bad Request) if the authorToLanguage is not valid,
     * or with status 500 (Internal Server Error) if the authorToLanguage couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/author-to-languages")
    @Timed
    public ResponseEntity<AuthorToLanguage> updateAuthorToLanguage(@Valid @RequestBody AuthorToLanguage authorToLanguage) throws URISyntaxException {
        log.debug("REST request to update AuthorToLanguage : {}", authorToLanguage);
        if (authorToLanguage.getId() == null) {
            return createAuthorToLanguage(authorToLanguage);
        }
        AuthorToLanguage result = authorToLanguageService.save(authorToLanguage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, authorToLanguage.getId().toString()))
            .body(result);
    }

    /**
     * GET  /author-to-languages : get all the authorToLanguages.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of authorToLanguages in body
     */
    @GetMapping("/author-to-languages")
    @Timed
    public ResponseEntity<List<AuthorToLanguage>> getAllAuthorToLanguages(AuthorToLanguageCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get AuthorToLanguages by criteria: {}", criteria);
        Page<AuthorToLanguage> page = authorToLanguageQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/author-to-languages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /author-to-languages/:id : get the "id" authorToLanguage.
     *
     * @param id the id of the authorToLanguage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the authorToLanguage, or with status 404 (Not Found)
     */
    @GetMapping("/author-to-languages/{id}")
    @Timed
    public ResponseEntity<AuthorToLanguage> getAuthorToLanguage(@PathVariable Long id) {
        log.debug("REST request to get AuthorToLanguage : {}", id);
        AuthorToLanguage authorToLanguage = authorToLanguageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(authorToLanguage));
    }

    /**
     * DELETE  /author-to-languages/:id : delete the "id" authorToLanguage.
     *
     * @param id the id of the authorToLanguage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/author-to-languages/{id}")
    @Timed
    public ResponseEntity<Void> deleteAuthorToLanguage(@PathVariable Long id) {
        log.debug("REST request to delete AuthorToLanguage : {}", id);
        authorToLanguageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/author-to-languages?query=:query : search for the authorToLanguage corresponding
     * to the query.
     *
     * @param query the query of the authorToLanguage search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/author-to-languages")
    @Timed
    public ResponseEntity<List<AuthorToLanguage>> searchAuthorToLanguages(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of AuthorToLanguages for query {}", query);
        Page<AuthorToLanguage> page = authorToLanguageService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/author-to-languages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
