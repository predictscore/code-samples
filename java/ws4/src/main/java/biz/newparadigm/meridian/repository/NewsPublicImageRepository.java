package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsPublicImage;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsPublicImage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsPublicImageRepository extends JpaRepository<NewsPublicImage, Long>, JpaSpecificationExecutor<NewsPublicImage> {

}
