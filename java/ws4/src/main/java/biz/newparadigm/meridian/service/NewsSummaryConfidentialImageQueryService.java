package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsSummaryConfidentialImage;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsSummaryConfidentialImageRepository;
import biz.newparadigm.meridian.repository.search.NewsSummaryConfidentialImageSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsSummaryConfidentialImageCriteria;


/**
 * Service for executing complex queries for NewsSummaryConfidentialImage entities in the database.
 * The main input is a {@link NewsSummaryConfidentialImageCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsSummaryConfidentialImage} or a {@link Page} of {%link NewsSummaryConfidentialImage} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsSummaryConfidentialImageQueryService extends QueryService<NewsSummaryConfidentialImage> {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryConfidentialImageQueryService.class);


    private final NewsSummaryConfidentialImageRepository newsSummaryConfidentialImageRepository;

    private final NewsSummaryConfidentialImageSearchRepository newsSummaryConfidentialImageSearchRepository;

    public NewsSummaryConfidentialImageQueryService(NewsSummaryConfidentialImageRepository newsSummaryConfidentialImageRepository, NewsSummaryConfidentialImageSearchRepository newsSummaryConfidentialImageSearchRepository) {
        this.newsSummaryConfidentialImageRepository = newsSummaryConfidentialImageRepository;
        this.newsSummaryConfidentialImageSearchRepository = newsSummaryConfidentialImageSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsSummaryConfidentialImage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsSummaryConfidentialImage> findByCriteria(NewsSummaryConfidentialImageCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsSummaryConfidentialImage> specification = createSpecification(criteria);
        return newsSummaryConfidentialImageRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsSummaryConfidentialImage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryConfidentialImage> findByCriteria(NewsSummaryConfidentialImageCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsSummaryConfidentialImage> specification = createSpecification(criteria);
        return newsSummaryConfidentialImageRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsSummaryConfidentialImageCriteria to a {@link Specifications}
     */
    private Specifications<NewsSummaryConfidentialImage> createSpecification(NewsSummaryConfidentialImageCriteria criteria) {
        Specifications<NewsSummaryConfidentialImage> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsSummaryConfidentialImage_.id));
            }
            if (criteria.getFilename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFilename(), NewsSummaryConfidentialImage_.filename));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), NewsSummaryConfidentialImage_.description));
            }
            if (criteria.getDescriptionTranslation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescriptionTranslation(), NewsSummaryConfidentialImage_.descriptionTranslation));
            }
            if (criteria.getImageOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getImageOrder(), NewsSummaryConfidentialImage_.imageOrder));
            }
            if (criteria.getNewsSummaryId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsSummaryId(), NewsSummaryConfidentialImage_.newsSummary, NewsSummary_.id));
            }
        }
        return specification;
    }

}
