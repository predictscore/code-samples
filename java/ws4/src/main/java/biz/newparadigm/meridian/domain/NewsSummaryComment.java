package biz.newparadigm.meridian.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A NewsSummaryComment.
 */
@Entity
@Table(name = "news_summary_comments")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "newssummarycomment")
public class NewsSummaryComment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="news_summary_comments_id_seq")
    @SequenceGenerator(name="news_summary_comments_id_seq", sequenceName="news_summary_comments_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "user_id", nullable = false)
    private String userId;

    @NotNull
    @Column(name = "comment", nullable = false)
    private String comment;

    @ManyToOne(optional = false)
    @NotNull
    private NewsSummary newsSummary;

    @ManyToOne
    private NewsSummaryComment parentComment;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public NewsSummaryComment userId(String userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public NewsSummaryComment comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public NewsSummary getNewsSummary() {
        return newsSummary;
    }

    public NewsSummaryComment newsSummary(NewsSummary newsSummary) {
        this.newsSummary = newsSummary;
        return this;
    }

    public void setNewsSummary(NewsSummary newsSummary) {
        this.newsSummary = newsSummary;
    }

    public NewsSummaryComment getParentComment() {
        return parentComment;
    }

    public NewsSummaryComment parentComment(NewsSummaryComment newsSummaryComment) {
        this.parentComment = newsSummaryComment;
        return this;
    }

    public void setParentComment(NewsSummaryComment newsSummaryComment) {
        this.parentComment = newsSummaryComment;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NewsSummaryComment newsSummaryComment = (NewsSummaryComment) o;
        if (newsSummaryComment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), newsSummaryComment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NewsSummaryComment{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", comment='" + getComment() + "'" +
            "}";
    }
}
