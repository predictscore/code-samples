package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsSummaryConfidentialImage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsSummaryConfidentialImage entity.
 */
public interface NewsSummaryConfidentialImageSearchRepository extends ElasticsearchRepository<NewsSummaryConfidentialImage, Long> {
}
