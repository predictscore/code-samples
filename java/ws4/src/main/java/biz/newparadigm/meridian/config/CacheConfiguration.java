package biz.newparadigm.meridian.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache("users", jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.User.class.getName() + ".authorities", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.Author.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.AuthorComment.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.AuthorName.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.AuthorProfile.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.AuthorToContact.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.AuthorToLanguage.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.AuthorToSiteChatgroup.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.AuthorToTag.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.Category.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.ContactType.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.Language.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.Location.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.Metafield.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.Metagroup.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.News.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.NewsAttachment.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.NewsComment.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.NewsConfidentialImage.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.NewsDownloadableContentUrl.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.NewsMetadata.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.NewsOriginalImage.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.NewsPublicImage.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.NewsSummary.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.NewsSummaryAttachment.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.NewsSummaryComment.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.NewsSummaryConfidentialImage.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.NewsSummaryMetadata.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.NewsSummaryPublicImage.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.NewsSummaryToNews.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.NewsSummaryToTag.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.NewsToLanguage.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.NewsToTag.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.NewsUrl.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.RFIResponse.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroup.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroupComment.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroupDomain.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroupProfile.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroupIp.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroupName.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroupNumUsers.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroupNumVisitors.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroupToLanguage.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroupToTag.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.Tag.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.MitigationsBypassed.class.getName(), jcacheConfiguration);
            cm.createCache(biz.newparadigm.meridian.domain.NewsToRelatedAuthor.class.getName(), jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.News.class.getName() + ".comments", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.News.class.getName() + ".attachments", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.News.class.getName() + ".originalImages", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.News.class.getName() + ".publicImages", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.News.class.getName() + ".urls", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.News.class.getName() + ".downloadableContentUrls", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.News.class.getName() + ".confidentialImages", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.News.class.getName() + ".metadata", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.News.class.getName() + ".tags", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.News.class.getName() + ".languages", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.News.class.getName() + ".newsSummaries", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.Author.class.getName() + ".profiles", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.Author.class.getName() + ".names", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.Author.class.getName() + ".comments", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.Author.class.getName() + ".contacts", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.Author.class.getName() + ".siteChatGroups", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.Author.class.getName() + ".tags", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.Author.class.getName() + ".languages", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroup.class.getName() + ".comments", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroup.class.getName() + ".domains", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroup.class.getName() + ".ips", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroup.class.getName() + ".names", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroup.class.getName() + ".infos", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroup.class.getName() + ".numVisitors", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroup.class.getName() + ".numUsers", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroup.class.getName() + ".languages", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroup.class.getName() + ".tags", jcacheConfiguration);
//            cm.createCache(biz.newparadigm.meridian.domain.SiteChatgroup.class.getName() + ".authors", jcacheConfiguration);
        };
    }
}
