package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsSummary;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsSummary entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsSummaryRepository extends JpaRepository<NewsSummary, Long>, JpaSpecificationExecutor<NewsSummary> {

}
