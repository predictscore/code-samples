package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.SiteChatgroupToLanguage;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SiteChatgroupToLanguage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SiteChatgroupToLanguageRepository extends JpaRepository<SiteChatgroupToLanguage, Long>, JpaSpecificationExecutor<SiteChatgroupToLanguage> {

}
