package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.SiteChatgroupComment;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SiteChatgroupComment entity.
 */
public interface SiteChatgroupCommentSearchRepository extends ElasticsearchRepository<SiteChatgroupComment, Long> {
}
