package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsSummaryToNews;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsSummaryToNews entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsSummaryToNewsRepository extends JpaRepository<NewsSummaryToNews, Long>, JpaSpecificationExecutor<NewsSummaryToNews> {

}
