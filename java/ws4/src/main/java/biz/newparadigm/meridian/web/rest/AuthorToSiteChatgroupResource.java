package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.AuthorToSiteChatgroup;
import biz.newparadigm.meridian.service.AuthorToSiteChatgroupService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.AuthorToSiteChatgroupCriteria;
import biz.newparadigm.meridian.service.AuthorToSiteChatgroupQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AuthorToSiteChatgroup.
 */
@RestController
@RequestMapping("/api")
public class AuthorToSiteChatgroupResource {

    private final Logger log = LoggerFactory.getLogger(AuthorToSiteChatgroupResource.class);

    private static final String ENTITY_NAME = "authorToSiteChatgroup";

    private final AuthorToSiteChatgroupService authorToSiteChatgroupService;

    private final AuthorToSiteChatgroupQueryService authorToSiteChatgroupQueryService;

    public AuthorToSiteChatgroupResource(AuthorToSiteChatgroupService authorToSiteChatgroupService, AuthorToSiteChatgroupQueryService authorToSiteChatgroupQueryService) {
        this.authorToSiteChatgroupService = authorToSiteChatgroupService;
        this.authorToSiteChatgroupQueryService = authorToSiteChatgroupQueryService;
    }

    /**
     * POST  /author-to-site-chatgroups : Create a new authorToSiteChatgroup.
     *
     * @param authorToSiteChatgroup the authorToSiteChatgroup to create
     * @return the ResponseEntity with status 201 (Created) and with body the new authorToSiteChatgroup, or with status 400 (Bad Request) if the authorToSiteChatgroup has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/author-to-site-chatgroups")
    @Timed
    public ResponseEntity<AuthorToSiteChatgroup> createAuthorToSiteChatgroup(@Valid @RequestBody AuthorToSiteChatgroup authorToSiteChatgroup) throws URISyntaxException {
        log.debug("REST request to save AuthorToSiteChatgroup : {}", authorToSiteChatgroup);
        if (authorToSiteChatgroup.getId() != null) {
            throw new BadRequestAlertException("A new authorToSiteChatgroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuthorToSiteChatgroup result = authorToSiteChatgroupService.save(authorToSiteChatgroup);
        return ResponseEntity.created(new URI("/api/author-to-site-chatgroups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /author-to-site-chatgroups : Updates an existing authorToSiteChatgroup.
     *
     * @param authorToSiteChatgroup the authorToSiteChatgroup to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated authorToSiteChatgroup,
     * or with status 400 (Bad Request) if the authorToSiteChatgroup is not valid,
     * or with status 500 (Internal Server Error) if the authorToSiteChatgroup couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/author-to-site-chatgroups")
    @Timed
    public ResponseEntity<AuthorToSiteChatgroup> updateAuthorToSiteChatgroup(@Valid @RequestBody AuthorToSiteChatgroup authorToSiteChatgroup) throws URISyntaxException {
        log.debug("REST request to update AuthorToSiteChatgroup : {}", authorToSiteChatgroup);
        if (authorToSiteChatgroup.getId() == null) {
            return createAuthorToSiteChatgroup(authorToSiteChatgroup);
        }
        AuthorToSiteChatgroup result = authorToSiteChatgroupService.save(authorToSiteChatgroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, authorToSiteChatgroup.getId().toString()))
            .body(result);
    }

    /**
     * GET  /author-to-site-chatgroups : get all the authorToSiteChatgroups.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of authorToSiteChatgroups in body
     */
    @GetMapping("/author-to-site-chatgroups")
    @Timed
    public ResponseEntity<List<AuthorToSiteChatgroup>> getAllAuthorToSiteChatgroups(AuthorToSiteChatgroupCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get AuthorToSiteChatgroups by criteria: {}", criteria);
        Page<AuthorToSiteChatgroup> page = authorToSiteChatgroupQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/author-to-site-chatgroups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /author-to-site-chatgroups/:id : get the "id" authorToSiteChatgroup.
     *
     * @param id the id of the authorToSiteChatgroup to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the authorToSiteChatgroup, or with status 404 (Not Found)
     */
    @GetMapping("/author-to-site-chatgroups/{id}")
    @Timed
    public ResponseEntity<AuthorToSiteChatgroup> getAuthorToSiteChatgroup(@PathVariable Long id) {
        log.debug("REST request to get AuthorToSiteChatgroup : {}", id);
        AuthorToSiteChatgroup authorToSiteChatgroup = authorToSiteChatgroupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(authorToSiteChatgroup));
    }

    /**
     * DELETE  /author-to-site-chatgroups/:id : delete the "id" authorToSiteChatgroup.
     *
     * @param id the id of the authorToSiteChatgroup to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/author-to-site-chatgroups/{id}")
    @Timed
    public ResponseEntity<Void> deleteAuthorToSiteChatgroup(@PathVariable Long id) {
        log.debug("REST request to delete AuthorToSiteChatgroup : {}", id);
        authorToSiteChatgroupService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/author-to-site-chatgroups?query=:query : search for the authorToSiteChatgroup corresponding
     * to the query.
     *
     * @param query the query of the authorToSiteChatgroup search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/author-to-site-chatgroups")
    @Timed
    public ResponseEntity<List<AuthorToSiteChatgroup>> searchAuthorToSiteChatgroups(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of AuthorToSiteChatgroups for query {}", query);
        Page<AuthorToSiteChatgroup> page = authorToSiteChatgroupService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/author-to-site-chatgroups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
