package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsSummaryAttachment;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsSummaryAttachmentRepository;
import biz.newparadigm.meridian.repository.search.NewsSummaryAttachmentSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsSummaryAttachmentCriteria;


/**
 * Service for executing complex queries for NewsSummaryAttachment entities in the database.
 * The main input is a {@link NewsSummaryAttachmentCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsSummaryAttachment} or a {@link Page} of {%link NewsSummaryAttachment} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsSummaryAttachmentQueryService extends QueryService<NewsSummaryAttachment> {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryAttachmentQueryService.class);


    private final NewsSummaryAttachmentRepository newsSummaryAttachmentRepository;

    private final NewsSummaryAttachmentSearchRepository newsSummaryAttachmentSearchRepository;

    public NewsSummaryAttachmentQueryService(NewsSummaryAttachmentRepository newsSummaryAttachmentRepository, NewsSummaryAttachmentSearchRepository newsSummaryAttachmentSearchRepository) {
        this.newsSummaryAttachmentRepository = newsSummaryAttachmentRepository;
        this.newsSummaryAttachmentSearchRepository = newsSummaryAttachmentSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsSummaryAttachment} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsSummaryAttachment> findByCriteria(NewsSummaryAttachmentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsSummaryAttachment> specification = createSpecification(criteria);
        return newsSummaryAttachmentRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsSummaryAttachment} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryAttachment> findByCriteria(NewsSummaryAttachmentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsSummaryAttachment> specification = createSpecification(criteria);
        return newsSummaryAttachmentRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsSummaryAttachmentCriteria to a {@link Specifications}
     */
    private Specifications<NewsSummaryAttachment> createSpecification(NewsSummaryAttachmentCriteria criteria) {
        Specifications<NewsSummaryAttachment> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsSummaryAttachment_.id));
            }
            if (criteria.getFilename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFilename(), NewsSummaryAttachment_.filename));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), NewsSummaryAttachment_.description));
            }
            if (criteria.getDescriptionTranslation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescriptionTranslation(), NewsSummaryAttachment_.descriptionTranslation));
            }
            if (criteria.getNewsSummaryId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsSummaryId(), NewsSummaryAttachment_.newsSummary, NewsSummary_.id));
            }
        }
        return specification;
    }

}
