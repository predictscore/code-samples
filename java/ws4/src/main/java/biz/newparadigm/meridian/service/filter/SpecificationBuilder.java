package biz.newparadigm.meridian.service.filter;

import biz.newparadigm.meridian.domain.*;
import biz.newparadigm.meridian.service.query.MeridianQueryService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import java.util.Collection;
import java.util.List;

public class SpecificationBuilder<ENTITY> {
    public Specification<ENTITY> likeUpperSpecification(String field, final String value) {
        return (root, query, builder) -> builder.like(builder.upper(root.get(field)), wrapLikeQuery(value));
    }

    private Specification<ENTITY> likeUpperSpecification(String reference, String idField,  String value) {
        return (root, query, builder) -> builder.like(builder.upper(root.get(reference).get(idField)), wrapLikeQuery(value));
    }

    public <X> Specification<ENTITY> equalsSpecification(String field, final X value) {
        return (root, query, builder) -> builder.equal(root.get(field), value);
    }

    private <X> Specification<ENTITY> equalsSpecification(String reference, String idField,  X value) {
        return (root, query, builder) -> builder.equal(root.get(reference).get(idField), value);
    }

    public <X extends Comparable<? super X>> Specification<ENTITY> greaterThanOrEqualTo(String field, final X value) {
        return (root, query, builder) -> builder.greaterThanOrEqualTo(root.get(field), value);
    }

    public <X extends Comparable<? super X>> Specification<ENTITY> lessThanOrEqualTo(String field, final X value) {
        return (root, query, builder) -> builder.lessThanOrEqualTo(root.get(field), value);
    }

    public Specification<ENTITY> buildReferringEntitySpecification(String reference, String idField, String value) {
        Specifications<ENTITY> specification = Specifications.where(null);
        specification = specification.and(equalsSpecification(reference, idField, value));
        return specification;
    }

    public Specification<News> buildExportTagsSpecification(Collection<String> tagsList) {
        return (root, query, builder) -> {
            final Subquery<Long> newsQuery = query.subquery(Long.class);
            final Root<News> news = newsQuery.from(News.class);
            final Join<News, NewsToTag> newsToTags = news.join("tags", JoinType.INNER);
            final Join<NewsToTag, Tag> tags = newsToTags.join("tag", JoinType.INNER);
            newsQuery.select(news.get("id"));
            CriteriaBuilder.In<String> in = builder.in(tags.get("name"));
            for (String value : tagsList) {
                in = in.value(value);
            }
            newsQuery.where(in);
            return builder.in(root.get("id")).value(newsQuery);
        };
    }

    public Specification<ENTITY> buildNoResultSpecification(SingularAttribute<? super ENTITY, Long> field) {
        return (root, query, builder) -> builder.equal(root.get(field), -1);
    }

    public Specification<ENTITY> buildNoResultSpecification(String field) {
        return (root, query, builder) -> builder.equal(root.get(field), -1);
    }

    public Specification<ENTITY> buildLocationsSpecification(Collection<String> locationNames, Class aClass) {
        return (root, query, builder) -> {
            final Subquery<Long> subquery = query.subquery(Long.class);
            final Root<ENTITY> from = subquery.from(aClass);
            final Join<ENTITY, Location> locations = from.join("location", JoinType.INNER);
            subquery.select(from.get("id"));
            CriteriaBuilder.In<String> in = builder.in(locations.get("name"));
            for (String value : locationNames) {
                in = in.value(value);
            }
            subquery.where(in);
            return builder.in(root.get("id")).value(subquery);
        };
    }

    public Specification<Location> buildLocationsSpecificationForLocation(Collection<String> locationNames) {
        return (root, query, builder) -> {
            final Subquery<Long> subquery = query.subquery(Long.class);
            final Root<Location> from = subquery.from(Location.class);
            subquery.select(from.get("id"));
            CriteriaBuilder.In<String> in = builder.in(from.get("name"));
            for (String value : locationNames) {
                in = in.value(value);
            }
            subquery.where(in);
            return builder.in(root.get("id")).value(subquery);
        };
    }

    private String wrapLikeQuery(String txt) {
        txt =  MeridianQueryService.encodeForQuery(txt);
        return "%" + txt.toUpperCase() + '%';
    }
}
