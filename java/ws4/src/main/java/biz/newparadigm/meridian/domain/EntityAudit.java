package biz.newparadigm.meridian.domain;

import biz.newparadigm.meridian.domain.util.MeridianDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

@Entity
@Table(name = "entity_audit")
public class EntityAudit implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="entity_audit_id_seq")
    @SequenceGenerator(name="entity_audit_id_seq", sequenceName="entity_audit_id_seq", allocationSize=1)
    private Long id;

    @Column(name = "entity_id")
    private Long entityId;

    @Column(name = "entity_type")
    private String entityType;

    @Column(name = "action")
    private String action;

    @Column(name = "entity_old_value")
    private String entityOldValue;

    @Column(name = "entity_news_value")
    private String entityNewValue;

    @Column(name = "entity_changes")
    private String entityChanges;

    @Column(name = "commit_version")
    private Integer commitVersion;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    @JsonSerialize(using = MeridianDateSerializer.class)
    private ZonedDateTime createdDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getEntityOldValue() {
        return entityOldValue;
    }

    public void setEntityOldValue(String entityOldValue) {
        this.entityOldValue = entityOldValue;
    }

    public String getEntityNewValue() {
        return entityNewValue;
    }

    public void setEntityNewValue(String entityNewValue) {
        this.entityNewValue = entityNewValue;
    }

    public String getEntityChanges() {
        return entityChanges;
    }

    public void setEntityChanges(String entityChanges) {
        this.entityChanges = entityChanges;
    }

    public Integer getCommitVersion() {
        return commitVersion;
    }

    public void setCommitVersion(Integer commitVersion) {
        this.commitVersion = commitVersion;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EntityAudit entityAuditEvent = (EntityAudit) o;
        return Objects.equals(id, entityAuditEvent.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "EntityAudit{" +
            "id=" + id +
            ", entityId='" + entityId + "'" +
            ", entityType='" + entityType + "'" +
            ", action='" + action + "'" +
            ", commitVersion='" + commitVersion + "'" +
            ", modifiedBy='" + createdBy + "'" +
            ", modifiedDate='" + createdDate + "'" +
            '}';
    }
}
