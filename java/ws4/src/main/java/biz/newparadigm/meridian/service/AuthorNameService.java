package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.AuthorName;
import biz.newparadigm.meridian.repository.AuthorNameRepository;
import biz.newparadigm.meridian.repository.search.AuthorNameSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing AuthorName.
 */
@Service
@Transactional
public class AuthorNameService {

    private final Logger log = LoggerFactory.getLogger(AuthorNameService.class);

    private final AuthorNameRepository authorNameRepository;

    private final AuthorNameSearchRepository authorNameSearchRepository;

    public AuthorNameService(AuthorNameRepository authorNameRepository, AuthorNameSearchRepository authorNameSearchRepository) {
        this.authorNameRepository = authorNameRepository;
        this.authorNameSearchRepository = authorNameSearchRepository;
    }

    /**
     * Save a authorName.
     *
     * @param authorName the entity to save
     * @return the persisted entity
     */
    public AuthorName save(AuthorName authorName) {
        log.debug("Request to save AuthorName : {}", authorName);
        AuthorName result = authorNameRepository.save(authorName);
        authorNameSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the authorNames.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuthorName> findAll(Pageable pageable) {
        log.debug("Request to get all AuthorNames");
        return authorNameRepository.findAll(pageable);
    }

    /**
     *  Get one authorName by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AuthorName findOne(Long id) {
        log.debug("Request to get AuthorName : {}", id);
        return authorNameRepository.findOne(id);
    }

    /**
     *  Delete the  authorName by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AuthorName : {}", id);
        authorNameRepository.delete(id);
        authorNameSearchRepository.delete(id);
    }

    /**
     * Search for the authorName corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuthorName> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of AuthorNames for query {}", query);
        Page<AuthorName> result = authorNameSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
