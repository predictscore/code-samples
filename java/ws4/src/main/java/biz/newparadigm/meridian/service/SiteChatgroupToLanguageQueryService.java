package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.SiteChatgroupToLanguage;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.SiteChatgroupToLanguageRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupToLanguageSearchRepository;
import biz.newparadigm.meridian.service.dto.SiteChatgroupToLanguageCriteria;


/**
 * Service for executing complex queries for SiteChatgroupToLanguage entities in the database.
 * The main input is a {@link SiteChatgroupToLanguageCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link SiteChatgroupToLanguage} or a {@link Page} of {%link SiteChatgroupToLanguage} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class SiteChatgroupToLanguageQueryService extends QueryService<SiteChatgroupToLanguage> {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupToLanguageQueryService.class);


    private final SiteChatgroupToLanguageRepository siteChatgroupToLanguageRepository;

    private final SiteChatgroupToLanguageSearchRepository siteChatgroupToLanguageSearchRepository;

    public SiteChatgroupToLanguageQueryService(SiteChatgroupToLanguageRepository siteChatgroupToLanguageRepository, SiteChatgroupToLanguageSearchRepository siteChatgroupToLanguageSearchRepository) {
        this.siteChatgroupToLanguageRepository = siteChatgroupToLanguageRepository;
        this.siteChatgroupToLanguageSearchRepository = siteChatgroupToLanguageSearchRepository;
    }

    /**
     * Return a {@link List} of {%link SiteChatgroupToLanguage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SiteChatgroupToLanguage> findByCriteria(SiteChatgroupToLanguageCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<SiteChatgroupToLanguage> specification = createSpecification(criteria);
        return siteChatgroupToLanguageRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link SiteChatgroupToLanguage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupToLanguage> findByCriteria(SiteChatgroupToLanguageCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<SiteChatgroupToLanguage> specification = createSpecification(criteria);
        return siteChatgroupToLanguageRepository.findAll(specification, page);
    }

    /**
     * Function to convert SiteChatgroupToLanguageCriteria to a {@link Specifications}
     */
    private Specifications<SiteChatgroupToLanguage> createSpecification(SiteChatgroupToLanguageCriteria criteria) {
        Specifications<SiteChatgroupToLanguage> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SiteChatgroupToLanguage_.id));
            }
            if (criteria.getSiteChatgroupId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSiteChatgroupId(), SiteChatgroupToLanguage_.siteChatgroup, SiteChatgroup_.id));
            }
            if (criteria.getLanguageId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getLanguageId(), SiteChatgroupToLanguage_.language, Language_.id));
            }
        }
        return specification;
    }

}
