package biz.newparadigm.meridian.domain.audit;

public class AuditChange {
    private AuditChangeType operation;
    private String fromValue;
    private String toValue;
    private String property;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public AuditChangeType getOperation() {
        return operation;
    }

    public void setOperation(AuditChangeType operation) {
        this.operation = operation;
    }

    public String getFromValue() {
        return fromValue;
    }

    public void setFromValue(String fromValue) {
        this.fromValue = fromValue;
    }

    public String getToValue() {
        return toValue;
    }

    public void setToValue(String toValue) {
        this.toValue = toValue;
    }
}
