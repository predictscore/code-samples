package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.SiteChatgroupToTag;
import biz.newparadigm.meridian.repository.SiteChatgroupToTagRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupToTagSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SiteChatgroupToTag.
 */
@Service
@Transactional
public class SiteChatgroupToTagService {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupToTagService.class);

    private final SiteChatgroupToTagRepository siteChatgroupToTagRepository;

    private final SiteChatgroupToTagSearchRepository siteChatgroupToTagSearchRepository;

    public SiteChatgroupToTagService(SiteChatgroupToTagRepository siteChatgroupToTagRepository, SiteChatgroupToTagSearchRepository siteChatgroupToTagSearchRepository) {
        this.siteChatgroupToTagRepository = siteChatgroupToTagRepository;
        this.siteChatgroupToTagSearchRepository = siteChatgroupToTagSearchRepository;
    }

    /**
     * Save a siteChatgroupToTag.
     *
     * @param siteChatgroupToTag the entity to save
     * @return the persisted entity
     */
    public SiteChatgroupToTag save(SiteChatgroupToTag siteChatgroupToTag) {
        log.debug("Request to save SiteChatgroupToTag : {}", siteChatgroupToTag);
        SiteChatgroupToTag result = siteChatgroupToTagRepository.save(siteChatgroupToTag);
        siteChatgroupToTagSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the siteChatgroupToTags.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupToTag> findAll(Pageable pageable) {
        log.debug("Request to get all SiteChatgroupToTags");
        return siteChatgroupToTagRepository.findAll(pageable);
    }

    /**
     *  Get one siteChatgroupToTag by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SiteChatgroupToTag findOne(Long id) {
        log.debug("Request to get SiteChatgroupToTag : {}", id);
        return siteChatgroupToTagRepository.findOne(id);
    }

    /**
     *  Delete the  siteChatgroupToTag by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SiteChatgroupToTag : {}", id);
        siteChatgroupToTagRepository.delete(id);
        siteChatgroupToTagSearchRepository.delete(id);
    }

    /**
     * Search for the siteChatgroupToTag corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupToTag> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SiteChatgroupToTags for query {}", query);
        Page<SiteChatgroupToTag> result = siteChatgroupToTagSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
