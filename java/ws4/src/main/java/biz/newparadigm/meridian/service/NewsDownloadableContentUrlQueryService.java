package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsDownloadableContentUrl;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsDownloadableContentUrlRepository;
import biz.newparadigm.meridian.repository.search.NewsDownloadableContentUrlSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsDownloadableContentUrlCriteria;


/**
 * Service for executing complex queries for NewsDownloadableContentUrl entities in the database.
 * The main input is a {@link NewsDownloadableContentUrlCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsDownloadableContentUrl} or a {@link Page} of {%link NewsDownloadableContentUrl} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsDownloadableContentUrlQueryService extends QueryService<NewsDownloadableContentUrl> {

    private final Logger log = LoggerFactory.getLogger(NewsDownloadableContentUrlQueryService.class);


    private final NewsDownloadableContentUrlRepository newsDownloadableContentUrlRepository;

    private final NewsDownloadableContentUrlSearchRepository newsDownloadableContentUrlSearchRepository;

    public NewsDownloadableContentUrlQueryService(NewsDownloadableContentUrlRepository newsDownloadableContentUrlRepository, NewsDownloadableContentUrlSearchRepository newsDownloadableContentUrlSearchRepository) {
        this.newsDownloadableContentUrlRepository = newsDownloadableContentUrlRepository;
        this.newsDownloadableContentUrlSearchRepository = newsDownloadableContentUrlSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsDownloadableContentUrl} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsDownloadableContentUrl> findByCriteria(NewsDownloadableContentUrlCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsDownloadableContentUrl> specification = createSpecification(criteria);
        return newsDownloadableContentUrlRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsDownloadableContentUrl} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsDownloadableContentUrl> findByCriteria(NewsDownloadableContentUrlCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsDownloadableContentUrl> specification = createSpecification(criteria);
        return newsDownloadableContentUrlRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsDownloadableContentUrlCriteria to a {@link Specifications}
     */
    private Specifications<NewsDownloadableContentUrl> createSpecification(NewsDownloadableContentUrlCriteria criteria) {
        Specifications<NewsDownloadableContentUrl> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsDownloadableContentUrl_.id));
            }
            if (criteria.getUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUrl(), NewsDownloadableContentUrl_.url));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), NewsDownloadableContentUrl_.description));
            }
            if (criteria.getNewsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsId(), NewsDownloadableContentUrl_.news, News_.id));
            }
        }
        return specification;
    }

}
