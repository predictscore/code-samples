package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.SiteChatgroupProfile;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SiteChatgroupProfile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SiteChatgroupProfileRepository extends JpaRepository<SiteChatgroupProfile, Long>, JpaSpecificationExecutor<SiteChatgroupProfile> {

}
