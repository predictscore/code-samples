package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsMetadata;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsMetadata entity.
 */
public interface NewsMetadataSearchRepository extends ElasticsearchRepository<NewsMetadata, Long> {
}
