package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.SiteChatgroupNumVisitors;
import biz.newparadigm.meridian.service.SiteChatgroupNumVisitorsService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.SiteChatgroupNumVisitorsCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupNumVisitorsQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SiteChatgroupNumVisitors.
 */
@RestController
@RequestMapping("/api")
public class SiteChatgroupNumVisitorsResource {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupNumVisitorsResource.class);

    private static final String ENTITY_NAME = "siteChatgroupNumVisitors";

    private final SiteChatgroupNumVisitorsService siteChatgroupNumVisitorsService;

    private final SiteChatgroupNumVisitorsQueryService siteChatgroupNumVisitorsQueryService;

    public SiteChatgroupNumVisitorsResource(SiteChatgroupNumVisitorsService siteChatgroupNumVisitorsService, SiteChatgroupNumVisitorsQueryService siteChatgroupNumVisitorsQueryService) {
        this.siteChatgroupNumVisitorsService = siteChatgroupNumVisitorsService;
        this.siteChatgroupNumVisitorsQueryService = siteChatgroupNumVisitorsQueryService;
    }

    /**
     * POST  /site-chatgroup-num-visitors : Create a new siteChatgroupNumVisitors.
     *
     * @param siteChatgroupNumVisitors the siteChatgroupNumVisitors to create
     * @return the ResponseEntity with status 201 (Created) and with body the new siteChatgroupNumVisitors, or with status 400 (Bad Request) if the siteChatgroupNumVisitors has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/site-chatgroup-num-visitors")
    @Timed
    public ResponseEntity<SiteChatgroupNumVisitors> createSiteChatgroupNumVisitors(@Valid @RequestBody SiteChatgroupNumVisitors siteChatgroupNumVisitors) throws URISyntaxException {
        log.debug("REST request to save SiteChatgroupNumVisitors : {}", siteChatgroupNumVisitors);
        if (siteChatgroupNumVisitors.getId() != null) {
            throw new BadRequestAlertException("A new siteChatgroupNumVisitors cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SiteChatgroupNumVisitors result = siteChatgroupNumVisitorsService.save(siteChatgroupNumVisitors);
        return ResponseEntity.created(new URI("/api/site-chatgroup-num-visitors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /site-chatgroup-num-visitors : Updates an existing siteChatgroupNumVisitors.
     *
     * @param siteChatgroupNumVisitors the siteChatgroupNumVisitors to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated siteChatgroupNumVisitors,
     * or with status 400 (Bad Request) if the siteChatgroupNumVisitors is not valid,
     * or with status 500 (Internal Server Error) if the siteChatgroupNumVisitors couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/site-chatgroup-num-visitors")
    @Timed
    public ResponseEntity<SiteChatgroupNumVisitors> updateSiteChatgroupNumVisitors(@Valid @RequestBody SiteChatgroupNumVisitors siteChatgroupNumVisitors) throws URISyntaxException {
        log.debug("REST request to update SiteChatgroupNumVisitors : {}", siteChatgroupNumVisitors);
        if (siteChatgroupNumVisitors.getId() == null) {
            return createSiteChatgroupNumVisitors(siteChatgroupNumVisitors);
        }
        SiteChatgroupNumVisitors result = siteChatgroupNumVisitorsService.save(siteChatgroupNumVisitors);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, siteChatgroupNumVisitors.getId().toString()))
            .body(result);
    }

    /**
     * GET  /site-chatgroup-num-visitors : get all the siteChatgroupNumVisitors.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of siteChatgroupNumVisitors in body
     */
    @GetMapping("/site-chatgroup-num-visitors")
    @Timed
    public ResponseEntity<List<SiteChatgroupNumVisitors>> getAllSiteChatgroupNumVisitors(SiteChatgroupNumVisitorsCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get SiteChatgroupNumVisitors by criteria: {}", criteria);
        Page<SiteChatgroupNumVisitors> page = siteChatgroupNumVisitorsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/site-chatgroup-num-visitors");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /site-chatgroup-num-visitors/:id : get the "id" siteChatgroupNumVisitors.
     *
     * @param id the id of the siteChatgroupNumVisitors to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the siteChatgroupNumVisitors, or with status 404 (Not Found)
     */
    @GetMapping("/site-chatgroup-num-visitors/{id}")
    @Timed
    public ResponseEntity<SiteChatgroupNumVisitors> getSiteChatgroupNumVisitors(@PathVariable Long id) {
        log.debug("REST request to get SiteChatgroupNumVisitors : {}", id);
        SiteChatgroupNumVisitors siteChatgroupNumVisitors = siteChatgroupNumVisitorsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(siteChatgroupNumVisitors));
    }

    /**
     * DELETE  /site-chatgroup-num-visitors/:id : delete the "id" siteChatgroupNumVisitors.
     *
     * @param id the id of the siteChatgroupNumVisitors to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/site-chatgroup-num-visitors/{id}")
    @Timed
    public ResponseEntity<Void> deleteSiteChatgroupNumVisitors(@PathVariable Long id) {
        log.debug("REST request to delete SiteChatgroupNumVisitors : {}", id);
        siteChatgroupNumVisitorsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/site-chatgroup-num-visitors?query=:query : search for the siteChatgroupNumVisitors corresponding
     * to the query.
     *
     * @param query the query of the siteChatgroupNumVisitors search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/site-chatgroup-num-visitors")
    @Timed
    public ResponseEntity<List<SiteChatgroupNumVisitors>> searchSiteChatgroupNumVisitors(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SiteChatgroupNumVisitors for query {}", query);
        Page<SiteChatgroupNumVisitors> page = siteChatgroupNumVisitorsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/site-chatgroup-num-visitors");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
