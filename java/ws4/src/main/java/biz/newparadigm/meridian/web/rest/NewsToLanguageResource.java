package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsToLanguage;
import biz.newparadigm.meridian.service.NewsToLanguageService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsToLanguageCriteria;
import biz.newparadigm.meridian.service.NewsToLanguageQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsToLanguage.
 */
@RestController
@RequestMapping("/api")
public class NewsToLanguageResource {

    private final Logger log = LoggerFactory.getLogger(NewsToLanguageResource.class);

    private static final String ENTITY_NAME = "newsToLanguage";

    private final NewsToLanguageService newsToLanguageService;

    private final NewsToLanguageQueryService newsToLanguageQueryService;

    public NewsToLanguageResource(NewsToLanguageService newsToLanguageService, NewsToLanguageQueryService newsToLanguageQueryService) {
        this.newsToLanguageService = newsToLanguageService;
        this.newsToLanguageQueryService = newsToLanguageQueryService;
    }

    /**
     * POST  /news-to-languages : Create a new newsToLanguage.
     *
     * @param newsToLanguage the newsToLanguage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsToLanguage, or with status 400 (Bad Request) if the newsToLanguage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-to-languages")
    @Timed
    public ResponseEntity<NewsToLanguage> createNewsToLanguage(@Valid @RequestBody NewsToLanguage newsToLanguage) throws URISyntaxException {
        log.debug("REST request to save NewsToLanguage : {}", newsToLanguage);
        if (newsToLanguage.getId() != null) {
            throw new BadRequestAlertException("A new newsToLanguage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsToLanguage result = newsToLanguageService.save(newsToLanguage);
        return ResponseEntity.created(new URI("/api/news-to-languages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-to-languages : Updates an existing newsToLanguage.
     *
     * @param newsToLanguage the newsToLanguage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsToLanguage,
     * or with status 400 (Bad Request) if the newsToLanguage is not valid,
     * or with status 500 (Internal Server Error) if the newsToLanguage couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-to-languages")
    @Timed
    public ResponseEntity<NewsToLanguage> updateNewsToLanguage(@Valid @RequestBody NewsToLanguage newsToLanguage) throws URISyntaxException {
        log.debug("REST request to update NewsToLanguage : {}", newsToLanguage);
        if (newsToLanguage.getId() == null) {
            return createNewsToLanguage(newsToLanguage);
        }
        NewsToLanguage result = newsToLanguageService.save(newsToLanguage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsToLanguage.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-to-languages : get all the newsToLanguages.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsToLanguages in body
     */
    @GetMapping("/news-to-languages")
    @Timed
    public ResponseEntity<List<NewsToLanguage>> getAllNewsToLanguages(NewsToLanguageCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsToLanguages by criteria: {}", criteria);
        Page<NewsToLanguage> page = newsToLanguageQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-to-languages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-to-languages/:id : get the "id" newsToLanguage.
     *
     * @param id the id of the newsToLanguage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsToLanguage, or with status 404 (Not Found)
     */
    @GetMapping("/news-to-languages/{id}")
    @Timed
    public ResponseEntity<NewsToLanguage> getNewsToLanguage(@PathVariable Long id) {
        log.debug("REST request to get NewsToLanguage : {}", id);
        NewsToLanguage newsToLanguage = newsToLanguageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsToLanguage));
    }

    /**
     * DELETE  /news-to-languages/:id : delete the "id" newsToLanguage.
     *
     * @param id the id of the newsToLanguage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-to-languages/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsToLanguage(@PathVariable Long id) {
        log.debug("REST request to delete NewsToLanguage : {}", id);
        newsToLanguageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-to-languages?query=:query : search for the newsToLanguage corresponding
     * to the query.
     *
     * @param query the query of the newsToLanguage search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-to-languages")
    @Timed
    public ResponseEntity<List<NewsToLanguage>> searchNewsToLanguages(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsToLanguages for query {}", query);
        Page<NewsToLanguage> page = newsToLanguageService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-to-languages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
