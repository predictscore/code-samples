package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.SiteChatgroupName;
import biz.newparadigm.meridian.repository.SiteChatgroupNameRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupNameSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SiteChatgroupName.
 */
@Service
@Transactional
public class SiteChatgroupNameService {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupNameService.class);

    private final SiteChatgroupNameRepository siteChatgroupNameRepository;

    private final SiteChatgroupNameSearchRepository siteChatgroupNameSearchRepository;

    public SiteChatgroupNameService(SiteChatgroupNameRepository siteChatgroupNameRepository, SiteChatgroupNameSearchRepository siteChatgroupNameSearchRepository) {
        this.siteChatgroupNameRepository = siteChatgroupNameRepository;
        this.siteChatgroupNameSearchRepository = siteChatgroupNameSearchRepository;
    }

    /**
     * Save a siteChatgroupName.
     *
     * @param siteChatgroupName the entity to save
     * @return the persisted entity
     */
    public SiteChatgroupName save(SiteChatgroupName siteChatgroupName) {
        log.debug("Request to save SiteChatgroupName : {}", siteChatgroupName);
        SiteChatgroupName result = siteChatgroupNameRepository.save(siteChatgroupName);
        siteChatgroupNameSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the siteChatgroupNames.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupName> findAll(Pageable pageable) {
        log.debug("Request to get all SiteChatgroupNames");
        return siteChatgroupNameRepository.findAll(pageable);
    }

    /**
     *  Get one siteChatgroupName by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SiteChatgroupName findOne(Long id) {
        log.debug("Request to get SiteChatgroupName : {}", id);
        return siteChatgroupNameRepository.findOne(id);
    }

    /**
     *  Delete the  siteChatgroupName by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SiteChatgroupName : {}", id);
        siteChatgroupNameRepository.delete(id);
        siteChatgroupNameSearchRepository.delete(id);
    }

    /**
     * Search for the siteChatgroupName corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupName> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SiteChatgroupNames for query {}", query);
        Page<SiteChatgroupName> result = siteChatgroupNameSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
