package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.Language;
import biz.newparadigm.meridian.repository.LanguageRepository;
import biz.newparadigm.meridian.repository.search.LanguageSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Language.
 */
@Service
@Transactional
public class LanguageService {

    private final Logger log = LoggerFactory.getLogger(LanguageService.class);

    private final LanguageRepository languageRepository;

    private final LanguageSearchRepository languageSearchRepository;

    public LanguageService(LanguageRepository languageRepository, LanguageSearchRepository languageSearchRepository) {
        this.languageRepository = languageRepository;
        this.languageSearchRepository = languageSearchRepository;
    }

    /**
     * Save a language.
     *
     * @param language the entity to save
     * @return the persisted entity
     */
    public Language save(Language language) {
        log.debug("Request to save Language : {}", language);
        Language result = languageRepository.save(language);
        languageSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the languages.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Language> findAll(Pageable pageable) {
        log.debug("Request to get all Languages");
        return languageRepository.findAll(pageable);
    }

    /**
     *  Get one language by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Language findOne(Long id) {
        log.debug("Request to get Language : {}", id);
        return languageRepository.findOne(id);
    }

    /**
     *  Delete the  language by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Language : {}", id);
        languageRepository.delete(id);
        languageSearchRepository.delete(id);
    }

    /**
     * Search for the language corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Language> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Languages for query {}", query);
        Page<Language> result = languageSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
