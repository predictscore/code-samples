package biz.newparadigm.meridian.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Metafield.
 */
@Entity
@Table(name = "metafields")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "metafield")
public class Metafield implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="metafields_id_seq")
    @SequenceGenerator(name="metafields_id_seq", sequenceName="metafields_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(optional = false)
    @NotNull
    private Metagroup metagroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Metafield name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Metagroup getMetagroup() {
        return metagroup;
    }

    public Metafield metagroup(Metagroup metagroup) {
        this.metagroup = metagroup;
        return this;
    }

    public void setMetagroup(Metagroup metagroup) {
        this.metagroup = metagroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Metafield metafield = (Metafield) o;
        if (metafield.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), metafield.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Metafield{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
