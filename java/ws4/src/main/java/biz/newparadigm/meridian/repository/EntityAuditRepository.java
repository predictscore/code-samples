package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.EntityAudit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface EntityAuditRepository extends JpaRepository<EntityAudit, Long>, JpaSpecificationExecutor<EntityAudit> {
}
