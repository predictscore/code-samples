package biz.newparadigm.meridian.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "site_chatgroups")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "sitechatgroup")
public class SiteChatgroup extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="site_chatgroups_id_seq")
    @SequenceGenerator(name="site_chatgroups_id_seq", sequenceName="site_chatgroups_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "is_site", nullable = false)
    private Boolean isSite;

    @NotNull
    @Column(name = "original_name", nullable = false)
    private String originalName;

    @NotNull
    @Column(name = "english_name", nullable = false)
    private String englishName;

    @ManyToOne(optional = false)
    @NotNull
    private Location location;

    @OneToMany(mappedBy = "siteChatgroup", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<SiteChatgroupDomain> domains = new ArrayList<>();

    @OneToMany(mappedBy = "siteChatgroup", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<SiteChatgroupIp> ips = new ArrayList<>();

    @OneToMany(mappedBy = "siteChatgroup", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<SiteChatgroupName> names = new ArrayList<>();

    @OneToMany(mappedBy = "siteChatgroup", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<SiteChatgroupNumVisitors> numVisitors = new ArrayList<>();

    @OneToMany(mappedBy = "siteChatgroup", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<SiteChatgroupNumUsers> numUsers = new ArrayList<>();

    @OneToMany(mappedBy = "siteChatgroup", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<SiteChatgroupToLanguage> languages = new ArrayList<>();

    @OneToMany(mappedBy = "siteChatgroup", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<SiteChatgroupToTag> tags = new ArrayList<>();

    @OneToMany(mappedBy = "siteChatgroup", cascade = CascadeType.MERGE, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<AuthorToSiteChatgroup> authors = new ArrayList<>();

    @Version
    private long version = 0L;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsSite() {
        return isSite;
    }

    public SiteChatgroup isSite(Boolean isSite) {
        this.isSite = isSite;
        return this;
    }

    public void setIsSite(Boolean isSite) {
        this.isSite = isSite;
    }

    public String getOriginalName() {
        return originalName;
    }

    public SiteChatgroup originalName(String originalName) {
        this.originalName = originalName;
        return this;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public SiteChatgroup englishName(String englishName) {
        this.englishName = englishName;
        return this;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public Location getLocation() {
        return location;
    }

    public SiteChatgroup location(Location location) {
        this.location = location;
        return this;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<SiteChatgroupDomain> getDomains() {
        return domains;
    }

    public SiteChatgroup domains(List<SiteChatgroupDomain> siteChatgroupDomains) {
        this.domains = siteChatgroupDomains;
        return this;
    }

    public SiteChatgroup addDomains(SiteChatgroupDomain siteChatgroupDomain) {
        this.domains.add(siteChatgroupDomain);
        siteChatgroupDomain.setSiteChatgroup(this);
        return this;
    }

    public SiteChatgroup removeDomains(SiteChatgroupDomain siteChatgroupDomain) {
        this.domains.remove(siteChatgroupDomain);
        siteChatgroupDomain.setSiteChatgroup(null);
        return this;
    }

    public void setDomains(List<SiteChatgroupDomain> siteChatgroupDomains) {
        this.domains = siteChatgroupDomains;
    }

    public List<SiteChatgroupIp> getIps() {
        return ips;
    }

    public SiteChatgroup ips(List<SiteChatgroupIp> siteChatgroupIps) {
        this.ips = siteChatgroupIps;
        return this;
    }

    public SiteChatgroup addIps(SiteChatgroupIp siteChatgroupIp) {
        this.ips.add(siteChatgroupIp);
        siteChatgroupIp.setSiteChatgroup(this);
        return this;
    }

    public SiteChatgroup removeIps(SiteChatgroupIp siteChatgroupIp) {
        this.ips.remove(siteChatgroupIp);
        siteChatgroupIp.setSiteChatgroup(null);
        return this;
    }

    public void setIps(List<SiteChatgroupIp> siteChatgroupIps) {
        this.ips = siteChatgroupIps;
    }

    public List<SiteChatgroupName> getNames() {
        return names;
    }

    public SiteChatgroup names(List<SiteChatgroupName> siteChatgroupNames) {
        this.names = siteChatgroupNames;
        return this;
    }

    public SiteChatgroup addNames(SiteChatgroupName siteChatgroupName) {
        this.names.add(siteChatgroupName);
        siteChatgroupName.setSiteChatgroup(this);
        return this;
    }

    public SiteChatgroup removeNames(SiteChatgroupName siteChatgroupName) {
        this.names.remove(siteChatgroupName);
        siteChatgroupName.setSiteChatgroup(null);
        return this;
    }

    public void setNames(List<SiteChatgroupName> siteChatgroupNames) {
        this.names = siteChatgroupNames;
    }

    public List<SiteChatgroupNumVisitors> getNumVisitors() {
        return numVisitors;
    }

    public SiteChatgroup numVisitors(List<SiteChatgroupNumVisitors> siteChatgroupNumVisitors) {
        this.numVisitors = siteChatgroupNumVisitors;
        return this;
    }

    public SiteChatgroup addNumVisitors(SiteChatgroupNumVisitors siteChatgroupNumVisitors) {
        this.numVisitors.add(siteChatgroupNumVisitors);
        siteChatgroupNumVisitors.setSiteChatgroup(this);
        return this;
    }

    public SiteChatgroup removeNumVisitors(SiteChatgroupNumVisitors siteChatgroupNumVisitors) {
        this.numVisitors.remove(siteChatgroupNumVisitors);
        siteChatgroupNumVisitors.setSiteChatgroup(null);
        return this;
    }

    public void setNumVisitors(List<SiteChatgroupNumVisitors> siteChatgroupNumVisitors) {
        this.numVisitors = siteChatgroupNumVisitors;
    }

    public List<SiteChatgroupNumUsers> getNumUsers() {
        return numUsers;
    }

    public SiteChatgroup numUsers(List<SiteChatgroupNumUsers> siteChatgroupNumUsers) {
        this.numUsers = siteChatgroupNumUsers;
        return this;
    }

    public SiteChatgroup addNumUsers(SiteChatgroupNumUsers siteChatgroupNumUsers) {
        this.numUsers.add(siteChatgroupNumUsers);
        siteChatgroupNumUsers.setSiteChatgroup(this);
        return this;
    }

    public SiteChatgroup removeNumUsers(SiteChatgroupNumUsers siteChatgroupNumUsers) {
        this.numUsers.remove(siteChatgroupNumUsers);
        siteChatgroupNumUsers.setSiteChatgroup(null);
        return this;
    }

    public void setNumUsers(List<SiteChatgroupNumUsers> siteChatgroupNumUsers) {
        this.numUsers = siteChatgroupNumUsers;
    }

    public List<SiteChatgroupToLanguage> getLanguages() {
        return languages;
    }

    public SiteChatgroup languages(List<SiteChatgroupToLanguage> siteChatgroupToLanguages) {
        this.languages = siteChatgroupToLanguages;
        return this;
    }

    public SiteChatgroup addLanguages(SiteChatgroupToLanguage siteChatgroupToLanguage) {
        this.languages.add(siteChatgroupToLanguage);
        siteChatgroupToLanguage.setSiteChatgroup(this);
        return this;
    }

    public SiteChatgroup removeLanguages(SiteChatgroupToLanguage siteChatgroupToLanguage) {
        this.languages.remove(siteChatgroupToLanguage);
        siteChatgroupToLanguage.setSiteChatgroup(null);
        return this;
    }

    public void setLanguages(List<SiteChatgroupToLanguage> siteChatgroupToLanguages) {
        this.languages = siteChatgroupToLanguages;
    }

    public List<SiteChatgroupToTag> getTags() {
        return tags;
    }

    public SiteChatgroup tags(List<SiteChatgroupToTag> siteChatgroupToTags) {
        this.tags = siteChatgroupToTags;
        return this;
    }

    public SiteChatgroup addTags(SiteChatgroupToTag siteChatgroupToTag) {
        this.tags.add(siteChatgroupToTag);
        siteChatgroupToTag.setSiteChatgroup(this);
        return this;
    }

    public SiteChatgroup removeTags(SiteChatgroupToTag siteChatgroupToTag) {
        this.tags.remove(siteChatgroupToTag);
        siteChatgroupToTag.setSiteChatgroup(null);
        return this;
    }

    public void setTags(List<SiteChatgroupToTag> siteChatgroupToTags) {
        this.tags = siteChatgroupToTags;
    }

    public List<AuthorToSiteChatgroup> getAuthors() {
        return authors;
    }

    public SiteChatgroup authors(List<AuthorToSiteChatgroup> authorToSiteChatgroups) {
        this.authors = authorToSiteChatgroups;
        return this;
    }

    public SiteChatgroup addAuthors(AuthorToSiteChatgroup authorToSiteChatgroup) {
        this.authors.add(authorToSiteChatgroup);
        authorToSiteChatgroup.setSiteChatgroup(this);
        return this;
    }

    public SiteChatgroup removeAuthors(AuthorToSiteChatgroup authorToSiteChatgroup) {
        this.authors.remove(authorToSiteChatgroup);
        authorToSiteChatgroup.setSiteChatgroup(null);
        return this;
    }

    public void setAuthors(List<AuthorToSiteChatgroup> authorToSiteChatgroups) {
        this.authors = authorToSiteChatgroups;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public SiteChatgroup toJPASimpleObject() {
        SiteChatgroup siteChatgroup = new SiteChatgroup();
        siteChatgroup.setId(this.getId());
        return siteChatgroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SiteChatgroup siteChatgroup = (SiteChatgroup) o;
        if (siteChatgroup.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), siteChatgroup.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SiteChatgroup{" +
            "id=" + getId() +
            ", isSite='" + isIsSite() + "'" +
            ", originalName='" + getOriginalName() + "'" +
            ", englishName='" + getEnglishName() + "'" +
            "}";
    }
}
