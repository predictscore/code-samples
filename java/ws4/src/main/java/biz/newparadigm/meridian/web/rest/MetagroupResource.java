package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.Metagroup;
import biz.newparadigm.meridian.service.MetagroupService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.MetagroupCriteria;
import biz.newparadigm.meridian.service.MetagroupQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Metagroup.
 */
@RestController
@RequestMapping("/api")
public class MetagroupResource {

    private final Logger log = LoggerFactory.getLogger(MetagroupResource.class);

    private static final String ENTITY_NAME = "metagroup";

    private final MetagroupService metagroupService;

    private final MetagroupQueryService metagroupQueryService;

    public MetagroupResource(MetagroupService metagroupService, MetagroupQueryService metagroupQueryService) {
        this.metagroupService = metagroupService;
        this.metagroupQueryService = metagroupQueryService;
    }

    /**
     * POST  /metagroups : Create a new metagroup.
     *
     * @param metagroup the metagroup to create
     * @return the ResponseEntity with status 201 (Created) and with body the new metagroup, or with status 400 (Bad Request) if the metagroup has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/metagroups")
    @Timed
    public ResponseEntity<Metagroup> createMetagroup(@Valid @RequestBody Metagroup metagroup) throws URISyntaxException {
        log.debug("REST request to save Metagroup : {}", metagroup);
        if (metagroup.getId() != null) {
            throw new BadRequestAlertException("A new metagroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Metagroup result = metagroupService.save(metagroup);
        return ResponseEntity.created(new URI("/api/metagroups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /metagroups : Updates an existing metagroup.
     *
     * @param metagroup the metagroup to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated metagroup,
     * or with status 400 (Bad Request) if the metagroup is not valid,
     * or with status 500 (Internal Server Error) if the metagroup couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/metagroups")
    @Timed
    public ResponseEntity<Metagroup> updateMetagroup(@Valid @RequestBody Metagroup metagroup) throws URISyntaxException {
        log.debug("REST request to update Metagroup : {}", metagroup);
        if (metagroup.getId() == null) {
            return createMetagroup(metagroup);
        }
        Metagroup result = metagroupService.save(metagroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, metagroup.getId().toString()))
            .body(result);
    }

    /**
     * GET  /metagroups : get all the metagroups.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of metagroups in body
     */
    @GetMapping("/metagroups")
    @Timed
    public ResponseEntity<List<Metagroup>> getAllMetagroups(MetagroupCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get Metagroups by criteria: {}", criteria);
        Page<Metagroup> page = metagroupQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/metagroups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /metagroups/:id : get the "id" metagroup.
     *
     * @param id the id of the metagroup to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the metagroup, or with status 404 (Not Found)
     */
    @GetMapping("/metagroups/{id}")
    @Timed
    public ResponseEntity<Metagroup> getMetagroup(@PathVariable Long id) {
        log.debug("REST request to get Metagroup : {}", id);
        Metagroup metagroup = metagroupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(metagroup));
    }

    /**
     * DELETE  /metagroups/:id : delete the "id" metagroup.
     *
     * @param id the id of the metagroup to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/metagroups/{id}")
    @Timed
    public ResponseEntity<Void> deleteMetagroup(@PathVariable Long id) {
        log.debug("REST request to delete Metagroup : {}", id);
        metagroupService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/metagroups?query=:query : search for the metagroup corresponding
     * to the query.
     *
     * @param query the query of the metagroup search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/metagroups")
    @Timed
    public ResponseEntity<List<Metagroup>> searchMetagroups(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Metagroups for query {}", query);
        Page<Metagroup> page = metagroupService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/metagroups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
