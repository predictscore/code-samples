package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.SiteChatgroupIp;
import biz.newparadigm.meridian.repository.SiteChatgroupIpRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupIpSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing SiteChatgroupIp.
 */
@Service
@Transactional
public class SiteChatgroupIpService {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupIpService.class);

    private final SiteChatgroupIpRepository siteChatgroupIpRepository;

    private final SiteChatgroupIpSearchRepository siteChatgroupIpSearchRepository;

    public SiteChatgroupIpService(SiteChatgroupIpRepository siteChatgroupIpRepository, SiteChatgroupIpSearchRepository siteChatgroupIpSearchRepository) {
        this.siteChatgroupIpRepository = siteChatgroupIpRepository;
        this.siteChatgroupIpSearchRepository = siteChatgroupIpSearchRepository;
    }

    /**
     * Save a siteChatgroupIp.
     *
     * @param siteChatgroupIp the entity to save
     * @return the persisted entity
     */
    public SiteChatgroupIp save(SiteChatgroupIp siteChatgroupIp) {
        log.debug("Request to save SiteChatgroupIp : {}", siteChatgroupIp);
        SiteChatgroupIp result = siteChatgroupIpRepository.save(siteChatgroupIp);
        // ssiteChatgroupIpSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the siteChatgroupIps.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupIp> findAll(Pageable pageable) {
        log.debug("Request to get all SiteChatgroupIps");
        return siteChatgroupIpRepository.findAll(pageable);
    }

    /**
     *  Get one siteChatgroupIp by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public SiteChatgroupIp findOne(Long id) {
        log.debug("Request to get SiteChatgroupIp : {}", id);
        return siteChatgroupIpRepository.findOne(id);
    }

    /**
     *  Delete the  siteChatgroupIp by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SiteChatgroupIp : {}", id);
        siteChatgroupIpRepository.delete(id);
        siteChatgroupIpSearchRepository.delete(id);
    }

    /**
     * Search for the siteChatgroupIp corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupIp> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of SiteChatgroupIps for query {}", query);
        Page<SiteChatgroupIp> result = siteChatgroupIpSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
