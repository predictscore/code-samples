package biz.newparadigm.meridian.domain;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "site_chatgroup_to_languages")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "sitechatgrouptolanguage")
public class SiteChatgroupToLanguage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="site_chatgroup_to_languages_id_seq")
    @SequenceGenerator(name="site_chatgroup_to_languages_id_seq", sequenceName="site_chatgroup_to_languages_id_seq", allocationSize=1)
    private Long id;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"comments", "domains", "ips", "names", "infos", "numVisitors", "numUsers", "languages", "tags", "authors"})
    private SiteChatgroup siteChatgroup;

    @ManyToOne(optional = false)
    @NotNull
    private Language language;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SiteChatgroup getSiteChatgroup() {
        return siteChatgroup;
    }

    public SiteChatgroupToLanguage siteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
        return this;
    }

    public void setSiteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
    }

    public Language getLanguage() {
        return language;
    }

    public SiteChatgroupToLanguage language(Language language) {
        this.language = language;
        return this;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SiteChatgroupToLanguage siteChatgroupToLanguage = (SiteChatgroupToLanguage) o;
        if (siteChatgroupToLanguage.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), siteChatgroupToLanguage.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SiteChatgroupToLanguage{" +
            "id=" + getId() +
            "}";
    }
}
