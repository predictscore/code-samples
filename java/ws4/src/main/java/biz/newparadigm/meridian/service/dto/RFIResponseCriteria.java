package biz.newparadigm.meridian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the RFIResponse entity. This class is used in RFIResponseResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /r-fi-responses?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RFIResponseCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter note;

    private StringFilter username;

    private ZonedDateTimeFilter submissionDate;

    public RFIResponseCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNote() {
        return note;
    }

    public void setNote(StringFilter note) {
        this.note = note;
    }

    public StringFilter getUsername() {
        return username;
    }

    public void setUsername(StringFilter username) {
        this.username = username;
    }

    public ZonedDateTimeFilter getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(ZonedDateTimeFilter submissionDate) {
        this.submissionDate = submissionDate;
    }

    @Override
    public String toString() {
        return "RFIResponseCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (note != null ? "note=" + note + ", " : "") +
                (username != null ? "username=" + username + ", " : "") +
                (submissionDate != null ? "submissionDate=" + submissionDate + ", " : "") +
            "}";
    }

}
