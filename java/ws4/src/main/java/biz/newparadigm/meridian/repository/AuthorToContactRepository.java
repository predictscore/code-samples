package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.AuthorToContact;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

@Repository
public interface AuthorToContactRepository extends JpaRepository<AuthorToContact, Long>, JpaSpecificationExecutor<AuthorToContact> {
    @Modifying
    @Query("DELETE FROM AuthorToContact a where a.author.id = :authorId")
    void deleteAllByAuthorId(@Param(value = "authorId") Long authorId);
}
