package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.SiteChatgroupDomain;
import biz.newparadigm.meridian.service.SiteChatgroupDomainService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.SiteChatgroupDomainCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupDomainQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SiteChatgroupDomain.
 */
@RestController
@RequestMapping("/api")
public class SiteChatgroupDomainResource {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupDomainResource.class);

    private static final String ENTITY_NAME = "siteChatgroupDomain";

    private final SiteChatgroupDomainService siteChatgroupDomainService;

    private final SiteChatgroupDomainQueryService siteChatgroupDomainQueryService;

    public SiteChatgroupDomainResource(SiteChatgroupDomainService siteChatgroupDomainService, SiteChatgroupDomainQueryService siteChatgroupDomainQueryService) {
        this.siteChatgroupDomainService = siteChatgroupDomainService;
        this.siteChatgroupDomainQueryService = siteChatgroupDomainQueryService;
    }

    /**
     * POST  /site-chatgroup-domains : Create a new siteChatgroupDomain.
     *
     * @param siteChatgroupDomain the siteChatgroupDomain to create
     * @return the ResponseEntity with status 201 (Created) and with body the new siteChatgroupDomain, or with status 400 (Bad Request) if the siteChatgroupDomain has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/site-chatgroup-domains")
    @Timed
    public ResponseEntity<SiteChatgroupDomain> createSiteChatgroupDomain(@Valid @RequestBody SiteChatgroupDomain siteChatgroupDomain) throws URISyntaxException {
        log.debug("REST request to save SiteChatgroupDomain : {}", siteChatgroupDomain);
        if (siteChatgroupDomain.getId() != null) {
            throw new BadRequestAlertException("A new siteChatgroupDomain cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SiteChatgroupDomain result = siteChatgroupDomainService.save(siteChatgroupDomain);
        return ResponseEntity.created(new URI("/api/site-chatgroup-domains/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /site-chatgroup-domains : Updates an existing siteChatgroupDomain.
     *
     * @param siteChatgroupDomain the siteChatgroupDomain to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated siteChatgroupDomain,
     * or with status 400 (Bad Request) if the siteChatgroupDomain is not valid,
     * or with status 500 (Internal Server Error) if the siteChatgroupDomain couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/site-chatgroup-domains")
    @Timed
    public ResponseEntity<SiteChatgroupDomain> updateSiteChatgroupDomain(@Valid @RequestBody SiteChatgroupDomain siteChatgroupDomain) throws URISyntaxException {
        log.debug("REST request to update SiteChatgroupDomain : {}", siteChatgroupDomain);
        if (siteChatgroupDomain.getId() == null) {
            return createSiteChatgroupDomain(siteChatgroupDomain);
        }
        SiteChatgroupDomain result = siteChatgroupDomainService.save(siteChatgroupDomain);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, siteChatgroupDomain.getId().toString()))
            .body(result);
    }

    /**
     * GET  /site-chatgroup-domains : get all the siteChatgroupDomains.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of siteChatgroupDomains in body
     */
    @GetMapping("/site-chatgroup-domains")
    @Timed
    public ResponseEntity<List<SiteChatgroupDomain>> getAllSiteChatgroupDomains(SiteChatgroupDomainCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get SiteChatgroupDomains by criteria: {}", criteria);
        Page<SiteChatgroupDomain> page = siteChatgroupDomainQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/site-chatgroup-domains");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /site-chatgroup-domains/:id : get the "id" siteChatgroupDomain.
     *
     * @param id the id of the siteChatgroupDomain to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the siteChatgroupDomain, or with status 404 (Not Found)
     */
    @GetMapping("/site-chatgroup-domains/{id}")
    @Timed
    public ResponseEntity<SiteChatgroupDomain> getSiteChatgroupDomain(@PathVariable Long id) {
        log.debug("REST request to get SiteChatgroupDomain : {}", id);
        SiteChatgroupDomain siteChatgroupDomain = siteChatgroupDomainService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(siteChatgroupDomain));
    }

    /**
     * DELETE  /site-chatgroup-domains/:id : delete the "id" siteChatgroupDomain.
     *
     * @param id the id of the siteChatgroupDomain to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/site-chatgroup-domains/{id}")
    @Timed
    public ResponseEntity<Void> deleteSiteChatgroupDomain(@PathVariable Long id) {
        log.debug("REST request to delete SiteChatgroupDomain : {}", id);
        siteChatgroupDomainService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/site-chatgroup-domains?query=:query : search for the siteChatgroupDomain corresponding
     * to the query.
     *
     * @param query the query of the siteChatgroupDomain search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/site-chatgroup-domains")
    @Timed
    public ResponseEntity<List<SiteChatgroupDomain>> searchSiteChatgroupDomains(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SiteChatgroupDomains for query {}", query);
        Page<SiteChatgroupDomain> page = siteChatgroupDomainService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/site-chatgroup-domains");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
