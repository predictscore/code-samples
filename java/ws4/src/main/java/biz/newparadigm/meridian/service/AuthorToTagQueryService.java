package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.AuthorToTag;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.AuthorToTagRepository;
import biz.newparadigm.meridian.repository.search.AuthorToTagSearchRepository;
import biz.newparadigm.meridian.service.dto.AuthorToTagCriteria;


/**
 * Service for executing complex queries for AuthorToTag entities in the database.
 * The main input is a {@link AuthorToTagCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link AuthorToTag} or a {@link Page} of {%link AuthorToTag} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class AuthorToTagQueryService extends QueryService<AuthorToTag> {

    private final Logger log = LoggerFactory.getLogger(AuthorToTagQueryService.class);


    private final AuthorToTagRepository authorToTagRepository;

    private final AuthorToTagSearchRepository authorToTagSearchRepository;

    public AuthorToTagQueryService(AuthorToTagRepository authorToTagRepository, AuthorToTagSearchRepository authorToTagSearchRepository) {
        this.authorToTagRepository = authorToTagRepository;
        this.authorToTagSearchRepository = authorToTagSearchRepository;
    }

    /**
     * Return a {@link List} of {%link AuthorToTag} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuthorToTag> findByCriteria(AuthorToTagCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<AuthorToTag> specification = createSpecification(criteria);
        return authorToTagRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link AuthorToTag} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuthorToTag> findByCriteria(AuthorToTagCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<AuthorToTag> specification = createSpecification(criteria);
        return authorToTagRepository.findAll(specification, page);
    }

    /**
     * Function to convert AuthorToTagCriteria to a {@link Specifications}
     */
    private Specifications<AuthorToTag> createSpecification(AuthorToTagCriteria criteria) {
        Specifications<AuthorToTag> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AuthorToTag_.id));
            }
            if (criteria.getAuthorId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getAuthorId(), AuthorToTag_.author, Author_.id));
            }
            if (criteria.getTagId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getTagId(), AuthorToTag_.tag, Tag_.id));
            }
        }
        return specification;
    }

}
