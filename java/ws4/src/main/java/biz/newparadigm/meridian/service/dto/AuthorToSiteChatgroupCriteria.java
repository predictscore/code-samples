package biz.newparadigm.meridian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the AuthorToSiteChatgroup entity. This class is used in AuthorToSiteChatgroupResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /author-to-site-chatgroups?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AuthorToSiteChatgroupCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private ZonedDateTimeFilter fromDate;

    private StringFilter originalName;

    private StringFilter englishName;

    private StringFilter profileLink;

    private ZonedDateTimeFilter toDate;

    private StringFilter staffFunction;

    private LongFilter authorId;

    private LongFilter siteChatgroupId;

    public AuthorToSiteChatgroupCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public ZonedDateTimeFilter getFromDate() {
        return fromDate;
    }

    public void setFromDate(ZonedDateTimeFilter fromDate) {
        this.fromDate = fromDate;
    }

    public StringFilter getOriginalName() {
        return originalName;
    }

    public void setOriginalName(StringFilter originalName) {
        this.originalName = originalName;
    }

    public StringFilter getEnglishName() {
        return englishName;
    }

    public void setEnglishName(StringFilter englishName) {
        this.englishName = englishName;
    }

    public StringFilter getProfileLink() {
        return profileLink;
    }

    public void setProfileLink(StringFilter profileLink) {
        this.profileLink = profileLink;
    }

    public ZonedDateTimeFilter getToDate() {
        return toDate;
    }

    public void setToDate(ZonedDateTimeFilter toDate) {
        this.toDate = toDate;
    }

    public StringFilter getStaffFunction() {
        return staffFunction;
    }

    public void setStaffFunction(StringFilter staffFunction) {
        this.staffFunction = staffFunction;
    }

    public LongFilter getAuthorId() {
        return authorId;
    }

    public void setAuthorId(LongFilter authorId) {
        this.authorId = authorId;
    }

    public LongFilter getSiteChatgroupId() {
        return siteChatgroupId;
    }

    public void setSiteChatgroupId(LongFilter siteChatgroupId) {
        this.siteChatgroupId = siteChatgroupId;
    }

    @Override
    public String toString() {
        return "AuthorToSiteChatgroupCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (fromDate != null ? "fromDate=" + fromDate + ", " : "") +
                (originalName != null ? "originalName=" + originalName + ", " : "") +
                (englishName != null ? "englishName=" + englishName + ", " : "") +
                (profileLink != null ? "profileLink=" + profileLink + ", " : "") +
                (toDate != null ? "toDate=" + toDate + ", " : "") +
                (staffFunction != null ? "staffFunction=" + staffFunction + ", " : "") +
                (authorId != null ? "authorId=" + authorId + ", " : "") +
                (siteChatgroupId != null ? "siteChatgroupId=" + siteChatgroupId + ", " : "") +
            "}";
    }

}
