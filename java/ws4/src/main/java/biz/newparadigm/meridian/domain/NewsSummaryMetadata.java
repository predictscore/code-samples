package biz.newparadigm.meridian.domain;

import biz.newparadigm.meridian.domain.util.MeridianDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A NewsSummaryMetadata.
 */
@Entity
@Table(name = "news_summary_metadata")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "newssummarymetadata")
public class NewsSummaryMetadata implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="news_summary_metadata_id_seq")
    @SequenceGenerator(name="news_summary_metadata_id_seq", sequenceName="news_summary_metadata_id_seq", allocationSize=1)
    private Long id;

    @Column(name = "value_string")
    private String valueString;

    @Column(name = "value_number")
    private Float valueNumber;

    @Column(name = "value_date")
    @JsonSerialize(using = MeridianDateSerializer.class)
    private ZonedDateTime valueDate;

    @ManyToOne(optional = false)
    @NotNull
    private NewsSummary newsSummary;

    @ManyToOne(optional = false)
    @NotNull
    private Metagroup metagroup;

    @ManyToOne(optional = false)
    @NotNull
    private Metafield metafield;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValueString() {
        return valueString;
    }

    public NewsSummaryMetadata valueString(String valueString) {
        this.valueString = valueString;
        return this;
    }

    public void setValueString(String valueString) {
        this.valueString = valueString;
    }

    public Float getValueNumber() {
        return valueNumber;
    }

    public NewsSummaryMetadata valueNumber(Float valueNumber) {
        this.valueNumber = valueNumber;
        return this;
    }

    public void setValueNumber(Float valueNumber) {
        this.valueNumber = valueNumber;
    }

    public ZonedDateTime getValueDate() {
        return valueDate;
    }

    public NewsSummaryMetadata valueDate(ZonedDateTime valueDate) {
        this.valueDate = valueDate;
        return this;
    }

    public void setValueDate(ZonedDateTime valueDate) {
        this.valueDate = valueDate;
    }

    public NewsSummary getNewsSummary() {
        return newsSummary;
    }

    public NewsSummaryMetadata newsSummary(NewsSummary newsSummary) {
        this.newsSummary = newsSummary;
        return this;
    }

    public void setNewsSummary(NewsSummary newsSummary) {
        this.newsSummary = newsSummary;
    }

    public Metagroup getMetagroup() {
        return metagroup;
    }

    public NewsSummaryMetadata metagroup(Metagroup metagroup) {
        this.metagroup = metagroup;
        return this;
    }

    public void setMetagroup(Metagroup metagroup) {
        this.metagroup = metagroup;
    }

    public Metafield getMetafield() {
        return metafield;
    }

    public NewsSummaryMetadata metafield(Metafield metafield) {
        this.metafield = metafield;
        return this;
    }

    public void setMetafield(Metafield metafield) {
        this.metafield = metafield;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NewsSummaryMetadata newsSummaryMetadata = (NewsSummaryMetadata) o;
        if (newsSummaryMetadata.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), newsSummaryMetadata.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NewsSummaryMetadata{" +
            "id=" + getId() +
            ", valueString='" + getValueString() + "'" +
            ", valueNumber='" + getValueNumber() + "'" +
            ", valueDate='" + getValueDate() + "'" +
            "}";
    }
}
