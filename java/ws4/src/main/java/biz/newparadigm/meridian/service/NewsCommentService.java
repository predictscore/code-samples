package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsComment;
import biz.newparadigm.meridian.repository.NewsCommentRepository;
import biz.newparadigm.meridian.repository.search.NewsCommentSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsComment.
 */
@Service
@Transactional
public class NewsCommentService {

    private final Logger log = LoggerFactory.getLogger(NewsCommentService.class);

    private final NewsCommentRepository newsCommentRepository;

    private final NewsCommentSearchRepository newsCommentSearchRepository;

    public NewsCommentService(NewsCommentRepository newsCommentRepository, NewsCommentSearchRepository newsCommentSearchRepository) {
        this.newsCommentRepository = newsCommentRepository;
        this.newsCommentSearchRepository = newsCommentSearchRepository;
    }

    /**
     * Save a newsComment.
     *
     * @param newsComment the entity to save
     * @return the persisted entity
     */
    public NewsComment save(NewsComment newsComment) {
        log.debug("Request to save NewsComment : {}", newsComment);
        NewsComment result = newsCommentRepository.save(newsComment);
        newsCommentSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsComments.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsComment> findAll(Pageable pageable) {
        log.debug("Request to get all NewsComments");
        return newsCommentRepository.findAll(pageable);
    }

    /**
     *  Get one newsComment by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsComment findOne(Long id) {
        log.debug("Request to get NewsComment : {}", id);
        return newsCommentRepository.findOne(id);
    }

    /**
     *  Delete the  newsComment by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsComment : {}", id);
        newsCommentRepository.delete(id);
        newsCommentSearchRepository.delete(id);
    }

    /**
     * Search for the newsComment corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsComment> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsComments for query {}", query);
        Page<NewsComment> result = newsCommentSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
