package biz.newparadigm.meridian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the SiteChatgroupIp entity. This class is used in SiteChatgroupIpResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /site-chatgroup-ips?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SiteChatgroupIpCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter ipName;

    private StringFilter ipWhois;

    private ZonedDateTimeFilter firstUsedDate;

    private ZonedDateTimeFilter lastUsedDate;

    private LongFilter siteChatgroupId;

    public SiteChatgroupIpCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getIpName() {
        return ipName;
    }

    public void setIpName(StringFilter ipName) {
        this.ipName = ipName;
    }

    public StringFilter getIpWhois() {
        return ipWhois;
    }

    public void setIpWhois(StringFilter ipWhois) {
        this.ipWhois = ipWhois;
    }

    public ZonedDateTimeFilter getFirstUsedDate() {
        return firstUsedDate;
    }

    public void setFirstUsedDate(ZonedDateTimeFilter firstUsedDate) {
        this.firstUsedDate = firstUsedDate;
    }

    public ZonedDateTimeFilter getLastUsedDate() {
        return lastUsedDate;
    }

    public void setLastUsedDate(ZonedDateTimeFilter lastUsedDate) {
        this.lastUsedDate = lastUsedDate;
    }

    public LongFilter getSiteChatgroupId() {
        return siteChatgroupId;
    }

    public void setSiteChatgroupId(LongFilter siteChatgroupId) {
        this.siteChatgroupId = siteChatgroupId;
    }

    @Override
    public String toString() {
        return "SiteChatgroupIpCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (ipName != null ? "ipName=" + ipName + ", " : "") +
                (ipWhois != null ? "ipWhois=" + ipWhois + ", " : "") +
                (firstUsedDate != null ? "firstUsedDate=" + firstUsedDate + ", " : "") +
                (lastUsedDate != null ? "lastUsedDate=" + lastUsedDate + ", " : "") +
                (siteChatgroupId != null ? "siteChatgroupId=" + siteChatgroupId + ", " : "") +
            "}";
    }

}
