package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsSummaryToTag;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsSummaryToTag entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsSummaryToTagRepository extends JpaRepository<NewsSummaryToTag, Long>, JpaSpecificationExecutor<NewsSummaryToTag> {

}
