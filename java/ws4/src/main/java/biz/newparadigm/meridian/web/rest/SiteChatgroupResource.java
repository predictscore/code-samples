package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.config.ApplicationProperties;
import biz.newparadigm.meridian.domain.Author;
import biz.newparadigm.meridian.domain.Location;
import biz.newparadigm.meridian.security.SecurityUtils;
import biz.newparadigm.meridian.service.CrowdService;
import biz.newparadigm.meridian.service.LocationService;
import biz.newparadigm.meridian.service.dto.old.SiteChatgroupDTO;
import biz.newparadigm.meridian.service.mapper.SiteChatgroupMapper;
import biz.newparadigm.meridian.service.util.UserUtil;
import biz.newparadigm.meridian.web.rest.util.AccessUtil;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.SiteChatgroup;
import biz.newparadigm.meridian.service.SiteChatgroupService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.SiteChatgroupCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupQueryService;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class SiteChatgroupResource {
    private final Logger log = LoggerFactory.getLogger(SiteChatgroupResource.class);

    public static final String ENTITY_NAME = "siteChatgroup";

    @Autowired
    private SiteChatgroupService siteChatgroupService;

    @Autowired
    private SiteChatgroupQueryService siteChatgroupQueryService;

    @Autowired
    private SiteChatgroupMapper siteChatgroupMapper;

    @Autowired
    private ApplicationProperties properties;

    @Autowired
    private CrowdService crowdService;

    @Autowired
    private LocationService locationService;

    /**
     * POST  /site-chatgroups : Create a new siteChatgroup.
     *
     * @param siteChatgroup the siteChatgroup to create
     * @return the ResponseEntity with status 201 (Created) and with body the new siteChatgroup, or with status 400 (Bad Request) if the siteChatgroup has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/site-chatgroups")
    @Timed
    public ResponseEntity<SiteChatgroup> createSiteChatgroup(@Valid @RequestBody SiteChatgroup siteChatgroup) throws URISyntaxException {
        log.debug("REST request to save SiteChatgroup : {}", siteChatgroup);
        if (siteChatgroup.getId() != null) {
            throw new BadRequestAlertException("A new siteChatgroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (properties.isCrowd() && isNotAdmin()) {
            boolean hasAccess = hasAccess(siteChatgroup);
            if (!hasAccess) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        }
        SiteChatgroup result = siteChatgroupService.save(siteChatgroup);
        return ResponseEntity.created(new URI("/api/site-chatgroups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /site-chatgroups : Updates an existing siteChatgroup.
     *
     * @param siteChatgroup the siteChatgroup to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated siteChatgroup,
     * or with status 400 (Bad Request) if the siteChatgroup is not valid,
     * or with status 500 (Internal Server Error) if the siteChatgroup couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/site-chatgroups")
    @Timed
    public ResponseEntity<SiteChatgroup> updateSiteChatgroup(@Valid @RequestBody SiteChatgroup siteChatgroup) throws URISyntaxException {
        log.debug("REST request to update SiteChatgroup : {}", siteChatgroup);
        if (properties.isCrowd() && isNotAdmin()) {
            boolean hasAccess = hasAccess(siteChatgroup);
            if (!hasAccess) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        }
        if (siteChatgroup.getId() == null) {
            return createSiteChatgroup(siteChatgroup);
        }
        SiteChatgroup result = siteChatgroupService.save(siteChatgroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, siteChatgroup.getId().toString()))
            .body(result);
    }

    /**
     * GET  /site-chatgroups : get all the siteChatgroups.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of siteChatgroups in body
     */
    @GetMapping("/site-chatgroups")
    @Timed
    public ResponseEntity<List<SiteChatgroup>> getAllSiteChatgroups(@RequestParam(name = "filter", required = false) String filter,
                                                                    @ApiParam SiteChatgroupCriteria criteria,
                                                                    @ApiParam Pageable pageable) {
        log.debug("REST request to get SiteChatgroups by criteria: {}", criteria);
        boolean filterByLocation = false;
        List<String> locationNames = new ArrayList<>();
        if (properties.isCrowd() && isNotAdmin()) {
            filterByLocation = true;
            locationNames = UserUtil.getUserLocations(crowdService, SecurityUtils.getCurrentUserLogin());
        }
        Page<SiteChatgroup> page = siteChatgroupQueryService.findByCriteria(filter, criteria, pageable, filterByLocation, locationNames);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/site-chatgroups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/siteChatgroups")
    @Timed
    public ResponseEntity<List<SiteChatgroupDTO>> getAllSiteChatgroupsOld(@RequestParam(value = "originalName", required = false) String originalName,
                                                                          @RequestParam(value="englishName", required = false) String englishName,
                                                                          @RequestParam(value = "domainName", required = false) String domainName,
                                                                          @RequestParam(value="exact", required = false) boolean isExact,
                                                                          @RequestParam(value="location", required = false) String locationName) {
        log.debug("REST request to get SiteChatgroups old structire");
        List<SiteChatgroup> list = siteChatgroupService.findAllOld(originalName, englishName, domainName, isExact, locationName);
        List<SiteChatgroupDTO> listDTO = new ArrayList<>();
        list.forEach(s -> listDTO.add(siteChatgroupMapper.toDTO(s, false)));
        return new ResponseEntity<>(listDTO, HttpStatus.OK);
    }

    /**
     * GET  /site-chatgroups/:id : get the "id" siteChatgroup.
     *
     * @param id the id of the siteChatgroup to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the siteChatgroup, or with status 404 (Not Found)
     */
    @GetMapping("/site-chatgroups/{id}")
    @Timed
    public ResponseEntity<SiteChatgroup> getSiteChatgroup(@PathVariable Long id) {
        log.debug("REST request to get SiteChatgroup : {}", id);
        SiteChatgroup siteChatgroup = siteChatgroupService.findOne(id, true);
        if (properties.isCrowd() && isNotAdmin()) {
            boolean hasAccess = hasAccess(siteChatgroup);
            if (!hasAccess) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        }
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(siteChatgroup));
    }

    @GetMapping("/siteChatgroups/{id}")
    @Timed
    public ResponseEntity<SiteChatgroupDTO> getSiteChatgroupOld(@PathVariable Long id) {
        log.debug("REST request to get SiteChatgroup old : {}", id);
        SiteChatgroup siteChatgroup = siteChatgroupService.findOne(id, true);
        SiteChatgroupDTO dto = siteChatgroupMapper.toDTO(siteChatgroup, true);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dto));
    }

    /**
     * DELETE  /site-chatgroups/:id : delete the "id" siteChatgroup.
     *
     * @param id the id of the siteChatgroup to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/site-chatgroups/{id}")
    @Timed
    public ResponseEntity<Void> deleteSiteChatgroup(@PathVariable Long id) {
        log.debug("REST request to delete SiteChatgroup : {}", id);
        if (properties.isCrowd() && isNotAdmin()) {
            SiteChatgroup siteChatgroup = siteChatgroupService.findOne(id, false);
            boolean hasAccess = hasAccess(siteChatgroup);
            if (!hasAccess) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        }
        siteChatgroupService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/site-chatgroups?query=:query : search for the siteChatgroup corresponding
     * to the query.
     *
     * @param query the query of the siteChatgroup search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/site-chatgroups")
    @Timed
    public ResponseEntity<List<SiteChatgroup>> searchSiteChatgroups(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SiteChatgroups for query {}", query);
        Page<SiteChatgroup> page = siteChatgroupService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/site-chatgroups");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    private boolean isNotAdmin() {
        CrowdUserDetails user = crowdService.getUserByUsername(SecurityUtils.getCurrentUserLogin());
        return !UserUtil.isAdmin(user);
    }

    private boolean hasAccess(SiteChatgroup siteChatgroup) {
        return AccessUtil.hasAccess(crowdService, locationService, siteChatgroup.getLocation());
    }
}
