package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsPublicImage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsPublicImage entity.
 */
public interface NewsPublicImageSearchRepository extends ElasticsearchRepository<NewsPublicImage, Long> {
}
