package biz.newparadigm.meridian.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsMetadata;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsMetadataRepository;
import biz.newparadigm.meridian.repository.search.NewsMetadataSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsMetadataCriteria;


/**
 * Service for executing complex queries for NewsMetadata entities in the database.
 * The main input is a {@link NewsMetadataCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsMetadata} or a {@link Page} of {%link NewsMetadata} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsMetadataQueryService extends QueryService<NewsMetadata> {

    private final Logger log = LoggerFactory.getLogger(NewsMetadataQueryService.class);


    private final NewsMetadataRepository newsMetadataRepository;

    private final NewsMetadataSearchRepository newsMetadataSearchRepository;

    public NewsMetadataQueryService(NewsMetadataRepository newsMetadataRepository, NewsMetadataSearchRepository newsMetadataSearchRepository) {
        this.newsMetadataRepository = newsMetadataRepository;
        this.newsMetadataSearchRepository = newsMetadataSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsMetadata} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsMetadata> findByCriteria(NewsMetadataCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsMetadata> specification = createSpecification(criteria);
        return newsMetadataRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsMetadata} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsMetadata> findByCriteria(NewsMetadataCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsMetadata> specification = createSpecification(criteria);
        return newsMetadataRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsMetadataCriteria to a {@link Specifications}
     */
    private Specifications<NewsMetadata> createSpecification(NewsMetadataCriteria criteria) {
        Specifications<NewsMetadata> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsMetadata_.id));
            }
            if (criteria.getValueString() != null) {
                specification = specification.and(buildStringSpecification(criteria.getValueString(), NewsMetadata_.valueString));
            }
            if (criteria.getValueNumber() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValueNumber(), NewsMetadata_.valueNumber));
            }
            if (criteria.getValueDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValueDate(), NewsMetadata_.valueDate));
            }
            if (criteria.getNewsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsId(), NewsMetadata_.news, News_.id));
            }
            if (criteria.getMetagroupId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getMetagroupId(), NewsMetadata_.metagroup, Metagroup_.id));
            }
            if (criteria.getMetafieldId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getMetafieldId(), NewsMetadata_.metafield, Metafield_.id));
            }
        }
        return specification;
    }

}
