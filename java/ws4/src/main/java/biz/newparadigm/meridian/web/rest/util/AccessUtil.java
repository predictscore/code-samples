package biz.newparadigm.meridian.web.rest.util;

import biz.newparadigm.meridian.domain.Author;
import biz.newparadigm.meridian.domain.Location;
import biz.newparadigm.meridian.security.SecurityUtils;
import biz.newparadigm.meridian.service.CrowdService;
import biz.newparadigm.meridian.service.LocationService;
import biz.newparadigm.meridian.service.util.UserUtil;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;

import java.util.List;
import java.util.stream.Collectors;

public class AccessUtil {
    public static boolean checkUserHasAccessToLocation(List<String> locationNames, Location location) {
        if (location != null && location.getId() > 0L) {
            boolean emptyFilter = locationNames.stream().filter(item -> item.equals(location.getName()))
                .collect(Collectors.toList()).isEmpty();
            return !emptyFilter;
        }
        return true;
    }

    public static boolean hasAccess(CrowdService crowdService, LocationService locationService, Location location) {
        List<String> locationNames = UserUtil.getUserLocations(crowdService, SecurityUtils.getCurrentUserLogin());
        Location authorLocation = locationService.findOne(location.getId());
        return AccessUtil.checkUserHasAccessToLocation(locationNames, authorLocation);
    }
}
