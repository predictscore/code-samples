package biz.newparadigm.meridian.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A NewsToRelatedAuthor.
 */
@Entity
@Table(name = "news_to_related_author")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "newstorelatedauthor")
public class NewsToRelatedAuthor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="news_to_related_authors_id_seq")
    @SequenceGenerator(name="news_to_related_authors_id_seq", sequenceName="news_to_related_authors_id_seq", allocationSize=1)
    private Long id;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"comments", "attachments", "originalImages", "publicImages", "confidentialImages", "urls",
        "downloadableContentUrls", "metadata", "tags", "languages", "newsSummaries", "metagroups", "metagroupsMap", "author", "siteChatgroup"})
    private News news;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"profiles", "names", "comments", "contacts", "siteChatGroups", "tags", "languages"})
    private Author author;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public News getNews() {
        return news;
    }

    public NewsToRelatedAuthor news(News news) {
        this.news = news;
        return this;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Author getAuthor() {
        return author;
    }

    public NewsToRelatedAuthor author(Author author) {
        this.author = author;
        return this;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NewsToRelatedAuthor newsToRelatedAuthor = (NewsToRelatedAuthor) o;
        if (newsToRelatedAuthor.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), newsToRelatedAuthor.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NewsToRelatedAuthor{" +
            "id=" + getId() +
            "}";
    }
}
