package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.SiteChatgroupToLanguage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SiteChatgroupToLanguage entity.
 */
public interface SiteChatgroupToLanguageSearchRepository extends ElasticsearchRepository<SiteChatgroupToLanguage, Long> {
}
