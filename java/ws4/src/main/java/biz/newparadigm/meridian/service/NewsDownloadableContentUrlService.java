package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsDownloadableContentUrl;
import biz.newparadigm.meridian.repository.NewsDownloadableContentUrlRepository;
import biz.newparadigm.meridian.repository.search.NewsDownloadableContentUrlSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsDownloadableContentUrl.
 */
@Service
@Transactional
public class NewsDownloadableContentUrlService {

    private final Logger log = LoggerFactory.getLogger(NewsDownloadableContentUrlService.class);

    private final NewsDownloadableContentUrlRepository newsDownloadableContentUrlRepository;

    private final NewsDownloadableContentUrlSearchRepository newsDownloadableContentUrlSearchRepository;

    public NewsDownloadableContentUrlService(NewsDownloadableContentUrlRepository newsDownloadableContentUrlRepository, NewsDownloadableContentUrlSearchRepository newsDownloadableContentUrlSearchRepository) {
        this.newsDownloadableContentUrlRepository = newsDownloadableContentUrlRepository;
        this.newsDownloadableContentUrlSearchRepository = newsDownloadableContentUrlSearchRepository;
    }

    /**
     * Save a newsDownloadableContentUrl.
     *
     * @param newsDownloadableContentUrl the entity to save
     * @return the persisted entity
     */
    public NewsDownloadableContentUrl save(NewsDownloadableContentUrl newsDownloadableContentUrl) {
        log.debug("Request to save NewsDownloadableContentUrl : {}", newsDownloadableContentUrl);
        NewsDownloadableContentUrl result = newsDownloadableContentUrlRepository.save(newsDownloadableContentUrl);
        newsDownloadableContentUrlSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsDownloadableContentUrls.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsDownloadableContentUrl> findAll(Pageable pageable) {
        log.debug("Request to get all NewsDownloadableContentUrls");
        return newsDownloadableContentUrlRepository.findAll(pageable);
    }

    /**
     *  Get one newsDownloadableContentUrl by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsDownloadableContentUrl findOne(Long id) {
        log.debug("Request to get NewsDownloadableContentUrl : {}", id);
        return newsDownloadableContentUrlRepository.findOne(id);
    }

    /**
     *  Delete the  newsDownloadableContentUrl by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsDownloadableContentUrl : {}", id);
        newsDownloadableContentUrlRepository.delete(id);
        newsDownloadableContentUrlSearchRepository.delete(id);
    }

    /**
     * Search for the newsDownloadableContentUrl corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsDownloadableContentUrl> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsDownloadableContentUrls for query {}", query);
        Page<NewsDownloadableContentUrl> result = newsDownloadableContentUrlSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
