package biz.newparadigm.meridian.web.rest.errors.problem;

import org.zalando.problem.StatusType;

public final class MeridianGenericProblems {
    MeridianGenericProblems() throws Exception {
        throw new IllegalAccessException();
    }

    static MeridianProblemBuilder create(final StatusType status) {
        return MeridianProblem.builder()
            .withTitle(status.getReasonPhrase())
            .withStatus(status);
    }
}
