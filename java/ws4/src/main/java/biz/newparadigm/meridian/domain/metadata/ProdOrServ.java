package biz.newparadigm.meridian.domain.metadata;

import biz.newparadigm.meridian.domain.Language;

import java.util.ArrayList;
import java.util.List;

public class ProdOrServ {
    private String productServiceNameOriginal;
    private String productServiceNameEnglish;
    private String productServiceVersionOriginal;
    private String productServiceVersionEnglish;
    private List<Language> productServiceLanguages = new ArrayList<>();

    public String getProductServiceNameOriginal() {
        return productServiceNameOriginal;
    }

    public void setProductServiceNameOriginal(String productServiceNameOriginal) {
        this.productServiceNameOriginal = productServiceNameOriginal;
    }

    public String getProductServiceNameEnglish() {
        return productServiceNameEnglish;
    }

    public void setProductServiceNameEnglish(String productServiceNameEnglish) {
        this.productServiceNameEnglish = productServiceNameEnglish;
    }

    public String getProductServiceVersionOriginal() {
        return productServiceVersionOriginal;
    }

    public void setProductServiceVersionOriginal(String productServiceVersionOriginal) {
        this.productServiceVersionOriginal = productServiceVersionOriginal;
    }

    public String getProductServiceVersionEnglish() {
        return productServiceVersionEnglish;
    }

    public void setProductServiceVersionEnglish(String productServiceVersionEnglish) {
        this.productServiceVersionEnglish = productServiceVersionEnglish;
    }

    public List<Language> getProductServiceLanguages() {
        return productServiceLanguages;
    }

    public void setProductServiceLanguages(List<Language> productServiceLanguages) {
        this.productServiceLanguages = productServiceLanguages;
    }
}
