package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.EntityAudit;
import biz.newparadigm.meridian.repository.EntityAuditRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EntityAuditService {
    private final Logger log = LoggerFactory.getLogger(EntityAuditService.class);

    @Autowired
    private EntityAuditRepository entityAuditRepository;

    public EntityAudit save(EntityAudit audit) {
        log.debug("Request to save EntityAudit : {}", audit);
        EntityAudit result = entityAuditRepository.save(audit);
        return result;
    }
}
