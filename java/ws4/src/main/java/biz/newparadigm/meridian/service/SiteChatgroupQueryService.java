package biz.newparadigm.meridian.service;


import java.util.List;

import biz.newparadigm.meridian.service.filter.SpecificationBuilder;
import biz.newparadigm.meridian.service.odata.Expression;
import biz.newparadigm.meridian.service.odata.ExpressionBuilder;
import biz.newparadigm.meridian.service.query.MeridianQueryService;
import biz.newparadigm.meridian.service.util.ServiceUtil;
import biz.newparadigm.meridian.web.rest.AuthorResource;
import biz.newparadigm.meridian.web.rest.SiteChatgroupResource;
import io.github.jhipster.service.filter.StringFilter;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.SiteChatgroup;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.SiteChatgroupRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupSearchRepository;
import biz.newparadigm.meridian.service.dto.SiteChatgroupCriteria;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.persistence.metamodel.ListAttribute;


/**
 * Service for executing complex queries for SiteChatgroup entities in the database.
 * The main input is a {@link SiteChatgroupCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link SiteChatgroup} or a {@link Page} of {%link SiteChatgroup} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class SiteChatgroupQueryService extends QueryService<SiteChatgroup> {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupQueryService.class);


    private final SiteChatgroupRepository siteChatgroupRepository;

    private final SiteChatgroupSearchRepository siteChatgroupSearchRepository;

    public SiteChatgroupQueryService(SiteChatgroupRepository siteChatgroupRepository, SiteChatgroupSearchRepository siteChatgroupSearchRepository) {
        this.siteChatgroupRepository = siteChatgroupRepository;
        this.siteChatgroupSearchRepository = siteChatgroupSearchRepository;
    }

    /**
     * Return a {@link List} of {%link SiteChatgroup} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SiteChatgroup> findByCriteria(SiteChatgroupCriteria criteria, boolean filterByLocation, List<String> locationNames) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<SiteChatgroup> specification = createSpecification(criteria, filterByLocation, locationNames);
        return siteChatgroupRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link SiteChatgroup} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroup> findByCriteria(String filter, SiteChatgroupCriteria criteria, Pageable page, boolean filterByLocation, List<String> locationNames) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        Expression filterExpression = ExpressionBuilder.build(filter);
        Specifications<SiteChatgroup> specification;
        if (filterExpression == null) {
            specification = createSpecification(criteria, filterByLocation, locationNames);
        } else {
            MeridianQueryService<SiteChatgroup> meridianQueryService = new MeridianQueryService<>(SiteChatgroupResource.ENTITY_NAME);
            specification = meridianQueryService.createFilterSpecification(filterExpression, filterByLocation, locationNames);
        }
        Page<SiteChatgroup> result = siteChatgroupRepository.findAll(specification, page);
        if (result != null && result.getContent() != null && !result.getContent().isEmpty()) {
            for (SiteChatgroup s : result) {
                if (ServiceUtil.checkContains(filter, ServiceUtil.SITECHATGROUP_FILTER_NAMES)) {
                    Hibernate.initialize(s.getNames());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.SITECHATGROUP_FILTER_DOMAINS)) {
                    Hibernate.initialize(s.getDomains());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.SITECHATGROUP_FILTER_IPS)) {
                    Hibernate.initialize(s.getIps());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.SITECHATGROUP_FILTER_NUMVISITORS)) {
                    Hibernate.initialize(s.getNumVisitors());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.SITECHATGROUP_FILTER_NUMUSERS)) {
                    Hibernate.initialize(s.getNumUsers());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.SITECHATGROUP_FILTER_LANGUAGES)) {
                    Hibernate.initialize(s.getLanguages());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.SITECHATGROUP_FILTER_TAGS)) {
                    Hibernate.initialize(s.getTags());
                }
                if (ServiceUtil.checkContains(filter, ServiceUtil.SITECHATGROUP_FILTER_AUTHORS)) {
                    Hibernate.initialize(s.getAuthors());
                }
            }
        }
        return result;
    }

    /**
     * Function to convert SiteChatgroupCriteria to a {@link Specifications}
     */
    private Specifications<SiteChatgroup> createSpecification(SiteChatgroupCriteria criteria, boolean filterByLocation, List<String> locationNames) {
        SpecificationBuilder<SiteChatgroup> builder = new SpecificationBuilder<>();
        Specifications<SiteChatgroup> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SiteChatgroup_.id));
            }
            if (criteria.getIsSite() != null) {
                specification = specification.and(buildSpecification(criteria.getIsSite(), SiteChatgroup_.isSite));
            }
            if (criteria.getOriginalName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOriginalName(), SiteChatgroup_.originalName));
            }
            if (criteria.getEnglishName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEnglishName(), SiteChatgroup_.englishName));
            }
            if (criteria.getLocationId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getLocationId(), SiteChatgroup_.location, Location_.id));
            }
            if (criteria.getLocation() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getLocation(), SiteChatgroup_.location, Location_.name));
            }
            if (criteria.getDomainId() != null) {
                specification = specification.and(buildSiteChatgroupDomainEntitySpecification(criteria.getDomainId(), SiteChatgroup_.domains, SiteChatgroupDomain_.domainId.getJavaMember().getName()));
            }
            if (filterByLocation) {
                if (locationNames == null || locationNames.isEmpty()) {
                    specification = specification.and(builder.buildNoResultSpecification(SiteChatgroup_.id));
                } else {
                    specification = specification.and(builder.buildLocationsSpecification(locationNames, SiteChatgroup.class));
                }
            }
        }
        return specification;
    }

    private Specification<SiteChatgroup> buildSiteChatgroupDomainEntitySpecification(StringFilter filter, ListAttribute<? super SiteChatgroup, SiteChatgroupDomain> reference, String valueField) {
        return (root, query, builder) -> {
            final Subquery<Long> subquery = query.subquery(Long.class);
            final Root<SiteChatgroup> from = subquery.from(SiteChatgroup.class);
            final Join<SiteChatgroup, SiteChatgroupDomain> joinTable = from.join(reference, JoinType.INNER);
            subquery.select(from.get("id"));
            if (filter.getEquals() != null) {
                subquery.where(builder.equal(joinTable.get(valueField), filter.getEquals()));
            } else if (filter.getContains() != null) {
                subquery.where(builder.like(builder.upper(joinTable.get(valueField)),"%"+filter.getContains().toUpperCase()+"%"));
            }
            return builder.in(root.get("id")).value(subquery);
        };
    }
}
