package biz.newparadigm.meridian.web.rest.errors.problem;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Status;
import org.zalando.problem.ThrowableProblem;

import java.util.Optional;

public interface MeridianAdviceTrait {
    default ResponseEntity<MeridianProblem> create(ThrowableProblem problem, NativeWebRequest request) {
        return this.create(problem, request, new HttpHeaders());
    }

    default ResponseEntity<MeridianProblem> create(ThrowableProblem problem, NativeWebRequest request, HttpHeaders headers) {
        return this.create((Throwable)problem, (MeridianProblem)problem, request, (HttpHeaders)headers);
    }

    default ResponseEntity<MeridianProblem> create(Throwable throwable, MeridianProblem problem, NativeWebRequest request) {
        return this.create(throwable, problem, request, new HttpHeaders());
    }

    default ResponseEntity<MeridianProblem> create(Throwable throwable, MeridianProblem problem, NativeWebRequest request, HttpHeaders headers) {
        HttpStatus status = HttpStatus.valueOf(Optional.ofNullable(problem.getStatus()).orElse(Status.INTERNAL_SERVER_ERROR).getStatusCode());
        if (status == HttpStatus.INTERNAL_SERVER_ERROR) {
            request.setAttribute("javax.servlet.error.exception", throwable, 0);
        }
        return ResponseEntity.status(status).headers(headers).contentType(MediaType.APPLICATION_JSON).body(problem);
    }

    default ResponseEntity<MeridianProblem> process(ResponseEntity<MeridianProblem> entity, NativeWebRequest request) {
        return this.process(entity);
    }

    default ResponseEntity<MeridianProblem> process(ResponseEntity<MeridianProblem> entity) {
        return entity;
    }
}
