package biz.newparadigm.meridian.domain;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A NewsConfidentialImage.
 */
@Entity
@Table(name = "news_confidential_images")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "newsconfidentialimage")
public class NewsConfidentialImage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="news_confidential_images_id_seq")
    @SequenceGenerator(name="news_confidential_images_id_seq", sequenceName="news_confidential_images_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "filename", nullable = false)
    private String filename;

    @NotNull
    @Lob
    @Column(name = "blob", nullable = false)
    private byte[] blob;

    @Transient
    private String blobContentType;

    @Transient
    private int blobContentLength;

    @Column(name = "description")
    private String description;

    @Column(name = "description_translation")
    private String descriptionTranslation;

    @Column(name = "image_order")
    private Integer imageOrder;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"comments", "attachments", "originalImages", "publicImages", "confidentialImages", "urls",
        "downloadableContentUrls", "metadata", "tags", "languages", "newsSummaries", "metagroups", "metagroupsMap", "author", "siteChatgroup"})
    private News news;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public NewsConfidentialImage filename(String filename) {
        this.filename = filename;
        return this;
    }

    public int getBlobContentLength() {
        return blobContentLength;
    }

    public void setBlobContentLength(int blobContentLength) {
        this.blobContentLength = blobContentLength;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public byte[] getBlob() {
        return blob;
    }

    public NewsConfidentialImage blob(byte[] blob) {
        this.blob = blob;
        return this;
    }

    public void setBlob(byte[] blob) {
        this.blob = blob;
    }

    public String getBlobContentType() {
        return blobContentType;
    }

    public NewsConfidentialImage blobContentType(String blobContentType) {
        this.blobContentType = blobContentType;
        return this;
    }

    public void setBlobContentType(String blobContentType) {
        this.blobContentType = blobContentType;
    }

    public String getDescription() {
        return description;
    }

    public NewsConfidentialImage description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionTranslation() {
        return descriptionTranslation;
    }

    public NewsConfidentialImage descriptionTranslation(String descriptionTranslation) {
        this.descriptionTranslation = descriptionTranslation;
        return this;
    }

    public void setDescriptionTranslation(String descriptionTranslation) {
        this.descriptionTranslation = descriptionTranslation;
    }

    public Integer getImageOrder() {
        return imageOrder;
    }

    public NewsConfidentialImage imageOrder(Integer imageOrder) {
        this.imageOrder = imageOrder;
        return this;
    }

    public void setImageOrder(Integer imageOrder) {
        this.imageOrder = imageOrder;
    }

    public News getNews() {
        return news;
    }

    public NewsConfidentialImage news(News news) {
        this.news = news;
        return this;
    }

    public void setNews(News news) {
        this.news = news;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NewsConfidentialImage newsConfidentialImage = (NewsConfidentialImage) o;
        if (newsConfidentialImage.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), newsConfidentialImage.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NewsConfidentialImage{" +
            "id=" + getId() +
            ", filename='" + getFilename() + "'" +
            ", blob='" + getBlob() + "'" +
            ", blobContentType='" + blobContentType + "'" +
            ", description='" + getDescription() + "'" +
            ", descriptionTranslation='" + getDescriptionTranslation() + "'" +
            ", imageOrder='" + getImageOrder() + "'" +
            "}";
    }
}
