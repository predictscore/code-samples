package biz.newparadigm.meridian.service;


import java.util.List;

import biz.newparadigm.meridian.service.filter.SpecificationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsSummary;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsSummaryRepository;
import biz.newparadigm.meridian.repository.search.NewsSummarySearchRepository;
import biz.newparadigm.meridian.service.dto.NewsSummaryCriteria;


/**
 * Service for executing complex queries for NewsSummary entities in the database.
 * The main input is a {@link NewsSummaryCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsSummary} or a {@link Page} of {%link NewsSummary} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsSummaryQueryService extends QueryService<NewsSummary> {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryQueryService.class);


    private final NewsSummaryRepository newsSummaryRepository;

    private final NewsSummarySearchRepository newsSummarySearchRepository;

    public NewsSummaryQueryService(NewsSummaryRepository newsSummaryRepository, NewsSummarySearchRepository newsSummarySearchRepository) {
        this.newsSummaryRepository = newsSummaryRepository;
        this.newsSummarySearchRepository = newsSummarySearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsSummary} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsSummary> findByCriteria(NewsSummaryCriteria criteria, boolean filterByLocation, List<String> locationNames) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsSummary> specification = createSpecification(criteria, filterByLocation, locationNames);
        return newsSummaryRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsSummary} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsSummary> findByCriteria(NewsSummaryCriteria criteria, Pageable page, boolean filterByLocation, List<String> locationNames) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsSummary> specification = createSpecification(criteria, filterByLocation, locationNames);
        return newsSummaryRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsSummaryCriteria to a {@link Specifications}
     */
    private Specifications<NewsSummary> createSpecification(NewsSummaryCriteria criteria, boolean filterByLocation, List<String> locationNames) {
        SpecificationBuilder<NewsSummary> builder = new SpecificationBuilder<>();
        Specifications<NewsSummary> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsSummary_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), NewsSummary_.name));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), NewsSummary_.description));
            }
            if (criteria.getLocationId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getLocationId(), NewsSummary_.location, Location_.id));
            }
            if (filterByLocation) {
                if (locationNames == null || locationNames.isEmpty()) {
                    specification = specification.and(builder.buildNoResultSpecification(NewsSummary_.id));
                } else {
                    specification = specification.and(builder.buildLocationsSpecification(locationNames, NewsSummary.class));
                }
            }
        }
        return specification;
    }

}
