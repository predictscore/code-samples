package biz.newparadigm.meridian.domain;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A AuthorToTag.
 */
@Entity
@Table(name = "author_to_tags")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "authortotag")
public class AuthorToTag implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="author_to_tags_id_seq")
    @SequenceGenerator(name="author_to_tags_id_seq", sequenceName="author_to_tags_id_seq", allocationSize=1)
    private Long id;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"profiles", "names", "comments", "contacts", "siteChatGroups", "tags", "languages"})
    private Author author;

    @ManyToOne(optional = false)
    @NotNull
    private Tag tag;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Author getAuthor() {
        return author;
    }

    public AuthorToTag author(Author author) {
        this.author = author;
        return this;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Tag getTag() {
        return tag;
    }

    public AuthorToTag tag(Tag tag) {
        this.tag = tag;
        return this;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AuthorToTag authorToTag = (AuthorToTag) o;
        if (authorToTag.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), authorToTag.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AuthorToTag{" +
            "id=" + getId() +
            "}";
    }
}
