package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.SiteChatgroupProfile;
import biz.newparadigm.meridian.service.SiteChatgroupProfileService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.SiteChatgroupProfileCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupProfileQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SiteChatgroupProfile.
 */
@RestController
@RequestMapping("/api")
public class SiteChatgroupProfileResource {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupProfileResource.class);

    private static final String ENTITY_NAME = "siteChatgroupProfile";

    private final SiteChatgroupProfileService siteChatgroupProfileService;

    private final SiteChatgroupProfileQueryService siteChatgroupProfileQueryService;

    public SiteChatgroupProfileResource(SiteChatgroupProfileService siteChatgroupProfileService, SiteChatgroupProfileQueryService siteChatgroupProfileQueryService) {
        this.siteChatgroupProfileService = siteChatgroupProfileService;
        this.siteChatgroupProfileQueryService = siteChatgroupProfileQueryService;
    }

    /**
     * POST  /site-chatgroup-infos : Create a new siteChatgroupProfile.
     *
     * @param siteChatgroupProfile the siteChatgroupProfile to create
     * @return the ResponseEntity with status 201 (Created) and with body the new siteChatgroupProfile, or with status 400 (Bad Request) if the siteChatgroupProfile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/site-chatgroup-profiles")
    @Timed
    public ResponseEntity<SiteChatgroupProfile> createSiteChatgroupProfile(@Valid @RequestBody SiteChatgroupProfile siteChatgroupProfile) throws URISyntaxException {
        log.debug("REST request to save SiteChatgroupProfile : {}", siteChatgroupProfile);
        if (siteChatgroupProfile.getId() != null) {
            throw new BadRequestAlertException("A new siteChatgroupProfile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SiteChatgroupProfile result = siteChatgroupProfileService.save(siteChatgroupProfile);
        return ResponseEntity.created(new URI("/api/site-chatgroup-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /site-chatgroup-infos : Updates an existing siteChatgroupProfile.
     *
     * @param siteChatgroupProfile the siteChatgroupProfile to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated siteChatgroupProfile,
     * or with status 400 (Bad Request) if the siteChatgroupProfile is not valid,
     * or with status 500 (Internal Server Error) if the siteChatgroupProfile couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/site-chatgroup-profiles")
    @Timed
    public ResponseEntity<SiteChatgroupProfile> updateSiteChatgroupProfile(@Valid @RequestBody SiteChatgroupProfile siteChatgroupProfile) throws URISyntaxException {
        log.debug("REST request to update SiteChatgroupProfile : {}", siteChatgroupProfile);
        if (siteChatgroupProfile.getId() == null) {
            return createSiteChatgroupProfile(siteChatgroupProfile);
        }
        SiteChatgroupProfile result = siteChatgroupProfileService.save(siteChatgroupProfile);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, siteChatgroupProfile.getId().toString()))
            .body(result);
    }

    /**
     * GET  /site-chatgroup-infos : get all the siteChatgroupProfiles.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of siteChatgroupProfiles in body
     */
    @GetMapping("/site-chatgroup-profiles")
    @Timed
    public ResponseEntity<List<SiteChatgroupProfile>> getAllSiteChatgroupProfiles(SiteChatgroupProfileCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get SiteChatgroupProfiles by criteria: {}", criteria);
        Page<SiteChatgroupProfile> page = siteChatgroupProfileQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/site-chatgroup-infos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /site-chatgroup-infos/:id : get the "id" siteChatgroupProfile.
     *
     * @param id the id of the siteChatgroupProfile to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the siteChatgroupProfile, or with status 404 (Not Found)
     */
    @GetMapping("/site-chatgroup-profiles/{id}")
    @Timed
    public ResponseEntity<SiteChatgroupProfile> getSiteChatgroupProfile(@PathVariable Long id) {
        log.debug("REST request to get SiteChatgroupProfile : {}", id);
        SiteChatgroupProfile siteChatgroupProfile = siteChatgroupProfileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(siteChatgroupProfile));
    }

    /**
     * DELETE  /site-chatgroup-infos/:id : delete the "id" siteChatgroupProfile.
     *
     * @param id the id of the siteChatgroupProfile to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/site-chatgroup-profiles/{id}")
    @Timed
    public ResponseEntity<Void> deleteSiteChatgroupProfile(@PathVariable Long id) {
        log.debug("REST request to delete SiteChatgroupProfile : {}", id);
        siteChatgroupProfileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/site-chatgroup-infos?query=:query : search for the siteChatgroupProfile corresponding
     * to the query.
     *
     * @param query the query of the siteChatgroupProfile search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/site-chatgroup-profiles")
    @Timed
    public ResponseEntity<List<SiteChatgroupProfile>> searchSiteChatgroupProfiles(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SiteChatgroupProfiles for query {}", query);
        Page<SiteChatgroupProfile> page = siteChatgroupProfileService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/site-chatgroup-infos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
