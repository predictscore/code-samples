package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.Metafield;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.MetafieldRepository;
import biz.newparadigm.meridian.repository.search.MetafieldSearchRepository;
import biz.newparadigm.meridian.service.dto.MetafieldCriteria;


/**
 * Service for executing complex queries for Metafield entities in the database.
 * The main input is a {@link MetafieldCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link Metafield} or a {@link Page} of {%link Metafield} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class MetafieldQueryService extends QueryService<Metafield> {

    private final Logger log = LoggerFactory.getLogger(MetafieldQueryService.class);


    private final MetafieldRepository metafieldRepository;

    private final MetafieldSearchRepository metafieldSearchRepository;

    public MetafieldQueryService(MetafieldRepository metafieldRepository, MetafieldSearchRepository metafieldSearchRepository) {
        this.metafieldRepository = metafieldRepository;
        this.metafieldSearchRepository = metafieldSearchRepository;
    }

    /**
     * Return a {@link List} of {%link Metafield} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Metafield> findByCriteria(MetafieldCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Metafield> specification = createSpecification(criteria);
        return metafieldRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link Metafield} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Metafield> findByCriteria(MetafieldCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Metafield> specification = createSpecification(criteria);
        return metafieldRepository.findAll(specification, page);
    }

    /**
     * Function to convert MetafieldCriteria to a {@link Specifications}
     */
    private Specifications<Metafield> createSpecification(MetafieldCriteria criteria) {
        Specifications<Metafield> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Metafield_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Metafield_.name));
            }
            if (criteria.getMetagroupId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getMetagroupId(), Metafield_.metagroup, Metagroup_.id));
            }
        }
        return specification;
    }

}
