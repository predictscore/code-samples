package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.Metagroup;
import biz.newparadigm.meridian.repository.MetagroupRepository;
import biz.newparadigm.meridian.repository.search.MetagroupSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Metagroup.
 */
@Service
@Transactional
public class MetagroupService {

    private final Logger log = LoggerFactory.getLogger(MetagroupService.class);

    private final MetagroupRepository metagroupRepository;

    private final MetagroupSearchRepository metagroupSearchRepository;

    public MetagroupService(MetagroupRepository metagroupRepository, MetagroupSearchRepository metagroupSearchRepository) {
        this.metagroupRepository = metagroupRepository;
        this.metagroupSearchRepository = metagroupSearchRepository;
    }

    /**
     * Save a metagroup.
     *
     * @param metagroup the entity to save
     * @return the persisted entity
     */
    public Metagroup save(Metagroup metagroup) {
        log.debug("Request to save Metagroup : {}", metagroup);
        Metagroup result = metagroupRepository.save(metagroup);
        metagroupSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the metagroups.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Metagroup> findAll(Pageable pageable) {
        log.debug("Request to get all Metagroups");
        return metagroupRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public List<Metagroup> findAll() {
        log.debug("Request to get all Metagroups");
        return metagroupRepository.findAll();
    }

    /**
     *  Get one metagroup by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Metagroup findOne(Long id) {
        log.debug("Request to get Metagroup : {}", id);
        return metagroupRepository.findOne(id);
    }

    /**
     *  Delete the  metagroup by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Metagroup : {}", id);
        metagroupRepository.delete(id);
        metagroupSearchRepository.delete(id);
    }

    /**
     * Search for the metagroup corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Metagroup> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Metagroups for query {}", query);
        Page<Metagroup> result = metagroupSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
