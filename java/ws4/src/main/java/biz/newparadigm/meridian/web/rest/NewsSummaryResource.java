package biz.newparadigm.meridian.web.rest;

import biz.newparadigm.meridian.config.ApplicationProperties;
import biz.newparadigm.meridian.security.SecurityUtils;
import biz.newparadigm.meridian.service.CrowdService;
import biz.newparadigm.meridian.service.util.UserUtil;
import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsSummary;
import biz.newparadigm.meridian.service.NewsSummaryService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsSummaryCriteria;
import biz.newparadigm.meridian.service.NewsSummaryQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsSummary.
 */
@RestController
@RequestMapping("/api")
public class NewsSummaryResource {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryResource.class);

    private static final String ENTITY_NAME = "newsSummary";

    private final NewsSummaryService newsSummaryService;

    private final NewsSummaryQueryService newsSummaryQueryService;

    @Autowired
    private ApplicationProperties properties;

    @Autowired
    private CrowdService crowdService;

    public NewsSummaryResource(NewsSummaryService newsSummaryService, NewsSummaryQueryService newsSummaryQueryService) {
        this.newsSummaryService = newsSummaryService;
        this.newsSummaryQueryService = newsSummaryQueryService;
    }

    /**
     * POST  /news-summaries : Create a new newsSummary.
     *
     * @param newsSummary the newsSummary to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsSummary, or with status 400 (Bad Request) if the newsSummary has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-summaries")
    @Timed
    public ResponseEntity<NewsSummary> createNewsSummary(@Valid @RequestBody NewsSummary newsSummary) throws URISyntaxException {
        log.debug("REST request to save NewsSummary : {}", newsSummary);
        if (newsSummary.getId() != null) {
            throw new BadRequestAlertException("A new newsSummary cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsSummary result = newsSummaryService.save(newsSummary);
        return ResponseEntity.created(new URI("/api/news-summaries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-summaries : Updates an existing newsSummary.
     *
     * @param newsSummary the newsSummary to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsSummary,
     * or with status 400 (Bad Request) if the newsSummary is not valid,
     * or with status 500 (Internal Server Error) if the newsSummary couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-summaries")
    @Timed
    public ResponseEntity<NewsSummary> updateNewsSummary(@Valid @RequestBody NewsSummary newsSummary) throws URISyntaxException {
        log.debug("REST request to update NewsSummary : {}", newsSummary);
        if (newsSummary.getId() == null) {
            return createNewsSummary(newsSummary);
        }
        NewsSummary result = newsSummaryService.save(newsSummary);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsSummary.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-summaries : get all the newsSummaries.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsSummaries in body
     */
    @GetMapping("/news-summaries")
    @Timed
    public ResponseEntity<List<NewsSummary>> getAllNewsSummaries(NewsSummaryCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsSummaries by criteria: {}", criteria);
        boolean filterByLocation = false;
        List<String> locationNames = new ArrayList<>();
        if (properties.isCrowd()) {
            filterByLocation = true;
            locationNames = UserUtil.getUserLocations(crowdService, SecurityUtils.getCurrentUserLogin());
        }
        Page<NewsSummary> page = newsSummaryQueryService.findByCriteria(criteria, pageable, filterByLocation, locationNames);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-summaries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-summaries/:id : get the "id" newsSummary.
     *
     * @param id the id of the newsSummary to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsSummary, or with status 404 (Not Found)
     */
    @GetMapping("/news-summaries/{id}")
    @Timed
    public ResponseEntity<NewsSummary> getNewsSummary(@PathVariable Long id) {
        log.debug("REST request to get NewsSummary : {}", id);
        NewsSummary newsSummary = newsSummaryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsSummary));
    }

    /**
     * DELETE  /news-summaries/:id : delete the "id" newsSummary.
     *
     * @param id the id of the newsSummary to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-summaries/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsSummary(@PathVariable Long id) {
        log.debug("REST request to delete NewsSummary : {}", id);
        newsSummaryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-summaries?query=:query : search for the newsSummary corresponding
     * to the query.
     *
     * @param query the query of the newsSummary search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-summaries")
    @Timed
    public ResponseEntity<List<NewsSummary>> searchNewsSummaries(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsSummaries for query {}", query);
        Page<NewsSummary> page = newsSummaryService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-summaries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
