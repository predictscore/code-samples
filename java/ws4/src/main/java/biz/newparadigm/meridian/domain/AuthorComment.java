package biz.newparadigm.meridian.domain;

import biz.newparadigm.meridian.domain.util.MeridianDateSerializer;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A AuthorComment.
 */
@Entity
@Table(name = "author_comments")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "authorcomment")
public class AuthorComment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="author_comments_id_seq")
    @SequenceGenerator(name="author_comments_id_seq", sequenceName="author_comments_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "user_id", nullable = false)
    private String userId;

    @NotNull
    @Column(name = "comment", nullable = false)
    private String comment;

    @NotNull
    @Column(name = "datetime", nullable = false)
    @JsonSerialize(using = MeridianDateSerializer.class)
    private ZonedDateTime datetime;

    @ManyToOne(optional = false)
    @NotNull
    @JoinColumn(name = "author_id")
    @JsonIgnoreProperties({"profiles", "names", "comments", "contacts", "siteChatGroups", "tags", "languages"})
    private Author author;

    @ManyToOne
    private AuthorComment parentComment;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public AuthorComment userId(String userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public AuthorComment comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ZonedDateTime getDatetime() {
        return datetime;
    }

    public AuthorComment datetime(ZonedDateTime datetime) {
        this.datetime = datetime;
        return this;
    }

    public void setDatetime(ZonedDateTime datetime) {
        this.datetime = datetime;
    }

    public Author getAuthor() {
        return author;
    }

    public AuthorComment author(Author author) {
        this.author = author;
        return this;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public AuthorComment getParentComment() {
        return parentComment;
    }

    public AuthorComment parentComment(AuthorComment authorComment) {
        this.parentComment = authorComment;
        return this;
    }

    public void setParentComment(AuthorComment authorComment) {
        this.parentComment = authorComment;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AuthorComment authorComment = (AuthorComment) o;
        if (authorComment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), authorComment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AuthorComment{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", comment='" + getComment() + "'" +
            ", datetime='" + getDatetime() + "'" +
            "}";
    }
}
