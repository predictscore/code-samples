package biz.newparadigm.meridian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the NewsSummaryAttachment entity. This class is used in NewsSummaryAttachmentResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /news-summary-attachments?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NewsSummaryAttachmentCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter filename;

    private StringFilter description;

    private StringFilter descriptionTranslation;

    private LongFilter newsSummaryId;

    public NewsSummaryAttachmentCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getFilename() {
        return filename;
    }

    public void setFilename(StringFilter filename) {
        this.filename = filename;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getDescriptionTranslation() {
        return descriptionTranslation;
    }

    public void setDescriptionTranslation(StringFilter descriptionTranslation) {
        this.descriptionTranslation = descriptionTranslation;
    }

    public LongFilter getNewsSummaryId() {
        return newsSummaryId;
    }

    public void setNewsSummaryId(LongFilter newsSummaryId) {
        this.newsSummaryId = newsSummaryId;
    }

    @Override
    public String toString() {
        return "NewsSummaryAttachmentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (filename != null ? "filename=" + filename + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (descriptionTranslation != null ? "descriptionTranslation=" + descriptionTranslation + ", " : "") +
                (newsSummaryId != null ? "newsSummaryId=" + newsSummaryId + ", " : "") +
            "}";
    }

}
