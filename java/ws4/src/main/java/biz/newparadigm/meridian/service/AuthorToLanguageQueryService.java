package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.AuthorToLanguage;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.AuthorToLanguageRepository;
import biz.newparadigm.meridian.repository.search.AuthorToLanguageSearchRepository;
import biz.newparadigm.meridian.service.dto.AuthorToLanguageCriteria;


/**
 * Service for executing complex queries for AuthorToLanguage entities in the database.
 * The main input is a {@link AuthorToLanguageCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link AuthorToLanguage} or a {@link Page} of {%link AuthorToLanguage} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class AuthorToLanguageQueryService extends QueryService<AuthorToLanguage> {

    private final Logger log = LoggerFactory.getLogger(AuthorToLanguageQueryService.class);


    private final AuthorToLanguageRepository authorToLanguageRepository;

    private final AuthorToLanguageSearchRepository authorToLanguageSearchRepository;

    public AuthorToLanguageQueryService(AuthorToLanguageRepository authorToLanguageRepository, AuthorToLanguageSearchRepository authorToLanguageSearchRepository) {
        this.authorToLanguageRepository = authorToLanguageRepository;
        this.authorToLanguageSearchRepository = authorToLanguageSearchRepository;
    }

    /**
     * Return a {@link List} of {%link AuthorToLanguage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuthorToLanguage> findByCriteria(AuthorToLanguageCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<AuthorToLanguage> specification = createSpecification(criteria);
        return authorToLanguageRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link AuthorToLanguage} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuthorToLanguage> findByCriteria(AuthorToLanguageCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<AuthorToLanguage> specification = createSpecification(criteria);
        return authorToLanguageRepository.findAll(specification, page);
    }

    /**
     * Function to convert AuthorToLanguageCriteria to a {@link Specifications}
     */
    private Specifications<AuthorToLanguage> createSpecification(AuthorToLanguageCriteria criteria) {
        Specifications<AuthorToLanguage> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AuthorToLanguage_.id));
            }
            if (criteria.getAuthorId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getAuthorId(), AuthorToLanguage_.author, Author_.id));
            }
            if (criteria.getLanguageId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getLanguageId(), AuthorToLanguage_.language, Language_.id));
            }
        }
        return specification;
    }

}
