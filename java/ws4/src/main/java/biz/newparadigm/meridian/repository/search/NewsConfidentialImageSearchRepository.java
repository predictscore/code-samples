package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsConfidentialImage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsConfidentialImage entity.
 */
public interface NewsConfidentialImageSearchRepository extends ElasticsearchRepository<NewsConfidentialImage, Long> {
}
