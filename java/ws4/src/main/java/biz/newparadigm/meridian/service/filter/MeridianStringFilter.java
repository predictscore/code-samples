package biz.newparadigm.meridian.service.filter;

public class MeridianStringFilter extends MeridianRangeFilter<String> {
    private String contains;

    public String getContains() {
        return contains;
    }

    public void setContains(String contains) {
        this.contains = contains;
    }

    @Override
    public String toString() {
        return "MeridianStringFilter ["
            + (getContains() != null ? "contains=" + getContains() + ", " : "")
            + (getEq() != null ? "equals=" + getEq() + ", " : "")
            + (getNe() != null ? "not equals=" + getNe() + ", " : "")
            + (getSpecified() != null ? "specified=" + getSpecified() + ", " : "")
            + (getNe() != null ? "ne=" + getNe() + ", " : "")
            + (getGt() != null ? "gt=" + getGt() + ", " : "")
            + (getGe() != null ? "ge=" + getGe() + ", " : "")
            + (getLt() != null ? "lt=" + getLt() + ", " : "")
            + (getLe() != null ? "le=" + getLe() + ", " : "")
            + "]";
    }
}
