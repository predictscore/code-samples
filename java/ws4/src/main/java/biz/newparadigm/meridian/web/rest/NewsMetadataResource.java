package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsMetadata;
import biz.newparadigm.meridian.service.NewsMetadataService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsMetadataCriteria;
import biz.newparadigm.meridian.service.NewsMetadataQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsMetadata.
 */
@RestController
@RequestMapping("/api")
public class NewsMetadataResource {

    private final Logger log = LoggerFactory.getLogger(NewsMetadataResource.class);

    private static final String ENTITY_NAME = "newsMetadata";

    private final NewsMetadataService newsMetadataService;

    private final NewsMetadataQueryService newsMetadataQueryService;

    public NewsMetadataResource(NewsMetadataService newsMetadataService, NewsMetadataQueryService newsMetadataQueryService) {
        this.newsMetadataService = newsMetadataService;
        this.newsMetadataQueryService = newsMetadataQueryService;
    }

    /**
     * POST  /news-metadata : Create a new newsMetadata.
     *
     * @param newsMetadata the newsMetadata to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsMetadata, or with status 400 (Bad Request) if the newsMetadata has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-metadata")
    @Timed
    public ResponseEntity<NewsMetadata> createNewsMetadata(@Valid @RequestBody NewsMetadata newsMetadata) throws URISyntaxException {
        log.debug("REST request to save NewsMetadata : {}", newsMetadata);
        if (newsMetadata.getId() != null) {
            throw new BadRequestAlertException("A new newsMetadata cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsMetadata result = newsMetadataService.save(newsMetadata);
        return ResponseEntity.created(new URI("/api/news-metadata/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-metadata : Updates an existing newsMetadata.
     *
     * @param newsMetadata the newsMetadata to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsMetadata,
     * or with status 400 (Bad Request) if the newsMetadata is not valid,
     * or with status 500 (Internal Server Error) if the newsMetadata couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-metadata")
    @Timed
    public ResponseEntity<NewsMetadata> updateNewsMetadata(@Valid @RequestBody NewsMetadata newsMetadata) throws URISyntaxException {
        log.debug("REST request to update NewsMetadata : {}", newsMetadata);
        if (newsMetadata.getId() == null) {
            return createNewsMetadata(newsMetadata);
        }
        NewsMetadata result = newsMetadataService.save(newsMetadata);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsMetadata.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-metadata : get all the newsMetadata.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsMetadata in body
     */
    @GetMapping("/news-metadata")
    @Timed
    public ResponseEntity<List<NewsMetadata>> getAllNewsMetadata(NewsMetadataCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsMetadata by criteria: {}", criteria);
        Page<NewsMetadata> page = newsMetadataQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-metadata");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-metadata/:id : get the "id" newsMetadata.
     *
     * @param id the id of the newsMetadata to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsMetadata, or with status 404 (Not Found)
     */
    @GetMapping("/news-metadata/{id}")
    @Timed
    public ResponseEntity<NewsMetadata> getNewsMetadata(@PathVariable Long id) {
        log.debug("REST request to get NewsMetadata : {}", id);
        NewsMetadata newsMetadata = newsMetadataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsMetadata));
    }

    /**
     * DELETE  /news-metadata/:id : delete the "id" newsMetadata.
     *
     * @param id the id of the newsMetadata to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-metadata/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsMetadata(@PathVariable Long id) {
        log.debug("REST request to delete NewsMetadata : {}", id);
        newsMetadataService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-metadata?query=:query : search for the newsMetadata corresponding
     * to the query.
     *
     * @param query the query of the newsMetadata search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-metadata")
    @Timed
    public ResponseEntity<List<NewsMetadata>> searchNewsMetadata(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsMetadata for query {}", query);
        Page<NewsMetadata> page = newsMetadataService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-metadata");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
