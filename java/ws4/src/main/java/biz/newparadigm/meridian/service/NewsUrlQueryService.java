package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsUrl;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsUrlRepository;
import biz.newparadigm.meridian.repository.search.NewsUrlSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsUrlCriteria;


/**
 * Service for executing complex queries for NewsUrl entities in the database.
 * The main input is a {@link NewsUrlCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsUrl} or a {@link Page} of {%link NewsUrl} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsUrlQueryService extends QueryService<NewsUrl> {

    private final Logger log = LoggerFactory.getLogger(NewsUrlQueryService.class);


    private final NewsUrlRepository newsUrlRepository;

    private final NewsUrlSearchRepository newsUrlSearchRepository;

    public NewsUrlQueryService(NewsUrlRepository newsUrlRepository, NewsUrlSearchRepository newsUrlSearchRepository) {
        this.newsUrlRepository = newsUrlRepository;
        this.newsUrlSearchRepository = newsUrlSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsUrl} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsUrl> findByCriteria(NewsUrlCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsUrl> specification = createSpecification(criteria);
        return newsUrlRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsUrl} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsUrl> findByCriteria(NewsUrlCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsUrl> specification = createSpecification(criteria);
        return newsUrlRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsUrlCriteria to a {@link Specifications}
     */
    private Specifications<NewsUrl> createSpecification(NewsUrlCriteria criteria) {
        Specifications<NewsUrl> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsUrl_.id));
            }
            if (criteria.getUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUrl(), NewsUrl_.url));
            }
            if (criteria.getNewsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsId(), NewsUrl_.news, News_.id));
            }
        }
        return specification;
    }

}
