package biz.newparadigm.meridian.service.view;

public class TestView {
    public static class Public {}

    public static class Extended extends Public {}
}
