package biz.newparadigm.meridian.domain.metadata;

import biz.newparadigm.meridian.domain.Author;
import biz.newparadigm.meridian.domain.Category;
import biz.newparadigm.meridian.domain.RFIResponse;

import java.util.ArrayList;
import java.util.List;

public class MMvar {
    private List<Author> relatedAuthors = new ArrayList<>();
    private Category category;
    private String offerType;
    private String confidenceLevel;
    private String confidenceLevelExplanation;
    private String levelOfImpact;
    private String levelOfImpactExplanation;
    private String analysis;
    private List<RFIResponse> rfiResponses = new ArrayList<>();

    public List<Author> getRelatedAuthors() {
        return relatedAuthors;
    }

    public void setRelatedAuthors(List<Author> relatedAuthors) {
        this.relatedAuthors = relatedAuthors;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getLevelOfImpact() {
        return levelOfImpact;
    }

    public void setLevelOfImpact(String levelOfImpact) {
        this.levelOfImpact = levelOfImpact;
    }

    public String getConfidenceLevel() {
        return confidenceLevel;
    }

    public void setConfidenceLevel(String confidenceLevel) {
        this.confidenceLevel = confidenceLevel;
    }

    public String getConfidenceLevelExplanation() {
        return confidenceLevelExplanation;
    }

    public void setConfidenceLevelExplanation(String confidenceLevelExplanation) {
        this.confidenceLevelExplanation = confidenceLevelExplanation;
    }

    public String getLevelOfImpactExplanation() {
        return levelOfImpactExplanation;
    }

    public void setLevelOfImpactExplanation(String levelOfImpactExplanation) {
        this.levelOfImpactExplanation = levelOfImpactExplanation;
    }

    public String getAnalysis() {
        return analysis;
    }

    public void setAnalysis(String analysis) {
        this.analysis = analysis;
    }

    public List<RFIResponse> getRfiResponses() {
        return rfiResponses;
    }

    public void setRfiResponses(List<RFIResponse> rfiResponses) {
        this.rfiResponses = rfiResponses;
    }
}
