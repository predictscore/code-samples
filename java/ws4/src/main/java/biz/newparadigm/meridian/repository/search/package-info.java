/**
 * Spring Data Elasticsearch repositories.
 */
package biz.newparadigm.meridian.repository.search;
