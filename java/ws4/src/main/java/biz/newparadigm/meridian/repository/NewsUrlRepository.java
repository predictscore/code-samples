package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsUrl;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsUrl entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsUrlRepository extends JpaRepository<NewsUrl, Long>, JpaSpecificationExecutor<NewsUrl> {

}
