package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsConfidentialImage;
import biz.newparadigm.meridian.repository.NewsConfidentialImageRepository;
import biz.newparadigm.meridian.repository.search.NewsConfidentialImageSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsConfidentialImage.
 */
@Service
@Transactional
public class NewsConfidentialImageService {

    private final Logger log = LoggerFactory.getLogger(NewsConfidentialImageService.class);

    private final NewsConfidentialImageRepository newsConfidentialImageRepository;

    private final NewsConfidentialImageSearchRepository newsConfidentialImageSearchRepository;

    public NewsConfidentialImageService(NewsConfidentialImageRepository newsConfidentialImageRepository, NewsConfidentialImageSearchRepository newsConfidentialImageSearchRepository) {
        this.newsConfidentialImageRepository = newsConfidentialImageRepository;
        this.newsConfidentialImageSearchRepository = newsConfidentialImageSearchRepository;
    }

    /**
     * Save a newsConfidentialImage.
     *
     * @param newsConfidentialImage the entity to save
     * @return the persisted entity
     */
    public NewsConfidentialImage save(NewsConfidentialImage newsConfidentialImage) {
        log.debug("Request to save NewsConfidentialImage : {}", newsConfidentialImage);
        NewsConfidentialImage result = newsConfidentialImageRepository.save(newsConfidentialImage);
        newsConfidentialImageSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsConfidentialImages.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsConfidentialImage> findAll(Pageable pageable) {
        log.debug("Request to get all NewsConfidentialImages");
        return newsConfidentialImageRepository.findAll(pageable);
    }

    /**
     *  Get one newsConfidentialImage by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsConfidentialImage findOne(Long id) {
        log.debug("Request to get NewsConfidentialImage : {}", id);
        return newsConfidentialImageRepository.findOne(id);
    }

    /**
     *  Delete the  newsConfidentialImage by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsConfidentialImage : {}", id);
        newsConfidentialImageRepository.delete(id);
        newsConfidentialImageSearchRepository.delete(id);
    }

    /**
     * Search for the newsConfidentialImage corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsConfidentialImage> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsConfidentialImages for query {}", query);
        Page<NewsConfidentialImage> result = newsConfidentialImageSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
