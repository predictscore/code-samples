package biz.newparadigm.meridian.domain.audit;

public enum AuditChangeType {
    NEW_OBJECT, OBJECT_REMOVED, VALUE_CHANGE, LIST_CHANGE, REFERENCE_CHANGE
}
