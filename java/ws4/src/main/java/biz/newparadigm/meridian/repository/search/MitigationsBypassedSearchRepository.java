package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.MitigationsBypassed;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the MitigationsBypassed entity.
 */
public interface MitigationsBypassedSearchRepository extends ElasticsearchRepository<MitigationsBypassed, Long> {
}
