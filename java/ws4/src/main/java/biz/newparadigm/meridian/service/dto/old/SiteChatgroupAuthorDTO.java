package biz.newparadigm.meridian.service.dto.old;

public class SiteChatgroupAuthorDTO {
    private Long siteChatGroupId = -1L;
    private AuthorDTO author;
    private String staffFunction;

    public Long getSiteChatGroupId() {
        return siteChatGroupId;
    }

    public void setSiteChatGroupId(Long siteChatGroupId) {
        this.siteChatGroupId = siteChatGroupId;
    }

    public AuthorDTO getAuthor() {
        return author;
    }

    public void setAuthor(AuthorDTO author) {
        this.author = author;
    }

    public String getStaffFunction() {
        return staffFunction;
    }

    public void setStaffFunction(String staffFunction) {
        this.staffFunction = staffFunction;
    }
}
