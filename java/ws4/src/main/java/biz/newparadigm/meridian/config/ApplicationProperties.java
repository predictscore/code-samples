package biz.newparadigm.meridian.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Newmeridian.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    public boolean crowd = false;

    public boolean isCrowd() {
        return crowd;
    }

    public void setCrowd(boolean crowd) {
        this.crowd = crowd;
    }
}
