package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.SiteChatgroupName;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.SiteChatgroupNameRepository;
import biz.newparadigm.meridian.repository.search.SiteChatgroupNameSearchRepository;
import biz.newparadigm.meridian.service.dto.SiteChatgroupNameCriteria;


/**
 * Service for executing complex queries for SiteChatgroupName entities in the database.
 * The main input is a {@link SiteChatgroupNameCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link SiteChatgroupName} or a {@link Page} of {%link SiteChatgroupName} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class SiteChatgroupNameQueryService extends QueryService<SiteChatgroupName> {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupNameQueryService.class);


    private final SiteChatgroupNameRepository siteChatgroupNameRepository;

    private final SiteChatgroupNameSearchRepository siteChatgroupNameSearchRepository;

    public SiteChatgroupNameQueryService(SiteChatgroupNameRepository siteChatgroupNameRepository, SiteChatgroupNameSearchRepository siteChatgroupNameSearchRepository) {
        this.siteChatgroupNameRepository = siteChatgroupNameRepository;
        this.siteChatgroupNameSearchRepository = siteChatgroupNameSearchRepository;
    }

    /**
     * Return a {@link List} of {%link SiteChatgroupName} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SiteChatgroupName> findByCriteria(SiteChatgroupNameCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<SiteChatgroupName> specification = createSpecification(criteria);
        return siteChatgroupNameRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link SiteChatgroupName} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SiteChatgroupName> findByCriteria(SiteChatgroupNameCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<SiteChatgroupName> specification = createSpecification(criteria);
        return siteChatgroupNameRepository.findAll(specification, page);
    }

    /**
     * Function to convert SiteChatgroupNameCriteria to a {@link Specifications}
     */
    private Specifications<SiteChatgroupName> createSpecification(SiteChatgroupNameCriteria criteria) {
        Specifications<SiteChatgroupName> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SiteChatgroupName_.id));
            }
            if (criteria.getOriginalName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOriginalName(), SiteChatgroupName_.originalName));
            }
            if (criteria.getEnglishName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEnglishName(), SiteChatgroupName_.englishName));
            }
            if (criteria.getSiteChatgroupId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSiteChatgroupId(), SiteChatgroupName_.siteChatgroup, SiteChatgroup_.id));
            }
        }
        return specification;
    }

}
