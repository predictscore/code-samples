package biz.newparadigm.meridian.domain;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SiteChatgroupToTag.
 */
@Entity
@Table(name = "site_chatgroup_to_tags")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "sitechatgrouptotag")
public class SiteChatgroupToTag implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="site_chatgroup_to_tags_id_seq")
    @SequenceGenerator(name="site_chatgroup_to_tags_id_seq", sequenceName="site_chatgroup_to_tags_id_seq", allocationSize=1)
    private Long id;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"comments", "domains", "ips", "names", "infos", "numVisitors", "numUsers", "languages", "tags", "authors"})
    private SiteChatgroup siteChatgroup;

    @ManyToOne(optional = false)
    @NotNull
    private Tag tag;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SiteChatgroup getSiteChatgroup() {
        return siteChatgroup;
    }

    public SiteChatgroupToTag siteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
        return this;
    }

    public void setSiteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
    }

    public Tag getTag() {
        return tag;
    }

    public SiteChatgroupToTag tag(Tag tag) {
        this.tag = tag;
        return this;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SiteChatgroupToTag siteChatgroupToTag = (SiteChatgroupToTag) o;
        if (siteChatgroupToTag.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), siteChatgroupToTag.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SiteChatgroupToTag{" +
            "id=" + getId() +
            "}";
    }
}
