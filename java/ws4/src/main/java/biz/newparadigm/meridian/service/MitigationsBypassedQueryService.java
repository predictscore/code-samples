package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.MitigationsBypassed;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.MitigationsBypassedRepository;
import biz.newparadigm.meridian.repository.search.MitigationsBypassedSearchRepository;
import biz.newparadigm.meridian.service.dto.MitigationsBypassedCriteria;


/**
 * Service for executing complex queries for MitigationsBypassed entities in the database.
 * The main input is a {@link MitigationsBypassedCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link MitigationsBypassed} or a {@link Page} of {%link MitigationsBypassed} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class MitigationsBypassedQueryService extends QueryService<MitigationsBypassed> {

    private final Logger log = LoggerFactory.getLogger(MitigationsBypassedQueryService.class);


    private final MitigationsBypassedRepository mitigationsBypassedRepository;

    private final MitigationsBypassedSearchRepository mitigationsBypassedSearchRepository;

    public MitigationsBypassedQueryService(MitigationsBypassedRepository mitigationsBypassedRepository, MitigationsBypassedSearchRepository mitigationsBypassedSearchRepository) {
        this.mitigationsBypassedRepository = mitigationsBypassedRepository;
        this.mitigationsBypassedSearchRepository = mitigationsBypassedSearchRepository;
    }

    /**
     * Return a {@link List} of {%link MitigationsBypassed} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MitigationsBypassed> findByCriteria(MitigationsBypassedCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<MitigationsBypassed> specification = createSpecification(criteria);
        return mitigationsBypassedRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link MitigationsBypassed} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MitigationsBypassed> findByCriteria(MitigationsBypassedCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<MitigationsBypassed> specification = createSpecification(criteria);
        return mitigationsBypassedRepository.findAll(specification, page);
    }

    /**
     * Function to convert MitigationsBypassedCriteria to a {@link Specifications}
     */
    private Specifications<MitigationsBypassed> createSpecification(MitigationsBypassedCriteria criteria) {
        Specifications<MitigationsBypassed> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), MitigationsBypassed_.id));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), MitigationsBypassed_.description));
            }
        }
        return specification;
    }

}
