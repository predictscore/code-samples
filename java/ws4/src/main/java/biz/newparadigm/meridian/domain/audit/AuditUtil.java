package biz.newparadigm.meridian.domain.audit;

import biz.newparadigm.meridian.domain.*;
import biz.newparadigm.meridian.security.SecurityUtils;
import biz.newparadigm.meridian.service.dto.old.Constants;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.javers.core.diff.Change;
import org.javers.core.diff.Diff;
import org.javers.core.diff.changetype.NewObject;
import org.javers.core.diff.changetype.ObjectRemoved;
import org.javers.core.diff.changetype.ReferenceChange;
import org.javers.core.diff.changetype.ValueChange;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class AuditUtil {
    private static Location prepareLocation(Location location) {
        Location l = new Location();
        l.setId(location.getId());
        l.setName(location.getName());
        return l;
    }

    private static ContactType prepareContactType(ContactType obj) {
        ContactType l = new ContactType();
        l.setId(obj.getId());
        l.setName(obj.getName());
        return l;
    }

    private static Tag prepareTag(Tag obj) {
        Tag a = new Tag();
        a.setId(obj.getId());
        a.setName(obj.getName());
        a.setDescription(obj.getDescription());
        if (obj.getCreatedDate() != null) {
            a.setCreatedDate(ZonedDateTime.ofInstant(obj.getCreatedDate().toInstant(), ZoneId.of("UTC")));
        }
        if (obj.getLastModifiedDate() != null) {
            a.setLastModifiedDate(ZonedDateTime.ofInstant(obj.getLastModifiedDate().toInstant(), ZoneId.of("UTC")));
        }
        return a;
    }

    private static Language prepareLanguage(Language obj) {
        Language a = new Language();
        a.setId(obj.getId());
        a.setName(obj.getName());
        a.setCreatedBy(obj.getCreatedBy());
        a.setLastModifiedBy(obj.getLastModifiedBy());
        if (obj.getCreatedDate() != null) {
            a.setCreatedDate(ZonedDateTime.ofInstant(obj.getCreatedDate().toInstant(), ZoneId.of("UTC")));
        }
        if (obj.getLastModifiedDate() != null) {
            a.setLastModifiedDate(ZonedDateTime.ofInstant(obj.getLastModifiedDate().toInstant(), ZoneId.of("UTC")));
        }
        return a;
    }

    private static Author prepareAuthorShort(Author author) {
        Author a = new Author();
        a.setId(author.getId());
        a.setEnglishName(author.getEnglishName());
        a.setOriginalName(author.getOriginalName());
        a.setCreatedBy(author.getCreatedBy());
        a.setLastModifiedBy(author.getLastModifiedBy());
        if (author.getCreatedDate() != null) {
            a.setCreatedDate(ZonedDateTime.ofInstant(author.getCreatedDate().toInstant(), ZoneId.of("UTC")));
        }
        if (author.getLastModifiedDate() != null) {
            a.setLastModifiedDate(ZonedDateTime.ofInstant(author.getLastModifiedDate().toInstant(), ZoneId.of("UTC")));
        }
        a.setLocation(prepareLocation(author.getLocation()));
        return a;
    }

    private static SiteChatgroup prepareSiteChatgroupShort(SiteChatgroup obj) {
        SiteChatgroup a = new SiteChatgroup();
        a.setId(obj.getId());
        a.setEnglishName(obj.getEnglishName());
        a.setOriginalName(obj.getOriginalName());
        a.setIsSite(obj.isIsSite());
        a.setCreatedBy(obj.getCreatedBy());
        a.setLastModifiedBy(obj.getLastModifiedBy());
        if (obj.getCreatedDate() != null) {
            a.setCreatedDate(ZonedDateTime.ofInstant(obj.getCreatedDate().toInstant(), ZoneId.of("UTC")));
        }
        if (obj.getLastModifiedDate() != null) {
            a.setLastModifiedDate(ZonedDateTime.ofInstant(obj.getLastModifiedDate().toInstant(), ZoneId.of("UTC")));
        }
        a.setLocation(prepareLocation(obj.getLocation()));
        return a;
    }

    private static NewsAttachment prepareNewsAttachment(NewsAttachment obj) {
        NewsAttachment a = new NewsAttachment();
        a.setNews(null);
        a.setId(obj.getId());
        a.setBlob(null);
        a.setFilename(obj.getFilename());
        a.setDescription(obj.getDescription());
        a.setBlobContentLength(obj.getBlob() == null ? 0 : obj.getBlob().length);
        return a;
    }

    private static NewsOriginalImage prepareNewsOriginalImage(NewsOriginalImage obj) {
        NewsOriginalImage a = new NewsOriginalImage();
        a.setNews(null);
        a.setId(obj.getId());
        a.setBlob(null);
        a.setFilename(obj.getFilename());
        a.setDescription(obj.getDescription());
        a.setBlobContentLength(obj.getBlob() == null ? 0 : obj.getBlob().length);
        return a;
    }

    private static NewsPublicImage prepareNewsPublicImage(NewsPublicImage obj) {
        NewsPublicImage a = new NewsPublicImage();
        a.setNews(null);
        a.setId(obj.getId());
        a.setBlob(null);
        a.setFilename(obj.getFilename());
        a.setDescription(obj.getDescription());
        a.setDescriptionTranslation(obj.getDescriptionTranslation());
        a.setBlobContentLength(obj.getBlob() == null ? 0 : obj.getBlob().length);
        return a;
    }

    private static NewsConfidentialImage prepareNewsConfidentialImage(NewsConfidentialImage obj) {
        NewsConfidentialImage a = new NewsConfidentialImage();
        a.setNews(null);
        a.setId(obj.getId());
        a.setBlob(null);
        a.setFilename(obj.getFilename());
        a.setDescription(obj.getDescription());
        a.setDescriptionTranslation(obj.getDescriptionTranslation());
        a.setBlobContentLength(obj.getBlob() == null ? 0 : obj.getBlob().length);
        return a;
    }

    private static AuthorName prepareAuthorName(AuthorName obj) {
        AuthorName a = new AuthorName();
        a.setAuthor(null);
        a.setId(obj.getId());
        a.setEnglishName(obj.getEnglishName());
        a.setOriginalName(obj.getOriginalName());
        return a;
    }

    private static SiteChatgroupName prepareSiteChatgroupName(SiteChatgroupName obj) {
        SiteChatgroupName a = new SiteChatgroupName();
        a.setSiteChatgroup(null);
        a.setId(obj.getId());
        a.setEnglishName(obj.getEnglishName());
        a.setOriginalName(obj.getOriginalName());
        return a;
    }

    private static AuthorToContact prepareAuthorContact(AuthorToContact obj) {
        AuthorToContact a = new AuthorToContact();
        a.setAuthor(null);
        a.setId(obj.getId());
        a.setContactValue(obj.getContactValue());
        a.setContactType(prepareContactType(obj.getContactType()));
        return a;
    }

    public static Author prepareAuthor(Author author) {
        Author a = new Author();
        a.setId(author.getId());
        a.setOriginalName(author.getOriginalName());
        a.setEnglishName(author.getEnglishName());
        a.setLocation(prepareLocation(author.getLocation()));
        a.setCreatedBy(author.getCreatedBy());
        a.setLastModifiedBy(author.getLastModifiedBy());
        if (author.getCreatedDate() != null) {
            a.setCreatedDate(ZonedDateTime.ofInstant(author.getCreatedDate().toInstant(), ZoneId.of("UTC")));
        }
        if (author.getLastModifiedDate() != null) {
            a.setLastModifiedDate(ZonedDateTime.ofInstant(author.getLastModifiedDate().toInstant(), ZoneId.of("UTC")));
        }
        a.setNames(new ArrayList<>());
        if (author.getNames() != null) {
            author.getNames().forEach(item -> {
                a.getNames().add(prepareAuthorName(item));
            });
        }
        a.setContacts(new ArrayList<>());
        if (author.getContacts() != null) {
            author.getContacts().forEach(item -> {
                a.getContacts().add(prepareAuthorContact(item));
            });
        }
        a.setTags(new ArrayList<>());
        if (author.getTags() != null) {
            author.getTags().forEach(item -> {
                a.getTags().add(prepareAuthorTag(item));
            });
        }
        a.setLanguages(new ArrayList<>());
        if (author.getLanguages() != null) {
            author.getLanguages().forEach(item -> {
                a.getLanguages().add(prepareAuthorLanguage(item));
            });
        }
        a.setSiteChatGroups(new ArrayList<>());
        if (author.getSiteChatGroups() != null) {
            author.getSiteChatGroups().forEach(item -> {
                a.getSiteChatGroups().add(prepareAuthorToSiteChatgroup(item));
            });
        }
        return a;
    }

    public static SiteChatgroup prepareSiteChatgroup(SiteChatgroup s) {
        SiteChatgroup a = new SiteChatgroup();
        a.setId(s.getId());
        a.setOriginalName(s.getOriginalName());
        a.setEnglishName(s.getEnglishName());
        a.setLocation(prepareLocation(s.getLocation()));
        a.setIsSite(s.isIsSite());
        a.setCreatedBy(s.getCreatedBy());
        a.setLastModifiedBy(s.getLastModifiedBy());
        if (s.getCreatedDate() != null) {
            a.setCreatedDate(ZonedDateTime.ofInstant(s.getCreatedDate().toInstant(), ZoneId.of("UTC")));
        }
        if (s.getLastModifiedDate() != null) {
            a.setLastModifiedDate(ZonedDateTime.ofInstant(s.getLastModifiedDate().toInstant(), ZoneId.of("UTC")));
        }
        a.setDomains(new ArrayList<>());
        if (s.getDomains() != null) {
            s.getDomains().forEach(item -> {
                a.getDomains().add(prepareSiteChatgroupDomain(item));
            });
        }
        a.setIps(new ArrayList<>());
        if (s.getIps() != null) {
            s.getIps().forEach(item -> {
                a.getIps().add(prepareSiteChatgroupIp(item));
            });
        }
        a.setNames(new ArrayList<>());
        if (s.getNames() != null) {
            s.getNames().forEach(item -> {
                a.getNames().add(prepareSiteChatgroupName(item));
            });
        }
        a.setNumUsers(new ArrayList<>());
        if (s.getNumUsers() != null) {
            s.getNumUsers().forEach(item -> {
                a.getNumUsers().add(prepareSiteChatgroupNumUsers(item));
            });
        }
        a.setNumVisitors(new ArrayList<>());
        if (s.getNumVisitors() != null) {
            s.getNumVisitors().forEach(item -> {
                a.getNumVisitors().add(prepareSiteChatgroupNumVisitors(item));
            });
        }
        a.setLanguages(new ArrayList<>());
        if (s.getLanguages() != null) {
            s.getLanguages().forEach(item -> {
                a.getLanguages().add(prepareSiteChatgroupLanguage(item));
            });
        }
        a.setTags(new ArrayList<>());
        if (s.getTags() != null) {
            s.getTags().forEach(item -> {
                a.getTags().add(prepareSiteChatgroupTag(item));
            });
        }
        a.setAuthors(new ArrayList<>());
        if (s.getAuthors() != null) {
            s.getAuthors().forEach(item -> {
                a.getAuthors().add(prepareAuthorToSiteChatgroup2(item));
            });
        }
        return a;
    }

    public static News prepareNews(News news) {
        News n = new News();
        n.setId(news.getId());
        if (news.getDatetime() != null) {
            n.setDatetime(ZonedDateTime.ofInstant(news.getDatetime().toInstant(), ZoneId.of("UTC")));
        }
        n.setOriginalSubject(news.getOriginalSubject());
        n.setEnglishSubject(news.getEnglishSubject());
        n.setOriginalPost(news.getOriginalPost());
        n.setEnglishPost(news.getEnglishPost());
        n.setCreatedBy(news.getCreatedBy());
        n.setLastModifiedBy(news.getLastModifiedBy());
        if (news.getCreatedDate() != null) {
            n.setCreatedDate(ZonedDateTime.ofInstant(news.getCreatedDate().toInstant(), ZoneId.of("UTC")));
        }
        if (news.getLastModifiedDate() != null) {
            n.setLastModifiedDate(ZonedDateTime.ofInstant(news.getLastModifiedDate().toInstant(), ZoneId.of("UTC")));
        }
        n.setAuthor(prepareAuthorShort(news.getAuthor()));
        n.setSiteChatgroup(prepareSiteChatgroupShort(news.getSiteChatgroup()));
        n.setLocation(prepareLocation(news.getLocation()));
        n.setAttachments(new ArrayList<>());
        if (news.getAttachments() != null) {
            news.getAttachments().forEach(item -> {
                n.getAttachments().add(prepareNewsAttachment(item));
            });
        }
        n.setOriginalImages(new ArrayList<>());
        if (news.getOriginalImages() != null) {
            news.getOriginalImages().forEach(item -> {
                n.getOriginalImages().add(prepareNewsOriginalImage(item));
            });
        }
        n.setPublicImages(new ArrayList<>());
        if (news.getPublicImages() != null) {
            news.getPublicImages().forEach(item -> {
                n.getPublicImages().add(prepareNewsPublicImage(item));
            });
        }
        n.setConfidentialImages(new ArrayList<>());
        if (news.getConfidentialImages() != null) {
            news.getConfidentialImages().forEach(item -> {
                n.getConfidentialImages().add(prepareNewsConfidentialImage(item));
            });
        }
        n.setUrls(new ArrayList<>());
        if (news.getUrls() != null) {
            news.getUrls().forEach(item -> {
                n.getUrls().add(prepareNewsUrl(item));
            });
        }
        n.setDownloadableContentUrls(new ArrayList<>());
        if (news.getDownloadableContentUrls() != null) {
            news.getDownloadableContentUrls().forEach(item -> {
                n.getDownloadableContentUrls().add(prepareNewsDownloadableContentUrl(item));
            });
        }
        n.setTags(new ArrayList<>());
        if (news.getTags() != null) {
            news.getTags().forEach(item -> {
                n.getTags().add(prepareNewsTag(item));
            });
        }
        n.setLanguages(new ArrayList<>());
        if (news.getLanguages() != null) {
            news.getLanguages().forEach(item -> {
                n.getLanguages().add(prepareNewsLanguage(item));
            });
        }
        n.setMetadata(new ArrayList<>());
//        if (news.getMetadata() != null) {
//            news.getMetadata().forEach(item -> {
//                n.getMetadata().add(prepareNewsMetadata(item));
//            });
//        }
        n.setNewsSummaries(new ArrayList<>());
        if (news.getNewsSummaries() != null) {
            news.getNewsSummaries().forEach(item -> {
                n.getNewsSummaries().add(prepareNewsToNewsSummary(item));
            });
        }
        return n;
    }

    private static AuthorToSiteChatgroup prepareAuthorToSiteChatgroup2(AuthorToSiteChatgroup obj) {
        AuthorToSiteChatgroup a = new AuthorToSiteChatgroup();
        a.setAuthor(prepareAuthorShort(obj.getAuthor()));
        a.setId(obj.getId());
        a.setEnglishName(obj.getEnglishName());
        a.setOriginalName(obj.getOriginalName());
        if (obj.getFromDate() != null) {
            a.setFromDate(ZonedDateTime.ofInstant(obj.getFromDate().toInstant(), ZoneId.of("UTC")));
        }
        a.setProfileLink(obj.getProfileLink());
        a.setStaffFunction(obj.getStaffFunction());
        if (obj.getToDate() != null) {
            a.setToDate(ZonedDateTime.ofInstant(obj.getToDate().toInstant(), ZoneId.of("UTC")));
        }
        a.setSiteChatgroup(null);
        return a;
    }

    private static SiteChatgroupIp prepareSiteChatgroupIp(SiteChatgroupIp obj) {
        SiteChatgroupIp a = new SiteChatgroupIp();
        a.setId(obj.getId());
        a.setSiteChatgroup(null);
        a.setIpName(obj.getIpName());
        a.setIpWhois(obj.getIpWhois());
        if (obj.getFirstUsedDate() != null) {
            a.setFirstUsedDate(ZonedDateTime.ofInstant(obj.getFirstUsedDate().toInstant(), ZoneId.of("UTC")));
        }
        if (obj.getLastUsedDate() != null) {
            a.setLastUsedDate(ZonedDateTime.ofInstant(obj.getLastUsedDate().toInstant(), ZoneId.of("UTC")));
        }
        return a;
    }

    private static SiteChatgroupDomain prepareSiteChatgroupDomain(SiteChatgroupDomain obj) {
        SiteChatgroupDomain a = new SiteChatgroupDomain();
        a.setId(obj.getId());
        a.setSiteChatgroup(null);
        a.setDomainId(obj.getDomainId());
        a.setDomainInfo(obj.getDomainInfo());
        if (obj.getFirstUsedDate() != null) {
            a.setFirstUsedDate(ZonedDateTime.ofInstant(obj.getFirstUsedDate().toInstant(), ZoneId.of("UTC")));
        }
        if (obj.getLastUsedDate() != null) {
            a.setLastUsedDate(ZonedDateTime.ofInstant(obj.getLastUsedDate().toInstant(), ZoneId.of("UTC")));
        }
        return a;
    }

    private static SiteChatgroupNumUsers prepareSiteChatgroupNumUsers(SiteChatgroupNumUsers obj) {
        SiteChatgroupNumUsers a = new SiteChatgroupNumUsers();
        a.setSiteChatgroup(null);
        a.setId(obj.getId());
        if (obj.getNumUserDate() != null) {
            a.setNumUserDate(ZonedDateTime.ofInstant(obj.getNumUserDate().toInstant(), ZoneId.of("UTC")));
        }
        a.setNumUsers(obj.getNumUsers());
        return a;
    }

    private static SiteChatgroupNumVisitors prepareSiteChatgroupNumVisitors(SiteChatgroupNumVisitors obj) {
        SiteChatgroupNumVisitors a = new SiteChatgroupNumVisitors();
        a.setSiteChatgroup(null);
        a.setId(obj.getId());
        if (obj.getNumVisitorDate() != null) {
            a.setNumVisitorDate(ZonedDateTime.ofInstant(obj.getNumVisitorDate().toInstant(), ZoneId.of("UTC")));
        }
        a.setNumVisitors(obj.getNumVisitors());
        return a;
    }

    private static NewsSummaryToNews prepareNewsToNewsSummary(NewsSummaryToNews obj) {
        NewsSummaryToNews a = new NewsSummaryToNews();
        a.setId(obj.getId());
        a.setNews(null);
        a.setNewsSummary(prepareNewsSummary(obj.getNewsSummary()));
        return a;
    }

    private static AuthorToSiteChatgroup prepareAuthorToSiteChatgroup(AuthorToSiteChatgroup obj) {
        AuthorToSiteChatgroup a = new AuthorToSiteChatgroup();
        a.setAuthor(null);
        a.setId(obj.getId());
        a.setEnglishName(obj.getEnglishName());
        a.setOriginalName(obj.getOriginalName());
        if (obj.getFromDate() != null) {
            a.setFromDate(ZonedDateTime.ofInstant(obj.getFromDate().toInstant(), ZoneId.of("UTC")));
        }
        a.setProfileLink(obj.getProfileLink());
        a.setStaffFunction(obj.getStaffFunction());
        if (obj.getToDate() != null) {
            a.setToDate(ZonedDateTime.ofInstant(obj.getToDate().toInstant(), ZoneId.of("UTC")));
        }
        a.setSiteChatgroup(prepareSiteChatgroupShort(obj.getSiteChatgroup()));
        return a;
    }

    private static NewsSummary prepareNewsSummary(NewsSummary obj) {
        NewsSummary a = new NewsSummary();
        a.setId(obj.getId());
        a.setLocation(prepareLocation(obj.getLocation()));
        a.setDescription(obj.getDescription());
        a.setName(obj.getName());
        return a;
    }

    private static NewsMetadata prepareNewsMetadata(NewsMetadata obj) {
        NewsMetadata a = new NewsMetadata();
        a.setNews(null);
        a.setId(obj.getId());
        a.setValueNumber(obj.getValueNumber());
        a.setValueString(obj.getValueString());
        a.setValueDate(obj.getValueDate());
        a.setMetafield(prepareMetafield(obj.getMetafield()));
        a.setMetagroup(prepareMetagroup(obj.getMetagroup()));
        return a;
    }

    private static Metafield prepareMetafield(Metafield obj) {
        Metafield a = new Metafield();
        a.setName(obj.getName());
        a.setId(obj.getId());
        a.setMetagroup(null);
        return a;
    }

    private static Metagroup prepareMetagroup(Metagroup obj) {
        Metagroup a = new Metagroup();
        a.setName(obj.getName());
        a.setId(obj.getId());
        return a;
    }

    private static NewsToLanguage prepareNewsLanguage(NewsToLanguage obj) {
        NewsToLanguage a = new NewsToLanguage();
        a.setNews(null);
        a.setId(obj.getId());
        a.setLanguage(prepareLanguage(obj.getLanguage()));
        return a;
    }

    private static AuthorToLanguage prepareAuthorLanguage(AuthorToLanguage obj) {
        AuthorToLanguage a = new AuthorToLanguage();
        a.setAuthor(null);
        a.setId(obj.getId());
        a.setLanguage(prepareLanguage(obj.getLanguage()));
        return a;
    }

    private static SiteChatgroupToLanguage prepareSiteChatgroupLanguage(SiteChatgroupToLanguage obj) {
        SiteChatgroupToLanguage a = new SiteChatgroupToLanguage();
        a.setSiteChatgroup(null);
        a.setId(obj.getId());
        a.setLanguage(prepareLanguage(obj.getLanguage()));
        return a;
    }

    private static NewsToTag prepareNewsTag(NewsToTag obj) {
        NewsToTag a = new NewsToTag();
        a.setNews(null);
        a.setId(obj.getId());
        a.setTag(prepareTag(obj.getTag()));
        return a;
    }

    private static SiteChatgroupToTag prepareSiteChatgroupTag(SiteChatgroupToTag obj) {
        SiteChatgroupToTag a = new SiteChatgroupToTag();
        a.setSiteChatgroup(null);
        a.setId(obj.getId());
        a.setTag(prepareTag(obj.getTag()));
        return a;
    }

    private static AuthorToTag prepareAuthorTag(AuthorToTag obj) {
        AuthorToTag a = new AuthorToTag();
        a.setAuthor(null);
        a.setId(obj.getId());
        a.setTag(prepareTag(obj.getTag()));
        return a;
    }

    private static NewsDownloadableContentUrl prepareNewsDownloadableContentUrl(NewsDownloadableContentUrl obj) {
        NewsDownloadableContentUrl a = new NewsDownloadableContentUrl();
        a.setNews(null);
        a.setId(obj.getId());
        a.setUrl(obj.getUrl());
        a.setDescription(obj.getDescription());
        return a;
    }

    private static NewsUrl prepareNewsUrl(NewsUrl obj) {
        NewsUrl a = new NewsUrl();
        a.setNews(null);
        a.setId(obj.getId());
        a.setUrl(obj.getUrl());
        return a;
    }

    public static ObjectMapper getMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.setDateFormat(new SimpleDateFormat(Constants.dateWithTimeFormat));
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.configure(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS, false)
            .configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS,false)
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,false)
            .configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID,false)
            .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
            .setVisibility(mapper.getSerializationConfig()
                .getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
    }

    private static List<AuditChange> getChanges(Diff diff) {
        List<AuditChange> auditChanges = new ArrayList<>();
        if (diff.hasChanges()) {
            List<Change> changes = diff.getChanges();
            for (Change change : changes) {
                if (change instanceof NewObject) {
                    NewObject val = (NewObject) change;
                    AuditChange ac = new AuditChange();
                    ac.setToValue(val.getAffectedObject().get().toString());
                    ac.setOperation(AuditChangeType.NEW_OBJECT);
                    auditChanges.add(ac);
                } else if (change instanceof ObjectRemoved) {
                    ObjectRemoved val = (ObjectRemoved) change;
                    AuditChange ac = new AuditChange();
                    ac.setFromValue(val.getAffectedObject().get().toString());
                    ac.setOperation(AuditChangeType.OBJECT_REMOVED);
                    auditChanges.add(ac);
                } else if (change instanceof ValueChange) {
                    ValueChange val = (ValueChange) change;
                    if (!val.getPropertyName().equalsIgnoreCase("createdDate") && !val.getPropertyName().equalsIgnoreCase("lastModifiedDate")) {
                        AuditChange ac = new AuditChange();
                        ac.setFromValue(val.getLeft().toString());
                        ac.setToValue(val.getRight().toString());
                        ac.setProperty(val.getPropertyName());
                        ac.setOperation(AuditChangeType.VALUE_CHANGE);
                        auditChanges.add(ac);
                    }
                } else if (change instanceof ReferenceChange) {
                    ReferenceChange val = (ReferenceChange) change;
                    AuditChange ac = new AuditChange();
                    ac.setProperty(val.getPropertyName());
                    ac.setFromValue(val.getLeftObject().get().toString());
                    ac.setToValue(val.getRightObject().get().toString());
                    ac.setOperation(AuditChangeType.REFERENCE_CHANGE);
                    auditChanges.add(ac);
                } else {
                    System.out.println(change);
                }
            }
        }
        return auditChanges;
    }

    public static EntityAudit getAuditEntity(ObjectMapper mapper, boolean insert, Long entityId, Diff diff, String originalJson, String updatedJson, String className) {
        try {
            List<AuditChange> auditChanges = getChanges(diff);
            EntityAudit audit = new EntityAudit();
            audit.setAction(insert ? AuditEventType.INSERT.name() : AuditEventType.UPDATE.name());
            audit.setCommitVersion(0);
            audit.setCreatedBy(SecurityUtils.getCurrentUserLogin());
            audit.setCreatedDate(ZonedDateTime.now(ZoneId.of("UTC")));
            audit.setEntityId(entityId);
            try {
                audit.setEntityChanges(mapper.writeValueAsString(auditChanges));
            } catch (Exception e) {
                audit.setEntityChanges("{}");
            }
            audit.setEntityOldValue(originalJson);
            audit.setEntityNewValue(updatedJson);
            audit.setEntityType(className);
            return audit;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
