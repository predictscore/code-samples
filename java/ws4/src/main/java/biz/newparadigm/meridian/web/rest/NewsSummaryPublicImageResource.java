package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.NewsSummaryPublicImage;
import biz.newparadigm.meridian.service.NewsSummaryPublicImageService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.NewsSummaryPublicImageCriteria;
import biz.newparadigm.meridian.service.NewsSummaryPublicImageQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NewsSummaryPublicImage.
 */
@RestController
@RequestMapping("/api")
public class NewsSummaryPublicImageResource {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryPublicImageResource.class);

    private static final String ENTITY_NAME = "newsSummaryPublicImage";

    private final NewsSummaryPublicImageService newsSummaryPublicImageService;

    private final NewsSummaryPublicImageQueryService newsSummaryPublicImageQueryService;

    public NewsSummaryPublicImageResource(NewsSummaryPublicImageService newsSummaryPublicImageService, NewsSummaryPublicImageQueryService newsSummaryPublicImageQueryService) {
        this.newsSummaryPublicImageService = newsSummaryPublicImageService;
        this.newsSummaryPublicImageQueryService = newsSummaryPublicImageQueryService;
    }

    /**
     * POST  /news-summary-public-images : Create a new newsSummaryPublicImage.
     *
     * @param newsSummaryPublicImage the newsSummaryPublicImage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new newsSummaryPublicImage, or with status 400 (Bad Request) if the newsSummaryPublicImage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news-summary-public-images")
    @Timed
    public ResponseEntity<NewsSummaryPublicImage> createNewsSummaryPublicImage(@Valid @RequestBody NewsSummaryPublicImage newsSummaryPublicImage) throws URISyntaxException {
        log.debug("REST request to save NewsSummaryPublicImage : {}", newsSummaryPublicImage);
        if (newsSummaryPublicImage.getId() != null) {
            throw new BadRequestAlertException("A new newsSummaryPublicImage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NewsSummaryPublicImage result = newsSummaryPublicImageService.save(newsSummaryPublicImage);
        return ResponseEntity.created(new URI("/api/news-summary-public-images/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /news-summary-public-images : Updates an existing newsSummaryPublicImage.
     *
     * @param newsSummaryPublicImage the newsSummaryPublicImage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated newsSummaryPublicImage,
     * or with status 400 (Bad Request) if the newsSummaryPublicImage is not valid,
     * or with status 500 (Internal Server Error) if the newsSummaryPublicImage couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/news-summary-public-images")
    @Timed
    public ResponseEntity<NewsSummaryPublicImage> updateNewsSummaryPublicImage(@Valid @RequestBody NewsSummaryPublicImage newsSummaryPublicImage) throws URISyntaxException {
        log.debug("REST request to update NewsSummaryPublicImage : {}", newsSummaryPublicImage);
        if (newsSummaryPublicImage.getId() == null) {
            return createNewsSummaryPublicImage(newsSummaryPublicImage);
        }
        NewsSummaryPublicImage result = newsSummaryPublicImageService.save(newsSummaryPublicImage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newsSummaryPublicImage.getId().toString()))
            .body(result);
    }

    /**
     * GET  /news-summary-public-images : get all the newsSummaryPublicImages.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of newsSummaryPublicImages in body
     */
    @GetMapping("/news-summary-public-images")
    @Timed
    public ResponseEntity<List<NewsSummaryPublicImage>> getAllNewsSummaryPublicImages(NewsSummaryPublicImageCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get NewsSummaryPublicImages by criteria: {}", criteria);
        Page<NewsSummaryPublicImage> page = newsSummaryPublicImageQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/news-summary-public-images");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /news-summary-public-images/:id : get the "id" newsSummaryPublicImage.
     *
     * @param id the id of the newsSummaryPublicImage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the newsSummaryPublicImage, or with status 404 (Not Found)
     */
    @GetMapping("/news-summary-public-images/{id}")
    @Timed
    public ResponseEntity<NewsSummaryPublicImage> getNewsSummaryPublicImage(@PathVariable Long id) {
        log.debug("REST request to get NewsSummaryPublicImage : {}", id);
        NewsSummaryPublicImage newsSummaryPublicImage = newsSummaryPublicImageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(newsSummaryPublicImage));
    }

    /**
     * DELETE  /news-summary-public-images/:id : delete the "id" newsSummaryPublicImage.
     *
     * @param id the id of the newsSummaryPublicImage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news-summary-public-images/{id}")
    @Timed
    public ResponseEntity<Void> deleteNewsSummaryPublicImage(@PathVariable Long id) {
        log.debug("REST request to delete NewsSummaryPublicImage : {}", id);
        newsSummaryPublicImageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/news-summary-public-images?query=:query : search for the newsSummaryPublicImage corresponding
     * to the query.
     *
     * @param query the query of the newsSummaryPublicImage search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/news-summary-public-images")
    @Timed
    public ResponseEntity<List<NewsSummaryPublicImage>> searchNewsSummaryPublicImages(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of NewsSummaryPublicImages for query {}", query);
        Page<NewsSummaryPublicImage> page = newsSummaryPublicImageService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/news-summary-public-images");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
