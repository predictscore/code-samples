package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.SiteChatgroupComment;
import biz.newparadigm.meridian.service.SiteChatgroupCommentService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.SiteChatgroupCommentCriteria;
import biz.newparadigm.meridian.service.SiteChatgroupCommentQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SiteChatgroupComment.
 */
@RestController
@RequestMapping("/api")
public class SiteChatgroupCommentResource {

    private final Logger log = LoggerFactory.getLogger(SiteChatgroupCommentResource.class);

    private static final String ENTITY_NAME = "siteChatgroupComment";

    private final SiteChatgroupCommentService siteChatgroupCommentService;

    private final SiteChatgroupCommentQueryService siteChatgroupCommentQueryService;

    public SiteChatgroupCommentResource(SiteChatgroupCommentService siteChatgroupCommentService, SiteChatgroupCommentQueryService siteChatgroupCommentQueryService) {
        this.siteChatgroupCommentService = siteChatgroupCommentService;
        this.siteChatgroupCommentQueryService = siteChatgroupCommentQueryService;
    }

    /**
     * POST  /site-chatgroup-comments : Create a new siteChatgroupComment.
     *
     * @param siteChatgroupComment the siteChatgroupComment to create
     * @return the ResponseEntity with status 201 (Created) and with body the new siteChatgroupComment, or with status 400 (Bad Request) if the siteChatgroupComment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/site-chatgroup-comments")
    @Timed
    public ResponseEntity<SiteChatgroupComment> createSiteChatgroupComment(@Valid @RequestBody SiteChatgroupComment siteChatgroupComment) throws URISyntaxException {
        log.debug("REST request to save SiteChatgroupComment : {}", siteChatgroupComment);
        if (siteChatgroupComment.getId() != null) {
            throw new BadRequestAlertException("A new siteChatgroupComment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SiteChatgroupComment result = siteChatgroupCommentService.save(siteChatgroupComment);
        return ResponseEntity.created(new URI("/api/site-chatgroup-comments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /site-chatgroup-comments : Updates an existing siteChatgroupComment.
     *
     * @param siteChatgroupComment the siteChatgroupComment to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated siteChatgroupComment,
     * or with status 400 (Bad Request) if the siteChatgroupComment is not valid,
     * or with status 500 (Internal Server Error) if the siteChatgroupComment couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/site-chatgroup-comments")
    @Timed
    public ResponseEntity<SiteChatgroupComment> updateSiteChatgroupComment(@Valid @RequestBody SiteChatgroupComment siteChatgroupComment) throws URISyntaxException {
        log.debug("REST request to update SiteChatgroupComment : {}", siteChatgroupComment);
        if (siteChatgroupComment.getId() == null) {
            return createSiteChatgroupComment(siteChatgroupComment);
        }
        SiteChatgroupComment result = siteChatgroupCommentService.save(siteChatgroupComment);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, siteChatgroupComment.getId().toString()))
            .body(result);
    }

    /**
     * GET  /site-chatgroup-comments : get all the siteChatgroupComments.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of siteChatgroupComments in body
     */
    @GetMapping("/site-chatgroup-comments")
    @Timed
    public ResponseEntity<List<SiteChatgroupComment>> getAllSiteChatgroupComments(SiteChatgroupCommentCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get SiteChatgroupComments by criteria: {}", criteria);
        Page<SiteChatgroupComment> page = siteChatgroupCommentQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/site-chatgroup-comments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /site-chatgroup-comments/:id : get the "id" siteChatgroupComment.
     *
     * @param id the id of the siteChatgroupComment to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the siteChatgroupComment, or with status 404 (Not Found)
     */
    @GetMapping("/site-chatgroup-comments/{id}")
    @Timed
    public ResponseEntity<SiteChatgroupComment> getSiteChatgroupComment(@PathVariable Long id) {
        log.debug("REST request to get SiteChatgroupComment : {}", id);
        SiteChatgroupComment siteChatgroupComment = siteChatgroupCommentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(siteChatgroupComment));
    }

    /**
     * DELETE  /site-chatgroup-comments/:id : delete the "id" siteChatgroupComment.
     *
     * @param id the id of the siteChatgroupComment to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/site-chatgroup-comments/{id}")
    @Timed
    public ResponseEntity<Void> deleteSiteChatgroupComment(@PathVariable Long id) {
        log.debug("REST request to delete SiteChatgroupComment : {}", id);
        siteChatgroupCommentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/site-chatgroup-comments?query=:query : search for the siteChatgroupComment corresponding
     * to the query.
     *
     * @param query the query of the siteChatgroupComment search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/site-chatgroup-comments")
    @Timed
    public ResponseEntity<List<SiteChatgroupComment>> searchSiteChatgroupComments(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of SiteChatgroupComments for query {}", query);
        Page<SiteChatgroupComment> page = siteChatgroupCommentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/site-chatgroup-comments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
