package biz.newparadigm.meridian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the SiteChatgroupNumUsers entity. This class is used in SiteChatgroupNumUsersResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /site-chatgroup-num-users?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SiteChatgroupNumUsersCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private IntegerFilter numUsers;

    private ZonedDateTimeFilter numUserDate;

    private LongFilter siteChatgroupId;

    public SiteChatgroupNumUsersCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getNumUsers() {
        return numUsers;
    }

    public void setNumUsers(IntegerFilter numUsers) {
        this.numUsers = numUsers;
    }

    public ZonedDateTimeFilter getNumUserDate() {
        return numUserDate;
    }

    public void setNumUserDate(ZonedDateTimeFilter numUserDate) {
        this.numUserDate = numUserDate;
    }

    public LongFilter getSiteChatgroupId() {
        return siteChatgroupId;
    }

    public void setSiteChatgroupId(LongFilter siteChatgroupId) {
        this.siteChatgroupId = siteChatgroupId;
    }

    @Override
    public String toString() {
        return "SiteChatgroupNumUsersCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (numUsers != null ? "numUsers=" + numUsers + ", " : "") +
                (numUserDate != null ? "numUserDate=" + numUserDate + ", " : "") +
                (siteChatgroupId != null ? "siteChatgroupId=" + siteChatgroupId + ", " : "") +
            "}";
    }

}
