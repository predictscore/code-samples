package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsSummaryComment;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsSummaryComment entity.
 */
public interface NewsSummaryCommentSearchRepository extends ElasticsearchRepository<NewsSummaryComment, Long> {
}
