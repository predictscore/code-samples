package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.Language;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.LanguageRepository;
import biz.newparadigm.meridian.repository.search.LanguageSearchRepository;
import biz.newparadigm.meridian.service.dto.LanguageCriteria;


/**
 * Service for executing complex queries for Language entities in the database.
 * The main input is a {@link LanguageCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link Language} or a {@link Page} of {%link Language} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class LanguageQueryService extends QueryService<Language> {

    private final Logger log = LoggerFactory.getLogger(LanguageQueryService.class);


    private final LanguageRepository languageRepository;

    private final LanguageSearchRepository languageSearchRepository;

    public LanguageQueryService(LanguageRepository languageRepository, LanguageSearchRepository languageSearchRepository) {
        this.languageRepository = languageRepository;
        this.languageSearchRepository = languageSearchRepository;
    }

    /**
     * Return a {@link List} of {%link Language} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Language> findByCriteria(LanguageCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Language> specification = createSpecification(criteria);
        return languageRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link Language} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Language> findByCriteria(LanguageCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Language> specification = createSpecification(criteria);
        return languageRepository.findAll(specification, page);
    }

    /**
     * Function to convert LanguageCriteria to a {@link Specifications}
     */
    private Specifications<Language> createSpecification(LanguageCriteria criteria) {
        Specifications<Language> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Language_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Language_.name));
            }
        }
        return specification;
    }

}
