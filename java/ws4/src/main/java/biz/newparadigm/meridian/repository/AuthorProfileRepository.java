package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.AuthorProfile;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AuthorProfile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuthorProfileRepository extends JpaRepository<AuthorProfile, Long>, JpaSpecificationExecutor<AuthorProfile> {

}
