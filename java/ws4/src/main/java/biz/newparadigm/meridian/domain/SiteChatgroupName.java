package biz.newparadigm.meridian.domain;

import biz.newparadigm.meridian.service.view.TestView;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiParam;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "site_chatgroup_names")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "sitechatgroupname")
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class SiteChatgroupName implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="site_chatgroup_names_id_seq")
    @SequenceGenerator(name="site_chatgroup_names_id_seq", sequenceName="site_chatgroup_names_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "original_name", nullable = false)
    private String originalName;

    @NotNull
    @Column(name = "english_name", nullable = false)
    private String englishName;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"comments", "domains", "ips", "names", "infos", "numVisitors", "numUsers", "languages", "tags", "authors"})
    private SiteChatgroup siteChatgroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public SiteChatgroupName originalName(String originalName) {
        this.originalName = originalName;
        return this;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public SiteChatgroupName englishName(String englishName) {
        this.englishName = englishName;
        return this;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public SiteChatgroup getSiteChatgroup() {
        return siteChatgroup;
    }

    public SiteChatgroupName siteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
        return this;
    }

    public void setSiteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SiteChatgroupName siteChatgroupName = (SiteChatgroupName) o;
        if (siteChatgroupName.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), siteChatgroupName.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SiteChatgroupName{" +
            "id=" + getId() +
            ", originalName='" + getOriginalName() + "'" +
            ", englishName='" + getEnglishName() + "'" +
            "}";
    }
}
