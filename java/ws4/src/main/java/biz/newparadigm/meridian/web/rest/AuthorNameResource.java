package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.AuthorName;
import biz.newparadigm.meridian.service.AuthorNameService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.AuthorNameCriteria;
import biz.newparadigm.meridian.service.AuthorNameQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AuthorName.
 */
@RestController
@RequestMapping("/api")
public class AuthorNameResource {

    private final Logger log = LoggerFactory.getLogger(AuthorNameResource.class);

    private static final String ENTITY_NAME = "authorName";

    private final AuthorNameService authorNameService;

    private final AuthorNameQueryService authorNameQueryService;

    public AuthorNameResource(AuthorNameService authorNameService, AuthorNameQueryService authorNameQueryService) {
        this.authorNameService = authorNameService;
        this.authorNameQueryService = authorNameQueryService;
    }

    /**
     * POST  /author-names : Create a new authorName.
     *
     * @param authorName the authorName to create
     * @return the ResponseEntity with status 201 (Created) and with body the new authorName, or with status 400 (Bad Request) if the authorName has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/author-names")
    @Timed
    public ResponseEntity<AuthorName> createAuthorName(@Valid @RequestBody AuthorName authorName) throws URISyntaxException {
        log.debug("REST request to save AuthorName : {}", authorName);
        if (authorName.getId() != null) {
            throw new BadRequestAlertException("A new authorName cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuthorName result = authorNameService.save(authorName);
        return ResponseEntity.created(new URI("/api/author-names/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /author-names : Updates an existing authorName.
     *
     * @param authorName the authorName to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated authorName,
     * or with status 400 (Bad Request) if the authorName is not valid,
     * or with status 500 (Internal Server Error) if the authorName couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/author-names")
    @Timed
    public ResponseEntity<AuthorName> updateAuthorName(@Valid @RequestBody AuthorName authorName) throws URISyntaxException {
        log.debug("REST request to update AuthorName : {}", authorName);
        if (authorName.getId() == null) {
            return createAuthorName(authorName);
        }
        AuthorName result = authorNameService.save(authorName);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, authorName.getId().toString()))
            .body(result);
    }

    /**
     * GET  /author-names : get all the authorNames.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of authorNames in body
     */
    @GetMapping("/author-names")
    @Timed
    public ResponseEntity<List<AuthorName>> getAllAuthorNames(AuthorNameCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get AuthorNames by criteria: {}", criteria);
        Page<AuthorName> page = authorNameQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/author-names");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /author-names/:id : get the "id" authorName.
     *
     * @param id the id of the authorName to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the authorName, or with status 404 (Not Found)
     */
    @GetMapping("/author-names/{id}")
    @Timed
    public ResponseEntity<AuthorName> getAuthorName(@PathVariable Long id) {
        log.debug("REST request to get AuthorName : {}", id);
        AuthorName authorName = authorNameService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(authorName));
    }

    /**
     * DELETE  /author-names/:id : delete the "id" authorName.
     *
     * @param id the id of the authorName to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/author-names/{id}")
    @Timed
    public ResponseEntity<Void> deleteAuthorName(@PathVariable Long id) {
        log.debug("REST request to delete AuthorName : {}", id);
        authorNameService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/author-names?query=:query : search for the authorName corresponding
     * to the query.
     *
     * @param query the query of the authorName search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/author-names")
    @Timed
    public ResponseEntity<List<AuthorName>> searchAuthorNames(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of AuthorNames for query {}", query);
        Page<AuthorName> page = authorNameService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/author-names");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
