package biz.newparadigm.meridian.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the NewsPublicImage entity. This class is used in NewsPublicImageResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /news-public-images?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NewsPublicImageCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter filename;

    private StringFilter description;

    private StringFilter descriptionTranslation;

    private IntegerFilter imageOrder;

    private LongFilter newsId;

    public NewsPublicImageCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getFilename() {
        return filename;
    }

    public void setFilename(StringFilter filename) {
        this.filename = filename;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getDescriptionTranslation() {
        return descriptionTranslation;
    }

    public void setDescriptionTranslation(StringFilter descriptionTranslation) {
        this.descriptionTranslation = descriptionTranslation;
    }

    public IntegerFilter getImageOrder() {
        return imageOrder;
    }

    public void setImageOrder(IntegerFilter imageOrder) {
        this.imageOrder = imageOrder;
    }

    public LongFilter getNewsId() {
        return newsId;
    }

    public void setNewsId(LongFilter newsId) {
        this.newsId = newsId;
    }

    @Override
    public String toString() {
        return "NewsPublicImageCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (filename != null ? "filename=" + filename + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (descriptionTranslation != null ? "descriptionTranslation=" + descriptionTranslation + ", " : "") +
                (imageOrder != null ? "imageOrder=" + imageOrder + ", " : "") +
                (newsId != null ? "newsId=" + newsId + ", " : "") +
            "}";
    }

}
