package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.NewsToLanguage;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsToLanguage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsToLanguageRepository extends JpaRepository<NewsToLanguage, Long>, JpaSpecificationExecutor<NewsToLanguage> {

}
