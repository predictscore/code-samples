package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsToTag;
import biz.newparadigm.meridian.repository.NewsToTagRepository;
import biz.newparadigm.meridian.repository.search.NewsToTagSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsToTag.
 */
@Service
@Transactional
public class NewsToTagService {

    private final Logger log = LoggerFactory.getLogger(NewsToTagService.class);

    private final NewsToTagRepository newsToTagRepository;

    private final NewsToTagSearchRepository newsToTagSearchRepository;

    public NewsToTagService(NewsToTagRepository newsToTagRepository, NewsToTagSearchRepository newsToTagSearchRepository) {
        this.newsToTagRepository = newsToTagRepository;
        this.newsToTagSearchRepository = newsToTagSearchRepository;
    }

    /**
     * Save a newsToTag.
     *
     * @param newsToTag the entity to save
     * @return the persisted entity
     */
    public NewsToTag save(NewsToTag newsToTag) {
        log.debug("Request to save NewsToTag : {}", newsToTag);
        NewsToTag result = newsToTagRepository.save(newsToTag);
        newsToTagSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsToTags.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsToTag> findAll(Pageable pageable) {
        log.debug("Request to get all NewsToTags");
        return newsToTagRepository.findAll(pageable);
    }

    /**
     *  Get one newsToTag by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsToTag findOne(Long id) {
        log.debug("Request to get NewsToTag : {}", id);
        return newsToTagRepository.findOne(id);
    }

    /**
     *  Delete the  newsToTag by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsToTag : {}", id);
        newsToTagRepository.delete(id);
        newsToTagSearchRepository.delete(id);
    }

    /**
     * Search for the newsToTag corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsToTag> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsToTags for query {}", query);
        Page<NewsToTag> result = newsToTagSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
