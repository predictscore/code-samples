package biz.newparadigm.meridian.domain;

import biz.newparadigm.meridian.domain.metadata.Metagroups;
import biz.newparadigm.meridian.domain.util.MeridianDateSerializer;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.*;

@Entity
@Table(name = "news")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "news")
public class News extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="news_id_seq")
    @SequenceGenerator(name="news_id_seq", sequenceName="news_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "datetime", nullable = false)
    @JsonSerialize(using = MeridianDateSerializer.class)
    private ZonedDateTime datetime;

    @NotNull
    @Column(name = "original_subject", nullable = false)
    private String originalSubject;

    @Column(name = "english_subject")
    private String englishSubject;

    @NotNull
    @Column(name = "original_post", nullable = false)
    private String originalPost;

    @Column(name = "english_post")
    private String englishPost;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"profiles", "comments", "contacts", "tags", "languages"})
    private Author author;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"comments", "domains", "ips", "infos", "numVisitors", "numUsers", "languages", "tags", "authors"})
    private SiteChatgroup siteChatgroup;

    @ManyToOne(optional = false)
    @NotNull
    private Location location;

    @OneToMany(mappedBy = "news", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<NewsAttachment> attachments = new ArrayList<>();

    @OneToMany(mappedBy = "news", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<NewsOriginalImage> originalImages = new ArrayList<>();

    @OneToMany(mappedBy = "news", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<NewsPublicImage> publicImages = new ArrayList<>();

    @OneToMany(mappedBy = "news", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<NewsConfidentialImage> confidentialImages = new ArrayList<>();

    @OneToMany(mappedBy = "news", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<NewsUrl> urls = new ArrayList<>();

    @OneToMany(mappedBy = "news", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<NewsDownloadableContentUrl> downloadableContentUrls = new ArrayList<>();

    @OneToMany(mappedBy = "news", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<NewsMetadata> metadata = new ArrayList<>();

    @OneToMany(mappedBy = "news", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<NewsToTag> tags = new ArrayList<>();

    @OneToMany(mappedBy = "news", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<NewsToLanguage> languages = new ArrayList<>();

    @OneToMany(mappedBy = "news", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<NewsSummaryToNews> newsSummaries = new ArrayList<>();

    @Transient
    @JsonSerialize
    @JsonDeserialize
    private Metagroups metagroups;

    @Version
    private long version = 0L;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDatetime() {
        return datetime;
    }

    public News datetime(ZonedDateTime datetime) {
        this.datetime = datetime;
        return this;
    }

    public void setDatetime(ZonedDateTime datetime) {
        this.datetime = datetime;
    }

    public String getOriginalSubject() {
        return originalSubject;
    }

    public News originalSubject(String originalSubject) {
        this.originalSubject = originalSubject;
        return this;
    }

    public void setOriginalSubject(String originalSubject) {
        this.originalSubject = originalSubject;
    }

    public String getEnglishSubject() {
        return englishSubject;
    }

    public News englishSubject(String englishSubject) {
        this.englishSubject = englishSubject;
        return this;
    }

    public void setEnglishSubject(String englishSubject) {
        this.englishSubject = englishSubject;
    }

    public String getOriginalPost() {
        return originalPost;
    }

    public News originalPost(String originalPost) {
        this.originalPost = originalPost;
        return this;
    }

    public void setOriginalPost(String originalPost) {
        this.originalPost = originalPost;
    }

    public String getEnglishPost() {
        return englishPost;
    }

    public News englishPost(String englishPost) {
        this.englishPost = englishPost;
        return this;
    }

    public void setEnglishPost(String englishPost) {
        this.englishPost = englishPost;
    }

    public Author getAuthor() {
        return author;
    }

    public News author(Author author) {
        this.author = author;
        return this;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public SiteChatgroup getSiteChatgroup() {
        return siteChatgroup;
    }

    public News siteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
        return this;
    }

    public void setSiteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
    }

    public Location getLocation() {
        return location;
    }

    public News location(Location location) {
        this.location = location;
        return this;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<NewsAttachment> getAttachments() {
        return attachments;
    }

    public News attachments(List<NewsAttachment> newsAttachments) {
        this.attachments = newsAttachments;
        return this;
    }

    public News addAttachments(NewsAttachment newsAttachment) {
        this.attachments.add(newsAttachment);
        newsAttachment.setNews(this);
        return this;
    }

    public News removeAttachments(NewsAttachment newsAttachment) {
        this.attachments.remove(newsAttachment);
        newsAttachment.setNews(null);
        return this;
    }

    public void setAttachments(List<NewsAttachment> newsAttachments) {
        this.attachments = newsAttachments;
    }

    public List<NewsOriginalImage> getOriginalImages() {
        return originalImages;
    }

    public News originalImages(List<NewsOriginalImage> newsOriginalImages) {
        this.originalImages = newsOriginalImages;
        return this;
    }

    public News addOriginalImages(NewsOriginalImage newsOriginalImage) {
        this.originalImages.add(newsOriginalImage);
        newsOriginalImage.setNews(this);
        return this;
    }

    public News removeOriginalImages(NewsOriginalImage newsOriginalImage) {
        this.originalImages.remove(newsOriginalImage);
        newsOriginalImage.setNews(null);
        return this;
    }

    public void setOriginalImages(List<NewsOriginalImage> newsOriginalImages) {
        this.originalImages = newsOriginalImages;
    }

    public List<NewsPublicImage> getPublicImages() {
        return publicImages;
    }

    public News publicImages(List<NewsPublicImage> newsPublicImages) {
        this.publicImages = newsPublicImages;
        return this;
    }

    public News addPublicImages(NewsPublicImage newsPublicImage) {
        this.publicImages.add(newsPublicImage);
        newsPublicImage.setNews(this);
        return this;
    }

    public News removePublicImages(NewsPublicImage newsPublicImage) {
        this.publicImages.remove(newsPublicImage);
        newsPublicImage.setNews(null);
        return this;
    }

    public void setPublicImages(List<NewsPublicImage> newsPublicImages) {
        this.publicImages = newsPublicImages;
    }

    public List<NewsUrl> getUrls() {
        return urls;
    }

    public News urls(List<NewsUrl> newsUrls) {
        this.urls = newsUrls;
        return this;
    }

    public News addUrls(NewsUrl newsUrl) {
        this.urls.add(newsUrl);
        newsUrl.setNews(this);
        return this;
    }

    public News removeUrls(NewsUrl newsUrl) {
        this.urls.remove(newsUrl);
        newsUrl.setNews(null);
        return this;
    }

    public void setUrls(List<NewsUrl> newsUrls) {
        this.urls = newsUrls;
    }

    public List<NewsDownloadableContentUrl> getDownloadableContentUrls() {
        return downloadableContentUrls;
    }

    public News downloadableContentUrls(List<NewsDownloadableContentUrl> newsDownloadableContentUrls) {
        this.downloadableContentUrls = newsDownloadableContentUrls;
        return this;
    }

    public News addDownloadableContentUrls(NewsDownloadableContentUrl newsDownloadableContentUrl) {
        this.downloadableContentUrls.add(newsDownloadableContentUrl);
        newsDownloadableContentUrl.setNews(this);
        return this;
    }

    public News removeDownloadableContentUrls(NewsDownloadableContentUrl newsDownloadableContentUrl) {
        this.downloadableContentUrls.remove(newsDownloadableContentUrl);
        newsDownloadableContentUrl.setNews(null);
        return this;
    }

    public void setDownloadableContentUrls(List<NewsDownloadableContentUrl> newsDownloadableContentUrls) {
        this.downloadableContentUrls = newsDownloadableContentUrls;
    }

    public List<NewsConfidentialImage> getConfidentialImages() {
        return confidentialImages;
    }

    public News confidentialImages(List<NewsConfidentialImage> newsConfidentialImages) {
        this.confidentialImages = newsConfidentialImages;
        return this;
    }

    public News addConfidentialImages(NewsConfidentialImage newsConfidentialImage) {
        this.confidentialImages.add(newsConfidentialImage);
        newsConfidentialImage.setNews(this);
        return this;
    }

    public News removeConfidentialImages(NewsConfidentialImage newsConfidentialImage) {
        this.confidentialImages.remove(newsConfidentialImage);
        newsConfidentialImage.setNews(null);
        return this;
    }

    public void setConfidentialImages(List<NewsConfidentialImage> newsConfidentialImages) {
        this.confidentialImages = newsConfidentialImages;
    }

    public List<NewsMetadata> getMetadata() {
        return metadata;
    }

    public News metadata(List<NewsMetadata> metadata) {
        this.metadata = metadata;
        return this;
    }

    public News addMetadata(NewsMetadata m) {
        this.metadata.add(m);
        m.setNews(this);
        return this;
    }

    public News removeMetadata(NewsMetadata m) {
        this.metadata.remove(m);
        m.setNews(null);
        return this;
    }

    public void setMetadata(List<NewsMetadata> metadata) {
        this.metadata = metadata;
    }

    public List<NewsToTag> getTags() {
        return tags;
    }

    public void setTags(List<NewsToTag> tags) {
        this.tags = tags;
    }

    public List<NewsToLanguage> getLanguages() {
        return languages;
    }

    public void setLanguages(List<NewsToLanguage> languages) {
        this.languages = languages;
    }

    public List<NewsSummaryToNews> getNewsSummaries() {
        return newsSummaries;
    }

    public void setNewsSummaries(List<NewsSummaryToNews> newsSummaries) {
        this.newsSummaries = newsSummaries;
    }

// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public Metagroups getMetagroups() {
        return metagroups;
    }

    public void setMetagroups(Metagroups metagroups) {
        this.metagroups = metagroups;
    }

    public News toJPASimpleObject() {
        News news = new News();
        news.setId(this.getId());
        return news;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        News news = (News) o;
        if (news.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), news.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "News{" +
            "id=" + getId() +
            ", datetime='" + getDatetime() + "'" +
            ", originalSubject='" + getOriginalSubject() + "'" +
            ", englishSubject='" + getEnglishSubject() + "'" +
            ", originalPost='" + getOriginalPost() + "'" +
            ", englishPost='" + getEnglishPost() + "'" +
            "}";
    }
}
