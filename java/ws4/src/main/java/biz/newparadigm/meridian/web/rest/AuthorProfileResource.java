package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.AuthorProfile;
import biz.newparadigm.meridian.service.AuthorProfileService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.AuthorProfileCriteria;
import biz.newparadigm.meridian.service.AuthorProfileQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AuthorProfile.
 */
@RestController
@RequestMapping("/api")
public class AuthorProfileResource {

    private final Logger log = LoggerFactory.getLogger(AuthorProfileResource.class);

    private static final String ENTITY_NAME = "authorProfile";

    private final AuthorProfileService authorProfileService;

    private final AuthorProfileQueryService authorProfileQueryService;

    public AuthorProfileResource(AuthorProfileService authorProfileService, AuthorProfileQueryService authorProfileQueryService) {
        this.authorProfileService = authorProfileService;
        this.authorProfileQueryService = authorProfileQueryService;
    }

    /**
     * POST  /author-profiles : Create a new authorProfile.
     *
     * @param authorProfile the authorProfile to create
     * @return the ResponseEntity with status 201 (Created) and with body the new authorProfile, or with status 400 (Bad Request) if the authorProfile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/author-profiles")
    @Timed
    public ResponseEntity<AuthorProfile> createAuthorProfile(@Valid @RequestBody AuthorProfile authorProfile) throws URISyntaxException {
        log.debug("REST request to save AuthorProfile : {}", authorProfile);
        if (authorProfile.getId() != null) {
            throw new BadRequestAlertException("A new authorProfile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuthorProfile result = authorProfileService.save(authorProfile);
        return ResponseEntity.created(new URI("/api/author-profiles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /author-profiles : Updates an existing authorProfile.
     *
     * @param authorProfile the authorProfile to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated authorProfile,
     * or with status 400 (Bad Request) if the authorProfile is not valid,
     * or with status 500 (Internal Server Error) if the authorProfile couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/author-profiles")
    @Timed
    public ResponseEntity<AuthorProfile> updateAuthorProfile(@Valid @RequestBody AuthorProfile authorProfile) throws URISyntaxException {
        log.debug("REST request to update AuthorProfile : {}", authorProfile);
        if (authorProfile.getId() == null) {
            return createAuthorProfile(authorProfile);
        }
        AuthorProfile result = authorProfileService.save(authorProfile);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, authorProfile.getId().toString()))
            .body(result);
    }

    /**
     * GET  /author-profiles : get all the authorProfiles.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of authorProfiles in body
     */
    @GetMapping("/author-profiles")
    @Timed
    public ResponseEntity<List<AuthorProfile>> getAllAuthorProfiles(AuthorProfileCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get AuthorProfiles by criteria: {}", criteria);
        Page<AuthorProfile> page = authorProfileQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/author-profiles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /author-profiles/:id : get the "id" authorProfile.
     *
     * @param id the id of the authorProfile to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the authorProfile, or with status 404 (Not Found)
     */
    @GetMapping("/author-profiles/{id}")
    @Timed
    public ResponseEntity<AuthorProfile> getAuthorProfile(@PathVariable Long id) {
        log.debug("REST request to get AuthorProfile : {}", id);
        AuthorProfile authorProfile = authorProfileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(authorProfile));
    }

    /**
     * DELETE  /author-profiles/:id : delete the "id" authorProfile.
     *
     * @param id the id of the authorProfile to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/author-profiles/{id}")
    @Timed
    public ResponseEntity<Void> deleteAuthorProfile(@PathVariable Long id) {
        log.debug("REST request to delete AuthorProfile : {}", id);
        authorProfileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/author-profiles?query=:query : search for the authorProfile corresponding
     * to the query.
     *
     * @param query the query of the authorProfile search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/author-profiles")
    @Timed
    public ResponseEntity<List<AuthorProfile>> searchAuthorProfiles(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of AuthorProfiles for query {}", query);
        Page<AuthorProfile> page = authorProfileService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/author-profiles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
