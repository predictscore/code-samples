package biz.newparadigm.meridian.domain;

import biz.newparadigm.meridian.domain.util.MeridianDateSerializer;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

@Entity
@Table(name = "site_chatgroup_num_users")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "sitechatgroupnumusers")
public class SiteChatgroupNumUsers implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="site_chatgroup_num_users_id_seq")
    @SequenceGenerator(name="site_chatgroup_num_users_id_seq", sequenceName="site_chatgroup_num_users_id_seq", allocationSize=1)
    private Long id;

    @NotNull
    @Column(name = "num_users", nullable = false)
    private Integer numUsers;

    @NotNull
    @Column(name = "num_user_date", nullable = false)
    @JsonSerialize(using = MeridianDateSerializer.class)
    private ZonedDateTime numUserDate;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties({"comments", "domains", "ips", "names", "infos", "numVisitors", "numUsers", "languages", "tags", "authors"})
    private SiteChatgroup siteChatgroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumUsers() {
        return numUsers;
    }

    public SiteChatgroupNumUsers numUsers(Integer numUsers) {
        this.numUsers = numUsers;
        return this;
    }

    public void setNumUsers(Integer numUsers) {
        this.numUsers = numUsers;
    }

    public ZonedDateTime getNumUserDate() {
        return numUserDate;
    }

    public SiteChatgroupNumUsers numUserDate(ZonedDateTime numUserDate) {
        this.numUserDate = numUserDate;
        return this;
    }

    public void setNumUserDate(ZonedDateTime numUserDate) {
        this.numUserDate = numUserDate;
    }

    public SiteChatgroup getSiteChatgroup() {
        return siteChatgroup;
    }

    public SiteChatgroupNumUsers siteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
        return this;
    }

    public void setSiteChatgroup(SiteChatgroup siteChatgroup) {
        this.siteChatgroup = siteChatgroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SiteChatgroupNumUsers siteChatgroupNumUsers = (SiteChatgroupNumUsers) o;
        if (siteChatgroupNumUsers.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), siteChatgroupNumUsers.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SiteChatgroupNumUsers{" +
            "id=" + getId() +
            ", numUsers='" + getNumUsers() + "'" +
            ", numUserDate='" + getNumUserDate() + "'" +
            "}";
    }
}
