package biz.newparadigm.meridian.repository;

import biz.newparadigm.meridian.domain.AuthorComment;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AuthorComment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuthorCommentRepository extends JpaRepository<AuthorComment, Long>, JpaSpecificationExecutor<AuthorComment> {

}
