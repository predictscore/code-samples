package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.NewsSummaryMetadata;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the NewsSummaryMetadata entity.
 */
public interface NewsSummaryMetadataSearchRepository extends ElasticsearchRepository<NewsSummaryMetadata, Long> {
}
