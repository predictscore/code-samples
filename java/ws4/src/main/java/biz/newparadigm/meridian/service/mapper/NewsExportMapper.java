package biz.newparadigm.meridian.service.mapper;

import biz.newparadigm.meridian.domain.*;
import biz.newparadigm.meridian.domain.metadata.Metagroups;
import biz.newparadigm.meridian.domain.util.MetadataConstants;
import biz.newparadigm.meridian.service.dto.export.NewsExport;
import biz.newparadigm.meridian.service.dto.export.NewsExportImgDescTrans;
import biz.newparadigm.meridian.service.util.DateTimeUtil;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class NewsExportMapper {
    public List<NewsExport> toNewsExport(List<News> news) {
        List<NewsExport> result = new ArrayList<>();
        if (news != null && !news.isEmpty()) {
            result = news.stream().map(this::toNewsExport).collect(Collectors.toList());
        }
        return result;
    }

    private NewsExport toNewsExport(News news) {
        NewsExport newsExport = new NewsExport();

        String locationName = news.getLocation().getName();
        newsExport.setIntelRegion(getString(locationName));
        newsExport.setActorRegion(getString(locationName));
        newsExport.setRegionOfActivity(getString(locationName));
        newsExport.setRecordId(news.getId().intValue());

        if (news.getMetagroups() == null) {
            news.setMetagroups(new Metagroups());
        }

        newsExport.setName(getString(news.getMetagroups().getProdOrServ().getProductServiceNameEnglish()));
        newsExport.setVersion(getString(news.getMetagroups().getProdOrServ().getProductServiceVersionEnglish()));

        List<RFIResponse> rfiList = news.getMetagroups().getMmvar().getRfiResponses();
        String analystReportNotes = null;
        if (rfiList != null && !rfiList.isEmpty()) {
            analystReportNotes = rfiList.get(0).getNote();
        }
        newsExport.setOfferClaim(getString(analystReportNotes));
        newsExport.setOfferDate(DateTimeUtil.toString(news.getDatetime(), DateTimeUtil.DATE_FORMAT_PURE_POSTGRES, ""));
        newsExport.setOfferPrice(getString(news.getMetagroups().getForProfit().getPriceEnglish()));
        newsExport.setStructuredPrice(getString(news.getMetagroups().getForProfit().getPriceEnglish()));
        newsExport.setOfferType(getString(news.getMetagroups().getMmvar().getOfferType()));
        newsExport.setPurchaserType(getString(news.getMetagroups().getForProfit().getTypicalUsers()));
        newsExport.setVendorNotes(getString(analystReportNotes));
        newsExport.setOfferClaim(getString(news.getMetagroups().getMmvar().getAnalysis()));
        newsExport.setClaimValidity(getString(news.getMetagroups().getMmvar().getConfidenceLevel()));
        newsExport.setLevelOfActivity(getString(news.getMetagroups().getLoa().getLevelOfActivity()));

        if (news.getCreatedDate() == null) {
            if (news.getDatetime() == null) {
                news.setCreatedDate(ZonedDateTime.now(ZoneId.of("UTC")));
            }
            news.setCreatedDate(news.getDatetime());
        }

        String month = news.getCreatedDate().getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH);
        int year = news.getCreatedDate().getYear();
        newsExport.setObservedMonthYear(month + "/" + year);
        newsExport.setObservedDate(DateTimeUtil.toString(news.getCreatedDate(), DateTimeUtil.DATE_FORMAT_JAVA_DATE, null));

        Author author = news.getAuthor();
        newsExport.setActor(getString(author.getEnglishName()));

        if (author.getNames() != null && !author.getNames().isEmpty()) {
            String tmpString = author.getNames().stream()
                .map(AuthorName::getEnglishName)
                .collect(Collectors.joining(","));
            newsExport.setActorAlias(getString(tmpString));
        } else {
            newsExport.setActorAlias("");
        }

        SiteChatgroup siteChatgroup = news.getSiteChatgroup();
        newsExport.setIntelForum(getString(siteChatgroup.getEnglishName()));

        if (siteChatgroup.getNames() != null && !siteChatgroup.getNames().isEmpty()) {
            String tmpString = siteChatgroup.getNames().stream()
                .map(SiteChatgroupName::getEnglishName)
                .collect(Collectors.joining(","));
            newsExport.setIntelForumAlias(getString(tmpString));
        } else {
            newsExport.setIntelForumAlias("");
        }

        List<SiteChatgroupToLanguage> siteChatgroupLanguages = siteChatgroup.getLanguages();
        if (siteChatgroupLanguages != null && !siteChatgroupLanguages.isEmpty()) {
            newsExport.setIntelForumLanguage(getString(siteChatgroupLanguages.get(0).getLanguage().getName()));
        } else {
            newsExport.setIntelForumLanguage("");
        }

        List<String> cveNames = news.getMetagroups().getVulnerability().getCve();
        newsExport.setCves((cveNames != null && !cveNames.isEmpty()) ? cveNames : new ArrayList<>());

        List<MitigationsBypassed> mitigationsBypassed = news.getMetagroups().getVulnerability().getMitigationsBypassed();
        List<String> mbpNameList = new ArrayList<>();
        if (mitigationsBypassed != null && !mitigationsBypassed.isEmpty()) {
            mitigationsBypassed.forEach(item -> mbpNameList.add(getString(item.getDescription())));
        }
        newsExport.setMitigationsByPassed(mbpNameList);

        List<NewsToLanguage> languages = news.getLanguages();
        List<String> langNames = new ArrayList<>();
        for (NewsToLanguage language : languages) {
            langNames.add(language.getLanguage().getName());
        }
        newsExport.setOfferLanguages(langNames);

        List<Language> uiLanguages = news.getMetagroups().getProdOrServ().getProductServiceLanguages();
        List<String> uiLangNames = new ArrayList<>();
        if (uiLanguages != null && !uiLanguages.isEmpty()) {
            uiLanguages.forEach(item -> uiLangNames.add(item.getName()));
        }
        newsExport.setUiLanguages(uiLangNames);

        Category microCategory = news.getMetagroups().getMmvar().getCategory();
        if (microCategory != null) {
            newsExport.setIntelMicroCategory(microCategory.getName());
            if (microCategory.getParent() != null) {
                newsExport.setIntelSubCategory(microCategory.getParent().getName());
                if (microCategory.getParent().getParent() != null) {
                    newsExport.setIntelCategory(microCategory.getParent().getParent().getName());
                }
            }
        }

        List<NewsConfidentialImage> offerRestrictedImages = news.getConfidentialImages();
        List<NewsExportImgDescTrans> tmp = new ArrayList<>();
        for (NewsConfidentialImage offerRestrictedImage : offerRestrictedImages) {
            NewsExportImgDescTrans newsExportImgDescTrans = new NewsExportImgDescTrans();
            newsExportImgDescTrans.setFilename(offerRestrictedImage.getFilename());
            newsExportImgDescTrans.setDescription(offerRestrictedImage.getDescription());
            newsExportImgDescTrans.setTranslation(offerRestrictedImage.getDescriptionTranslation());
            if (offerRestrictedImage.getBlob() != null) {
                newsExportImgDescTrans.setImage(new String(offerRestrictedImage.getBlob()));
            }
            tmp.add(newsExportImgDescTrans);
        }
        newsExport.setOfferRestrictedImages(tmp);

        List<NewsPublicImage> newsEditedImagesCopyPaste = news.getPublicImages();
        List<NewsExportImgDescTrans> nonSensitiveOfferImages = new ArrayList<>();
        for (NewsPublicImage newsEditedImageCopyPaste : newsEditedImagesCopyPaste) {
            NewsExportImgDescTrans nonSensitiveOfferImage = new NewsExportImgDescTrans();
            nonSensitiveOfferImage.setFilename(newsEditedImageCopyPaste.getFilename());
            nonSensitiveOfferImage.setDescription(newsEditedImageCopyPaste.getDescription());
            nonSensitiveOfferImage.setTranslation(newsEditedImageCopyPaste.getDescriptionTranslation());
            nonSensitiveOfferImage.setImage(new String(newsEditedImageCopyPaste.getBlob()));
            nonSensitiveOfferImages.add(nonSensitiveOfferImage);
        }
        newsExport.setNonSensitiveOfferImages(nonSensitiveOfferImages);

        List<NewsAttachment> newsAttachments = news.getAttachments();
        List<NewsExportImgDescTrans> newsExportAttachments = new ArrayList<>();
        for (NewsAttachment newsAttachment : newsAttachments) {
            NewsExportImgDescTrans newsExportAttachment = new NewsExportImgDescTrans();
            newsExportAttachment.setFilename(newsAttachment.getFilename());
            newsExportAttachment.setDescription(newsAttachment.getDescription());
            newsExportAttachment.setImage(new String(newsAttachment.getBlob()));
            newsExportAttachments.add(newsExportAttachment);
        }
        newsExport.setAttachments(newsExportAttachments);

        newsExport.setReportedDate(DateTimeUtil.toString(ZonedDateTime.now(ZoneId.of("UTC")), DateTimeUtil.DATE_FORMAT_JAVA_DATE, null));
        return newsExport;
    }

    private String getString(Object obj) {
        return obj == null ? "" : obj.toString();
    }
}
