package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsSummaryConfidentialImage;
import biz.newparadigm.meridian.repository.NewsSummaryConfidentialImageRepository;
import biz.newparadigm.meridian.repository.search.NewsSummaryConfidentialImageSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsSummaryConfidentialImage.
 */
@Service
@Transactional
public class NewsSummaryConfidentialImageService {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryConfidentialImageService.class);

    private final NewsSummaryConfidentialImageRepository newsSummaryConfidentialImageRepository;

    private final NewsSummaryConfidentialImageSearchRepository newsSummaryConfidentialImageSearchRepository;

    public NewsSummaryConfidentialImageService(NewsSummaryConfidentialImageRepository newsSummaryConfidentialImageRepository, NewsSummaryConfidentialImageSearchRepository newsSummaryConfidentialImageSearchRepository) {
        this.newsSummaryConfidentialImageRepository = newsSummaryConfidentialImageRepository;
        this.newsSummaryConfidentialImageSearchRepository = newsSummaryConfidentialImageSearchRepository;
    }

    /**
     * Save a newsSummaryConfidentialImage.
     *
     * @param newsSummaryConfidentialImage the entity to save
     * @return the persisted entity
     */
    public NewsSummaryConfidentialImage save(NewsSummaryConfidentialImage newsSummaryConfidentialImage) {
        log.debug("Request to save NewsSummaryConfidentialImage : {}", newsSummaryConfidentialImage);
        NewsSummaryConfidentialImage result = newsSummaryConfidentialImageRepository.save(newsSummaryConfidentialImage);
        newsSummaryConfidentialImageSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsSummaryConfidentialImages.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryConfidentialImage> findAll(Pageable pageable) {
        log.debug("Request to get all NewsSummaryConfidentialImages");
        return newsSummaryConfidentialImageRepository.findAll(pageable);
    }

    /**
     *  Get one newsSummaryConfidentialImage by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsSummaryConfidentialImage findOne(Long id) {
        log.debug("Request to get NewsSummaryConfidentialImage : {}", id);
        return newsSummaryConfidentialImageRepository.findOne(id);
    }

    /**
     *  Delete the  newsSummaryConfidentialImage by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsSummaryConfidentialImage : {}", id);
        newsSummaryConfidentialImageRepository.delete(id);
        newsSummaryConfidentialImageSearchRepository.delete(id);
    }

    /**
     * Search for the newsSummaryConfidentialImage corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryConfidentialImage> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsSummaryConfidentialImages for query {}", query);
        Page<NewsSummaryConfidentialImage> result = newsSummaryConfidentialImageSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
