package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.AuthorComment;
import biz.newparadigm.meridian.service.AuthorCommentService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.AuthorCommentCriteria;
import biz.newparadigm.meridian.service.AuthorCommentQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AuthorComment.
 */
@RestController
@RequestMapping("/api")
public class AuthorCommentResource {

    private final Logger log = LoggerFactory.getLogger(AuthorCommentResource.class);

    private static final String ENTITY_NAME = "authorComment";

    private final AuthorCommentService authorCommentService;

    private final AuthorCommentQueryService authorCommentQueryService;

    public AuthorCommentResource(AuthorCommentService authorCommentService, AuthorCommentQueryService authorCommentQueryService) {
        this.authorCommentService = authorCommentService;
        this.authorCommentQueryService = authorCommentQueryService;
    }

    /**
     * POST  /author-comments : Create a new authorComment.
     *
     * @param authorComment the authorComment to create
     * @return the ResponseEntity with status 201 (Created) and with body the new authorComment, or with status 400 (Bad Request) if the authorComment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/author-comments")
    @Timed
    public ResponseEntity<AuthorComment> createAuthorComment(@Valid @RequestBody AuthorComment authorComment) throws URISyntaxException {
        log.debug("REST request to save AuthorComment : {}", authorComment);
        if (authorComment.getId() != null) {
            throw new BadRequestAlertException("A new authorComment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuthorComment result = authorCommentService.save(authorComment);
        return ResponseEntity.created(new URI("/api/author-comments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /author-comments : Updates an existing authorComment.
     *
     * @param authorComment the authorComment to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated authorComment,
     * or with status 400 (Bad Request) if the authorComment is not valid,
     * or with status 500 (Internal Server Error) if the authorComment couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/author-comments")
    @Timed
    public ResponseEntity<AuthorComment> updateAuthorComment(@Valid @RequestBody AuthorComment authorComment) throws URISyntaxException {
        log.debug("REST request to update AuthorComment : {}", authorComment);
        if (authorComment.getId() == null) {
            return createAuthorComment(authorComment);
        }
        AuthorComment result = authorCommentService.save(authorComment);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, authorComment.getId().toString()))
            .body(result);
    }

    /**
     * GET  /author-comments : get all the authorComments.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of authorComments in body
     */
    @GetMapping("/author-comments")
    @Timed
    public ResponseEntity<List<AuthorComment>> getAllAuthorComments(AuthorCommentCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get AuthorComments by criteria: {}", criteria);
        Page<AuthorComment> page = authorCommentQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/author-comments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /author-comments/:id : get the "id" authorComment.
     *
     * @param id the id of the authorComment to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the authorComment, or with status 404 (Not Found)
     */
    @GetMapping("/author-comments/{id}")
    @Timed
    public ResponseEntity<AuthorComment> getAuthorComment(@PathVariable Long id) {
        log.debug("REST request to get AuthorComment : {}", id);
        AuthorComment authorComment = authorCommentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(authorComment));
    }

    /**
     * DELETE  /author-comments/:id : delete the "id" authorComment.
     *
     * @param id the id of the authorComment to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/author-comments/{id}")
    @Timed
    public ResponseEntity<Void> deleteAuthorComment(@PathVariable Long id) {
        log.debug("REST request to delete AuthorComment : {}", id);
        authorCommentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/author-comments?query=:query : search for the authorComment corresponding
     * to the query.
     *
     * @param query the query of the authorComment search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/author-comments")
    @Timed
    public ResponseEntity<List<AuthorComment>> searchAuthorComments(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of AuthorComments for query {}", query);
        Page<AuthorComment> page = authorCommentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/author-comments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
