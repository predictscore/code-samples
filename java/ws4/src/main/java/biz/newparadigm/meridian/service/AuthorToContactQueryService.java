package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.AuthorToContact;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.AuthorToContactRepository;
import biz.newparadigm.meridian.repository.search.AuthorToContactSearchRepository;
import biz.newparadigm.meridian.service.dto.AuthorToContactCriteria;


/**
 * Service for executing complex queries for AuthorToContact entities in the database.
 * The main input is a {@link AuthorToContactCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link AuthorToContact} or a {@link Page} of {%link AuthorToContact} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class AuthorToContactQueryService extends QueryService<AuthorToContact> {

    private final Logger log = LoggerFactory.getLogger(AuthorToContactQueryService.class);


    private final AuthorToContactRepository authorToContactRepository;

    private final AuthorToContactSearchRepository authorToContactSearchRepository;

    public AuthorToContactQueryService(AuthorToContactRepository authorToContactRepository, AuthorToContactSearchRepository authorToContactSearchRepository) {
        this.authorToContactRepository = authorToContactRepository;
        this.authorToContactSearchRepository = authorToContactSearchRepository;
    }

    /**
     * Return a {@link List} of {%link AuthorToContact} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuthorToContact> findByCriteria(AuthorToContactCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<AuthorToContact> specification = createSpecification(criteria);
        return authorToContactRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link AuthorToContact} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuthorToContact> findByCriteria(AuthorToContactCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<AuthorToContact> specification = createSpecification(criteria);
        return authorToContactRepository.findAll(specification, page);
    }

    /**
     * Function to convert AuthorToContactCriteria to a {@link Specifications}
     */
    private Specifications<AuthorToContact> createSpecification(AuthorToContactCriteria criteria) {
        Specifications<AuthorToContact> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AuthorToContact_.id));
            }
            if (criteria.getContactValue() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactValue(), AuthorToContact_.contactValue));
            }
            if (criteria.getAuthorId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getAuthorId(), AuthorToContact_.author, Author_.id));
            }
            if (criteria.getContactTypeId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getContactTypeId(), AuthorToContact_.contactType, ContactType_.id));
            }
        }
        return specification;
    }

}
