package biz.newparadigm.meridian.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.NewsSummaryMetadata;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.NewsSummaryMetadataRepository;
import biz.newparadigm.meridian.repository.search.NewsSummaryMetadataSearchRepository;
import biz.newparadigm.meridian.service.dto.NewsSummaryMetadataCriteria;


/**
 * Service for executing complex queries for NewsSummaryMetadata entities in the database.
 * The main input is a {@link NewsSummaryMetadataCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link NewsSummaryMetadata} or a {@link Page} of {%link NewsSummaryMetadata} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class NewsSummaryMetadataQueryService extends QueryService<NewsSummaryMetadata> {

    private final Logger log = LoggerFactory.getLogger(NewsSummaryMetadataQueryService.class);


    private final NewsSummaryMetadataRepository newsSummaryMetadataRepository;

    private final NewsSummaryMetadataSearchRepository newsSummaryMetadataSearchRepository;

    public NewsSummaryMetadataQueryService(NewsSummaryMetadataRepository newsSummaryMetadataRepository, NewsSummaryMetadataSearchRepository newsSummaryMetadataSearchRepository) {
        this.newsSummaryMetadataRepository = newsSummaryMetadataRepository;
        this.newsSummaryMetadataSearchRepository = newsSummaryMetadataSearchRepository;
    }

    /**
     * Return a {@link List} of {%link NewsSummaryMetadata} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NewsSummaryMetadata> findByCriteria(NewsSummaryMetadataCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<NewsSummaryMetadata> specification = createSpecification(criteria);
        return newsSummaryMetadataRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link NewsSummaryMetadata} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NewsSummaryMetadata> findByCriteria(NewsSummaryMetadataCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<NewsSummaryMetadata> specification = createSpecification(criteria);
        return newsSummaryMetadataRepository.findAll(specification, page);
    }

    /**
     * Function to convert NewsSummaryMetadataCriteria to a {@link Specifications}
     */
    private Specifications<NewsSummaryMetadata> createSpecification(NewsSummaryMetadataCriteria criteria) {
        Specifications<NewsSummaryMetadata> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NewsSummaryMetadata_.id));
            }
            if (criteria.getValueString() != null) {
                specification = specification.and(buildStringSpecification(criteria.getValueString(), NewsSummaryMetadata_.valueString));
            }
            if (criteria.getValueNumber() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValueNumber(), NewsSummaryMetadata_.valueNumber));
            }
            if (criteria.getValueDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValueDate(), NewsSummaryMetadata_.valueDate));
            }
            if (criteria.getNewsSummaryId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getNewsSummaryId(), NewsSummaryMetadata_.newsSummary, NewsSummary_.id));
            }
            if (criteria.getMetagroupId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getMetagroupId(), NewsSummaryMetadata_.metagroup, Metagroup_.id));
            }
            if (criteria.getMetafieldId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getMetafieldId(), NewsSummaryMetadata_.metafield, Metafield_.id));
            }
        }
        return specification;
    }

}
