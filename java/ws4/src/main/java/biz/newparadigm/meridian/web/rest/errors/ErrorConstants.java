package biz.newparadigm.meridian.web.rest.errors;

import java.net.URI;

public final class ErrorConstants {
    public static final String ERR_CONCURRENCY_FAILURE = "Concurrency Failure";
    public static final String ERR_ACCESS_DENIED = "Access Denied";
    public static final String ERR_VALIDATION = "Validation Error";
    public static final String ERR_METHOD_NOT_SUPPORTED = "Method Not Supported";
    public static final String ERR_INTERNAL_SERVER_ERROR = "Internal Server Error";
    public static final String ERR_BAD_REQUEST = "Bad Request";
    public static final String ERR_CONSTRAINT_VIOLATION = "Constraint Violation Exception";

    public static final String PROBLEM_BASE_URL = "http://www.jhipster.tech/problem";
    public static final URI DEFAULT_TYPE = URI.create(PROBLEM_BASE_URL + "/problem-with-message");
    public static final URI CONSTRAINT_VIOLATION_TYPE = URI.create(PROBLEM_BASE_URL + "/contraint-violation");
    public static final URI PARAMETERIZED_TYPE = URI.create(PROBLEM_BASE_URL + "/parameterized");
    public static final URI INVALID_PASSWORD_TYPE = URI.create(PROBLEM_BASE_URL + "/invalid-password");
    public static final URI EMAIL_ALREADY_USED_TYPE = URI.create(PROBLEM_BASE_URL + "/email-already-used");
    public static final URI LOGIN_ALREADY_USED_TYPE = URI.create(PROBLEM_BASE_URL + "/login-already-used");
    public static final URI EMAIL_NOT_FOUND_TYPE = URI.create(PROBLEM_BASE_URL + "/email-not-found");

    private ErrorConstants() {
    }
}
