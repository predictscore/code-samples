package biz.newparadigm.meridian.service.query;

import biz.newparadigm.meridian.domain.*;
import biz.newparadigm.meridian.service.filter.SpecificationBuilder;
import biz.newparadigm.meridian.service.odata.Expression;
import biz.newparadigm.meridian.web.rest.AuthorResource;
import biz.newparadigm.meridian.web.rest.NewsResource;
import biz.newparadigm.meridian.web.rest.SiteChatgroupResource;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Transactional(readOnly = true)
public class MeridianQueryService<ENTITY> {
    private static final String OPERATION_NOT = "NOT";
    private static final String OPERATION_AND = "AND";
    private static final String OPERATION_OR = "OR";
    private static final String OPERATION_EQ = "eq";
    private static final String OPERATION_NE = "ne";
    private static final String OPERATION_GT = "gt";
    private static final String OPERATION_GE = "ge";
    private static final String OPERATION_LT = "lt";
    private static final String OPERATION_LE = "le";
    private static final String OPERATION_CONTAINS = "contains";
    private static final String OPERATION_STARTSWITH = "startswith";
    private static final String OPERATION_ENDSWITH = "endswith";

    private static final List<String> newsManyToOneFields = Arrays.asList("author", "siteChatgroup", "location");
    private static final List<String> newsOneToManyFields = Arrays.asList("comments", "attachments", "originalImages", "publicImages", "confidentialImages", "urls", "downloadableContentUrls");
    private static final List<String> newsManyToManyFields = Arrays.asList("tags", "languages");
    private static final Map<String, String> newsManyToManyFieldsMapping = Collections.unmodifiableMap(
        Stream.of(new AbstractMap.SimpleEntry<>("tags", "tag"), new AbstractMap.SimpleEntry<>("languages", "language"))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

    private static final List<String> authorManyToOneFields = Collections.singletonList("location");
    private static final List<String> authorOneToManyFields = Arrays.asList("names", "contacts", "siteChatGroups");
    private static final List<String> authorManyToManyFields = Arrays.asList("tags", "languages");
    private static final Map<String, String> authorManyToManyFieldsMapping = Collections.unmodifiableMap(
        Stream.of(new AbstractMap.SimpleEntry<>("tags", "tag"), new AbstractMap.SimpleEntry<>("languages", "language"))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

    private static final List<String> siteChatgroupManyToOneFields = Collections.singletonList("location");
    private static final List<String> siteChatgroupOneToManyFields = Arrays.asList("domains", "ips", "names", "numVisitors", "numUsers", "authors");
    private static final List<String> siteChatgroupManyToManyFields = Arrays.asList("tags", "languages");
    private static final Map<String, String> siteChatgroupManyToManyFieldsMapping = Collections.unmodifiableMap(
        Stream.of(new AbstractMap.SimpleEntry<>("tags", "tag"), new AbstractMap.SimpleEntry<>("languages", "language"))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

    private String entityName;

    public MeridianQueryService(String entityName) {
        this.entityName = entityName == null ? "" : entityName;
    }

    public Specifications<ENTITY> createFilterSpecification(Expression filter, boolean filterByLocation, List<String> locationNames) {
        Specifications<ENTITY> specification = Specifications.where(null);
        if (filter instanceof Expression.Binary) {
            specification = getExpressionSpecification(filter, ((Expression.Binary) filter).getOp(), Specifications.where(null));
        } else if (filter instanceof Expression.Unary) {
            specification = getExpressionSpecification(((Expression.Unary) filter).getExpr(), MeridianQueryService.OPERATION_NOT, Specifications.not(null)) ;
        }
        if (filterByLocation) {
            SpecificationBuilder<ENTITY> builder = new SpecificationBuilder<>();
            if (locationNames == null || locationNames.isEmpty()) {
                specification = specification.and(builder.buildNoResultSpecification("id"));
            } else {
                if (entityName.equalsIgnoreCase(AuthorResource.ENTITY_NAME)) {
                    specification = specification.and(builder.buildLocationsSpecification(locationNames, Author.class));
                } else if (entityName.equalsIgnoreCase(SiteChatgroupResource.ENTITY_NAME)) {
                    specification = specification.and(builder.buildLocationsSpecification(locationNames, SiteChatgroup.class));
                } else if (entityName.equalsIgnoreCase(NewsResource.ENTITY_NAME)) {
                    specification = specification.and(builder.buildLocationsSpecification(locationNames, News.class));
                }
            }
        }
        return specification;
    }

    private Specifications<ENTITY> getExpressionSpecification(Expression expression, String operator, Specifications<ENTITY> specification) {
        if (operator.equalsIgnoreCase(MeridianQueryService.OPERATION_OR) || operator.equalsIgnoreCase(MeridianQueryService.OPERATION_AND)) {
            Specifications<ENTITY> left;
            Expression leftExpression = null;
            String leftExpressionOp = null;
            if (((Expression.Binary) expression).getX1() instanceof Expression.Binary) {
                leftExpression = ((Expression.Binary) expression).getX1();
                leftExpressionOp = ((Expression.Binary) ((Expression.Binary) expression).getX1()).getOp();
            } else if (((Expression.Binary) expression).getX1() instanceof Expression.Unary) {
                leftExpression = ((Expression.Unary) ((Expression.Binary) expression).getX1()).getExpr();
                leftExpressionOp = MeridianQueryService.OPERATION_NOT;
            }
            left = getExpressionSpecification(leftExpression, leftExpressionOp, specification);

            Specifications<ENTITY> right;
            Expression rightExpression = null;
            String rightExpressionOp = null;
            if (((Expression.Binary) expression).getX2() instanceof Expression.Binary) {
                rightExpression = ((Expression.Binary) expression).getX2();
                rightExpressionOp = ((Expression.Binary) ((Expression.Binary) expression).getX2()).getOp();
            } else if (((Expression.Binary) expression).getX2() instanceof Expression.Unary) {
                rightExpression = ((Expression.Unary) ((Expression.Binary) expression).getX2()).getExpr();
                rightExpressionOp = MeridianQueryService.OPERATION_NOT;
            }
            right = getExpressionSpecification(rightExpression, rightExpressionOp, specification);

            if (left != null && right != null) {
                if (operator.equalsIgnoreCase(MeridianQueryService.OPERATION_AND)) {
                    return specification.and(left).and(right);
                } else {
                    return specification.and(left).or(right);
                }
            }
        } else if (operator.equalsIgnoreCase(MeridianQueryService.OPERATION_NOT)) {
            Specifications<ENTITY> notSpecification;
            String notOperator = null;
            Expression notExpression = null;
            if (expression instanceof Expression.Binary) {
                notExpression = expression;
                notOperator = ((Expression.Binary) expression).getOp();
            } else if (expression instanceof Expression.Unary) {
                notExpression = ((Expression.Unary) ((Expression.Unary) expression).getExpr()).getExpr();
                notOperator = MeridianQueryService.OPERATION_NOT;
            }
            notSpecification = getExpressionSpecification(notExpression, notOperator, specification);
            if (notSpecification != null) {
                return specification.and(Specifications.not(notSpecification));
            }
        } else {
            Specifications<ENTITY> expressionSpecification = getLowExpressionSpecification(expression, operator);
            return specification.and(expressionSpecification);
        }
        return Specifications.where(null);
    }

    private Specifications<ENTITY> getLowExpressionSpecification(Expression expression, String operator) {
        Specifications<ENTITY> specification = Specifications.where(null);
        String fieldName = Expression.getStringValue(((Expression.Binary)expression).getX1());
        String fieldValue = Expression.getStringValue(((Expression.Binary)expression).getX2());

        switch (operator.toLowerCase()) {
            case OPERATION_EQ:
                try {
                    ZonedDateTime zdt = ZonedDateTime.parse(fieldValue);
                    specification = specification.and(equalsSpecificationDate(fieldName, zdt));
                } catch (Exception e) {
                    if ((fieldValue.equalsIgnoreCase("true") || fieldValue.equalsIgnoreCase("false"))) {
                        Boolean b = Boolean.valueOf(fieldValue);
                        specification = specification.and(equalsSpecificationBoolean(fieldName, b));
                    } else {
                        specification = specification.and(equalsSpecificationString(fieldName, fieldValue));
                    }
                }
                break;
            case OPERATION_NE:
                try {
                    ZonedDateTime zdt = ZonedDateTime.parse(fieldValue);
                    specification = specification.and(notEqualsSpecification(fieldName, zdt));
                } catch (Exception e) {
                    specification = specification.and(notEqualsSpecification(fieldName, fieldValue));
                }
                break;
            case OPERATION_GT:
                try {
                    ZonedDateTime zdt = ZonedDateTime.parse(fieldValue);
                    specification = specification.and(greaterThan(fieldName, zdt));
                } catch (Exception e) {
                    specification = specification.and(greaterThan(fieldName, fieldValue));
                }
                break;
            case OPERATION_GE:
                try {
                    ZonedDateTime zdt = ZonedDateTime.parse(fieldValue);
                    specification = specification.and(greaterThanOrEqualTo(fieldName, zdt));
                } catch (Exception e) {
                    specification = specification.and(greaterThanOrEqualTo(fieldName, fieldValue));
                }
                break;
            case OPERATION_LT:
                try {
                    ZonedDateTime zdt = ZonedDateTime.parse(fieldValue);
                    specification = specification.and(lessThan(fieldName, zdt));
                } catch (Exception e) {
                    specification = specification.and(lessThan(fieldName, fieldValue));
                }
                break;
            case OPERATION_LE:
                try {
                    ZonedDateTime zdt = ZonedDateTime.parse(fieldValue);
                    specification = specification.and(lessThanOrEqualTo(fieldName, zdt));
                } catch (Exception e) {
                    specification = specification.and(lessThanOrEqualTo(fieldName, fieldValue));
                }
                break;
            case OPERATION_CONTAINS:
                specification = specification.and(likeUpperSpecification(fieldName, fieldValue));
                break;
            case OPERATION_ENDSWITH:
                specification = specification.and(endswithSpecification(fieldName, fieldValue));
                break;
            case OPERATION_STARTSWITH:
                specification = specification.and(startswithSpecification(fieldName, fieldValue));
                break;
            default:
                break;
        }

        return specification;
    }

    private Specification<ENTITY> equalsSpecificationString(String field, final String value) {
        if (field.contains(".")) {
            String[] parts = field.split("\\.");
            if (parts.length == 2) {
                if (newsManyToOneFields.contains(parts[0]) || authorManyToOneFields.contains(parts[0]) || siteChatgroupManyToOneFields.contains(parts[0])) {
                    return (root, query, builder) -> builder.equal(root.get(parts[0]).get(parts[1]), value);
                } else if (newsOneToManyFields.contains(parts[0]) || authorOneToManyFields.contains(parts[0]) || siteChatgroupOneToManyFields.contains(parts[0])) {
                    return buildOneToManySpecification(parts[0], parts[1], value, 1, getClassForEntity());
                } else if (entityName.equalsIgnoreCase(NewsResource.ENTITY_NAME) && newsManyToManyFields.contains(parts[0])) {
                    return buildManyToManySpecification(parts[0], newsManyToManyFieldsMapping.getOrDefault(parts[0], "id"), parts[1], value, 1, getClassForEntity());
                } else if (entityName.equalsIgnoreCase(AuthorResource.ENTITY_NAME) && authorManyToManyFields.contains(parts[0])) {
                    return buildManyToManySpecification(parts[0], authorManyToManyFieldsMapping.getOrDefault(parts[0], "id"), parts[1], value, 1, getClassForEntity());
                } else if (entityName.equalsIgnoreCase(SiteChatgroupResource.ENTITY_NAME) && siteChatgroupManyToManyFields.contains(parts[0])) {
                    return buildManyToManySpecification(parts[0], siteChatgroupManyToManyFieldsMapping.getOrDefault(parts[0], "id"), parts[1], value, 1, getClassForEntity());
                }
            } else if (parts.length == 3 && (parts[0].equalsIgnoreCase("author") || parts[0].equalsIgnoreCase("siteChatgroup"))) {
                return buildManyToManySpecification(parts[0], parts[1], parts[2], value, 1, getClassForEntity());
            } else if (parts.length == 4 && parts[0].equalsIgnoreCase("metagroups")) {
                return buildNewsMetadataCategorySpecification(value, 1);
            }
        }
        return (root, query, builder) -> builder.equal(root.get(field), value);
    }

    private Specification<ENTITY> equalsSpecificationDate(String field, final ZonedDateTime value) {
        return (root, query, builder) -> builder.equal(root.get(field), value);
    }

    private Specification<ENTITY> equalsSpecificationBoolean(String field, final Boolean value) {
        if (field.contains(".")) {
            String[] parts = field.split("\\.");
            if (parts.length == 2 && newsManyToOneFields.contains(parts[0]) || authorManyToOneFields.contains(parts[0])) {
                return (root, query, builder) -> builder.equal(root.get(parts[0]).get(parts[1]), value);
            }
        }
        return (root, query, builder) -> builder.equal(root.get(field), value);
    }

    private <X> Specification<ENTITY> notEqualsSpecification(String fieldName, final X value) {
        return (root, query, builder) -> builder.notEqual(root.get(fieldName), value);
    }

    private Specification<ENTITY> likeUpperSpecification(String field, final String value) {
        if (field.contains(".")) {
            String[] parts = field.split("\\.");
            if (parts.length == 2) {
                if (newsManyToOneFields.contains(parts[0]) || authorManyToOneFields.contains(parts[0]) || siteChatgroupManyToOneFields.contains(parts[0])) {
                    return (root, query, builder) -> builder.like(builder.upper(root.get(parts[0]).get(parts[1])), wrapLikeQuery(value));
                } else if (newsOneToManyFields.contains(parts[0]) || authorOneToManyFields.contains(parts[0]) || siteChatgroupOneToManyFields.contains(parts[0])) {
                    return buildOneToManySpecification(parts[0], parts[1], value, 2, getClassForEntity());
                } else if (entityName.equalsIgnoreCase(NewsResource.ENTITY_NAME) && newsManyToManyFields.contains(parts[0])) {
                    return buildManyToManySpecification(parts[0], newsManyToManyFieldsMapping.getOrDefault(parts[0], "id"), parts[1], value, 2, getClassForEntity());
                } else if (entityName.equalsIgnoreCase(AuthorResource.ENTITY_NAME) && authorManyToManyFields.contains(parts[0])) {
                    return buildManyToManySpecification(parts[0], authorManyToManyFieldsMapping.getOrDefault(parts[0], "id"), parts[1], value, 2, getClassForEntity());
                } else if (entityName.equalsIgnoreCase(SiteChatgroupResource.ENTITY_NAME) && siteChatgroupManyToManyFields.contains(parts[0])) {
                    return buildManyToManySpecification(parts[0], siteChatgroupManyToManyFieldsMapping.getOrDefault(parts[0], "id"), parts[1], value, 2, getClassForEntity());
                }
            } else if (parts.length == 3 && (parts[0].equalsIgnoreCase("author") || parts[0].equalsIgnoreCase("siteChatgroup"))) {
                return buildManyToManySpecification(parts[0], parts[1], parts[2], value, 2, getClassForEntity());
            } else if (parts.length == 4 && parts[0].equalsIgnoreCase("metagroups")) {
                return buildNewsMetadataCategorySpecification(value, 2);
            }
        }
        return (root, query, builder) -> builder.like(builder.upper(root.get(field)), wrapLikeQuery(value));
    }

    private <OTHER> Specification<ENTITY> buildOneToManySpecification(String ref, String fieldName, String fieldValue, int operation, Class cl) {
        return (root, query, builder) -> {
            final Subquery<Long> subQuery = query.subquery(Long.class);
            final Root<ENTITY> entityRoot = subQuery.from(cl);
            final Join<News, OTHER> join = entityRoot.join(ref, JoinType.INNER);
            subQuery.select(entityRoot.get("id"));
            if (operation == 1) {
                subQuery.where(builder.equal(join.get(fieldName), fieldValue));
            } else if (operation == 2) {
                String txt = encodeForQuery(fieldValue.toUpperCase());
                subQuery.where(builder.like(builder.upper(join.get(fieldName)),"%"+txt+"%"));
            }
            return builder.in(root.get("id")).value(subQuery);
        };
    }

    private <OTHER1, OTHER2> Specification<ENTITY> buildManyToManySpecification(String ref1, String ref2, String fieldName, String fieldValue, int operation, Class cl) {
        return (root, query, builder) -> {
            final Subquery<Long> subQuery = query.subquery(Long.class);
            final Root<ENTITY> entityRoot = subQuery.from(cl);
            final Join<ENTITY, OTHER1> join1 = entityRoot.join(ref1, JoinType.INNER);
            final Join<OTHER1, OTHER2> join2 = join1.join(ref2, JoinType.INNER);
            subQuery.select(entityRoot.get("id"));
            if (operation == 1) {
                subQuery.where(builder.equal(join2.get(fieldName), fieldValue));
            } else if (operation == 2) {
                String txt = encodeForQuery(fieldValue.toUpperCase());
                subQuery.where(builder.like(builder.upper(join2.get(fieldName)),"%"+txt+"%"));
            }
            return builder.in(root.get("id")).value(subQuery);
        };
    }

    private Specification<ENTITY> buildNewsMetadataCategorySpecification(String fieldValue, int operation) {
        return (root, query, builder) -> {
            final Subquery<Long> subQuery = query.subquery(Long.class);
            final Root<News> entityRoot = subQuery.from(News.class);
            final Join<News, NewsMetadata> join1 = entityRoot.join("metadata", JoinType.INNER);
            final Join<NewsMetadata, Metafield> join2 = join1.join("metafield", JoinType.INNER);
            subQuery.select(entityRoot.get("id"));

            final Subquery<Long> subQueryCategory = query.subquery(Long.class);
            final Root<Category> entityRootCategory = subQueryCategory.from(Category.class);
            subQueryCategory.select(entityRootCategory.get("id"));

            if (operation == 1) {
                subQueryCategory.where(builder.equal(entityRootCategory.get("name"), fieldValue));
            } else if (operation == 2) {
                String txt = encodeForQuery(fieldValue.toUpperCase());
                subQueryCategory.where(builder.like(builder.upper(entityRootCategory.get("name")),"%"+txt+"%"));
            }

            subQuery.where(builder.and(
                builder.equal(join2.get("id"), 200011),
                builder.equal(join1.get("valueNumber"), subQueryCategory)
            ));

            return builder.in(root.get("id")).value(subQuery);
        };
    }

    private Specification<ENTITY> endswithSpecification(String field, final String value) {
        return (root, query, builder) -> builder.like(builder.upper(root.get(field)), wrapEndswithLikeQuery(value));
    }

    private Specification<ENTITY> startswithSpecification(String field, final String value) {
        return (root, query, builder) -> builder.like(builder.upper(root.get(field)), wrapStartswithLikeQuery(value));
    }

    private <X extends Comparable<? super X>> Specification<ENTITY> greaterThanOrEqualTo(String field, X value) {
        return (root, query, builder) -> builder.greaterThanOrEqualTo(root.get(field), value);
    }

    private <X extends Comparable<? super X>> Specification<ENTITY> greaterThan(String field, X value) {
        return (root, query, builder) -> builder.greaterThan(root.get(field), value);
    }

    private <X extends Comparable<? super X>> Specification<ENTITY> lessThanOrEqualTo(String field, X value) {
        return (root, query, builder) -> builder.lessThanOrEqualTo(root.get(field), value);
    }

    private <X extends Comparable<? super X>> Specification<ENTITY> lessThan(String field, X value) {
        return (root, query, builder) -> builder.lessThan(root.get(field), value);
    }

    private String wrapLikeQuery(String txt) {
        txt = encodeForQuery(txt);
        return "%" + txt.toUpperCase() + '%';
    }

    private String wrapEndswithLikeQuery(String txt) {
        txt = encodeForQuery(txt);
        return "%" + txt.toUpperCase();

    }

    private String wrapStartswithLikeQuery(String txt) {
        txt = encodeForQuery(txt);
        return txt.toUpperCase() + "%";
    }

    public static String encodeForQuery(String txt) {
        if (txt.contains("%")) {
            txt = txt.replaceAll("%", "\\\\%");
        }
        if (txt.contains("_")) {
            txt = txt.replaceAll("_", "\\\\_");
        }
        return txt;
    }

    private Class getClassForEntity() {
        if (entityName.equalsIgnoreCase(NewsResource.ENTITY_NAME)) {
            return News.class;
        } else if (entityName.equalsIgnoreCase(AuthorResource.ENTITY_NAME)) {
            return Author.class;
        } else if (entityName.equalsIgnoreCase(SiteChatgroupResource.ENTITY_NAME)) {
            return SiteChatgroup.class;
        }
        return Object.class;
    }
}


