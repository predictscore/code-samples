package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.AuthorToSiteChatgroup;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AuthorToSiteChatgroup entity.
 */
public interface AuthorToSiteChatgroupSearchRepository extends ElasticsearchRepository<AuthorToSiteChatgroup, Long> {
}
