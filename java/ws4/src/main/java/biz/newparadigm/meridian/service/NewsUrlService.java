package biz.newparadigm.meridian.service;

import biz.newparadigm.meridian.domain.NewsUrl;
import biz.newparadigm.meridian.repository.NewsUrlRepository;
import biz.newparadigm.meridian.repository.search.NewsUrlSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NewsUrl.
 */
@Service
@Transactional
public class NewsUrlService {

    private final Logger log = LoggerFactory.getLogger(NewsUrlService.class);

    private final NewsUrlRepository newsUrlRepository;

    private final NewsUrlSearchRepository newsUrlSearchRepository;

    public NewsUrlService(NewsUrlRepository newsUrlRepository, NewsUrlSearchRepository newsUrlSearchRepository) {
        this.newsUrlRepository = newsUrlRepository;
        this.newsUrlSearchRepository = newsUrlSearchRepository;
    }

    /**
     * Save a newsUrl.
     *
     * @param newsUrl the entity to save
     * @return the persisted entity
     */
    public NewsUrl save(NewsUrl newsUrl) {
        log.debug("Request to save NewsUrl : {}", newsUrl);
        NewsUrl result = newsUrlRepository.save(newsUrl);
        newsUrlSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the newsUrls.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsUrl> findAll(Pageable pageable) {
        log.debug("Request to get all NewsUrls");
        return newsUrlRepository.findAll(pageable);
    }

    /**
     *  Get one newsUrl by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public NewsUrl findOne(Long id) {
        log.debug("Request to get NewsUrl : {}", id);
        return newsUrlRepository.findOne(id);
    }

    /**
     *  Delete the  newsUrl by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NewsUrl : {}", id);
        newsUrlRepository.delete(id);
        newsUrlSearchRepository.delete(id);
    }

    /**
     * Search for the newsUrl corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NewsUrl> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NewsUrls for query {}", query);
        Page<NewsUrl> result = newsUrlSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
