package biz.newparadigm.meridian.repository.search;

import biz.newparadigm.meridian.domain.AuthorToLanguage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AuthorToLanguageSearchRepository extends ElasticsearchRepository<AuthorToLanguage, Long> {

}
