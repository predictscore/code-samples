package biz.newparadigm.meridian.web.rest;

import com.codahale.metrics.annotation.Timed;
import biz.newparadigm.meridian.domain.Metafield;
import biz.newparadigm.meridian.service.MetafieldService;
import biz.newparadigm.meridian.web.rest.errors.BadRequestAlertException;
import biz.newparadigm.meridian.web.rest.util.HeaderUtil;
import biz.newparadigm.meridian.web.rest.util.PaginationUtil;
import biz.newparadigm.meridian.service.dto.MetafieldCriteria;
import biz.newparadigm.meridian.service.MetafieldQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Metafield.
 */
@RestController
@RequestMapping("/api")
public class MetafieldResource {

    private final Logger log = LoggerFactory.getLogger(MetafieldResource.class);

    private static final String ENTITY_NAME = "metafield";

    private final MetafieldService metafieldService;

    private final MetafieldQueryService metafieldQueryService;

    public MetafieldResource(MetafieldService metafieldService, MetafieldQueryService metafieldQueryService) {
        this.metafieldService = metafieldService;
        this.metafieldQueryService = metafieldQueryService;
    }

    /**
     * POST  /metafields : Create a new metafield.
     *
     * @param metafield the metafield to create
     * @return the ResponseEntity with status 201 (Created) and with body the new metafield, or with status 400 (Bad Request) if the metafield has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/metafields")
    @Timed
    public ResponseEntity<Metafield> createMetafield(@Valid @RequestBody Metafield metafield) throws URISyntaxException {
        log.debug("REST request to save Metafield : {}", metafield);
        if (metafield.getId() != null) {
            throw new BadRequestAlertException("A new metafield cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Metafield result = metafieldService.save(metafield);
        return ResponseEntity.created(new URI("/api/metafields/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /metafields : Updates an existing metafield.
     *
     * @param metafield the metafield to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated metafield,
     * or with status 400 (Bad Request) if the metafield is not valid,
     * or with status 500 (Internal Server Error) if the metafield couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/metafields")
    @Timed
    public ResponseEntity<Metafield> updateMetafield(@Valid @RequestBody Metafield metafield) throws URISyntaxException {
        log.debug("REST request to update Metafield : {}", metafield);
        if (metafield.getId() == null) {
            return createMetafield(metafield);
        }
        Metafield result = metafieldService.save(metafield);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, metafield.getId().toString()))
            .body(result);
    }

    /**
     * GET  /metafields : get all the metafields.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of metafields in body
     */
    @GetMapping("/metafields")
    @Timed
    public ResponseEntity<List<Metafield>> getAllMetafields(MetafieldCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get Metafields by criteria: {}", criteria);
        Page<Metafield> page = metafieldQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/metafields");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /metafields/:id : get the "id" metafield.
     *
     * @param id the id of the metafield to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the metafield, or with status 404 (Not Found)
     */
    @GetMapping("/metafields/{id}")
    @Timed
    public ResponseEntity<Metafield> getMetafield(@PathVariable Long id) {
        log.debug("REST request to get Metafield : {}", id);
        Metafield metafield = metafieldService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(metafield));
    }

    /**
     * DELETE  /metafields/:id : delete the "id" metafield.
     *
     * @param id the id of the metafield to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/metafields/{id}")
    @Timed
    public ResponseEntity<Void> deleteMetafield(@PathVariable Long id) {
        log.debug("REST request to delete Metafield : {}", id);
        metafieldService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/metafields?query=:query : search for the metafield corresponding
     * to the query.
     *
     * @param query the query of the metafield search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/metafields")
    @Timed
    public ResponseEntity<List<Metafield>> searchMetafields(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Metafields for query {}", query);
        Page<Metafield> page = metafieldService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/metafields");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
