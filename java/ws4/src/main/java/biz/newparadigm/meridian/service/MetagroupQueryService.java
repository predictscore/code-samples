package biz.newparadigm.meridian.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import biz.newparadigm.meridian.domain.Metagroup;
import biz.newparadigm.meridian.domain.*; // for static metamodels
import biz.newparadigm.meridian.repository.MetagroupRepository;
import biz.newparadigm.meridian.repository.search.MetagroupSearchRepository;
import biz.newparadigm.meridian.service.dto.MetagroupCriteria;


/**
 * Service for executing complex queries for Metagroup entities in the database.
 * The main input is a {@link MetagroupCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link Metagroup} or a {@link Page} of {%link Metagroup} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class MetagroupQueryService extends QueryService<Metagroup> {

    private final Logger log = LoggerFactory.getLogger(MetagroupQueryService.class);


    private final MetagroupRepository metagroupRepository;

    private final MetagroupSearchRepository metagroupSearchRepository;

    public MetagroupQueryService(MetagroupRepository metagroupRepository, MetagroupSearchRepository metagroupSearchRepository) {
        this.metagroupRepository = metagroupRepository;
        this.metagroupSearchRepository = metagroupSearchRepository;
    }

    /**
     * Return a {@link List} of {%link Metagroup} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Metagroup> findByCriteria(MetagroupCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Metagroup> specification = createSpecification(criteria);
        return metagroupRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link Metagroup} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Metagroup> findByCriteria(MetagroupCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Metagroup> specification = createSpecification(criteria);
        return metagroupRepository.findAll(specification, page);
    }

    /**
     * Function to convert MetagroupCriteria to a {@link Specifications}
     */
    private Specifications<Metagroup> createSpecification(MetagroupCriteria criteria) {
        Specifications<Metagroup> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Metagroup_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Metagroup_.name));
            }
        }
        return specification;
    }

}
