(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService', '$state', '$uibModal'];

    function HomeController ($scope, Principal, LoginService, $state, $uibModal) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        vm.reindex = reindex;
        vm.afterReindex = '';

        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }
        function register () {
            $state.go('register');
        }

        function reindex() {
            $uibModal.open({
                templateUrl: 'app/admin/elasticsearch-reindex/elasticsearch-reindex-dialog.html',
                controller: 'ElasticsearchReindexDialogController',
                controllerAs: 'vm',
                size: 'sm'
            }).result.finally(function () {
                vm.afterReindex = 'Reindex started.';
            });
        }
    }
})();
