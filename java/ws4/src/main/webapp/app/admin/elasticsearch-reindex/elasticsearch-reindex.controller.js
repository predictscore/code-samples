(function () {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('ElasticsearchReindexController', ElasticsearchReindexController);

    ElasticsearchReindexController.$inject = [];

    function ElasticsearchReindexController() {
    }
})();
