(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsCommentDialogController', NewsCommentDialogController);

    NewsCommentDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NewsComment', 'News'];

    function NewsCommentDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NewsComment, News) {
        var vm = this;

        vm.newsComment = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.news = News.query();
        vm.newscomments = NewsComment.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsComment.id !== null) {
                NewsComment.update(vm.newsComment, onSaveSuccess, onSaveError);
            } else {
                NewsComment.save(vm.newsComment, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsCommentUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.datetime = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
