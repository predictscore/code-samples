(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsCommentSearch', NewsCommentSearch);

    NewsCommentSearch.$inject = ['$resource'];

    function NewsCommentSearch($resource) {
        var resourceUrl =  'api/_search/news-comments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
