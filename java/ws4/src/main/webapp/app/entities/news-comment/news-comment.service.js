(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsComment', NewsComment);

    NewsComment.$inject = ['$resource', 'DateUtils'];

    function NewsComment ($resource, DateUtils) {
        var resourceUrl =  'api/news-comments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.datetime = DateUtils.convertDateTimeFromServer(data.datetime);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
