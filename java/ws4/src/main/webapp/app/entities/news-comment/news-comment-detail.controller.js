(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsCommentDetailController', NewsCommentDetailController);

    NewsCommentDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'NewsComment', 'News'];

    function NewsCommentDetailController($scope, $rootScope, $stateParams, previousState, entity, NewsComment, News) {
        var vm = this;

        vm.newsComment = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsCommentUpdate', function(event, result) {
            vm.newsComment = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
