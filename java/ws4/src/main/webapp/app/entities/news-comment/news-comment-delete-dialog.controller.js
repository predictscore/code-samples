(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsCommentDeleteController',NewsCommentDeleteController);

    NewsCommentDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsComment'];

    function NewsCommentDeleteController($uibModalInstance, entity, NewsComment) {
        var vm = this;

        vm.newsComment = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsComment.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
