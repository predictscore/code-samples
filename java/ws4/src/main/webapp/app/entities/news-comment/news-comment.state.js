(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-comment', {
            parent: 'entity',
            url: '/news-comment?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsComments'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-comment/news-comments.html',
                    controller: 'NewsCommentController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-comment-detail', {
            parent: 'news-comment',
            url: '/news-comment/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsComment'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-comment/news-comment-detail.html',
                    controller: 'NewsCommentDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsComment', function($stateParams, NewsComment) {
                    return NewsComment.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-comment',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-comment-detail.edit', {
            parent: 'news-comment-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-comment/news-comment-dialog.html',
                    controller: 'NewsCommentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsComment', function(NewsComment) {
                            return NewsComment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-comment.new', {
            parent: 'news-comment',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-comment/news-comment-dialog.html',
                    controller: 'NewsCommentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                userId: null,
                                comment: null,
                                datetime: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-comment', null, { reload: 'news-comment' });
                }, function() {
                    $state.go('news-comment');
                });
            }]
        })
        .state('news-comment.edit', {
            parent: 'news-comment',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-comment/news-comment-dialog.html',
                    controller: 'NewsCommentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsComment', function(NewsComment) {
                            return NewsComment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-comment', null, { reload: 'news-comment' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-comment.delete', {
            parent: 'news-comment',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-comment/news-comment-delete-dialog.html',
                    controller: 'NewsCommentDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsComment', function(NewsComment) {
                            return NewsComment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-comment', null, { reload: 'news-comment' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
