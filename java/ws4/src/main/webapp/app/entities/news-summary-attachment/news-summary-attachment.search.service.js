(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsSummaryAttachmentSearch', NewsSummaryAttachmentSearch);

    NewsSummaryAttachmentSearch.$inject = ['$resource'];

    function NewsSummaryAttachmentSearch($resource) {
        var resourceUrl =  'api/_search/news-summary-attachments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
