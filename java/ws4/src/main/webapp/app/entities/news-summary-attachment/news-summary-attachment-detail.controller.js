(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryAttachmentDetailController', NewsSummaryAttachmentDetailController);

    NewsSummaryAttachmentDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'NewsSummaryAttachment', 'NewsSummary'];

    function NewsSummaryAttachmentDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, NewsSummaryAttachment, NewsSummary) {
        var vm = this;

        vm.newsSummaryAttachment = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsSummaryAttachmentUpdate', function(event, result) {
            vm.newsSummaryAttachment = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
