(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-summary-attachment', {
            parent: 'entity',
            url: '/news-summary-attachment?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummaryAttachments'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary-attachment/news-summary-attachments.html',
                    controller: 'NewsSummaryAttachmentController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-summary-attachment-detail', {
            parent: 'news-summary-attachment',
            url: '/news-summary-attachment/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummaryAttachment'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary-attachment/news-summary-attachment-detail.html',
                    controller: 'NewsSummaryAttachmentDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsSummaryAttachment', function($stateParams, NewsSummaryAttachment) {
                    return NewsSummaryAttachment.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-summary-attachment',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-summary-attachment-detail.edit', {
            parent: 'news-summary-attachment-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-attachment/news-summary-attachment-dialog.html',
                    controller: 'NewsSummaryAttachmentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummaryAttachment', function(NewsSummaryAttachment) {
                            return NewsSummaryAttachment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary-attachment.new', {
            parent: 'news-summary-attachment',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-attachment/news-summary-attachment-dialog.html',
                    controller: 'NewsSummaryAttachmentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                filename: null,
                                blob: null,
                                blobContentType: null,
                                description: null,
                                descriptionTranslation: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-summary-attachment', null, { reload: 'news-summary-attachment' });
                }, function() {
                    $state.go('news-summary-attachment');
                });
            }]
        })
        .state('news-summary-attachment.edit', {
            parent: 'news-summary-attachment',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-attachment/news-summary-attachment-dialog.html',
                    controller: 'NewsSummaryAttachmentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummaryAttachment', function(NewsSummaryAttachment) {
                            return NewsSummaryAttachment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary-attachment', null, { reload: 'news-summary-attachment' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary-attachment.delete', {
            parent: 'news-summary-attachment',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-attachment/news-summary-attachment-delete-dialog.html',
                    controller: 'NewsSummaryAttachmentDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsSummaryAttachment', function(NewsSummaryAttachment) {
                            return NewsSummaryAttachment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary-attachment', null, { reload: 'news-summary-attachment' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
