(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsSummaryAttachment', NewsSummaryAttachment);

    NewsSummaryAttachment.$inject = ['$resource'];

    function NewsSummaryAttachment ($resource) {
        var resourceUrl =  'api/news-summary-attachments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
