(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryAttachmentDialogController', NewsSummaryAttachmentDialogController);

    NewsSummaryAttachmentDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'NewsSummaryAttachment', 'NewsSummary'];

    function NewsSummaryAttachmentDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, NewsSummaryAttachment, NewsSummary) {
        var vm = this;

        vm.newsSummaryAttachment = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        vm.newssummaries = NewsSummary.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsSummaryAttachment.id !== null) {
                NewsSummaryAttachment.update(vm.newsSummaryAttachment, onSaveSuccess, onSaveError);
            } else {
                NewsSummaryAttachment.save(vm.newsSummaryAttachment, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsSummaryAttachmentUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        vm.setBlob = function ($file, newsSummaryAttachment) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        newsSummaryAttachment.blob = base64Data;
                        newsSummaryAttachment.blobContentType = $file.type;
                    });
                });
            }
        };

    }
})();
