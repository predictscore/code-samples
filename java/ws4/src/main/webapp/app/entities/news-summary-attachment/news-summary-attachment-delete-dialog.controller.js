(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryAttachmentDeleteController',NewsSummaryAttachmentDeleteController);

    NewsSummaryAttachmentDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsSummaryAttachment'];

    function NewsSummaryAttachmentDeleteController($uibModalInstance, entity, NewsSummaryAttachment) {
        var vm = this;

        vm.newsSummaryAttachment = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsSummaryAttachment.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
