(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsSummarySearch', NewsSummarySearch);

    NewsSummarySearch.$inject = ['$resource'];

    function NewsSummarySearch($resource) {
        var resourceUrl =  'api/_search/news-summaries/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
