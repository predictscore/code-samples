(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryDialogController', NewsSummaryDialogController);

    NewsSummaryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NewsSummary', 'Location'];

    function NewsSummaryDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NewsSummary, Location) {
        var vm = this;

        vm.newsSummary = entity;
        vm.clear = clear;
        vm.save = save;
        vm.locations = Location.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsSummary.id !== null) {
                NewsSummary.update(vm.newsSummary, onSaveSuccess, onSaveError);
            } else {
                NewsSummary.save(vm.newsSummary, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsSummaryUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
