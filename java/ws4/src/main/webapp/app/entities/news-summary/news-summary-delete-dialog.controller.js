(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryDeleteController',NewsSummaryDeleteController);

    NewsSummaryDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsSummary'];

    function NewsSummaryDeleteController($uibModalInstance, entity, NewsSummary) {
        var vm = this;

        vm.newsSummary = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsSummary.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
