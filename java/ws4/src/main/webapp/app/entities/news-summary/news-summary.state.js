(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-summary', {
            parent: 'entity',
            url: '/news-summary?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummaries'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary/news-summaries.html',
                    controller: 'NewsSummaryController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-summary-detail', {
            parent: 'news-summary',
            url: '/news-summary/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummary'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary/news-summary-detail.html',
                    controller: 'NewsSummaryDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsSummary', function($stateParams, NewsSummary) {
                    return NewsSummary.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-summary',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-summary-detail.edit', {
            parent: 'news-summary-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary/news-summary-dialog.html',
                    controller: 'NewsSummaryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummary', function(NewsSummary) {
                            return NewsSummary.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary.new', {
            parent: 'news-summary',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary/news-summary-dialog.html',
                    controller: 'NewsSummaryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-summary', null, { reload: 'news-summary' });
                }, function() {
                    $state.go('news-summary');
                });
            }]
        })
        .state('news-summary.edit', {
            parent: 'news-summary',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary/news-summary-dialog.html',
                    controller: 'NewsSummaryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummary', function(NewsSummary) {
                            return NewsSummary.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary', null, { reload: 'news-summary' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary.delete', {
            parent: 'news-summary',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary/news-summary-delete-dialog.html',
                    controller: 'NewsSummaryDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsSummary', function(NewsSummary) {
                            return NewsSummary.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary', null, { reload: 'news-summary' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
