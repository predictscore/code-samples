(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsSummary', NewsSummary);

    NewsSummary.$inject = ['$resource'];

    function NewsSummary ($resource) {
        var resourceUrl =  'api/news-summaries/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
