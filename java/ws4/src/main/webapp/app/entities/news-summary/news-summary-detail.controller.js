(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryDetailController', NewsSummaryDetailController);

    NewsSummaryDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'NewsSummary', 'Location'];

    function NewsSummaryDetailController($scope, $rootScope, $stateParams, previousState, entity, NewsSummary, Location) {
        var vm = this;

        vm.newsSummary = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsSummaryUpdate', function(event, result) {
            vm.newsSummary = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
