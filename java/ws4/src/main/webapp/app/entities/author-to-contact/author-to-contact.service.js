(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('AuthorToContact', AuthorToContact);

    AuthorToContact.$inject = ['$resource'];

    function AuthorToContact ($resource) {
        var resourceUrl =  'api/author-to-contacts/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
