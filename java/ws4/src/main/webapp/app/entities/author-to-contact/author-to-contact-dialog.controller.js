(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorToContactDialogController', AuthorToContactDialogController);

    AuthorToContactDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'AuthorToContact', 'Author', 'ContactType'];

    function AuthorToContactDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, AuthorToContact, Author, ContactType) {
        var vm = this;

        vm.authorToContact = entity;
        vm.clear = clear;
        vm.save = save;
        vm.authors = Author.query();
        vm.contacttypes = ContactType.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.authorToContact.id !== null) {
                AuthorToContact.update(vm.authorToContact, onSaveSuccess, onSaveError);
            } else {
                AuthorToContact.save(vm.authorToContact, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:authorToContactUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
