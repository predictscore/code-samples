(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('AuthorToContactSearch', AuthorToContactSearch);

    AuthorToContactSearch.$inject = ['$resource'];

    function AuthorToContactSearch($resource) {
        var resourceUrl =  'api/_search/author-to-contacts/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
