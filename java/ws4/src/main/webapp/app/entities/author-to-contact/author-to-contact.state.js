(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('author-to-contact', {
            parent: 'entity',
            url: '/author-to-contact?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'AuthorToContacts'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/author-to-contact/author-to-contacts.html',
                    controller: 'AuthorToContactController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('author-to-contact-detail', {
            parent: 'author-to-contact',
            url: '/author-to-contact/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'AuthorToContact'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/author-to-contact/author-to-contact-detail.html',
                    controller: 'AuthorToContactDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'AuthorToContact', function($stateParams, AuthorToContact) {
                    return AuthorToContact.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'author-to-contact',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('author-to-contact-detail.edit', {
            parent: 'author-to-contact-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-contact/author-to-contact-dialog.html',
                    controller: 'AuthorToContactDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuthorToContact', function(AuthorToContact) {
                            return AuthorToContact.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('author-to-contact.new', {
            parent: 'author-to-contact',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-contact/author-to-contact-dialog.html',
                    controller: 'AuthorToContactDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                contactValue: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('author-to-contact', null, { reload: 'author-to-contact' });
                }, function() {
                    $state.go('author-to-contact');
                });
            }]
        })
        .state('author-to-contact.edit', {
            parent: 'author-to-contact',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-contact/author-to-contact-dialog.html',
                    controller: 'AuthorToContactDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuthorToContact', function(AuthorToContact) {
                            return AuthorToContact.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('author-to-contact', null, { reload: 'author-to-contact' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('author-to-contact.delete', {
            parent: 'author-to-contact',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-contact/author-to-contact-delete-dialog.html',
                    controller: 'AuthorToContactDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AuthorToContact', function(AuthorToContact) {
                            return AuthorToContact.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('author-to-contact', null, { reload: 'author-to-contact' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
