(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorToContactDeleteController',AuthorToContactDeleteController);

    AuthorToContactDeleteController.$inject = ['$uibModalInstance', 'entity', 'AuthorToContact'];

    function AuthorToContactDeleteController($uibModalInstance, entity, AuthorToContact) {
        var vm = this;

        vm.authorToContact = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            AuthorToContact.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
