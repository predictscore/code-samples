(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorToContactDetailController', AuthorToContactDetailController);

    AuthorToContactDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'AuthorToContact', 'Author', 'ContactType'];

    function AuthorToContactDetailController($scope, $rootScope, $stateParams, previousState, entity, AuthorToContact, Author, ContactType) {
        var vm = this;

        vm.authorToContact = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:authorToContactUpdate', function(event, result) {
            vm.authorToContact = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
