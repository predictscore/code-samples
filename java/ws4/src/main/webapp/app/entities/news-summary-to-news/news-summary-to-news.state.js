(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-summary-to-news', {
            parent: 'entity',
            url: '/news-summary-to-news?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummaryToNews'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary-to-news/news-summary-to-news.html',
                    controller: 'NewsSummaryToNewsController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-summary-to-news-detail', {
            parent: 'news-summary-to-news',
            url: '/news-summary-to-news/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummaryToNews'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary-to-news/news-summary-to-news-detail.html',
                    controller: 'NewsSummaryToNewsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsSummaryToNews', function($stateParams, NewsSummaryToNews) {
                    return NewsSummaryToNews.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-summary-to-news',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-summary-to-news-detail.edit', {
            parent: 'news-summary-to-news-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-to-news/news-summary-to-news-dialog.html',
                    controller: 'NewsSummaryToNewsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummaryToNews', function(NewsSummaryToNews) {
                            return NewsSummaryToNews.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary-to-news.new', {
            parent: 'news-summary-to-news',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-to-news/news-summary-to-news-dialog.html',
                    controller: 'NewsSummaryToNewsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-summary-to-news', null, { reload: 'news-summary-to-news' });
                }, function() {
                    $state.go('news-summary-to-news');
                });
            }]
        })
        .state('news-summary-to-news.edit', {
            parent: 'news-summary-to-news',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-to-news/news-summary-to-news-dialog.html',
                    controller: 'NewsSummaryToNewsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummaryToNews', function(NewsSummaryToNews) {
                            return NewsSummaryToNews.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary-to-news', null, { reload: 'news-summary-to-news' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary-to-news.delete', {
            parent: 'news-summary-to-news',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-to-news/news-summary-to-news-delete-dialog.html',
                    controller: 'NewsSummaryToNewsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsSummaryToNews', function(NewsSummaryToNews) {
                            return NewsSummaryToNews.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary-to-news', null, { reload: 'news-summary-to-news' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
