(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryToNewsDetailController', NewsSummaryToNewsDetailController);

    NewsSummaryToNewsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'NewsSummaryToNews', 'NewsSummary', 'News'];

    function NewsSummaryToNewsDetailController($scope, $rootScope, $stateParams, previousState, entity, NewsSummaryToNews, NewsSummary, News) {
        var vm = this;

        vm.newsSummaryToNews = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsSummaryToNewsUpdate', function(event, result) {
            vm.newsSummaryToNews = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
