(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryToNewsDeleteController',NewsSummaryToNewsDeleteController);

    NewsSummaryToNewsDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsSummaryToNews'];

    function NewsSummaryToNewsDeleteController($uibModalInstance, entity, NewsSummaryToNews) {
        var vm = this;

        vm.newsSummaryToNews = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsSummaryToNews.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
