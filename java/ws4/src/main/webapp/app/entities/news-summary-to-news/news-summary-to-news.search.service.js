(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsSummaryToNewsSearch', NewsSummaryToNewsSearch);

    NewsSummaryToNewsSearch.$inject = ['$resource'];

    function NewsSummaryToNewsSearch($resource) {
        var resourceUrl =  'api/_search/news-summary-to-news/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
