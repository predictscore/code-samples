(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryToNewsDialogController', NewsSummaryToNewsDialogController);

    NewsSummaryToNewsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NewsSummaryToNews', 'NewsSummary', 'News'];

    function NewsSummaryToNewsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NewsSummaryToNews, NewsSummary, News) {
        var vm = this;

        vm.newsSummaryToNews = entity;
        vm.clear = clear;
        vm.save = save;
        vm.newssummaries = NewsSummary.query();
        vm.news = News.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsSummaryToNews.id !== null) {
                NewsSummaryToNews.update(vm.newsSummaryToNews, onSaveSuccess, onSaveError);
            } else {
                NewsSummaryToNews.save(vm.newsSummaryToNews, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsSummaryToNewsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
