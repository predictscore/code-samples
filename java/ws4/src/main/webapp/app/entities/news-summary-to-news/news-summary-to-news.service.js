(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsSummaryToNews', NewsSummaryToNews);

    NewsSummaryToNews.$inject = ['$resource'];

    function NewsSummaryToNews ($resource) {
        var resourceUrl =  'api/news-summary-to-news/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
