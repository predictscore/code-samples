(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupNameDialogController', SiteChatgroupNameDialogController);

    SiteChatgroupNameDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SiteChatgroupName', 'SiteChatgroup'];

    function SiteChatgroupNameDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SiteChatgroupName, SiteChatgroup) {
        var vm = this;

        vm.siteChatgroupName = entity;
        vm.clear = clear;
        vm.save = save;
        vm.sitechatgroups = SiteChatgroup.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.siteChatgroupName.id !== null) {
                SiteChatgroupName.update(vm.siteChatgroupName, onSaveSuccess, onSaveError);
            } else {
                SiteChatgroupName.save(vm.siteChatgroupName, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:siteChatgroupNameUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
