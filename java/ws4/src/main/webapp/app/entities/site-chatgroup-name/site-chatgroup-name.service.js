(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupName', SiteChatgroupName);

    SiteChatgroupName.$inject = ['$resource'];

    function SiteChatgroupName ($resource) {
        var resourceUrl =  'api/site-chatgroup-names/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
