(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupNameDetailController', SiteChatgroupNameDetailController);

    SiteChatgroupNameDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SiteChatgroupName', 'SiteChatgroup'];

    function SiteChatgroupNameDetailController($scope, $rootScope, $stateParams, previousState, entity, SiteChatgroupName, SiteChatgroup) {
        var vm = this;

        vm.siteChatgroupName = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:siteChatgroupNameUpdate', function(event, result) {
            vm.siteChatgroupName = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
