(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupNameDeleteController',SiteChatgroupNameDeleteController);

    SiteChatgroupNameDeleteController.$inject = ['$uibModalInstance', 'entity', 'SiteChatgroupName'];

    function SiteChatgroupNameDeleteController($uibModalInstance, entity, SiteChatgroupName) {
        var vm = this;

        vm.siteChatgroupName = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SiteChatgroupName.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
