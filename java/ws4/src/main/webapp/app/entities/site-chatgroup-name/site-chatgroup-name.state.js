(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('site-chatgroup-name', {
            parent: 'entity',
            url: '/site-chatgroup-name?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupNames'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-name/site-chatgroup-names.html',
                    controller: 'SiteChatgroupNameController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('site-chatgroup-name-detail', {
            parent: 'site-chatgroup-name',
            url: '/site-chatgroup-name/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupName'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-name/site-chatgroup-name-detail.html',
                    controller: 'SiteChatgroupNameDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SiteChatgroupName', function($stateParams, SiteChatgroupName) {
                    return SiteChatgroupName.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'site-chatgroup-name',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('site-chatgroup-name-detail.edit', {
            parent: 'site-chatgroup-name-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-name/site-chatgroup-name-dialog.html',
                    controller: 'SiteChatgroupNameDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupName', function(SiteChatgroupName) {
                            return SiteChatgroupName.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-name.new', {
            parent: 'site-chatgroup-name',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-name/site-chatgroup-name-dialog.html',
                    controller: 'SiteChatgroupNameDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                originalName: null,
                                englishName: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-name', null, { reload: 'site-chatgroup-name' });
                }, function() {
                    $state.go('site-chatgroup-name');
                });
            }]
        })
        .state('site-chatgroup-name.edit', {
            parent: 'site-chatgroup-name',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-name/site-chatgroup-name-dialog.html',
                    controller: 'SiteChatgroupNameDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupName', function(SiteChatgroupName) {
                            return SiteChatgroupName.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-name', null, { reload: 'site-chatgroup-name' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-name.delete', {
            parent: 'site-chatgroup-name',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-name/site-chatgroup-name-delete-dialog.html',
                    controller: 'SiteChatgroupNameDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SiteChatgroupName', function(SiteChatgroupName) {
                            return SiteChatgroupName.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-name', null, { reload: 'site-chatgroup-name' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
