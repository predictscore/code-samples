(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupNameSearch', SiteChatgroupNameSearch);

    SiteChatgroupNameSearch.$inject = ['$resource'];

    function SiteChatgroupNameSearch($resource) {
        var resourceUrl =  'api/_search/site-chatgroup-names/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
