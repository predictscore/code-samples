(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-confidential-image', {
            parent: 'entity',
            url: '/news-confidential-image?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsConfidentialImages'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-confidential-image/news-confidential-images.html',
                    controller: 'NewsConfidentialImageController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-confidential-image-detail', {
            parent: 'news-confidential-image',
            url: '/news-confidential-image/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsConfidentialImage'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-confidential-image/news-confidential-image-detail.html',
                    controller: 'NewsConfidentialImageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsConfidentialImage', function($stateParams, NewsConfidentialImage) {
                    return NewsConfidentialImage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-confidential-image',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-confidential-image-detail.edit', {
            parent: 'news-confidential-image-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-confidential-image/news-confidential-image-dialog.html',
                    controller: 'NewsConfidentialImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsConfidentialImage', function(NewsConfidentialImage) {
                            return NewsConfidentialImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-confidential-image.new', {
            parent: 'news-confidential-image',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-confidential-image/news-confidential-image-dialog.html',
                    controller: 'NewsConfidentialImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                filename: null,
                                blob: null,
                                blobContentType: null,
                                description: null,
                                descriptionTranslation: null,
                                imageOrder: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-confidential-image', null, { reload: 'news-confidential-image' });
                }, function() {
                    $state.go('news-confidential-image');
                });
            }]
        })
        .state('news-confidential-image.edit', {
            parent: 'news-confidential-image',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-confidential-image/news-confidential-image-dialog.html',
                    controller: 'NewsConfidentialImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsConfidentialImage', function(NewsConfidentialImage) {
                            return NewsConfidentialImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-confidential-image', null, { reload: 'news-confidential-image' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-confidential-image.delete', {
            parent: 'news-confidential-image',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-confidential-image/news-confidential-image-delete-dialog.html',
                    controller: 'NewsConfidentialImageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsConfidentialImage', function(NewsConfidentialImage) {
                            return NewsConfidentialImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-confidential-image', null, { reload: 'news-confidential-image' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
