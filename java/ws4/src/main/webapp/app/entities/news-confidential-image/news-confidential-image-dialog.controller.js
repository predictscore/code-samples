(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsConfidentialImageDialogController', NewsConfidentialImageDialogController);

    NewsConfidentialImageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'NewsConfidentialImage', 'News'];

    function NewsConfidentialImageDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, NewsConfidentialImage, News) {
        var vm = this;

        vm.newsConfidentialImage = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        vm.news = News.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsConfidentialImage.id !== null) {
                NewsConfidentialImage.update(vm.newsConfidentialImage, onSaveSuccess, onSaveError);
            } else {
                NewsConfidentialImage.save(vm.newsConfidentialImage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsConfidentialImageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        vm.setBlob = function ($file, newsConfidentialImage) {
            if ($file && $file.$error === 'pattern') {
                return;
            }
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        newsConfidentialImage.blob = base64Data;
                        newsConfidentialImage.blobContentType = $file.type;
                    });
                });
            }
        };

    }
})();
