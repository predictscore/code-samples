(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsConfidentialImageDeleteController',NewsConfidentialImageDeleteController);

    NewsConfidentialImageDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsConfidentialImage'];

    function NewsConfidentialImageDeleteController($uibModalInstance, entity, NewsConfidentialImage) {
        var vm = this;

        vm.newsConfidentialImage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsConfidentialImage.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
