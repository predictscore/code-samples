(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsConfidentialImageDetailController', NewsConfidentialImageDetailController);

    NewsConfidentialImageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'NewsConfidentialImage', 'News'];

    function NewsConfidentialImageDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, NewsConfidentialImage, News) {
        var vm = this;

        vm.newsConfidentialImage = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsConfidentialImageUpdate', function(event, result) {
            vm.newsConfidentialImage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
