(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsConfidentialImageSearch', NewsConfidentialImageSearch);

    NewsConfidentialImageSearch.$inject = ['$resource'];

    function NewsConfidentialImageSearch($resource) {
        var resourceUrl =  'api/_search/news-confidential-images/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
