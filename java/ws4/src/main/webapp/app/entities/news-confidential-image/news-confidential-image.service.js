(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsConfidentialImage', NewsConfidentialImage);

    NewsConfidentialImage.$inject = ['$resource'];

    function NewsConfidentialImage ($resource) {
        var resourceUrl =  'api/news-confidential-images/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
