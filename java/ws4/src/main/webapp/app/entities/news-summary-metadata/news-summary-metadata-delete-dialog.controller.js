(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryMetadataDeleteController',NewsSummaryMetadataDeleteController);

    NewsSummaryMetadataDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsSummaryMetadata'];

    function NewsSummaryMetadataDeleteController($uibModalInstance, entity, NewsSummaryMetadata) {
        var vm = this;

        vm.newsSummaryMetadata = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsSummaryMetadata.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
