(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsSummaryMetadataSearch', NewsSummaryMetadataSearch);

    NewsSummaryMetadataSearch.$inject = ['$resource'];

    function NewsSummaryMetadataSearch($resource) {
        var resourceUrl =  'api/_search/news-summary-metadata/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
