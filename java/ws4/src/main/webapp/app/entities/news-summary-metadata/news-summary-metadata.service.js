(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsSummaryMetadata', NewsSummaryMetadata);

    NewsSummaryMetadata.$inject = ['$resource', 'DateUtils'];

    function NewsSummaryMetadata ($resource, DateUtils) {
        var resourceUrl =  'api/news-summary-metadata/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.valueDate = DateUtils.convertDateTimeFromServer(data.valueDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
