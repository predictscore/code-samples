(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryMetadataDetailController', NewsSummaryMetadataDetailController);

    NewsSummaryMetadataDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'NewsSummaryMetadata', 'NewsSummary', 'Metagroup', 'Metafield'];

    function NewsSummaryMetadataDetailController($scope, $rootScope, $stateParams, previousState, entity, NewsSummaryMetadata, NewsSummary, Metagroup, Metafield) {
        var vm = this;

        vm.newsSummaryMetadata = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsSummaryMetadataUpdate', function(event, result) {
            vm.newsSummaryMetadata = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
