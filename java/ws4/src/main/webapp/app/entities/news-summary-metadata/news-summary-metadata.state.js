(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-summary-metadata', {
            parent: 'entity',
            url: '/news-summary-metadata?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummaryMetadata'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary-metadata/news-summary-metadata.html',
                    controller: 'NewsSummaryMetadataController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-summary-metadata-detail', {
            parent: 'news-summary-metadata',
            url: '/news-summary-metadata/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummaryMetadata'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary-metadata/news-summary-metadata-detail.html',
                    controller: 'NewsSummaryMetadataDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsSummaryMetadata', function($stateParams, NewsSummaryMetadata) {
                    return NewsSummaryMetadata.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-summary-metadata',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-summary-metadata-detail.edit', {
            parent: 'news-summary-metadata-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-metadata/news-summary-metadata-dialog.html',
                    controller: 'NewsSummaryMetadataDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummaryMetadata', function(NewsSummaryMetadata) {
                            return NewsSummaryMetadata.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary-metadata.new', {
            parent: 'news-summary-metadata',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-metadata/news-summary-metadata-dialog.html',
                    controller: 'NewsSummaryMetadataDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                valueString: null,
                                valueNumber: null,
                                valueDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-summary-metadata', null, { reload: 'news-summary-metadata' });
                }, function() {
                    $state.go('news-summary-metadata');
                });
            }]
        })
        .state('news-summary-metadata.edit', {
            parent: 'news-summary-metadata',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-metadata/news-summary-metadata-dialog.html',
                    controller: 'NewsSummaryMetadataDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummaryMetadata', function(NewsSummaryMetadata) {
                            return NewsSummaryMetadata.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary-metadata', null, { reload: 'news-summary-metadata' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary-metadata.delete', {
            parent: 'news-summary-metadata',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-metadata/news-summary-metadata-delete-dialog.html',
                    controller: 'NewsSummaryMetadataDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsSummaryMetadata', function(NewsSummaryMetadata) {
                            return NewsSummaryMetadata.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary-metadata', null, { reload: 'news-summary-metadata' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
