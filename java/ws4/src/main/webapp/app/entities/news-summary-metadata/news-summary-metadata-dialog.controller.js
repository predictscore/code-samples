(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryMetadataDialogController', NewsSummaryMetadataDialogController);

    NewsSummaryMetadataDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NewsSummaryMetadata', 'NewsSummary', 'Metagroup', 'Metafield'];

    function NewsSummaryMetadataDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NewsSummaryMetadata, NewsSummary, Metagroup, Metafield) {
        var vm = this;

        vm.newsSummaryMetadata = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.newssummaries = NewsSummary.query();
        vm.metagroups = Metagroup.query();
        vm.metafields = Metafield.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsSummaryMetadata.id !== null) {
                NewsSummaryMetadata.update(vm.newsSummaryMetadata, onSaveSuccess, onSaveError);
            } else {
                NewsSummaryMetadata.save(vm.newsSummaryMetadata, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsSummaryMetadataUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.valueDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
