(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorToSiteChatgroupDeleteController',AuthorToSiteChatgroupDeleteController);

    AuthorToSiteChatgroupDeleteController.$inject = ['$uibModalInstance', 'entity', 'AuthorToSiteChatgroup'];

    function AuthorToSiteChatgroupDeleteController($uibModalInstance, entity, AuthorToSiteChatgroup) {
        var vm = this;

        vm.authorToSiteChatgroup = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            AuthorToSiteChatgroup.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
