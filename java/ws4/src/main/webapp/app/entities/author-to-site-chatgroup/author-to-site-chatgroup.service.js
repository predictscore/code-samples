(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('AuthorToSiteChatgroup', AuthorToSiteChatgroup);

    AuthorToSiteChatgroup.$inject = ['$resource', 'DateUtils'];

    function AuthorToSiteChatgroup ($resource, DateUtils) {
        var resourceUrl =  'api/author-to-site-chatgroups/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.fromDate = DateUtils.convertDateTimeFromServer(data.fromDate);
                        data.toDate = DateUtils.convertDateTimeFromServer(data.toDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
