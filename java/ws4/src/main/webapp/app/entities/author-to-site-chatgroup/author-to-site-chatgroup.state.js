(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('author-to-site-chatgroup', {
            parent: 'entity',
            url: '/author-to-site-chatgroup?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'AuthorToSiteChatgroups'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/author-to-site-chatgroup/author-to-site-chatgroups.html',
                    controller: 'AuthorToSiteChatgroupController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('author-to-site-chatgroup-detail', {
            parent: 'author-to-site-chatgroup',
            url: '/author-to-site-chatgroup/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'AuthorToSiteChatgroup'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/author-to-site-chatgroup/author-to-site-chatgroup-detail.html',
                    controller: 'AuthorToSiteChatgroupDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'AuthorToSiteChatgroup', function($stateParams, AuthorToSiteChatgroup) {
                    return AuthorToSiteChatgroup.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'author-to-site-chatgroup',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('author-to-site-chatgroup-detail.edit', {
            parent: 'author-to-site-chatgroup-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-site-chatgroup/author-to-site-chatgroup-dialog.html',
                    controller: 'AuthorToSiteChatgroupDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuthorToSiteChatgroup', function(AuthorToSiteChatgroup) {
                            return AuthorToSiteChatgroup.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('author-to-site-chatgroup.new', {
            parent: 'author-to-site-chatgroup',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-site-chatgroup/author-to-site-chatgroup-dialog.html',
                    controller: 'AuthorToSiteChatgroupDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                fromDate: null,
                                originalName: null,
                                englishName: null,
                                profileLink: null,
                                toDate: null,
                                staffFunction: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('author-to-site-chatgroup', null, { reload: 'author-to-site-chatgroup' });
                }, function() {
                    $state.go('author-to-site-chatgroup');
                });
            }]
        })
        .state('author-to-site-chatgroup.edit', {
            parent: 'author-to-site-chatgroup',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-site-chatgroup/author-to-site-chatgroup-dialog.html',
                    controller: 'AuthorToSiteChatgroupDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuthorToSiteChatgroup', function(AuthorToSiteChatgroup) {
                            return AuthorToSiteChatgroup.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('author-to-site-chatgroup', null, { reload: 'author-to-site-chatgroup' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('author-to-site-chatgroup.delete', {
            parent: 'author-to-site-chatgroup',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-site-chatgroup/author-to-site-chatgroup-delete-dialog.html',
                    controller: 'AuthorToSiteChatgroupDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AuthorToSiteChatgroup', function(AuthorToSiteChatgroup) {
                            return AuthorToSiteChatgroup.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('author-to-site-chatgroup', null, { reload: 'author-to-site-chatgroup' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
