(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorToSiteChatgroupDialogController', AuthorToSiteChatgroupDialogController);

    AuthorToSiteChatgroupDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'AuthorToSiteChatgroup', 'Author', 'SiteChatgroup'];

    function AuthorToSiteChatgroupDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, AuthorToSiteChatgroup, Author, SiteChatgroup) {
        var vm = this;

        vm.authorToSiteChatgroup = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.authors = Author.query();
        vm.sitechatgroups = SiteChatgroup.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.authorToSiteChatgroup.id !== null) {
                AuthorToSiteChatgroup.update(vm.authorToSiteChatgroup, onSaveSuccess, onSaveError);
            } else {
                AuthorToSiteChatgroup.save(vm.authorToSiteChatgroup, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:authorToSiteChatgroupUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.fromDate = false;
        vm.datePickerOpenStatus.toDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
