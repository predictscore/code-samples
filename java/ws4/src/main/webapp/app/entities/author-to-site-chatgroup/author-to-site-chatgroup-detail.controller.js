(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorToSiteChatgroupDetailController', AuthorToSiteChatgroupDetailController);

    AuthorToSiteChatgroupDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'AuthorToSiteChatgroup', 'Author', 'SiteChatgroup'];

    function AuthorToSiteChatgroupDetailController($scope, $rootScope, $stateParams, previousState, entity, AuthorToSiteChatgroup, Author, SiteChatgroup) {
        var vm = this;

        vm.authorToSiteChatgroup = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:authorToSiteChatgroupUpdate', function(event, result) {
            vm.authorToSiteChatgroup = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
