(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('AuthorToSiteChatgroupSearch', AuthorToSiteChatgroupSearch);

    AuthorToSiteChatgroupSearch.$inject = ['$resource'];

    function AuthorToSiteChatgroupSearch($resource) {
        var resourceUrl =  'api/_search/author-to-site-chatgroups/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
