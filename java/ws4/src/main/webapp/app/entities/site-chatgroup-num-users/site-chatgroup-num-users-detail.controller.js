(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupNumUsersDetailController', SiteChatgroupNumUsersDetailController);

    SiteChatgroupNumUsersDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SiteChatgroupNumUsers', 'SiteChatgroup'];

    function SiteChatgroupNumUsersDetailController($scope, $rootScope, $stateParams, previousState, entity, SiteChatgroupNumUsers, SiteChatgroup) {
        var vm = this;

        vm.siteChatgroupNumUsers = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:siteChatgroupNumUsersUpdate', function(event, result) {
            vm.siteChatgroupNumUsers = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
