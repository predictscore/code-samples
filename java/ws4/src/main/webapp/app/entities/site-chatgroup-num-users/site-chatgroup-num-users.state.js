(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('site-chatgroup-num-users', {
            parent: 'entity',
            url: '/site-chatgroup-num-users?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupNumUsers'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-num-users/site-chatgroup-num-users.html',
                    controller: 'SiteChatgroupNumUsersController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('site-chatgroup-num-users-detail', {
            parent: 'site-chatgroup-num-users',
            url: '/site-chatgroup-num-users/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupNumUsers'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-num-users/site-chatgroup-num-users-detail.html',
                    controller: 'SiteChatgroupNumUsersDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SiteChatgroupNumUsers', function($stateParams, SiteChatgroupNumUsers) {
                    return SiteChatgroupNumUsers.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'site-chatgroup-num-users',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('site-chatgroup-num-users-detail.edit', {
            parent: 'site-chatgroup-num-users-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-num-users/site-chatgroup-num-users-dialog.html',
                    controller: 'SiteChatgroupNumUsersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupNumUsers', function(SiteChatgroupNumUsers) {
                            return SiteChatgroupNumUsers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-num-users.new', {
            parent: 'site-chatgroup-num-users',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-num-users/site-chatgroup-num-users-dialog.html',
                    controller: 'SiteChatgroupNumUsersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                numUsers: null,
                                numUserDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-num-users', null, { reload: 'site-chatgroup-num-users' });
                }, function() {
                    $state.go('site-chatgroup-num-users');
                });
            }]
        })
        .state('site-chatgroup-num-users.edit', {
            parent: 'site-chatgroup-num-users',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-num-users/site-chatgroup-num-users-dialog.html',
                    controller: 'SiteChatgroupNumUsersDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupNumUsers', function(SiteChatgroupNumUsers) {
                            return SiteChatgroupNumUsers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-num-users', null, { reload: 'site-chatgroup-num-users' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-num-users.delete', {
            parent: 'site-chatgroup-num-users',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-num-users/site-chatgroup-num-users-delete-dialog.html',
                    controller: 'SiteChatgroupNumUsersDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SiteChatgroupNumUsers', function(SiteChatgroupNumUsers) {
                            return SiteChatgroupNumUsers.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-num-users', null, { reload: 'site-chatgroup-num-users' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
