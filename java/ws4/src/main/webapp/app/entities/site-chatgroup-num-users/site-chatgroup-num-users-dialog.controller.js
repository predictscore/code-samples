(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupNumUsersDialogController', SiteChatgroupNumUsersDialogController);

    SiteChatgroupNumUsersDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SiteChatgroupNumUsers', 'SiteChatgroup'];

    function SiteChatgroupNumUsersDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SiteChatgroupNumUsers, SiteChatgroup) {
        var vm = this;

        vm.siteChatgroupNumUsers = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.sitechatgroups = SiteChatgroup.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.siteChatgroupNumUsers.id !== null) {
                SiteChatgroupNumUsers.update(vm.siteChatgroupNumUsers, onSaveSuccess, onSaveError);
            } else {
                SiteChatgroupNumUsers.save(vm.siteChatgroupNumUsers, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:siteChatgroupNumUsersUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.numUserDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
