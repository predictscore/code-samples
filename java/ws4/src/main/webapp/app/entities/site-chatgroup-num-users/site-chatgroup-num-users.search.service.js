(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupNumUsersSearch', SiteChatgroupNumUsersSearch);

    SiteChatgroupNumUsersSearch.$inject = ['$resource'];

    function SiteChatgroupNumUsersSearch($resource) {
        var resourceUrl =  'api/_search/site-chatgroup-num-users/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
