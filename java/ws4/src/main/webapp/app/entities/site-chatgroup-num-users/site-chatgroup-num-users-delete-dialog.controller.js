(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupNumUsersDeleteController',SiteChatgroupNumUsersDeleteController);

    SiteChatgroupNumUsersDeleteController.$inject = ['$uibModalInstance', 'entity', 'SiteChatgroupNumUsers'];

    function SiteChatgroupNumUsersDeleteController($uibModalInstance, entity, SiteChatgroupNumUsers) {
        var vm = this;

        vm.siteChatgroupNumUsers = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SiteChatgroupNumUsers.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
