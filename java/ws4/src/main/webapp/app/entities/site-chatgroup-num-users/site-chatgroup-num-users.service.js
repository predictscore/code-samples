(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupNumUsers', SiteChatgroupNumUsers);

    SiteChatgroupNumUsers.$inject = ['$resource', 'DateUtils'];

    function SiteChatgroupNumUsers ($resource, DateUtils) {
        var resourceUrl =  'api/site-chatgroup-num-users/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.numUserDate = DateUtils.convertDateTimeFromServer(data.numUserDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
