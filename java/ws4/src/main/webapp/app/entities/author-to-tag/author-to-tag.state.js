(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('author-to-tag', {
            parent: 'entity',
            url: '/author-to-tag?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'AuthorToTags'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/author-to-tag/author-to-tags.html',
                    controller: 'AuthorToTagController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('author-to-tag-detail', {
            parent: 'author-to-tag',
            url: '/author-to-tag/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'AuthorToTag'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/author-to-tag/author-to-tag-detail.html',
                    controller: 'AuthorToTagDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'AuthorToTag', function($stateParams, AuthorToTag) {
                    return AuthorToTag.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'author-to-tag',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('author-to-tag-detail.edit', {
            parent: 'author-to-tag-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-tag/author-to-tag-dialog.html',
                    controller: 'AuthorToTagDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuthorToTag', function(AuthorToTag) {
                            return AuthorToTag.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('author-to-tag.new', {
            parent: 'author-to-tag',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-tag/author-to-tag-dialog.html',
                    controller: 'AuthorToTagDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('author-to-tag', null, { reload: 'author-to-tag' });
                }, function() {
                    $state.go('author-to-tag');
                });
            }]
        })
        .state('author-to-tag.edit', {
            parent: 'author-to-tag',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-tag/author-to-tag-dialog.html',
                    controller: 'AuthorToTagDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuthorToTag', function(AuthorToTag) {
                            return AuthorToTag.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('author-to-tag', null, { reload: 'author-to-tag' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('author-to-tag.delete', {
            parent: 'author-to-tag',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-tag/author-to-tag-delete-dialog.html',
                    controller: 'AuthorToTagDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AuthorToTag', function(AuthorToTag) {
                            return AuthorToTag.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('author-to-tag', null, { reload: 'author-to-tag' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
