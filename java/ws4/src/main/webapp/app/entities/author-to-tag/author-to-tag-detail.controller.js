(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorToTagDetailController', AuthorToTagDetailController);

    AuthorToTagDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'AuthorToTag', 'Author', 'Tag'];

    function AuthorToTagDetailController($scope, $rootScope, $stateParams, previousState, entity, AuthorToTag, Author, Tag) {
        var vm = this;

        vm.authorToTag = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:authorToTagUpdate', function(event, result) {
            vm.authorToTag = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
