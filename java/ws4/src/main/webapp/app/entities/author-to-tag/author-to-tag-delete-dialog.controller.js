(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorToTagDeleteController',AuthorToTagDeleteController);

    AuthorToTagDeleteController.$inject = ['$uibModalInstance', 'entity', 'AuthorToTag'];

    function AuthorToTagDeleteController($uibModalInstance, entity, AuthorToTag) {
        var vm = this;

        vm.authorToTag = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            AuthorToTag.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
