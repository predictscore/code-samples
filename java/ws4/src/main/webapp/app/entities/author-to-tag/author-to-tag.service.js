(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('AuthorToTag', AuthorToTag);

    AuthorToTag.$inject = ['$resource'];

    function AuthorToTag ($resource) {
        var resourceUrl =  'api/author-to-tags/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
