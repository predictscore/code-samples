(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorToTagDialogController', AuthorToTagDialogController);

    AuthorToTagDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'AuthorToTag', 'Author', 'Tag'];

    function AuthorToTagDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, AuthorToTag, Author, Tag) {
        var vm = this;

        vm.authorToTag = entity;
        vm.clear = clear;
        vm.save = save;
        vm.authors = Author.query();
        vm.tags = Tag.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.authorToTag.id !== null) {
                AuthorToTag.update(vm.authorToTag, onSaveSuccess, onSaveError);
            } else {
                AuthorToTag.save(vm.authorToTag, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:authorToTagUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
