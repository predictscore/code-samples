(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('AuthorToTagSearch', AuthorToTagSearch);

    AuthorToTagSearch.$inject = ['$resource'];

    function AuthorToTagSearch($resource) {
        var resourceUrl =  'api/_search/author-to-tags/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
