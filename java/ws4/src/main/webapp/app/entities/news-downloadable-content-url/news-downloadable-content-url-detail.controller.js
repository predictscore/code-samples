(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsDownloadableContentUrlDetailController', NewsDownloadableContentUrlDetailController);

    NewsDownloadableContentUrlDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'NewsDownloadableContentUrl', 'News'];

    function NewsDownloadableContentUrlDetailController($scope, $rootScope, $stateParams, previousState, entity, NewsDownloadableContentUrl, News) {
        var vm = this;

        vm.newsDownloadableContentUrl = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsDownloadableContentUrlUpdate', function(event, result) {
            vm.newsDownloadableContentUrl = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
