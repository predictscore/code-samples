(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsDownloadableContentUrlSearch', NewsDownloadableContentUrlSearch);

    NewsDownloadableContentUrlSearch.$inject = ['$resource'];

    function NewsDownloadableContentUrlSearch($resource) {
        var resourceUrl =  'api/_search/news-downloadable-content-urls/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
