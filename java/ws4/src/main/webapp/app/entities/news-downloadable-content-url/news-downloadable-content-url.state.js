(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-downloadable-content-url', {
            parent: 'entity',
            url: '/news-downloadable-content-url?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsDownloadableContentUrls'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-downloadable-content-url/news-downloadable-content-urls.html',
                    controller: 'NewsDownloadableContentUrlController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-downloadable-content-url-detail', {
            parent: 'news-downloadable-content-url',
            url: '/news-downloadable-content-url/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsDownloadableContentUrl'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-downloadable-content-url/news-downloadable-content-url-detail.html',
                    controller: 'NewsDownloadableContentUrlDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsDownloadableContentUrl', function($stateParams, NewsDownloadableContentUrl) {
                    return NewsDownloadableContentUrl.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-downloadable-content-url',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-downloadable-content-url-detail.edit', {
            parent: 'news-downloadable-content-url-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-downloadable-content-url/news-downloadable-content-url-dialog.html',
                    controller: 'NewsDownloadableContentUrlDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsDownloadableContentUrl', function(NewsDownloadableContentUrl) {
                            return NewsDownloadableContentUrl.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-downloadable-content-url.new', {
            parent: 'news-downloadable-content-url',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-downloadable-content-url/news-downloadable-content-url-dialog.html',
                    controller: 'NewsDownloadableContentUrlDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                url: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-downloadable-content-url', null, { reload: 'news-downloadable-content-url' });
                }, function() {
                    $state.go('news-downloadable-content-url');
                });
            }]
        })
        .state('news-downloadable-content-url.edit', {
            parent: 'news-downloadable-content-url',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-downloadable-content-url/news-downloadable-content-url-dialog.html',
                    controller: 'NewsDownloadableContentUrlDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsDownloadableContentUrl', function(NewsDownloadableContentUrl) {
                            return NewsDownloadableContentUrl.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-downloadable-content-url', null, { reload: 'news-downloadable-content-url' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-downloadable-content-url.delete', {
            parent: 'news-downloadable-content-url',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-downloadable-content-url/news-downloadable-content-url-delete-dialog.html',
                    controller: 'NewsDownloadableContentUrlDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsDownloadableContentUrl', function(NewsDownloadableContentUrl) {
                            return NewsDownloadableContentUrl.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-downloadable-content-url', null, { reload: 'news-downloadable-content-url' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
