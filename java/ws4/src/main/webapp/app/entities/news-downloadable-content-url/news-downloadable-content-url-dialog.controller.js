(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsDownloadableContentUrlDialogController', NewsDownloadableContentUrlDialogController);

    NewsDownloadableContentUrlDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NewsDownloadableContentUrl', 'News'];

    function NewsDownloadableContentUrlDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NewsDownloadableContentUrl, News) {
        var vm = this;

        vm.newsDownloadableContentUrl = entity;
        vm.clear = clear;
        vm.save = save;
        vm.news = News.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsDownloadableContentUrl.id !== null) {
                NewsDownloadableContentUrl.update(vm.newsDownloadableContentUrl, onSaveSuccess, onSaveError);
            } else {
                NewsDownloadableContentUrl.save(vm.newsDownloadableContentUrl, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsDownloadableContentUrlUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
