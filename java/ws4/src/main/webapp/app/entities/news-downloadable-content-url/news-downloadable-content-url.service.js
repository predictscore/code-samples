(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsDownloadableContentUrl', NewsDownloadableContentUrl);

    NewsDownloadableContentUrl.$inject = ['$resource'];

    function NewsDownloadableContentUrl ($resource) {
        var resourceUrl =  'api/news-downloadable-content-urls/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
