(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsDownloadableContentUrlDeleteController',NewsDownloadableContentUrlDeleteController);

    NewsDownloadableContentUrlDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsDownloadableContentUrl'];

    function NewsDownloadableContentUrlDeleteController($uibModalInstance, entity, NewsDownloadableContentUrl) {
        var vm = this;

        vm.newsDownloadableContentUrl = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsDownloadableContentUrl.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
