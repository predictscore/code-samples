(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsSummaryToTag', NewsSummaryToTag);

    NewsSummaryToTag.$inject = ['$resource'];

    function NewsSummaryToTag ($resource) {
        var resourceUrl =  'api/news-summary-to-tags/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
