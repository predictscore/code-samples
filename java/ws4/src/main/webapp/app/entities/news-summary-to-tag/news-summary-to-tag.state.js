(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-summary-to-tag', {
            parent: 'entity',
            url: '/news-summary-to-tag?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummaryToTags'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary-to-tag/news-summary-to-tags.html',
                    controller: 'NewsSummaryToTagController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-summary-to-tag-detail', {
            parent: 'news-summary-to-tag',
            url: '/news-summary-to-tag/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummaryToTag'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary-to-tag/news-summary-to-tag-detail.html',
                    controller: 'NewsSummaryToTagDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsSummaryToTag', function($stateParams, NewsSummaryToTag) {
                    return NewsSummaryToTag.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-summary-to-tag',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-summary-to-tag-detail.edit', {
            parent: 'news-summary-to-tag-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-to-tag/news-summary-to-tag-dialog.html',
                    controller: 'NewsSummaryToTagDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummaryToTag', function(NewsSummaryToTag) {
                            return NewsSummaryToTag.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary-to-tag.new', {
            parent: 'news-summary-to-tag',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-to-tag/news-summary-to-tag-dialog.html',
                    controller: 'NewsSummaryToTagDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-summary-to-tag', null, { reload: 'news-summary-to-tag' });
                }, function() {
                    $state.go('news-summary-to-tag');
                });
            }]
        })
        .state('news-summary-to-tag.edit', {
            parent: 'news-summary-to-tag',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-to-tag/news-summary-to-tag-dialog.html',
                    controller: 'NewsSummaryToTagDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummaryToTag', function(NewsSummaryToTag) {
                            return NewsSummaryToTag.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary-to-tag', null, { reload: 'news-summary-to-tag' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary-to-tag.delete', {
            parent: 'news-summary-to-tag',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-to-tag/news-summary-to-tag-delete-dialog.html',
                    controller: 'NewsSummaryToTagDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsSummaryToTag', function(NewsSummaryToTag) {
                            return NewsSummaryToTag.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary-to-tag', null, { reload: 'news-summary-to-tag' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
