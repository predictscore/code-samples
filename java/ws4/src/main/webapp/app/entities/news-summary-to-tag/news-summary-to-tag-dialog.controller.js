(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryToTagDialogController', NewsSummaryToTagDialogController);

    NewsSummaryToTagDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NewsSummaryToTag', 'NewsSummary', 'Tag'];

    function NewsSummaryToTagDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NewsSummaryToTag, NewsSummary, Tag) {
        var vm = this;

        vm.newsSummaryToTag = entity;
        vm.clear = clear;
        vm.save = save;
        vm.newssummaries = NewsSummary.query();
        vm.tags = Tag.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsSummaryToTag.id !== null) {
                NewsSummaryToTag.update(vm.newsSummaryToTag, onSaveSuccess, onSaveError);
            } else {
                NewsSummaryToTag.save(vm.newsSummaryToTag, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsSummaryToTagUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
