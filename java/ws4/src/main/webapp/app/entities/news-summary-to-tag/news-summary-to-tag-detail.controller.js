(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryToTagDetailController', NewsSummaryToTagDetailController);

    NewsSummaryToTagDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'NewsSummaryToTag', 'NewsSummary', 'Tag'];

    function NewsSummaryToTagDetailController($scope, $rootScope, $stateParams, previousState, entity, NewsSummaryToTag, NewsSummary, Tag) {
        var vm = this;

        vm.newsSummaryToTag = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsSummaryToTagUpdate', function(event, result) {
            vm.newsSummaryToTag = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
