(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsSummaryToTagSearch', NewsSummaryToTagSearch);

    NewsSummaryToTagSearch.$inject = ['$resource'];

    function NewsSummaryToTagSearch($resource) {
        var resourceUrl =  'api/_search/news-summary-to-tags/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
