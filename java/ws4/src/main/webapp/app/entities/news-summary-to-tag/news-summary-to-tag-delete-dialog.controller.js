(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryToTagDeleteController',NewsSummaryToTagDeleteController);

    NewsSummaryToTagDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsSummaryToTag'];

    function NewsSummaryToTagDeleteController($uibModalInstance, entity, NewsSummaryToTag) {
        var vm = this;

        vm.newsSummaryToTag = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsSummaryToTag.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
