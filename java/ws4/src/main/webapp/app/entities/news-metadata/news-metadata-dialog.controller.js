(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsMetadataDialogController', NewsMetadataDialogController);

    NewsMetadataDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NewsMetadata', 'News', 'Metagroup', 'Metafield'];

    function NewsMetadataDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NewsMetadata, News, Metagroup, Metafield) {
        var vm = this;

        vm.newsMetadata = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.news = News.query();
        vm.metagroups = Metagroup.query();
        vm.metafields = Metafield.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsMetadata.id !== null) {
                NewsMetadata.update(vm.newsMetadata, onSaveSuccess, onSaveError);
            } else {
                NewsMetadata.save(vm.newsMetadata, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsMetadataUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.valueDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
