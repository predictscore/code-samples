(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-metadata', {
            parent: 'entity',
            url: '/news-metadata?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsMetadata'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-metadata/news-metadata.html',
                    controller: 'NewsMetadataController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-metadata-detail', {
            parent: 'news-metadata',
            url: '/news-metadata/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsMetadata'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-metadata/news-metadata-detail.html',
                    controller: 'NewsMetadataDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsMetadata', function($stateParams, NewsMetadata) {
                    return NewsMetadata.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-metadata',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-metadata-detail.edit', {
            parent: 'news-metadata-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-metadata/news-metadata-dialog.html',
                    controller: 'NewsMetadataDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsMetadata', function(NewsMetadata) {
                            return NewsMetadata.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-metadata.new', {
            parent: 'news-metadata',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-metadata/news-metadata-dialog.html',
                    controller: 'NewsMetadataDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                valueString: null,
                                valueNumber: null,
                                valueDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-metadata', null, { reload: 'news-metadata' });
                }, function() {
                    $state.go('news-metadata');
                });
            }]
        })
        .state('news-metadata.edit', {
            parent: 'news-metadata',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-metadata/news-metadata-dialog.html',
                    controller: 'NewsMetadataDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsMetadata', function(NewsMetadata) {
                            return NewsMetadata.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-metadata', null, { reload: 'news-metadata' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-metadata.delete', {
            parent: 'news-metadata',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-metadata/news-metadata-delete-dialog.html',
                    controller: 'NewsMetadataDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsMetadata', function(NewsMetadata) {
                            return NewsMetadata.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-metadata', null, { reload: 'news-metadata' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
