(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsMetadataSearch', NewsMetadataSearch);

    NewsMetadataSearch.$inject = ['$resource'];

    function NewsMetadataSearch($resource) {
        var resourceUrl =  'api/_search/news-metadata/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
