(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsMetadataDetailController', NewsMetadataDetailController);

    NewsMetadataDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'NewsMetadata', 'News', 'Metagroup', 'Metafield'];

    function NewsMetadataDetailController($scope, $rootScope, $stateParams, previousState, entity, NewsMetadata, News, Metagroup, Metafield) {
        var vm = this;

        vm.newsMetadata = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsMetadataUpdate', function(event, result) {
            vm.newsMetadata = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
