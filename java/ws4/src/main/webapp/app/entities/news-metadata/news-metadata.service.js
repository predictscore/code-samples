(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsMetadata', NewsMetadata);

    NewsMetadata.$inject = ['$resource', 'DateUtils'];

    function NewsMetadata ($resource, DateUtils) {
        var resourceUrl =  'api/news-metadata/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.valueDate = DateUtils.convertDateTimeFromServer(data.valueDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
