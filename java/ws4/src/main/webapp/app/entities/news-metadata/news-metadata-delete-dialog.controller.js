(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsMetadataDeleteController',NewsMetadataDeleteController);

    NewsMetadataDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsMetadata'];

    function NewsMetadataDeleteController($uibModalInstance, entity, NewsMetadata) {
        var vm = this;

        vm.newsMetadata = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsMetadata.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
