(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsDetailController', NewsDetailController);

    NewsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'News', 'Author', 'SiteChatgroup', 'Location', 'NewsComment', 'NewsAttachment', 'NewsOriginalImage', 'NewsPublicImage', 'NewsUrl', 'NewsDownloadableContentUrl', 'NewsConfidentialImage'];

    function NewsDetailController($scope, $rootScope, $stateParams, previousState, entity, News, Author, SiteChatgroup, Location, NewsComment, NewsAttachment, NewsOriginalImage, NewsPublicImage, NewsUrl, NewsDownloadableContentUrl, NewsConfidentialImage) {
        var vm = this;

        vm.news = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsUpdate', function(event, result) {
            vm.news = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
