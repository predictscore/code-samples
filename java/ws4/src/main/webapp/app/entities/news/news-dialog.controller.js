(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsDialogController', NewsDialogController);

    NewsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'News', 'Author', 'SiteChatgroup', 'Location', 'NewsComment', 'NewsAttachment', 'NewsOriginalImage', 'NewsPublicImage', 'NewsUrl', 'NewsDownloadableContentUrl', 'NewsConfidentialImage'];

    function NewsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, News, Author, SiteChatgroup, Location, NewsComment, NewsAttachment, NewsOriginalImage, NewsPublicImage, NewsUrl, NewsDownloadableContentUrl, NewsConfidentialImage) {
        var vm = this;

        vm.news = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.authors = Author.query();
        vm.sitechatgroups = SiteChatgroup.query();
        vm.locations = Location.query();
        vm.newscomments = NewsComment.query();
        vm.newsattachments = NewsAttachment.query();
        vm.newsoriginalimages = NewsOriginalImage.query();
        vm.newspublicimages = NewsPublicImage.query();
        vm.newsurls = NewsUrl.query();
        vm.newsdownloadablecontenturls = NewsDownloadableContentUrl.query();
        vm.newsconfidentialimages = NewsConfidentialImage.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.news.id !== null) {
                News.update(vm.news, onSaveSuccess, onSaveError);
            } else {
                News.save(vm.news, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.datetime = false;
        vm.datePickerOpenStatus.creationDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
