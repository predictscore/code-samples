(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsToTagDetailController', NewsToTagDetailController);

    NewsToTagDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'NewsToTag', 'News', 'Tag'];

    function NewsToTagDetailController($scope, $rootScope, $stateParams, previousState, entity, NewsToTag, News, Tag) {
        var vm = this;

        vm.newsToTag = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsToTagUpdate', function(event, result) {
            vm.newsToTag = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
