(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsToTagSearch', NewsToTagSearch);

    NewsToTagSearch.$inject = ['$resource'];

    function NewsToTagSearch($resource) {
        var resourceUrl =  'api/_search/news-to-tags/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
