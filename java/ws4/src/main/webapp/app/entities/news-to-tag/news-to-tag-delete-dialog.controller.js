(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsToTagDeleteController',NewsToTagDeleteController);

    NewsToTagDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsToTag'];

    function NewsToTagDeleteController($uibModalInstance, entity, NewsToTag) {
        var vm = this;

        vm.newsToTag = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsToTag.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
