(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsToTagDialogController', NewsToTagDialogController);

    NewsToTagDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NewsToTag', 'News', 'Tag'];

    function NewsToTagDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NewsToTag, News, Tag) {
        var vm = this;

        vm.newsToTag = entity;
        vm.clear = clear;
        vm.save = save;
        vm.news = News.query();
        vm.tags = Tag.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsToTag.id !== null) {
                NewsToTag.update(vm.newsToTag, onSaveSuccess, onSaveError);
            } else {
                NewsToTag.save(vm.newsToTag, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsToTagUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
