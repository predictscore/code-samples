(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsToTag', NewsToTag);

    NewsToTag.$inject = ['$resource'];

    function NewsToTag ($resource) {
        var resourceUrl =  'api/news-to-tags/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
