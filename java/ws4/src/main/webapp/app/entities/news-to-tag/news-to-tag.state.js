(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-to-tag', {
            parent: 'entity',
            url: '/news-to-tag?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsToTags'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-to-tag/news-to-tags.html',
                    controller: 'NewsToTagController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-to-tag-detail', {
            parent: 'news-to-tag',
            url: '/news-to-tag/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsToTag'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-to-tag/news-to-tag-detail.html',
                    controller: 'NewsToTagDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsToTag', function($stateParams, NewsToTag) {
                    return NewsToTag.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-to-tag',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-to-tag-detail.edit', {
            parent: 'news-to-tag-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-to-tag/news-to-tag-dialog.html',
                    controller: 'NewsToTagDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsToTag', function(NewsToTag) {
                            return NewsToTag.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-to-tag.new', {
            parent: 'news-to-tag',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-to-tag/news-to-tag-dialog.html',
                    controller: 'NewsToTagDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-to-tag', null, { reload: 'news-to-tag' });
                }, function() {
                    $state.go('news-to-tag');
                });
            }]
        })
        .state('news-to-tag.edit', {
            parent: 'news-to-tag',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-to-tag/news-to-tag-dialog.html',
                    controller: 'NewsToTagDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsToTag', function(NewsToTag) {
                            return NewsToTag.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-to-tag', null, { reload: 'news-to-tag' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-to-tag.delete', {
            parent: 'news-to-tag',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-to-tag/news-to-tag-delete-dialog.html',
                    controller: 'NewsToTagDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsToTag', function(NewsToTag) {
                            return NewsToTag.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-to-tag', null, { reload: 'news-to-tag' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
