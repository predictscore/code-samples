(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsToLanguage', NewsToLanguage);

    NewsToLanguage.$inject = ['$resource'];

    function NewsToLanguage ($resource) {
        var resourceUrl =  'api/news-to-languages/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
