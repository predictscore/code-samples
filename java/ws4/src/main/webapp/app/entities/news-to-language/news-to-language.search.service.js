(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsToLanguageSearch', NewsToLanguageSearch);

    NewsToLanguageSearch.$inject = ['$resource'];

    function NewsToLanguageSearch($resource) {
        var resourceUrl =  'api/_search/news-to-languages/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
