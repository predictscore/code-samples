(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsToLanguageDeleteController',NewsToLanguageDeleteController);

    NewsToLanguageDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsToLanguage'];

    function NewsToLanguageDeleteController($uibModalInstance, entity, NewsToLanguage) {
        var vm = this;

        vm.newsToLanguage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsToLanguage.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
