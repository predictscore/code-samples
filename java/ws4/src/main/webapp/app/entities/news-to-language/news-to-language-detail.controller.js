(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsToLanguageDetailController', NewsToLanguageDetailController);

    NewsToLanguageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'NewsToLanguage', 'News', 'Language'];

    function NewsToLanguageDetailController($scope, $rootScope, $stateParams, previousState, entity, NewsToLanguage, News, Language) {
        var vm = this;

        vm.newsToLanguage = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsToLanguageUpdate', function(event, result) {
            vm.newsToLanguage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
