(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsToLanguageDialogController', NewsToLanguageDialogController);

    NewsToLanguageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NewsToLanguage', 'News', 'Language'];

    function NewsToLanguageDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NewsToLanguage, News, Language) {
        var vm = this;

        vm.newsToLanguage = entity;
        vm.clear = clear;
        vm.save = save;
        vm.news = News.query();
        vm.languages = Language.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsToLanguage.id !== null) {
                NewsToLanguage.update(vm.newsToLanguage, onSaveSuccess, onSaveError);
            } else {
                NewsToLanguage.save(vm.newsToLanguage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsToLanguageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
