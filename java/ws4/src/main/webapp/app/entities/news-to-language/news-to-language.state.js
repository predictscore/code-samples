(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-to-language', {
            parent: 'entity',
            url: '/news-to-language?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsToLanguages'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-to-language/news-to-languages.html',
                    controller: 'NewsToLanguageController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-to-language-detail', {
            parent: 'news-to-language',
            url: '/news-to-language/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsToLanguage'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-to-language/news-to-language-detail.html',
                    controller: 'NewsToLanguageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsToLanguage', function($stateParams, NewsToLanguage) {
                    return NewsToLanguage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-to-language',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-to-language-detail.edit', {
            parent: 'news-to-language-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-to-language/news-to-language-dialog.html',
                    controller: 'NewsToLanguageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsToLanguage', function(NewsToLanguage) {
                            return NewsToLanguage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-to-language.new', {
            parent: 'news-to-language',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-to-language/news-to-language-dialog.html',
                    controller: 'NewsToLanguageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-to-language', null, { reload: 'news-to-language' });
                }, function() {
                    $state.go('news-to-language');
                });
            }]
        })
        .state('news-to-language.edit', {
            parent: 'news-to-language',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-to-language/news-to-language-dialog.html',
                    controller: 'NewsToLanguageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsToLanguage', function(NewsToLanguage) {
                            return NewsToLanguage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-to-language', null, { reload: 'news-to-language' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-to-language.delete', {
            parent: 'news-to-language',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-to-language/news-to-language-delete-dialog.html',
                    controller: 'NewsToLanguageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsToLanguage', function(NewsToLanguage) {
                            return NewsToLanguage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-to-language', null, { reload: 'news-to-language' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
