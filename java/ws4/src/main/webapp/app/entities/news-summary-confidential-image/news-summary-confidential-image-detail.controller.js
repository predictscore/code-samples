(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryConfidentialImageDetailController', NewsSummaryConfidentialImageDetailController);

    NewsSummaryConfidentialImageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'NewsSummaryConfidentialImage', 'NewsSummary'];

    function NewsSummaryConfidentialImageDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, NewsSummaryConfidentialImage, NewsSummary) {
        var vm = this;

        vm.newsSummaryConfidentialImage = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsSummaryConfidentialImageUpdate', function(event, result) {
            vm.newsSummaryConfidentialImage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
