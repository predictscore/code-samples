(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsSummaryConfidentialImage', NewsSummaryConfidentialImage);

    NewsSummaryConfidentialImage.$inject = ['$resource'];

    function NewsSummaryConfidentialImage ($resource) {
        var resourceUrl =  'api/news-summary-confidential-images/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
