(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryConfidentialImageDeleteController',NewsSummaryConfidentialImageDeleteController);

    NewsSummaryConfidentialImageDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsSummaryConfidentialImage'];

    function NewsSummaryConfidentialImageDeleteController($uibModalInstance, entity, NewsSummaryConfidentialImage) {
        var vm = this;

        vm.newsSummaryConfidentialImage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsSummaryConfidentialImage.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
