(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryConfidentialImageDialogController', NewsSummaryConfidentialImageDialogController);

    NewsSummaryConfidentialImageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'NewsSummaryConfidentialImage', 'NewsSummary'];

    function NewsSummaryConfidentialImageDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, NewsSummaryConfidentialImage, NewsSummary) {
        var vm = this;

        vm.newsSummaryConfidentialImage = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        vm.newssummaries = NewsSummary.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsSummaryConfidentialImage.id !== null) {
                NewsSummaryConfidentialImage.update(vm.newsSummaryConfidentialImage, onSaveSuccess, onSaveError);
            } else {
                NewsSummaryConfidentialImage.save(vm.newsSummaryConfidentialImage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsSummaryConfidentialImageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        vm.setBlob = function ($file, newsSummaryConfidentialImage) {
            if ($file && $file.$error === 'pattern') {
                return;
            }
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        newsSummaryConfidentialImage.blob = base64Data;
                        newsSummaryConfidentialImage.blobContentType = $file.type;
                    });
                });
            }
        };

    }
})();
