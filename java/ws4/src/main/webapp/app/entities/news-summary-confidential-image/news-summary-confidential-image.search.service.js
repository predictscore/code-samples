(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsSummaryConfidentialImageSearch', NewsSummaryConfidentialImageSearch);

    NewsSummaryConfidentialImageSearch.$inject = ['$resource'];

    function NewsSummaryConfidentialImageSearch($resource) {
        var resourceUrl =  'api/_search/news-summary-confidential-images/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
