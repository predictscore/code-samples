(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-summary-confidential-image', {
            parent: 'entity',
            url: '/news-summary-confidential-image?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummaryConfidentialImages'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary-confidential-image/news-summary-confidential-images.html',
                    controller: 'NewsSummaryConfidentialImageController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-summary-confidential-image-detail', {
            parent: 'news-summary-confidential-image',
            url: '/news-summary-confidential-image/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummaryConfidentialImage'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary-confidential-image/news-summary-confidential-image-detail.html',
                    controller: 'NewsSummaryConfidentialImageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsSummaryConfidentialImage', function($stateParams, NewsSummaryConfidentialImage) {
                    return NewsSummaryConfidentialImage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-summary-confidential-image',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-summary-confidential-image-detail.edit', {
            parent: 'news-summary-confidential-image-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-confidential-image/news-summary-confidential-image-dialog.html',
                    controller: 'NewsSummaryConfidentialImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummaryConfidentialImage', function(NewsSummaryConfidentialImage) {
                            return NewsSummaryConfidentialImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary-confidential-image.new', {
            parent: 'news-summary-confidential-image',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-confidential-image/news-summary-confidential-image-dialog.html',
                    controller: 'NewsSummaryConfidentialImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                filename: null,
                                blob: null,
                                blobContentType: null,
                                description: null,
                                descriptionTranslation: null,
                                imageOrder: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-summary-confidential-image', null, { reload: 'news-summary-confidential-image' });
                }, function() {
                    $state.go('news-summary-confidential-image');
                });
            }]
        })
        .state('news-summary-confidential-image.edit', {
            parent: 'news-summary-confidential-image',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-confidential-image/news-summary-confidential-image-dialog.html',
                    controller: 'NewsSummaryConfidentialImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummaryConfidentialImage', function(NewsSummaryConfidentialImage) {
                            return NewsSummaryConfidentialImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary-confidential-image', null, { reload: 'news-summary-confidential-image' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary-confidential-image.delete', {
            parent: 'news-summary-confidential-image',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-confidential-image/news-summary-confidential-image-delete-dialog.html',
                    controller: 'NewsSummaryConfidentialImageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsSummaryConfidentialImage', function(NewsSummaryConfidentialImage) {
                            return NewsSummaryConfidentialImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary-confidential-image', null, { reload: 'news-summary-confidential-image' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
