(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsAttachmentDialogController', NewsAttachmentDialogController);

    NewsAttachmentDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'NewsAttachment', 'News'];

    function NewsAttachmentDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, NewsAttachment, News) {
        var vm = this;

        vm.newsAttachment = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        vm.news = News.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsAttachment.id !== null) {
                NewsAttachment.update(vm.newsAttachment, onSaveSuccess, onSaveError);
            } else {
                NewsAttachment.save(vm.newsAttachment, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsAttachmentUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        vm.setBlob = function ($file, newsAttachment) {
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        newsAttachment.blob = base64Data;
                        newsAttachment.blobContentType = $file.type;
                    });
                });
            }
        };

    }
})();
