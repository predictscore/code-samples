(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-attachment', {
            parent: 'entity',
            url: '/news-attachment?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsAttachments'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-attachment/news-attachments.html',
                    controller: 'NewsAttachmentController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-attachment-detail', {
            parent: 'news-attachment',
            url: '/news-attachment/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsAttachment'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-attachment/news-attachment-detail.html',
                    controller: 'NewsAttachmentDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsAttachment', function($stateParams, NewsAttachment) {
                    return NewsAttachment.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-attachment',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-attachment-detail.edit', {
            parent: 'news-attachment-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-attachment/news-attachment-dialog.html',
                    controller: 'NewsAttachmentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsAttachment', function(NewsAttachment) {
                            return NewsAttachment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-attachment.new', {
            parent: 'news-attachment',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-attachment/news-attachment-dialog.html',
                    controller: 'NewsAttachmentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                filename: null,
                                blob: null,
                                blobContentType: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-attachment', null, { reload: 'news-attachment' });
                }, function() {
                    $state.go('news-attachment');
                });
            }]
        })
        .state('news-attachment.edit', {
            parent: 'news-attachment',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-attachment/news-attachment-dialog.html',
                    controller: 'NewsAttachmentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsAttachment', function(NewsAttachment) {
                            return NewsAttachment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-attachment', null, { reload: 'news-attachment' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-attachment.delete', {
            parent: 'news-attachment',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-attachment/news-attachment-delete-dialog.html',
                    controller: 'NewsAttachmentDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsAttachment', function(NewsAttachment) {
                            return NewsAttachment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-attachment', null, { reload: 'news-attachment' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
