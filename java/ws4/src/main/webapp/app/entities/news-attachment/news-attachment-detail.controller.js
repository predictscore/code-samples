(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsAttachmentDetailController', NewsAttachmentDetailController);

    NewsAttachmentDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'NewsAttachment', 'News'];

    function NewsAttachmentDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, NewsAttachment, News) {
        var vm = this;

        vm.newsAttachment = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsAttachmentUpdate', function(event, result) {
            vm.newsAttachment = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
