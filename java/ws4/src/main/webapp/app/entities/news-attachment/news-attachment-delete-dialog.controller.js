(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsAttachmentDeleteController',NewsAttachmentDeleteController);

    NewsAttachmentDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsAttachment'];

    function NewsAttachmentDeleteController($uibModalInstance, entity, NewsAttachment) {
        var vm = this;

        vm.newsAttachment = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsAttachment.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
