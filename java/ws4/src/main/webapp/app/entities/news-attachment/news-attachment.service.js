(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsAttachment', NewsAttachment);

    NewsAttachment.$inject = ['$resource'];

    function NewsAttachment ($resource) {
        var resourceUrl =  'api/news-attachments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
