(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsAttachmentSearch', NewsAttachmentSearch);

    NewsAttachmentSearch.$inject = ['$resource'];

    function NewsAttachmentSearch($resource) {
        var resourceUrl =  'api/_search/news-attachments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
