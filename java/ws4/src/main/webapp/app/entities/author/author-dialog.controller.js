(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorDialogController', AuthorDialogController);

    AuthorDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Author', 'Location', 'AuthorProfile', 'AuthorName', 'AuthorComment', 'News'];

    function AuthorDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Author, Location, AuthorProfile, AuthorName, AuthorComment, News) {
        var vm = this;

        vm.author = entity;
        vm.clear = clear;
        vm.save = save;
        vm.locations = Location.query();
        vm.authorprofiles = AuthorProfile.query();
        vm.authornames = AuthorName.query();
        vm.authorcomments = AuthorComment.query();
        vm.news = News.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.author.id !== null) {
                Author.update(vm.author, onSaveSuccess, onSaveError);
            } else {
                Author.save(vm.author, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:authorUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
