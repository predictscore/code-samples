(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorDetailController', AuthorDetailController);

    AuthorDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Author', 'Location', 'AuthorProfile', 'AuthorName', 'AuthorComment', 'News'];

    function AuthorDetailController($scope, $rootScope, $stateParams, previousState, entity, Author, Location, AuthorProfile, AuthorName, AuthorComment, News) {
        var vm = this;

        vm.author = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:authorUpdate', function(event, result) {
            vm.author = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
