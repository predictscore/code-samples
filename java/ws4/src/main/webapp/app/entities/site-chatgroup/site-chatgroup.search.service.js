(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupSearch', SiteChatgroupSearch);

    SiteChatgroupSearch.$inject = ['$resource'];

    function SiteChatgroupSearch($resource) {
        var resourceUrl =  'api/_search/site-chatgroups/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
