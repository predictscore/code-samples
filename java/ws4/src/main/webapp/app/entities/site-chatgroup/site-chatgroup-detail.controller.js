(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupDetailController', SiteChatgroupDetailController);

    SiteChatgroupDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SiteChatgroup', 'Location', 'SiteChatgroupComment', 'SiteChatgroupDomain', 'SiteChatgroupIp', 'SiteChatgroupName', 'SiteChatgroupInfo', 'SiteChatgroupNumVisitors', 'SiteChatgroupNumUsers', 'SiteChatgroupToLanguage', 'SiteChatgroupToTag', 'AuthorToSiteChatgroup'];

    function SiteChatgroupDetailController($scope, $rootScope, $stateParams, previousState, entity, SiteChatgroup, Location, SiteChatgroupComment, SiteChatgroupDomain, SiteChatgroupIp, SiteChatgroupName, SiteChatgroupInfo, SiteChatgroupNumVisitors, SiteChatgroupNumUsers, SiteChatgroupToLanguage, SiteChatgroupToTag, AuthorToSiteChatgroup) {
        var vm = this;

        vm.siteChatgroup = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:siteChatgroupUpdate', function(event, result) {
            vm.siteChatgroup = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
