(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupDialogController', SiteChatgroupDialogController);

    SiteChatgroupDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SiteChatgroup', 'Location', 'SiteChatgroupComment', 'SiteChatgroupDomain', 'SiteChatgroupIp', 'SiteChatgroupName', 'SiteChatgroupInfo', 'SiteChatgroupNumVisitors', 'SiteChatgroupNumUsers', 'SiteChatgroupToLanguage', 'SiteChatgroupToTag', 'AuthorToSiteChatgroup'];

    function SiteChatgroupDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SiteChatgroup, Location, SiteChatgroupComment, SiteChatgroupDomain, SiteChatgroupIp, SiteChatgroupName, SiteChatgroupInfo, SiteChatgroupNumVisitors, SiteChatgroupNumUsers, SiteChatgroupToLanguage, SiteChatgroupToTag, AuthorToSiteChatgroup) {
        var vm = this;

        vm.siteChatgroup = entity;
        vm.clear = clear;
        vm.save = save;
        vm.locations = Location.query();
        vm.sitechatgroupcomments = SiteChatgroupComment.query();
        vm.sitechatgroupdomains = SiteChatgroupDomain.query();
        vm.sitechatgroupips = SiteChatgroupIp.query();
        vm.sitechatgroupnames = SiteChatgroupName.query();
        vm.sitechatgroupinfos = SiteChatgroupInfo.query();
        vm.sitechatgroupnumvisitors = SiteChatgroupNumVisitors.query();
        vm.sitechatgroupnumusers = SiteChatgroupNumUsers.query();
        vm.sitechatgrouptolanguages = SiteChatgroupToLanguage.query();
        vm.sitechatgrouptotags = SiteChatgroupToTag.query();
        vm.authortositechatgroups = AuthorToSiteChatgroup.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.siteChatgroup.id !== null) {
                SiteChatgroup.update(vm.siteChatgroup, onSaveSuccess, onSaveError);
            } else {
                SiteChatgroup.save(vm.siteChatgroup, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:siteChatgroupUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
