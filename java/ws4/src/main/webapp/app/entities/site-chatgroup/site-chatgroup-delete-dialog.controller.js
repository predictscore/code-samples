(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupDeleteController',SiteChatgroupDeleteController);

    SiteChatgroupDeleteController.$inject = ['$uibModalInstance', 'entity', 'SiteChatgroup'];

    function SiteChatgroupDeleteController($uibModalInstance, entity, SiteChatgroup) {
        var vm = this;

        vm.siteChatgroup = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SiteChatgroup.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
