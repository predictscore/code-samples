(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('SiteChatgroup', SiteChatgroup);

    SiteChatgroup.$inject = ['$resource'];

    function SiteChatgroup ($resource) {
        var resourceUrl =  'api/site-chatgroups/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
