(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('site-chatgroup', {
            parent: 'entity',
            url: '/site-chatgroup?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroups'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup/site-chatgroups.html',
                    controller: 'SiteChatgroupController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('site-chatgroup-detail', {
            parent: 'site-chatgroup',
            url: '/site-chatgroup/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroup'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup/site-chatgroup-detail.html',
                    controller: 'SiteChatgroupDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SiteChatgroup', function($stateParams, SiteChatgroup) {
                    return SiteChatgroup.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'site-chatgroup',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('site-chatgroup-detail.edit', {
            parent: 'site-chatgroup-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup/site-chatgroup-dialog.html',
                    controller: 'SiteChatgroupDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroup', function(SiteChatgroup) {
                            return SiteChatgroup.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup.new', {
            parent: 'site-chatgroup',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup/site-chatgroup-dialog.html',
                    controller: 'SiteChatgroupDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                isSite: false,
                                originalName: null,
                                englishName: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup', null, { reload: 'site-chatgroup' });
                }, function() {
                    $state.go('site-chatgroup');
                });
            }]
        })
        .state('site-chatgroup.edit', {
            parent: 'site-chatgroup',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup/site-chatgroup-dialog.html',
                    controller: 'SiteChatgroupDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroup', function(SiteChatgroup) {
                            return SiteChatgroup.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup', null, { reload: 'site-chatgroup' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup.delete', {
            parent: 'site-chatgroup',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup/site-chatgroup-delete-dialog.html',
                    controller: 'SiteChatgroupDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SiteChatgroup', function(SiteChatgroup) {
                            return SiteChatgroup.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup', null, { reload: 'site-chatgroup' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
