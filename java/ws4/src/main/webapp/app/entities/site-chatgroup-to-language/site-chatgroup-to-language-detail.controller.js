(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupToLanguageDetailController', SiteChatgroupToLanguageDetailController);

    SiteChatgroupToLanguageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SiteChatgroupToLanguage', 'SiteChatgroup', 'Language'];

    function SiteChatgroupToLanguageDetailController($scope, $rootScope, $stateParams, previousState, entity, SiteChatgroupToLanguage, SiteChatgroup, Language) {
        var vm = this;

        vm.siteChatgroupToLanguage = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:siteChatgroupToLanguageUpdate', function(event, result) {
            vm.siteChatgroupToLanguage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
