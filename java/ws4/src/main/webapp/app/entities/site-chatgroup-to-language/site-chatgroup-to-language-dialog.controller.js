(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupToLanguageDialogController', SiteChatgroupToLanguageDialogController);

    SiteChatgroupToLanguageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SiteChatgroupToLanguage', 'SiteChatgroup', 'Language'];

    function SiteChatgroupToLanguageDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SiteChatgroupToLanguage, SiteChatgroup, Language) {
        var vm = this;

        vm.siteChatgroupToLanguage = entity;
        vm.clear = clear;
        vm.save = save;
        vm.sitechatgroups = SiteChatgroup.query();
        vm.languages = Language.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.siteChatgroupToLanguage.id !== null) {
                SiteChatgroupToLanguage.update(vm.siteChatgroupToLanguage, onSaveSuccess, onSaveError);
            } else {
                SiteChatgroupToLanguage.save(vm.siteChatgroupToLanguage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:siteChatgroupToLanguageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
