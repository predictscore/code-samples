(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('site-chatgroup-to-language', {
            parent: 'entity',
            url: '/site-chatgroup-to-language?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupToLanguages'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-to-language/site-chatgroup-to-languages.html',
                    controller: 'SiteChatgroupToLanguageController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('site-chatgroup-to-language-detail', {
            parent: 'site-chatgroup-to-language',
            url: '/site-chatgroup-to-language/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupToLanguage'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-to-language/site-chatgroup-to-language-detail.html',
                    controller: 'SiteChatgroupToLanguageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SiteChatgroupToLanguage', function($stateParams, SiteChatgroupToLanguage) {
                    return SiteChatgroupToLanguage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'site-chatgroup-to-language',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('site-chatgroup-to-language-detail.edit', {
            parent: 'site-chatgroup-to-language-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-to-language/site-chatgroup-to-language-dialog.html',
                    controller: 'SiteChatgroupToLanguageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupToLanguage', function(SiteChatgroupToLanguage) {
                            return SiteChatgroupToLanguage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-to-language.new', {
            parent: 'site-chatgroup-to-language',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-to-language/site-chatgroup-to-language-dialog.html',
                    controller: 'SiteChatgroupToLanguageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-to-language', null, { reload: 'site-chatgroup-to-language' });
                }, function() {
                    $state.go('site-chatgroup-to-language');
                });
            }]
        })
        .state('site-chatgroup-to-language.edit', {
            parent: 'site-chatgroup-to-language',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-to-language/site-chatgroup-to-language-dialog.html',
                    controller: 'SiteChatgroupToLanguageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupToLanguage', function(SiteChatgroupToLanguage) {
                            return SiteChatgroupToLanguage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-to-language', null, { reload: 'site-chatgroup-to-language' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-to-language.delete', {
            parent: 'site-chatgroup-to-language',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-to-language/site-chatgroup-to-language-delete-dialog.html',
                    controller: 'SiteChatgroupToLanguageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SiteChatgroupToLanguage', function(SiteChatgroupToLanguage) {
                            return SiteChatgroupToLanguage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-to-language', null, { reload: 'site-chatgroup-to-language' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
