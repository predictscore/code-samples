(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupToLanguageSearch', SiteChatgroupToLanguageSearch);

    SiteChatgroupToLanguageSearch.$inject = ['$resource'];

    function SiteChatgroupToLanguageSearch($resource) {
        var resourceUrl =  'api/_search/site-chatgroup-to-languages/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
