(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupToLanguageDeleteController',SiteChatgroupToLanguageDeleteController);

    SiteChatgroupToLanguageDeleteController.$inject = ['$uibModalInstance', 'entity', 'SiteChatgroupToLanguage'];

    function SiteChatgroupToLanguageDeleteController($uibModalInstance, entity, SiteChatgroupToLanguage) {
        var vm = this;

        vm.siteChatgroupToLanguage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SiteChatgroupToLanguage.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
