(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupToLanguage', SiteChatgroupToLanguage);

    SiteChatgroupToLanguage.$inject = ['$resource'];

    function SiteChatgroupToLanguage ($resource) {
        var resourceUrl =  'api/site-chatgroup-to-languages/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
