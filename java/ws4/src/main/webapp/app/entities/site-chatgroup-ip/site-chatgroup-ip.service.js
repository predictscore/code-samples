(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupIp', SiteChatgroupIp);

    SiteChatgroupIp.$inject = ['$resource', 'DateUtils'];

    function SiteChatgroupIp ($resource, DateUtils) {
        var resourceUrl =  'api/site-chatgroup-ips/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.firstUsedDate = DateUtils.convertDateTimeFromServer(data.firstUsedDate);
                        data.lastUsedDate = DateUtils.convertDateTimeFromServer(data.lastUsedDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
