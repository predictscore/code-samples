(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupIpDetailController', SiteChatgroupIpDetailController);

    SiteChatgroupIpDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SiteChatgroupIp', 'SiteChatgroup'];

    function SiteChatgroupIpDetailController($scope, $rootScope, $stateParams, previousState, entity, SiteChatgroupIp, SiteChatgroup) {
        var vm = this;

        vm.siteChatgroupIp = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:siteChatgroupIpUpdate', function(event, result) {
            vm.siteChatgroupIp = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
