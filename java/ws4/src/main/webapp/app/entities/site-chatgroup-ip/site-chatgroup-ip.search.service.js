(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupIpSearch', SiteChatgroupIpSearch);

    SiteChatgroupIpSearch.$inject = ['$resource'];

    function SiteChatgroupIpSearch($resource) {
        var resourceUrl =  'api/_search/site-chatgroup-ips/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
