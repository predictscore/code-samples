(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupIpDeleteController',SiteChatgroupIpDeleteController);

    SiteChatgroupIpDeleteController.$inject = ['$uibModalInstance', 'entity', 'SiteChatgroupIp'];

    function SiteChatgroupIpDeleteController($uibModalInstance, entity, SiteChatgroupIp) {
        var vm = this;

        vm.siteChatgroupIp = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SiteChatgroupIp.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
