(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('site-chatgroup-ip', {
            parent: 'entity',
            url: '/site-chatgroup-ip?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupIps'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-ip/site-chatgroup-ips.html',
                    controller: 'SiteChatgroupIpController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('site-chatgroup-ip-detail', {
            parent: 'site-chatgroup-ip',
            url: '/site-chatgroup-ip/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupIp'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-ip/site-chatgroup-ip-detail.html',
                    controller: 'SiteChatgroupIpDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SiteChatgroupIp', function($stateParams, SiteChatgroupIp) {
                    return SiteChatgroupIp.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'site-chatgroup-ip',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('site-chatgroup-ip-detail.edit', {
            parent: 'site-chatgroup-ip-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-ip/site-chatgroup-ip-dialog.html',
                    controller: 'SiteChatgroupIpDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupIp', function(SiteChatgroupIp) {
                            return SiteChatgroupIp.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-ip.new', {
            parent: 'site-chatgroup-ip',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-ip/site-chatgroup-ip-dialog.html',
                    controller: 'SiteChatgroupIpDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                ipName: null,
                                ipWhois: null,
                                firstUsedDate: null,
                                lastUsedDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-ip', null, { reload: 'site-chatgroup-ip' });
                }, function() {
                    $state.go('site-chatgroup-ip');
                });
            }]
        })
        .state('site-chatgroup-ip.edit', {
            parent: 'site-chatgroup-ip',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-ip/site-chatgroup-ip-dialog.html',
                    controller: 'SiteChatgroupIpDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupIp', function(SiteChatgroupIp) {
                            return SiteChatgroupIp.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-ip', null, { reload: 'site-chatgroup-ip' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-ip.delete', {
            parent: 'site-chatgroup-ip',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-ip/site-chatgroup-ip-delete-dialog.html',
                    controller: 'SiteChatgroupIpDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SiteChatgroupIp', function(SiteChatgroupIp) {
                            return SiteChatgroupIp.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-ip', null, { reload: 'site-chatgroup-ip' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
