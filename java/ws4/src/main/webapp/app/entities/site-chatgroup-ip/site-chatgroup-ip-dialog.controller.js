(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupIpDialogController', SiteChatgroupIpDialogController);

    SiteChatgroupIpDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SiteChatgroupIp', 'SiteChatgroup'];

    function SiteChatgroupIpDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SiteChatgroupIp, SiteChatgroup) {
        var vm = this;

        vm.siteChatgroupIp = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.sitechatgroups = SiteChatgroup.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.siteChatgroupIp.id !== null) {
                SiteChatgroupIp.update(vm.siteChatgroupIp, onSaveSuccess, onSaveError);
            } else {
                SiteChatgroupIp.save(vm.siteChatgroupIp, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:siteChatgroupIpUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.firstUsedDate = false;
        vm.datePickerOpenStatus.lastUsedDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
