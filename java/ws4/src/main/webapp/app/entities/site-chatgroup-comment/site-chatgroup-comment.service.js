(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupComment', SiteChatgroupComment);

    SiteChatgroupComment.$inject = ['$resource', 'DateUtils'];

    function SiteChatgroupComment ($resource, DateUtils) {
        var resourceUrl =  'api/site-chatgroup-comments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.datetime = DateUtils.convertDateTimeFromServer(data.datetime);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
