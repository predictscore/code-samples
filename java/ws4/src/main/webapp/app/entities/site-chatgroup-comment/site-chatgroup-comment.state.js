(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('site-chatgroup-comment', {
            parent: 'entity',
            url: '/site-chatgroup-comment?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupComments'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-comment/site-chatgroup-comments.html',
                    controller: 'SiteChatgroupCommentController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('site-chatgroup-comment-detail', {
            parent: 'site-chatgroup-comment',
            url: '/site-chatgroup-comment/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupComment'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-comment/site-chatgroup-comment-detail.html',
                    controller: 'SiteChatgroupCommentDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SiteChatgroupComment', function($stateParams, SiteChatgroupComment) {
                    return SiteChatgroupComment.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'site-chatgroup-comment',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('site-chatgroup-comment-detail.edit', {
            parent: 'site-chatgroup-comment-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-comment/site-chatgroup-comment-dialog.html',
                    controller: 'SiteChatgroupCommentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupComment', function(SiteChatgroupComment) {
                            return SiteChatgroupComment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-comment.new', {
            parent: 'site-chatgroup-comment',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-comment/site-chatgroup-comment-dialog.html',
                    controller: 'SiteChatgroupCommentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                userId: null,
                                comment: null,
                                datetime: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-comment', null, { reload: 'site-chatgroup-comment' });
                }, function() {
                    $state.go('site-chatgroup-comment');
                });
            }]
        })
        .state('site-chatgroup-comment.edit', {
            parent: 'site-chatgroup-comment',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-comment/site-chatgroup-comment-dialog.html',
                    controller: 'SiteChatgroupCommentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupComment', function(SiteChatgroupComment) {
                            return SiteChatgroupComment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-comment', null, { reload: 'site-chatgroup-comment' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-comment.delete', {
            parent: 'site-chatgroup-comment',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-comment/site-chatgroup-comment-delete-dialog.html',
                    controller: 'SiteChatgroupCommentDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SiteChatgroupComment', function(SiteChatgroupComment) {
                            return SiteChatgroupComment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-comment', null, { reload: 'site-chatgroup-comment' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
