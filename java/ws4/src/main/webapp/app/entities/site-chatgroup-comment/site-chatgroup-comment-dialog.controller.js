(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupCommentDialogController', SiteChatgroupCommentDialogController);

    SiteChatgroupCommentDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SiteChatgroupComment', 'SiteChatgroup'];

    function SiteChatgroupCommentDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SiteChatgroupComment, SiteChatgroup) {
        var vm = this;

        vm.siteChatgroupComment = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.sitechatgroups = SiteChatgroup.query();
        vm.sitechatgroupcomments = SiteChatgroupComment.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.siteChatgroupComment.id !== null) {
                SiteChatgroupComment.update(vm.siteChatgroupComment, onSaveSuccess, onSaveError);
            } else {
                SiteChatgroupComment.save(vm.siteChatgroupComment, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:siteChatgroupCommentUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.datetime = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
