(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupCommentSearch', SiteChatgroupCommentSearch);

    SiteChatgroupCommentSearch.$inject = ['$resource'];

    function SiteChatgroupCommentSearch($resource) {
        var resourceUrl =  'api/_search/site-chatgroup-comments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
