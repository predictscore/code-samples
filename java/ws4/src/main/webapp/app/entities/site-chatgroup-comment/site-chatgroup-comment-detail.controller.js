(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupCommentDetailController', SiteChatgroupCommentDetailController);

    SiteChatgroupCommentDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SiteChatgroupComment', 'SiteChatgroup'];

    function SiteChatgroupCommentDetailController($scope, $rootScope, $stateParams, previousState, entity, SiteChatgroupComment, SiteChatgroup) {
        var vm = this;

        vm.siteChatgroupComment = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:siteChatgroupCommentUpdate', function(event, result) {
            vm.siteChatgroupComment = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
