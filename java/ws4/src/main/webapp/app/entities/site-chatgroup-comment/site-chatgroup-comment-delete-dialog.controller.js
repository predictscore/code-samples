(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupCommentDeleteController',SiteChatgroupCommentDeleteController);

    SiteChatgroupCommentDeleteController.$inject = ['$uibModalInstance', 'entity', 'SiteChatgroupComment'];

    function SiteChatgroupCommentDeleteController($uibModalInstance, entity, SiteChatgroupComment) {
        var vm = this;

        vm.siteChatgroupComment = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SiteChatgroupComment.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
