(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsSummaryComment', NewsSummaryComment);

    NewsSummaryComment.$inject = ['$resource'];

    function NewsSummaryComment ($resource) {
        var resourceUrl =  'api/news-summary-comments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
