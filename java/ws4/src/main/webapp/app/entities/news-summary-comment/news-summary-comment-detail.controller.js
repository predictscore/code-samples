(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryCommentDetailController', NewsSummaryCommentDetailController);

    NewsSummaryCommentDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'NewsSummaryComment', 'NewsSummary'];

    function NewsSummaryCommentDetailController($scope, $rootScope, $stateParams, previousState, entity, NewsSummaryComment, NewsSummary) {
        var vm = this;

        vm.newsSummaryComment = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsSummaryCommentUpdate', function(event, result) {
            vm.newsSummaryComment = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
