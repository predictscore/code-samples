(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-summary-comment', {
            parent: 'entity',
            url: '/news-summary-comment?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummaryComments'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary-comment/news-summary-comments.html',
                    controller: 'NewsSummaryCommentController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-summary-comment-detail', {
            parent: 'news-summary-comment',
            url: '/news-summary-comment/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummaryComment'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary-comment/news-summary-comment-detail.html',
                    controller: 'NewsSummaryCommentDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsSummaryComment', function($stateParams, NewsSummaryComment) {
                    return NewsSummaryComment.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-summary-comment',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-summary-comment-detail.edit', {
            parent: 'news-summary-comment-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-comment/news-summary-comment-dialog.html',
                    controller: 'NewsSummaryCommentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummaryComment', function(NewsSummaryComment) {
                            return NewsSummaryComment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary-comment.new', {
            parent: 'news-summary-comment',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-comment/news-summary-comment-dialog.html',
                    controller: 'NewsSummaryCommentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                userId: null,
                                comment: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-summary-comment', null, { reload: 'news-summary-comment' });
                }, function() {
                    $state.go('news-summary-comment');
                });
            }]
        })
        .state('news-summary-comment.edit', {
            parent: 'news-summary-comment',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-comment/news-summary-comment-dialog.html',
                    controller: 'NewsSummaryCommentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummaryComment', function(NewsSummaryComment) {
                            return NewsSummaryComment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary-comment', null, { reload: 'news-summary-comment' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary-comment.delete', {
            parent: 'news-summary-comment',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-comment/news-summary-comment-delete-dialog.html',
                    controller: 'NewsSummaryCommentDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsSummaryComment', function(NewsSummaryComment) {
                            return NewsSummaryComment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary-comment', null, { reload: 'news-summary-comment' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
