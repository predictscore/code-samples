(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryCommentDialogController', NewsSummaryCommentDialogController);

    NewsSummaryCommentDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NewsSummaryComment', 'NewsSummary'];

    function NewsSummaryCommentDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NewsSummaryComment, NewsSummary) {
        var vm = this;

        vm.newsSummaryComment = entity;
        vm.clear = clear;
        vm.save = save;
        vm.newssummaries = NewsSummary.query();
        vm.newssummarycomments = NewsSummaryComment.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsSummaryComment.id !== null) {
                NewsSummaryComment.update(vm.newsSummaryComment, onSaveSuccess, onSaveError);
            } else {
                NewsSummaryComment.save(vm.newsSummaryComment, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsSummaryCommentUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
