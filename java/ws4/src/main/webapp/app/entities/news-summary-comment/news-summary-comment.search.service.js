(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsSummaryCommentSearch', NewsSummaryCommentSearch);

    NewsSummaryCommentSearch.$inject = ['$resource'];

    function NewsSummaryCommentSearch($resource) {
        var resourceUrl =  'api/_search/news-summary-comments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
