(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryCommentDeleteController',NewsSummaryCommentDeleteController);

    NewsSummaryCommentDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsSummaryComment'];

    function NewsSummaryCommentDeleteController($uibModalInstance, entity, NewsSummaryComment) {
        var vm = this;

        vm.newsSummaryComment = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsSummaryComment.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
