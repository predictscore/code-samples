(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('AuthorComment', AuthorComment);

    AuthorComment.$inject = ['$resource', 'DateUtils'];

    function AuthorComment ($resource, DateUtils) {
        var resourceUrl =  'api/author-comments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.datetime = DateUtils.convertDateTimeFromServer(data.datetime);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
