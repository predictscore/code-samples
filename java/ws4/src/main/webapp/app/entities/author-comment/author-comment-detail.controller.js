(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorCommentDetailController', AuthorCommentDetailController);

    AuthorCommentDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'AuthorComment', 'Author'];

    function AuthorCommentDetailController($scope, $rootScope, $stateParams, previousState, entity, AuthorComment, Author) {
        var vm = this;

        vm.authorComment = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:authorCommentUpdate', function(event, result) {
            vm.authorComment = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
