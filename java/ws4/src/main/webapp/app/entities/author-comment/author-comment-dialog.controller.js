(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorCommentDialogController', AuthorCommentDialogController);

    AuthorCommentDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'AuthorComment', 'Author'];

    function AuthorCommentDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, AuthorComment, Author) {
        var vm = this;

        vm.authorComment = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.authors = Author.query();
        vm.authorcomments = AuthorComment.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.authorComment.id !== null) {
                AuthorComment.update(vm.authorComment, onSaveSuccess, onSaveError);
            } else {
                AuthorComment.save(vm.authorComment, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:authorCommentUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.datetime = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
