(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('AuthorCommentSearch', AuthorCommentSearch);

    AuthorCommentSearch.$inject = ['$resource'];

    function AuthorCommentSearch($resource) {
        var resourceUrl =  'api/_search/author-comments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
