(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('author-comment', {
            parent: 'entity',
            url: '/author-comment?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'AuthorComments'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/author-comment/author-comments.html',
                    controller: 'AuthorCommentController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('author-comment-detail', {
            parent: 'author-comment',
            url: '/author-comment/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'AuthorComment'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/author-comment/author-comment-detail.html',
                    controller: 'AuthorCommentDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'AuthorComment', function($stateParams, AuthorComment) {
                    return AuthorComment.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'author-comment',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('author-comment-detail.edit', {
            parent: 'author-comment-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-comment/author-comment-dialog.html',
                    controller: 'AuthorCommentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuthorComment', function(AuthorComment) {
                            return AuthorComment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('author-comment.new', {
            parent: 'author-comment',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-comment/author-comment-dialog.html',
                    controller: 'AuthorCommentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                userId: null,
                                comment: null,
                                datetime: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('author-comment', null, { reload: 'author-comment' });
                }, function() {
                    $state.go('author-comment');
                });
            }]
        })
        .state('author-comment.edit', {
            parent: 'author-comment',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-comment/author-comment-dialog.html',
                    controller: 'AuthorCommentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuthorComment', function(AuthorComment) {
                            return AuthorComment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('author-comment', null, { reload: 'author-comment' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('author-comment.delete', {
            parent: 'author-comment',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-comment/author-comment-delete-dialog.html',
                    controller: 'AuthorCommentDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AuthorComment', function(AuthorComment) {
                            return AuthorComment.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('author-comment', null, { reload: 'author-comment' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
