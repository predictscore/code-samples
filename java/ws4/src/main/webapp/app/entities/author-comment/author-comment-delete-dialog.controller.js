(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorCommentDeleteController',AuthorCommentDeleteController);

    AuthorCommentDeleteController.$inject = ['$uibModalInstance', 'entity', 'AuthorComment'];

    function AuthorCommentDeleteController($uibModalInstance, entity, AuthorComment) {
        var vm = this;

        vm.authorComment = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            AuthorComment.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
