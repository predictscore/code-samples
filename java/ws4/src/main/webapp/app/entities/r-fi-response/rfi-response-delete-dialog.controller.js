(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('RFIResponseDeleteController',RFIResponseDeleteController);

    RFIResponseDeleteController.$inject = ['$uibModalInstance', 'entity', 'RFIResponse'];

    function RFIResponseDeleteController($uibModalInstance, entity, RFIResponse) {
        var vm = this;

        vm.rFIResponse = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            RFIResponse.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
