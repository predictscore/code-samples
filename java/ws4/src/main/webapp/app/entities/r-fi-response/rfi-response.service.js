(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('RFIResponse', RFIResponse);

    RFIResponse.$inject = ['$resource', 'DateUtils'];

    function RFIResponse ($resource, DateUtils) {
        var resourceUrl =  'api/r-fi-responses/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.submissionDate = DateUtils.convertDateTimeFromServer(data.submissionDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
