(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('RFIResponseSearch', RFIResponseSearch);

    RFIResponseSearch.$inject = ['$resource'];

    function RFIResponseSearch($resource) {
        var resourceUrl =  'api/_search/r-fi-responses/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
