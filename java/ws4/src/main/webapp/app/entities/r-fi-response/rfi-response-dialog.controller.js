(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('RFIResponseDialogController', RFIResponseDialogController);

    RFIResponseDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'RFIResponse'];

    function RFIResponseDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, RFIResponse) {
        var vm = this;

        vm.rFIResponse = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.rFIResponse.id !== null) {
                RFIResponse.update(vm.rFIResponse, onSaveSuccess, onSaveError);
            } else {
                RFIResponse.save(vm.rFIResponse, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:rFIResponseUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.submissionDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
