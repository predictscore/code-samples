(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('RFIResponseDetailController', RFIResponseDetailController);

    RFIResponseDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'RFIResponse'];

    function RFIResponseDetailController($scope, $rootScope, $stateParams, previousState, entity, RFIResponse) {
        var vm = this;

        vm.rFIResponse = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:rFIResponseUpdate', function(event, result) {
            vm.rFIResponse = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
