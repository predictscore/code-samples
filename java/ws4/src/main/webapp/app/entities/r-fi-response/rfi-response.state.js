(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('rfi-response', {
            parent: 'entity',
            url: '/rfi-response?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'RFIResponses'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/r-fi-response/r-fi-responses.html',
                    controller: 'RFIResponseController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('rfi-response-detail', {
            parent: 'rfi-response',
            url: '/rfi-response/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'RFIResponse'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/r-fi-response/rfi-response-detail.html',
                    controller: 'RFIResponseDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'RFIResponse', function($stateParams, RFIResponse) {
                    return RFIResponse.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'rfi-response',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('rfi-response-detail.edit', {
            parent: 'rfi-response-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/r-fi-response/rfi-response-dialog.html',
                    controller: 'RFIResponseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['RFIResponse', function(RFIResponse) {
                            return RFIResponse.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('rfi-response.new', {
            parent: 'rfi-response',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/r-fi-response/rfi-response-dialog.html',
                    controller: 'RFIResponseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                note: null,
                                username: null,
                                submissionDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('rfi-response', null, { reload: 'rfi-response' });
                }, function() {
                    $state.go('rfi-response');
                });
            }]
        })
        .state('rfi-response.edit', {
            parent: 'rfi-response',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/r-fi-response/rfi-response-dialog.html',
                    controller: 'RFIResponseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['RFIResponse', function(RFIResponse) {
                            return RFIResponse.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('rfi-response', null, { reload: 'rfi-response' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('rfi-response.delete', {
            parent: 'rfi-response',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/r-fi-response/rfi-response-delete-dialog.html',
                    controller: 'RFIResponseDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['RFIResponse', function(RFIResponse) {
                            return RFIResponse.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('rfi-response', null, { reload: 'rfi-response' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
