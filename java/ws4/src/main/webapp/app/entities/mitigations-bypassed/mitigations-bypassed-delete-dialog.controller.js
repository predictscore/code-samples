(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('MitigationsBypassedDeleteController',MitigationsBypassedDeleteController);

    MitigationsBypassedDeleteController.$inject = ['$uibModalInstance', 'entity', 'MitigationsBypassed'];

    function MitigationsBypassedDeleteController($uibModalInstance, entity, MitigationsBypassed) {
        var vm = this;

        vm.mitigationsBypassed = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MitigationsBypassed.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
