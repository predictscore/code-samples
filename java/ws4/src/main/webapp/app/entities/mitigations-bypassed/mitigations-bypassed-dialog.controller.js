(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('MitigationsBypassedDialogController', MitigationsBypassedDialogController);

    MitigationsBypassedDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'MitigationsBypassed'];

    function MitigationsBypassedDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, MitigationsBypassed) {
        var vm = this;

        vm.mitigationsBypassed = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.mitigationsBypassed.id !== null) {
                MitigationsBypassed.update(vm.mitigationsBypassed, onSaveSuccess, onSaveError);
            } else {
                MitigationsBypassed.save(vm.mitigationsBypassed, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:mitigationsBypassedUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
