(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('MitigationsBypassed', MitigationsBypassed);

    MitigationsBypassed.$inject = ['$resource'];

    function MitigationsBypassed ($resource) {
        var resourceUrl =  'api/mitigations-bypasseds/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
