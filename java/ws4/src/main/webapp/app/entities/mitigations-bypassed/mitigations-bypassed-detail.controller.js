(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('MitigationsBypassedDetailController', MitigationsBypassedDetailController);

    MitigationsBypassedDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'MitigationsBypassed'];

    function MitigationsBypassedDetailController($scope, $rootScope, $stateParams, previousState, entity, MitigationsBypassed) {
        var vm = this;

        vm.mitigationsBypassed = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:mitigationsBypassedUpdate', function(event, result) {
            vm.mitigationsBypassed = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
