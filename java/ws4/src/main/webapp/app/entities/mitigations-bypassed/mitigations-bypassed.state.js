(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('mitigations-bypassed', {
            parent: 'entity',
            url: '/mitigations-bypassed?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'MitigationsBypasseds'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/mitigations-bypassed/mitigations-bypasseds.html',
                    controller: 'MitigationsBypassedController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('mitigations-bypassed-detail', {
            parent: 'mitigations-bypassed',
            url: '/mitigations-bypassed/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'MitigationsBypassed'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/mitigations-bypassed/mitigations-bypassed-detail.html',
                    controller: 'MitigationsBypassedDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'MitigationsBypassed', function($stateParams, MitigationsBypassed) {
                    return MitigationsBypassed.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'mitigations-bypassed',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('mitigations-bypassed-detail.edit', {
            parent: 'mitigations-bypassed-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mitigations-bypassed/mitigations-bypassed-dialog.html',
                    controller: 'MitigationsBypassedDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MitigationsBypassed', function(MitigationsBypassed) {
                            return MitigationsBypassed.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('mitigations-bypassed.new', {
            parent: 'mitigations-bypassed',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mitigations-bypassed/mitigations-bypassed-dialog.html',
                    controller: 'MitigationsBypassedDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('mitigations-bypassed', null, { reload: 'mitigations-bypassed' });
                }, function() {
                    $state.go('mitigations-bypassed');
                });
            }]
        })
        .state('mitigations-bypassed.edit', {
            parent: 'mitigations-bypassed',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mitigations-bypassed/mitigations-bypassed-dialog.html',
                    controller: 'MitigationsBypassedDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MitigationsBypassed', function(MitigationsBypassed) {
                            return MitigationsBypassed.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('mitigations-bypassed', null, { reload: 'mitigations-bypassed' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('mitigations-bypassed.delete', {
            parent: 'mitigations-bypassed',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/mitigations-bypassed/mitigations-bypassed-delete-dialog.html',
                    controller: 'MitigationsBypassedDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MitigationsBypassed', function(MitigationsBypassed) {
                            return MitigationsBypassed.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('mitigations-bypassed', null, { reload: 'mitigations-bypassed' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
