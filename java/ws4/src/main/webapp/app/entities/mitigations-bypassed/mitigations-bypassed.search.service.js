(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('MitigationsBypassedSearch', MitigationsBypassedSearch);

    MitigationsBypassedSearch.$inject = ['$resource'];

    function MitigationsBypassedSearch($resource) {
        var resourceUrl =  'api/_search/mitigations-bypasseds/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
