(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupNumVisitorsDetailController', SiteChatgroupNumVisitorsDetailController);

    SiteChatgroupNumVisitorsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SiteChatgroupNumVisitors', 'SiteChatgroup'];

    function SiteChatgroupNumVisitorsDetailController($scope, $rootScope, $stateParams, previousState, entity, SiteChatgroupNumVisitors, SiteChatgroup) {
        var vm = this;

        vm.siteChatgroupNumVisitors = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:siteChatgroupNumVisitorsUpdate', function(event, result) {
            vm.siteChatgroupNumVisitors = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
