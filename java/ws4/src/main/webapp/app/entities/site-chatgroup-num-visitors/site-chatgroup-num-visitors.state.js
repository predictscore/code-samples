(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('site-chatgroup-num-visitors', {
            parent: 'entity',
            url: '/site-chatgroup-num-visitors?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupNumVisitors'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-num-visitors/site-chatgroup-num-visitors.html',
                    controller: 'SiteChatgroupNumVisitorsController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('site-chatgroup-num-visitors-detail', {
            parent: 'site-chatgroup-num-visitors',
            url: '/site-chatgroup-num-visitors/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupNumVisitors'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-num-visitors/site-chatgroup-num-visitors-detail.html',
                    controller: 'SiteChatgroupNumVisitorsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SiteChatgroupNumVisitors', function($stateParams, SiteChatgroupNumVisitors) {
                    return SiteChatgroupNumVisitors.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'site-chatgroup-num-visitors',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('site-chatgroup-num-visitors-detail.edit', {
            parent: 'site-chatgroup-num-visitors-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-num-visitors/site-chatgroup-num-visitors-dialog.html',
                    controller: 'SiteChatgroupNumVisitorsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupNumVisitors', function(SiteChatgroupNumVisitors) {
                            return SiteChatgroupNumVisitors.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-num-visitors.new', {
            parent: 'site-chatgroup-num-visitors',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-num-visitors/site-chatgroup-num-visitors-dialog.html',
                    controller: 'SiteChatgroupNumVisitorsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                numVisitors: null,
                                numVisitorDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-num-visitors', null, { reload: 'site-chatgroup-num-visitors' });
                }, function() {
                    $state.go('site-chatgroup-num-visitors');
                });
            }]
        })
        .state('site-chatgroup-num-visitors.edit', {
            parent: 'site-chatgroup-num-visitors',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-num-visitors/site-chatgroup-num-visitors-dialog.html',
                    controller: 'SiteChatgroupNumVisitorsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupNumVisitors', function(SiteChatgroupNumVisitors) {
                            return SiteChatgroupNumVisitors.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-num-visitors', null, { reload: 'site-chatgroup-num-visitors' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-num-visitors.delete', {
            parent: 'site-chatgroup-num-visitors',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-num-visitors/site-chatgroup-num-visitors-delete-dialog.html',
                    controller: 'SiteChatgroupNumVisitorsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SiteChatgroupNumVisitors', function(SiteChatgroupNumVisitors) {
                            return SiteChatgroupNumVisitors.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-num-visitors', null, { reload: 'site-chatgroup-num-visitors' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
