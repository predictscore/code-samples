(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupNumVisitors', SiteChatgroupNumVisitors);

    SiteChatgroupNumVisitors.$inject = ['$resource', 'DateUtils'];

    function SiteChatgroupNumVisitors ($resource, DateUtils) {
        var resourceUrl =  'api/site-chatgroup-num-visitors/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.numVisitorDate = DateUtils.convertDateTimeFromServer(data.numVisitorDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
