(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupNumVisitorsSearch', SiteChatgroupNumVisitorsSearch);

    SiteChatgroupNumVisitorsSearch.$inject = ['$resource'];

    function SiteChatgroupNumVisitorsSearch($resource) {
        var resourceUrl =  'api/_search/site-chatgroup-num-visitors/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
