(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupNumVisitorsDeleteController',SiteChatgroupNumVisitorsDeleteController);

    SiteChatgroupNumVisitorsDeleteController.$inject = ['$uibModalInstance', 'entity', 'SiteChatgroupNumVisitors'];

    function SiteChatgroupNumVisitorsDeleteController($uibModalInstance, entity, SiteChatgroupNumVisitors) {
        var vm = this;

        vm.siteChatgroupNumVisitors = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SiteChatgroupNumVisitors.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
