(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupNumVisitorsDialogController', SiteChatgroupNumVisitorsDialogController);

    SiteChatgroupNumVisitorsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SiteChatgroupNumVisitors', 'SiteChatgroup'];

    function SiteChatgroupNumVisitorsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SiteChatgroupNumVisitors, SiteChatgroup) {
        var vm = this;

        vm.siteChatgroupNumVisitors = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.sitechatgroups = SiteChatgroup.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.siteChatgroupNumVisitors.id !== null) {
                SiteChatgroupNumVisitors.update(vm.siteChatgroupNumVisitors, onSaveSuccess, onSaveError);
            } else {
                SiteChatgroupNumVisitors.save(vm.siteChatgroupNumVisitors, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:siteChatgroupNumVisitorsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.numVisitorDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
