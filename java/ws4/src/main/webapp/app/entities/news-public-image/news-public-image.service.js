(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsPublicImage', NewsPublicImage);

    NewsPublicImage.$inject = ['$resource'];

    function NewsPublicImage ($resource) {
        var resourceUrl =  'api/news-public-images/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
