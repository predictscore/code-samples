(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-public-image', {
            parent: 'entity',
            url: '/news-public-image?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsPublicImages'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-public-image/news-public-images.html',
                    controller: 'NewsPublicImageController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-public-image-detail', {
            parent: 'news-public-image',
            url: '/news-public-image/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsPublicImage'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-public-image/news-public-image-detail.html',
                    controller: 'NewsPublicImageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsPublicImage', function($stateParams, NewsPublicImage) {
                    return NewsPublicImage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-public-image',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-public-image-detail.edit', {
            parent: 'news-public-image-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-public-image/news-public-image-dialog.html',
                    controller: 'NewsPublicImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsPublicImage', function(NewsPublicImage) {
                            return NewsPublicImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-public-image.new', {
            parent: 'news-public-image',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-public-image/news-public-image-dialog.html',
                    controller: 'NewsPublicImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                filename: null,
                                blob: null,
                                blobContentType: null,
                                description: null,
                                descriptionTranslation: null,
                                imageOrder: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-public-image', null, { reload: 'news-public-image' });
                }, function() {
                    $state.go('news-public-image');
                });
            }]
        })
        .state('news-public-image.edit', {
            parent: 'news-public-image',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-public-image/news-public-image-dialog.html',
                    controller: 'NewsPublicImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsPublicImage', function(NewsPublicImage) {
                            return NewsPublicImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-public-image', null, { reload: 'news-public-image' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-public-image.delete', {
            parent: 'news-public-image',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-public-image/news-public-image-delete-dialog.html',
                    controller: 'NewsPublicImageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsPublicImage', function(NewsPublicImage) {
                            return NewsPublicImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-public-image', null, { reload: 'news-public-image' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
