(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsPublicImageDialogController', NewsPublicImageDialogController);

    NewsPublicImageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'NewsPublicImage', 'News'];

    function NewsPublicImageDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, NewsPublicImage, News) {
        var vm = this;

        vm.newsPublicImage = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        vm.news = News.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsPublicImage.id !== null) {
                NewsPublicImage.update(vm.newsPublicImage, onSaveSuccess, onSaveError);
            } else {
                NewsPublicImage.save(vm.newsPublicImage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsPublicImageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        vm.setBlob = function ($file, newsPublicImage) {
            if ($file && $file.$error === 'pattern') {
                return;
            }
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        newsPublicImage.blob = base64Data;
                        newsPublicImage.blobContentType = $file.type;
                    });
                });
            }
        };

    }
})();
