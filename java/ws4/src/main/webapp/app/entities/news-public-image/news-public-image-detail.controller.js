(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsPublicImageDetailController', NewsPublicImageDetailController);

    NewsPublicImageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'NewsPublicImage', 'News'];

    function NewsPublicImageDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, NewsPublicImage, News) {
        var vm = this;

        vm.newsPublicImage = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsPublicImageUpdate', function(event, result) {
            vm.newsPublicImage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
