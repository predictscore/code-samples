(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsPublicImageDeleteController',NewsPublicImageDeleteController);

    NewsPublicImageDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsPublicImage'];

    function NewsPublicImageDeleteController($uibModalInstance, entity, NewsPublicImage) {
        var vm = this;

        vm.newsPublicImage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsPublicImage.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
