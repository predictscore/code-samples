(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsPublicImageSearch', NewsPublicImageSearch);

    NewsPublicImageSearch.$inject = ['$resource'];

    function NewsPublicImageSearch($resource) {
        var resourceUrl =  'api/_search/news-public-images/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
