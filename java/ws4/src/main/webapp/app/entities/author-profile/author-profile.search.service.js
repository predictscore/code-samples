(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('AuthorProfileSearch', AuthorProfileSearch);

    AuthorProfileSearch.$inject = ['$resource'];

    function AuthorProfileSearch($resource) {
        var resourceUrl =  'api/_search/author-profiles/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
