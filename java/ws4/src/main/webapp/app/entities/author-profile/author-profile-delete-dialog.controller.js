(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorProfileDeleteController',AuthorProfileDeleteController);

    AuthorProfileDeleteController.$inject = ['$uibModalInstance', 'entity', 'AuthorProfile'];

    function AuthorProfileDeleteController($uibModalInstance, entity, AuthorProfile) {
        var vm = this;

        vm.authorProfile = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            AuthorProfile.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
