(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('AuthorProfile', AuthorProfile);

    AuthorProfile.$inject = ['$resource', 'DateUtils'];

    function AuthorProfile ($resource, DateUtils) {
        var resourceUrl =  'api/author-profiles/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.submissionDate = DateUtils.convertDateTimeFromServer(data.submissionDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
