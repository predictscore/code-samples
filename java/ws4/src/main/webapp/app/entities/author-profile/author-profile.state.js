(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('author-profile', {
            parent: 'entity',
            url: '/author-profile?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'AuthorProfiles'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/author-profile/author-profiles.html',
                    controller: 'AuthorProfileController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('author-profile-detail', {
            parent: 'author-profile',
            url: '/author-profile/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'AuthorProfile'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/author-profile/author-profile-detail.html',
                    controller: 'AuthorProfileDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'AuthorProfile', function($stateParams, AuthorProfile) {
                    return AuthorProfile.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'author-profile',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('author-profile-detail.edit', {
            parent: 'author-profile-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-profile/author-profile-dialog.html',
                    controller: 'AuthorProfileDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuthorProfile', function(AuthorProfile) {
                            return AuthorProfile.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('author-profile.new', {
            parent: 'author-profile',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-profile/author-profile-dialog.html',
                    controller: 'AuthorProfileDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                info: null,
                                username: null,
                                submissionDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('author-profile', null, { reload: 'author-profile' });
                }, function() {
                    $state.go('author-profile');
                });
            }]
        })
        .state('author-profile.edit', {
            parent: 'author-profile',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-profile/author-profile-dialog.html',
                    controller: 'AuthorProfileDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuthorProfile', function(AuthorProfile) {
                            return AuthorProfile.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('author-profile', null, { reload: 'author-profile' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('author-profile.delete', {
            parent: 'author-profile',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-profile/author-profile-delete-dialog.html',
                    controller: 'AuthorProfileDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AuthorProfile', function(AuthorProfile) {
                            return AuthorProfile.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('author-profile', null, { reload: 'author-profile' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
