(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorProfileDialogController', AuthorProfileDialogController);

    AuthorProfileDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'AuthorProfile', 'Author'];

    function AuthorProfileDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, AuthorProfile, Author) {
        var vm = this;

        vm.authorProfile = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.authors = Author.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.authorProfile.id !== null) {
                AuthorProfile.update(vm.authorProfile, onSaveSuccess, onSaveError);
            } else {
                AuthorProfile.save(vm.authorProfile, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:authorProfileUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.submissionDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
