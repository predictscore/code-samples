(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorProfileDetailController', AuthorProfileDetailController);

    AuthorProfileDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'AuthorProfile', 'Author'];

    function AuthorProfileDetailController($scope, $rootScope, $stateParams, previousState, entity, AuthorProfile, Author) {
        var vm = this;

        vm.authorProfile = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:authorProfileUpdate', function(event, result) {
            vm.authorProfile = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
