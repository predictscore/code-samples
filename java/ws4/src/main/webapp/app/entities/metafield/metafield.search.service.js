(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('MetafieldSearch', MetafieldSearch);

    MetafieldSearch.$inject = ['$resource'];

    function MetafieldSearch($resource) {
        var resourceUrl =  'api/_search/metafields/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
