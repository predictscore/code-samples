(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('MetafieldDialogController', MetafieldDialogController);

    MetafieldDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Metafield', 'Metagroup'];

    function MetafieldDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Metafield, Metagroup) {
        var vm = this;

        vm.metafield = entity;
        vm.clear = clear;
        vm.save = save;
        vm.metagroups = Metagroup.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.metafield.id !== null) {
                Metafield.update(vm.metafield, onSaveSuccess, onSaveError);
            } else {
                Metafield.save(vm.metafield, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:metafieldUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
