(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('MetafieldDetailController', MetafieldDetailController);

    MetafieldDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Metafield', 'Metagroup'];

    function MetafieldDetailController($scope, $rootScope, $stateParams, previousState, entity, Metafield, Metagroup) {
        var vm = this;

        vm.metafield = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:metafieldUpdate', function(event, result) {
            vm.metafield = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
