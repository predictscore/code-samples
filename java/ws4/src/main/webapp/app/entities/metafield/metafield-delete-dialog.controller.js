(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('MetafieldDeleteController',MetafieldDeleteController);

    MetafieldDeleteController.$inject = ['$uibModalInstance', 'entity', 'Metafield'];

    function MetafieldDeleteController($uibModalInstance, entity, Metafield) {
        var vm = this;

        vm.metafield = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Metafield.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
