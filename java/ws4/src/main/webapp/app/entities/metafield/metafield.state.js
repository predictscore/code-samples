(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('metafield', {
            parent: 'entity',
            url: '/metafield?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'Metafields'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/metafield/metafields.html',
                    controller: 'MetafieldController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('metafield-detail', {
            parent: 'metafield',
            url: '/metafield/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'Metafield'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/metafield/metafield-detail.html',
                    controller: 'MetafieldDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Metafield', function($stateParams, Metafield) {
                    return Metafield.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'metafield',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('metafield-detail.edit', {
            parent: 'metafield-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/metafield/metafield-dialog.html',
                    controller: 'MetafieldDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Metafield', function(Metafield) {
                            return Metafield.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('metafield.new', {
            parent: 'metafield',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/metafield/metafield-dialog.html',
                    controller: 'MetafieldDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('metafield', null, { reload: 'metafield' });
                }, function() {
                    $state.go('metafield');
                });
            }]
        })
        .state('metafield.edit', {
            parent: 'metafield',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/metafield/metafield-dialog.html',
                    controller: 'MetafieldDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Metafield', function(Metafield) {
                            return Metafield.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('metafield', null, { reload: 'metafield' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('metafield.delete', {
            parent: 'metafield',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/metafield/metafield-delete-dialog.html',
                    controller: 'MetafieldDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Metafield', function(Metafield) {
                            return Metafield.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('metafield', null, { reload: 'metafield' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
