(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsUrlSearch', NewsUrlSearch);

    NewsUrlSearch.$inject = ['$resource'];

    function NewsUrlSearch($resource) {
        var resourceUrl =  'api/_search/news-urls/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
