(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsUrlDeleteController',NewsUrlDeleteController);

    NewsUrlDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsUrl'];

    function NewsUrlDeleteController($uibModalInstance, entity, NewsUrl) {
        var vm = this;

        vm.newsUrl = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsUrl.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
