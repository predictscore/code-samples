(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsUrl', NewsUrl);

    NewsUrl.$inject = ['$resource'];

    function NewsUrl ($resource) {
        var resourceUrl =  'api/news-urls/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
