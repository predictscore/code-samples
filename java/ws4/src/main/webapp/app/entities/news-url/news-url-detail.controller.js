(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsUrlDetailController', NewsUrlDetailController);

    NewsUrlDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'NewsUrl', 'News'];

    function NewsUrlDetailController($scope, $rootScope, $stateParams, previousState, entity, NewsUrl, News) {
        var vm = this;

        vm.newsUrl = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsUrlUpdate', function(event, result) {
            vm.newsUrl = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
