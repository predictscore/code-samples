(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-url', {
            parent: 'entity',
            url: '/news-url?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsUrls'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-url/news-urls.html',
                    controller: 'NewsUrlController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-url-detail', {
            parent: 'news-url',
            url: '/news-url/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsUrl'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-url/news-url-detail.html',
                    controller: 'NewsUrlDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsUrl', function($stateParams, NewsUrl) {
                    return NewsUrl.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-url',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-url-detail.edit', {
            parent: 'news-url-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-url/news-url-dialog.html',
                    controller: 'NewsUrlDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsUrl', function(NewsUrl) {
                            return NewsUrl.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-url.new', {
            parent: 'news-url',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-url/news-url-dialog.html',
                    controller: 'NewsUrlDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                url: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-url', null, { reload: 'news-url' });
                }, function() {
                    $state.go('news-url');
                });
            }]
        })
        .state('news-url.edit', {
            parent: 'news-url',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-url/news-url-dialog.html',
                    controller: 'NewsUrlDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsUrl', function(NewsUrl) {
                            return NewsUrl.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-url', null, { reload: 'news-url' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-url.delete', {
            parent: 'news-url',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-url/news-url-delete-dialog.html',
                    controller: 'NewsUrlDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsUrl', function(NewsUrl) {
                            return NewsUrl.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-url', null, { reload: 'news-url' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
