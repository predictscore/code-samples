(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsUrlDialogController', NewsUrlDialogController);

    NewsUrlDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NewsUrl', 'News'];

    function NewsUrlDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NewsUrl, News) {
        var vm = this;

        vm.newsUrl = entity;
        vm.clear = clear;
        vm.save = save;
        vm.news = News.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsUrl.id !== null) {
                NewsUrl.update(vm.newsUrl, onSaveSuccess, onSaveError);
            } else {
                NewsUrl.save(vm.newsUrl, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsUrlUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
