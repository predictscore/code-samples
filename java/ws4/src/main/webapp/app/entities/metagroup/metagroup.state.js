(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('metagroup', {
            parent: 'entity',
            url: '/metagroup?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'Metagroups'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/metagroup/metagroups.html',
                    controller: 'MetagroupController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('metagroup-detail', {
            parent: 'metagroup',
            url: '/metagroup/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'Metagroup'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/metagroup/metagroup-detail.html',
                    controller: 'MetagroupDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Metagroup', function($stateParams, Metagroup) {
                    return Metagroup.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'metagroup',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('metagroup-detail.edit', {
            parent: 'metagroup-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/metagroup/metagroup-dialog.html',
                    controller: 'MetagroupDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Metagroup', function(Metagroup) {
                            return Metagroup.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('metagroup.new', {
            parent: 'metagroup',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/metagroup/metagroup-dialog.html',
                    controller: 'MetagroupDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('metagroup', null, { reload: 'metagroup' });
                }, function() {
                    $state.go('metagroup');
                });
            }]
        })
        .state('metagroup.edit', {
            parent: 'metagroup',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/metagroup/metagroup-dialog.html',
                    controller: 'MetagroupDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Metagroup', function(Metagroup) {
                            return Metagroup.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('metagroup', null, { reload: 'metagroup' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('metagroup.delete', {
            parent: 'metagroup',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/metagroup/metagroup-delete-dialog.html',
                    controller: 'MetagroupDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Metagroup', function(Metagroup) {
                            return Metagroup.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('metagroup', null, { reload: 'metagroup' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
