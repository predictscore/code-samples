(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('MetagroupDeleteController',MetagroupDeleteController);

    MetagroupDeleteController.$inject = ['$uibModalInstance', 'entity', 'Metagroup'];

    function MetagroupDeleteController($uibModalInstance, entity, Metagroup) {
        var vm = this;

        vm.metagroup = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Metagroup.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
