(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('MetagroupDialogController', MetagroupDialogController);

    MetagroupDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Metagroup'];

    function MetagroupDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Metagroup) {
        var vm = this;

        vm.metagroup = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.metagroup.id !== null) {
                Metagroup.update(vm.metagroup, onSaveSuccess, onSaveError);
            } else {
                Metagroup.save(vm.metagroup, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:metagroupUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
