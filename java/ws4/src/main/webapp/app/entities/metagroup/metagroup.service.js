(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('Metagroup', Metagroup);

    Metagroup.$inject = ['$resource'];

    function Metagroup ($resource) {
        var resourceUrl =  'api/metagroups/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
