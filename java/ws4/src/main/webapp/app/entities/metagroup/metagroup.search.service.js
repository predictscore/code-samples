(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('MetagroupSearch', MetagroupSearch);

    MetagroupSearch.$inject = ['$resource'];

    function MetagroupSearch($resource) {
        var resourceUrl =  'api/_search/metagroups/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
