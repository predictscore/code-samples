(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('MetagroupDetailController', MetagroupDetailController);

    MetagroupDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Metagroup'];

    function MetagroupDetailController($scope, $rootScope, $stateParams, previousState, entity, Metagroup) {
        var vm = this;

        vm.metagroup = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:metagroupUpdate', function(event, result) {
            vm.metagroup = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
