(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('ContactTypeDeleteController',ContactTypeDeleteController);

    ContactTypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'ContactType'];

    function ContactTypeDeleteController($uibModalInstance, entity, ContactType) {
        var vm = this;

        vm.contactType = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ContactType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
