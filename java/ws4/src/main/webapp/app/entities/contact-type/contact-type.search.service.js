(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('ContactTypeSearch', ContactTypeSearch);

    ContactTypeSearch.$inject = ['$resource'];

    function ContactTypeSearch($resource) {
        var resourceUrl =  'api/_search/contact-types/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
