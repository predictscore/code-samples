(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('ContactTypeDetailController', ContactTypeDetailController);

    ContactTypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ContactType'];

    function ContactTypeDetailController($scope, $rootScope, $stateParams, previousState, entity, ContactType) {
        var vm = this;

        vm.contactType = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:contactTypeUpdate', function(event, result) {
            vm.contactType = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
