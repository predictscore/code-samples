(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('ContactTypeDialogController', ContactTypeDialogController);

    ContactTypeDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ContactType'];

    function ContactTypeDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ContactType) {
        var vm = this;

        vm.contactType = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.contactType.id !== null) {
                ContactType.update(vm.contactType, onSaveSuccess, onSaveError);
            } else {
                ContactType.save(vm.contactType, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:contactTypeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
