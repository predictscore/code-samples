(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('site-chatgroup-to-tag', {
            parent: 'entity',
            url: '/site-chatgroup-to-tag?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupToTags'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-to-tag/site-chatgroup-to-tags.html',
                    controller: 'SiteChatgroupToTagController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('site-chatgroup-to-tag-detail', {
            parent: 'site-chatgroup-to-tag',
            url: '/site-chatgroup-to-tag/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupToTag'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-to-tag/site-chatgroup-to-tag-detail.html',
                    controller: 'SiteChatgroupToTagDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SiteChatgroupToTag', function($stateParams, SiteChatgroupToTag) {
                    return SiteChatgroupToTag.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'site-chatgroup-to-tag',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('site-chatgroup-to-tag-detail.edit', {
            parent: 'site-chatgroup-to-tag-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-to-tag/site-chatgroup-to-tag-dialog.html',
                    controller: 'SiteChatgroupToTagDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupToTag', function(SiteChatgroupToTag) {
                            return SiteChatgroupToTag.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-to-tag.new', {
            parent: 'site-chatgroup-to-tag',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-to-tag/site-chatgroup-to-tag-dialog.html',
                    controller: 'SiteChatgroupToTagDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-to-tag', null, { reload: 'site-chatgroup-to-tag' });
                }, function() {
                    $state.go('site-chatgroup-to-tag');
                });
            }]
        })
        .state('site-chatgroup-to-tag.edit', {
            parent: 'site-chatgroup-to-tag',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-to-tag/site-chatgroup-to-tag-dialog.html',
                    controller: 'SiteChatgroupToTagDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupToTag', function(SiteChatgroupToTag) {
                            return SiteChatgroupToTag.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-to-tag', null, { reload: 'site-chatgroup-to-tag' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-to-tag.delete', {
            parent: 'site-chatgroup-to-tag',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-to-tag/site-chatgroup-to-tag-delete-dialog.html',
                    controller: 'SiteChatgroupToTagDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SiteChatgroupToTag', function(SiteChatgroupToTag) {
                            return SiteChatgroupToTag.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-to-tag', null, { reload: 'site-chatgroup-to-tag' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
