(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupToTagDeleteController',SiteChatgroupToTagDeleteController);

    SiteChatgroupToTagDeleteController.$inject = ['$uibModalInstance', 'entity', 'SiteChatgroupToTag'];

    function SiteChatgroupToTagDeleteController($uibModalInstance, entity, SiteChatgroupToTag) {
        var vm = this;

        vm.siteChatgroupToTag = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SiteChatgroupToTag.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
