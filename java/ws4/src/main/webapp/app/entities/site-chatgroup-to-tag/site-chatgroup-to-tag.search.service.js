(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupToTagSearch', SiteChatgroupToTagSearch);

    SiteChatgroupToTagSearch.$inject = ['$resource'];

    function SiteChatgroupToTagSearch($resource) {
        var resourceUrl =  'api/_search/site-chatgroup-to-tags/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
