(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupToTag', SiteChatgroupToTag);

    SiteChatgroupToTag.$inject = ['$resource'];

    function SiteChatgroupToTag ($resource) {
        var resourceUrl =  'api/site-chatgroup-to-tags/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
