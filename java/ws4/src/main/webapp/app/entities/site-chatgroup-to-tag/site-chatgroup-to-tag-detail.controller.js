(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupToTagDetailController', SiteChatgroupToTagDetailController);

    SiteChatgroupToTagDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SiteChatgroupToTag', 'SiteChatgroup', 'Tag'];

    function SiteChatgroupToTagDetailController($scope, $rootScope, $stateParams, previousState, entity, SiteChatgroupToTag, SiteChatgroup, Tag) {
        var vm = this;

        vm.siteChatgroupToTag = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:siteChatgroupToTagUpdate', function(event, result) {
            vm.siteChatgroupToTag = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
