(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupToTagDialogController', SiteChatgroupToTagDialogController);

    SiteChatgroupToTagDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SiteChatgroupToTag', 'SiteChatgroup', 'Tag'];

    function SiteChatgroupToTagDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SiteChatgroupToTag, SiteChatgroup, Tag) {
        var vm = this;

        vm.siteChatgroupToTag = entity;
        vm.clear = clear;
        vm.save = save;
        vm.sitechatgroups = SiteChatgroup.query();
        vm.tags = Tag.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.siteChatgroupToTag.id !== null) {
                SiteChatgroupToTag.update(vm.siteChatgroupToTag, onSaveSuccess, onSaveError);
            } else {
                SiteChatgroupToTag.save(vm.siteChatgroupToTag, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:siteChatgroupToTagUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
