(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsToRelatedAuthorDetailController', NewsToRelatedAuthorDetailController);

    NewsToRelatedAuthorDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'NewsToRelatedAuthor', 'News', 'Author'];

    function NewsToRelatedAuthorDetailController($scope, $rootScope, $stateParams, previousState, entity, NewsToRelatedAuthor, News, Author) {
        var vm = this;

        vm.newsToRelatedAuthor = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsToRelatedAuthorUpdate', function(event, result) {
            vm.newsToRelatedAuthor = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
