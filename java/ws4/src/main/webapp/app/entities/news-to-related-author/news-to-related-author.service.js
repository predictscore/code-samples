(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsToRelatedAuthor', NewsToRelatedAuthor);

    NewsToRelatedAuthor.$inject = ['$resource'];

    function NewsToRelatedAuthor ($resource) {
        var resourceUrl =  'api/news-to-related-authors/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
