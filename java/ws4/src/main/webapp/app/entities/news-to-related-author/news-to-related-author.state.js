(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-to-related-author', {
            parent: 'entity',
            url: '/news-to-related-author',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsToRelatedAuthors'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-to-related-author/news-to-related-authors.html',
                    controller: 'NewsToRelatedAuthorController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('news-to-related-author-detail', {
            parent: 'news-to-related-author',
            url: '/news-to-related-author/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsToRelatedAuthor'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-to-related-author/news-to-related-author-detail.html',
                    controller: 'NewsToRelatedAuthorDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsToRelatedAuthor', function($stateParams, NewsToRelatedAuthor) {
                    return NewsToRelatedAuthor.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-to-related-author',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-to-related-author-detail.edit', {
            parent: 'news-to-related-author-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-to-related-author/news-to-related-author-dialog.html',
                    controller: 'NewsToRelatedAuthorDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsToRelatedAuthor', function(NewsToRelatedAuthor) {
                            return NewsToRelatedAuthor.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-to-related-author.new', {
            parent: 'news-to-related-author',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-to-related-author/news-to-related-author-dialog.html',
                    controller: 'NewsToRelatedAuthorDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-to-related-author', null, { reload: 'news-to-related-author' });
                }, function() {
                    $state.go('news-to-related-author');
                });
            }]
        })
        .state('news-to-related-author.edit', {
            parent: 'news-to-related-author',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-to-related-author/news-to-related-author-dialog.html',
                    controller: 'NewsToRelatedAuthorDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsToRelatedAuthor', function(NewsToRelatedAuthor) {
                            return NewsToRelatedAuthor.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-to-related-author', null, { reload: 'news-to-related-author' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-to-related-author.delete', {
            parent: 'news-to-related-author',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-to-related-author/news-to-related-author-delete-dialog.html',
                    controller: 'NewsToRelatedAuthorDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsToRelatedAuthor', function(NewsToRelatedAuthor) {
                            return NewsToRelatedAuthor.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-to-related-author', null, { reload: 'news-to-related-author' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
