(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsToRelatedAuthorController', NewsToRelatedAuthorController);

    NewsToRelatedAuthorController.$inject = ['NewsToRelatedAuthor', 'NewsToRelatedAuthorSearch'];

    function NewsToRelatedAuthorController(NewsToRelatedAuthor, NewsToRelatedAuthorSearch) {

        var vm = this;

        vm.newsToRelatedAuthors = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            NewsToRelatedAuthor.query(function(result) {
                vm.newsToRelatedAuthors = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            NewsToRelatedAuthorSearch.query({query: vm.searchQuery}, function(result) {
                vm.newsToRelatedAuthors = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();
