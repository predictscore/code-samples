(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsToRelatedAuthorDialogController', NewsToRelatedAuthorDialogController);

    NewsToRelatedAuthorDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NewsToRelatedAuthor', 'News', 'Author'];

    function NewsToRelatedAuthorDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NewsToRelatedAuthor, News, Author) {
        var vm = this;

        vm.newsToRelatedAuthor = entity;
        vm.clear = clear;
        vm.save = save;
        vm.news = News.query();
        vm.authors = Author.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsToRelatedAuthor.id !== null) {
                NewsToRelatedAuthor.update(vm.newsToRelatedAuthor, onSaveSuccess, onSaveError);
            } else {
                NewsToRelatedAuthor.save(vm.newsToRelatedAuthor, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsToRelatedAuthorUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
