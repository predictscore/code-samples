(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsToRelatedAuthorSearch', NewsToRelatedAuthorSearch);

    NewsToRelatedAuthorSearch.$inject = ['$resource'];

    function NewsToRelatedAuthorSearch($resource) {
        var resourceUrl =  'api/_search/news-to-related-authors/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
