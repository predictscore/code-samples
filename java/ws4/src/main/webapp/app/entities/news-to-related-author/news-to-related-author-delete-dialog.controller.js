(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsToRelatedAuthorDeleteController',NewsToRelatedAuthorDeleteController);

    NewsToRelatedAuthorDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsToRelatedAuthor'];

    function NewsToRelatedAuthorDeleteController($uibModalInstance, entity, NewsToRelatedAuthor) {
        var vm = this;

        vm.newsToRelatedAuthor = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsToRelatedAuthor.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
