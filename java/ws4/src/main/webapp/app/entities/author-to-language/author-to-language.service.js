(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('AuthorToLanguage', AuthorToLanguage);

    AuthorToLanguage.$inject = ['$resource'];

    function AuthorToLanguage ($resource) {
        var resourceUrl =  'api/author-to-languages/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
