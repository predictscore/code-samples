(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorToLanguageDeleteController',AuthorToLanguageDeleteController);

    AuthorToLanguageDeleteController.$inject = ['$uibModalInstance', 'entity', 'AuthorToLanguage'];

    function AuthorToLanguageDeleteController($uibModalInstance, entity, AuthorToLanguage) {
        var vm = this;

        vm.authorToLanguage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            AuthorToLanguage.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
