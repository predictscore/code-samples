(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorToLanguageDialogController', AuthorToLanguageDialogController);

    AuthorToLanguageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'AuthorToLanguage', 'Author', 'Language'];

    function AuthorToLanguageDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, AuthorToLanguage, Author, Language) {
        var vm = this;

        vm.authorToLanguage = entity;
        vm.clear = clear;
        vm.save = save;
        vm.authors = Author.query();
        vm.languages = Language.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.authorToLanguage.id !== null) {
                AuthorToLanguage.update(vm.authorToLanguage, onSaveSuccess, onSaveError);
            } else {
                AuthorToLanguage.save(vm.authorToLanguage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:authorToLanguageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
