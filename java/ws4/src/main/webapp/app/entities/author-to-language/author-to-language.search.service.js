(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('AuthorToLanguageSearch', AuthorToLanguageSearch);

    AuthorToLanguageSearch.$inject = ['$resource'];

    function AuthorToLanguageSearch($resource) {
        var resourceUrl =  'api/_search/author-to-languages/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
