(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('author-to-language', {
            parent: 'entity',
            url: '/author-to-language?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'AuthorToLanguages'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/author-to-language/author-to-languages.html',
                    controller: 'AuthorToLanguageController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('author-to-language-detail', {
            parent: 'author-to-language',
            url: '/author-to-language/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'AuthorToLanguage'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/author-to-language/author-to-language-detail.html',
                    controller: 'AuthorToLanguageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'AuthorToLanguage', function($stateParams, AuthorToLanguage) {
                    return AuthorToLanguage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'author-to-language',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('author-to-language-detail.edit', {
            parent: 'author-to-language-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-language/author-to-language-dialog.html',
                    controller: 'AuthorToLanguageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuthorToLanguage', function(AuthorToLanguage) {
                            return AuthorToLanguage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('author-to-language.new', {
            parent: 'author-to-language',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-language/author-to-language-dialog.html',
                    controller: 'AuthorToLanguageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('author-to-language', null, { reload: 'author-to-language' });
                }, function() {
                    $state.go('author-to-language');
                });
            }]
        })
        .state('author-to-language.edit', {
            parent: 'author-to-language',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-language/author-to-language-dialog.html',
                    controller: 'AuthorToLanguageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuthorToLanguage', function(AuthorToLanguage) {
                            return AuthorToLanguage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('author-to-language', null, { reload: 'author-to-language' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('author-to-language.delete', {
            parent: 'author-to-language',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-to-language/author-to-language-delete-dialog.html',
                    controller: 'AuthorToLanguageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AuthorToLanguage', function(AuthorToLanguage) {
                            return AuthorToLanguage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('author-to-language', null, { reload: 'author-to-language' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
