(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorToLanguageDetailController', AuthorToLanguageDetailController);

    AuthorToLanguageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'AuthorToLanguage', 'Author', 'Language'];

    function AuthorToLanguageDetailController($scope, $rootScope, $stateParams, previousState, entity, AuthorToLanguage, Author, Language) {
        var vm = this;

        vm.authorToLanguage = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:authorToLanguageUpdate', function(event, result) {
            vm.authorToLanguage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
