(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorNameDeleteController',AuthorNameDeleteController);

    AuthorNameDeleteController.$inject = ['$uibModalInstance', 'entity', 'AuthorName'];

    function AuthorNameDeleteController($uibModalInstance, entity, AuthorName) {
        var vm = this;

        vm.authorName = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            AuthorName.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
