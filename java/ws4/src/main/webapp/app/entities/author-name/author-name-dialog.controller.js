(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorNameDialogController', AuthorNameDialogController);

    AuthorNameDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'AuthorName', 'Author'];

    function AuthorNameDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, AuthorName, Author) {
        var vm = this;

        vm.authorName = entity;
        vm.clear = clear;
        vm.save = save;
        vm.authors = Author.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.authorName.id !== null) {
                AuthorName.update(vm.authorName, onSaveSuccess, onSaveError);
            } else {
                AuthorName.save(vm.authorName, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:authorNameUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
