(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('AuthorName', AuthorName);

    AuthorName.$inject = ['$resource'];

    function AuthorName ($resource) {
        var resourceUrl =  'api/author-names/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
