(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('author-name', {
            parent: 'entity',
            url: '/author-name?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'AuthorNames'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/author-name/author-names.html',
                    controller: 'AuthorNameController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('author-name-detail', {
            parent: 'author-name',
            url: '/author-name/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'AuthorName'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/author-name/author-name-detail.html',
                    controller: 'AuthorNameDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'AuthorName', function($stateParams, AuthorName) {
                    return AuthorName.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'author-name',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('author-name-detail.edit', {
            parent: 'author-name-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-name/author-name-dialog.html',
                    controller: 'AuthorNameDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuthorName', function(AuthorName) {
                            return AuthorName.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('author-name.new', {
            parent: 'author-name',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-name/author-name-dialog.html',
                    controller: 'AuthorNameDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                originalName: null,
                                englishName: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('author-name', null, { reload: 'author-name' });
                }, function() {
                    $state.go('author-name');
                });
            }]
        })
        .state('author-name.edit', {
            parent: 'author-name',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-name/author-name-dialog.html',
                    controller: 'AuthorNameDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AuthorName', function(AuthorName) {
                            return AuthorName.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('author-name', null, { reload: 'author-name' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('author-name.delete', {
            parent: 'author-name',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/author-name/author-name-delete-dialog.html',
                    controller: 'AuthorNameDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AuthorName', function(AuthorName) {
                            return AuthorName.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('author-name', null, { reload: 'author-name' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
