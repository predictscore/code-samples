(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('AuthorNameDetailController', AuthorNameDetailController);

    AuthorNameDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'AuthorName', 'Author'];

    function AuthorNameDetailController($scope, $rootScope, $stateParams, previousState, entity, AuthorName, Author) {
        var vm = this;

        vm.authorName = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:authorNameUpdate', function(event, result) {
            vm.authorName = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
