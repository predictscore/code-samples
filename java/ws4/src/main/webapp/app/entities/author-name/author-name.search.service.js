(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('AuthorNameSearch', AuthorNameSearch);

    AuthorNameSearch.$inject = ['$resource'];

    function AuthorNameSearch($resource) {
        var resourceUrl =  'api/_search/author-names/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
