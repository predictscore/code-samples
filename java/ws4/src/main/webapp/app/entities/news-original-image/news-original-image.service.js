(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsOriginalImage', NewsOriginalImage);

    NewsOriginalImage.$inject = ['$resource'];

    function NewsOriginalImage ($resource) {
        var resourceUrl =  'api/news-original-images/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
