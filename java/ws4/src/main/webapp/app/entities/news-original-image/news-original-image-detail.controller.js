(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsOriginalImageDetailController', NewsOriginalImageDetailController);

    NewsOriginalImageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'NewsOriginalImage', 'News'];

    function NewsOriginalImageDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, NewsOriginalImage, News) {
        var vm = this;

        vm.newsOriginalImage = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsOriginalImageUpdate', function(event, result) {
            vm.newsOriginalImage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
