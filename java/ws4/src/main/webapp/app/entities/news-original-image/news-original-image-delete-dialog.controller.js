(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsOriginalImageDeleteController',NewsOriginalImageDeleteController);

    NewsOriginalImageDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsOriginalImage'];

    function NewsOriginalImageDeleteController($uibModalInstance, entity, NewsOriginalImage) {
        var vm = this;

        vm.newsOriginalImage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsOriginalImage.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
