(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-original-image', {
            parent: 'entity',
            url: '/news-original-image?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsOriginalImages'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-original-image/news-original-images.html',
                    controller: 'NewsOriginalImageController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-original-image-detail', {
            parent: 'news-original-image',
            url: '/news-original-image/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsOriginalImage'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-original-image/news-original-image-detail.html',
                    controller: 'NewsOriginalImageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsOriginalImage', function($stateParams, NewsOriginalImage) {
                    return NewsOriginalImage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-original-image',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-original-image-detail.edit', {
            parent: 'news-original-image-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-original-image/news-original-image-dialog.html',
                    controller: 'NewsOriginalImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsOriginalImage', function(NewsOriginalImage) {
                            return NewsOriginalImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-original-image.new', {
            parent: 'news-original-image',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-original-image/news-original-image-dialog.html',
                    controller: 'NewsOriginalImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                filename: null,
                                blob: null,
                                blobContentType: null,
                                description: null,
                                imageOrder: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-original-image', null, { reload: 'news-original-image' });
                }, function() {
                    $state.go('news-original-image');
                });
            }]
        })
        .state('news-original-image.edit', {
            parent: 'news-original-image',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-original-image/news-original-image-dialog.html',
                    controller: 'NewsOriginalImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsOriginalImage', function(NewsOriginalImage) {
                            return NewsOriginalImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-original-image', null, { reload: 'news-original-image' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-original-image.delete', {
            parent: 'news-original-image',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-original-image/news-original-image-delete-dialog.html',
                    controller: 'NewsOriginalImageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsOriginalImage', function(NewsOriginalImage) {
                            return NewsOriginalImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-original-image', null, { reload: 'news-original-image' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
