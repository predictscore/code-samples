(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsOriginalImageDialogController', NewsOriginalImageDialogController);

    NewsOriginalImageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'NewsOriginalImage', 'News'];

    function NewsOriginalImageDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, NewsOriginalImage, News) {
        var vm = this;

        vm.newsOriginalImage = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        vm.news = News.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsOriginalImage.id !== null) {
                NewsOriginalImage.update(vm.newsOriginalImage, onSaveSuccess, onSaveError);
            } else {
                NewsOriginalImage.save(vm.newsOriginalImage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsOriginalImageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        vm.setBlob = function ($file, newsOriginalImage) {
            if ($file && $file.$error === 'pattern') {
                return;
            }
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        newsOriginalImage.blob = base64Data;
                        newsOriginalImage.blobContentType = $file.type;
                    });
                });
            }
        };

    }
})();
