(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsOriginalImageSearch', NewsOriginalImageSearch);

    NewsOriginalImageSearch.$inject = ['$resource'];

    function NewsOriginalImageSearch($resource) {
        var resourceUrl =  'api/_search/news-original-images/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
