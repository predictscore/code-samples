(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupDomainSearch', SiteChatgroupDomainSearch);

    SiteChatgroupDomainSearch.$inject = ['$resource'];

    function SiteChatgroupDomainSearch($resource) {
        var resourceUrl =  'api/_search/site-chatgroup-domains/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
