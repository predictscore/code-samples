(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('site-chatgroup-domain', {
            parent: 'entity',
            url: '/site-chatgroup-domain?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupDomains'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-domain/site-chatgroup-domains.html',
                    controller: 'SiteChatgroupDomainController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('site-chatgroup-domain-detail', {
            parent: 'site-chatgroup-domain',
            url: '/site-chatgroup-domain/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupDomain'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-domain/site-chatgroup-domain-detail.html',
                    controller: 'SiteChatgroupDomainDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SiteChatgroupDomain', function($stateParams, SiteChatgroupDomain) {
                    return SiteChatgroupDomain.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'site-chatgroup-domain',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('site-chatgroup-domain-detail.edit', {
            parent: 'site-chatgroup-domain-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-domain/site-chatgroup-domain-dialog.html',
                    controller: 'SiteChatgroupDomainDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupDomain', function(SiteChatgroupDomain) {
                            return SiteChatgroupDomain.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-domain.new', {
            parent: 'site-chatgroup-domain',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-domain/site-chatgroup-domain-dialog.html',
                    controller: 'SiteChatgroupDomainDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                domainId: null,
                                domainInfo: null,
                                firstUsedDate: null,
                                lastUsedDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-domain', null, { reload: 'site-chatgroup-domain' });
                }, function() {
                    $state.go('site-chatgroup-domain');
                });
            }]
        })
        .state('site-chatgroup-domain.edit', {
            parent: 'site-chatgroup-domain',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-domain/site-chatgroup-domain-dialog.html',
                    controller: 'SiteChatgroupDomainDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupDomain', function(SiteChatgroupDomain) {
                            return SiteChatgroupDomain.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-domain', null, { reload: 'site-chatgroup-domain' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-domain.delete', {
            parent: 'site-chatgroup-domain',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-domain/site-chatgroup-domain-delete-dialog.html',
                    controller: 'SiteChatgroupDomainDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SiteChatgroupDomain', function(SiteChatgroupDomain) {
                            return SiteChatgroupDomain.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-domain', null, { reload: 'site-chatgroup-domain' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
