(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupDomain', SiteChatgroupDomain);

    SiteChatgroupDomain.$inject = ['$resource', 'DateUtils'];

    function SiteChatgroupDomain ($resource, DateUtils) {
        var resourceUrl =  'api/site-chatgroup-domains/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.firstUsedDate = DateUtils.convertDateTimeFromServer(data.firstUsedDate);
                        data.lastUsedDate = DateUtils.convertDateTimeFromServer(data.lastUsedDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
