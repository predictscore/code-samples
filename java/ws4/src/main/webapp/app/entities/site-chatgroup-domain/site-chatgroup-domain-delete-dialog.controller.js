(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupDomainDeleteController',SiteChatgroupDomainDeleteController);

    SiteChatgroupDomainDeleteController.$inject = ['$uibModalInstance', 'entity', 'SiteChatgroupDomain'];

    function SiteChatgroupDomainDeleteController($uibModalInstance, entity, SiteChatgroupDomain) {
        var vm = this;

        vm.siteChatgroupDomain = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SiteChatgroupDomain.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
