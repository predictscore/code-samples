(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupDomainDialogController', SiteChatgroupDomainDialogController);

    SiteChatgroupDomainDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SiteChatgroupDomain', 'SiteChatgroup'];

    function SiteChatgroupDomainDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SiteChatgroupDomain, SiteChatgroup) {
        var vm = this;

        vm.siteChatgroupDomain = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.sitechatgroups = SiteChatgroup.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.siteChatgroupDomain.id !== null) {
                SiteChatgroupDomain.update(vm.siteChatgroupDomain, onSaveSuccess, onSaveError);
            } else {
                SiteChatgroupDomain.save(vm.siteChatgroupDomain, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:siteChatgroupDomainUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.firstUsedDate = false;
        vm.datePickerOpenStatus.lastUsedDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
