(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupDomainDetailController', SiteChatgroupDomainDetailController);

    SiteChatgroupDomainDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SiteChatgroupDomain', 'SiteChatgroup'];

    function SiteChatgroupDomainDetailController($scope, $rootScope, $stateParams, previousState, entity, SiteChatgroupDomain, SiteChatgroup) {
        var vm = this;

        vm.siteChatgroupDomain = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:siteChatgroupDomainUpdate', function(event, result) {
            vm.siteChatgroupDomain = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
