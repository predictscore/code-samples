(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryPublicImageDialogController', NewsSummaryPublicImageDialogController);

    NewsSummaryPublicImageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'NewsSummaryPublicImage', 'NewsSummary'];

    function NewsSummaryPublicImageDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, NewsSummaryPublicImage, NewsSummary) {
        var vm = this;

        vm.newsSummaryPublicImage = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        vm.newssummaries = NewsSummary.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.newsSummaryPublicImage.id !== null) {
                NewsSummaryPublicImage.update(vm.newsSummaryPublicImage, onSaveSuccess, onSaveError);
            } else {
                NewsSummaryPublicImage.save(vm.newsSummaryPublicImage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:newsSummaryPublicImageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        vm.setBlob = function ($file, newsSummaryPublicImage) {
            if ($file && $file.$error === 'pattern') {
                return;
            }
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        newsSummaryPublicImage.blob = base64Data;
                        newsSummaryPublicImage.blobContentType = $file.type;
                    });
                });
            }
        };

    }
})();
