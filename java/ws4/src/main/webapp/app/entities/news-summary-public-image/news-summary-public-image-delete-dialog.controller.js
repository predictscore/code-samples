(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryPublicImageDeleteController',NewsSummaryPublicImageDeleteController);

    NewsSummaryPublicImageDeleteController.$inject = ['$uibModalInstance', 'entity', 'NewsSummaryPublicImage'];

    function NewsSummaryPublicImageDeleteController($uibModalInstance, entity, NewsSummaryPublicImage) {
        var vm = this;

        vm.newsSummaryPublicImage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NewsSummaryPublicImage.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
