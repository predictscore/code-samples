(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('NewsSummaryPublicImageDetailController', NewsSummaryPublicImageDetailController);

    NewsSummaryPublicImageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'NewsSummaryPublicImage', 'NewsSummary'];

    function NewsSummaryPublicImageDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, NewsSummaryPublicImage, NewsSummary) {
        var vm = this;

        vm.newsSummaryPublicImage = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('newmeridianApp:newsSummaryPublicImageUpdate', function(event, result) {
            vm.newsSummaryPublicImage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
