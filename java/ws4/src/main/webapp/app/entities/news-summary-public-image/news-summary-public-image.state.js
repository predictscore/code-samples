(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('news-summary-public-image', {
            parent: 'entity',
            url: '/news-summary-public-image?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummaryPublicImages'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary-public-image/news-summary-public-images.html',
                    controller: 'NewsSummaryPublicImageController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('news-summary-public-image-detail', {
            parent: 'news-summary-public-image',
            url: '/news-summary-public-image/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'NewsSummaryPublicImage'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/news-summary-public-image/news-summary-public-image-detail.html',
                    controller: 'NewsSummaryPublicImageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'NewsSummaryPublicImage', function($stateParams, NewsSummaryPublicImage) {
                    return NewsSummaryPublicImage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'news-summary-public-image',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('news-summary-public-image-detail.edit', {
            parent: 'news-summary-public-image-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-public-image/news-summary-public-image-dialog.html',
                    controller: 'NewsSummaryPublicImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummaryPublicImage', function(NewsSummaryPublicImage) {
                            return NewsSummaryPublicImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary-public-image.new', {
            parent: 'news-summary-public-image',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-public-image/news-summary-public-image-dialog.html',
                    controller: 'NewsSummaryPublicImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                filename: null,
                                blob: null,
                                blobContentType: null,
                                description: null,
                                descriptionTranslation: null,
                                imageOrder: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('news-summary-public-image', null, { reload: 'news-summary-public-image' });
                }, function() {
                    $state.go('news-summary-public-image');
                });
            }]
        })
        .state('news-summary-public-image.edit', {
            parent: 'news-summary-public-image',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-public-image/news-summary-public-image-dialog.html',
                    controller: 'NewsSummaryPublicImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NewsSummaryPublicImage', function(NewsSummaryPublicImage) {
                            return NewsSummaryPublicImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary-public-image', null, { reload: 'news-summary-public-image' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('news-summary-public-image.delete', {
            parent: 'news-summary-public-image',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/news-summary-public-image/news-summary-public-image-delete-dialog.html',
                    controller: 'NewsSummaryPublicImageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NewsSummaryPublicImage', function(NewsSummaryPublicImage) {
                            return NewsSummaryPublicImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('news-summary-public-image', null, { reload: 'news-summary-public-image' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
