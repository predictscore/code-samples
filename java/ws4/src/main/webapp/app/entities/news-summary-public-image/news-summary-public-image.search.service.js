(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('NewsSummaryPublicImageSearch', NewsSummaryPublicImageSearch);

    NewsSummaryPublicImageSearch.$inject = ['$resource'];

    function NewsSummaryPublicImageSearch($resource) {
        var resourceUrl =  'api/_search/news-summary-public-images/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
