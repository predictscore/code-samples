(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('NewsSummaryPublicImage', NewsSummaryPublicImage);

    NewsSummaryPublicImage.$inject = ['$resource'];

    function NewsSummaryPublicImage ($resource) {
        var resourceUrl =  'api/news-summary-public-images/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
