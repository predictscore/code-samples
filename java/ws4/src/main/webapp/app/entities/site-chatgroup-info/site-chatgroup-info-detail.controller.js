(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupInfoDetailController', SiteChatgroupInfoDetailController);

    SiteChatgroupInfoDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SiteChatgroupInfo', 'SiteChatgroup'];

    function SiteChatgroupInfoDetailController($scope, $rootScope, $stateParams, previousState, entity, SiteChatgroupInfo, SiteChatgroup) {
        var vm = this;

        vm.siteChatgroupInfo = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('newmeridianApp:siteChatgroupInfoUpdate', function(event, result) {
            vm.siteChatgroupInfo = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
