(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupInfoSearch', SiteChatgroupInfoSearch);

    SiteChatgroupInfoSearch.$inject = ['$resource'];

    function SiteChatgroupInfoSearch($resource) {
        var resourceUrl =  'api/_search/site-chatgroup-infos/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
