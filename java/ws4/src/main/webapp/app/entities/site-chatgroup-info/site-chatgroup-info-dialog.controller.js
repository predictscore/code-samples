(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupInfoDialogController', SiteChatgroupInfoDialogController);

    SiteChatgroupInfoDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SiteChatgroupInfo', 'SiteChatgroup'];

    function SiteChatgroupInfoDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SiteChatgroupInfo, SiteChatgroup) {
        var vm = this;

        vm.siteChatgroupInfo = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.sitechatgroups = SiteChatgroup.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.siteChatgroupInfo.id !== null) {
                SiteChatgroupInfo.update(vm.siteChatgroupInfo, onSaveSuccess, onSaveError);
            } else {
                SiteChatgroupInfo.save(vm.siteChatgroupInfo, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('newmeridianApp:siteChatgroupInfoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.submissionDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
