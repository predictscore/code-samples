(function() {
    'use strict';
    angular
        .module('newmeridianApp')
        .factory('SiteChatgroupInfo', SiteChatgroupInfo);

    SiteChatgroupInfo.$inject = ['$resource', 'DateUtils'];

    function SiteChatgroupInfo ($resource, DateUtils) {
        var resourceUrl =  'api/site-chatgroup-infos/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.submissionDate = DateUtils.convertDateTimeFromServer(data.submissionDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
