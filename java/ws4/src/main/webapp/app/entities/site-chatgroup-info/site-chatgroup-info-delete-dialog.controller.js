(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .controller('SiteChatgroupInfoDeleteController',SiteChatgroupInfoDeleteController);

    SiteChatgroupInfoDeleteController.$inject = ['$uibModalInstance', 'entity', 'SiteChatgroupInfo'];

    function SiteChatgroupInfoDeleteController($uibModalInstance, entity, SiteChatgroupInfo) {
        var vm = this;

        vm.siteChatgroupInfo = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SiteChatgroupInfo.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
