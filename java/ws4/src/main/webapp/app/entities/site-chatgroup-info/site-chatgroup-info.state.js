(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('site-chatgroup-info', {
            parent: 'entity',
            url: '/site-chatgroup-info?page&sort&search',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupInfos'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-info/site-chatgroup-infos.html',
                    controller: 'SiteChatgroupInfoController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }]
            }
        })
        .state('site-chatgroup-info-detail', {
            parent: 'site-chatgroup-info',
            url: '/site-chatgroup-info/{id}',
            data: {
                authorities: ['ROLE_WEBUI'],
                pageTitle: 'SiteChatgroupInfo'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-chatgroup-info/site-chatgroup-info-detail.html',
                    controller: 'SiteChatgroupInfoDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'SiteChatgroupInfo', function($stateParams, SiteChatgroupInfo) {
                    return SiteChatgroupInfo.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'site-chatgroup-info',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('site-chatgroup-info-detail.edit', {
            parent: 'site-chatgroup-info-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-info/site-chatgroup-info-dialog.html',
                    controller: 'SiteChatgroupInfoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupInfo', function(SiteChatgroupInfo) {
                            return SiteChatgroupInfo.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-info.new', {
            parent: 'site-chatgroup-info',
            url: '/new',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-info/site-chatgroup-info-dialog.html',
                    controller: 'SiteChatgroupInfoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                info: null,
                                username: null,
                                submissionDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-info', null, { reload: 'site-chatgroup-info' });
                }, function() {
                    $state.go('site-chatgroup-info');
                });
            }]
        })
        .state('site-chatgroup-info.edit', {
            parent: 'site-chatgroup-info',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-info/site-chatgroup-info-dialog.html',
                    controller: 'SiteChatgroupInfoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteChatgroupInfo', function(SiteChatgroupInfo) {
                            return SiteChatgroupInfo.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-info', null, { reload: 'site-chatgroup-info' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-chatgroup-info.delete', {
            parent: 'site-chatgroup-info',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_WEBUI']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-chatgroup-info/site-chatgroup-info-delete-dialog.html',
                    controller: 'SiteChatgroupInfoDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SiteChatgroupInfo', function(SiteChatgroupInfo) {
                            return SiteChatgroupInfo.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-chatgroup-info', null, { reload: 'site-chatgroup-info' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
