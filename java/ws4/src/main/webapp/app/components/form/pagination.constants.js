(function() {
    'use strict';

    angular
        .module('newmeridianApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
