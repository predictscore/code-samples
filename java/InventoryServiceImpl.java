package org.talend.ipaas.services.inventory.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import com.fasterxml.jackson.core.Version;

import org.apache.cxf.jaxrs.ext.MessageContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.talend.iam.security.userdetails.UserDetails;
import org.talend.ipaas.model.metadata.Action;
import org.talend.ipaas.model.metadata.EnvironmentVersion;
import org.talend.ipaas.model.metadata.action.Origin;
import org.talend.ipaas.rt.cr.exception.CustomResourcesException;
import org.talend.ipaas.rt.tpsvc.iam.model.Subscription;
import org.talend.ipaas.services.TipaasUserName;
import org.talend.ipaas.services.inventory.InventoryException;
import org.talend.ipaas.services.inventory.InventoryServiceAccelerate;
import org.talend.ipaas.services.inventory.Workspace;
import org.talend.ipaas.web.common.bean.WorkspaceForPublish;
import org.talend.ipaas.web.service.ActionController;
import org.talend.ipaas.web.service.WorkspaceController;

public class InventoryServiceImpl extends InventoryServiceBase implements InventoryServiceAccelerate {

    private static final String ACCELERATE_PUBLISH_ACTION_CUSTOM_RESOURCE_ACCOUNT_ID = "000";

    private static final String SEPARATOR_ENVIRONMENT_WORKSPACE = " -> ";

    private static final Version MINIMAL_SUPPORTED_STUDIO_VERSION =
            EnvironmentVersion.getActionStudioVersion("Talend Studio (tipaas_6.2.1.20160704_1411)");

    private static final String UNSUPPORTED_STUDIO_VERSION_MESSAGE =
            "Your Talend Studio version is no longer supported. Please upgrade it.";

    private static final Logger LOG = LoggerFactory.getLogger(InventoryServiceImpl.class);

    @Context
    private MessageContext messageContext;

    @Override
    public Map<String, Boolean> getStatus() throws InventoryException {
        final TipaasUserName user = getLoggedInUser();

        // check availability for services:
        Map<String, Boolean> result = new HashMap<>();

        // account manager
        boolean status = false;
        String accountId = null;
        try {
            accountId = getAccountId(user);
            status = true;
        } catch (Exception e) {
            LOG.error("userId:" + user + " - workspace-service::getStatus for account manager failed", e);
        }
        result.put(STATUS_ACCOUNT_MANAGER, status);

        if (null != accountId) {
            // artifact manager
            status = false;
            try {
                status = 0 < getArtifactManager().listRepositories(accountId).size();
            } catch (Exception e) {
                LOG.error("userId:" + user + " - workspace-service::getStatus for artifact manager failed", e);
            }
            result.put(STATUS_ARTIFACT_MANAGER, status);
        }

        // workspace service
        status = false;
        try {
            getWorkspaceClient(getAuthorizationHeader()).get(WorkspaceController.class).checkAlive();
            status = true;
        } catch (Exception e) {
            LOG.error("userId:" + user + " - workspace-service::getStatus for workspace service failed", e);
        }
        result.put(STATUS_WORKSPACE_SERVICE, status);

        return result;
    }

    /**
     * @deprecated As of release 1.5, replaced by {@link #getAvailableWorkspacesInfo(String)}
     */
    @Deprecated
    @GET
    @Path("/workspaces")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, String> getAvailableWorkspaces(@QueryParam(value = "name") String name,
            @QueryParam(value = "id") String id) throws InventoryException {
        getAccountId(getLoggedInUser());

        throw new InventoryException(UNSUPPORTED_STUDIO_VERSION_MESSAGE);
    }

    /**
     * @deprecated As of release 1.7, replaced by {@link #getAvailableWorkspacesInfo(String, String)}
     */
    @Deprecated
    @GET
    @Path("/workspaces-available")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Workspace> getAvailableWorkspacesInfo17(
            @QueryParam("id") String id, @QueryParam("groupId") String groupId) throws InventoryException {
        final Collection<Workspace> workspaces = getAvailableWorkspacesInfo(id, groupId);
        for (Workspace workspace : workspaces) {
            String environmentName = workspace.getEnvironmentName();
            if (null != environmentName && !environmentName.isEmpty()) {
                workspace.setName(environmentName + SEPARATOR_ENVIRONMENT_WORKSPACE + workspace.getName());
            }
            workspace.setEnvironmentId(null);
            workspace.setEnvironmentName(null);
        }
        return workspaces;
    }

    @Override
    public Collection<Workspace> getAvailableWorkspacesInfo(String id, String groupId) throws InventoryException {
        final TipaasUserName user = getLoggedInUser();
        getAccountId(user);

        Collection<Workspace> availableWorkspaces = new ArrayList<>();
        try {
            Collection<WorkspaceForPublish> workspacesForPublish = getWorkspaceClient(getAuthorizationHeader())
                    .get(WorkspaceController.class).getWorkspacesAvailableForPublish(id, groupId);
            for (WorkspaceForPublish workspaceForPublish : workspacesForPublish) {
                availableWorkspaces.add(buildWorkspace(workspaceForPublish));
            }
        } catch (NotAuthorizedException e) {
            LOG.error("userId:" + user + " - workspace-service::getAvailableWorkspaces(" + id + ") - unauthorized");
            throw new InventoryException("workspace-service: unauthorized");
        } catch (ForbiddenException e) {
            LOG.error("userId:" + user + " - workspace-service::getAvailableWorkspaces(" + id + ") - forbidden", e);
            throw new InventoryException("workspace-service: forbidden - no workspaces available for publish");
        } catch (Exception e) {
            LOG.error("userId:" + user + " - workspace-service::getAvailableWorkspaces(" + id + ") - failed", e);
            throw new InventoryException("workspace-service: cannot get available workspaces");
        }

        if (availableWorkspaces.isEmpty()) {
            LOG.warn("userId:" + user + " - workspace-service::getAvailableWorkspaces(" + id
                    + ") - forbidden (no workspaces available for publish)");
            throw new InventoryException("workspace-service: forbidden - no workspaces available for publish");
        }
        return availableWorkspaces;
    }

    @Deprecated
    @GET
    @Path("/action")
    @Produces(MediaType.TEXT_PLAIN)
    public String getLatestActionVersion15(@QueryParam(value = "name") String name, @QueryParam(value = "id") String id)
            throws InventoryException {
        getAccountId(getLoggedInUser());

        throw new InventoryException(UNSUPPORTED_STUDIO_VERSION_MESSAGE);
    }

    @Override
    public String getLatestActionVersion(String id, String groupId) throws InventoryException {
        final TipaasUserName user = getLoggedInUser();
        final String accountId = getAccountId(user);

        try {
            return getArtifactManager().getLatestActionVersion(accountId, id, groupId);
        } catch (NotAuthorizedException e) {
            LOG.error("userId:" + user + " accountId:" + accountId
                + " - artifact-manager::getLatestActionVersion(id: " + id + ", groupId: " + groupId
                + ") - unauthorized");
            throw e;
        } catch (Exception e) {
            LOG.error("userId:" + user + " accountId:" + accountId
                + " - artifact-manager::getLatestActionVersion(id: " + id + ", groupId: " + groupId
                + ") - failed to get latest action version", e);
            throw new InventoryException("artifact-manager: failed to get latest action version");
        }
    }

    private void checkActionsPublishAllowed(TipaasUserName user, String accountId, Action action)
            throws InventoryException {

        if (Origin.STANDALONE == action.getOrigin()) {
            return; // no subscription option check need
        }

        Subscription subscription = null;
        try {
            subscription = getAccountClient().getSubscription(accountId);
        } catch (Exception e) {
            LOG.error("userId:" + user + " - account-client::getSubscription() - cannot get subscription", e);
            throw new InventoryException("account-client: cannot check account subscription");
        }
        if (null == subscription) {
            LOG.error("userId:" + user + " - subscription is not obtained for account");
            throw new InventoryException("account-client: subscription is not obtained for account");
        }

        if (!subscription.hasActionsAllowedSubscription()) {
            throw new InventoryException("Your account subscription does not allow you to publish Actions."
                    + " Jobs publication is still available.");
        }
    }

    private static void checkStudioCompatibility(Action action) throws InventoryException {
        try {
            final Version version = EnvironmentVersion.getActionStudioVersion(action.getMarketplaceProduct());
            if (MINIMAL_SUPPORTED_STUDIO_VERSION.compareTo(version) > 0) {
                throw new InventoryException(UNSUPPORTED_STUDIO_VERSION_MESSAGE);
            }
        } catch (Exception e) {
            throw new InventoryException(e.getMessage());
        }
    }

    @Override
    public String publishAction(Action action, InputStream actionArtifact,
            InputStream screenshot, InputStream image, InputStream source,
            String workspace, boolean createOrUpdateFlow) throws InventoryException {

        checkStudioCompatibility(action);

        final TipaasUserName user = getLoggedInUser();
        final String accountId = getAccountId(user);

        checkActionsPublishAllowed(user, accountId, action);

        String actionId;
        final String groupId = action.getGroupId();
        if (null != groupId) {
            actionId = action.getId();
        } else {
            actionId = getActionId(action.getId(), action.getName());
            if (null == actionId) {
                actionId = action.getName() + '_' + UUID.randomUUID().toString();
                action.setId(actionId);
            }
        }
        try {
            if (null != source && 0 == source.available()) {
                source = null;
            }
        } catch (IOException e) {
            LOG.error("userId:" + user
                    + " - inventory-service::publishAction(workspaceId: " + workspace + ", actionId: " + actionId
                    + ") - failed to process action sources", e);
            throw new InventoryException("inventory-service: failed to process action sources");
        }

        final boolean standalone = action.getOrigin() == Origin.STANDALONE;
        final String actionVersion = action.getVersion();
        try {
            getArtifactManager().publishAction(accountId, actionId, actionVersion, action.getGroupId(), standalone,
                    actionArtifact, source);
        } catch (Exception e) {
            LOG.error("userId:" + user + " accountId:" + accountId
                    + " - artifact-manager::publishAction(workspaceId: " + workspace + ", actionId: " + actionId
                    + ") - failed to publish action", e);
            throw new InventoryException("artifact-manager: failed to publish action");
        }

        try {
            getWorkspaceClient(getAuthorizationHeader()).get(ActionController.class).publishAction(workspace, action,
                null != screenshot && screenshot.available() > 0 ? screenshot : null, // TIPAAS-931
                null != image && image.available() > 0 ? image : null);
        } catch (NotAuthorizedException e) {
            LOG.error("userId:" + user + " accountId:" + accountId
                    + " - workspace-service::publishAction(workspaceId: " + workspace + ", actionId: " + actionId
                    + ") - unauthorized");
            throw new InventoryException("workspace-service: unauthorized");
        } catch (Exception e) {
            LOG.error("userId:" + user + " accountId:" + accountId
                    + " - workspace-service::publishAction(workspaceId: " + workspace + ", action: " + actionId
                    + ") - failed to create action", e);
            throw new InventoryException("workspace-service: failed to create action");
        }

        final String response;
        if (standalone && createOrUpdateFlow) {
            response = createOrUpdateJobActionFlow(user, accountId, workspace, actionId, actionVersion, groupId);
        } else {
            response = actionId;
        }

        LOG.info("userId:" + user + " accountId:" + accountId
                + " - User Published from Studio, action/job name: " + action.getName());
        return response;
    }

    @Override
    public String getAcceleratePublishUploadUrl(String acceleratePublishId) throws InventoryException {
        final TipaasUserName user = getLoggedInUser();
        final String accountId = getAccountId(user);

        try {
            return getCustomResourcesAdmin().addCustomResource(ACCELERATE_PUBLISH_ACTION_CUSTOM_RESOURCE_ACCOUNT_ID,
                    acceleratePublishId).toString();
        } catch (CustomResourcesException e) {
            LOG.error("userId:" + user + " accountId:" + accountId
                    + " - accelerate-publish::getAcceleratePublishUploadUrl(acceleratePublishId: " + acceleratePublishId
                    + ") - failed to obtain upload URL", e);
            throw new InventoryException("accelerate-publish: failed to obtain upload URL");
        }
    }

    @Override
    public String acceleratePublishAction(String acceleratePublishId, Action action,
            InputStream screenshot, InputStream image, InputStream source,
            String workspaceId, boolean createOrUpdateFlow) throws InventoryException {

        final TipaasUserName user = getLoggedInUser();
        final String accountId = getAccountId(user);

        final URL acceleratePublishDownloadUrl;
        try {
            acceleratePublishDownloadUrl = getCustomResources().getCustomResourceContent(
                    ACCELERATE_PUBLISH_ACTION_CUSTOM_RESOURCE_ACCOUNT_ID, acceleratePublishId);
        } catch (CustomResourcesException e) {
            LOG.error("userId:" + user + " accountId:" + accountId
                    + " - accelerate-publish::get-accelerate-publish-download-url(acceleratePublishId: "
                    + acceleratePublishId + ") - failed to obtain download URL", e);
            throw new InventoryException("accelerate-publish: failed to obtain download URL");
        }

        try (InputStream input = acceleratePublishDownloadUrl.openStream()) {
            // checkActionsPublishAllowed(user, accountId, action);
            return publishAction(action, input, screenshot, image, source, workspaceId, createOrUpdateFlow);
        } catch (IOException e) {
            LOG.error("userId:" + user + " accountId:" + accountId
                    + " - accelerate-publish::accelerate-publish-download-artifact(acceleratePublishId: "
                    + acceleratePublishId + ") - failed to download artifact", e);
            throw new InventoryException("accelerate-publish: failed to download artifact");
        } finally {
            if (!skipAcceleratePublishPostCleanup(user.getAccountName(), messageContext)) {
                try {
                    getCustomResourcesAdmin().deleteCustomResource(ACCELERATE_PUBLISH_ACTION_CUSTOM_RESOURCE_ACCOUNT_ID,
                            acceleratePublishId);
                } catch (CustomResourcesException e) {
                    LOG.warn("userId:" + user + " accountId:" + accountId
                            + " - accelerate-publish::finalize - failed cleanup accelerated artifact", e);
                }
            }
        }
    }

    private String createOrUpdateJobActionFlow(TipaasUserName user, String accountId, String workspace, String actionId,
            String actionVersion, String groupId) throws InventoryException {
        try {
            return getWorkspaceClient(getAuthorizationHeader()).get(WorkspaceController.class)
                    .createOrUpdateJobActionFlow(workspace, actionId, actionVersion, groupId);
        } catch (NotAuthorizedException e) {
            LOG.error("userId:" + user + " accountId:" + accountId
                    + " - workspace-service::createOrUpdateJobActionFlow(workspaceId: " + workspace + ", actionId: "
                    + actionId + ") - unauthorized");
            throw new InventoryException("workspace-service: unauthorized");
        } catch (Exception e) {
            LOG.error("userId:" + user + " accountId:" + accountId
                + " - workspace-service::createOrUpdateJobActionFlow(workspaceId: " + workspace + ", action: "
                + actionId + ") - failed to create single job flow", e);
            throw new InventoryException("workspace-service: failed to create/update single job flow");
        }
    }

    private static String getActionId(String id, String name) {
        if (null != id && id.startsWith(name + '_')) {
            return id;
        }
        return null;
    }

    private static Workspace buildWorkspace(WorkspaceForPublish workspaceForPublish) {
        Workspace workspace = new Workspace();
        workspace.setId(workspaceForPublish.getId());
        workspace.setName(workspaceForPublish.getName());
        workspace.setOwner(workspaceForPublish.getOwner());
        workspace.setOwnerFullName(workspaceForPublish.getOwnerFullName());
        workspace.setType(workspaceForPublish.getType().name());
        if (workspaceForPublish.getActionDetails() != null) {
            workspace.setLastPublishedVersion(workspaceForPublish.getActionDetails().getVersion());
        }
        workspace.setEnvironmentId(workspaceForPublish.getEnvironmentId());
        workspace.setEnvironmentName(workspaceForPublish.getEnvironmentName());
        return workspace;
    }

    private TipaasUserName getLoggedInUser() throws InventoryException {
        if (null != messageContext) {
            SecurityContext securityContext = messageContext.getSecurityContext();
            if (null != securityContext) {
                Principal principal = securityContext.getUserPrincipal();
                if (null != principal) {

                    final String loginname;
                    if (principal instanceof OAuth2Authentication) {
                        loginname = getOAuth2LoggedInUser((OAuth2Authentication) principal);
                    } else {
                        loginname = principal.getName();
                    }

                    if (null != loginname) {
                        try {
                            return new TipaasUserName(loginname);
                        } catch (IllegalArgumentException e) {
                            throw new InventoryException("inventory-service: " + e.getMessage());
                        }
                    }
                }
            }
        }
        throw new SecurityException("inventory-service: cannot retrive authentication info");
    }

    private static String getOAuth2LoggedInUser(OAuth2Authentication oauth2Authentication) {
        Object principal = oauth2Authentication.getPrincipal();
        if (null != principal) {
            if (principal instanceof UserDetails) {
                return ((UserDetails) principal).getUsername();
            } else if (principal instanceof String) {
                return (String) principal;
            }
        }

        throw new SecurityException("inventory-service: unknown principal type " + principal.getClass().getName());
    }

    private String getAccountId(final TipaasUserName user) throws InventoryException {
        final String id;
        try {
            id = getAccountClient().getAccountId(user.getAccountName());
        } catch (Exception e) {
            LOG.error("userId:" + user + " - account-client::findAccounts() - cannot get account", e);
            throw new InventoryException("account-client: cannot get account");
        }
        if (null == id) {
            LOG.error("userId:" + user + " - account not found or account is not active");
            throw new InventoryException("account-client: account not found or account is not active");
        }
        return id;
    }

    private String getAuthorizationHeader() {
        //return messageContext.getHttpHeaders().getRequestHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth instanceof OAuth2Authentication) {
            OAuth2Authentication o2Auth = (OAuth2Authentication) auth;
            if (o2Auth.getDetails() instanceof OAuth2AuthenticationDetails) {
                OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) o2Auth.getDetails();
                return "Bearer " + details.getTokenValue();
            }
        }
        throw new SecurityException("inventory-service: cannot retrive o2Auth authentication token");
    }

}
