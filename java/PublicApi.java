package org.talend.ipaas.api.execution;

import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.talend.ipaas.api.execution.exception.IpaasWebApiException;
import org.talend.ipaas.api.execution.model.ErrorResponse;
import org.talend.ipaas.api.execution.model.ExecutableDetails;
import org.talend.ipaas.api.execution.model.ExecutableInfo;
import org.talend.ipaas.api.execution.model.ExecutableTask;
import org.talend.ipaas.api.execution.model.Execution;
import org.talend.ipaas.api.execution.model.ExecutionStatus;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;
import io.swagger.annotations.BasicAuthDefinition;

@CrossOriginResourceSharing(
        allowAllOrigins = true,
        allowCredentials = true,
        maxAge = 60, // 1 minute
//        allowHeaders = {
//           "Accept", "Authorization", "Cache-Control", "Content-Type","Expires", "Location", "Origin", "Pragma"
//        },
        exposeHeaders = {
           "Location"
        }
)

@Path("/")
@Api(value = "/", authorizations =
    @Authorization(
            scopes = @AuthorizationScope(scope = "Basic Authentication", description = ""),
            value = "Basic Authentication")
        )
@BasicAuthDefinition(key = "Basic Authentication")
@SuppressWarnings("CPD-START")
public interface PublicApi {

    @GET
    @Path("/executables")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get Jobs available", notes = "Get Jobs available", tags = "executables",
            response = ExecutableInfo.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful response",
                    response = ExecutableInfo.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request",
                    response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized",
                    response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "Forbidden - no permissions to access resource",
                    response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "Not found - resource not found",
                    response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Server error - something went wrong on server side",
                    response = ErrorResponse.class)
        }
    )
    Collection<ExecutableInfo> getExecutablesAvailable(
            @ApiParam(value = "search query (FIQL format), e.g. "
                    + "\"name==dataprep_*\", "
                    + "\"workspace.environment.default==true\", "
                    + "\"name==test*;workspace.type==personal;workspace.owner==admin\"", required = false)
            @QueryParam("_s")
                String searchQuery
        ) throws IpaasWebApiException;

    @GET
    @Path("/executables/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get Job details", notes = "Get Job details", tags = "executables",
            response = ExecutableDetails.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful response",
                    response = ExecutableDetails.class),
            @ApiResponse(code = 400, message = "Bad Request",
                    response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized",
                    response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "Forbidden - no permissions to access resource",
                    response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "Not found - resource not found",
                    response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Server error - something went wrong on server side",
                    response = ErrorResponse.class)
        }
    )
    ExecutableDetails getExecutableDetails(
            @ApiParam(value = "executable ID", required = true)
            @PathParam("id")
                String executableId
        ) throws IpaasWebApiException;

    @POST
    @Path("/executions")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Execute Job", notes = "Execute Job", tags = "executions",
            response = Execution.class, code = 201)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Execution started",
                    response = Execution.class),
            @ApiResponse(code = 400, message = "Bad Request",
                    response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized",
                    response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "Forbidden - no permissions to access resource",
                    response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "Not found - resource not found",
                    response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Server error - something went wrong on server side",
                    response = ErrorResponse.class),
            @ApiResponse(code = 501, message = "Job cannot be deployed because it is not configured",
                    response = ErrorResponse.class)
        }
    )
    Execution execute(
            @ApiParam(value = "Executable identifier", required = true)
                ExecutableTask executable
        ) throws IpaasWebApiException;

    @GET
    @Path("/executions/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get Job execution status", notes = "Get Job execution status", tags = "executions",
            response = ExecutionStatus.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful response",
                    response = ExecutionStatus.class),
            @ApiResponse(code = 400, message = "Bad Request",
                    response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized",
                    response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "Forbidden - no permissions to access resource",
                    response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "Not found - resource not found",
                    response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Server error - something went wrong on server side",
                    response = ErrorResponse.class)
        }
    )
    ExecutionStatus getExecutionStatus(
            @ApiParam(value = "execution ID", required = true)
            @PathParam("id")
                String executionId
        ) throws IpaasWebApiException;

    @DELETE
    @Path("/executions/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(value = "Terminate Job execution", notes = "Terminate Job execution", tags = "executions",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Execution already finished",
                    response = String.class),
            @ApiResponse(code = 204, message = "Execution stopped succesfully",
                    response = String.class),
            @ApiResponse(code = 400, message = "Bad Request",
                    response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "Unauthorized",
                    response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "Forbidden - no permissions to access resource",
                    response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "Not found - resource not found",
                    response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Server error - something went wrong on server side",
                    response = ErrorResponse.class)
        }
    )
    String stopExecution(
            @ApiParam(value = "execution ID", required = true)
            @PathParam("id")
                String executionId
        ) throws IpaasWebApiException;

}
