package org.talend.ipaas.api.execution.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.talend.iam.security.password.EnableBasicAuthToOAuth2Adapter;
import org.talend.ipaas.IpaasRoles;

@Configuration
@EnableWebSecurity
@EnableBasicAuthToOAuth2Adapter
@EnableGlobalMethodSecurity(prePostEnabled = true, proxyTargetClass = true)
@PropertySource("classpath:/ipaas_api.properties")
public class SecurityConfiguration extends ResourceServerConfigurerAdapter {

    @Value("${security.oauth2.client.client_id}")
    private String clientId;

    @Value("${security.oauth2.client.client_secret}")
    private String clientSecret;

    @Value("${security.oauth2.client.access_token_uri}")
    private String accessTokenUri;

    @Value("${security.oauth2.resource.token_info_uri}")
    private String tokenInfoUri;

    @Bean
    @Primary
    public ResourceOwnerPasswordResourceDetails resourceOwnerPasswordResourceDetails() {
        ResourceOwnerPasswordResourceDetails answer = new ResourceOwnerPasswordResourceDetails();

        answer.setClientId(clientId);
        answer.setClientSecret(clientSecret);
        answer.setAccessTokenUri(accessTokenUri);

        return answer;
    }

    @Bean
    @Primary
    public ResourceServerProperties resourceServerProperties() {
        ResourceServerProperties answer = new ResourceServerProperties(clientId, clientSecret);

        answer.setTokenInfoUri(tokenInfoUri);

        return answer;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                .disable()

                .authorizeRequests()

                .antMatchers("/swagger-ui.html", "/swagger-resources/**", "/v2/api-docs") // TODO: check ? all contexts
                .permitAll()

                .antMatchers("/**")
                .hasAuthority(IpaasRoles.AUTHENTICATED);
    }

}
