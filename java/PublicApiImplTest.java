package org.talend.ipaas.api.execution.impl;

import org.junit.Assert;
import org.junit.Test;
import org.talend.ipaas.api.execution.exception.IpaasWebApiException;

public class PublicApiImplTest {

    private PublicApiImpl publicApi = new PublicApiImpl() {
        protected void assignRequestId() { // TODO: move to request pre-filter
        }
    };
	
    @Test
    public void testWrongArgumentDeployFlow() {
        try {
            publicApi.execute(null);
            Assert.fail("IpaasApiWrongArgumentException argument exception should me thrown");
        } catch (IpaasWebApiException e) {
            Assert.assertSame(IllegalArgumentException.class, e.getCause().getClass());
        }
        try {
            publicApi.execute(null);
            Assert.fail("IpaasApiWrongArgumentException argument exception should me thrown");
        } catch (IpaasWebApiException e) {
            Assert.assertSame(IllegalArgumentException.class, e.getCause().getClass());
        }
    }

    @Test
    public void testWrongArgumentGetExecutionStatus() {
        try {
            publicApi.getExecutionStatus(null);
            Assert.fail("IpaasApiWrongArgumentException argument exception should me thrown");
        } catch (IpaasWebApiException e) {
            Assert.assertSame(IllegalArgumentException.class, e.getCause().getClass());
        }
        try {
            publicApi.getExecutionStatus("");
            Assert.fail("IpaasApiWrongArgumentException argument exception should me thrown");
        } catch (IpaasWebApiException e) {
            Assert.assertSame(IllegalArgumentException.class, e.getCause().getClass());
        }
    }

    @Test
    public void testWrongArgumentStopExecution() {
        try {
            publicApi.stopExecution(null);
            Assert.fail("IpaasApiWrongArgumentException argument exception should me thrown");
        } catch (IpaasWebApiException e) {
            Assert.assertSame(IllegalArgumentException.class, e.getCause().getClass());
        }
        try {
            publicApi.stopExecution("");
            Assert.fail("IpaasApiWrongArgumentException argument exception should me thrown");
        } catch (IpaasWebApiException e) {
            Assert.assertSame(IllegalArgumentException.class, e.getCause().getClass());
        }
    }
}
