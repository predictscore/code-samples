exports.config =
  # See http://brunch.readthedocs.org/en/latest/config.html for documentation.
  watcher:
    usePolling: true
  # workers:
  #   enabled: false
  plugins:
    uglify:
      mangle: true
      compress:
        global_defs:
          DEBUG: false
    sass:
      mode: 'ruby'
    copyfilemon:
      'javascript/tinymce': ['bower_components/tinymce'],
      'css/': [
        'bower_components/select2/select2.png',
        'bower_components/select2/select2x2.png',
        'bower_components/select2/select2-spinner.gif'
      ],
      'css/images': [
        'bower_components/bxslider-4/dist/images',
        'bower_components/leaflet-0.7.5/dist/images',
        'bower_components/jquery-ui/themes/smoothness/images'
      ],
      'fonts/': ['bower_components/font-awesome/fonts'],
      'fonts/open-sans': ['bower_components/google-open-sans/open-sans'],
      'open-sans-condensed': ['bower_components/google-open-sans/open-sans-condensed']
    javascript:
      validate: true
    jshint:
      pattern: /^app\/.*\.js$/
      options:
        bitwise: false
        curly: false
      globals:
        jQuery: true
      warnOnly: true
    angular_templates:
      path_transform: (path) -> path.replace(/^app/, '/assets')
  modules:
    definition: false
    wrapper: (path, data) ->
      if /^app[\\/].*\.js/.test(path)
        """(function(module){'use strict'\n#{data}})();\n\n"""
      else data
  paths:
    public: '../assets'
  files:
    javascripts:
      joinTo:
        'javascript/app.js': /^app[\\/]/
        'javascript/vendor.js': /^bower_components[\\/]/
      order:
        before: [
          /[\\/]module\.js/
        ]
        after: [
          /[\\/]jquery\.jvectormap\.min\.js/,
          /[\\/]jquery-jvectormap-world-mill-en\.js/
        ]

    stylesheets:
      joinTo:
        'css/app.css': /^app[\\/]/
        'css/vendor.css': /^bower_components[\\/]/
      order:
        before: [
          'app/reset.css',
          'bower_components/bootstrap/dist/css/bootstrap.css',
          'app/global.less'
        ]
                                                    
    templates:
      joinTo:
        'javascript/templates.js': /^app[\\/]/

  conventions:
    ignored: [
      'bower_components/google-open-sans/'
    ]
    assets: (path) ->
      if /[\\/]$/.test path
        return true
      if /^app[\\/].*\.html/.test path
        return false
      if /^app[\\/].*\.json/.test path
        return true
      if /^app[\\/].*\.txt/.test path
        return true
      if /^app[\\/].*\.png/.test path
        return true
      if /^app[\\/].*\.jpg/.test path
        return true
      if /^app[\\/].*\.svg/.test path
        return true
      if /^app[\\/].*\.ico/.test path
        return true
      if /^app[\\/].*\.gif/.test path
        return true
      return false
