var gulp = require('gulp');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var mainBowerFiles = require('main-bower-files');
var templateCache = require('gulp-angular-templatecache');
var ngAnnotate = require('gulp-ng-annotate');
var wrapJS = require("gulp-wrap-js");
var sourcemaps = require('gulp-sourcemaps');
var merge = require('merge-stream');
var order = require('gulp-order');
var print = require('gulp-print');
var colors = require('colors');
var cache = require('gulp-cached');
var remember = require('gulp-remember');
var revHash = require('gulp-rev-hash');
var del = require('del');

var reporter = {
    reporter: function (result, config, options) {
        result.forEach(function(err) {
            console.log(colors.green(err.file) + ':' + err.error.line + ':' + err.error.character + ' ' + colors.gray(err.error.reason));
        });
    }
};

function styles() {
    return gulp.src('app/**/*.+(scss|css)')
        // .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: ['public/css']
        }))
        .pipe(concat('app.css'))
        .pipe(autoprefixer('last 2 versions'))
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest('../assets/css/'));
}


function vendor() {
    function copy(from, to) {
        return gulp.src(from)
            .pipe(gulp.dest(to));
    }
    
    return merge([
        gulp.src(mainBowerFiles('**/*.js'))
            .pipe(order([
                'pdfmake.js',
                'jquery.js',
                'jquery-ui.js',
                'jquery.flot.js',
                'angular.js',
                'moment.js',
                'pace.js',
                'leaflet-src.js',
                '*.js'
            ]))
            // .pipe(print())
            .pipe(sourcemaps.init())
                .pipe(concat('vendor.js'))  
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest("../assets/javascript/")),
        copy('bower_components/tinymce/**', '../assets/javascript/tinymce'),
        copy('bower_components/select2/*.+(png|gif)', '../assets/css'),
        copy([
          'bower_components/bxslider-4/dist/images/*',
          'bower_components/leaflet-0.7.5/dist/images/*',
          'bower_components/jquery-ui/themes/smoothness/images/*'
        ], '../assets/css/images'),
        copy('bower_components/font-awesome/fonts/*', '../assets/fonts/'),
        copy('bower_components/google-open-sans/open-sans/*', '../assets/fonts/open-sans'),
        copy('bower_components/google-open-sans/open-sans-condensed/*', '../assets/fonts/open-sans-condensed'),
        gulp.src(mainBowerFiles('**/*.+(css|scss)'))
            .pipe(sass())
            .pipe(concat('vendor.css'))
            .pipe(gulp.dest("../assets/css/"))
    ]);
}

function scripts() {
    return gulp.src('app/**/*.js')
        .pipe(order([
            '**/module.js',
            '**/*'
        ]))
        .pipe(cache('scripts'))
        // .pipe(print())
        .pipe(jshint())
        .pipe(jshint.reporter(reporter))
        .pipe(wrapJS("(function(){'use strict';\n%=body%\n})();"))
        .pipe(ngAnnotate())
        .pipe(remember('scripts'))
        .pipe(sourcemaps.init())
            .pipe(concat('app.js'))
        .pipe(sourcemaps.write('./'))
        
        .pipe(gulp.dest('../assets/javascript/'));
        // .pipe(rename({
        //     suffix: '.min'
        // }))
        // .pipe(uglify())
        // .pipe(gulp.dest('../assets/javascript/'));
}

function staticData() {
    return gulp.src('app/**/*.+(jpg|png|svg|ico|gif|txt|json)')
        .pipe(gulp.dest('../assets/'));
}

function templates() {
    return gulp.src('app/**/*.html')
        .pipe(templateCache({
            filename: {
                path: '/dev/null'
            },
            module: 'templates',
            standalone: true,
            transformUrl: function(url) {
            	return '/assets/' + url;
            }
        }))
        .pipe(concat('templates.js'))
        .pipe(gulp.dest('../assets/javascript/'));
}

function clear() {
    return del([
        '../assets',
        '../index.html'
    ], {
        force: true
    });
}

gulp.task('clear', clear);

function index() {
    return gulp.src('index.template.html')
        .pipe(revHash({
            assetsDir: '../'
        }))
        .pipe(rename('index.html'))
        .pipe(gulp.dest('../'));
}

function indexV2() {
    return gulp.src('index-v2.template.html')
        .pipe(revHash({
            assetsDir: '../'
        }))
        .pipe(rename('index.html'))
        .pipe(gulp.dest('../'));
}

gulp.task('build', gulp.parallel(staticData, vendor, templates, styles, scripts));

function watchStaticData() {
    return gulp.watch('app/**/*.+(jpg|png|svg|ico|gif|txt|json)', staticData);
}

function watchStyles() {
    return gulp.watch("app/**/*.scss", gulp.series(styles, index));
}

function watchTemplates() {
    return gulp.watch("app/**/*.html", gulp.series(templates, index));
}

function watchScripts() {
    return gulp.watch("app/**/*.js", gulp.series(scripts, index));
}

function watchStylesV2() {
    return gulp.watch("app/**/*.scss", gulp.series(styles, indexV2));
}

function watchTemplatesV2() {
    return gulp.watch("app/**/*.html", gulp.series(templates, indexV2));
}

function watchScriptsV2() {
    return gulp.watch("app/**/*.js", gulp.series(scripts, indexV2));
}

gulp.task('buildV1', gulp.series(
    clear,
    gulp.parallel(scripts, staticData, vendor, templates, styles),
    index
));

gulp.task('build', gulp.series(
    clear,
    gulp.parallel(scripts, staticData, vendor, templates, styles),
    indexV2
));

gulp.task('watchV1', gulp.series(
    clear,
    gulp.parallel(scripts, staticData, vendor, templates, styles),
    index,
    gulp.parallel(watchStaticData, watchStyles, watchTemplates, watchScripts)
));

gulp.task('watch', gulp.series(
    clear,
    gulp.parallel(scripts, staticData, vendor, templates, styles),
    indexV2,
    gulp.parallel(watchStaticData, watchStylesV2, watchTemplatesV2, watchScriptsV2)
));

gulp.task('static', staticData);
gulp.task('templates', templates);