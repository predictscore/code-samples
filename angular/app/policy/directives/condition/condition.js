var m = angular.module('policy');

m.directive('condition', function(path, policyPropertyParser, policyPropertyIsOneToMany, doubleDataReplacer) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('policy').directive('condition').template(),
        scope: {
            model: '=condition',
            options: '=',
            propertyTypes: '=',
            remove: '&'
        },
        link: function ($scope, element, attrs) {
            $scope.invalid = {};
            $scope.internal = {
                operatorLabel: '',
                operator: {},
                options: [],
                data: $scope.model.data
            };
            
            if(_($scope.model.data).isArray()) {
                $scope.internal.data = $.extend(true, [], $scope.model.data);
            } else if(_($scope.model.data).isObject()) {
                $scope.internal.data = $.extend(true, {}, $scope.model.data);
            }

            $scope.applyAggregateVariants = [{
                id: 'all',
                label: 'ALL'
            }, {
                id: 'any',
                label: 'ANY'
            }, {
                id: 'none',
                label: 'NONE'
            }];

            $scope.timeshiftOptions = [{
                id: 'minutes',
                label: 'Minutes'
            }, {
                id: 'hours',
                label: 'Hours'
            }, {
                id: 'days',
                label: 'Days'
            }, {
                id: 'months',
                label: 'Months'
            }, {
                id: 'years',
                label: 'Years'
            }];

            $scope.$watch('model', function(model) {
                $scope.invalid.data = false;
                $scope.oneToMany = undefined;
                if($scope.model.property) {
                    $scope.oneToMany = policyPropertyIsOneToMany($scope.options.properties, $scope.model.property);
                    if($scope.oneToMany) {
                        if($scope.model.not) {
                            delete $scope.model.not;
                            $scope.model.aggregatedOperator = 'all';
                        }
                    } else {
                        if($scope.model.aggregatedOperator) {
                            delete $scope.model.aggregatedOperator;
                            $scope.model.not = false;
                        }
                    }
                }
                if($scope.internal.operator && $scope.internal.operator.type === 'time-shift') {
                    var splitted = model.data.split(' ');
                    $scope.internal.data = {
                        first: parseInt(splitted[0]) || '',
                        second: splitted[1] || ''
                    };
                }
                model.check = function() {
                    $scope.invalid = {};
                    if(_(model.property).isUndefined()) {
                        $scope.invalid.property = true;
                        return false;
                    }
                    if(model.operator == 'not_null' || model.operator == 'is_null') {
                        return true;
                    }
                    if (_(model.data).isUndefined() ||
                        model.data === '' || 
                        (_(model.data).isArray() && !model.data.length) || 
                        (_(model.data).isObject() && (model.data.first === '' || model.data.second === '')) ||
                        ((model.operator === 'from' || model.operator === 'older_than') && !/\d+ [a-z]+/.test(model.data))
                    ) {
                        $scope.invalid.data = true;
                        return false;
                    }
                    return true;
                };
            }, true);

            $scope.getPropertyTypes = function(property) {
                if(_(property).isUndefined() || _(property.nodeId).isUndefined()) {
                    return [];
                }
                var propertyTypeId = $scope.options.properties[property.nodeId].type;
                return $scope.propertyTypes[propertyTypeId];
            };

            var cachedOperatorVariants = null;
            $scope.getOperatorVariants = function(property) {
                var variants = _($scope.getPropertyTypes(property)).map(function(o) {
                    return {
                        id: o.label,
                        label: o.label
                    };
                });
                if(_(variants).isEqual(angular.copy(cachedOperatorVariants))) {
                    return cachedOperatorVariants;
                }
                cachedOperatorVariants = variants;
                return cachedOperatorVariants;
            };

            $scope.isOperatorHidden = function(property) {
                var types = $scope.getPropertyTypes(property);
                return types.length == 1 && _(types).first().hidden;
            };

            $scope.getVariants = function(propertyId) {
                if(_(propertyId).isUndefined() || _(propertyId.nodeId).isUndefined()) {
                    return [];
                }
                var property = $scope.options.properties[propertyId.nodeId];
                if(property.type === 'boolean') {
                    if(!property.booleanVariants) {
                        property.booleanVariants = _.clone(property.variants);
                        property.booleanVariants.push({
                            id: null,
                            label: 'Is empty'
                        });
                    }
                    return property.booleanVariants;
                }
                return property.variants;
            };

            $scope.getOperatorByLabel = function(label) {
                var operators = $scope.getPropertyTypes($scope.internal.property);
                return _(operators).find(function(operator) {
                    return operator.label === label;
                });
            };

            $scope.getOperatorById = function(id) {
                var result = _($scope.getPropertyTypes($scope.internal.property)).find(function(property) {
                    return property.id == id;
                });
                if(_(result).isUndefined()) {
                    result = _($scope.getPropertyTypes($scope.internal.property)).first();
                }
                return result;
            };
            
            $scope.$watch('internal.data', function() {
                if ($scope.internal.operator.type === 'time-shift') {
                    $scope.model.data = $scope.internal.data.first + ' ' + $scope.internal.data.second;
                } else {
                    $scope.model.data = doubleDataReplacer($scope.internal.data, {
                        'double': 'number',
                        'double-date': 'date'
                    }[$scope.internal.operator.type]);
                }
            }, true);

            $scope.$watch('internal.property', function(property) {
                $scope.invalid.property = false;
                if(!_(property).isUndefined()) {
                    var types = $scope.getPropertyTypes(property);
                    if(_(_(types).find(function(type) {
                        return type.label == $scope.internal.operatorLabel;
                    })).isUndefined()) {
                        $scope.internal.operatorLabel = $scope.getPropertyTypes(property)[0].label;
                    }
                    $scope.model.property = $scope.internal.property.fullId;
                }
            }, true);

            function updateOptions(prevOptions) {
                if (_($scope.internal.operator.options).isArray()) {
                    _($scope.internal.operator.options).each(function (option) {
                        if (option.type === 'flag') {
                            var prevOption = _(prevOptions).find(function(o) {
                                return o.id == option.id;
                            });
                            var state = option.defaultState;
                            if(!_(prevOption).isUndefined()) {
                                state = prevOption.state;
                            }
                            $scope.internal.options.push({
                                id: option.id,
                                text: option.label,
                                type: option.type,
                                state: state
                            });
                        }
                    });
                }
                var state = false;
                if(!_(prevOptions).isUndefined()) {
                    var prevOption = _(prevOptions).find(function(o) {
                        return o.id == 'data_is_property';
                    });
                    if(!_(prevOption).isUndefined()) {
                        state = prevOption.state;
                    }
                }
                $scope.internal.options.push({
                    id: 'data_is_property',
                    text: 'Data is property',
                    type: 'flag',
                    state: state
                });
            }

            $scope.setNot = function($event, value) {
                $event.preventDefault();
                $scope.model.not = value;
            };

            $scope.setOperator = function($event, value) {
                $event.preventDefault();
                $scope.internal.operatorLabel = value;
            };

            $scope.$watch('internal.property.fullId + ":" + internal.operatorLabel', function(newVal, oldVal) {
                if(newVal != oldVal && $scope.model.generatedFrom) {
                    delete $scope.model.generatedFrom;
                }
                var label = $scope.internal.operatorLabel;
                var previousType;
                if(!_($scope.internal.operator).isUndefined()) {
                    previousType = $scope.internal.operator.type;
                }
                $scope.internal.operator = $scope.getOperatorByLabel(label);
                if(_($scope.internal.operator).isUndefined()) {
                    return;
                }
                $scope.model.operator = $scope.internal.operator.id;
                var prevOptions = $scope.internal.options;
                $scope.internal.options = [];
                if(!_($scope.internal.operator).isUndefined()) {
                    var type = $scope.internal.operator.type;
                    if(type != previousType) {
                        if (type === 'selector') {
                            if (!_($scope.internal.data).isUndefined()) {
                                delete $scope.internal.data;
                            }
                        } else if (type === 'single' || type === 'single-date') {
                            $scope.internal.data = '';
                        } else if (type === 'double' || type === 'double-date') {
                            $scope.internal.data = {
                                first: '',
                                second: ''
                            };
                        } else if (type === 'time-shift') {
                            $scope.internal.data = {
                                first: '',
                                second: ''
                            };
                            $scope.model.data = '';
                        } else if (type === 'list') {
                            $scope.internal.data = [];
                        }
                    }
                    updateOptions(prevOptions);
                }
            });

            $scope.$watch('internal.options', function(options) {
                $scope.model.options = {};
                _(options).each(function(option) {
                    $scope.model.options[option.id] = option.state;
                });
            }, true);

            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            if(!_($scope.model.operator).isUndefined()) {
                $scope.internal.property = policyPropertyParser($scope.model.property);
                $scope.internal.operator = $scope.getOperatorById($scope.model.operator);
                $scope.internal.operatorLabel = $scope.internal.operator.label;
                updateOptions(_($scope.model.options).map(function(val, key) {
                    return {
                        id: key,
                        state: val
                    };
                }));
            }

            if(_($scope.model.not).isUndefined()) {
                $scope.model.not = false;
            }
        }
    };
});