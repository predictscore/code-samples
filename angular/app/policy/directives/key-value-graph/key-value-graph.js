var m = angular.module('policy');

m.directive('keyValueGraph', function(path, $timeout, $q, modals, widgetLogic) {
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: path('policy').directive('key-value-graph').template(),
        scope: {
            model: '=keyValueGraph',
            options: '=',
            onChange: '&?'
        },
        link: function($scope, element) {
            widgetLogic($scope, $scope.model.logic);

            $scope.hideLegend = !$scope.options.editor && !$scope.options.hideControls;
            function makeTickToId(data) {
                var lastId = 0;
                var tickToId = {};
                _(data).each(function(d) {
                    tickToId[d.key] = tickToId[d.key] || lastId++;
                });
                return tickToId;
            }
            function makeXAxis(tickToId) {
                return {
                    ticks: _(tickToId).chain().map(function(tick, id) {
                        if(id.length > 24) {
                            id = id.substring(0, 21) + '...';
                        } 
                        return [tick, id];
                    }).sortBy(function(d) {
                        return d[0];
                    }).value(),
                    labelHeight: 50
                };
            }
            function applyTicksToData(data, tickToId) {
                return _(data).map(function(d) {
                    return [tickToId[d.key], d.value];
                });
            }
            $scope.graphTypes = [{
                id: 'bar',
                dataConverter: function(data) {
                    var tickToId = makeTickToId(data);
                    var idToTick = _(tickToId).invert();
                    return {
                        flotOptions: {
                            xaxis: makeXAxis(tickToId),
                            yaxis: {
                                tickFormatter: function(val) {
                                    return $scope.dataFormatter(val);
                                }
                            },
                            series: {
                                bars: {
                                    show: true, 
                                    barWidth: 0.6,
                                    align: 'center'
                                }
                            },
                            grid: {
                                hoverable: true
                            },
                            tooltip: {
                                show: true,
                                content: function(label, xval, yval, flotItem) {
                                    return '%s: %y';
                                }
                            },
                            legend: {
                                show: true
                            }
                        },
                        data: _(applyTicksToData(data, tickToId)).map(function(d) {
                            return {
                                label: idToTick[d[0]],
                                data: [d]
                            };
                        })
                    };
                }
            }, {
                id: 'hbar',
                dataConverter: function(data) {
                    var tickToId = makeTickToId(_.clone(data).reverse());
                    var idToTick = _(tickToId).invert();
                    return {
                        flotOptions: {
                            yaxis: makeXAxis(tickToId),
                            xaxis: {
                                tickFormatter: function(val) {
                                    return $scope.dataFormatter(val);
                                }
                            },
                            series: {
                                bars: {
                                    show: true, 
                                    horizontal: true,
                                    barWidth: 0.6,
                                    align: 'center'
                                }
                            },
                            grid: {
                                hoverable: true
                            },
                            tooltip: {
                                show: true,
                                content: '%s: %x'
                            },
                            legend: {
                                show: true
                            }
                        },
                        data: _(applyTicksToData(data, tickToId)).map(function(d) {
                            return {
                                label: idToTick[d[0]],
                                data: [[d[1], d[0]]]
                            };
                        })
                    };
                }
            }, {
                id: 'line',
                dataConverter: function(data) {
                    var tickToId = makeTickToId(data);
                    var idToTick = _(tickToId).invert();
                    return {
                        flotOptions: {
                            xaxis: makeXAxis(tickToId),
                            yaxis: {
                                tickFormatter: function(val) {
                                    return $scope.dataFormatter(val);
                                }
                            },
                            series: {
                                lines: {
                                    show: true
                                }
                            },
                            grid: {
                                hoverable: true
                            },
                            tooltip: {
                                show: true,
                                content: function(label, xval, yval, flotItem) {
                                    return idToTick[xval] + ': %y';
                                }
                            }
                        },
                        data: [applyTicksToData(data, tickToId)]
                    };
                }
            }, {
                id: 'pie',
                dataConverter: function(data) {
                    var flotData = _(data).map(function(d) {
                        return {
                            label: d.key,
                            data: d.value
                        };
                    });
                    return {
                        flotOptions: {
                            series: {
                                pie: {
                                    show: true
                                }
                            },
                            grid: {
                                hoverable: true
                            },
                            tooltip: {
                                show: true,
                                content: function(label, xval, yval, flotItem) {
                                    var d = flotData[flotItem.seriesIndex];
                                    return d.label + ': ' + $scope.dataFormatter(d.data);
                                }
                            },
                            legend: {
                                show: true
                            }
                        },
                        data: flotData
                    };
                }
            }, {
                id: 'donut',
                dataConverter: function(data) {
                    var flotData = _(data).map(function(d) {
                        return {
                            label: d.key,
                            data: d.value
                        };
                    });
                    return {
                        flotOptions: {
                            series: {
                                pie: {
                                    innerRadius: 0.5,
                                    show: true
                                }
                            },
                            grid: {
                                hoverable: true
                            },
                            tooltip: {
                                show: true,
                                content: function(label, xval, yval, flotItem) {
                                    var d = flotData[flotItem.seriesIndex];
                                    return d.label + ': ' + $scope.dataFormatter(d.data);
                                }
                            },
                            legend: {
                                show: true
                            }
                        },
                        data: flotData
                    };
                }
            }, {
                id: 'funnel',
                dataConverter: function(data) {
                    var flotData = _(data).map(function(d) {
                        return {
                            label: d.key,
                            data: d.value
                        };
                    });
                    return {
                        flotOptions: {
                            series: {
                                funnel: {
                                    show: true,
                                    margin: {
                                        left: 0.1,
                                        right: 0.1
                                    }
                                }
                            },
                            grid: {
                                hoverable: true
                            },
                            tooltip: {
                                show: true,
                                content: function(label, xval, yval, flotItem) {
                                    var d = flotData[flotItem.seriesIndex];
                                    return d.label + ': ' + $scope.dataFormatter(d.data);
                                }
                            },
                            legend: {
                                show: true
                            }
                        },
                        data: flotData
                    };
                }
            }, {
                id: 'dot',
                dataConverter: function(data) {
                    var tickToId = makeTickToId(data);
                    var idToTick = _(tickToId).invert();
                    return {
                        flotOptions: {
                            xaxis: makeXAxis(tickToId),
                            yaxis: {
                                tickFormatter: function(val) {
                                    return $scope.dataFormatter(val);
                                }
                            },
                            series: {
                                points: {
                                    show: true,
                                    fillColor: false,
                                    lineWidth: 5
                                }
                            },
                            grid: {
                                hoverable: true
                            },
                            tooltip: {
                                show: true,
                                content: '%s: %y'
                            },
                            legend: {
                                show: true
                            }
                        },
                        data: _(applyTicksToData(data, tickToId)).map(function(d) {
                            return {
                                label: idToTick[d[0]],
                                data: [d]
                            };
                        })
                    };
                }
            }];
            
            var timeoutHandle = null;
            var requestTimeout = null;
            $scope.requestData = function() {
                if(timeoutHandle) {
                    $timeout.cancel(timeoutHandle);
                    requestTimeout.resolve();
                }
                if(!$scope.isConfigured()) {
                    return;
                }
                var top = $scope.model.top;
                if(top > 20) {
                    top = 20;
                } else if(top < 1) {
                    top = 1;
                }
                requestTimeout = $q.defer();
                timeoutHandle = $timeout(function() {
                    $scope.options.request($scope.model.keyColumn, $scope.model.valueColumn, top).timeout(requestTimeout).then(function(data) {
                        $scope.data = data;
                        $scope.makeFlotModel();
                    });
                }, 300);
                
            };
            
            $scope.makeFlotModel = function() {
                var graph = _($scope.graphTypes).find(function(type) {
                    return type.id == $scope.model.graph;
                });
                $scope.dataFormatter = (_($scope.options.valueVariants).find(function(variant) {
                    return variant.id == $scope.model.valueColumn;
                }) || {}).formatter || _.identity;
                $scope.flotModel = graph.dataConverter($scope.data);
                $scope.flotModel.onFlotRender = $scope.model.onFlotRender;
            };
            
            $scope.selectGraph = function(id) {
                $scope.model.graph = id;
                if (!$scope.isConfigured()) {
                    return;
                }
                $scope.makeFlotModel();
            };
            
            $scope.showSettings = function() {
                modals.keyValueGraph($scope.model, $.extend({editor: true}, $scope.options), {}, 'lg').then(function(data) {
                    $.extend($scope.model, data);
                    $scope.requestData();
                    if($scope.onChange) {
                        $scope.onChange();
                    }
                });  
            };
            
            $scope.toggleLegend = function() {
                $scope.hideLegend = !$scope.hideLegend;  
            };
            
            $scope.isConfigured = function() {
                return $scope.model.keyColumn && $scope.model.valueColumn && $scope.model.top;
            };
            
            $scope.requestData();
            
            $scope.$on($scope.options.refreshEvent, function() {
                $scope.requestData();
            });
        }
    };
});