var m = angular.module('policy');

m.directive('policyColumnsPick', function(path, recreateDropdownWorkaround) {
    var maxColumnHeight = 10;
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('policy').directive('policy-columns-pick').template(),
        scope: {
            model: '=policyColumnsPick',
            options: '=?'
        },
        link: function($scope, element) {
            var internalModel = {};
            $scope.internalGroups = [];
            $scope.getItemClass = function(item) {
                return internalModel[item.id] ? 'fa-check-square-o' : 'fa-square-o';
            };
            
            $scope.onItemClick = function(event, item) {
                event.preventDefault();
                event.stopPropagation();
                internalModel[item.id] = !internalModel[item.id];
            };
            $scope.applyChange = function() {
                var pathToColumn = {};
                _($scope.options.groups).each(function(group) {
                    _(group.columns).each(function(column) {
                        pathToColumn[column.id] = column;
                    });
                });
                $scope.model = _(internalModel).chain().map(function(value, key) {
                    if(!value) {
                        return null;
                    }
                    return pathToColumn[key] || {
                        id: key
                    };
                }).compact().value();
                ($scope.options.onChange || _.noop)($scope.model);
            };
            
            function getColumnLabel(path) {
                var found = _($scope.options.columns).find(function(column) {
                    return column.path == path;
                }) || {};
                return $scope.options.columnNames[path] || found.text || path;
            }
            
            recreateDropdownWorkaround($scope, element, {
                onOpen: function() {
                    internalModel = $.extend({}, $scope.model);
                    $scope.internalGroups = $.extend([], $scope.options.groups);
                    $scope.internalGroups.push({
                        label: 'Advanced',
                        columns: _(internalModel).chain().map(function(val, key) {
                            return val ? key : null;
                        }).compact().filter(function(key) {
                            return !_($scope.options.groups).find(function(group) {
                                return _(group.columns).find(function(column) {
                                    return column.id == key;
                                });
                            });
                        }).map(function(key) {
                            return {
                                id: key,
                                label: getColumnLabel(key)
                            };
                        }).value()
                    });
                    
                    $scope.columns = [[]];
                    function addLine(data) {
                        var lastColumn = _($scope.columns).last();
                        if(!data.id && lastColumn.length == maxColumnHeight - 1) {
                            lastColumn.push({});
                            lastColumn = [];
                            $scope.columns.push(lastColumn);
                        }
                        if(lastColumn.length == maxColumnHeight) {
                            lastColumn = [];
                            $scope.columns.push(lastColumn);
                        }
                        lastColumn.push(data);
                    }
                    _($scope.internalGroups).each(function(group) {
                        addLine({
                            type: 'title',
                            label: group.label
                        });
                        _(group.columns).each(function(column) {
                            addLine({
                                type: 'item',
                                id: column.id,
                                label: column.label
                            });
                        });
                    });
                    
                    $scope.rows = [];
                    _($scope.columns).each(function(column, colId) {
                        _(column).each(function(row, rowId) {
                            if($scope.rows.length <= rowId) {
                                $scope.rows.push([]);
                            }
                            $scope.rows[rowId].push(row);
                        });
                    });
                }
            });
        }
    };
});