var m = angular.module('policy');

m.directive('remediationActions', function(path, modals, uuid, remediationProperties) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('policy').directive('remediation-actions').template(),
        scope: {
            actions: '=remediationActions',
            actionTypes: '=',
            options: '='
        },
        link: function($scope, element, attr) {
            $scope.addAction = function() {
                $scope.actions.push({
                    id: uuid.random(),
                    attributes: {},
                    tags: {}
                });
            };

            $scope.showConfigurePopup = function(action) {
                var actionType = $scope.getActionType(action);
                modals.params([{
                    params: _(actionType.attributes).chain().filter(function(param) {
                        return _(param.label).isString();
                    }).map(function(param) {
                        return $.extend(true, {}, param, {
                            data: action.attributes[param.id]
                        });
                    }).value(),
                    lastPage: true,
                    getProperties: _.memoize(remediationProperties($scope.options.properties))
                }], actionType.label, actionType.size, {
                    noDataLabel: actionType.confirm
                }).then(function(data) {
                    _(data).each(function(param, id) {
                        action.attributes[id] = param;
                    });
                });
            };

            $scope.getActionType = function(action) {
                return _($scope.actionTypes).find(function(type) {
                    return type.id == action.name;
                });
            };

            $scope.actionTypeChanged = function(action) {
                action.attributes = {};
                _($scope.getActionType(action).attributes).each(function(attribute) {
                    action.attributes[attribute.id] = attribute.defaultAttribute;
                });
            };

            $scope.removeAction = function(index) {
                $scope.actions.splice(index, 1);
            };
        }
    };
});