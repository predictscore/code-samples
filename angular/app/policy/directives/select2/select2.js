var m = angular.module('policy');

m.directive('select2', function($timeout, commonDao) {

    function defaultCreateSearchChoice (term, data) {
        if ($(data).filter(function() {
                return this.text.localeCompare(term)===0;
            }).length===0) {
            return {id:term, text:term};
        }
    }
    return {
        restrict: 'A',
        replace: false,
        scope: {
            model: '=select2',
            variants: '=?',
            settings: '=?',
            lookupParams: '=?',
            selectDisabled: '=?',
            focusOnLoad: '=?'
        },
        link: function ($scope, element, attrs) {
            var initialized = false;
            $scope.settings = $scope.settings || {};
            var multiple = false;
            var recordsPerPage = 10;

            function select2Data () {
                var selectData = element.select2('data');
                if (_(selectData).isArray()) {
                    return _(selectData).map(function (d) {
                        return d.id;
                    });
                } else if (selectData) {
                    return selectData.id;
                } else {
                    return undefined;
                }
            }

            element.on('change', function(data) {
                $timeout(function() {
                    $scope.model = select2Data();
                });
            });

            function reinit() {
                multiple = $scope.settings.multiple !== false;

                var selectSettings = {
                    dropdownCssClass : 'hidden-list-selector-dropdown',
                    placeholder: 'Type and press enter',
                    selectOnBlur: true,
                    createSearchChoice: $scope.settings.createSearchChoice || defaultCreateSearchChoice,
                    multiple: multiple,
                    data: []
                };
                if(!_($scope.variants).isUndefined()) {
                    var data = _($scope.variants).chain();
                    if($scope.settings.sortVariants) {
                        data = data.sortBy(function(variant) {
                            return variant.label || variant;
                        });
                    }
                    data = data.map(function (variant) {
                        if (variant.label) {
                            return {id: variant.id, text: variant.label};
                        } else {
                            return variant;
                        }
                    }).value();
                    selectSettings = {
                        dropdownCssClass: 'list-selector-dropdown',
                        placeholder: 'Click to select',
                        multiple: multiple,
                        data: data
                    };
                    if ($scope.settings.allowCreate) {
                        selectSettings.createSearchChoice = $scope.settings.createSearchChoice || defaultCreateSearchChoice;
                        selectSettings.placeholder = 'Click to select or enter value';
                    }
                }
                if ($scope.settings.lookup) {
                    selectSettings = {
                        dropdownCssClass: 'list-selector-dropdown',
                        placeholder: 'Click to select',
                        multiple: multiple,
                        query: function (options) {
                            return commonDao.genericLookup($scope.settings.lookup.endpoint, $.extend(
                                {}, $scope.settings.lookup.params, $scope.lookupParams, {q: options.term}
                            )).pagination((options.page - 1) * recordsPerPage, recordsPerPage).then(function (response) {
                                options.callback({
                                    results: _(response.data.rows).map(function (row) {
                                        return {
                                            id: row.id,
                                            text: row.label
                                        };
                                    }),
                                    more: response.data.total_rows > (options.page) * recordsPerPage
                                });
                            });
                        },
                        allowClear: $scope.settings.allowClear
                    };
                    if (_($scope.settings.lookup.minInput).isUndefined()) {
                        selectSettings.minimumInputLength = 2;
                    } else {
                        selectSettings.minimumInputLength = $scope.settings.lookup.minInput;
                    }
                    if (_($scope.settings.lookup.no_matches_text).isString()) {
                        selectSettings.formatNoMatches = $scope.settings.lookup.no_matches_text;
                    }
                    if (!$scope.settings.lookup.strict) {
                        selectSettings.placeholder = 'Type and press enter';
                        selectSettings.dropdownCssClass += ' search-field-always-visible';
                        selectSettings.selectOnBlur = true;
                        selectSettings.createSearchChoice = $scope.settings.createSearchChoice || defaultCreateSearchChoice;
                    }
                }

                element.prop('readonly', !!$scope.selectDisabled);
                if(initialized) {
                    element.select2('destroy');
                }
                initialized = false;
                element.addClass('list-selector');
                element.css('display', 'inline-block');
                element.css('width','100%');
                element.find('.select2-choices').addClass('form-control');
                element.select2(selectSettings);
                initialized = true;

                function findElement(id) {
                    if (_(id).isUndefined()) {
                        return;
                    }
                    if (_($scope.variants).isUndefined()) {
                        return {id: id, text: id};
                    }
                    var found = false;
                    _(selectSettings.data).find(function (item) {
                        if (item.children) {
                            _(item.children).find(function (item) {
                                if (item.id === id) {
                                    found = item;
                                }
                                return found;
                            });
                        }
                        if (item.id === id) {
                            found = item;
                        }
                        return found;
                    });
                    if (found !== false) {
                        return found;
                    }
                }

                if ($scope.settings.lookup) {
                    if ($scope.model && (!_($scope.model).isArray() || $scope.model.length)) {
                        element.select2('enable', false);
                        var ids = (selectSettings.multiple ? $scope.model.join(',') : $scope.model);
                        commonDao.genericLookup($scope.settings.lookup.endpoint, $.extend(
                            {}, $scope.settings.lookup.params, {ids: ids}))
                            .then(function (response) {
                                var dataMap = {};
                                _(response.data.rows).each(function (row) {
                                    dataMap[row.id] = {
                                        id: row.id,
                                        text: row.label
                                    };
                                });
                                element.select2('enable', true);
                                if (selectSettings.multiple) {
                                    element.select2('data', _($scope.model).map(function (id) {
                                        return dataMap[id] || {
                                                id: id,
                                                text: id
                                            };
                                    }));
                                } else {
                                    element.select2('data', dataMap[$scope.model] || ($scope.settings.lookup.clearIfNotExists ? {
                                        id: '',
                                        text: ''
                                    } : {
                                            id: $scope.model,
                                            text: $scope.model
                                        }));
                                }
                            });
                    }
                } else {
                    if (selectSettings.multiple) {
                        element.select2('data', _($scope.model).chain().map(findElement).filter(function (d) {
                            return _(d).isObject();
                        }).value());
                    } else {
                        element.select2('data', findElement($scope.model));
                    }
                }

            }

            $scope.$watch('model', function(newVal, oldVal) {
                if(newVal != oldVal) {
                    if (!initialized || !_(newVal).isEqual(select2Data())) {
                        reinit();
                    }
                }
            }, true);
            $scope.$watch('variants', function(newVal, oldVal) {
                if(newVal != oldVal) {
                    reinit();
                }
            }, true);

            $scope.$watch('selectDisabled', function(newVal, oldVal) {
                if(newVal != oldVal) {
                    reinit();
                }
            });

            reinit();
            if ($scope.focusOnLoad) {
                $timeout(function () {
                    element.select2('open');
                }, 500);
            }

            $scope.$on('$destroy', function() {
                element.select2('destroy');
                initialized = false;
            });
        }
    };
});