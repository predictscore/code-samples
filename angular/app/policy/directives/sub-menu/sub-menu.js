var m = angular.module('policy');

m.directive('subMenu', function(path, recursionHelper, $timeout, policyPropertyToNodes, feature, headerOptionsBuilder, policyFilterTypes) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('policy').directive('sub-menu').template(),
        scope: {
            menus: '=subMenu',
            menuId: '=',
            selectedId: '=',
            depth: '@',
            menuClicked: '&menuClicked',
            options: '=?',
            menuFilter: '=?',
            tempSelection: '=?'
        },
        compile: function(element) {
            return recursionHelper.compile(element, this.link);
        },
        link: function($scope, element, attrs) {
            $scope.root = $scope;
            $scope.internal = {};
            $scope.options = $scope.options || {};
            $scope.options.blackList = $scope.options.blackList || [];
            $scope.menuFilter = undefined;
            $scope.showAdvance = false;
            $scope.tempSelection = $scope.tempSelection || {};
            
            function isGroupMenu(nodeId) {
                return $scope.menus[nodeId].groupMenu;
            }
            
            function makeSubMenuId(menuId, nodeId) {
                if(_(nodeId).isObject()) {
                    return nodeId;
                }
                var id = _(nodeId.split('.')).last();
                var fullId = _(menuId.fullId.split('.')).initial().join('.');
                if(fullId) {
                    fullId += '.';
                }
                return {
                    id: id,
                    nodeId: nodeId,
                    fullId: fullId + nodeId
                };
            } 
            
            $timeout(function() {
                $(element).find('.submenu-search input').focus();
            });
            
            $scope.$watch('menus', function() {
                $scope.hoveredSubMenu = null;
            });

            $scope.singleMenuClickedWrapper = function(event, menuId, filter, inEditor) {
                if (inEditor && !filter) {
                    event.stopPropagation();
                    return;
                }
                var menuIds = {};
                menuIds[menuId.fullId] = true;
                if($scope.options.multiSelect) {
                    if($scope.menus[menuId.nodeId].selectable) {
                        if(_($scope.tempSelection[menuId.fullId]).isUndefined()) {
                            $scope.tempSelection[menuId.fullId] = !_($scope.options.selectedList).contains(menuId.fullId);
                        } else {
                            $scope.tempSelection[menuId.fullId] = !$scope.tempSelection[menuId.fullId];
                        }
                        if($scope.tempSelection[menuId.fullId] == _($scope.options.selectedList).contains(menuId.fullId)) {
                            delete $scope.tempSelection[menuId.fullId];
                        }
                    }
                } else if(!inEditor) {
                    if($scope.options.editor) {
                        var property = $scope.menus[menuId.nodeId];
                        var type = policyFilterTypes(property);
                        $scope.internal.filterOptions = headerOptionsBuilder($scope.options.editorOptions, property)[type];
                        $scope.internal.filter = $scope.options.getConditions(menuId);
                    } else if($scope.menus[menuId.nodeId].selectable) {
                        $scope.menuClicked({event: event, menuIds: menuIds});
                    }
                } else {
                    $scope.menuClickedWrapper(event, menuIds, filter, inEditor);
                }
            };
            
            $scope.menuClickedWrapper = function(event, menuIds, filter, inEditor) {
                $scope.menuClicked({event: event, menuIds: menuIds, filter: filter, inEditor: inEditor});
            };
            
            $scope.isTempSelected = function(menuId) {
                return !_($scope.tempSelection[menuId.fullId]).isUndefined();
            };
            
            $scope.onApplyClick = function(event) {
                $scope.menuClicked({event: event, menuIds: $scope.tempSelection});
            };
            
            $scope.editorClick = function(event) {
                if(event.target != $(element).find('.ok-button').get(0)) {
                    event.stopPropagation();
                }
            };
            
            $scope.getMenuIconClass = function(menuId) {
                var selected = false;
                if(_($scope.tempSelection[menuId.fullId]).isUndefined()) {
                    selected = _($scope.options.selectedList).contains(menuId.fullId);
                } else {
                    selected = $scope.tempSelection[menuId.fullId];
                }
                return selected ? $scope.options.menuIconSelected : $scope.options.menuIconUnselected;
            };
            
            $scope.isApplyActive = function() {
                return _($scope.tempSelection).keys().length;  
            };

            $scope.getSubMenus = function() {
                if(!_($scope.menus).isUndefined()) {
                    var scroll = $scope.internal.scroll;
                    var maxLines = $scope.internal.maxLines;
                    var count = 0;
                    var idx = 0;
                    var menus = _($scope.allSubMenus).reduce(function(memo, menu){
                        if(idx >= scroll && count < maxLines) {
                            memo.push(menu);
                            if(_(menu).isString()) {
                                count++;
                            }
                        }
                        if(_(menu).isString()) {
                            idx++;
                        }
                        return memo;
                    }, []);
                    while(_(_(menus).last()).isObject()) {
                        menus = _(menus).initial();
                    }
                    return _(menus).map(function(nodeId) {
                        return makeSubMenuId($scope.menuId, nodeId);
                    });
                }
            };

            $scope.isScrollUpEnabled = function() {
                if(!_($scope.menus).isUndefined()) {
                    return $scope.internal.scroll > 0;
                }
            };

            function filterMenus(menu) {
                return _(menu).isString();
            }
            
            function filterVisibleMenus(menuId, menuFilter) {
                return function filterVisibleMenus(subId) {
                    if($scope.depth === 0 && $scope.menus[subId].sub) {
                        return false;
                    }
                    if(menuFilter) {
                        var filterIndex = $scope.menus[subId].label.toLowerCase().indexOf(menuFilter.toLowerCase());
                        if(filterIndex == -1) {
                            return false;
                        }
                    }
                    var menu = $scope.menus[subId];
                    if(!menu) {
                        return false;
                    }
                    $scope.ignore[menu.pointer] = $scope.ignore[menu.pointer] || 0;
                    if(!menu || $scope.ignore[menu.pointer] >= feature.policyPropertiesRecursionLimit) {
                        return false;
                    }
                    if(!$scope.showAdvance) {
                        var isRoot = menuId.nodeId === 0 || isGroupMenu(menuId.nodeId) && menuId.fullId.indexOf('.') == -1;
                        if(isRoot && menu.advanceProperty) {
                            return false;
                        }
                        if(!isRoot && menu.advancePointer || menu.advanceProperty) {
                            return false;
                        }
                    }
                    if(menuId.nodeId === 0) {
                        return true;
                    }
                    if(_($scope.options.blackList).contains(menuId.fullId + '.' + _(subId.split('.')).last())) {
                        return false;
                    }
                    if(_(subId).isObject()) {
                        return !subId.onlyOnRoot;
                    }
                    return !menu.onlyOnRoot;
                };
            }
            

            $scope.isScrollDownEnabled = function() {
                if(!_($scope.menus).isUndefined()) {
                    return _($scope.allSubMenus).filter(filterMenus).length > $scope.internal.scroll + $scope.internal.maxLines;
                }
            };

            $scope.onWheel = function(event, delta) {
                if($scope.internal.maxLines < Number.MAX_VALUE) {
                    event.preventDefault();
                    event.stopPropagation();
                    $scope.processScroll(delta);
                }
            };

            $scope.onScrollClick = function(event, delta) {
                $scope.processScroll(delta);
                event.preventDefault();
                event.stopPropagation();
                //$scope.menuClicked({event: event, menuId: {}});
            };

            $scope.processScroll = function(delta) {
                $scope.hoveredSubMenu = null;
                var prevScroll = $scope.internal.scroll;
                $scope.internal.scroll -= delta;
                $scope.internal.scroll = Math.min(_($scope.allSubMenus).filter(filterMenus).length - $scope.internal.maxLines, $scope.internal.scroll);
                $scope.internal.scroll = Math.max(0, $scope.internal.scroll);
                if($scope.internal.scroll != prevScroll) {
                    $(element).find('.submenu-menus > a').each(function() {
                        $(this).blur();
                    });
                }
                $scope.subMenus = $scope.getSubMenus();
            };

            $scope.getTopForMenu = function(index) {
                var submenu = $scope.menus[$scope.subMenus[index].nodeId];
                if(_(submenu.sub).isUndefined()) {
                    return 0;
                }
                var submenuHeight = Math.min(_(submenu.sub).chain().filter(function(subId) {
                    return !($scope.depth == 1 && $scope.menus[subId].sub);
                }).filter(filterMenus).filter(function(subId) {
                    return filterVisibleMenus($scope.subMenus[index], $scope.internal.subMenuFilter)(subId);
                }).value().length - 1, $scope.internal.maxLines);
                var realIndex = 0;
                for(var i = 0; i < index; i++) {
                    if(_($scope.subMenus[i].id).isString()) {
                        realIndex++;
                    }
                }
                return -((Math.min(submenuHeight, realIndex) + 1) * 100) + '%';
            };

            $scope.isSelectedPath = function(menu) {
                return $scope.selectedId &&
                    _($scope.selectedId.fullId).isString() &&
                    $scope.selectedId.fullId.indexOf(menu.fullId) === 0;
            };

            $scope.getSubSelected = function() {
                if(_($scope.selectedId).isUndefined()) {
                    return undefined;
                }
                var dot = $scope.selectedId.indexOf('.');
                if(~dot) {
                    return undefined;
                }
                return $scope.selectedId.slice(dot + 1);
            };
            
            $scope.clearFilter = function() {
                $scope.menuFilter = '';  
            };
            
            $scope.setAdvance = function(event, newAdvance) {
                $scope.showAdvance = newAdvance;
                event.preventDefault();
                event.stopPropagation();
            };

            function init() {
                var menus = $scope.menus;
                $scope.hoveredSubMenu = null;
                $scope.internal = {
                    maxLines: 18,//Number.MAX_VALUE,
                    scroll: 0
                };

                if(_(menus).isUndefined()) {
                    return;
                }
                
                $scope.ignore = {};
                if($scope.menuId.id && !feature.propertiesRecursion) {
                    var addNodeToIgnore = function(nodeId) {
                        $scope.ignore[nodeId] = ($scope.ignore[nodeId] || 0) + 1;
                    };
                    var nodes = policyPropertyToNodes($scope.menuId.fullId);
                    _(nodes).chain().map(function(nodeId) {
                        if(!$scope.menus[nodeId]) {
                            return null;
                        }
                        return $scope.menus[nodeId].entityName;
                    }).filter(_.isString).each(addNodeToIgnore);
                    var lastNode = _(nodes).last();
                    if($scope.menus[lastNode] && $scope.menus[lastNode].pointer) {
                        addNodeToIgnore($scope.menus[lastNode].pointer);
                    }
                }
                $scope.allSubMenus = _($scope.menus[$scope.menuId.nodeId].sub).filter(filterVisibleMenus($scope.menuId, $scope.menuFilter));

                $scope.subMenus = $scope.getSubMenus();
                $scope.title = $scope.menus[$scope.menuId.nodeId].pointerLabel;
            }
            
            function initWatch(newVal, oldVal) {
                if(oldVal == newVal) {
                    return;
                }
                init();
            }

            $scope.$watch('menus', initWatch);
            $scope.$watch('menuFilter', initWatch);
            $scope.$watch('showAdvance', initWatch);
            init();
            
            var keys = {
                left: 37,
                right: 39,
                up: 38,
                down: 40
            };
            function processKeys(e) {
                var processed = false;
                if(e.keyCode == keys.down && $(element).find('.submenu-search input').is(':focus')) {
                    $(element).find('.submenu-menus:first > a').focus();
                    processed = true;
                } else {
                    var focusedIdx = -1;
                    var submenus = $(element).find('.submenu-menus > a');
                    submenus.each(function(idx, el) {
                        if($(el).is(':focus')) {
                            focusedIdx = idx;
                        }
                    });
                    processed = true;
                    if(e.keyCode == keys.left && _($scope.hoveredSubMenu).isNumber()) {
                        $timeout(function() {
                            submenus[$scope.hoveredSubMenu].focus();
                            $scope.hoveredSubMenu = undefined;
                        });
                    } else if(focusedIdx < 0) {
                        processed = false;
                    } else if(e.keyCode == keys.up && focusedIdx === 0) {
                        $timeout(function() {
                            $(element).find('.submenu-search input').focus();
                        });
                    } else if(e.keyCode == keys.up) {
                        $timeout(function() {
                            submenus[focusedIdx - 1].focus();
                        });
                    } else if(e.keyCode == keys.down) {
                        $timeout(function() {
                            if(submenus.length > focusedIdx + 1) {
                                submenus[focusedIdx + 1].focus();
                            }
                        });
                    } else if(e.keyCode == keys.right) {
                        if($scope.menus[$scope.subMenus[focusedIdx].nodeId].sub) {
                            $timeout(function() {
                                $scope.hoveredSubMenu = focusedIdx;
                            });
                        }
                    } else {
                        processed = false;
                    }
                }
                
                if(processed) {
                    e.stopPropagation();
                    e.preventDefault();
                }
            }
            
            $(element).on('keydown', processKeys);
            
            $scope.$on('$destroy', function() {
                $(element).off('keydown', processKeys);
            });
        }
    };
});