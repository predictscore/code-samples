var m = angular.module('policy');

m.directive('stateSwitch', function(path) {
    var root = path('policy').directive('state-switch');
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('policy').directive('state-switch').template(),
        scope: {
            model: '=stateSwitch',
            states: '=?'
        },
        link: function($scope, element, attrs) {
            $scope.states = $scope.states || {
                'off': root.img('off.png'),
                'on-blue': root.img('on-blue.png'),
                'on-green': root.img('on-green.png')
            };
            $scope.icons = $scope.icons || {
                'updating': 'fa fa-spinner fa-pulse state-switch-icon'
            };
        }
    };
});