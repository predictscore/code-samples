var m = angular.module('policy');

m.directive('policyColumnsPickV2', function(path, recreateDropdownWorkaround) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('policy').directive('policy-columns-pick-v2').template(),
        scope: {
            model: '=policyColumnsPickV2',
            options: '=?'
        },
        link: function($scope, element) {
            $scope.internal = {};
            $scope.internalModel = {};
            $scope.internalGroups = [];
            $scope.getItemClass = function(item) {
                return $scope.internalModel[item.id] ? 'fa-check-square-o' : 'fa-square-o';
            };
            
            $scope.onItemClick = function(event, item) {
                event.preventDefault();
                event.stopPropagation();
                $scope.internalModel[item.id] = !$scope.internalModel[item.id];
            };
            $scope.applyChange = function() {
                var pathToColumn = {};
                _($scope.options.groups).each(function(group) {
                    _(group.columns).each(function(column) {
                        pathToColumn[column.id] = column;
                    });
                });
                $scope.model = _($scope.internalModel).chain().map(function(value, key) {
                    if(!value) {
                        return null;
                    }
                    return pathToColumn[key] || {
                        id: key
                    };
                }).compact().value();
                ($scope.options.onChange || _.noop)($scope.model);
            };
            
            function getColumnLabel(path) {
                var found = _($scope.options.columns).find(function(column) {
                    return column.path == path;
                }) || {};
                return $scope.options.columnNames[path] || found.text || path;
            }
            
            $scope.selectGroup = function(group) {
                $scope.prevGroup = $scope.selectedGroup = group;
            };
            
            $scope.showSearch = function() {
                if($scope.internal.search) {
                    $scope.searchGroup = $scope.selectedGroup = {
                        columns:  _($scope.internalGroups).chain().map(function(val) {
                            return val.columns;
                        }).flatten().filter(function(column) {
                            return column.label.toLowerCase().indexOf($scope.internal.search.toLowerCase()) != -1;
                        }).sortBy('label').value()
                    };
                } else {
                    $scope.selectedGroup = $scope.prevGroup;
                }
            };
            
            recreateDropdownWorkaround($scope, element, {
                onOpen: function() {
                    $scope.internalModel = $.extend({}, $scope.model);
                    $scope.internalGroups = $.extend([], $scope.options.groups);
                    var columns = _($scope.internalModel).chain().map(function(val, key) {
                        return val ? key : null;
                    }).compact().filter(function(key) {
                        return !_($scope.options.groups).find(function(group) {
                            return _(group.columns).find(function(column) {
                                return column.id == key;
                            });
                        });
                    }).map(function(key) {
                        return {
                            id: key,
                            label: getColumnLabel(key)
                        };
                    }).value();
                    
                    if(!_(columns).isEmpty()) {
                        $scope.internalGroups.push({
                            label: 'Advanced',
                            columns: columns
                        });
                    }
                    $scope.selectGroup(_($scope.internalGroups).first());
                }
            });
        }
    };
});