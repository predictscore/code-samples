var m = angular.module('policy');

m.directive('policyExcludedBy', function(path) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('policy').directive('policy-excluded-by').template(),
        scope: {
            model: '=policyExcludedBy',
            properties: '='
        },
        link: function($scope, element, attrs) {
            $scope.model = $scope.model || [];

            $scope.addPath = function() {
                $scope.model.push({});
            };

            $scope.removePath = function(index) {
                $scope.model.splice(index, 1);
            };
        }
    };
});