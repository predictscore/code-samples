var m = angular.module('policy');

m.directive('showSubMenu', function($parse) {
    return {
        restrict: 'A',
        replace: false,
        link: function($scope, element, attrs) {
            var subMenuId = $parse(attrs.showSubMenu)($scope);
            var action = attrs.showSubMenuAction || 'hover';
            if(action == 'hover') {
                $(element).hover(function() {
                    $scope.$parent.hoveredSubMenu = subMenuId;
                    $scope.$parent.$digest();
                }, function() {
                });
            }
            $(element).click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                if(action == 'click') {
                    $scope.$parent.hoveredSubMenu = $scope.$parent.hoveredSubMenu == subMenuId ? null : subMenuId;
                }
                $scope.$parent.$digest();
            });
        }
    };
});
