var m = angular.module('policy');

m.directive('subButton', function(path, $timeout, feature, recreateDropdownWorkaround) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('policy').directive('sub-button').template(),
        scope: {
            options: '=subButton',
            onClick: '&'
        },
        link: function ($scope, element, attrs, controller, transclude) {
            $scope.options = $.extend(true, {
                dropdownClass: 'avanan-square'
            }, $scope.options);
            $scope.depth = feature.policyPropertyListDepth - 1;
            $scope.tempSelection = {};
            function updateMenus() {
                $scope.menus = _($scope.options.menus).chain().map(function(menu, menuId){
                    var result = $.extend(true, {}, menu);
                    if(result.sub && $scope.options.menusBlackList) {
                        result.sub = _(result.sub).filter(function(subMenu) {
                            return !_($scope.options.menusBlackList).contains(subMenu);
                        });
                    }
                    return [menuId, result];
                }).object().value();
                
                $scope.root = {
                    id: 0,
                    nodeId: 0,
                    fullId: $scope.menus[0].id
                };
            }
            
            $scope.$watch('options.menus', updateMenus);
            $scope.$watch('options.menusBlackList', updateMenus);
            
            $scope.menuClicked = function(event, menuIds, filter) {
                event.preventDefault();
                $scope.onClick({menuIds: menuIds, filter: filter});
                $scope.showDropdown = false;
            };
            
            recreateDropdownWorkaround($scope, element, {
                onOpen: function() {
                    $scope.tempSelection = {};
                }
            });
        }
    };
});
