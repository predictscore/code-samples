var m = angular.module('policy');

m.directive('policyTemplateOptions', function(path, policyDao, modals, columnsHelper, tablePaginationHelper, uuid, remediationProperties, feature, modulesManager, objectTypeDao, $q) {
    var root = path('policy').directive('policy-template-options');
    return {
        restrict: 'A',
        replace: false,
        templateUrl: root.template(),
        scope: {
            model: '=policyTemplateOptions',
            sources: '=',
            sourcesInfo: '=',
            propertyTypes: '=',
            properties: '=',
            actionTypes: '=',
            generateBody: '&'
        },
        link: function($scope, element, attrs) {
            $scope.objectTypesRetrieve = $q.when(objectTypeDao.retrieve());
            
            $scope.feature = feature;
            $scope.modulesManager = modulesManager;
            $scope.properties = $scope.sources($scope.model.entityType);

            $scope.internalModel = {
                originalModel: $.extend(true, {}, $scope.model),
                editableTemplate: true
            };

            $scope.columnsPanel = {
                title: 'Columns to display',
                minimize: true,
                minimized: true,
                'class': 'expandable-group left-controls no-margin',
                icons: {
                    minimize: 'fa-minus',
                    expand: 'fa-plus'
                }
            };

            $scope.getActionType = function(action) {
                return _($scope.actionTypes[$scope.model.entityType]).find(function(type) {
                    return type.id == action.name;
                });
            };

            if($scope.model.templateName) {
                $scope.internalModel.editableTemplate = false;
                // selectTemplate();
            } else {
                root.json('template-list-conf').retrieve().then(function(response) {
                    $scope.options = response.data;
                    var saasList = modulesManager.cloudApps();
                    $scope.policyIdx = columnsHelper.getIdxById(response.data.columns, 'policy');
                    var columnsCount = 0;
                    _(response.data.columns).each(function (column) {
                        if (columnsHelper.isColumnVisible(column, $scope.tableOptions)) {
                            columnsCount++;
                        }
                    });
                    var templatesList = policyDao.getTemplatesList(response.data.columns, $scope.sourcesInfo).converter(function (input) {
                        var data = [];
                        var total = 0;
                        _(saasList).each(function (app) {
                            var rows = _(input.data.data).filter(function (row) {
                                return row[$scope.policyIdx].originalText.policy_desc.saas === app.name;
                            });
                            if (rows.length > 0) {
                                data.push([{
                                    text: '#' + JSON.stringify({
                                        display: 'linked-text',
                                        text: '#' + JSON.stringify({
                                            display: 'img',
                                            path: app.img
                                        }) + '#' + JSON.stringify({
                                            display: 'text',
                                            text: 'Query templates for ' + app.title
                                        }),
                                        'class': 'policy-template-saas-title'
                                    }),
                                    alwaysShow: true,
                                    colSpan: columnsCount,
                                    saasTitle: true,
                                    saasName: app.name
                                }].concat(_(response.data.columns.length - 1).chain().range().map(function () {
                                    return {
                                        hidden: true
                                    };
                                }).value()));
                                data = data.concat(rows);
                                total += (1 + rows.length);
                            }
                        });
                        input.data.pagination.total = total;
                        $.extend(input.data, {data: data});
                        return $q.when(input);
                    });
                    $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function (args) {
                        return templatesList;
                    }, function (tableModel) {
                        $scope.tableModel = tableModel;
                    });

                    $scope.$watchCollection('tableHelper.getSelected()', function(selected) {
                        if(_(selected).isUndefined() || selected.length === 0) {
                            return;
                        }
                        var template = _(selected).first();
                        if (template[0].saasTitle) {
                            $scope.tableHelper.clearSelection();
                            return;
                        }
                        var nameIdx = columnsHelper.getIdxById(response.data.columns, 'template_name');
                        var descIdx = columnsHelper.getIdxById(response.data.columns, 'template_desc');
                        var policyIdx = columnsHelper.getIdxById(response.data.columns, 'policy');
                        var policy = template[policyIdx].originalText;

                        $scope.selection = {};
                        $.extend(true, $scope.selection, policy.policy_desc, {
                            templateName: template[nameIdx].originalText,
                            templateDesc: template[descIdx].originalText,
                            variables: policy.variables
                        });

                        selectTemplate();
                        $scope.tableHelper.clearSelection();
                    });
                });
            }

            $scope.tableOptions = {
                pagesAround: 2,
                pageSize: Number.MAX_VALUE,
                pagination: {
                    page: 1,
                    ordering: {},
                    filter: ''
                },
                disableBottom: true,
                showWorkingIndicator: true,
                filter: function (row) {
                    var result = {};
                    if (row[0].saasTitle) {
                        result['policy-template-title-row'] = true;
                        result['saas-' + row[0].saasName] = true;
                    } else {
                        result['saas-' + row[$scope.policyIdx].originalText.policy_desc.saas] = true;
                    }
                    return result;
                }
            };

            $scope.reset = function() {
                if($scope.internalModel.editableTemplate) {
                    for(var key in $scope.model) if($scope.model.hasOwnProperty(key)) {
                        delete $scope.model[key];
                    }
                    $.extend($scope.model, $scope.internalModel.originalModel);
                    delete $scope.internalModel.questions;
                    $scope.internalModel.templateSelected = false;
                }
            };

            function selectTemplate() {
                var conditions = {};
                function parseConditions(group) {
                    _(group.conditions).each(function(condition) {
                        if(condition.type == 'condition') {
                            if(_(condition.dataVar).isString()) {
                                conditions[condition.dataVar] = condition;
                            }
                        } else {
                            parseConditions(condition);
                        }
                    });
                }
                parseConditions($scope.selection.includeCondition || {});
                parseConditions($scope.selection.excludeCondition || {});

                $scope.internalModel.questions = _($scope.selection.variables).map(function(value, key) {
                    return $.extend({
                        id: key,
                        condition: conditions[key]
                    }, value);
                });
                
                if(($scope.selection.optionalActions || []).length === 0 && $scope.internalModel.questions.length === 0) {
                    $scope.model = $.extend($scope.model, $scope.selection);
                    $scope.$emit('auto-save', {});
                } else {
                    $scope.objectTypesRetrieve.then(function(response) {
                        $scope.model = $.extend($scope.model, $scope.selection);
                        var sources = response.data.sources;
                        $scope.properties = sources[$scope.model.entityType];
                        
                        $scope.internalModel.optionalActions = _($scope.model.optionalActions).map(function(optional) {
                            var actionType = $scope.getActionType(optional);
                            var properties = remediationProperties($scope.properties.properties);
                            var self = {
                                label: optional.label,
                                action: _($scope.model.actions).find(function(action) {
                                    return !_(optional.id).isUndefined() && action.id == optional.id;
                                }),
                                enabled: !_(optional.id).isUndefined(),
                                questions: _(optional.variables).map(function(value, key) {
                                    return $.extend({
                                        id: key,
                                        param: $.extend(true, {}, _(actionType.attributes).find(function(attr) {
                                            return attr.id == key;
                                        }))
                                    }, value);
                                }),
                                properties: properties,
                                onChange: function() {
                                    if(self.enabled) {
                                        $scope.model.actions = _($scope.model.actions).filter(function(action) {
                                            return action != self.action;
                                        });
                                        delete optional.id;
                                    } else {
                                        var action = $.extend(true, {}, _(optional).omit('variables'));
                                        optional.id = action.id = uuid.random();
                                        $scope.model.actions.push(action);
        
                                        self.action = action;
                                    }
                                }
                            };
                            return self;
                        });
                        $scope.internalModel.templateSelected = true;
                    });
                }
            }
        }
    };
});