var m = angular.module('policy');

m.directive('policyConditionSquare', function(path, recreateDropdownWorkaround, headerOptionsBuilder, $timeout) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('policy').directive('policy-condition-square').template(),
        scope: {
            options: '=policyConditionSquare'
        },
        link: function ($scope, element, attrs) {
            $scope.internal = {};
            $scope.headerOptions = headerOptionsBuilder($scope.options.headerOptions, $scope.options.property)[$scope.options.propertyType];
            
            $scope.editorClick = function(event) {
                if(event.target != $(element).find('.save-button').get(0) && event.target != $(element).find('.add-column-button').get(0)) {
                    event.stopPropagation();
                }
            };
            
            $scope.save = function() {
                $timeout(function () {
                    $scope.options.onSave($scope.internal.filters);
                });
            };
            
            $scope.addColumn = function() {
                $scope.options.onColumnAdd($scope.options.path);
            };
            
            $scope.conditionClicked = function(event) {
                if($(event.target).hasClass('condition-remove')) {
                    $(element).removeClass('open');
                } else {
                    $scope.buttonClicked();
                }
            };
            
            $scope.close = function() {
                $scope.showDropdown = false;
            };
            
            recreateDropdownWorkaround($scope, element, {
                onOpen: function() {
                    $scope.internal.filters = $.extend(true, [], $scope.options.filters);
                    $scope.internal.showAddColumn = $scope.options.canAddColumn($scope.options.path);
                }
            });
        }
    };
});