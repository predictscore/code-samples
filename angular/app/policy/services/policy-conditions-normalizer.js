var m = angular.module('policy');

m.factory('policyConditionsNormalizer', function(policyPropertyPathToId, policyPropertyParser, policyFilterTypes, policyHeaderOptions, logging, policyConditionToFiltersConverter, policyPropertyMissingMsg) {
    function expandConditions(cond) {
        if(cond.operator != 'AND') {
            throw "Root operator is not AND";
        }
        var uniqProperties = {};
        _(cond.conditions).each(function(c) {
            var property;
            if(c.type == 'condition') {
                property = c.property;
            } else {
                var uniqSub = _(c.conditions).chain().map(function(subOneC) {
                    if(subOneC.type == 'condition') {
                        return subOneC.property;
                    } else if(c.operator == 'OR') {
                        throw "Group under OR group (1)";
                    } else {
                        return _(subOneC.conditions).map(function(subTwoC) {
                            if(subTwoC.type == 'condition') {
                                return subTwoC.property;
                            } else {
                                throw "Group under OR group (2)";
                            }
                        });
                    }
                }).flatten().uniq().value();
                if(uniqSub.length > 1) {
                    throw "Different properties under non root group";
                } else if(uniqSub.length == 1) {
                    property = _(uniqSub).first();
                }
            }
            if(property) {
                uniqProperties[property] = uniqProperties[property] || [];
                uniqProperties[property].push(c);
            }
        });
        var propertiesConditions = _(uniqProperties).map(function(conds, property) {
            try {
                if(conds.length == 1) {
                    var firstCond = _(conds).first();
                    if(firstCond.type == 'group' && firstCond.operator == 'AND') {
                        var groupFound = _(firstCond.conditions).find(function(c) {
                            return c.type == 'group';
                        });
                        if(groupFound) {
                            conds = firstCond.conditions;
                        } else if(firstCond.conditions.length == 2) {
                            var negativeExist = _(firstCond.conditions).find(function(c) {
                                return c.not;
                            });
                            var positiveExist = _(firstCond.conditions).find(function(c) {
                                return !c.not;
                            });
                            if(negativeExist && positiveExist) {
                                 conds = firstCond.conditions;
                            }
                        }
                    }
                }
                if(conds.length > 3) {
                    throw "More then 3 properties/groups with same entity under root group";
                }
                var notNull;
                var positive;
                var negative;
                _(conds).each(function(c) {
                    if(c.type == 'condition' && c.operator == 'not_null') {
                        notNull = c;
                    } else if (c.type == 'condition') {
                        if(c.not || c.aggregatedOperator == 'none') {
                            negative = c;
                        } else {
                            positive = c;
                        }
                    } else {
                        var nots = _(c.conditions).chain().map(function(subC) {
                            if(subC.type != 'condition') {
                                throw 'Too deep group';
                            }
                            if(subC.operator == 'not_null') {
                                throw 'Not null under group';
                            }
                            return subC.not || subC.aggregatedOperator == 'none';
                        }).uniq().value();
                        
                        if(nots.length > 1) {
                            throw "Positive and negative conditions under same group";
                        } else if(_(nots).first()) {
                            if(c.operator != 'AND') {
                                throw 'OR operator between negative conditions';
                            }
                            negative = c;
                        } else {
                            if(c.operator != 'OR') {
                                throw 'AND operator between positive conditions';
                            }
                            positive = c;
                        }
                    }
                });
                var found = [];
                
                if(notNull) {
                    if(notNull.not) {
                        throw "Not empty condition with not";
                    }
                    found.push(notNull);
                }
                if(positive) {
                    if(positive.not) {
                        throw "Positive codition/group with not";
                    }
                    if(positive.type == 'condition') {
                        positive = {
                            type: 'group',
                            not: false,
                            operator: 'OR',
                            conditions: [positive]
                        };
                    }
                    found.push(positive);
                }
                if(negative) {
                    if(negative.type == 'group' && negative.not) {
                        throw "Negative group with not";
                    }
                    if(negative.type == 'condition') {
                        negative = {
                            type: 'group',
                            not: false,
                            operator: 'OR',
                            conditions: [negative]
                        };
                    }
                    found.push(negative);
                }
                if(found.length != conds.length) {
                    throw "Incorrect configuration of found properties/groups";
                }
                
                return {
                    type: 'group',
                    not: false,
                    operator: 'AND',
                    conditions: found
                };
            } catch (e) {
                if(_(e).isString()) {
                    throw e + " (" + property + ")";
                }
                throw e;
            }
        });
        
        return {
            type: 'group',
            not: false,
            operator: 'AND',
            conditions: propertiesConditions
        };
    }
    
    function restoreMetaInfo(properties, condition, depth) {
        var property;
        var propertiesList;
        if(depth === 0) {
            //Nothing todo 
        } else if(depth === 1) {
            var subConditions = _(condition.conditions).find(function(c) {
                return c.type == 'group'; 
            });
            var notNullCondition = _(condition.conditions).find(function(c) {
                return c.type == 'condition' && c.operator == 'not_null'; 
            });
            property = null;
            if(subConditions) {
                propertiesList = _(subConditions.conditions).chain().map(function(c) {
                    return c.property;
                }).uniq().value();
                if(propertiesList.length > 1) {
                    return;
                } else if(propertiesList.length == 1) {
                    property = _(propertiesList).first();
                }
                
            }
            
            if(notNullCondition) {
                if(property !== null && property != notNullCondition.property) {
                    return;
                }
                property = notNullCondition.property;
                notNullCondition.generatedFrom = {
                    notNull: true,
                    id: policyPropertyPathToId.toId(property)
                };
            }
            condition.generatedFrom = {
                id: policyPropertyPathToId.toId(property)
            };
            
        } else if(depth === 2) {
            var found = _(condition.conditions).find(function(c) {
                return c.type == 'group'; 
            });
            if(found) {
                return;
            }
            propertiesList = _(condition.conditions).chain().map(function(c) {
                return c.property;
            }).uniq().value();
            if(propertiesList.length != 1) {
                return;
            }
            property = _(propertiesList).first();
            condition.generatedFrom = {
                id: policyPropertyPathToId.toId(property)
            };
        } else if(depth === 3) {
            var parsed = policyPropertyParser(condition.property);
            var objectProperty = properties[parsed.nodeId];
            if (!objectProperty) {
                throw {
                    type: 'not_found',
                    fullId: condition.property
                };
            }
            var dataLabel;
            if(objectProperty.variants) {
                var variantsMap = _(objectProperty.variants).chain().map(function(variant) {
                    return [variant.id, variant.label];
                }).object().value();
                var data = condition.data;
                if(!_(data).isArray()) {
                    data = [data];
                }
                dataLabel = _(data).map(function(d) {
                    var result = variantsMap[d];
                    if(!result && d === null) {
                        return 'Is empty';
                    }
                    return result || d;
                });
            }
            var type = policyFilterTypes(objectProperty);
            if(type == 'multiEnum' && condition.operator == 'is') {
                condition.operator = 'in';
                condition.data = [condition.data];
            }
            var operator = _(policyHeaderOptions()[type].operators).find(function(o) {
                return o.id == condition.operator;
            });
            if(condition.operator == 'bool') {
                operator = {
                    id: 'is',
                    label: 'is'
                };
            }
            if(_(operator).isUndefined()) {
                return;
            }
            condition.generatedFrom = {
                id: policyPropertyPathToId.toId(condition.property),
                filter: {
                    type: type,
                    operator: operator.id,
                    operatorLabel: operator.label,
                    dataLabel: dataLabel
                }
            };
            return;
        }
        _(condition.conditions).each(function(c) {
            restoreMetaInfo(properties, c, depth + 1);
        });
    }
        
    return function(properties, condition) {
        try {
            var result = expandConditions(condition);
            restoreMetaInfo(properties, result, 0);
            var filters = policyConditionToFiltersConverter.toFilters(result);
            result = policyConditionToFiltersConverter.toConditions(filters);
            return result;
        } catch(e) {
            if (e.type && e.type === 'not_found' && e.fullId) {
                var missingMsg = policyPropertyMissingMsg(properties, e.fullId);
                if (missingMsg) {
                    logging.log('Its advanced policy because ' + missingMsg);
                } else {
                    logging.log('Its advanced policy because of property not found exception');
                }
                return false;
            }
            logging.log('Its advanced policy because of: ' + e);
            return null;
        }
    };
});