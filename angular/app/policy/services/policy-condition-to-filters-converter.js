var m = angular.module('policy');

m.factory('policyConditionToFiltersConverter', function(columnsHelper, policyColumnsFiltersConverter, policyPropertyPathToId) {
    function generateCondition(id, filter) {
        var operator = filter.operator;
        var data = filter.data;
        var not = filter.not || false;
        var aggregatedOperator = filter.aggregatedOperator;
        var options = filter.options;
        filter = _(filter).omit('data', 'not', 'options');
        if(filter.type == 'boolean') {
            operator = 'bool';
        }
        var result = {
            generatedFrom: {
                id: id,
                filter: filter
            },
            type: 'condition',
            not: not,
            options: options,
            property: policyPropertyPathToId.toPath(id),
            operator: operator,
            data: data
        };
        if(aggregatedOperator) {
            result.aggregatedOperator = aggregatedOperator;
        }
        
        return result;
    }
    
    function generateFilter(condition) {
        var filter = condition.generatedFrom.filter;
        filter.data = condition.data;
        filter.not = condition.not;
        filter.options = condition.options;
        if(condition.aggregatedOperator) {
            filter.aggregatedOperator = condition.aggregatedOperator;
        }
        return filter;
    }
    
    return {
        toFilters: function(conditions) {
            return _(conditions.conditions).chain().filter(function(condition) {
                return condition.generatedFrom;
            }).map(function(condition) {
                if(condition.type == 'condition') {
                    return [condition.generatedFrom.id, [generateFilter(condition)]];
                } else {
                    if(condition.operator == 'OR') { 
                        return [condition.generatedFrom.id, _(condition.conditions).map(function(c) {
                            return generateFilter(c);
                        })];
                    } else {
                        var notNullCondition;
                        var otherConditions = [];
                        _(condition.conditions).each(function(c) {
                            if(c.type == 'condition' && c.operator == 'not_null') {
                                notNullCondition = c;
                            } else if (c.type == 'condition') {
                                otherConditions.push(c);
                            } else {
                                _(c.conditions).each(function(sub) {
                                    otherConditions.push(sub);
                                });
                            }
                        });
                        var filters = [];
                        if(otherConditions) {
                            _(otherConditions).each(function(c) {
                                filters.push(generateFilter(c));
                            });
                        }
                        if(notNullCondition) {
                            filters.push({
                                notNull: true
                            });
                        }
                        return [condition.generatedFrom.id, filters];
                    }
                }
            }).object().value();
        },
        toConditions: function(filters) {
            var conditions = _(filters).map(function(filter, id) {
                var notNull = false;
                var positive = [];
                var negative = [];
                _(filter).each(function(f) {
                    if(f.notNull) {
                        notNull = true;
                    } else if(f.not || f.aggregatedOperator == 'none') {
                        negative.push(f);
                    } else {
                        positive.push(f);
                    }
                });
                var result = {
                    generatedFrom: {
                        id: id
                    },
                    type: 'group',
                    operator: 'AND',
                    not: false,
                    conditions: []
                };
                if(notNull) {
                    var property = policyPropertyPathToId.toPath(id);
                    
                    result.conditions.push({
                        generatedFrom: {
                            id: id,
                            filter: {
                                notNull: true
                            }
                        },
                        type: 'condition',
                        not: false,
                        property: property,
                        operator: 'not_null',
                        data: {}
                    });
                }
                if(positive.length) {
                    var pCond = {
                        generatedFrom: {
                            id: id
                        },
                        type: 'group',
                        operator: 'OR',
                        not: false,
                        conditions: _(positive).map(function(f) {
                            return generateCondition(id, f);
                        })
                    };
                    if(pCond.conditions.length == 1) {
                        pCond = _(pCond.conditions).first();
                    }
                    result.conditions.push(pCond);
                }
                if(negative.length) {
                    var nCond = {
                        generatedFrom: {
                            id: id
                        },
                        type: 'group',
                        operator: 'AND',
                        not: false,
                        conditions: _(negative).map(function(f) {
                            return generateCondition(id, f);
                        })
                    };
                    if(nCond.conditions.length == 1) {
                        nCond = _(nCond.conditions).first();
                    }
                    result.conditions.push(nCond);
                }
                if(result.conditions.length == 1) {
                    result = _(result.conditions).first();
                }
                return result;
            });
            
            return {
                type: 'group',
                operator: 'AND',
                not: false,
                conditions: conditions
            };
        }
    };
});