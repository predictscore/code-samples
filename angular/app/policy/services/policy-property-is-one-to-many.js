var m = angular.module('policy');

m.factory('policyPropertyIsOneToMany', function(policyPropertyToNodes) {
    return function(properties, path) {
        var nodes = policyPropertyToNodes(path);
        return !!_(nodes).find(function(node) {
            return properties[node].oneToMany;
        });
    };
});