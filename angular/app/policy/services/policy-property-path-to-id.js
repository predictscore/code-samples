var m = angular.module('policy');

m.factory('policyPropertyPathToId', function() {
    return {
        toId: function(path) {
            return path.split('.').join('^');
        },
        toPath: function(id) {
            return id.split('^').join('.');
        }
    };
});