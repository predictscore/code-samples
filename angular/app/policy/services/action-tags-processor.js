var m = angular.module('policy');

//This file contains things that should be performed on backend.
//Don't really know why frontend need to do that.
m.factory('actionTagsProcessor', function() {
    var actionTagProcessor = {
        replaceTags: function(action) {
            _(action.attributes).each(function(attr, id) {
                action.attributes[id] = _(action.tags).reduce(function(memo, propertyId, tag) {
                    if(_(memo).isString()) {
                        return memo.split('{' + tag + '}').join('{' + propertyId + '}');
                    }
                    return memo;
                }, attr);
            });
        },
        generateTags: function(action, data) {
            data = data || action.attributes;
            var properties = actionTagProcessor.getProperties(data);

            action.tags = action.tags || {};
            _(properties).each(function(propertyId, idx) {
                action.tags['tag' + (idx + 1)] = propertyId;
            });

            _(data).each(function(param, id) {
                action.attributes[id] = _(properties).reduce(function(memo, property, idx) {
                    if(_(memo).isString()) {
                        return memo.split('{' + property + '}').join('{tag' + (idx + 1) + '}');
                    }
                    return memo;
                }, param);
            });
        },
        getProperties: function (attributes) {
            var properties = [];
            _(attributes).each(function(param) {
                if(_(param).isString()) {
                    var start = 0;
                    while(true) {
                        var open = param.indexOf('{', start);
                        if(open == -1) {
                            break;
                        }
                        var close = param.indexOf('}', open);
                        if(close == -1) {
                            break;
                        }
                        start = close;
                        var propertyId = param.slice(open + 1, close);
                        properties.push(propertyId);
                    }
                }
            });
            return _(properties).uniq();
        }
    };
    return actionTagProcessor;
});