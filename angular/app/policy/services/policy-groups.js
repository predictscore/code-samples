var m = angular.module('policy');

m.factory('policyGroups', function() {
    var notTags = [];
    var policyGroups = [{
        label: 'DLP',
        description: 'Description of DLP group',
        tags: ['dlp']
    }, {
        label: 'Malicious',
        description: 'Description of Malicious group',
        tags: ['malicious']
    }, {
        label: 'Usage',
        description: 'Description of Usage group',
        tags: ['usage']
    }, {
        label: 'Shadow-IT',
        description: 'Description of Shadow-IT group',
        tags: ['shadow-it']
    }, {
        label: 'User queries',
        description: 'Description of User queries group',
        notTags: notTags
    }];
    
    _(policyGroups).each(function(group) {
        _(group.tags).each(function(tag) {
            notTags.push(tag);
        });
    });
    return policyGroups;
});