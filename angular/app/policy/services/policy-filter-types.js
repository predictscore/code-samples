var m = angular.module('policy');

m.factory('policyFilterTypes', function () {
    var typesMap = {
        'TEXT': 'text',
        'BOOLEAN': 'boolean',
        'INT': 'number',
        'FLOAT': 'float',
        'DATETIME': 'date'
    };
    return function(property) {
        if(!_(property).isObject()) {
            return typesMap[property] || 'text';
        }
        var result = typesMap[property.originType] || 'text';
        if(property.type == 'enum' && property.variants) {
            result = 'multiEnum';
        }
        return result;
    };
});
