var m = angular.module('policy');

m.factory('policyPropertyMissingMsg', function(policyPropertyToNodes) {
    return function(properties, fullId) {
        var msg = '';
        _(policyPropertyToNodes(fullId)).find(function (nodeId) {
            var pair = nodeId.split('.');
            if (!properties[nodeId]) {
                var entity = pair[0].split(':')[0];
                msg = 'Entity ' + entity + ' don\'t have property ' + pair[1];
                return true;
            }
            return false;
        });
        return msg;
    };
});