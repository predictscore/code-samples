var m = angular.module('policy');

/*jshint loopfunc: true */
m.factory('remediationProperties', function() {
    return function(originalProperties) {
        return function (propertyType, selectableType) {
            var properties = $.extend(true, {}, originalProperties);
            if (_(propertyType).isUndefined() && _(selectableType).isUndefined()) {
                return properties;
            }
            if(!_(propertyType).isArray()) {
                propertyType = [propertyType];
            }

            properties[0].pointer = properties[0].id;
            properties['.' + properties[0].id] = properties[0];
            properties[0] = {
                id: '',
                sub: ['.' + properties[0].id]
            };

            _(properties).chain().keys().each(function (key) {
                if (properties[key].selectable) {
                    if(selectableType && properties[key].selectable && properties[key].type == selectableType) {
                        //nothing todo
                    } else {
                        delete properties[key];
                    }
                } else if (_(propertyType).contains(properties[key].pointer)) {
                    properties[key].selectable = true;
                }
            });
            var removed = true;
            while (removed) {
                removed = false;
                _(properties).chain().keys().each(function (key) {
                    properties[key].sub = _(properties[key].sub).filter(function (subId) {
                        return _(properties[subId]).isObject();
                    });
                    if (!properties[key].sub.length) {
                        delete properties[key].sub;
                        if (!_(propertyType).contains(properties[key].pointer)) {
                            delete properties[key];
                            removed = true;
                        }
                    }
                });
            }

            function fixTree(id, depth) {
                if(depth === 0) {
                    return false;
                }
                properties[id].sub = _(properties[id].sub).filter(function(subId) {
                    return fixTree(subId, depth - 1);
                });
                if(properties[id].sub && !properties[id].sub.length) {
                    delete properties[id].sub;
                }
                return properties[id].selectable || properties[id].sub;
            }

            fixTree(0, 5);

            if (_(properties[0]).isUndefined()) {
                properties.not_found_menu = {
                    label: 'Not found'
                };
                properties[0] = {
                    sub: ['not_found_menu']
                };
            }

            return properties;
        };
    };
});