var m = angular.module('policy');

m.factory('policyPropertiesFilterConverter', function($q, policyPropertiesFilter) {
    return function (input, args) {
        var deferred = $q.defer();

        input.data._filteredSources = {};
        var baseSources = input.data.sources;
        input.data.sources = function(entityName) {
            if(!input.data.sourcesInfo[entityName]) {
                return undefined;
            }
            if(!input.data._filteredSources[entityName]) {
                input.data._filteredSources[entityName] = policyPropertiesFilter(baseSources(entityName).properties, args.filter);
            }
            return input.data._filteredSources[entityName];
        };

        deferred.resolve($.extend({}, input));
        return deferred.promise;
    };
});