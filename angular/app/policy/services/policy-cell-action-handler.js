var m = angular.module('policy');

m.factory('policyCellActionHandler', function($q, policyPropertyPathToId, policyPropertyParser, policyAllowsDao, policyHeaderOptions) {
    var timeFormat = 'YYYY-MM-DD HH:mm:ss';
    return function($scope) {
        return function(event, data) {
            function updateAdvancedConditions(value, path, property) {
                var conditionData = value;
                var options;
                var operator = 'is';
                
                if(property.type == 'string') {
                    options = {
                        ignoreCase: true
                    };
                } else if(property.type == 'boolean') {
                    if(data.action === 'allow') {
                        data.action = 'filter';
                        conditionData = !value;
                    }
                } else if(property.type == 'enum') {
                    operator = 'in';
                    conditionData = [value];
                } else if(property.type == 'number') {
                    operator = 'equals';
                } else if(property.type == 'date') {
                    operator = 'date_between';
                    conditionData = {
                        first: moment(value).hour(0).minute(0).second(0).millisecond(0).format(timeFormat),
                        second: moment(value).hour(0).minute(0).second(0).millisecond(0).add(1, 'day').format(timeFormat)
                    };
                }
                
                $scope.model.includeCondition.conditions = $scope.model.includeCondition.conditions || [];
                $scope.model.includeCondition.conditions.push({
                    type: 'condition',
                    data: conditionData,
                    data_is_property: false,
                    not: data.action === 'allow',
                    operator: operator,
                    options: options,
                    property: path
                });
            }
            
            function updateSimpleConditions(value, path, property) {
                var headerOptions = policyHeaderOptions();
                var type = 'text';
                var operator = 'is';
                var filterDataValue = value;
                var filterDataLabel;
                var options;
                if(property.type == 'string') {
                    options = {
                        ignoreCase: true
                    };
                } else if(property.type == 'boolean') {
                    if(data.action === 'allow') {
                        data.action = 'filter';
                        filterDataValue = !filterDataValue;
                    }
                    type = 'boolean';
                    filterDataLabel = filterDataValue;
                    var found = _(property.variants).find(function(v) {
                        return v.id == filterDataValue;
                    });
                    if(found) {
                        filterDataLabel = found.label || filterDataLabel;
                    }
                } else if(property.type == 'enum') {
                    type = 'multiEnum';
                    operator = 'in';
                    filterDataValue = [value];
                } else if(property.type == 'number') {
                    type = 'number';
                    operator = 'equals';
                    filterDataValue = value;
                } else if(property.type == 'date') {
                    type = 'date';
                    operator = 'date_between';
                    filterDataValue = {
                        first: moment(value).hour(0).minute(0).second(0).millisecond(0).format(timeFormat),
                        second: moment(value).hour(0).minute(0).second(0).millisecond(0).add(1, 'day').format(timeFormat)
                    };
                }
                var operatorLabel = 'is';
                if(type != 'boolean') {
                    operatorLabel = _(headerOptions[type].operators).find(function(o) {
                        return o.id == operator;
                    }).label;
                }
                
                var filters = [];
                var filterFound = _($scope.getFiltersInfo()).find(function(column) {
                    return column.path == path;
                });
                if(filterFound) {
                    filters = filterFound.columnData; 
                }
                var filter = _(filters).find(function(f) {
                    return f.not && f.operator == 'is' && f.type == 'text' && f.data == value;
                });
                if(!filter) {
                    var not = false;
                    var aggregatedOperator;
                    if(data.action === 'allow') {
                        not = true;
                    } else if(data.action === 'allow-aggregated') {
                        aggregatedOperator = 'none';
                    } else if(data.action === 'filter-aggregated') {
                        aggregatedOperator = 'any';
                    }
                    filter = {
                        data: filterDataValue,
                        dataLabel: filterDataLabel,
                        not: not,
                        aggregatedOperator: aggregatedOperator,
                        options: options,
                        operator: operator,
                        operatorLabel: operatorLabel,
                        type: type
                    };
                    filters.push(filter);
                }
                var filterData = {};
                filterData[data.columnId] = filters;
                $scope.addConditions(data.columnId, filterData);
            }
            
            var text = data.text;
            var value = data.text;
            var path = policyPropertyPathToId.toPath(data.columnId);
            var column = $scope.tableModel.idToColumn[data.columnId];
            if(($scope.internalModel.system || !_(column.link).isUndefined()) && data.action === 'allow') {
                if(!_(column.link).isUndefined()) {
                    var idValue = data.linkValue;
                    //Workaround we missed promise because of linked-text stringify. So we are using internal state of resolved promise
                    //TODO instead of columnsMap use tableModel
                    if(idValue.$$state) {
                        idValue = idValue.$$state.value;
                    }
                    value = _(idValue.split('/')).last();
                
                    text = '#' + JSON.stringify({
                        display: 'link',
                        type: column.link.type,
                        module: column.link.saas,
                        id: value,
                        text: text
                    });
                    
                    path = policyPropertyPathToId.toPath(column.link.id);
                }
                
                policyAllowsDao.create($scope.getActualPolicyId(), text, path, [value]).then(function() {
                    $scope.tableHelper.clearSelection();
                    $scope.tableHelper.reload();
                });
            } else {
                var nodeId = policyPropertyParser(path).nodeId;
                var property = $scope.internalModel.conditionProperties.properties[nodeId];
                
                if($scope.model.advancedConditions) {
                    updateAdvancedConditions(value, path, property);
                    $scope.updatePolicy().then(function() {
                        $scope.tableHelper.clearSelection();
                        $scope.tableHelper.reload();
                    });
                } else {
                    updateSimpleConditions(value, path, property);
                }
            }
        };
    };
});