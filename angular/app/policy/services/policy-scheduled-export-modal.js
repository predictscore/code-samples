var m = angular.module('policy');

m.factory('policyScheduledExportModal', function(modals, sideMenuManager, timeOffsetHelper, timeVariants) {
    return function(scheduled) {
        scheduled = scheduled || {};
        return modals.params([{
            params: [{
                id: 'emails',
                type: 'string',
                label: 'Email this report to',
                data: scheduled.emails || sideMenuManager.currentUser().email,
                validation: {
                    required: true
                }
            }, {
                id: 'frequency',
                type: 'selector',
                label: 'Frequency',
                data: scheduled.frequency || 'none',
                variants: [{
                    id: 'none',
                    label: 'None'
                }, {
                    id: 'monthly',
                    label: 'Monthly'
                }, {
                    id: 'weekly',
                    label: 'Weekly'
                }, {
                    id: 'daily',
                    label: 'Daily'
                }]
            }, {
                id: 'monthlyWeek',
                type: 'selector',
                label: 'On week',
                data: scheduled.monthlyWeek || 1,
                variants: timeVariants.monthWeeks.getVariants(),
                enabled: {
                    param: 'frequency',
                    equal: 'monthly'
                }
            }, {
                id: 'weeklyDay',
                type: 'selector',
                label: 'On day',
                data: scheduled.weeklyDay || 'sunday',
                variants: timeVariants.weekDays.getVariants(),
                enabled: {
                    param: 'frequency',
                    in: ['monthly', 'weekly']
                }
            }, {
                id: 'timeSent',
                type: 'time',
                label: 'Time sent',
                data: scheduled.timeSent || timeOffsetHelper.localToOffset('08:00'),
                enabled: {
                    param: 'frequency',
                    in: ['monthly', 'weekly', 'daily']
                }
            }],
            lastPage: true 
        }], 'Scheduled export');
    };
});