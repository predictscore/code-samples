var m = angular.module('policy');

m.factory('policyColumnsFiltersConverter', function() {
    return function(columnsFilters) {
        return _(columnsFilters).chain().map(function(filter, columnId) {
            if(_(filter).isObject()) {
                var first = '';
                var second = '';
                if(_(filter.data).isObject()) {
                    first = '' + filter.data.first;
                    second = '' + filter.data.second;
                } else {
                    first = '' + filter.data;
                }
                if(filter.type == 'date') {
                    first = moment(first).unix();
                    if(second) {
                        second = moment(second).unix();
                    }
                }
                if(filter.operator == '<') {
                    second = first;
                    first = '';
                }
                if(_(['text', 'boolean']).contains(filter.type)) {
                    filter = first;
                } else {
                    filter = first + '~' + second;
                }
            }
            return [columnId, filter];
        }).object().value();
    };
});