var m = angular.module('policy');

m.factory('tableAllowsActionsBuilder', function(policyAllowsDao, routeHelper, feature) {
    return function(policyId, buttons, actionTypes, allowsTypes, args) {
        function atLeastOneSelected() {
            return args.getSelectedIdsCount() > 0;
        }
        
        buttons.push({
            label: 'Manage Allows...',
            display: 'dropdown-button',
            execute: function() {
                routeHelper.redirectTo('policy-allows', {
                    id: policyId
                });
            }
        });
        
        _(allowsTypes).map(function(value) {
            buttons.push({
                label: value.label,
                active: atLeastOneSelected,
                execute: function() {
                    args.getPathValues(value.path.fullId).then(function(pathValues) {
                        policyAllowsDao.create(policyId, value.label, value.path.fullId, pathValues).then(function() {
                            args.reload();
                        });
                    });
                }
            });
        });

        buttons.push({
            label: 'Exclude lines',
            active: atLeastOneSelected,
            execute: function () {
                args.getSelectedIds().then(function (entityIds) {
                    var path = _(_(entityIds).first()).chain().map(function(value, key) {
                        return key;
                    }).find(function(key) {
                        return key.indexOf('.') == -1;
                    }).value();
                    var pathValues = _(entityIds).map(function(id) {
                        return id[path];
                    });
                    policyAllowsDao.create(policyId, 'Exclude lines', path, pathValues).then(function () {
                        args.reload();
                    });
                    //TODO when exclude specific line api will be done use next instead:
                    //policyAllowsDao.create(policyId, 'Exclude lines', undefined, entityIds).then(function () {
                    //    args.reload();
                    //});
                });
            }
        });
    };
});