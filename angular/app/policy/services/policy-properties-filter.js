var m = angular.module('policy');

m.factory('policyPropertiesFilter', function() {
    return function(properties, filter) {
        var result = $.extend({}, properties);
        if(_(result.properties).isObject()) {
            result.properties = _(result.properties).chain().map(function (property, key) {
                return [key, $.extend(true, {}, property)];
            }).filter(function (property) {
                return property[0] === '0' || property[1][filter];
            }).object().value();

            _(result.properties).each(function(property) {
                if(_(property.sub).isArray()) {
                    property.sub = _(property.sub).filter(function (sub) {
                        return !_(result.properties[sub]).isUndefined();
                    });
                }
            });
        }
        return result;
    };
});