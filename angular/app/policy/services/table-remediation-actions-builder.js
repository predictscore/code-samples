var m = angular.module('policy');

m.factory('tableRemediationActionsBuilder', function(entityTypes, modals, manualActionDao, remediationProperties) {
    return function(buttons, actionTypes, sources, saas, getSelectedIds, args) {
        args = $.extend({
            actionRepeatOptions: {}
        }, args);
        var actionTypesMap = _(actionTypes).chain().map(function(value, key) {
            return [key, _(value).chain().map(function(action) {
                return [action.id, action];
            }).object().value()];
        }).object().value();

        var uniqueActions = {};
        _(actionTypesMap).each(function(actions, entityType) {
            _(actions).each(function(action, actionKey) {
                var uniq = uniqueActions[actionKey];
                if(_(uniq).isUndefined()) {
                    uniq = {
                        entityTypes: [],
                        action: action
                    };
                }
                uniq.entityTypes.push(entityType);
                uniqueActions[actionKey] = uniq;
            });
        });
        function isActive(eTypes, ids) {
            var types = _(ids).chain().map(function(id) {
                return id.entity_type;
            }).uniq().value();
            return types.length == 1 && _(eTypes).contains(entityTypes.toBackend(saas, _(types).first())) ||
                _(eTypes).contains(args.policyEntityType);
        }
        var selectedIds = getSelectedIds();
        _(uniqueActions).each(function(uniq, actionKey) {
            if(args.constEntities && !isActive(uniq.entityTypes, selectedIds)) {
                return;
            }
            buttons.push({
                label: uniq.action.label,
                display: 'dropdown-button',
                entityTypes: uniq.entityTypes,
                active: args.constEntities ? _.constant(true) : function() {
                    return isActive(this.entityTypes, getSelectedIds());
                },
                execute: function(event) {
                    if(event) {
                        event.preventDefault();
                    }
                    
                    var params = _(uniq.action.attributes).map(function(param) {
                            return $.extend(true, {}, param, {
                                data: ((args.defaultData || {})[actionKey] || {})[param.id]
                            });
                        });
                    if(args.predefined[uniq.action.id]) {
                        var predefined = args.predefined[uniq.action.id]();
                        _(params).each(function(param) {
                            if(predefined[param.id]) {
                                param.data = predefined[param.id];
                            }
                        });
                    }
                    
                    if(getSelectedIds().length === 0) {
                        modals.params([{
                            params: params,
                            lastPage: true,
                            getProperties: _.memoize(remediationProperties(sources(args.policyEntityType).properties)),
                            actionRepeatOptions: args.actionRepeatOptions[actionKey]
                        }], uniq.action.label, uniq.action.size).then(function(data) {
                            return modals.confirm('<strong>WARNING: </strong>This action will affect <strong>all</strong> results of this query.<br>Proceed with caution.').then(function() {
                                var body = {
                                    name: actionKey,
                                    attributes: data
                                };
                                
                                manualActionDao.createForPolicy(args.policyId, args.policyEntityType, body);
                            });
                        });
                    } else {
                        var ids = getSelectedIds();
                        var type = _(ids).first().entity_type;
                        var entities = _(ids).map(function(id) {
                            return id.entity_id;
                        });
                        
                        modals.params([{
                            params: params,
                            lastPage: true,
                            getProperties: _.memoize(remediationProperties(sources(entityTypes.toBackend(saas, type)).properties)),
                            actionRepeatOptions: args.actionRepeatOptions[actionKey]
                        }], uniq.action.label, uniq.action.size).then(function(data) {
                            var confirmMessage = 'This action will affect the following items:<div class="action-confirm-items-list">';
                            _(ids).each(function(id) {
                                confirmMessage += '<br>' + id.label;
                            });
                            confirmMessage += '</div>';
                            modals.confirm(confirmMessage).then(function() {
                                var body = {
                                    name: actionKey,
                                    attributes: data
                                };
                                
                                manualActionDao.create(entityTypes.toBackend(saas, type), entities, body);
                            });
                        });
                    }
                }
            });
        });
        
        if(_(buttons).isEmpty()) {
            buttons.push({
                label: 'None selected',
                display: 'dropdown-button'
            });
        }
    };
});