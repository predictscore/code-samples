var m = angular.module('policy');

m.factory('policyHeaderOptions', function () {
    return function(options) {
        options = options || {};
        return {
            multiEnum: {
                notAllowed: options.notAllowed,
                aggregatedOperatorAllowed: options.aggregatedOperatorAllowed,
                operators: [{
                    id: 'in',
                    label: 'is one of',
                    type: 'multiEnum'
                }, {
                    id: 'is_null',
                    label: 'is empty',
                    type: 'none'
                }],
                notNullAllowed: true
            },
            enum: {
                notNullAllowed: true
            },
            boolean: {
                type: 'boolean',
                input: 'none',
                operatorId: 'is',
                operatorLabel: 'is',
                operators: [{
                    id: 'false',
                    label: 'false',
                    type: 'none',
                    data: false
                }, {
                    id: 'true',
                    label: 'true',
                    type: 'none',
                    data: true
                }, {
                    id: 'is_null',
                    label: 'Is empty',
                    type: 'none',
                    data: null
                }],
                notNullAllowed: true
            },
            number: {
                notAllowed: options.notAllowed,
                aggregatedOperatorAllowed: options.aggregatedOperatorAllowed,
                multiple: true,
                operators: [{
                    id: 'greater_than',
                    label: '>',
                    type: 'single'
                }, {
                    id: 'less_than',
                    label: '<',
                    type: 'single'
                }, {
                    id: 'equals',
                    label: '=',
                    type: 'single'
                }, {
                    id: 'between',
                    label: 'between',
                    type: 'double'
                }, {
                    id: 'is_null',
                    label: 'is empty',
                    type: 'none'
                }],
                notNullAllowed: true
            },
            float: {
                notAllowed: options.notAllowed,
                aggregatedOperatorAllowed: options.aggregatedOperatorAllowed,
                multiple: true,
                operators: [{
                    id: 'greater_than',
                    label: '>',
                    type: 'single'
                }, {
                    id: 'less_than',
                    label: '<',
                    type: 'single'
                }, {
                    id: 'equals',
                    label: '=',
                    type: 'single'
                }, {
                    id: 'between',
                    label: 'between',
                    type: 'double'
                }, {
                    id: 'is_null',
                    label: 'is empty',
                    type: 'none'
                }],
                notNullAllowed: true
            },
            date: {
                notAllowed: options.notAllowed,
                aggregatedOperatorAllowed: options.aggregatedOperatorAllowed,
                multiple: true,
                operators: [{
                    id: 'date_after',
                    label: 'after',
                    type: 'single'
                }, {
                    id: 'date_before',
                    label: 'before',
                    type: 'single'
                }, {
                    id: 'date_between',
                    label: 'between',
                    type: 'double'
                }, {
                    id: 'from',
                    label: 'newer than',
                    type: 'time-shift'
                }, {
                    id: 'older_than',
                    label: 'older than',
                    type: 'time-shift'
                }, {
                    id: 'is_null',
                    label: 'is empty',
                    type: 'none'
                }],
                notNullAllowed: true
            },
            text: {
                notAllowed: options.notAllowed,
                aggregatedOperatorAllowed: options.aggregatedOperatorAllowed,
                multiple: true,
                operators: [{
                    id: 'is',
                    label: 'is',
                    type: 'single',
                    "options": [
                        {
                            "id": "ignoreCase",
                            "tooltip": "Ignore case",
                            "class": "fa fa-font",
                            "defaultState": true
                        }
                    ]
                }, {
                    id: 'is_not',
                    label: 'is not',
                    type: 'single',
                    aggregatedOnly: true,
                    "options": [
                        {
                            "id": "ignoreCase",
                            "tooltip": "Ignore case",
                            "class": "fa fa-font",
                            "defaultState": true
                        }
                    ]
                }, {
                    id: 'contains',
                    label: 'contains',
                    type: 'single',
                    "options": [
                        {
                            "id": "ignoreCase",
                            "tooltip": "Ignore case",
                            "class": "fa fa-font",
                            "defaultState": true
                        }
                    ]
                }, {
                    id: 'not_contains',
                    label: 'not contains',
                    type: 'single',
                    aggregatedOnly: true,
                    "options": [
                        {
                            "id": "ignoreCase",
                            "tooltip": "Ignore case",
                            "class": "fa fa-font",
                            "defaultState": true
                        }
                    ]
                }, {
                    id: 'startsWith',
                    label: 'starts with',
                    type: 'single',
                    "options": [
                        {
                            "id": "ignoreCase",
                            "tooltip": "Ignore case",
                            "class": "fa fa-font",
                            "defaultState": true
                        }
                    ]
                }, {
                    id: 'regexp',
                    label: 'regexp',
                    type: 'single',
                    "options": [
                        {
                            "id": "ignoreCase",
                            "tooltip": "Ignore case",
                            "class": "fa fa-font",
                            "defaultState": true
                        }
                    ]
                }, {
                    id: 'is_null',
                    label: 'is empty',
                    type: 'none'
                }],
                notNullAllowed: true
            }
        };
    };
});
