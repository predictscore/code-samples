var m = angular.module('policy');

m.factory('conditionPropertyTypes', function(feature) {
    return function() {
        var propertyTypes = {
            "number": [{
                "id": "equals",
                "label": "=",
                "type": "single"
            }, {
                "id": "greater_than",
                "label": ">",
                "type": "single"
            }, {
                "id": "less_than",
                "label": "<",
                "type": "single"
            }, {
                "id": "between",
                "label": "between",
                "type": "double",
                "firstLabel": "",
                "secondLabel": "and",
                "options": [
                    {
                        "id": "leftInclusive",
                        "label": "Left inclusive",
                        "type": "flag",
                        "defaultState": false
                    },
                    {
                        "id": "rightInclusive",
                        "label": "Right inclusive",
                        "type": "flag",
                        "defaultState": false
                    }
                ]
            }, {
                "id": "is_null",
                "label": "empty",
                "type": "none"
            }, {
                "id": "not_null",
                "label": "Not empty",
                "type": "none"
            }],
            "float": [{
                "id": "equals",
                "label": "=",
                "type": "single"
            }, {
                "id": "greater_than",
                "label": ">",
                "type": "single"
            }, {
                "id": "less_than",
                "label": "<",
                "type": "single"
            }, {
                "id": "between",
                "label": "between",
                "type": "double",
                "firstLabel": "",
                "secondLabel": "and",
                "options": [
                    {
                        "id": "leftInclusive",
                        "label": "Left inclusive",
                        "type": "flag",
                        "defaultState": false
                    },
                    {
                        "id": "rightInclusive",
                        "label": "Right inclusive",
                        "type": "flag",
                        "defaultState": false
                    }
                ]
            }, {
                "id": "is_null",
                "label": "empty",
                "type": "none"
            }, {
                "id": "not_null",
                "label": "Not empty",
                "type": "none"
            }],
            "string": [{
                "id": "is",
                "label": "is",
                "type": "single",
                "options": [
                    {
                        "id": "ignoreCase",
                        "label": "Ignore case",
                        "type": "flag",
                        "defaultState": true
                    }
                ]
            }, {
                "id": "contains",
                "label": "contains",
                "type": "single",
                "options": [
                    {
                        "id": "ignoreCase",
                        "label": "Ignore case",
                        "type": "flag",
                        "defaultState": true
                    }
                ]
            }, {
                "id": "startsWith",
                "label": "starts with",
                "type": "single",
                "options": [
                    {
                        "id": "ignoreCase",
                        "label": "Ignore case",
                        "type": "flag",
                        "defaultState": true
                    }
                ]
            }, {
                "id": "regexp",
                "label": "regexp",
                "type": "single"
            }, {
                "id": "is_null",
                "label": "empty",
                "type": "none"
            }, {
                "id": "not_null",
                "label": "Not empty",
                "type": "none"
            }],
            "ipAddr": [{
                "id": "is",
                "label": "is",
                "type": "single"
            }, {
                "id": "contains",
                "label": "contains",
                "type": "single"
            }, {
                "id": "startsWith",
                "label": "starts with",
                "type": "single"
            }, {
                "id": "regexp",
                "label": "regexp",
                "type": "single"
            }, {
                "id": "ip_between",
                "label": "between",
                "type": "double",
                "firstLabel": "",
                "secondLabel": "and",
                "options": [
                    {
                        "id": "leftInclusive",
                        "label": "Left inclusive",
                        "type": "flag",
                        "defaultState": false
                    },
                    {
                        "id": "rightInclusive",
                        "label": "Right inclusive",
                        "type": "flag",
                        "defaultState": false
                    }
                ]
            }, {
                "id": "is_null",
                "label": "empty",
                "type": "none"
            }, {
                "id": "not_null",
                "label": "Not empty",
                "type": "none"
            }],
            "date": [{
                "id": "date_after",
                "label": "After",
                "type": "single-date"
            }, {
                "id": "date_before",
                "label": "Before",
                "type": "single-date"
            }, {
                "id": "date_between",
                "label": "Between",
                "type": "double-date",
                "secondLabel": 'and'
            }, {
                "id": "from",
                "label": "Newer than",
                "type": "time-shift"
            }, {
                "id": "older_than",
                "label": "Older than",
                "type": "time-shift"
            }, {
                "id": "is_null",
                "label": "empty",
                "type": "none"
            }, {
                "id": "not_null",
                "label": "Not empty",
                "type": "none"
            }],
            "enum": [{
                "id": "in",
                "label": "is one of",
                "type": "list"
            }, {
                "id": "is_null",
                "label": "empty",
                "type": "none"
            }, {
                "id": "not_null",
                "label": "Not empty",
                "type": "none"
            }],
            "policies": [{
                "id": "policy_match",
                "label": "Match",
                "type": "selector"
            }, {
                "id": "policy_unmatch",
                "label": "Unmatch",
                "type": "selector"
            }],
            "boolean": [{
                "id": "bool",
                "label": "is",
                "hidden": "true",
                "type": "selector",
                "defOptions": [{
                    "id": true,
                    "label": "is true"
                }, {
                    "id": false,
                    "label": "is false"
                }, {
                    "id": null,
                    "label": "is empty"
                }]
            }]
        };

        return propertyTypes;
    };
});