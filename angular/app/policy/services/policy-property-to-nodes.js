var m = angular.module('policy');

m.factory('policyPropertyToNodes', function() {
    return function(fullId) {
        return _(fullId.split('.')).chain().reduce(function(memo, part) {
            if(memo.length === 0) {
                memo.push([part]);
            } else if(memo.length == 1 && memo[0].length == 1) {
                memo[0].push(part);
            } else {
                memo.push([_(memo).last()[1], part]);
            }
            return memo;
        }, []).map(function(pair) {
            return pair.join('.');
        }).value();
    };
});