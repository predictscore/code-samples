var m = angular.module('policy');

m.factory('policySeverityTypes', function() {
    var types = [{
        label: 'None',
        id: 'none',
        dashboardColor: '#16a085'
    }, {
        label: 'Low',
        id: 'low',
        dashboardColor: '#9bbb59'
    }, {
        label: 'Med',
        id: 'med',
        dashboardColor: '#f39c12'
    }, {
        label: 'High',
        id: 'high',
        dashboardColor: '#f35428'
    }];
    
    return {
        asVariants: function() {
            return types;
        },
        idToLabel: function(id) {
            id = id || 'none';
            return (_(types).find(function(t) {
                return t.id == id;
            }) || {}).label || id;
        },
        idFixed: function (id) {
            return id || 'none';
        }
    };
});