var m = angular.module('policy');

m.controller('PolicyCreateController', function($scope, $routeParams, modals, routeHelper, policyDao, objectTypeDao, feature, $q) {
    $scope.internalModel = {
        saasList: {},
        sources: {},
        propertyTypes: {},
        properties: {}
    };

    if(feature.policyTemplates) {
        $scope.policyOptionsPanel = {
            title: "Query options",
            'class': 'light white-body white-header no-margin',
            checkbox: {
                value: false,
                text: 'Advanced',
                'class': 'advanced-checkbox display-none'
            }
        };

        $scope.$watchCollection(function() {
            return [$scope.model.templateName, $scope.policyOptionsPanel.checkbox.value];
        }, function() {
            if($scope.policyOptionsPanel.checkbox.value) {
                $scope.policyOptionsPanel.title = "Policy options";
            } else if(_($scope.model.templateName).isUndefined()) {
                $scope.policyOptionsPanel.title = "Select template";
            } else {
                $scope.policyOptionsPanel.title = "Query template options";
            }
        });
    } else {
        $scope.policyOptionsPanel = {
            title: "Query options",
            'class': 'light white-body white-header no-margin',
            checkbox: {
                value: true,
                text: 'Advanced',
                'class': 'advanced-checkbox display-none'
            }
        };
    }

    $scope.model = {};
    $scope.$watch('policyOptionsPanel.checkbox.value', function() {
        $scope.model = {
            includeCondition: {},
            excludeCondition: {},
            actions: [],
            columns: {}
        };
    });

    objectTypeDao.retrieve(undefined, true).then(function(response) {
        $scope.internalModel.saasList = response.data.saasList;
        $scope.internalModel.sources = response.data.sources;
        $scope.internalModel.sourcesInfo = response.data.sourcesInfo;
        $scope.internalModel.propertyTypes = response.data.propertyTypes;
        $scope.internalModel.actionTypes = response.data.actionTypes;
        $scope.internalModel.loaded = true;
    });

    $scope.isSaveVisible = function() {
        if(!$scope.policyOptionsPanel.checkbox.value) {
            return !_($scope.model.entityType).isUndefined();
        }
        return true;
    };

    $scope.generateBody = function() {
        var saas = $scope.model.saas;
        var model = $.extend({
            name: '',
            'alert': false
        }, _($scope.model).omit('shown_columns'));
        delete model.saas;
        return {
            "policy_status": "running",
            "policy_data": model,
            "context": {
                "saas": saas
            },
            "hidden": true,
            "temporary": true
        };
    };

    $scope.save = function() {
        if(_($scope.model.entityType).isUndefined()) {
            modals.alert('You must select object type before you can continue');
            return;
        }

        policyDao.create($scope.generateBody()).then(function(response) {
            var promise = $q.when();
            if(feature.newPolicyApi && _($scope.model.shown_columns).isObject()) {
                promise = policyDao.updateColumns(response.data.policy_id, {
                    columns: $scope.model.shown_columns
                });
            }
            promise.then(function() {
                routeHelper.redirectTo('policy-edit', {
                    id: response.data.policy_id,
                    isNew: true
                });
            });
        });
    };
    
    $scope.$on('auto-save', function() {
        $scope.internalModel.loaded = false;
        $scope.save();
    });
});