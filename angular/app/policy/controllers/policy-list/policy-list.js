var m = angular.module('policy');

m.controller('PolicyListController', function($scope, $q, tablePaginationHelper, routeHelper, $routeParams, path, policyDao, objectTypeDao, columnsHelper, $timeout, widgetSettings, workingIndicatorHelper, policySeverityTypes, policyScheduledExportModal, sideMenuManager) {
    workingIndicatorHelper.init($scope);
    function changeStatus(policyIds, status) {
        policyDao.changeBatchStatus(policyIds, status).then(function() {
            $scope.tableHelper.clearSelection();
            $scope.tableHelper.reload();
        });
    }

    function isActionActive(state, singleSelection) {
        return function() {
            var selected = $scope.tableHelper.getSelected();
            if (singleSelection && selected.length > 1 || selected.length === 0) {
                return false;
            }
            if(_(state).isUndefined()) {
                return true;
            }
            var idx = columnsHelper.getIdxById($scope.options.columns, 'policy_status');
            return _(selected).some(function(s) {
                return s[idx].originalText !== state;
            });
        };
    }

    var policyListWidgetUUID = '523cd709-a4b5-4857-bfa0-06b280b0686e';
    var showPausedPoliciesKey = 'show-paused-policies';
    var showHiddenPoliciesKey = 'show-hidden-policies';
    var showTemporaryPoliciesKey = 'show-temporary-policies';
    $scope.policyListPanel = {
        uuid: policyListWidgetUUID,
        wrapper: {
            title: "Queries",
            'class': 'light white-body white-header',
            filters: [{
                type: 'flag',
                local: true,
                state: false,
                text: 'Show paused queries',
                value: showPausedPoliciesKey
            }, {
                type: 'flag',
                local: true,
                state: false,
                text: 'Show system queries',
                value: showHiddenPoliciesKey
            }, {
                type: 'flag',
                local: true,
                state: false,
                text: 'Show temporary queries',
                value: showTemporaryPoliciesKey
            }]
        }
    };

    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 20,
        pagination: {
            page: 1,
            ordering: {},
            filter: ''
        },
        saveState: true,
        disableTop: true,
        showWorkingIndicator: true,
        dropdownHeader: true,
        headerOptions: {
            enum: {
                multiple: true
            }
        },
        actions: [{
            label: 'Pause',
            active: isActionActive('pause'),
            execute: function() {
                changeStatus($scope.tableHelper.getSelectedIds(), 'pause');
            }
        }, {
            label: 'Start',
            active: isActionActive('running'),
            execute: function() {
                changeStatus($scope.tableHelper.getSelectedIds(), 'running');
            }
        }, {
            label: 'Delete',
            active: isActionActive(),
            execute: function() {
                changeStatus($scope.tableHelper.getSelectedIds(), 'mark_for_deletion');
            }
        }, {
            label: 'Export',
            active: isActionActive(),
            execute: function() {
                $q.all(_($scope.tableHelper.getSelectedIds()).map(function(policyId) {
                    return policyDao.retrieve(policyId);
                })).then(function(responses) {
                    var policies = _(responses).map(function(response) {
                        var policy = response.data;
                        delete policy.policy_id;
                        return policy;
                    });
                    var blob = new Blob([JSON.stringify(policies)], {type: "application/json;charset=utf-8"});
                    var name = 'policies_export';
                    if(policies.length == 1) {
                        name = _(policies).first().policy_name;
                    }
                    saveAs(blob, name + '.json');
                });
            }
        }, {
            label: 'Add new query',
            display: 'button',
            execute: function() {
                routeHelper.redirectTo('policy-create');
            }
        }, {
            label: 'Import a query',
            display: 'button',
            execute: function() {
                $timeout(function() {
                    $('#import-file').trigger('click');
                });
            }
        }],
        filter: function(row) {
            var idx = columnsHelper.getIdxById($scope.options.columns, 'policy_status');
            return row[idx].originalText !== 'mark_for_deletion';
        }
    };

    $scope.importFileReadComplete = function(content) {
        try {
            var policies = JSON.parse(content);
            if(!_(policies).isArray()) {
                policies = [policies];
            }
            $q.all(_(policies).map(function(policy) {
                return policyDao.create(policy);
            })).then(function(responses) {
                if(responses.length == 1) {
                    routeHelper.redirectTo('policy-edit', {id: _(responses).first().data.policy_id});
                } else {
                    $scope.tableHelper.reload();
                }
            });
        } catch(e) {
            alert('Incorrect file');
        }
    };

    $scope.$watchCollection(function() {
        return [
            widgetSettings.param(policyListWidgetUUID, showPausedPoliciesKey),
            widgetSettings.param(policyListWidgetUUID, showHiddenPoliciesKey),
            widgetSettings.param(policyListWidgetUUID, showTemporaryPoliciesKey)
        ];
    }, function(newVal, oldVal) {
        if(newVal != oldVal && !_($scope.tableHelper).isUndefined()) {
            $scope.tableHelper.clearSelection();
            $scope.tableHelper.reload();
        }
    });

    $q.all([
        path('policy').ctrl('policy-list').json('list-conf').retrieve(),
        objectTypeDao.retrieve(undefined, true),
        policyDao.listFacets()
    ]).then(function(responses) {
        $scope.options = responses[0].data;
        $scope.sources = responses[1].data.sources;
        $scope.sourcesInfo = responses[1].data.sourcesInfo;
        $scope.saasList = responses[1].data.saasList;
        $scope.facets = responses[2].data;

        $scope.actionLabels = {};
        _(responses[1].data.actionTypes).each(function (arr) {
            _(arr).each(function (action) {
                $scope.actionLabels[action.id] = action.label;
            });
        });

        function setVariants(columnId, variants) {
            _($scope.options.columns).find(function(column) {
                return column.id == columnId;
            }).property = {
                variants: variants
            };
        }
        
        setVariants('severity', policySeverityTypes.asVariants());
        setVariants('context.saas', _($scope.saasList).map(function(saas, id) {
            return {
                id: id,
                label: saas.label
            };
        }));
        setVariants('tags', _($scope.facets.tags).map(function(tag) {
            return {
                id: tag,
                label: tag
            };
        }));
        setVariants('sec_apps', _($scope.facets.sec_apps).map(function(sec_app) {
            return {
                id: sec_app,
                label: sec_app
            };
        }));
        setVariants('policy_data.actions', _($scope.facets.actions).map(function(action) {
            return {
                id: action,
                label: $scope.actionLabels[action]
            };
        }));

        setVariants('entity_type', _($scope.sourcesInfo).chain().map(function(entity, id) {
            var saasLabel = 'Unknown Saas';
            if($scope.saasList[entity.saas]) {
                saasLabel = $scope.saasList[entity.saas].label;
            }
            return {
                id: id,
                label: saasLabel + ' - ' + entity.label
            };
        }).sortBy('label').value());

        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
            var filterParams = _(args.columnsFilters).chain().map(function(value, name) {
                var first = _(value).first();
                if(first.type == 'text' && first.operator == 'contains') {
                    return [name, first.data];
                }
                if(first.type == 'enum' && first.operator == 'is') {
                    return [name, _(value).map(function(v) {
                        return v.data;
                    }).join(',')];
                }
                if (first.type === 'boolean' && first.operator === 'is') {
                    return [name, first.data];
                }
                return null;
            }).compact().object().value();
            var resultTags = '';
            if(filterParams.filterBy_tags) {
                resultTags += filterParams.filterBy_tags;
            }
            if(filterParams.filterBy_tags && $routeParams.tags) {
                resultTags += ',';
            }
            if($routeParams.tags) {
                resultTags += $routeParams.tags;
            }
            filterParams.filterBy_tags = resultTags;
            filterParams.filterBY_not_tags = $routeParams.not_tags;
            var showPaused = widgetSettings.param(policyListWidgetUUID, showPausedPoliciesKey);
            var showHidden = widgetSettings.param(policyListWidgetUUID, showHiddenPoliciesKey);
            var showTemporary = widgetSettings.param(policyListWidgetUUID, showTemporaryPoliciesKey);
            filterParams.keywords = _(args.filter.split(/\s+/)).filter(function (keyword) {
                return keyword;
            }).join(',');
            return policyDao.list($scope.options.columns, $scope.sourcesInfo, $scope.actionLabels, !showPaused, !showHidden, !showTemporary)
                .params(filterParams)
                .pagination(args.offset, args.limit)
                .order(args.ordering.columnName, args.ordering.order);
        }, function(tableModel) {
            $scope.tableModel = tableModel;
        });
        
        $scope.$on('edit-scheduled-export', function(event, data) {
            var policyId = data.policyId;
            policyDao.retrieve(policyId).then(function(response) {
                var policy = response.data;
                policyScheduledExportModal(policy.scheduled).then(function(data) {
                    policy.scheduled = data;
                    policyDao.update(policyId, policy).then(function() {
                        $scope.tableHelper.reload();
                    }); 
                });
            });
        });
        
        $scope.$on('remove-scheduled-export', function(event, data) {
            var policyId = data.policyId;
            policyDao.retrieve(policyId).then(function(response) {
                var policy = response.data;
                policy.scheduled = {
                    frequency: 'none'
                };
                policyDao.update(policyId, policy).then(function() {
                    $scope.tableHelper.reload();
                });
            });
        });
    });
    
    if($routeParams.tags && !$routeParams.not_tags) {
        sideMenuManager.treeNavigationPath(['analytics', _($routeParams.tags.split(',')).first()]);
    } else if(!$routeParams.tags && $routeParams.not_tags) {
        sideMenuManager.treeNavigationPath(['analytics', 'user']);
    } else {
        sideMenuManager.treeNavigationPath(['analytics']);
    }
});