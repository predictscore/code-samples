var m = angular.module('policy');

m.controller('PolicyRootCauseViewController', function ($scope, $routeParams, policyDao, path, policyReportDao, tablePaginationHelper, $q, objectTypeDao, columnsHelper, enchantDao, tableRemediationActionsBuilder, $route, policyAllowsDao, tableAllowsActionsBuilder) {
    alert('You are on deprecated policy root cause viewer');
    var module = $routeParams.module;
    var internalName = $routeParams.id;

    $scope.internalModel = {};
    $q.all([
        path('policy').ctrl('policy-root-cause-view').json(module + '/' + internalName).retrieve(),
        objectTypeDao.retrieve(),
        policyDao.retrieveByInternalName(internalName)
    ]).then(function(responses) {
        $scope.options = responses[0].data;
        $scope.properties = responses[1].data;
        $scope.policy = responses[2].data;
        $scope.policyId = $scope.policy.policy_id;
        $scope.internalModel.saasList = $scope.properties.saasList;
        $scope.internalModel.sources = $scope.properties.sources;
        $scope.internalModel.allSources = $scope.properties.allSources;
        $scope.internalModel.propertyTypes = $scope.properties.propertyTypes;
        $scope.internalModel.actionTypes = $scope.properties.actionTypes;
        $scope.internalModel.dataUrls = $scope.properties.dataUrls;

        $scope.tableOptions = {
            pagesAround: 2,
            pageSize: 20,
            pagination: {
                page: 1,
                ordering: {},
                filter: ''
            },
            disableTop: true,
            saveState: true,
            toggleState: 'Match',
            actions: []
        };

        tableAllowsActionsBuilder($scope.policyId, $scope.tableOptions, $scope.internalModel.actionTypes, $scope.policy.policy_data.excludedBy, {
            getSelectedIdsCount: function() {
                return $scope.tableHelper.getSelectedIds().length;
            },
            getSelectedIds: function() {
                var selectedIds = $scope.tableHelper.getSelectedIds();
                var primaryIdx = columnsHelper.getPrimaryIdx($scope.options.columns);
                return $q.all(_(selectedIds).map(function(id) {
                    if(id.entity_type == 'file') {
                        return $q.when([id.entity_id]);
                    }
                    return enchantDao.dataEntitiesByRootCause($scope.options.columns, id.entity_id, $scope.internalModel.actionTypes).converter(function(input) {
                        return $q.when(_(input.data.data).map(function(row) {
                            return row[primaryIdx].text.entity_id;
                        }));
                    });
                })).then(function(responses) {
                    return $q.when(_(responses).chain().flatten().uniq().value());
                });
            },
            getSelectedDomains: function() {
                var selected = $scope.tableHelper.getSelected();
                var primaryIdx = columnsHelper.getPrimaryIdx($scope.options.columns);
                var domainIdx = columnsHelper.getIdxById($scope.options.columns, 'domain');

                return $q.all(_(selected).map(function(row) {
                    if(row[primaryIdx].text.entity_type == 'file') {
                        return $q.when([row[domainIdx].originalText]);
                    }
                    return enchantDao.dataEntitiesByRootCause($scope.options.columns, id.entity_id, $scope.internalModel.actionTypes).converter(function(input) {

                        return $q.when(_(input.data.data).map(function(row) {
                            return row[domainIdx].originalText;
                        }));
                    });
                })).then(function(responses) {
                    return $q.when(_(responses).chain().flatten().uniq().value());
                });
            },
            reload: function() {
                $scope.tableHelper.reload();
            }
        });

        tableRemediationActionsBuilder($scope.tableOptions, $scope.internalModel.actionTypes, $scope.internalModel.sources, 'google_drive', function() {
            return $scope.tableHelper.getSelectedIds();
        }, $scope.options.remediationActionBuilderArgs);

        $scope.resultsPanel = {
            title: 'Loading...',
            'class': 'avanan white-body single-widget',
            filters: [{
                type: 'flag',
                local: true,
                state: false,
                text: 'Show fixed',
                value: 'showFixed'
            }]
        };

        $route.current.title = '#' + JSON.stringify({
            display: 'html',
            text: '<strong>' + $scope.options.options.title + '</strong>'
        });

        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, {
            isExpandable: function(row) {
                var idx = columnsHelper.getIdxById($scope.options.columns, 'is_expandable');
                return row[idx].text;
            },
            expand: function(row, filter) {
                var primaryIdx = columnsHelper.getPrimaryIdx($scope.options.columns);
                var rootCause = row[primaryIdx].text.entity_id;
                return enchantDao.dataEntitiesByRootCause($scope.options.columns, $scope.policyId, rootCause, $scope.internalModel.actionTypes, {
                    showFiles: true,
                    showFolders: true
                });
            },
            retrieve: function (args) {
                return policyReportDao.dataPolicyRootCauses($scope.options.options.policyType, $scope.policyId, $scope.options.options.rootCauseType)
                    .pagination(args.offset, args.limit)
                    .filter(args.filter)
                    .order(args.ordering.columnName, args.ordering.order)
                    .converter(function(input) {
                        var entityIdOf = function (row) {
                            return row[_($scope.options.options.entityIdColumns).find(function (entityIdColumn) {
                                    return row[entityIdColumn];
                                })];
                        };

                        var filter = function (row) {
                            return _(entityIdOf(row)).isString();
                        };
                        var ids = _(input.data.rows).chain().filter(filter).map(function (row) {
                            return entityIdOf(row);
                        }).value();
                        var idsCounts = _(input.data.rows).chain().filter(filter).map(function (row) {
                            return [entityIdOf(row), row.count];
                        }).object().value();
                        var idsExpandable = _(input.data.rows).chain().filter(filter).map(function (row) {
                            return [entityIdOf(row), row.is_expandable];
                        }).object().value();

                        return enchantDao.dataEntitiesByIds($scope.options.columns, ids, $scope.internalModel.actionTypes, idsCounts, idsExpandable).converter(function(cInput) {
                            cInput.data.pagination.current = args.offset / $scope.tableOptions.pageSize + 1;
                            cInput.data.pagination.total = input.data.total_rows;
                            return $q.when(cInput);
                        });
                    });
            }
        }, function (tableModel) {
            $scope.tableModel = tableModel;
            $scope.resultsPanel.title = $scope.tableHelper.getTotal() + ' ' + $scope.options.options.title + ' Root causes';
        });

        $scope.initialized = true;
    });
});