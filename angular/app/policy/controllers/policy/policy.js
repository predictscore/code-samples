var m = angular.module('policy');

m.controller('PolicyController', function($scope, $routeParams, $timeout, $q, defaultHttpErrorHandler, path, uuid, modals, policyDao, policyReportDao, objectTypeDao, tablePaginationHelper, columnsHelper, $route, routeHelper, commonDao, policySaveConverter, tableRemediationActionsBuilder, policyPropertiesFilter, policyAllowsDao, tableAllowsActionsBuilder, policyConditionToFiltersConverter, policyPropertyParser, entityTypes, remediationProperties, feature, $location, policyHeaderOptions, $filter, policySeverityTypes, policyPropertyPathToId, policyPropertyToNodes, modulesManager, logging, docTitleHelper, policyPropertyIsOneToMany, policyConditionsNormalizer, policyFilterTypes, policyCellActionHandler, linkedText, avananUserDao, sideMenuManager, policyScheduledExportModal, policyGroups, actionTagsProcessor, troubleshooting, fileDao, recheckParams, tableBodyAdjuster, policyPropertyMissingMsg, secToolsExceptionParams, suspiciousReportParams) {
    var id = $routeParams.id;
    var tempId = null;
    var useTemp = false;
    var isNew = routeHelper.getParams().isNew;
    var propertiesUpdated = false;

    var initialState = $location.search().status || 'Match';

    $scope.feature = feature;
    $scope.refreshRemember = null;
    $scope.isEmpty = function(o) {
        return _.isEmpty(o);
    };
    $scope.internalModel = {
        saasList: {},
        properties: {},
        conditionProperties: {},
        excludeByProperties: {},
        sources: {},
        propertyTypes: {},
        actionTypes: {},
        columnsList: {}
    };

    $scope.model = {
        includeCondition: {},
        excludeCondition: {},
        actions: [],
        columnsToShow: {},
        columnsOrderOverride: null,
        columnNames: {}
    };
    
    $scope.conditionsListOptions = {
        label: 'Conditions:',
        hideIfMoreDisabled: 2,
        showLinkLabel: 'Show Conditions',
        hideLinkLabel: 'Hide Conditions',
        hideLabel: true
    };
    
    $scope.excludedItemsListOptions = {
        label: 'Excluded items:'  
    };
    
    $scope.isNewPolicy = function() {
        return $scope.internalModel.temporary && _($scope.model.basePolicyId).isUndefined();
    };
    
    $scope.isTempPolicy = function() {
        return !_($scope.model.basePolicyId).isUndefined() || useTemp || propertiesUpdated || $scope.internalModel.temporary;
    };
    
    $scope.hideAllowButtons = function() {
        return !($scope.tableModel && $scope.tableOptions && $scope.tableOptions.toggleState == 'Match');
    };
    
    $scope.getActualPolicyId = function() {
        return useTemp ? tempId || id : id;
    };

    $scope.isPropertyWithCondition = function(path, condition) {
        condition = condition || $scope.model.includeCondition;
        if(condition.type == 'condition' && condition.property == path) {
            return true;
        } else {
            return _(condition.conditions).find(function(cond) {
                return $scope.isPropertyWithCondition(path, cond);
            }) ? true : false;
        }
    };

    $scope.policyTableAdjuster = tableBodyAdjuster($scope, '.policy .results-panel', ['.squares-line', 'thead', '.pagination-bottom:first()', 35]);
    
    var propertiesPromise = objectTypeDao.retrieve(false);
    $q.all([
        propertiesPromise,
        policyDao.retrieve($scope.getActualPolicyId(), propertiesPromise),
        policyDao.getTemplates(),
        objectTypeDao.columnsSelection(),
        avananUserDao.users(),
        policyReportDao.getColumns(id)
    ]).then(function(responses) {
        var properties = responses[0].data;
        $scope.internalModel.saasList = properties.saasList;
        $scope.internalModel.sources = properties.sources;
        $scope.internalModel.allSources = properties.allSources;
        $scope.internalModel.propertyTypes = properties.propertyTypes;
        $scope.internalModel.actionTypes = _(properties.actionTypes).chain().map(function (actions, actionTypeName) {
            return [
                actionTypeName,
                _(actions).filter(function (action) {
                    return !action.exclude_from_policy;
                })
            ];
        }).object().value();
        $scope.internalModel.dataUrls = properties.dataUrls;
        var policy = responses[1].data.policy_data;
        $scope.internalModel.oldPolicy = responses[1].data.old_policy_data;
        $scope.internalModel.scheduled = responses[1].data.scheduled;
        $.extend(true, $scope.model, policy);
        $scope.model.saas = responses[1].data.context.saas;
        $scope.model.layout = responses[1].data.context.layout || {
            id: 'info',
            leftChart: {
                graph: 'bar',
                top: 10
            },
            rightChart: {
                graph: 'bar',
                top: 10
            }
        };
        $scope.internalModel.policy_status = responses[1].data.policy_status;
        $scope.internalModel.templates = responses[2].data.templates;
        $scope.internalModel.temporary = responses[1].data.temporary;
        $scope.internalModel.system = responses[1].data.system;
        $scope.internalModel.columnSelection = responses[3].data;
        $scope.internalModel.users = responses[4].data.rows;
        
        if(policy.disableReason) {
            modals.alert(policy.disableReason, {
                closeText: 'Back to policies list'
            }).then(function() {
                routeHelper.redirectTo('policy-list');
            });
            return;
        }
        
        if(!_($scope.model.basePolicyId).isUndefined()) {
            tempId = id;
            id = $scope.model.basePolicyId;
            useTemp = true;
        }
        
        updateOptionsLabel();
        
        var found = _(policyGroups).find(function(group) {
            if(group.tags) {
                return _(group.tags).every(function(tag) {
                    return _(policy.tags).contains(tag);
                });
            } else if(group.notTags) {
                return _(group.notTags).every(function(tag) {
                    return !_(policy.tags).contains(tag);
                });
            }
            return false;
        });
        if(found) {
            sideMenuManager.treeNavigationPath(['analytics', _(found.tags).first() || 'user']);
        } else {
            sideMenuManager.treeNavigationPath(['analytics']);
        }
        
        var pingPolicyTimeout = null;
        function pingPolicy() {
            policyDao.ping($scope.getActualPolicyId());
            pingPolicyTimeout = $timeout(pingPolicy, 60 * 1000);
        }
        
        pingPolicy();
        
        $scope.$on('$destroy', function() {
            if(pingPolicyTimeout) {
                $timeout.cancel(pingPolicyTimeout);
            }
        });
        
        var openConditionsEditor = function(event, readOnly) {
            if(event) {
                event.preventDefault();
            }
            if ($scope.model.advancedConditions === 'not_parsable') {
                modals.alert('There is a problem viewing this query\'s conditions. Please contact support.');
                return;
            }
            modals.conditionsEditor($scope.model.includeCondition, {
                properties: $scope.internalModel.conditionProperties,
                types: $scope.internalModel.propertyTypes,
                title: readOnly ? 'Policy conditions (Read only)' : 'Edit conditions',
                readOnly: readOnly
            }).then(function(result) {
                $scope.model.includeCondition = result;
                var normalized = policyConditionsNormalizer($scope.internalModel.conditionProperties.properties, $scope.model.includeCondition);
                if(normalized) {
                    $scope.model.includeCondition = normalized;
                }
                $scope.tableOptions.pagination.columnsFilters = policyConditionToFiltersConverter.toFilters($scope.model.includeCondition);
                $scope.tableHelper.clearSelection();
                $scope.updatePolicy().then(function() {
                    $scope.tableHelper.reload().then(function() {
                        $scope.model.advancedConditions = (normalized === false ? 'not_parsable' : !normalized);
                    });
                });
            });
        };
        
        var policyPropertiesModal = function(label, saveAs) {
            return modals.params([{
                params: [{
                    id: 'name',
                    type: 'string',
                    label: 'Query name',
                    data: $scope.model.name,
                    validation: {
                        required: true
                    }
                }, {
                    id: 'description',
                    type: 'string',
                    label: 'Query description',
                    data: $scope.model.description
                }, {
                    id: 'severity',
                    type: 'selector',
                    variants: policySeverityTypes.asVariants(),
                    label: 'Query severity',
                    data: $scope.model.severity
                }, {
                    id: 'tags',
                    type: 'list',
                    label: 'Query tags',
                    data: saveAs ? [] : $scope.model.tags
                }, {
                    id: 'alert',
                    type: 'boolean',
                    label: 'Alert',
                    data: saveAs ? false : $scope.model.alert
                }, {
                    id: 'assignee',
                    label: 'Alert Assignee',
                    type: 'list',
                    variants: [{
                        id: null,
                        label: '[no assignee]'
                    }].concat(_($scope.internalModel.users).map(function (user) {
                        return {
                            id: user.email,
                            label: user.email
                        };
                    })),
                    settings: {
                        multiple: false
                    },
                    enabled: {
                        param: 'alert',
                        equal: true
                    },
                    data: $scope.model.assignee || null
                }, {
                    id: 'not_run_backwards',
                    type: 'boolean',
                    label: 'Run actions on new items only',
                    hidden: true,
                    data: saveAs ? true: $scope.model.not_run_backwards
                }],
                lastPage: true
            }], label);
        };
        
        function getButton(id) {
            var found = _($scope.resultsPanel.buttons).find(function(button) {
                return button.id == id;
            });
            if(!found) {
                found = _($scope.buttons).find(function(button) {
                    return button.id == id;
                });
            }
            return found;
        }
        
        $scope.buttons = {
            refresh: {
                tooltip: 'Refresh',
                'class': {},
                execute: function() {
                    $scope.setRefreshHighlight(false);
                    cancelFindingsUpdate();
                    $scope.tableHelper.clearSelection();
                    $scope.tableHelper.reload();
                }
            },
            query: {
                label: 'Query',
                buttons: [
                    'newButton',
                    'saveButton', 
                    'saveAsButton', 
                    'propertiesButton', 
                    'policyStatusButton',
                    'exportCsvButton',
                    'scheduledExportButton'
                ]
            },
            newButton: {
                id: 'newButton',
                label: 'New',
                'class': {},
                execute: function(event) {
                    event.preventDefault();
                    if(getButton('newButton').class['disabled-link']) {
                        return;
                    }
                    routeHelper.redirectTo('policy-create');
                }
            },
            saveButton: {
                id: 'saveButton',
                label: 'Save',
                hideOnSystem: true,
                position: 'left',
                'class': {},
                execute: function(event) {
                    event.preventDefault();
                    if(getButton('saveButton').class['disabled-link']) {
                        return;
                    }
                    if($scope.isNewPolicy() || !$scope.isTempPolicy()) {
                        return;
                    }
                    $scope.updatePolicy({save: true}).then(function() {
                        $scope.rebuildActions();
                        updateOptionsLabel();
                    });
                }
            }, 
            saveAsButton: {
                id: 'saveAsButton',
                label: 'Save as...',
                position: 'left',
                'class': {},
                execute: function(event) {
                    event.preventDefault();
                    if(getButton('saveAsButton').class['disabled-link']) {
                        return;
                    }
                    policyPropertiesModal('Save as query', true).then(function(result) {
                        $scope.model.name = result.name;
                        $scope.model.description = result.description;
                        $scope.model.severity = result.severity;
                        $scope.model.tags = result.tags;
                        $scope.model.isPolicy = result.isPolicy;
                        $scope.model.alert = result.alert;
                        $scope.model.not_run_backwards = result.not_run_backwards;
                        $scope.model.assignee = result.assignee || null;

                        $scope.updatePolicy({saveAs: true}).then(function() {
                            $scope.rebuildActions();
                            updateOptionsLabel();
                        });
                    });
                }
            }, 
            propertiesButton: {
                id: 'propertiesButton',
                label: 'Properties',
                'class': {
                    'hide-on-system': true
                },
                execute: function(event) {
                    event.preventDefault();
                    if(getButton('propertiesButton').class['disabled-link']) {
                        return;
                    }
                    policyPropertiesModal('Edit query properties', false).then(function(result) {
                        $scope.model.name = result.name;
                        $scope.model.description = result.description;
                        $scope.model.severity = result.severity;
                        $scope.model.tags = result.tags;
                        $scope.model.isPolicy = result.isPolicy;
                        $scope.model.alert = result.alert;
                        $scope.model.not_run_backwards = result.not_run_backwards;
                        $scope.model.assignee = result.assignee || null;
                        propertiesUpdated = true;
                        $scope.updatePolicy().then(function() {
                            $scope.rebuildActions();
                            updateOptionsLabel();
                        });
                    });
                }
            },
            policyStatusButton: {
                id: 'policyStatusButton',
                position: 'left',
                iconClass: {
                    'fa': true
                },
                'class': {},
                execute: function(event) {
                    event.preventDefault();
                    if(getButton('policyStatusButton').class['disabled-link']) {
                        return;
                    }
                    if ($scope.isTempPolicy()) {
                        return;
                    }
                    if ($scope.internalModel.policy_status !== 'running') {
                        changePolicyStatus('running');
                    }
                    else {
                        changePolicyStatus('pause');
                    }
                }
            },
            exportCsvButton: {
                id: 'exportCsvButton',
                label: 'Export as CSV',
                'class': {},
                execute: function(event) {
                    event.preventDefault();
                    if(getButton('exportCsvButton').class['disabled-link']) {
                        return;
                    }
                    policyReportDao.exportCsv($scope.getActualPolicyId()).then(function() {
                        modals.alert('You will receive CSV report to your email in few minutes');
                    });
                }
            },
            scheduledExportButton: {
                id: 'scheduledExportButton',
                label: 'Scheduled export',
                'class': {},
                execute: function(event) {
                    event.preventDefault();
                    if(getButton('scheduledExportButton').class['disabled-link']) {
                        return;
                    }
                    policyScheduledExportModal($scope.internalModel.scheduled).then(function(data) {
                        $scope.internalModel.scheduled = data;
                        $scope.updatePolicy({save: true});
                    });
                },
                tooltip: function() {
                    if(getButton('scheduledExportButton').class['disabled-link']) {
                        return 'Please save the query to enable this feature';
                    }
                }
            },
            policyColumnsPick: null,
            policyAddColumns: null,
            policyAddConditions: null,
            manualActions: {
                label: 'Manual actions',
                buttons: []
            },
            queryActions: {
                label: 'Query actions',
                isEnabled: function() {
                    return !$scope.isTempPolicy();  
                },
                addAction: {
                    label: 'Add action',
                    buttons: []
                },
                buttons: []
            },
            layoutActions: {
                label: 'Layouts',
                layouts: [{
                    id: 'big-table',
                    elements: [{
                        height: 2,
                        width: 2,
                        icon: 'fa-table'
                    }]
                }, {
                    id: 'info',
                    elements: [{
                        height: 1,
                        width: 2,
                        icon: 'fa-cog'
                    }, {
                        height: 1,
                        width: 2,
                        icon: 'fa-table'
                    }]
                }, {
                    id: 'info-chart',
                    elements: [{
                        height: 1,
                        width: 1,
                        icon: 'fa-cog'
                    }, {
                        height: 1,
                        width: 1,
                        icon: 'fa-bar-chart'
                    }, {
                        height: 1,
                        width: 2,
                        icon: 'fa-table'
                    }]
                }, {
                    id: 'chart-info',
                    elements: [{
                        height: 1,
                        width: 1,
                        icon: 'fa-bar-chart'
                    }, {
                        height: 1,
                        width: 1,
                        icon: 'fa-cog'
                    }, {
                        height: 1,
                        width: 2,
                        icon: 'fa-table'
                    }]
                }, {
                    id: 'chart-chart',
                    elements: [{
                        height: 1,
                        width: 1,
                        icon: 'fa-bar-chart'
                    }, {
                        height: 1,
                        width: 1,
                        icon: 'fa-pie-chart'
                    }, {
                        height: 1,
                        width: 2,
                        icon: 'fa-table'
                    }]
                }]
            },
            status: {
                label: 'Loading...',
                progress: null
            },
            cogs: {
                tooltip: 'Advanced',
                buttons: [{
                    label: 'Raw query',
                    execute: function(event) {
                        event.preventDefault();
                        var body = $scope.generateBody();
                        policySaveConverter(body, {rawPolicy: true}).then(function(raw) {
                            modals.alert(raw, {
                                display: 'json',
                                title: 'Raw policy ' + $scope.getActualPolicyId()
                            });
                        });
                    }
                }, {
                    label: 'Edit conditions',
                    'class': 'hide-on-system',
                    execute: function(event) {
                        openConditionsEditor(event, $scope.internalModel.system);
                    }
                }, {
                    label: 'Recheck query',
                    execute: function(event) {
                        event.preventDefault();
                        policyDao.recheck($scope.getActualPolicyId()).then(function() {
                            cancelFindingsUpdate();
                            $scope.tableHelper.clearSelection();
                            $scope.tableHelper.reload();
                        });
                    }
                }]
            },
            toggleSquares: {
                'class': {},
                label: 'Hide conditions',
                execute: function() {
                    $scope.hideSquares = !$scope.hideSquares;
                    $scope.buttons.toggleSquares.label = $scope.hideSquares ? 'Show conditions' : 'Hide conditions';
                    $timeout($scope.policyTableAdjuster.adjust);
                }
            }
        };
        
        $scope.resultsPanel = {
            title: '',
            'class': 'light white-body white-header no-margin',
            buttons: [],
            progress: {
                tooltip: '',
                'class': 'policy-progress',
                label: 'Loading',
                data: [{
                    'class': 'progress-bar-success',
                    value: 0
                }]
            }
        };
        
        $scope.setRefreshHighlight = function(val) {
            $scope.buttons.refresh.class.highlight = val;
            $scope.buttons.refresh.class['fa-spin'] = val;
        };
        
        $scope.updateButtons = function() {
            var pause = $scope.internalModel.policy_status == 'running';
            var button = getButton('policyStatusButton');
            button.label = pause ? 'Pause query' : 'Start query';
            button.iconClass['fa-play'] = !pause;
            button.iconClass['fa-pause'] = pause;
            button.class['disabled-link'] = $scope.isTempPolicy();
            
            button = getButton('saveButton');
            var saveButtonEnabled = !$scope.isNewPolicy() && $scope.isTempPolicy() && !$scope.internalModel.system;
            button.class['disabled-link'] = !saveButtonEnabled;
            
            button = getButton('exportCsvButton');
            button.class['disabled-link'] = $scope.isTempPolicy();
            
            button = getButton('scheduledExportButton');
            button.class['disabled-link'] = $scope.isTempPolicy() || $scope.internalModel.system;
        };

        var icons = {
            minimize: 'fa-minus',
            expand: 'fa-plus'
        };

        $scope.policyOptionsPanel = {
            title: "Query options",
            minimize: true,
            minimized: !isNew,
            'class': 'expandable-group left-controls no-margin',
            icons: icons
        };

        $scope.includeConditionPanel = {
            title: "Condition",
            minimize: true,
            minimized: !isNew,
            'class': 'expandable-group left-controls no-margin',
            icons: icons
        };

        $scope.excludeConditionPanel = {
            title: "Excluded items",
            minimize: true,
            minimized: true,
            'class': 'expandable-group left-controls no-margin',
            icons: icons
        };

        $scope.excludedByPanel = {
            title: 'Excluded By',
            minimize: true,
            minimized: true,
            'class': 'expandable-group left-controls no-margin',
            icons: icons
        };

        $scope.remediationActionsPanel = {
            title: "Remediation actions",
            minimize: true,
            minimized: !isNew,
            'class': 'expandable-group left-controls no-margin',
            icons: icons
        };

        $scope.jsonPanel = {
            title: "Raw data",
            minimize: true,
            minimized: true,
            'class': 'expandable-group left-controls no-margin',
            icons: icons
        };

        $scope.generateBody = function() {
            var saas = $scope.model.saas;
            var model = $.extend({}, $scope.model);
            delete model.saas;
            return {
                "policy_status": $scope.internalModel.policy_status,
                "policy_data": model,
                "scheduled": $scope.internalModel.scheduled,
                "old_policy_data": $scope.internalModel.oldPolicy,
                "context": {
                    "saas": saas,
                    "layout": $scope.model.layout
                },
                "hidden": $scope.internalModel.temporary,
                "temporary": $scope.internalModel.temporary,
                "system": $scope.internalModel.system
            };
        };
        
        $scope.startPolicy = function() {
            changePolicyStatus('running');
        };
        
        $scope.isPaused = function() {
            return $scope.internalModel.policy_status != 'running';
        };

        $scope.recheckPolicy = function() {
            policyDao.recheck($scope.getActualPolicyId());
        };
        
        $scope.updatePolicy = function(options) {
            options = options || {};
            if(_($scope.model.entityType).isUndefined()) {
                modals.alert('You must select object type before updating query');
                return;
            }
            
            if(!options.soft) {
                cancelFindingsUpdate();
            }
            
            var promise;
            var saveTempId = false;
            if(options.saveAs) {
                $scope.internalModel.system = false;
                $scope.model.basePolicyId = undefined;
                if($scope.internalModel.temporary) {
                    $scope.internalModel.temporary = false;
                    promise = policyDao.update($scope.getActualPolicyId(), $scope.generateBody());
                    tempId = null;
                } else {
                    $scope.internalModel.temporary = false;
                    promise = policyDao.create($scope.generateBody());
                }
                useTemp = false;
            } else if(options.save) {
                $scope.internalModel.temporary = false;
                $scope.model.basePolicyId = undefined;
                promise = policyDao.update(id, $scope.generateBody());
                useTemp = false;
            } else if($scope.internalModel.temporary && _(tempId).isNull()) {
                promise = policyDao.update(id, $scope.generateBody());
            } else if($scope.internalModel.system) {
                promise = policyDao.update(id, $scope.generateBody());
            } else {
                $scope.internalModel.temporary = true;
                if(_(tempId).isNull()) {
                    $scope.model.basePolicyId = id;
                    promise = policyDao.create($scope.generateBody());
                    saveTempId = true;
                    useTemp = true;
                } else {
                    promise = policyDao.update(tempId, $scope.generateBody());
                    saveTempId = false;
                    useTemp = true;
                }
            }
            
            updateOptionsLabel();
            
            return promise.then(function(response) {
                propertiesUpdated = false;
                var promises = [];
                if(saveTempId) {
                    tempId = response.data.policy_id;
                    promises.push(policyAllowsDao.move(id, tempId));
                }
                if(!useTemp && tempId) {
                    promises.push(policyDao.changeBatchStatus([tempId], 'mark_for_deletion'));
                    promises.push(policyAllowsDao.move(tempId, id));
                    tempId = null;
                }
                
                id = response.data.policy_id;
                if(!_($scope.model.basePolicyId).isUndefined()) {
                    id = $scope.model.basePolicyId;
                }
                
                routeHelper.redirectTo('policy-edit', {
                    id: '' + $scope.getActualPolicyId()
                }, {
                    saveQueryString: true,
                    reload: false
                });
                
                if(!options.soft) {
                    cancelFindingsUpdate();
                    $scope.rebuildActions();
                }
                
                return $q.all(promises);
            }).then(function() {
                updateOptionsLabel();
                return $q.when();
            });
        };

        function updateOptionsLabel(forceStar) {
            var policyName = $scope.model.name;
            var policyTypeLabel = $scope.internalModel.sources($scope.model.entityType).label;
            if(!$scope.internalModel.system && ($scope.isTempPolicy() || forceStar)) {
                policyName += '*';
                docTitleHelper.setTitle('*');
            } else {
                docTitleHelper.setTitle('');
            }
            $route.current.fullTitle = $route.current.title + ' - ' + policyName + ' (' + policyTypeLabel + ')';
        }

        function changePolicyStatus(status) {
            return policyDao.changeStatus($scope.getActualPolicyId(), status).then(function () {
                $scope.internalModel.policy_status = status;
                $scope.updateButtons();
            });
        }
        
        var requestTimeout = null;
        var timeoutHandle = null;
        var oldFindings = null;
        function findingsUpdate() {
            requestTimeout = $q.defer();
            var idToUse = $scope.getActualPolicyId();
            policyDao.findingsStatus(idToUse).timeout(requestTimeout).then(function(response) {
                if(_($scope.tableModel).isNull()) {
                    return;
                }
                var findings = response.data[idToUse] || {};
                var isCalculating = findings.isCalculating;
                findings = _(findings).omit('isCalculating');
                
                function getCount(id) {
                    return $filter('number')(findings[id] || 0);
                }
                
                $scope.buttons.status.progress = {
                    total: 100,
                    progress: {}
                };
                if(!isCalculating) {
                    var matchCount = findings.Match || 0;
                    _($scope.tableModel.columns).each(function(col) {
                        col.sortable = !col.aggregatable && matchCount < feature.maxRowsInPolicyForSorting;
                        if(col.aggregatable) {
                            col.sortableTooltip = 'Sorting is Disable for aggregatable columns';
                        } else if (matchCount > feature.maxRowsInPolicyForSorting) {
                            col.sortableTooltip = 'Sorting is disable to the amount of matches, add conditions to reduce matches';
                        } else {
                            col.sortableTooltip = null;
                        }
                    });
                    $scope.buttons.status.progress.label = 'Match: ' + getCount('Match');
                    $scope.buttons.status.progress.data = [{
                        value: 100
                    }];
                    
                    var total = Math.floor(matchCount / $scope.tableOptions.pageSize);
                    if(matchCount % $scope.tableOptions.pageSize) {
                        total++;
                    }
                    if(total === 0) {
                        total = 1;
                    }
                    $scope.tableModel.pagination = $.extend({}, $scope.tableModel.pagination, {
                        total: total
                    });
                    if($scope.tableModel.pagination.haveNext) {
                        delete $scope.tableModel.pagination.haveNext;
                    }
                } else {
                    _($scope.tableModel.columns).each(function(col) {
                        col.sortable = false;
                    });
                    $scope.buttons.status.progress.label = 'Calculating...';
                    $scope.buttons.status.progress.data = [{
                        value: 0
                    }];
                }
                
                if(!isCalculating && !_(findings).isEqual(oldFindings) && findings.total > 0 && !_(oldFindings).isNull()) {
                    $scope.refreshRemember = false; //Force to use logic with highlighting refresh
                    if($scope.refreshRemember === true/* || findings[$scope.tableOptions.toggleState] < $scope.tableOptions.pageSize*/) {
                        $scope.tableHelper.clearSelection();
                        $scope.tableHelper.reload();
                    } else if($scope.refreshRemember === false) {
                        $scope.setRefreshHighlight(true);
                    } else if(!$scope.refreshOpen) {
                        $scope.refreshOpen = true;
                        modals.confirm({
                            title: 'New data available',
                            okText: 'Refresh',
                            cancelText: 'Later',
                            message: 'Perform refresh?',
                            remember: 'Remember my choice'
                        }).then(function(result) {
                            if(result.remember) {
                                $scope.refreshRemember = true;
                            }
                            $scope.tableHelper.clearSelection();
                            $scope.tableHelper.reload();
                        }, function(result) {
                            if(result.remember) {
                                $scope.refreshRemember = false;
                            }
                            $scope.setRefreshHighlight(true);
                            $scope.refreshOpen = false;
                        });
                    }
                } else {
                    timeoutHandle = $timeout(findingsUpdate, 10000);
                }
                if(!isCalculating || findings.total > 0) {
                    oldFindings = findings;
                }
            }, function() {
                timeoutHandle = $timeout(findingsUpdate, 10000);
            });
        }
        
        function cancelFindingsUpdate() {
            if(requestTimeout) {
                requestTimeout.resolve();
            }
            if(timeoutHandle) {
                $timeout.cancel(timeoutHandle);
            }
        }
        
        $scope.$on('$destroy', cancelFindingsUpdate);
        
        $scope.$watch('tableModel.columnsOrder', function(newOrder, oldOrder) {
            if(_(newOrder).isEqual(oldOrder) || _(oldOrder).isUndefined() || _(newOrder).isUndefined()) {
                return;
            }
            $scope.model.columnsOrderOverride = newOrder;
            if(!$scope.disableColumnsOrderUpdate) {
                useTemp = true;
                $scope.updatePolicy().then(function() {
                    $scope.updateButtons();
                    updateOptionsLabel();
                });
            }
            $scope.disableColumnsOrderUpdate = false;
        }, true);

        function isColumnShown (path) {
            if ($scope.model.columnsToShow[path] && !_($scope.model.columnsToShow[path].order).isNull()) {
                return true;
            }
            return false;
        }
        
        function addColumn(path, options) {
            var menuId = policyPropertyParser(path);
            if(!isColumnShown(menuId.fullId)) {
                var maxOrder = _($scope.model.columnsToShow).reduce(function(memo, column) {
                    return Math.max(memo, column.order);
                }, 0);
                var property = $scope.internalModel.conditionProperties.properties[menuId.nodeId];
                
                var entityPath = _(menuId.fullId.split('.')).initial().join('.');
                var existEntity = _($scope.model.columnsToShow).find(function(column, path) {
                    return isColumnShown(path) && path.indexOf(entityPath) === 0;
                });
                var aggregate = options.aggregate;
                if(existEntity) {
                    aggregate = existEntity.aggregate;
                }
                var oldResolve = ($scope.model.columnsToShow[menuId.fullId] && $scope.model.columnsToShow[menuId.fullId].resolve || false);
                $scope.model.columnsToShow[menuId.fullId] = {
                    order: maxOrder + 1,
                    aggregate: aggregate,
                    resolve: options.resolve
                };
                if(!property.entity.saas) {
                    $scope.model.columnsToShow[menuId.fullId].resolve = oldResolve;
                }
                if(options.label) {
                    $scope.model.columnNames[menuId.fullId] = options.label;
                }
                $scope.model.columnsOrderOverride.push(policyPropertyPathToId.toId(menuId.fullId));
                $scope.tableOptions.columnsOrder = $scope.model.columnsOrderOverride;
                return true;
            }
            return false;
        }
        
        function removeColumn(path) {
            var columnId = policyPropertyPathToId.toId(path);
            $scope.model.columnsOrderOverride = _($scope.model.columnsOrderOverride).filter(function(fullId) {
                return columnId != fullId;
            });
            if ($scope.model.columnsToShow[path].resolve) {
                $scope.model.columnsToShow[path].order = null;
                delete $scope.model.columnsToShow[path].sortIndex;
                delete $scope.model.columnsToShow[path].sortDirection;
            } else {
                delete $scope.model.columnsToShow[path];
            }
            $scope.tableModel.columnsOrder = $scope.tableOptions.columnsOrder = $scope.model.columnsOrderOverride;
            $scope.buttons.policyColumnsPick.model[path] = false;
        }
        
        $scope.setLayout = function(layout) {
            $scope.model.layout.id = layout.id;
            $scope.updatePolicy();
            $timeout(function() {
                $scope.policyTableAdjuster.adjust();    
            });
        };
        
        var sourceProperties = $scope.internalModel.sources($scope.model.entityType);

        $scope.internalModel.conditionProperties = policyPropertiesFilter(sourceProperties, 'showInCondition');
        $scope.internalModel.excludeByProperties = policyPropertiesFilter(sourceProperties, 'showInExcludeBy');
        if($scope.tableHelper) {
            $scope.tableHelper.destroy();
        }
        $scope.tableModel = null;
        $scope.tableOptions = {
            pagesAround: 2,
            pageSize: 20,
            pagination: {
                page: 1,
                ordering: {},
                filter: feature.newPolicyApi ? undefined : '',
                columnsFilters: policyConditionToFiltersConverter.toFilters($scope.model.includeCondition)
            },
            infoText: '',
            eventOnFilterChange: 'policy-filter-change',
            saveState: false,
            disableTop: true,
            dropdownHeader: true,
            bodyScroll: true,
            bodyScrollAdjustEvent: 'policy-table-adjust-done',
            bodyScrollTableSelector: '.results-table',
            orderingChangedEvent: 'policy-table-order-change',
            orderDisplayMode: 'highlight-column',
            showWorkingIndicator: true,
            lazyPromiseResolve: true,
            toggleState: initialState,
            actions: [],
            buttons: [],
            columnsOrder: $scope.model.columnsOrderOverride || [],
            columnNamesOverride: function(column) {
                return $scope.model.columnNames[column.path];
            },
            headerOptions: policyHeaderOptions({
                notAllowed: function(columnId) {
                    var path = policyPropertyPathToId.toPath(columnId);
                    return !policyPropertyIsOneToMany($scope.internalModel.conditionProperties.properties, path);
                },
                aggregatedOperatorAllowed: function(columnId) {
                    var path = policyPropertyPathToId.toPath(columnId);
                    return policyPropertyIsOneToMany($scope.internalModel.conditionProperties.properties, path);
                }
            }),
            onColumnRemove: function(columnId, column) {
                removeColumn(column.path);
            },
            filter: function(row, columns) {
                var idx = columnsHelper.getPrimaryIdx(columns);
                var found = _($scope.model.excludeObjects).find(function(e) {
                    return e === row[idx].originalText;
                });
                if(_(found).isUndefined()) {
                    return true;
                }
                return 'excluded';
            }
        };
        if (troubleshooting.entityDebugEnabled()) {
            $scope.tableOptions.allowedPageSizes = [20, 50, 100, 200];
        }
        
        $scope.buttons.policyColumnsPick = null;
        $scope.buttons.policyAddColumns = null;
        $scope.buttons.policyAddConditions = null;
        if($scope.internalModel.conditionProperties) {
            $scope.buttons.policyColumnsPick = {
                model: {},
                options: {
                    label: 'Columns',
                    groups: $scope.internalModel.columnSelection[$scope.model.entityType],
                    properties: $scope.internalModel.conditionProperties.properties,
                    columns: [],
                    columnNames: {},
                    onChange: function(model) {
                        var changed = false;
                        _($scope.model.columnsToShow).each(function(column, path) {
                            if (!isColumnShown(path)) {
                                return;
                            }
                            var found = _(model).find(function(c) {
                                return c.id == path;
                            });
                            if(!found) {
                                changed = true;
                                removeColumn(path);
                            } 
                        });
                        _(model).each(function(value) {
                            if(value && !isColumnShown(value.id)) {
                                changed = true;
                                addColumn(value.id, value);
                            }
                        });
                        if(changed) {
                            $scope.disableColumnsOrderUpdate = true;
                            $scope.updatePolicy().then(function() {
                                $scope.tableHelper.clearSelection();
                                $scope.tableHelper.reload();
                            });
                        }
                    }
                }
            };
            
            $scope.buttons.policyAddColumns = {
                options: {
                    menus: $scope.internalModel.conditionProperties.properties,
                    'class': 'fa fa-plus',
                    'label': '',
                    dropdownClass: 'btn-blue-border',
                    menuOptions: {
                        menusBlackList: [],
                        selectedList: [],
                        menuIconsOverride: true,
                        menuIconSelected: 'fa-check-square-o',
                        menuIconUnselected: 'fa-square-o',
                        multiSelect: true,
                        getConditions: function(menuId) {
                            var filters = policyConditionToFiltersConverter.toFilters($scope.model.includeCondition);
                            var columnId = policyPropertyPathToId.toId(menuId.fullId);
                            return filters[columnId];
                        }
                    }
                },
                onClick: function(menuIds, filter) {
                    _(menuIds).each(function(added, fullId) {
                        if(isColumnShown(fullId)) {
                            removeColumn(fullId);
                        } else if(addColumn(fullId, {aggregate: true})) {
                            $scope.disableColumnsOrderUpdate = true;
                        }
                    });
                    if($scope.disableColumnsOrderUpdate) {
                        $scope.updatePolicy().then(function() {
                            $scope.tableHelper.clearSelection();
                            $scope.tableHelper.reload();
                        });
                    }
                }
            };
            
            $scope.buttons.policyAddConditions = {
                options: {
                    menus: $scope.internalModel.conditionProperties.properties,
                    'class': 'fa fa-plus',
                    'label': 'Add condition',
                    dropdownClass: 'btn-blue-border',
                    menuOptions: {
                        menusBlackList: [],
                        editor: 'conditions-editor',
                        editorOptions: policyHeaderOptions({
                            notAllowed: function(path) {
                                return !policyPropertyIsOneToMany($scope.internalModel.conditionProperties.properties, path);
                            },
                            aggregatedOperatorAllowed: function(path) {
                                return policyPropertyIsOneToMany($scope.internalModel.conditionProperties.properties, path);
                            }
                        }),
                        getConditions: function(menuId) {
                            var filters = policyConditionToFiltersConverter.toFilters($scope.model.includeCondition);
                            var columnId = policyPropertyPathToId.toId(menuId.fullId);
                            return filters[columnId];
                        }
                    }
                },
                onClick: function(menuIds, filter) {
                    var fullId = _(menuIds).chain().keys().first().value();
                    var columnId = policyPropertyPathToId.toId(fullId);
                    if(filter) {
                        var filters = {};
                        filters[columnId] = filter;
                        $scope.addConditions(fullId, filters);
                    }
                }
            };
        }

        function policyActionConfigureDialog(action, params, actionType, actionCanHaveTags) {
            return modals.params([{
                params: params,
                lastPage: true,
                getProperties: _.memoize(remediationProperties($scope.internalModel.conditionProperties.properties))
            }], actionType.label, actionType.size, {
                noDataLabel: actionType.confirm
            }).then(function(data) {
                var attributes = _(data).omit('_label', '_not_run_backwards');
                if (actionCanHaveTags && actionTagsProcessor.getProperties(attributes).length < 1) {
                    return modals.alert('Action should have some tags. Please select one.', {
                        title: 'Error',
                        keyboard: false,
                        backdrop: 'static',
                        disableCloseX: true
                    }).then(function () {
                        _(params).each(function (param) {
                            param.data = data[param.id];
                        });
                        return policyActionConfigureDialog(action, params, actionType, actionCanHaveTags);
                    });
                }
                action.label = data._label;
                $scope.model.not_run_backwards = data._not_run_backwards;
                _(attributes).each(function(param, id) {
                    action.attributes[id] = param;
                });
                return action;
            });
        }
        
        $scope.generateSendMail = function() {
            var attributes = {};
            var policyName = $scope.model.name || 'No name policy';
            attributes.subject = 'Avanan alert: ' + policyName;
            attributes.content = '';
            if ($scope.model.name) {
                attributes.content = '<p class="top-alert-name">' + $scope.model.name + '</p>';
            }
            attributes.content += '<p>';
            attributes.content += 'Hello,<br />';
            attributes.content += 'Avanan has just detected a security event.<br />';
            attributes.content += 'Please see below:';
            attributes.content += '</p>';
            attributes.content += '<h3>Event data</h3>';
            attributes.content += '<table><tbody>';
            _($scope.model.columnsOrderOverride).each(function(columnId) {
                var column = $scope.getColumn(columnId);
                var path = policyPropertyPathToId.toPath(columnId);
                var columnName = $scope.getColumnName(column, path);
                attributes.content += '<tr><td>' + columnName + '</td><td><strong>{' + path + '}</strong></td></tr>';
            });
            attributes.content += '</tbody></table>';

            attributes.content += '<h3>Policy filters</h3>';
            attributes.content += '<table><tbody>';
            _($scope.getFiltersInfo()).each(function(info) {
                var asText = linkedText.toText(info.label);
                var parsed = asText.match(/<strong>(.*)<\/strong>:\s*(.*)/);
                if (parsed.length === 3) {
                    attributes.content += '<tr><td>' + parsed[1] + '</td><td><strong>' + parsed[2] + '</strong></td></tr>';
                }
            });
            attributes.content += '</tbody></table>';
            attributes.content += '<p class="query-view-link"><a href="' + window.location.toString() + '" target="_blank">Click here to see all detections of this type</a></p>';
            attributes.to = sideMenuManager.currentUser().email;
            return attributes;
        };
        
        $scope.generateReportCef = function() {
            var attributes = {};
            attributes.raw_cef_extension = _($scope.model.columnsOrderOverride).map(function(columnId) {
                var path = policyPropertyPathToId.toPath(columnId);
                return path + '={' + path + '}';
            }).join(' ');
            return attributes;
        };
        
        $scope.generateReportSplunk = function() {
            var attributes = {};
            attributes.json = _($scope.model.columnsOrderOverride).map(function(columnId) {
                var column = $scope.getColumn(columnId);
                var path = policyPropertyPathToId.toPath(columnId);
                var columnName = $scope.getColumnName(column, path);
                return columnName + '={' + path + '};';
            }).join('\n');
            return attributes;
        };
        
        $scope.policyActions = {
            add: function(type) {
                var attributes = {};
                if(type.id == 'send_mail') {
                    attributes = $scope.generateSendMail();
                } else if(type.id == 'report_cef_over_syslog') {
                    attributes = $scope.generateReportCef();
                } else if(type.id == 'report_json_over_http') {
                    attributes = $scope.generateReportSplunk();
                }
                return {
                    id: uuid.random(),
                    attributes: attributes,
                    tags: {},
                    name: type.id
                };
            },
            showConfigurePopup: function(action) {
                var actionCanHaveTags = false;
                var actionType = $scope.policyActions.getActionType(action);
                var params = _(actionType.attributes).chain().filter(function(param) {
                    return _(param.label).isString();
                }).map(function(param) {
                    actionCanHaveTags = actionCanHaveTags || param.type === 'property-string' ||
                            param.type === 'property' || param.type === 'property-email-body' ||
                            param.type === 'cef-items';
                    return $.extend(true, {}, param, {
                        data: action.attributes[param.id]
                    });
                }).value();
                params.splice(0, 0, {
                    label: 'Action name',
                    id: '_label',
                    data: action.label || actionType.label,
                    type: 'string'
                });
                params.push({
                    label: 'Run on new items only (*)',
                    labelTip: {
                        content: {
                            text: 'Query wise property, applicable to all actions'
                        },
                        position: {
                            my: 'bottom left',
                            at: 'top center'
                        }
                    },
                    id: '_not_run_backwards',
                    data: $scope.model.not_run_backwards,
                    type: 'boolean'
                });
                return policyActionConfigureDialog(action, params, actionType, actionCanHaveTags);
            },
            getActionType: function(action) {
                var actionTypes = $scope.internalModel.actionTypes[$scope.model.entityType];
                return _(actionTypes).find(function(type) {
                    return type.id == action.name;
                });
            }
        };
        
        $scope.$watchCollection(function() {
            return $scope.tableHelper.getSelectedIds();
        }, function() {
            var manualActionsOptions = $scope.buttons.manualActions.buttons = [];
            
            var selectedEntities = _($scope.tableHelper.getSelectedIds()).chain().map(function(id) {
                var entityId = id[$scope.model.entityType];
                return [entityId, {
                    entity_type: entityTypes.toLocal($scope.model.entityType),
                    entity_id: entityId,
                    labelPromise: $scope.tableHelper.getLabel(id, true, function(column) {
                        return column.mainProperty;
                    })
                }];
            }).object().values().value();
            
            $q.all(_(selectedEntities).map(function(entity) {
                return $q.when(entity.labelPromise).then(function(resolved) {
                    entity.label = resolved;
                });
            })).then(function() {
                var source = $scope.internalModel.allSources[$scope.model.entityType];
                if(troubleshooting.entityDebugEnabled() && source.parentEntity == 'av_file') {
                    manualActionsOptions.push({
                        label: 'Recheck',
                        display: 'dropdown-button',
                        active: function() {
                            return !!selectedEntities.length;
                        },
                        execute: function(event) {
                            if(event) {
                                event.preventDefault();
                            }
                            
                            recheckParams.show('Do you want to recheck?', $scope.model.entityType).then(function(data) {
                                _(selectedEntities).each(function(entity) {
                                    return fileDao.recheck($scope.model.entityType, entity.entity_id, data.selectApplications ? data.apps : undefined);
                                });
                            });
                        }
                    });
                    manualActionsOptions.push({
                        label: 'Report',
                        display: 'dropdown-button',
                        active: function() {
                            return !!selectedEntities.length;
                        },
                        execute: function(event) {
                            if(event) {
                                event.preventDefault();
                            }
                            if (!selectedEntities.length) {
                                return;
                            }

                            suspiciousReportParams.show('Do you want to report ' + (selectedEntities.length === 1 ? 'this entity' :
                                (selectedEntities.length + ' selected entities')) + '?', $scope.model.entityType,
                                selectedEntities.length === 1 ? selectedEntities[0].entity_id : null, {
                                    objectTypesData: properties
                                }).then(function(data) {
                                _(selectedEntities).each(function(entity) {
                                    return fileDao.suspiciousReport($scope.model.entityType, entity.entity_id, data);
                                });
                                $scope.tableHelper.clearSelection();
                            });
                        }
                    });
                }

                if (source.parentEntity == 'av_file') {
                    manualActionsOptions.push({
                        label: 'Add to exceptions',
                        display: 'dropdown-button',
                        active: function() {
                            return !!selectedEntities.length;
                        },
                        execute: function(event) {
                            if(event) {
                                event.preventDefault();
                            }

                            secToolsExceptionParams.show('Add selected to security tools exceptions').then(function(data) {
                                var addData = _(selectedEntities).chain().map(function(entity) {
                                    return _(data).chain().map(function (value, key) {
                                        if (value) {
                                            return {
                                                entity_type: $scope.model.entityType,
                                                entity_id: entity.entity_id,
                                                sec_type: key
                                            } ;
                                        }
                                        return false;
                                    }).compact().value();
                                }).flatten().value();
                                if (addData.length) {
                                    return fileDao.exceptionsAdd(addData).then(function () {
                                        $scope.tableHelper.clearSelection();
                                        $scope.tableHelper.reload();
                                    });
                                }
                            });
                        }
                    });
                }


                manualActionsOptions.push({
                    label: 'Exclude selected',
                    display: 'dropdown-button',
                    active: function() {
                        return !!selectedEntities.length;
                    },
                    execute: function(event) {
                        if(event) {
                            event.preventDefault();
                        }
                        commonDao.entityIdentifier(_(selectedEntities).map(function(entity) {
                            return entity.entity_id;
                        }), $scope.model.entityType).then(function(response) {
                            var type = entityTypes.toLocal($scope.model.entityType);
                            var saas = entityTypes.toSaas($scope.model.entityType);
                            
                            $q.all(_(response).map(function(entity) {
                                var text = '#' + JSON.stringify({
                                    display: 'link',
                                    type: type,
                                    module: saas,
                                    id: entity.id,
                                    text: entity.label
                                });
                                return policyAllowsDao.create($scope.getActualPolicyId(), text, $scope.model.entityType, [entity.id]);
                            })).then(function() {
                                $scope.tableHelper.clearSelection();
                                $scope.tableHelper.reload();
                            });
                        });
                    }
                });
                
                tableRemediationActionsBuilder(manualActionsOptions, $scope.internalModel.actionTypes, $scope.internalModel.sources, $scope.model.saas, _.constant(selectedEntities), {
                    constEntities: true,
                    predefined: {
                        send_mail: function() {
                            return $scope.generateSendMail();
                        },
                        report_cef_over_syslog: function() {
                            return $scope.generateReportCef();
                        },
                        report_json_over_http: function() {
                            return $scope.generateReportSplunk();
                        }
                    },
                    policyEntityType: $scope.model.entityType,
                    policyId: parseInt($scope.getActualPolicyId(), 10)
                });
            });
        });


        $scope.rebuildActions = function() {
            if(!$scope.tableModel) {
                return;
            }
            $scope.updateButtons();
            $scope.tableOptions.actions = [];
            $scope.tableOptions.buttons = [];
            
            $scope.buttons.queryActions.addAction.buttons = [];
            $scope.buttons.queryActions.buttons = [];
            $scope.buttons.queryActions.addAction.buttons = _($scope.internalModel.actionTypes[$scope.model.entityType]).map(function (type) {
                return {
                    label: type.label,
                    execute: function (event) {
                        event.preventDefault();
                        var action = $scope.policyActions.add(type);
                        $scope.policyActions.showConfigurePopup(action).then(function (configuredAction) {
                            $scope.model.actions.push(configuredAction);
                            return $scope.updatePolicy();
                        }).then(function() {
                            $scope.tableHelper.clearSelection();
                            $scope.tableHelper.reload();
                        });
                    }
                };
            });

            $scope.buttons.queryActions.buttons = _($scope.model.actions).map(function (action) {
                return {
                    label: action.label,
                    'class': 'policy-action',
                    execute: function (event) {
                        event.preventDefault();
                        $scope.policyActions.showConfigurePopup(action).then(function () {
                            return $scope.updatePolicy();
                        }).then(function() {
                            $scope.tableHelper.clearSelection();
                            $scope.tableHelper.reload();
                        });
                    },
                    secondary: {
                        'class': 'policy-action-remove fa fa-times',
                        execute: function (event) {
                            event.preventDefault();
                            event.stopPropagation();
                            $scope.model.actions = _($scope.model.actions).filter(function (a) {
                                return a.id != action.id;
                            });
                            $scope.updatePolicy().then(function() {
                                $scope.tableHelper.clearSelection();
                                $scope.tableHelper.reload();
                            });
                        }
                    }
                };
            });

            if (!$scope.buttons.queryActions.buttons.length) {
                $scope.buttons.queryActions.buttons = [{
                    label: 'None',
                    'class': 'disabled-link',
                    execute: _.noop
                }];
            }
            
            function buildAllowsList(data, inConditions) {
                return _(data.rows).chain().map(function(row) {
                    var columnId = policyPropertyPathToId.toId(row.rule_path);
                    var columnIdx = columnsHelper.getIdxById($scope.tableModel.columns, columnId);
                    var column = $scope.tableModel.columns[columnIdx];
                    var prefix = row.allow_name;
                    if(!column.primarySubId) {
                        prefix = '#' + JSON.stringify({
                            display: 'text',
                            text: column.text + ': ',
                            'class': 'display-inline-block bold-text'
                        }) + '#' + JSON.stringify({
                            display: 'text',
                            text: 'not ',
                            'class': 'display-inline-block'
                        }) + '#' + JSON.stringify({
                            display: 'text',
                            text: row.allow_name,
                            'class': 'display-inline-block bold-text'
                        });
                    }
                    return {
                        label: prefix + '#' + JSON.stringify({
                            display: 'text',
                            'class': 'fa fa-times blue-text cursor-pointer',
                            clickEvent: {
                                name: 'remove-allow',
                                attributes: {
                                    allowId: row.id,
                                    allowName: row.allow_name
                                }
                            },
                            tooltip: 'Remove'
                        }),
                        showInConditions: !column.primarySubId
                    };
                }).filter(function(allow) {
                    return allow.showInConditions === inConditions;
                }).value();
            }
            
            $scope.excludedItemsList = buildAllowsList($scope.rawAllowsList, false);
            
            $scope.conditionsList = buildAllowsList($scope.rawAllowsList, true);
            
            if($scope.model.advancedConditions) {
                $scope.conditionsList.push({
                    label: '#' + JSON.stringify({
                        display: 'text',
                        text: 'Advanced conditions',
                        'class': 'cursor-pointer',
                        clickEvent: {
                            name: 'open-conditions-editor',
                            attributes: {}
                        }
                    })
                });
            } else if($scope.getFiltersInfo) {
                _($scope.getFiltersInfo()).each(function(condition) {
                    var nodeId = _(policyPropertyToNodes(condition.path)).last();
                    var property = $scope.internalModel.conditionProperties.properties[nodeId];
                    var type = policyFilterTypes(property);
                    $scope.conditionsList.push({
                        label: condition.label,
                        path: condition.path,
                        filters: condition.columnData,
                        property: property,
                        propertyType: type,
                        headerOptions: policyHeaderOptions({
                            notAllowed: function(path) {
                                return !policyPropertyIsOneToMany($scope.internalModel.conditionProperties.properties, path);
                            },
                            aggregatedOperatorAllowed: function(path) {
                                return policyPropertyIsOneToMany($scope.internalModel.conditionProperties.properties, path);
                            }
                        }),
                        onSave: function(newFilters) {
                            var fullId = condition.path;
                            var columnId = policyPropertyPathToId.toId(fullId);
                            var filters = {};
                            filters[columnId] = newFilters;
                            $scope.addConditions(fullId, filters);
                        },
                        canAddColumn: function(path) {
                            return !isColumnShown(path);
                        },
                        onColumnAdd: function(path) {
                            if(addColumn(path, {aggregate: true})) {
                                $scope.disableColumnsOrderUpdate = true;
                                $scope.updatePolicy().then(function() {
                                    $scope.tableHelper.clearSelection();
                                    $scope.tableHelper.reload();
                                });
                            }
                        },
                        noEvents: true,
                        condition: true
                    });
                });
            }
            
            if(!$scope.excludedItemsList.length) {
                $scope.excludedItemsList.push({
                    label: '#' + JSON.stringify({
                        display: 'text',
                        text: 'None',
                        'class': 'font-italic'
                    })
                });
            }
                
            if(!$scope.conditionsList.length) {
                $scope.conditionsList.push({
                    label: '#' + JSON.stringify({
                        display: 'text',
                        text: 'None',
                        'class': 'font-italic'
                    }),
                    noEvents: true
                });
            }
            
            $scope.buttons.policyAddColumns.options.menuOptions.selectedList = _($scope.model.columnsToShow).chain()
                .pick(function (column, path) {
                    return isColumnShown(path);
                }).keys().value();
            
            $scope.updateGraphOptions();
        };

        $scope.rebuildActions();

        var options = $.extend({
            options: {
                autoUpdate: undefined,
                checkboxes: true,
                dnd: true,
                showMessageOnFail: true
            }
        }, $scope.internalModel.conditionProperties);
        
        $scope.getColumn = function(columnId) {
           return _($scope.tableModel.columns).find(function (column) {
               return column.id == columnId;
           });
        };
        $scope.getColumnName = function(column, path) {
            if(!column) {
                var nodeId = _(policyPropertyToNodes(path)).last();
                var property = $scope.internalModel.conditionProperties.properties[nodeId];
                return property.label;
            }
            return $scope.model.columnNames[column.path] || column.text;
        };

        var lastArgs = null;
        $scope.getFiltersInfo = function() {
            if(_($scope.tableModel).isNull()) {
                return [];
            }
            
            return _(lastArgs.columnsFilters).chain().map(function (columnData, columnId) {
                function makeRemoveButton(filterId) {
                    return '#' + JSON.stringify({
                        display: 'text',
                        'class': 'fa fa-times blue-text cursor-pointer condition-remove hide-on-system',
                        clickEvent: {
                            name: 'remove-filter',
                            columnId: columnId,
                            filterId: filterId
                        },
                        tooltip: 'Remove'
                    });
                }
            
                var column = $scope.getColumn(columnId);
                var path = policyPropertyPathToId.toPath(columnId);
                var nodeId = policyPropertyParser(path).nodeId;
                var property = $scope.internalModel.conditionProperties.properties[nodeId];
                var columnName = $scope.getColumnName(column, path);
                var mappedConditions = _(columnData).map(function(value, filterId) {
                    return [value, filterId];
                });
                var notNullConditions = _(mappedConditions).filter(function(c) {
                    return _(c[0].notNull).isUndefined();
                });
                var nullConditions = _(mappedConditions).filter(function(c) {
                    return c[0].notNull === true;
                });
                var notEmpty = _(nullConditions).map(function(c) {
                    return 'not empty' + makeRemoveButton(c[1]);
                }).join('');
                return {
                    path: path,
                    columnData: columnData,
                    label: '#' + JSON.stringify({
                        display: 'text',
                        text: columnName + ': ',
                        asText: '<strong>' + columnName + '</strong>: ',
                        tooltip: path,
                        'class': 'display-inline-block bold-text'
                    }) + _(notNullConditions).map(function(c, cIdx) {
                        var value = c[0];
                        var filterId = c[1];
                        var last = (cIdx == notNullConditions.length - 1 && !nullConditions.length);
                        var removeButton = makeRemoveButton(filterId);
                        var operatorText = '';
                        if(value.not) {
                            operatorText += 'not ';
                        }
                        if(value.aggregatedOperator) {
                            operatorText += value.aggregatedOperator + ' ';
                        }
                        operatorText += value.operatorLabel + ' ';
                        var text = [];
                        if(value.dataLabel) {
                            var label = value.dataLabel;
                            if(!_(label).isArray()) {
                                label = [label];
                            }
                            text = label;
                        } else if(_(value.data).isArray()) {
                            text = _(value.data).map(function(id) {
                                var variants = property.variants || {};
                                var found = _(variants).find(function(v) {
                                    return v.id == id;
                                });
                                return found ? found.label : id;
                            });
                        } else if(_(value.data).isObject()) {
                            if(!_(value.data.first).isUndefined() && !_(value.data.second).isUndefined()) {
                                text.push(value.data.first + ' and ' + value.data.second);
                            }
                        } else {
                            text.push(value.data);
                        }
                        return '#' + JSON.stringify({
                            display: 'text',
                            text: operatorText,
                            'class': 'display-inline-block'
                        }) + _(text).map(function(t, idx) {
                            var postfix = ',';
                            if(idx == text.length - 1) {
                                postfix = '';
                            }
                            
                            return '#' + JSON.stringify({
                                display: 'text',
                                text: t + postfix,
                                asText: t + postfix + (last ? '' : ';'),
                                'class': 'display-inline-block bold-text'
                            });
                        }).join('') + removeButton;
                    }).join('') + notEmpty
                };
            }).filter(_.isObject).value();
        };
        
        $scope.addConditions = function(fullId, filters) {
            var generatedConditions = policyConditionToFiltersConverter.toConditions(filters);
            var columnId = policyPropertyPathToId.toId(fullId);
            var conditions = _($scope.model.includeCondition.conditions).filter(function(condition) {
                return !condition.generatedFrom || condition.generatedFrom.id != columnId;
            });
            _(generatedConditions.conditions).each(function(condition) {
                conditions.push(condition);
            });
            _(filters).each(function(filter, columnId) {
                $scope.tableOptions.pagination.columnsFilters[columnId] = filter;
            });
        };
        
        $scope.graphOptions = {
            refreshEvent: 'key-value-graph-refresh'
        };
        
        $scope.updateGraphOptions = function() {
            var keyVariants = [];
            var valueVariants = [{
                id: 'count(*)',
                label: 'Count'
            }];
            _($scope.tableModel.columns).each(function(column) {
                if(column.hidden) {
                    return;
                }
                var node = _(policyPropertyToNodes(column.path)).last();
                var property = $scope.internalModel.allSources[node];
                if(!property) {
                    return;
                }
                var formatter = _.identity;
                if(property.showAsBytes) {
                    formatter = function(data) {
                        return $filter('bytes')(data, 1, true);
                    };
                }
                var variant = {
                    id: column.path,
                    label: column.text,
                    formatter: formatter
                };
                if(property.type == 'number') {
                    valueVariants.push(variant);
                } else {
                    keyVariants.push(variant);
                }
            });
            $scope.graphOptions.keyVariants = keyVariants;
            $scope.graphOptions.valueVariants = valueVariants;
            $scope.graphOptions.request = function(keyPath, valuePath, top) {
                return policyDao.graph($scope.getActualPolicyId(), keyPath, valuePath, top);
            };
        };
        
        $scope.onChartChange = function() {
            $scope.updatePolicy();
        };
        
        $scope.rebuildActions();
        
        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, options, function(args) {
            $scope.setRefreshHighlight(false);
            cancelFindingsUpdate();
            lastArgs = args;
            
            policyAllowsDao.retrieve(undefined, $scope.getActualPolicyId()).then(function(response) {
                $scope.rawAllowsList = response.data;
            });
            
            $scope.$broadcast($scope.graphOptions.refreshEvent);
            
            var result = {
                timeout: function(promise) {
                    return $q.all([
                        policyReportDao.retrieve({
                            policyId: $scope.getActualPolicyId(),
                            properties: $scope.internalModel.allSources,
                            actions: $scope.internalModel.actionTypes,
                            page: args.offset / args.limit + 1,
                            perPage: args.limit,
                            filter: args.filter,
                            status: $scope.tableOptions.toggleState
                        }).timeout(promise),
                        policyAllowsDao.retrieve(undefined, $scope.getActualPolicyId())
                    ]).then(function(responses) {
                        $scope.rawAllowsList = responses[1].data;
                        return $q.when(responses[0]);
                    });
                }
            };
            return result;
        }, function(tableModel) {
            $scope.refreshOpen = false;
            if(!_(tableModel).isNull()) {

                _(tableModel.columns).chain().filter(function(column) {
                    return !column.hidden;
                }).each(function(column) {
                    function checkClass(state) {
                        return {
                            'fa-check-square-o': state,
                            'fa-square-o': !state
                        };
                    }
                    var metadata = $scope.model.columnsToShow[column.path], property;
                    if(_(metadata).isObject()) {
                        var menuId = policyPropertyParser(column.path);
                        property = $scope.internalModel.conditionProperties.properties[menuId.nodeId];
                        if (!property) {
                            logging.log('Unable to resolve ' + menuId.fullId + ' property using api/v1/policies/policy_ui_conditions API');
                            var missingMsg = policyPropertyMissingMsg($scope.internalModel.conditionProperties.properties, menuId.fullId);
                            if (missingMsg) {
                                logging.log(missingMsg);
                            }
                            column.hidden = true;
                            return;
                        }
                    }

                    column.buttons = column.buttons || [];
                    column.headerButtons = column.headerButtons || [];
                    column.buttons.push({
                        label: 'Rename column',
                        iconClass: 'fa-pencil',
                        execute: function(event) {
                            event.preventDefault();
                            event.stopPropagation();
                            modals.params([{
                                params: [{
                                    id: 'name',
                                    type: 'string',
                                    label: 'Column name',
                                    data: $scope.model.columnNames[column.path] || column.text
                                }],
                                lastPage: true
                            }], 'Rename column ' + column.path).then(function(result) {
                                $scope.model.columnNames[column.path] = result.name;
                                $scope.updatePolicy({soft: true}).then(function() {
                                    $scope.rebuildActions();
                                    $scope.tableModel.refreshGui();
                                });
                            });
                        }
                    });
                    if(_(metadata).isObject()) {
                        if(feature.showActiveResolver && !property.entity.saas && (metadata.resolve || troubleshooting.entityDebugEnabled())) {
                            var resolveButton = {
                                label: 'Active resolve',
                                linkClass: 'cursor-default',
                                iconClass: {
                                    'fa-check-square-o': metadata.resolve,
                                    'fa-square-o': !metadata.resolve
                                },
                                execute: function(event, context) {
                                    event.stopPropagation();
                                    if(troubleshooting.entityDebugEnabled()) {
                                        context.resolve = !context.resolve;
                                        resolveButton.iconClass = checkClass(context.resolve);
                                    }
                                },
                                save: function(context) {
                                    if(troubleshooting.entityDebugEnabled()) {
                                        var changed = metadata.resolve != context.resolve;
                                        metadata.resolve = context.resolve;
                                        return changed;
                                    }
                                    return false;
                                },
                                init: function(context) {
                                    context.resolve = metadata.resolve;
                                    resolveButton.iconClass = checkClass(metadata.resolve);
                                }
                            };
                            column.buttons.push(resolveButton);
                        }
                        if(!column.mainProperty && column.aggregatable) {
                            var extendButton = {
                                label: 'Expand',
                                execute: function(event, context) {
                                    event.stopPropagation();
                                    context.aggregate = !context.aggregate;
                                    extendButton.iconClass = checkClass(!context.aggregate);
                                },
                                save: function(context) {
                                    var changed = metadata.aggregate != context.aggregate;
                                    metadata.aggregate = context.aggregate;
                                    if(metadata.aggregate) {
                                        return policyAllowsDao.retrieve(undefined, $scope.getActualPolicyId()).then(function(response) {
                                            return $q.all(_(response.data.rows).chain().filter(function(rule) {
                                                return rule.rule_path == column.path;
                                            }).map(function(rule) {
                                                return policyAllowsDao.remove(rule.id);
                                            }).value());
                                        }).then(function() {
                                            return changed;
                                        });
                                    }
                                    return changed;
                                },
                                init: function(context) {
                                    context.aggregate = metadata.aggregate;
                                    extendButton.iconClass = checkClass(!metadata.aggregate);
                                }
                            };
                            column.buttons.push(extendButton);
                        }
                        
                        column.property = property;
                        if($scope.model.advancedConditions) {
                            column.filter = undefined;
                        }
                    }                        
                });

                $scope.buttons.policyColumnsPick.model = _(tableModel.columns).chain().filter(function(column) {
                    return !column.hidden;
                }).map(function(column) {
                    return [column.path, true];
                }).object().value();
                $scope.buttons.policyColumnsPick.options.columns = tableModel.columns;
                $scope.buttons.policyColumnsPick.options.columnNames = $scope.model.columnNames;

                findingsUpdate();
                $scope.tableOptions.columnsOrder = $scope.model.columnsOrderOverride = tableModel.columnsOrder;

                
                var foundColumn = null;
                var foundColumnId = null;
                _($scope.model.columnsToShow).each(function(column, columnId) {
                    if(column.sortIndex) {
                        foundColumn = column;
                        foundColumnId = columnId;
                    }
                });
                if(foundColumn) {
                    var columnId = policyPropertyPathToId.toId(foundColumnId);
                    var columnIdx = columnsHelper.getIdxById(tableModel.columns, columnId);
                    $scope.tableOptions.pagination.ordering = {
                        column: columnIdx,
                        columnName: columnId,
                        order: foundColumn.sortDirection.toLowerCase()
                    };
                }
            }
            $scope.tableModel = tableModel;
            $scope.rebuildActions();
        });

        $scope.$on('remove-filter', function(event, data) {
            // $scope.model.includeCondition.conditions = _($scope.model.includeCondition.conditions).filter(function(condition) {
            //     return !condition.generatedFrom || condition.generatedFrom.id != data.columnId;
            // });
            var columnsFilters = $scope.tableOptions.pagination.columnsFilters;
            if(columnsFilters && columnsFilters[data.columnId] && columnsFilters[data.columnId][data.filterId]) {
                columnsFilters[data.columnId].splice(data.filterId, 1);
                if(columnsFilters[data.columnId].length === 0) {
                    delete columnsFilters[data.columnId];
                }
            }
        });
        
        $scope.$on('policy-filter-change', function(event, data) {
            
            var generatedConditions = policyConditionToFiltersConverter.toConditions(data.columnsFilters);
            var conditions = _($scope.model.includeCondition.conditions).filter(function(condition) {
                return !condition.generatedFrom;
            });
            _(generatedConditions.conditions).each(function(condition) {
                conditions.push(condition);
            });
            if(!data.force && _($scope.model.includeCondition.conditions).isEqualsSpare(conditions)) {
                return; 
            }
            $scope.model.includeCondition.conditions = conditions;
            $scope.updatePolicy().then(function() {
                $scope.tableHelper.reload();
            });
        });
        
        $scope.$on('cell-action', policyCellActionHandler($scope));
        
        $scope.$on('remove-allow', function(event, data) {
            var allowId = data.attributes.allowId;
            var initialPolicyId = $scope.getActualPolicyId();
            $scope.updatePolicy().then(function() {
                var actualPolicyId = $scope.getActualPolicyId();
                if(initialPolicyId != actualPolicyId) {
                    return policyAllowsDao.retrieve(undefined, actualPolicyId).addUuid(true).then(function(response) {
                        var found = _(response.data.rows).find(function(row) {
                            return row.allow_name == data.attributes.allowName;
                        });
                        return policyAllowsDao.remove(found.id);
                    });
                }
                return policyAllowsDao.remove(allowId);
            }).then(function() {
                $scope.tableHelper.clearSelection();
                $scope.tableHelper.reload();
            });
        });
        
        $scope.$on('open-conditions-editor', function(event, data) {
            openConditionsEditor(null, $scope.internalModel.system);
        });
        
        $scope.$on('policy-table-adjust-done', function() {
            $scope.policyTableAdjuster.adjust();
        });

        $scope.$on('policy-table-order-change', function(event, data) {

            var orderPath = data.columnName ? policyPropertyPathToId.toPath(data.columnName) : null;
            var orderDir = data.order && data.order.toUpperCase() || null;
            var changed = false;

            _($scope.model.columnsToShow).each(function(column, path) {
                if ((column.sortIndex || column.sortDirection) && path !== orderPath) {
                    changed = true;
                    delete column.sortIndex;
                    delete column.sortDirection;
                }
                if (path === orderPath && (column.sortIndex !== 1 || column.sortDirection !== orderDir)) {
                    changed = true;
                    column.sortIndex = 1;
                    column.sortDirection = orderDir;
                }
            });
            if (!changed) {
                return;
            }
            $scope.updatePolicy().then(function() {
                $scope.tableHelper.reload();
            });
        });

        $scope.initialized = true;
    }, defaultHttpErrorHandler);
});
