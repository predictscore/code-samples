var m = angular.module('policy');

m.controller('PolicyDashboardController', function($scope, routeHelper) {
    $scope.groups = [{
        label: 'DLP',
        description: 'Description of DLP group',
        tags: ['dlp']
    }, {
        label: 'Malicious',
        description: 'Description of Malicious group',
        tags: ['malicious']
    }, {
        label: 'Usage',
        description: 'Description of Usage group',
        tags: ['usage']
    }, {
        label: 'Shadow-IT',
        description: 'Description of Shadow-IT group',
        tags: ['shadow-it']
    }, {
        label: 'User queries',
        description: 'Description of User queries group'
    }];
    
    $scope.openGroup = function(group) {
        if(group.tags) {
            routeHelper.redirectTo('policy-list', {}, {
                qs: {
                    tags: group.tags.join(',')
                }
            });
        } else {
            routeHelper.redirectTo('policy-list', {}, {
                qs: {
                    not_tags: _($scope.groups).chain().map(function(group) {
                        return group.tags;
                    }).flatten().value()
                }
            });
        }
    };
});