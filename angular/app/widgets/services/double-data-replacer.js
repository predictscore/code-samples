var m = angular.module('widgets');

m.factory('doubleDataReplacer', function() {
    return function(data, type) {
        if(type == 'number' || type == 'float') {
            return {
                first: Math.min(data.first, data.second),
                second: Math.max(data.first, data.second)
            };
        } else if(type == 'date') {
            var replace = moment(data.first).isAfter(data.second);
            return {
                first: replace ? data.second : data.first,
                second: replace ? data.first : data.second,
            };
        } else {
            return data;
        }
    };
});