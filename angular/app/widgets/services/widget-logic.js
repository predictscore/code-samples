var m = angular.module('widgets');

m.factory('widgetLogic', function($timeout, $injector, slowdownHelper) {
    var logics = {};

    function getLogic(logicName) {
        var logic = logics[logicName];
        if(_(logic).isUndefined()) {
            logic = $injector.get(logicName);
            if(_(logic).isUndefined()) {
                alert('Logic ' + logic.name + ' is not defined');
            }
            logics[logicName] = logic;
        }
        return  logic;
    }

    return function($scope, logic) {
        if(_(logic).isUndefined()) {
            return function() {};
        }
        var reinitLogicTimeout = null;
        if (_(logic.updateTick).isUndefined()) {
            logic.updateTick = 20000;
        }
        logic.originalUpdateTick = logic.updateTick;

        var destroyLogic = _.noop;
        function reinitLogic() {
            if(reinitLogicTimeout !== null) {
                $timeout.cancel(reinitLogicTimeout);
                reinitLogicTimeout = null;
            }
            destroyLogic();
            destroyLogic = getLogic(logic.name)($scope, logic, reinitLogic) || _.noop;
            var updateTick = logic.updateTick;
            updateTick = slowdownHelper.adjustInterval(updateTick);

            if(!_(updateTick).isNull() && !_(updateTick).isUndefined()) {
                reinitLogicTimeout = $timeout(reinitLogic, updateTick);
            }
        }
        reinitLogic();
        $scope.$on('$destroy', function() {
            if(reinitLogicTimeout !== null) {
                $timeout.cancel(reinitLogicTimeout);
                reinitLogicTimeout = null;
            }
            destroyLogic();
        });
    };
});