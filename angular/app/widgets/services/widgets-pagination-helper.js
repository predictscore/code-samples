var m = angular.module('widgets');

m.factory('widgetsPaginationHelper', function(ref) {
    return {
        currentPage: ref(0),
        totalPages: ref(0)
    };
});