var m = angular.module('widgets');

m.factory('tableExportHelper', function($q, modals) {
    return function (type, title, tableHelper) {
        var total = tableHelper.getTotal();
        var p;
        if (total > 10000 && type !== 'pdf' || (type === 'pdf' && total > 1000)) {
            p = modals.confirm({
                message: 'The export file seems very big and might impact your browser’s performance. Would you like to continue anyway?',
                title: 'Report is too big',
                okText: 'Continue'
            });
        } else {
            p = $q.when();
        }
        p.then(function () {
            if (type === 'pdf') {
                var win = window.open('', '_blank');
                tableHelper.makeExport(title, 'pdf').then(function (pdf) {
                    pdf.getDataUrl(function (result) {
                        win.location.href = result;
                    });
                }, function () {
                    win.close();
                });
            } else {
                tableHelper.makeExport(title, type).then(function (file) {
                    file.save();
                });
            }
        });
        modals.alert('Export started',{
            title: ' '
        });

    };
});
