var m = angular.module('dashboard');

m.factory('worldAccessMapLogicCore', function(ipDao, widgetPolling, eventsMapLogicCore, widgetSettings, $timeout) {
    return function($scope, logic, coreOptions) {
        
        function updateData() {
            if (_($scope.model.updateData).isFunction()) {
                $scope.model.updateData();
            }
        }

        function loadData() {
            var params = widgetSettings.params($scope.model.uuid);
            $timeout(function() {
                updateData();
                coreOptions.retrieve(params.days).then(function(response) {
                    $scope.model.data = response.data;
                    updateData();
                });
            });
        }

        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, function(params) {
            loadData();
        }, true);

        return function() {
            paramsWatch();
        };

    };
});