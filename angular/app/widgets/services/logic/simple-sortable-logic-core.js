var m = angular.module('widgets');

m.factory('simpleSortableLogicCore', function(tablePaginationHelper, $timeout, columnsHelper) {
    return function ($scope, logic, coreOptions) {

        if (!$scope.model.options) {
            $scope.model.options = {};
        }

        if (!$scope.model.options.pagination) {
            $scope.model.options.pagination = {
                page: 1
            };
        }
        if (!$scope.model.options.pagination.ordering) {
            $scope.model.options.pagination.ordering = {};
        }

        var options = {
            columns: $scope.model.columns,
            options: $.extend({
                autoUpdate: logic.updateTick,
                pagesAround: 2,
                pageSize: 10,
                pagination: {
                    page: 1,
                    ordering: {}
                }
            }, $scope.model.options)
        };

        logic.updateTick = null;
        if (_(options.options.autoUpdate).isNull()) {
            delete options.options.autoUpdate;
        }

        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        $scope.model.wrapper.isLoading = true;


        function getFilter() {
            if(_($scope.model.wrapper.filter).isObject())
                return $scope.model.wrapper.filter.value || '';
            return '';
        }

        var filterReloadTimeout = null;
        var filterWatch = $scope.$watch(getFilter, function(newValue, oldValue) {
            if(newValue == oldValue) {
                return;
            }
            if(filterReloadTimeout !== null) {
                $timeout.cancel(filterReloadTimeout);
            }
            filterReloadTimeout = $timeout(function() {
                filterReloadTimeout = null;
                $scope.model.wrapper.isLoading = true;
                $scope.tableHelper.reload();
            }, 700);
        });

        $scope.tableHelper = tablePaginationHelper($scope, $scope.model.options, options, function(args) {
            var result = coreOptions.retrieve();
            if (args.ordering.columnName) {
                result.order(args.ordering.columnName, args.ordering.order);
                if (!_(args.ordering).isEqual($scope.prevOrdering)) {
                    $scope.model.wrapper.isLoading = true;
                }
                $scope.prevOrdering = args.ordering;
            }
            var filterValue = getFilter();
            if (filterValue) {
                result.filter(filterValue);
            }
            if (args.columnsFilters) {
                var filterParams = columnsHelper.filtersToParams(args.columnsFilters);
                result.params(filterParams);
                if (!_(filterParams).isEqual($scope.prevColumnsFilters)) {
                    $scope.model.wrapper.isLoading = true;
                }
                $scope.prevColumnsFilters = filterParams;
            }
            result.pagination(args.offset, args.limit);
            result.after(function () {
                $scope.model.wrapper.isLoading = false;
            });
            return result;
        }, function(tableModel) {
            if(!_(tableModel).isNull()) {
                $scope.model.pagination = tableModel.pagination;
                $scope.model.data = tableModel.data;
                $scope.model.selected = tableModel.selected;
                $scope.model.rowClasses = tableModel.rowClasses;
                $scope.model.totalRows = $scope.tableHelper.getTotal();
            }
        });

        return function() {
            $scope.tableHelper.destroy();
            filterWatch();
        };
    };
});