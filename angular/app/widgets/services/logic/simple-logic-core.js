var m = angular.module('widgets');

m.factory('simpleLogicCore', function() {
    return function ($scope, logic, coreOptions) {
        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }
        coreOptions.retrieve().then(function(response) {
            $scope.model.wrapper.isLoading = false;
            if(!_(response.data.data).isEqual(angular.copy($scope.model.data))) {
                $scope.model.data = response.data.data;
            }
        }, function (error) {
            $scope.model.wrapper.isLoading = false;
        });

        return _.noop;
    };
});