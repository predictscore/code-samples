var m = angular.module('widgets');

m.factory('metaInfoLogicCore', function(widgetSettings, beautifyKey, $injector, $filter, objectTypeDao, $q, entityTypes, modulesManager, modulesDao, widgetPolling, commonDao) {
    var converters = {};
    function getConverter(converter) {
        if (_(converters[converter]).isUndefined()) {
            converters[converter] = $injector.get(converter);
            if (_(converters[converter]).isUndefined()) {
                converters[converter] = null;
            }
        }
        return converters[converter];
    }

    function createIdentifierLinkWithLocal (entityId, entityType, resolved) {
        if (!entityId || !entityType) {
            return entityId;
        }
        var local = entityTypes.toLocal(entityType);
        var saas = entityTypes.toSaas(entityType);
        var entity = resolved && resolved[entityType] && resolved[entityType][entityId];
        if (!local || !saas) {
            return entity && entity.main_identifier_value || entityId;
        }
        if (!entity || !entity.main_identifier_value) {
            return null;
        }
        return '#' + JSON.stringify({
                display: 'link',
                type: local,
                module: saas,
                id: entityId,
                text: entity && entity.main_identifier_value || entityId
            });
    }

    function createIdentifierLinkSingleProfile (entityId, entityType, resolved) {
        if (!entityId || !entityType) {
            return entityId;
        }
        var entity = resolved && resolved[entityType] && resolved[entityType][entityId];
        if (!entity || !entity.main_identifier_value) {
            return null;
        }
        return '#' + JSON.stringify({
                display: 'link',
                type: 'profile',
                id: entityId,
                entityType: entityType,
                text: entity && entity.main_identifier_value || entityId
            });
    }


    return function ($scope, logic, coreOptions) {
        var createIdentifierLink = (window.guiVersion === 2 ? createIdentifierLinkSingleProfile : createIdentifierLinkWithLocal);
        function isAutoAdvancedOnly(property) {
            if ($scope.autoWidgetType === 'ticket_view') {
                return !_(property.ticket_view_advanced_order).isUndefined() &&
                    _(property.ticket_view_default_order).isUndefined();
            }
            if ($scope.autoWidgetType === 'widget_view') {
                return _(property.widget_view).isUndefined();
            }
            return false;
        }

        function isAutoShowDefault(property) {
            if ($scope.autoWidgetType === 'ticket_view') {
                return !_(property.ticket_view_default_order).isUndefined();
            }
            if ($scope.autoWidgetType === 'widget_view') {
                return property.widget_view === true;
            }
            return false;
        }

        function isAutoShowAdvanced(property) {
            if ($scope.autoWidgetType === 'ticket_view') {
                return !_(property.ticket_view_advanced_order).isUndefined();
            }
            if ($scope.autoWidgetType === 'widget_view') {
                return property.widget_view !== false;
            }
            return false;
        }

        function autoSortField (advanced) {
            if ($scope.autoWidgetType === 'ticket_view') {
                return advanced ? 'ticket_view_advanced_order' : 'ticket_view_default_order';
            }
            return 'widget_view_order';
        }



        var objectTypes = widgetPolling.scope().objectTypes;

        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }

        if (_($scope.advancedModeEnabled).isUndefined() || (!coreOptions.autoByEntityInfo && !$scope.autoWidgetEnabled)) {
            $scope.advancedModeEnabled = !!coreOptions.advanced;
        }

        function convertKey(key) {
            if (coreOptions.rawOutput) {
                return key;
            }
            if (logic.niceKeyLabels && logic.niceKeyLabels[key]) {
                return logic.niceKeyLabels[key];
            }
            if ($scope.entityInfo && $scope.entityInfo[key] && $scope.entityInfo[key].label) {
                return  $scope.entityInfo[key].label;
            }
            if (logic.beautifyKey) {
                return beautifyKey(key);
            }
            return key;
        }

        function convertValue(value, key) {
            if (!coreOptions.rawOutput && logic.converters && logic.converters[key] && getConverter(logic.converters[key].converter)) {
                return getConverter(logic.converters[key].converter)(value, logic.converters[key].args);
            }
            if (!coreOptions.rawOutput && $scope.entityInfo && $scope.entityInfo[key] && $scope.entityInfo[key].options) {
                var found = _($scope.entityInfo[key].options).find(function (option) {
                    return option.id === value;
                });
                if (found) {
                    return found.label;
                }
            }
            if (_(value).isArray() && !coreOptions.rawOutput && logic.allArraysConverter && getConverter(logic.allArraysConverter.converter)) {
                return getConverter(logic.allArraysConverter.converter)(value, logic.allArraysConverter.args);
            }
            if (!coreOptions.rawOutput) {
                if ($scope.entityInfo && $scope.entityInfo[key] &&
                    $scope.entityInfo[key].entity_pointer) {
                    if (_(value).isArray()) {
                        return '#' + JSON.stringify({
                                display: 'linked-text',
                                'class': 'inline-array',
                                text: '[' + _(value).chain().map(function (id) {
                                    return createIdentifierLink(id, $scope.entityInfo[key].entity_pointer, $scope.resolvedIdentifiers);
                                }).filter(function (item) {
                                    return !_(item).isNull();
                                }).join(', ') + ']'
                            });
                    } else {
                        return createIdentifierLink(value, $scope.entityInfo[key].entity_pointer, $scope.resolvedIdentifiers);
                    }
                }
            }
            if (_(value).isObject() || _(value).isNull()) {
                return JSON.stringify(value);
            }
            if(/^https?:\/\//.test(value)) {
                return '#' + JSON.stringify({
                    display: 'external-link',
                    href: value,
                    text: value
                });
            }
            return $filter('localTimeIfIsoString')(value);
        }

        function pairVisibleByConfig (advanced, key, value) {
            if (!coreOptions.rawOutput && coreOptions.autoByEntityInfo && $scope.entityInfo) {
                if (_(value).isNull()) {
                    return false;
                }
                if (!$scope.entityInfo[key] || !$scope.entityInfo[key].label) {
                    return false;
                }
                if ($scope.entityInfo[key].advance_property && !advanced) {
                    return false;
                }
                return true;
            }
            if (!coreOptions.rawOutput && $scope.autoWidgetEnabled && $scope.entityInfo) {
                if (_(value).isNull()) {
                    return false;
                }
                if (!$scope.entityInfo[key]) {
                    return false;
                }

                if (advanced) {
                    return isAutoShowAdvanced($scope.entityInfo[key]);
                } else {
                    return isAutoShowDefault($scope.entityInfo[key]);
                }
            }
            if (logic.onlyWithLabels &&
                (!$scope.entityInfo || !$scope.entityInfo[key] || !$scope.entityInfo[key].label)) {
                return false;
            }
            return (coreOptions.rawOutput || (!_(value).isNull() && !_($scope.obsoleteProperties).contains(key))) &&
                (advanced || (!coreOptions.whiteList) || _(coreOptions.whiteList).contains(key)) &&
                !_(coreOptions.blackList).contains(key);
        }

        function pairVisible (advanced, key, value) {
            if (!pairVisibleByConfig(advanced, key, value)) {
                return false;
            }
            if (coreOptions.rawOutput || !$scope.entityInfo || !$scope.entityInfo[key] || !$scope.entityInfo[key].entity_pointer) {
                return true;
            }
            if (!_(value).isArray()) {
                return !_(createIdentifierLink(value, $scope.entityInfo[key].entity_pointer, $scope.resolvedIdentifiers)).isNull();
            }
            return !!_(value).find(function (item) {
                return !_(createIdentifierLink(item, $scope.entityInfo[key].entity_pointer, $scope.resolvedIdentifiers)).isNull();
            });
        }

        function buildData() {
            var advanced = true;
            if($scope.advancedModeEnabled) {
                advanced = widgetSettings.params($scope.model.uuid).advanced;
            }
            var data = $scope.cachedData;
            if (coreOptions.ordering === 'key') {
                data = _(data).sortBy('key');
            } else if ($scope.entityInfo && (coreOptions.ordering === 'entityOrder' || coreOptions.ordering === 'widgetViewOrder' || $scope.autoWidgetEnabled)) {
                var sortField = (coreOptions.ordering === 'widgetViewOrder' ? 'widget_view_order' : 'order');
                if ($scope.autoWidgetEnabled) {
                    sortField = autoSortField(advanced);
                }
                data = _(data).chain().sortBy(function (dataEl) {
                    return $scope.entityInfo[dataEl.key] && $scope.entityInfo[dataEl.key].label || 'zzzzzzz';
                }).sortBy(function (dataEl) {
                    return parseInt($scope.entityInfo[dataEl.key] && $scope.entityInfo[dataEl.key][sortField]) || 0;
                }).sortBy(function (dataEl) {
                    return isFinite(parseInt($scope.entityInfo[dataEl.key] && $scope.entityInfo[dataEl.key][sortField])) ? 0 : 1;
                }).value();
            }
            var pairs = _(data).reduce(function(memo, dataEl) {
                var key = dataEl.key;
                var value = dataEl.value;
                if(pairVisible(advanced, key, value)) {
                    var itemClass = {};
                    itemClass['info-field-' + key] = true;
                    if (logic.expandMap && logic.expandMap[key] && _(value).isObject()) {
                        _(value).each(function (mapValue, mapKey) {
                            memo.push({
                                'class': itemClass,
                                key: convertKey(key + ' ' + mapKey),
                                value: $filter('localTimeIfIsoString')(mapValue)
                            });
                        });
                        return memo;
                    } else {

                        memo.push({
                            'class': itemClass,
                            key: convertKey(key),
                            value: convertValue(value, key)
                        });
                    }
                }
                return memo;
            }, []);
            if (coreOptions.buttons) {
                var buttons = _(coreOptions.buttons).filter(function(button) {
                    return (advanced || !button.advanced) && button.label;
                });
                if(buttons.length) {
                    pairs.push({
                        buttons: buttons
                    });
                }
            }
            return pairs;
        }
        
        var loadObsoleteProperties = function () {
            return $q.when({
                data: []
            });
        };
        var loadEntityInfo = function () {
            return $q.when({
                data: []
            });
        };
        if (!coreOptions.rawOutput && coreOptions.entityType) {
            var saas = modulesManager.currentModule();
            var backendEntityType = entityTypes.toBackend(saas, coreOptions.entityType);
            loadObsoleteProperties = function () {
                return objectTypeDao.obsoleteProperties(saas, backendEntityType);
            };
            loadEntityInfo = function () {
                return modulesDao.entityInfo(saas, backendEntityType);
            };
            if (objectTypes && objectTypes.data.entities[backendEntityType] && objectTypes.data.entities[backendEntityType].properties) {
                $scope.entityInfoProperties = objectTypes.data.entities[backendEntityType].properties;
            }
        }
        if (!coreOptions.rawOutput && coreOptions.moduleLabels) {
            loadEntityInfo = function () {
                return modulesDao.entityInfo(coreOptions.moduleLabels.module, coreOptions.moduleLabels.entity);
            };
            if (objectTypes && objectTypes.data.entities[coreOptions.moduleLabels.entity] && objectTypes.data.entities[coreOptions.moduleLabels.entity].properties) {
                $scope.entityInfoProperties = objectTypes.data.entities[coreOptions.moduleLabels.entity].properties;
            }
        }
        $q.all([
            coreOptions.retrieve(),
            loadObsoleteProperties(),
            loadEntityInfo()
        ]).then(function(responses) {
            var resolveArr = [];

            $scope.obsoleteProperties = responses[1].data;
            $scope.model.entityData = responses[0].data;
            $scope.cachedData = _($scope.model.entityData).map(function (value, key) {
                return {
                    key: key,
                    value: value
                };
            });
            $scope.entityInfo = responses[2].data;

            if (coreOptions.extendEntityInfo) {
                $scope.entityInfo = $.extend({}, coreOptions.extendEntityInfo, $scope.entityInfo);
            }

            $scope.autoWidgetEnabled = false;

            if ($scope.entityInfo) {
                if (coreOptions.autoWidget) {
                    $scope.autoWidgetEnabled = !!_($scope.entityInfo).find(function (property) {
                        return !_(property.ticket_view_default_order).isUndefined() ||
                            !_(property.ticket_view_advanced_order).isUndefined();
                    });
                    if ($scope.autoWidgetEnabled) {
                        $scope.autoWidgetType = 'ticket_view';
                    } else {
                        $scope.autoWidgetEnabled = !!_($scope.entityInfo).find(function (property) {
                            return !_(property.widget_view).isUndefined();
                        });
                        if ($scope.autoWidgetEnabled) {
                            $scope.autoWidgetType = 'widget_view';
                        }
                    }
                }
                if (coreOptions.autoByEntityInfo || $scope.autoWidgetEnabled) {
                    var haveAdvanced = _($scope.entityInfo).find(function (property) {
                        if (coreOptions.autoByEntityInfo) {
                            return property.label && property.advance_property;
                        } else {
                            return isAutoAdvancedOnly(property);
                        }
                    });
                    var advancedFilter = false;
                    if (!haveAdvanced) {
                        haveAdvanced = _(coreOptions.buttons).find(function (button) {
                            return button.advanced;
                        });
                    }
                    if (haveAdvanced) {
                        $scope.advancedModeEnabled = true;
                        if ($scope.model.wrapper.filters) {
                            advancedFilter = _($scope.model.wrapper.filters).find(function (filter) {
                                return filter.value === 'advanced';
                            });
                        }
                        if (!advancedFilter) {
                            var newFilters = _($scope.model.wrapper.filters).map(function (filter) {
                                return filter;
                            });
                            newFilters.push({
                                "type": "flag",
                                "text": "Advanced",
                                "value": "advanced",
                                "local": true
                            });
                            $scope.model.wrapper.filters = newFilters;
                        }
                    } else {
                        $scope.advancedModeEnabled = false;
                        if ($scope.model.wrapper.filters) {
                            advancedFilter = _($scope.model.wrapper.filters).find(function (filter) {
                                return filter.value === 'advanced';
                            });
                            if (advancedFilter && $scope.model.wrapper.filters.length > 1) {
                                $scope.model.wrapper.filters = _($scope.model.wrapper.filters).filter(function (filter) {
                                    return filter.value !== 'advanced';
                                });
                            } else {
                                $scope.model.wrapper.filters = null;
                            }
                        }
                    }
                }
                if ($scope.entityInfoProperties) {
                    _($scope.cachedData).each(function (el) {
                        if (el.value) {
                            var found = _($scope.entityInfoProperties).find(function (property) {
                               return property.name.search('\\(' + el.key + '\\)$') !== -1;
                            });
                            if (found && found.entity_pointer && $scope.entityInfo[el.key]) {
                                $scope.entityInfo[el.key].entity_pointer = found.entity_pointer;
                                if (_(el.value).isArray()) {
                                    _(el.value).each(function (id) {
                                        resolveArr.push({
                                            'entity_type': $scope.entityInfo[el.key].entity_pointer,
                                            'entity_id': id
                                        });
                                    });
                                } else {
                                    resolveArr.push({
                                        'entity_type': $scope.entityInfo[el.key].entity_pointer,
                                        'entity_id': el.value
                                    });
                                }
                            }
                        }
                    });
                }
            }
            return commonDao.entityIdentifierMulti(resolveArr)
                .then(function (resolved) {
                    $scope.resolvedIdentifiers = resolved;
                });
        }).then(function () {
            $scope.model.wrapper.isLoading = false;
            $scope.model.data = buildData();
        });

        var paramsWatch = _.noop;
        if($scope.model.uuid) {
            paramsWatch = $scope.$watch(function () {
                return widgetSettings.params($scope.model.uuid);
            }, function (params) {
                if (coreOptions.processParams) {
                    coreOptions.processParams(params);
                }
                $scope.model.data = buildData();
            }, true);
        }

        return function() {
            paramsWatch();
        };
    };
});