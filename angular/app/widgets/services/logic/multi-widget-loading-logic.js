var m = angular.module('widgets');

m.factory('multiWidgetLoadingLogic', function() {
    return function ($scope, logic, coreOptions) {
        $scope.model.wrapper.showLoading = true;

        return $scope.$watch(function () {
            return !!_($scope.model.widgets).find(function (widget) {
                return widget.wrapper.isLoading;
            });
        }, function (isLoading) {
            $scope.model.wrapper.isLoading = isLoading;
        });
    };
});