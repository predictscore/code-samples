var m = angular.module('widgets');

m.factory('eventsListLogicCore', function(eventsDao, columnsHelper, $timeout, widgetSettings, tablePaginationHelper, $q, widgetPolling, $filter, path, appScanStatusHelper, appIconHelper) {
    return function ($scope, logic, coreOptions) {
        var data = [];
        var clearData = false;
        var after = '2000-01-01';

        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        $scope.model.wrapper.isLoading = true;


        if(_(coreOptions.retrieveTypes).isFunction()) {
            coreOptions.retrieveTypes().then(function (response) {
                $scope.model.wrapper.filters = _(response.data).map(function (eventType) {
                    return {
                        type: 'flag',
                        local: true,
                        state: true,
                        text: eventType,
                        value: eventType
                    };
                });
                if (coreOptions.permanentEvents) {
                    $scope.model.wrapper.filters.unshift({
                        type: 'flag',
                        local: true,
                        state: true,
                        text: 'Avanan Events',
                        value: 'avanan_events'
                    });
                }
            });
        }

        $scope.tableOptions = {
            pagesAround: 2,
            pageSize: 10,
            pagination: {
                page: 1,
                ordering: {},
                filter: ''
            }
        };

        var options = {
            columns: $scope.model.columns,
            options: {
                autoUpdate: logic.updateTick
            }
        };
        logic.updateTick = null;


        function eventsConverter(response, args) {
            if(clearData) {
                clearData = false;
                data = [];
            }

            var timeColumnIdx = columnsHelper.getIdxById($scope.model.columns, coreOptions.timeColumnName);
            var descriptionColumnIdx = columnsHelper.getIdxById($scope.model.columns, coreOptions.descriptionColumn);
            var idColumnIdx = columnsHelper.getIdxById($scope.model.columns, coreOptions.idColumn);
            var imageColumnIdx = columnsHelper.getIdxById($scope.model.columns, coreOptions.imageColumn);
            var checkboxColumnIdx = columnsHelper.getIdxById($scope.model.columns, coreOptions.checkboxColumn);
            if (response.data.data.length > 0) {
                after = response.data.data[0][timeColumnIdx].originalText;
            }

            _.chain(response.data.data).clone().reverse().each(function(d) {
                data.splice(0, 0, d);
            });

            if(data.length > 100) {
                data = _(data).first(100);
            }
            var returnData = data;


            function buildRow(time, moduleName, appName, moduleLabel, module, statistics, itemId, isHistory, scanEntity, scanResult) {
                var row = _($scope.model.columns).map(function() {
                    return {
                        originalText: null,
                        text: null
                    };
                });
                row[idColumnIdx] = {
                    originalText: itemId,
                    text: itemId
                };
                row[timeColumnIdx] = {
                    originalText: time,
                    text: $filter('localTime')(time)
                };
                row[descriptionColumnIdx] = {
                    originalText: 'Inspected by ' + moduleLabel,
                    text: 'Inspected by ' + appIconHelper.appIconByApp(module, true) + '#' + JSON.stringify({
                        display: 'text',
                        'class': $.extend({
                            'float-right': true
                        }, appScanStatusHelper.statisticsToClass(statistics, moduleName, scanResult)),
                        clickEvent: {
                            name: 'open-app-status',
                            appName: appName,
                            moduleName: moduleName,
                            appLabel: moduleLabel,
                            scanStatus: appScanStatusHelper.statisticsToStatus(statistics, moduleName, scanResult),
                            isHistory: isHistory,
                            time: time,
                            scanEntity: scanEntity
                        },
                        tooltip: appScanStatusHelper.eventViewTooltip(statistics, moduleName, scanResult)
                    })
                };
                row[imageColumnIdx] = {
                    originalText: 'Avanan Service',
                    text: 'Avanan Service'
                };
                if (checkboxColumnIdx) {
                    row[checkboxColumnIdx] = {
                        originalText: true,
                        text: '#' + JSON.stringify({
                            display: 'html',
                            text: '<div class="fa fa-check green-check"></div>'
                        })
                    };
                }
                return row;
            }

            if(args.permanentEvents) {
                var filter = getFilter().toLowerCase();
                var permanentData = _(args.permanentEvents.data).chain().compact().sortBy(coreOptions.permanentTimeColumn)
                    .filter(function (row) {
                        return ("avanan service".indexOf(filter) !== -1) ||
                            ("inspected by".indexOf(filter) !== -1) ||
                            (row.found.label.toLowerCase().indexOf(filter) !== -1);
                    })
                    .reverse().first(100).map(function (event, idx) {
                        var appName = event.found.name;
                        var moduleLabel = event.found.label;
                        var itemId = appName;
                        if (event.isHistory) {
                            itemId += event[coreOptions.permanentTimeColumn];
                        }
                        return buildRow(event[coreOptions.permanentTimeColumn], event.found.module_name, appName, moduleLabel, event.found, event.statistics, itemId, event.isHistory, event.found.sec_entity, event);
                    }).value();

                returnData = _(returnData.concat(permanentData)).chain().sortBy(function (item) {
                    return item[timeColumnIdx].originalText;
                }).reverse().value();
            }

            return $q.when({
                data: {
                    data: returnData,
                    pagination: {
                        total: data.length
                    }
                }
            });
        }


        $scope.tableHelper = tablePaginationHelper($scope, $scope.model.options, options, function(args) {
            var params = [];
            if ($scope.model.uuid) {
                params = _(widgetSettings.params($scope.model.uuid)).chain().map(function(param, value) {
                    return param !== true ? value : null;
                }).filter(function(param) {
                    return param !== null;
                }).value();
            }
            if (coreOptions.retrieve) {
                var result = coreOptions.retrieve(after)
                    .filter(getFilter())
                    .order(coreOptions.timeColumnName, 'desc')
                    .pagination(0, 100);
                
                var eventTypes = params.join(',');
                if(eventTypes) {
                    result.params({
                        event_types: eventTypes
                    });
                }

                if (coreOptions.permanentEvents && !_(params).contains('avanan_events')) {
                    result = result.preFetch(coreOptions.permanentEvents(), 'permanentEvents');
                }

                return result.converter(eventsConverter , _.identity)
                    .after(function () {
                        $scope.model.wrapper.isLoading = false;
                    }).onRequestFail(function (error) {
                        $scope.model.wrapper.isLoading = false;
                        return $q.reject(error);
                    });

            } else {
                if (coreOptions.permanentEvents && !_(params).contains('avanan_events')) {
                    return coreOptions.permanentEvents()
                        .converter(function (response) {
                            return eventsConverter({
                                data: {
                                    data: []
                                }
                            }, {
                                permanentEvents: response
                            });
                        }).after(function () {
                            $scope.model.wrapper.isLoading = false;
                        }).onRequestFail(function (error) {
                            $scope.model.wrapper.isLoading = false;
                            return $q.reject(error);
                        });
                } else {
                    return $q.when();
                }
            }
        }, function(tableModel) {
            if(!_(tableModel).isNull()) {
                $scope.model.pagination = tableModel.pagination;
                $scope.model.data = tableModel.data;
                $scope.model.selected = tableModel.selected;
                $scope.model.rowClasses = tableModel.rowClasses;
            }
        });

        function getFilter() {
            if(_($scope.model.wrapper.filter).isObject())
                return $scope.model.wrapper.filter.value || '';
            return '';
        }

        function reload(newValue, oldValue) {
            if (newValue !== oldValue) {
                after = '2000-01-01';
                clearData = true;
                $scope.tableHelper.reload();
            }
        }

        var filterWatch = $scope.$watch(getFilter, reload);
        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, reload, true);

        return function() {
            $scope.tableHelper.destroy();
            filterWatch();
            paramsWatch();
        };
    };
});