var m = angular.module('widgets');

m.factory('columnsHelper', function(feature, $location) {
    function translateDate(dateData) {
        return moment(dateData).utc().format('YYYY-MM-DD HH:mm:ss');
    }

    return {
        getPrimaryIdx: function(columns) {
            var primaryIdx = 0;
            _(columns).each(function(column, idx) {
                if(column.primary) {
                    primaryIdx = idx;
                }
            });
            return primaryIdx;
        },
        getLabelIdx: function(columns, labelIdxSearch) {
            labelIdxSearch = labelIdxSearch || _.constant(true);
            var labelIdx = 0;
            _(columns).each(function(column, idx) {
                if(column.entityLabel && labelIdxSearch(column)) {
                    labelIdx = idx;
                }
            });
            return labelIdx;
        },
        getIdxById: function(columns, id) {
            var columnIdx = 0;
            _(columns).each(function(column, idx) {
                if(column.id == id) {
                    columnIdx = idx;
                }
            });
            return columnIdx;
        },
        getIdxByIdStrict: function(columns, id) {
            var columnIdx = -1;
            _(columns).each(function(column, idx) {
                if(column.id == id) {
                    columnIdx = idx;
                }
            });
            return columnIdx;
        },
        updateFacets: function(columns, facets) {
            _(columns).each(function(column) {
                if(column.facetsName) {
                    var oldFacets = column.facets;
                    column.facets = facets[column.facetsName];
                    if(_(oldFacets).isObject()) {
                        _(oldFacets.variants).each(function(variant) {
                            var found = _(column.facets.variants).find(function(existVariant) {
                                return existVariant.id == variant.id;
                            });
                            if(_(found).isUndefined()) {
                                column.facets.variants.push({
                                    id: variant.id,
                                    label: variant.label,
                                    count: 0
                                });
                            }
                        });
                    }
                }
            });
        },
        isColumnVisible: function(column, tableOptions, isExport, showOptional) {
            if(!column) {
                return false;
            }
            var isColumnVisible = (tableOptions || {}).isColumnVisible || _.constant(true);
            var optionallyHidden = false;
            if (!showOptional && column.optional && tableOptions.optionalFilter) {
                var optional = _(tableOptions.optionalFilter).find(function (filter) {
                    return column.id === filter.id;
                });
                optionallyHidden = optional && !optional.state;
            }
            var qsRequireHidden = false;
            if(_(column.qsRequire).isObject()) {
                qsRequireHidden = _(column.qsRequire).some(function(value, key) {
                    if(_(value).isString()) {
                        value = [value];
                    }
                    return !_(value).contains($location.search()[key]);
                });
            }
            return !column.hidden && !optionallyHidden && !qsRequireHidden &&
                (_(column.feature).isUndefined() || feature[column.feature]) && isColumnVisible(column, isExport);
        },
        filtersToParams: function (filters) {
            var params = {};
            _(filters).each(function (conditions, param) {
                var filter = conditions[0];
                switch (filter.type) {
                    case 'date':
                        switch (filter.operator) {
                            case '<':
                                params[param + '_lt'] = translateDate(filter.data);
                                break;
                            case '>=':
                                params[param + '_ge'] = translateDate(filter.data);
                                break;
                            case 'between':
                                params[param + '_lt'] = translateDate(filter.data.second);
                                params[param + '_ge'] = translateDate(filter.data.first);
                                break;
                        }
                        break;
                    case 'text':
                        if (filter.operator === 'contains') {
                            params[param] = filter.data;
                        }
                        break;
                    case 'enum':
                        params[param] = _(conditions).chain().filter(function (condition) {
                            return condition.type === 'enum' && condition.operator === 'is';
                        }).pluck('data').value().join(',');
                        break;
                    case 'boolean':
                        if (filter.operator === 'is') {
                            params[param] = filter.data;
                        }
                        break;
                    case 'number':
                        switch (filter.operator) {
                            case '<':
                                params[param + '_lt'] = filter.data;
                                break;
                            case '>=':
                                params[param + '_ge'] = filter.data;
                                break;
                            case 'between':
                                params[param + '_lt'] = filter.data.second;
                                params[param + '_ge'] = filter.data.first;
                                break;
                        }
                        break;
                }
            });
            return params;
        }
    };
});