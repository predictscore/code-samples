var m = angular.module('widgets');

m.factory('widgetPolling', function (http, $timeout, routeHelper, widgetSettings, defaultHttpErrorHandler, commonWidgetsConverter, slowdownHelper, $route, $q) {
    var scope = {};
    var data = {
        isNew: true,
        rows: [],
        pagination: {}
    };
    var timeouts = {};
    var autoUpdate = false;

    function updater(id, type) {
        var updaterData = data.rows;
        updaterData[type][id].forceUpdate = forceUpdate(type, id);
        var updateTick = updaterData[type][id].updateTick || 0;
        var updateUrl = updaterData[type][id].updateUrl;
        if (_(updateUrl).isUndefined() || updateTick === 'none') {
            return;
        }

        function update() {
            var params = widgetSettings.params(updaterData[type][id].uuid);
            http.get(updateUrl).then(function (result) {
                if (updaterData !== data.rows) {
                    return;
                }
                var resultData = $.extend(true, {}, updaterData[type][id], result.data);
                if (_(resultData.updateData).isFunction()) {
                    resultData.updateData();
                }
                resultData.forceUpdate = forceUpdate(type, id);
                updaterData[type][id] = resultData;
                updateTick = updaterData[type][id].updateTick;
                updateUrl = updaterData[type][id].updateUrl;
                var adjustedTick = slowdownHelper.adjustInterval(updateTick);
                if (autoUpdate && _(updateUrl).isString() && updateTick !== 'none' && !_(adjustedTick).isUndefined()) {
                    timeouts[id + '-' + type] = $timeout(function () {
                        update();
                    }, adjustedTick);
                }
            }, function () {
                var adjustedTick = slowdownHelper.adjustInterval(updateTick);
                if (autoUpdate && !_(adjustedTick).isUndefined()) {
                    timeouts[id + '-' + type] = $timeout(function () {
                        update();
                    }, adjustedTick);
                }
            });
        }

        function forceUpdate(type, id) {
            return function () {
                $timeout.cancel(timeouts[id + '-' + type]);
                timeouts[id + '-' + type] = $timeout(function () {
                    update();
                });
            };
        }

        var adjustedTick = slowdownHelper.adjustInterval(updateTick);
        if (autoUpdate && !_(adjustedTick).isUndefined()) {
            timeouts[id + '-' + type] = $timeout(function () {
                update();
            }, adjustedTick);
        }
    }

    function createUpdaters() {
        for (var i = 0; i < data.rows.length; i++) {
            for (var j = 0; j < data.rows[i].length; j++) {
                updater(j, i);
            }
        }
    }

    var reloadFn = _.noop;

    var subscribers = {};

    return {
        scope: function (s) {
            if (!_(s).isUndefined()) {
                scope = s;
            }
            return scope;
        },
        setLocal: function (widgetsData, reset, softReset) {
            softReset = (softReset && !data.isNew);
            if(reset || data.isNew) {
                this.stopUpdating();
                autoUpdate = true;
                if (softReset) {
                    data = widgetsData;
                    data.rows = data.rows || [data.widgets];
                    createUpdaters();
                } else {
                    $timeout(function () {
                        data = widgetsData;
                        data.rows = data.rows || [data.widgets];
                        createUpdaters();
                    });
                }
            } else {
                var newData = widgetsData;
                newData.rows = newData.rows || [data.widgets];
                $.extend(true, data, newData);
                createUpdaters();
            }
        },
        updateWidgets: function (resource, commonWidgets) {
            autoUpdate = true;
            function update() {
                var req = http.get(resource);
                if (!_(commonWidgets).isUndefined()) {
                    req = req.preFetch(http.get(commonWidgets), 'commonWidgets');
                }
                return req.converter(commonWidgetsConverter, function (preFetches) {
                    return {
                        commonWidgets: preFetches.commonWidgets
                    };
                }).then(function (widgets) {
                    data = widgets.data;
                    if(data.title) {
                        $route.current.fullTitle = data.title;
                        if(data.title.indexOf('#') == -1) {
                            $route.current.fullTitle = '#' + JSON.stringify({
                                display: 'html',
                                text: '<strong>' + data.title + '</strong>'
                            });
                        }
                    }
                    var resolvers = [];
                    _(data.rows).each(function(row) {
                        _(row).each(function(widget) {
                            if(widget.logic) {
                                widget.logic.resolver = $q.defer();
                                resolvers.push(widget.logic.resolver.promise);
                            }
                        });
                    });
                    createUpdaters();
                    return $q.all(resolvers).then(function() {
                        return $timeout();
                    });
                }, defaultHttpErrorHandler);
            }

            return update();
        },
        stopUpdating: function () {
            scope = {};
            autoUpdate = false;
            _(_(timeouts).values()).each(function (timeout) {
                $timeout.cancel(timeout);
            });
            timeouts = {};
            data = {
                isNew: true,
                infoBoxes: [],
                panels: [],
                pagination: {}
            };
        },
        getRows: function () {
            return data.rows;
        },
        getPagination: function () {
            return data.pagination;
        },
        reload: function (newReloadFn) {
            if (_(newReloadFn).isFunction()) {
                reloadFn = newReloadFn;
            }
            return reloadFn();
        },
        on: function (eventName, callback) {
            if (!subscribers[eventName]) {
                subscribers[eventName] = [];
            }
            var subscribeItem = {
                cb: callback
            };
            subscribers[eventName].push(subscribeItem);
            return function () {
                if (subscribers[eventName]) {
                    subscribers[eventName] = _(subscribers[eventName]).without(subscribeItem);
                    if (subscribers[eventName].length === 0) {
                        delete subscribers[eventName];
                    }
                }
            };
        },
        emit: function (eventName, data) {
            _(subscribers[eventName]).each(function (item) {
                item.cb(data);
            });
        }
    };
});