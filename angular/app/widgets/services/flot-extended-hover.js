var m = angular.module('widgets');

m.factory('flotExtendedHover', function($timeout, $compile) {
    return function (scope, el) {
        var extendedHover = {
            root: $('#ng-view'),
            shown: false
        };
        function assignExtendedHover() {
            if (extendedHover.element) {
                extendedHover.element.remove();
            }
            extendedHover.element = $compile('<div class="flot-extended-hover"><div linked-text="extendedHover"></div>')(scope);
            extendedHover.root.append(extendedHover.element);
            extendedHover.element.on('mouseover', function () {
                hoverHideCancel();
            });
            extendedHover.element.on('mouseout', function () {
                hoverHide();
            });
            if (!extendedHover.scopeDestroySet) {
                extendedHover.scopeDestroySet = true;
                scope.$on('$destroy', function () {
                    if (extendedHover.element) {
                        extendedHover.element.remove();
                    }
                });
            }
            if (!extendedHover.plotHoverSet) {
                extendedHover.plotHoverSet = true;
                el.on('plothover', function (event, pos, item) {
                    if (item && item.series.hoverText) {
                        hoverHideCancel();
                        if (_(extendedHover.shown).isEqual(item)) {
                            return;
                        }
                        $timeout(function (){
                            extendedHover.shown = item;
                            scope.extendedHover = item.series.hoverText;
                            var rootOffset = extendedHover.root.offset();
                            if (((pos.pageX - rootOffset.left) / extendedHover.root.width()) > 0.5) {
                                extendedHover.element.css({
                                    top: (pos.pageY - rootOffset.top - 2) + 'px',
                                    left: 'auto',
                                    right: (extendedHover.root.outerWidth(true) - pos.pageX + rootOffset.left - 2) + 'px',
                                    display: 'block'
                                });
                            } else {
                                extendedHover.element.css({
                                    top: (pos.pageY - rootOffset.top - 2) + 'px',
                                    left: (pos.pageX - rootOffset.left - 2) + 'px',
                                    right: 'auto',
                                    display: 'block'
                                });
                            }

                        });
                    } else {
                        hoverHide();
                    }
                });
            }
        }

        function hoverHideCancel() {
            if (extendedHover.hideTimer) {
                $timeout.cancel(extendedHover.hideTimer);
                extendedHover.hideTimer = null;
            }
        }

        function hoverHide() {
            hoverHideCancel();
            extendedHover.hideTimer = $timeout(overHidePerform, 100);
        }
        function overHidePerform() {
            extendedHover.hideTimer = null;
            scope.extendedHover = '';
            extendedHover.shown = false;
            extendedHover.element.css('display', 'none');
        }
        return {
            init: assignExtendedHover
        };
    };
});
