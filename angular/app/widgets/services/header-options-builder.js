var m = angular.module('widgets');

m.factory('headerOptionsBuilder', function(policyFilterTypes) {
    return function(override, property) {
        var variantsOverride = null;
        if(property) {
            variantsOverride = _(property.variants).map(function(variant) {
                variant.operatorIds = ['in'];
                return variant;
            });
        }
                    
        var result = {
            enum: {
                type: 'enum',
                input: 'none',
                operatorId: 'is',
                operatorLabel: 'is',
                operators: []
            },
            multiEnum: {
                type: 'multiEnum',
                input: 'none',
                operators: [],
                multiple: false
            },
            boolean: {
                type: 'boolean',
                input: 'none',
                operatorId: 'is',
                operatorLabel: 'is',
                operators: [{
                    id: 'false',
                    label: 'false',
                    type: 'none',
                    data: false
                }, {
                    id: 'true',
                    label: 'true',
                    type: 'none',
                    data: true
                }]
            },
            text: {
                type: 'text',
                input: 'text',
                operators: [{
                    id: 'contains',
                    label: 'contains',
                    type: 'single'
                }]
            },
            number: {
                type: 'number',
                input: 'text',
                parser: function(data) {
                    var result = parseInt(data);
                    if (!isFinite(result)) {
                        throw new Error('Incorrect data');
                    }
                    return result;
                },
                operators: [{
                    id: '>=',
                    label: '>=',
                    type: 'single'
                }, {
                    id: '<',
                    label: '<',
                    type: 'single'
                }, {
                    id: 'between',
                    label: 'between',
                    type: 'double'
                }]
            },
            float: {
                type: 'float',
                input: 'text',
                parser: function(data) {
                    var result = parseFloat(data);
                    if (!isFinite(result)) {
                        throw new Error('Incorrect data');
                    }
                    return result;
                },
                operators: [{
                    id: '>=',
                    label: '>=',
                    type: 'single'
                }, {
                    id: '<',
                    label: '<',
                    type: 'single'
                }, {
                    id: 'between',
                    label: 'between',
                    type: 'double'
                }]
            },
            date: {
                type: 'date',
                input: 'date',
                operators: [{
                    id: '>=',
                    label: '>=',
                    type: 'single'
                }, {
                    id: '<',
                    label: '<',
                    type: 'single'
                }, {
                    id: 'between',
                    label: 'between',
                    type: 'double'
                }]
            }
        };
        
        if(override) {
            $.extend(true, result, override);
            
            //Correct overriding of operators
            _(override).each(function(value, key) {
                if(value.operators) {
                    result[key].operators = $.extend(true, [], value.operators);
                }
            });
        }
        
        if(variantsOverride) {
            _(result).each(function(option, optionId) {
                if(optionId == 'multiEnum') {
                    _(variantsOverride).each(function(variant) {
                        _(option.operators).each(function(operator) {
                            if(_(variant.operatorIds).contains(operator.id)) {
                                operator.variants = operator.variants || [];
                                operator.variants.push({
                                    id: variant.id,
                                    label: variant.label
                                });
                            }
                        });
                    });
                } else if(optionId == 'enum') {
                    option.operators = _(variantsOverride).map(function(variant) {
                        return {
                            id: variant.id,
                            label: variant.label,
                            type: 'none',
                            data: variant.id
                        };
                    });
                } else {
                    _(option.operators).each(function(operator) {
                        var headerVariant = _(variantsOverride).find(function(variant) {
                            return '' + variant.id == operator.id;
                        }) || {};
                        operator.label = headerVariant.label || operator.label;
                    });
                }
            });
        }

        return result;
    };
});