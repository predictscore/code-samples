var m = angular.module('widgets');

m.factory('widgetSettings', function() {
    var settings = localStorage.getItem('widgetSettings');
    settings = (settings && JSON.parse(settings)) || {};

    return {
        params: function(widgetUuid, newParams) {
            if(!_(newParams).isUndefined()) {
                settings[widgetUuid] = newParams;
                localStorage.setItem('widgetSettings', JSON.stringify(settings));
            }
            return settings[widgetUuid] || {};
        },
        param: function(widgetUuid, key, value) {
            if(!_(value).isUndefined()) {
                settings[widgetUuid] = settings[widgetUuid] || {};
                settings[widgetUuid][key] = value;
                localStorage.setItem('widgetSettings', JSON.stringify(settings));
            }
            return settings[widgetUuid] && settings[widgetUuid][key];
        }
    };
});