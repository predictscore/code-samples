var m = angular.module('widgets');

m.factory('tablePaginationHelper', function($timeout, $filter, columnsHelper, $q, $location, base64, slowdownHelper, exports, workingIndicatorHelper, feature, modals, logging, linkedText) {
    return function($scope, tableOptions, options, args, setTableModel) {
        tableOptions.reloadTable = 0;
        if(_(args).isFunction()) {
            args = {
                retrieve: args
            };
        }
        options = $.extend(true, options, $.extend(true, {
            options: {
                highLightNew: true,
                forceServerProcessing: true
            }
        }, options));

        var destroyed = false;
        var inMemory = false;
        var ignoreNewColumnsColoring = true;
        var inMemoryFilter = '';
        var results;
        var afterReload = null;

        if(tableOptions.saveState) {
            var paramName = 'pagination';
            if(_(tableOptions.saveState).isString()) {
                paramName = tableOptions.saveState;
            }
            var savedPagination = $location.search()[paramName];
            if(_(savedPagination).isString()) {
                try {
                    tableOptions.pagination = JSON.parse(base64.decode(savedPagination));
                } catch (e) {

                }
            }
        }
        tableOptions.pageSize = tableOptions.pagination.pageSize || tableOptions.pageSize || Math.pow(2, 53) - 1;

        var memory = {
            totalRows: undefined,
            data: undefined,
            rowClasses: [],
            selected: [],
            ordering: {
                column: undefined,
                order: undefined
            },
            prevTableModel: {
                data: undefined,
                pagination: undefined
            }
        };

        function resolvePromises(data) {
            if(tableOptions.lazyPromiseResolve) {
                return $q.when();
            }
            var promises = [];
            var idxToColumn = {};
            _(data).each(function(row) {
                _(row).chain().filter(function(column) {
                    return _(column.text).isObject();
                }).each(function(column) {
                    promises.push($q.when(column.text));
                    idxToColumn[promises.length - 1] = column;
                });
            });

            return $q.all(promises).then(function(results) {
                _(results).each(function(result, idx) {
                    idxToColumn[idx].text = result;
                });

                return $q.when();
            });
        }

        function setTableModelCached(tableModel, filter) {
            tableModel.selected = memory.selected;

            var rowClasses = [];
            if(_(tableOptions.filter).isFunction()) {
                tableModel.data = _(tableModel.data).filter(function(row, idx) {
                    rowClasses[idx] = {};
                    var result = tableOptions.filter(row, tableModel.columns);
                    if(_(result).isBoolean()) {
                        return result;
                    }
                    if(_(result).isString()) {
                        rowClasses[idx][result] = true;
                    }
                    if(_(result).isObject()) {
                        $.extend(rowClasses[idx], result);
                    }
                    return true;
                });
            }
            tableModel.rowClasses = $.extend(tableModel.rowClasses || {}, rowClasses);

            resolvePromises(tableModel.data).then(function() {
                function expandEnchant(row) {
                    if(!_(row.expanded).isUndefined()) {
                        return;
                    }
                    row.expanded = false;
                    row.isExpandable = function() {
                        return args.isExpandable(row);
                    };

                    row.toggleExpand = function() {
                        if(row.expanded === false) {
                            row.expanded = null;
                            args.expand(row, filter).then(function(response) {
                                row.expanded = true;
                                row.subrows = _(response.data.data).map(function(subrow) {
                                    subrow.loaded = true;
                                    return subrow;
                                });
                            });
                        } else if(row.expanded === true) {
                            row.expanded = false;
                            delete row.subrows;
                        }
                    };
                }

                function isDataEqual(newData, oldData, checkSubRows) {
                    if(!_(angular.copy(oldData)).isEqual(newData)) {
                        return false;
                    }

                    return !checkSubRows || !_(newData).some(function(row, idx) {
                        return !_(row.subrows).isEqual(angular.copy(oldData[idx].subrows));
                    });
                }

                function finishProcessing() {
                    if(options.options.expandable) {
                        _(tableModel.data).each(function(row) {
                            expandEnchant(row);
                        });
                    }
                    tableModel.pagination = $.extend(true, {}, memory.prevTableModel.pagination, tableModel.pagination);
                    if(_(tableModel.pagination).isEqual(memory.prevTableModel.pagination)) {
                        tableModel.pagination = memory.prevTableModel.pagination;
                    }
                    setTableModel(tableModel);
                    memory.prevTableModel = tableModel;
                }

                if(!isDataEqual(tableModel.data, memory.prevTableModel.data, false)) {
                    finishProcessing();
                } else {
                    $q.all(_(memory.prevTableModel.data).map(function (row) {
                        if (!_(row.subrows).isUndefined()) {
                            return args.expand(row);
                        }
                        return $q.when(null);
                    })).then(function (responses) {
                        _(responses).each(function (response, idx) {
                            var row = tableModel.data[idx];
                            expandEnchant(row);
                            if (_(response).isNull()) {
                                return;
                            }
                            if(_(memory.prevTableModel.data[idx].subrows).isUndefined()) {
                                return;
                            }
                            row.expanded = true;
                            row.subrows = _(response.data.data).map(function (subrow) {
                                subrow.loaded = true;
                                return subrow;
                            });
                        });

                        if (isDataEqual(tableModel.data, memory.prevTableModel.data, true)) {
                            tableModel.data = memory.prevTableModel.data;
                        }

                        finishProcessing();
                    });
                }
            });
        }

        function sortingNumber(text) {
            var prefixLength = 32;
            var postfixLength = 8;
            var result = '' + text;
            if (result.indexOf('.') == -1) {
                result += '.';
            }
            while(result.indexOf('.') < prefixLength) {
                result = '0' + result;
            }
            while (result.length < prefixLength + 1 + postfixLength) {
                result += '0';
            }
            return result;
        }

        function updateBackendOrdering(ordering) {
            if(!_(options.columns).isUndefined() && !_(ordering.column).isUndefined()) {
                var orderingColumn = options.columns[ordering.column];
                if (!_(orderingColumn).isUndefined()) {
                    var sortable = orderingColumn.sortable || {};
                    if (sortable.id) {
                        ordering.column = columnsHelper.getIdxById(options.columns, sortable.id);
                        ordering.columnName = sortable.id;
                    }
                }
            }
        }

        function setFromMemory(page, ordering, filter) {
            var primaryColumn = columnsHelper.getPrimaryIdx(options.columns);
            if(_(memory.data).isUndefined() || !_(memory.ordering).isEqual(ordering)) {
                memory.data = results.data;

                if(!_(filter).isUndefined() && filter.length) {
                    var expected = filter.toLowerCase();
                    memory.data = _(memory.data).filter(function (row, idx) {
                        return !_(row).chain().find(function (val, colIdx) {
                            if (!columnsHelper.isColumnVisible(options.columns[colIdx], tableOptions)) {
                                return false;
                            }
                            if (val && val.alwaysShow) {
                                return true;
                            }
                            if(_(val).isObject() && !_(val.text).isUndefined()) {
                                var text = linkedText.toText(val.text).replace(/<[^>]+>/g, ''); // fast and dirty internal use only way to remove html
                                return ~text.toLowerCase().indexOf(expected);
                            }
                            return false;
                        }).isUndefined().value();
                    });
                }

                if(!_(ordering).isUndefined()) {
                    var orderingColumn = ordering.column;
                    if(!_(orderingColumn).isUndefined()) {
                        var sortable = options.columns[orderingColumn].sortable;
                        if (!_(sortable).isObject()) {
                            sortable = {};
                        }
                        if (sortable.id) {
                            orderingColumn = columnsHelper.getIdxById(options.columns, sortable.id);
                        }
                        if (!_(orderingColumn).isUndefined()) {
                            memory.data = $filter('orderBy')(memory.data, function (val) {
                                var orderPrefix = val[orderingColumn].text;
                                if (sortable.original) {
                                    orderPrefix = val[orderingColumn].originalText;
                                }
                                if (sortable.originalResolved) {
                                    orderPrefix = val[orderingColumn].originalResolved;
                                }
                                if (sortable.number) {
                                    orderPrefix = sortingNumber(orderPrefix);
                                }
                                if(_(orderPrefix).isNull() || _(orderPrefix).isUndefined()) {
                                    orderPrefix = '~';
                                }
                                if(_(orderPrefix).isArray()) {
                                    orderPrefix = orderPrefix.join('/');
                                }
                                orderPrefix = orderPrefix + '-';

                                return orderPrefix + sortingNumber(val[primaryColumn].text);
                            }, ordering.order == 'desc');
                        }
                    }
                }

                var rowClasses = memory.rowClasses = [];

                if(_(tableOptions.filter).isFunction()) {
                    memory.data = _(memory.data).filter(function(row, idx) {
                        rowClasses[idx] = {};
                        var result = tableOptions.filter(row, options.columns);
                        if(_(result).isBoolean()) {
                            return result;
                        }
                        if(_(result).isString()) {
                            rowClasses[idx][result] = true;
                        }
                        if(_(result).isObject()) {
                            $.extend(rowClasses[idx], result);
                        }
                        return true;
                    });
                }

                if(ignoreNewColumnsColoring || !options.options.highLightNew) {
                    ignoreNewColumnsColoring = false;
                } else {
                    var newRows = [];
                    _(memory.data).each(function (row, idx) {
                        var id = row[primaryColumn].text;
                        if (_(_(memory.prevTableModel.data).find(function (d) {
                                return d[primaryColumn].text == id;
                            })).isUndefined()) {
                            rowClasses[idx] = rowClasses[idx] || {};
                            rowClasses[idx]['new'] = true;
                            newRows.push(idx);
                        }
                    });
                    if (newRows.length) {
                        $timeout(function () {
                            _(newRows).each(function (row) {
                                rowClasses[row]['new'] = false;
                            });
                        }, 500);
                    }
                }
            }
            memory.totalRows = memory.data.length;
            var total = Math.ceil(memory.data.length / tableOptions.pageSize);
            page = Math.min(page, total);
            var tableModel = $.extend({}, results, options);
            tableModel.data = memory.data.slice((page - 1) * tableOptions.pageSize, page * tableOptions.pageSize);
            tableModel.rowClasses = memory.rowClasses.slice((page - 1) * tableOptions.pageSize, page * tableOptions.pageSize);
            tableModel.pagination = {
                current: page,
                total: total
            };
            tableModel.columnsOrder = tableOptions.columnsOrder;
            setTableModelCached(tableModel, filter);
        }

        function saveState(pagination) {
            var paramName = 'pagination';
            if(_(tableOptions.saveState).isString()) {
                paramName = tableOptions.saveState;
            }
            $location.replace();
            $location.search(paramName, base64.encode(JSON.stringify(pagination)));
        }

        var retrieveDelay = 500;
        var retrieveTimeout = null;
        var autoReloadTimeout = null;
        var requestTimeout = $q.defer();

        function reload(pagination, force, isAutoReload) {
            var page = pagination.page;
            var ordering = $.extend(true, {}, pagination.ordering);
            var filter = pagination.filter;
            var columnsFilters = pagination.columnsFilters;
            var facets = pagination.facets;
            
            function isColumnsOrderCorrect() {
                var visibleColumns = _(options.columns).chain().filter(function(column) {
                    return columnsHelper.isColumnVisible(column, tableOptions, false, true);
                }).map(function(column) {
                    return column.id;
                }).uniq().value();
                tableOptions.columnsOrder = tableOptions.columnsOrder || [];
                if(tableOptions.columnsOrder.length !== visibleColumns.length ||
                   _(visibleColumns).intersection(tableOptions.columnsOrder).length !== visibleColumns.length) {
                    logging.log('Exist columns order is: ' + JSON.stringify(_(tableOptions.columnsOrder).sortBy(_.identity)));
                    logging.log('But visible columns is: ' + JSON.stringify(_(visibleColumns).sortBy(_.identity)));
                    return false;
                }
                return true;
            }

            function performRetrieve() {
                updateBackendOrdering(ordering);
                
                if(tableOptions.saveState) {
                    saveState(pagination);
                }

                if (!isAutoReload && tableOptions.showWorkingIndicator) {
                    workingIndicatorHelper.init($scope);
                    workingIndicatorHelper.show(tableOptions.workingIndicatorDisableGui);
                }

                if(autoReloadTimeout !== null) {
                    $timeout.cancel(autoReloadTimeout);
                    autoReloadTimeout = null;
                }
                requestTimeout.resolve();
                requestTimeout = $q.defer();
                var promise = args.retrieve({
                    offset: (page - 1) * tableOptions.pageSize,
                    limit: tableOptions.pageSize,
                    filter: filter,
                    ordering: _(ordering).isEqual({}) ? (options.options.defaultOrdering || {}) : ordering,
                    columnsFilters: columnsFilters,
                    facets: facets
                });
                if(_(promise.timeout).isFunction()) {
                    promise = promise.timeout(requestTimeout.promise);
                }
                promise.then(function (response) {
                    tableOptions.reload = false;
                    if(afterReload) {
                        afterReload.resolve();
                        afterReload = null;
                    }
                    if(destroyed) {
                        return;
                    }
                    if(!_(response.data.columns).isUndefined()) {
                        options.columns = response.data.columns;
                    }
                    if(response.data.deferreds) {
                        _(response.data.deferreds).each(function(deferred) {
                            deferred.resolve();
                        });
                    }
                        
                    var fillColumnsOrder = false;
                    if(_(tableOptions.columnsOrder).isUndefined() || !isColumnsOrderCorrect()) {
                        tableOptions.columnsOrder = [];
                        fillColumnsOrder = true;
                    }
                    _(options.columns).each(function(column, idx) {
                        var visible = columnsHelper.isColumnVisible(column, tableOptions, false, true);
                        if(fillColumnsOrder && visible) {
                            tableOptions.columnsOrder.push(column.id);
                        }
                        column.dnd = {
                            data: column.id,
                            draggable: options.options.dnd ? {} : false,
                            droppable: options.options.dnd ? {
                                onDrop: function(draggable, droppable) {
                                    var draggableIdx = null;
                                    var droppableIdx = null;
                                    _(tableOptions.columnsOrder).each(function(column, idx) {
                                        if(column == draggable) {
                                            draggableIdx = idx;
                                        } else if(column == droppable) {
                                            droppableIdx = idx;
                                        }
                                    });
                                    if(!_(draggableIdx).isNull() && !_(droppableIdx).isNull()) {
                                        if(draggableIdx < droppableIdx) {
                                            tableOptions.columnsOrder.splice(droppableIdx + 1, 0, draggable);
                                            tableOptions.columnsOrder.splice(draggableIdx, 1);
                                        } else if(draggableIdx > droppableIdx) {
                                            tableOptions.columnsOrder.splice(draggableIdx, 1);
                                            tableOptions.columnsOrder.splice(droppableIdx, 0, draggable);
                                        }
                                    }
                                    memory.prevTableModel.columnsOrder = tableOptions.columnsOrder;
                                }
                            } : false
                        };
                    });
                    var page = pagination.page;
                    var ordering = pagination.ordering;
                    var filter = pagination.filter;

                    retrieveTimeout = $timeout(function() {
                        retrieveTimeout = null;
                    }, retrieveDelay);
                    results = response.data;
                    if (results.pagination.total == results.data.length && !options.options.forceServerProcessing) {
                        inMemory = true;
                        inMemoryFilter = filter;
                        setFromMemory(page, ordering, filter);
                    } else {
                        inMemory = false;
                        memory.totalRows = results.pagination.total;
                        var tableModel = $.extend(true, results, options, {
                            columnsOrder: tableOptions.columnsOrder,
                            pagination: {
                                total: Math.ceil(results.pagination.total / tableOptions.pageSize)
                            }
                        });

                        setTableModelCached(tableModel, filter);
                    }
                    
                    initWatchers();

                    if (!isAutoReload && tableOptions.showWorkingIndicator) {
                        workingIndicatorHelper.hide();
                    }

                    if(!_(slowdownHelper.adjustInterval(options.options.autoUpdate)).isUndefined()) {
                        autoReloadTimeout = $timeout(function () {
                            reload(tableOptions.pagination, true, true);
                        }, slowdownHelper.adjustInterval(options.options.autoUpdate));
                    }
                }, function(error) {
                    tableOptions.reload = false;
                    if (!isAutoReload && options.options.showMessageOnFail && error.status > 0) {
                        if (tableOptions.showWorkingIndicator) {
                            workingIndicatorHelper.hide();
                        }
                        var message = error.data.message || 'This query is taking longer than expected to complete, please retry shortly.';
                        return modals.alert(message, {
                            title: ' ',
                            singleton: 'reload_fail_message'
                        });
                    }
                    if (tableOptions.showWorkingIndicator) {
                        workingIndicatorHelper.hideIfDisabled();
                    }
                    var afterCancelInterval = slowdownHelper.adjustInterval(5000);
                    if(!destroyed && !feature.disableAjaxRetryOnFail && error.status > 0 && afterCancelInterval) { //Angular behaviour changed, now "canceled status" is -1, for compatibility, > 0 should ignore both -1 and 0 (previous "canceled" status)
                        autoReloadTimeout = $timeout(function () {
                            reload(tableOptions.pagination, true, true);
                        }, afterCancelInterval);
                    }
                });
            }

            if(inMemory && (_(filter).isUndefined() || ~filter.indexOf(inMemoryFilter)) && !force && !options.options.forceServerProcessing) {
                tableOptions.reload = false;
                setFromMemory(page, ordering, filter);
                if(tableOptions.saveState) {
                    saveState(pagination);
                }
            } else {
                if(retrieveTimeout !== null) {
                    $timeout.cancel(retrieveTimeout);
                    retrieveTimeout = $timeout(performRetrieve, retrieveDelay);
                } else {
                    $timeout(performRetrieve);
                }
            }
        }
        
        var watchers = {
            init: false
        };
        function initWatchers() {
            if(watchers.init) {
                return;
            }
            watchers.init = true;
            watchers.reloadWatch = $scope.$watch(function() {
                return tableOptions.reload;
            }, function() {
                if(tableOptions.reload) {
                    console.log('Reloading started');
                    var forceReload = tableOptions.forceReload;
                    var noIndicator = tableOptions.noIndicator;
                    $timeout(function() {
                        reload(tableOptions.pagination, forceReload, noIndicator);
                        $timeout(function() {
                            tableOptions.forceReload = false;
                            tableOptions.noIndicator = false;
                        });
                    });
                }
            });
    
            watchers.pageWatch = $scope.$watch(function() {
                return tableOptions.pagination.page;
            }, function(newValue, oldValue) {
                console.log('Initiate reload because pageWatch');
                tableOptions.reload = true;
            });
    
            watchers.orderingWatch = $scope.$watch(function() {
                return tableOptions.pagination.ordering;
            }, function(newValue, oldValue) {
                if(oldValue == newValue) {
                    return;
                }
                if(tableOptions.orderingChangedEvent) {
                    $scope.$emit(tableOptions.orderingChangedEvent, tableOptions.pagination.ordering);
                } else {
                    console.log('Initiate reload because orderingWatch');
                    tableOptions.reload = true;
                }
            }, true);
    
            var filterReloadTimeout = null;
            watchers.filterWatch = $scope.$watch(function() {
                return tableOptions.pagination.filter;
            }, function(newValue, oldValue) {
                if(newValue == oldValue) {
                    return;
                }
                if(filterReloadTimeout !== null) {
                    $timeout.cancel(filterReloadTimeout);
                }
                filterReloadTimeout = $timeout(function() {
                    console.log('Initiate reload because filterWatch');
                    filterReloadTimeout = null;
                    tableOptions.pagination.page = 1;
                    tableOptions.reload = true;
                }, 700);
            });
    
            watchers.toggleWatch = $scope.$watch(function() {
                return tableOptions.toggleState;
            }, function(newValue, oldValue) {
                if(newValue == oldValue) {
                    return;
                }
                console.log('Initiate reload because columnsFiltersWatch');
                tableOptions.pagination.page = 1;
                tableOptions.reload = true;
                tableOptions.forceReload = true;
            });
    
            watchers.columnsFiltersWatch = $scope.$watch(function() {
                return tableOptions.pagination.columnsFilters;
            }, function(newValue, oldValue) {
                if(newValue == oldValue) {
                    return;
                }
                if(tableOptions.eventOnFilterChange) {
                    $scope.$emit(tableOptions.eventOnFilterChange, {
                        columnsFilters: newValue
                    });
                    return;
                }
                if(tableOptions.pagination.columnsFilterNoReload) {
                    tableOptions.pagination.columnsFilterNoReload = false;
                    return;
                }
                console.log('Initiate reload because columnsFiltersWatch');
                tableOptions.pagination.page = 1;
                tableOptions.reload = true;
            }, true);
    
            watchers.facetsWatch = $scope.$watch(function() {
                return tableOptions.pagination.facets;
            }, function(newValue, oldValue) {
                if(newValue == oldValue) {
                    return;
                }
                console.log('Initiate reload because facetsWatch');
                tableOptions.pagination.page = 1;
                tableOptions.reload = true;
            }, true);
    
            watchers.pageSizeWatch = $scope.$watch(function () {
                return tableOptions.pageSize;
            }, function(newValue, oldValue) {
                tableOptions.pagination.pageSize = tableOptions.pageSize;
                tableOptions.pagination.page = 1;
                if(newValue == oldValue || !newValue) {
                    return;
                }
                console.log('Initiate reload because pageSizeWatch');
                tableOptions.reload = true;
            });
            
            watchers.reloadTableWatch = $scope.$watch(function() {
                return tableOptions.reloadTable;
            }, function(newValue, oldValue) {
                console.log('Initiate reload because reloadTableWatch');
                tableOptions.reload = true;
            });
        }

        initWatchers();

        $scope.$on('$destroy', function() {
            api.destroy();
        });

        var api = {
            getTotal: function() {
                return memory.totalRows;
            },
            getSelected: function() {
                return memory.selected;
            },
            getSelectedIds: function() {
                var primaryIdx = columnsHelper.getPrimaryIdx(options.columns);
                return _(memory.selected).map(function(line) {
                    return line[primaryIdx].text;
                });
            },
            getLabel: function(id, onlySelected, labelIdxSearch) {
                var data = memory.data;
                if(onlySelected) {
                    data = memory.selected;
                }
                var primaryIdx = columnsHelper.getPrimaryIdx(options.columns);
                var labelIdx = columnsHelper.getLabelIdx(options.columns, labelIdxSearch);
                if(_(labelIdx).isUndefined()) {
                    labelIdx = primaryIdx;
                }
                return _(data).find(function(line) {
                    return line[primaryIdx].text == id;
                })[labelIdx].originalText;
            },
            clearSelection: function() {
                memory.selected.length = 0;
            },
            reload: function(force, noIndicator) {
                if(_(force).isUndefined()) {
                    force = true;
                }
                console.log('Initiate reload because reload()');
                tableOptions.reload = true;
                tableOptions.forceReload = force;
                tableOptions.noIndicator = noIndicator;
                if(!afterReload) {
                    afterReload = $q.defer();
                }
                return afterReload.promise; 
            },
            destroy: function() {
                if(watchers.init) {
                    watchers.reloadWatch();
                    watchers.toggleWatch();
                    watchers.pageWatch();
                    watchers.orderingWatch();
                    watchers.filterWatch();
                    watchers.columnsFiltersWatch();
                    watchers.facetsWatch();
                    watchers.pageSizeWatch();
                    watchers.reloadTableWatch();
                    watchers.init = false;
                }

                if(retrieveTimeout !== null) {
                    $timeout.cancel(retrieveTimeout);
                    retrieveTimeout = null;
                }
                if(autoReloadTimeout !== null) {
                    $timeout.cancel(autoReloadTimeout);
                    autoReloadTimeout = null;
                }
                requestTimeout.reject();
                destroyed = true;
                setTableModel(null);
            },
            makeExport: function(title, exportType) {
                var pagination = $.extend(true, {}, tableOptions.pagination);
                var ordering = pagination.ordering;
                updateBackendOrdering(ordering);
                var filter = pagination.filter;
                var columnsFilters = pagination.columnsFilters;
                var facets = pagination.facets;

                return args.retrieve({
                    filter: filter,
                    ordering: _(ordering).isEqual({}) ? (options.options.defaultOrdering || {}) : ordering,
                    columnsFilters: columnsFilters,
                    facets: facets,
                    export: true
                }).then(function(response) {
                    return $q.when(exports[exportType](title, response.data, tableOptions, options));
                });
            },
            options: options
        };
        return api;
    };
});