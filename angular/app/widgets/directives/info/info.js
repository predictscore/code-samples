var m = angular.module('widgets');

m.directive('info', function(path, widgetLogic) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('widgets').directive('info').template(),
        scope: {
            model: '=info'
        },
        link: function($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);
        }
    };
});