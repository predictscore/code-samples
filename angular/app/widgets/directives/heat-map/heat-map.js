var m = angular.module('widgets');

m.directive('heatMap', function($timeout, widgetLogic) {
    L.Icon.Default.imagePath = '/assets/css/images/';
    return {
        restrict: 'A',
        scope: {
            model: '=heatMap'
        },
        link: function ($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);
            var mapSingleton = null;
            var lastMarkerId = 0;

            function getMap() {
                if(_(mapSingleton).isNull()) {
                    mapSingleton = L.map(element[0]);//.fitWorld().zoomIn();
                    // L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(mapSingleton);
                    L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
                        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
                        subdomains: 'abcd',
                        maxZoom: 19,
                        minZoom: 1
                    }).addTo(mapSingleton);
                    mapSingleton.fitWorld();
                }
                return mapSingleton;
             }

            $scope.model.data = $scope.model.data || [];

            $scope.$watch('model', function() {
                $scope.model.data = $scope.model.data || [];
                $scope.model.updateData = updateData;
            });

            var heatLayer = false;

            function updateData() {
                var map = getMap();
                var totalCount = 0;
                var maxCount = 0;
                _($scope.model.data).each(function (point) {
                    totalCount += (point.count || 0);
                    if (point.count > maxCount) {
                        maxCount = point.count;
                    }
                });
                if (heatLayer) {
                    map.removeLayer(heatLayer);
                }

                var heatParams = {radius: 15, blur: 25, minOpacity: 0.2, max: maxCount};
                if ($scope.model.heatParams) {
                    heatParams.radius = $scope.model.heatParams.radius || 12;
                    heatParams.blur = $scope.model.heatParams.blur || 12;
                    heatParams.minOpacity = ($scope.model.heatParams.minOpacity || 35) / 100;
                    heatParams.maxZoom = $scope.model.heatParams.maxZoom || 7;
                    heatParams.max = ($scope.model.heatParams.ignoreMax ? 1 : maxCount);
                }

                heatLayer = L.heatLayer(_($scope.model.data).map(function(point) {
                    return [point.geo_lat, point.geo_lon, point.count];
                }), heatParams).addTo(map);
            }


            $scope.$on('$destroy', function() {
                if(!_(mapSingleton).isNull()) {
                    mapSingleton.remove();
                }
            });
        }
    };
});