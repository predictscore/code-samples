var m = angular.module('widgets');

m.directive('flot', function($timeout, widgetLogic, $location, flotExtendedHover) {
    return {
        restrict: 'A',
        scope: {
            model: '=flot'
        },
        link: function ($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);
            $scope.plot = null;

            var extendedHover = flotExtendedHover($scope, element);

            var legendContainer = null;
            if ($scope.model && $scope.model.logic && $scope.model.logic.outerLegend) {
                legendContainer = $('<div class="flot-outer-legend"></div>');
                if ($scope.model.logic.outerLegend === 'top') {
                    legendContainer.insertBefore(element);
                } else {
                    legendContainer.insertAfter(element);
                }
            }


            var updateTimeout = null;
            function updateFlot() {
                if(updateTimeout !== null) {
                    return;
                }
                updateTimeout = $timeout(function() {
                    updateTimeout = null;
                    $scope.model.updateData = updateData;
                    element.empty();
                    if ($scope.plot) {
                        $scope.plot.destroy();
                        $scope.plot = null;
                    }
                    if (legendContainer) {
                        legendContainer.empty();
                    }
                    if($scope.model.data && $scope.model.data.length) {
                        var options = $.extend({}, $scope.model.flotOptions, {
                            interaction: {
                                redrawOverlayInterval: 10
                            }
                        });
                        if(options.hideBeforeCreate) {
                            element.hide();
                        }
                        if (legendContainer) {
                            options.legend = $.extend({}, options.legend, {
                                container: legendContainer
                            });
                        }
                        $scope.plot = $.plot(element, $scope.model.data, options);
                        if ($scope.model.flotOptions.extendedHover) {
                            extendedHover.init();
                        }
                        element.show();
                        if($scope.model.onFlotRender) {
                            $scope.model.onFlotRender();
                        }
                    }
                });
            }
            $scope.$watch('model.data', function(newVal, oldVal) {
                if(!_(newVal).isEqual(oldVal)) {
                    updateFlot();
                }
            }, true);
            if($scope.model.watchFlotOptions !== false) {
                $scope.$watch('model.flotOptions', function (newVal, oldVal) {
                    if (!_(newVal).isEqual(oldVal)) {
                        updateFlot();
                    }
                }, true);
            }
            updateFlot();

            function updateData() {
                var self = this;
                if(_(self.dataUpdate).isArray()) {
                    _(self.dataUpdate).each(function (newData, dataId) {
                        self.data[dataId].data.splice(0, newData.length);
                        _(newData).each(function (e) {
                            self.data[dataId].data.push(e);
                        });
                    });
                }
                delete self.dataUpdate;
            }

            $scope.$on('$destroy', function() {
                if($scope.plot !== null) {
                    if ($scope.model.flotOptions.tooltip) {
                        $scope.plot.hideTooltip();
                    }
                    $scope.plot.destroy();
                    $scope.plot = null;
                }
                if (legendContainer) {
                    legendContainer.remove();
                    legendContainer = null;
                }
            });
            if ($scope.model.flotOnClick) {
                element.on('plotclick', $scope.model.flotOnClick);
            }
            element.on('plotclick', function (event, pos, item) {
                if (item && item.series && item.series.link) {
                    $timeout(function () {
                        $location.url(item.series.link);
                    });
                }
            });
            element.on('plothover', function (event, pos, item) {
                if (item && ($scope.model.flotOnClick || item.series.link)) {
                    element.css('cursor', 'pointer');
                } else {
                    element.css('cursor', 'default');
                }
            });
        }
    };
});