var m = angular.module('widgets');

m.directive('columnFilter', function(path, doubleDataReplacer) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('widgets').directive('column-filter').template(),
        scope: {
            model: '=columnFilter',
            options: '=',
            disabled: '=?',
            columnId: '=?'
        },
        link: function($scope, element, attr) {
            $scope.options = $.extend(true, {
                parser: _.identity
            }, $scope.options);
            
            $scope.internal = {};
            
            $scope.timeshiftOptions = [{
                id: 'minutes',
                label: 'Minutes'
            }, {
                id: 'hours',
                label: 'Hours'
            }, {
                id: 'days',
                label: 'Days'
            }, {
                id: 'months',
                label: 'Months'
            }, {
                id: 'years',
                label: 'Years'
            }];
            
            $scope.isNotAllowed = function() {
                if(_($scope.options.notAllowed).isFunction()) {
                    return $scope.options.notAllowed($scope.columnId);
                }
                return $scope.options.notAllowed;
            }();
            
            $scope.isAggregatedOperatorAllowed = function() {
                if(_($scope.options.aggregatedOperatorAllowed).isFunction()) {
                    return $scope.options.aggregatedOperatorAllowed($scope.columnId);
                }
                return $scope.options.aggregatedOperatorAllowed;
            }();
            
            $scope.actualOperators = _($scope.options.operators).filter(function(operator) {
                if(operator.aggregatedOnly && !$scope.isAggregatedOperatorAllowed) {
                    return false;
                }
                return true;
            });
            
            function emptyCondition() {
                return {
                    data: {
                        first: '',
                        second: ''
                    },
                    not: '',
                    aggregatedOperator: $scope.isAggregatedOperatorAllowed ? 'all' : undefined 
                };
            }
            
            function setInternalModel () {
                var model;
                if(!$scope.model || $scope.model.length === 0) {
                    model = [undefined];
                } else {
                    model = $scope.model;
                }
                $scope.internalModel = _(model).chain().filter(function(model) {
                    if(model && _(model.notNull).isBoolean()) {
                        $scope.internal.notNull = model.notNull;
                        return false;
                    }
                    return true;
                }).map(function(model) {
                    var result = emptyCondition();
                    if(model) {
                        result.disabled = model.disabled;
                        result.not = model.not ? 'true' : '';
                        result.aggregatedOperator = model.aggregatedOperator;
                        result.options = model.options;
                        result.operator = _($scope.options.operators).find(function (operator) {
                            return operator.id === model.operator || _(model.data).isEqual(operator.data);
                        });
                        if(_(model.data).isBoolean()) {
                            result.data = model.data;
                        } else if(_(model.data).isArray()) {
                            result.data.first = model.data;
                        } else if (_(model.data).isObject()) {
                            result.data.first = model.data.first;
                            result.data.second = model.data.second;
                        } else  if(result.operator.type === 'time-shift') {
                            var splitted = model.data.split(' ');
                            result.data.first = parseInt(splitted[0]);
                            result.data.second = _($scope.timeshiftOptions).find(function(opt) {
                                return opt.id == splitted[1];
                            });
                        } else {
                            result.data.first = model.data;
                        }
                    }
                    
                    if(!result.operator && $scope.options.operators.length == 1) {
                        result.operator = _($scope.options.operators).first();
                    }
                    return result;
                }).value();
                
                if($scope.internalModel.length > 1) {
                    $scope.options.multiple = true;
                }
                
                if(model.length == 1 && _(model).first() && _(model).first().notNull) {
                    $scope.internalModel.push(emptyCondition());
                }
            }

            
            function updateModel() {
                if($scope.disabled) {
                    $scope.model = undefined;
                    return;
                }
                $scope.model = _($scope.internalModel).chain().map(function(internal) {
                    try {
                        if (!internal.operator) {
                            throw new Error('No operator');
                        }
                        if(internal.operator.id == 'in' && (!internal.data.first || !internal.data.first.length)) {
                            throw new Error('Not selected variants for in operator');
                        }
                        internal.options = internal.options || {};
                        internal.options = _(internal.operator.options).chain().map(function(option) {
                            var state = internal.options[option.id];
                            if(_(state).isUndefined()) {
                                state = option.defaultState;
                            }
                            return [option.id, state];
                        }).object().value();
                        var result = {
                            type: $scope.options.type,
                            operator: $scope.options.operatorId || internal.operator.id,
                            operatorLabel: $scope.options.operatorLabel || internal.operator.label,
                            not: !!internal.not,
                            options: internal.options
                        };
                        
                        if(internal.aggregatedOperator) {
                            result.aggregatedOperator = internal.aggregatedOperator;
                        }
                        
                        if($scope.options.disabled) {
                            result.disabled = true;
                        }
                        
                        var data = internal.operator.data;
                        if(_(data).isUndefined()) {
                            data = {};
                            if(internal.operator.type !== 'none') {
                                data.first = $scope.options.parser(internal.data.first);
                            }
                            if(internal.operator.type === 'double' || internal.operator.type === 'time-shift') {
                                data.second = $scope.options.parser(internal.data.second);
                            }
                        }
                        
                        if ((internal.operator.type === 'single' || internal.operator.type === 'multiEnum') && !_(data.first).isUndefined()) {
                            result.data = data.first;
                        } else if (internal.operator.type === 'double' && !_(data.first).isUndefined() && !_(data.second).isUndefined()) {
                            result.data = doubleDataReplacer(data, $scope.options.type);
                        } else if (internal.operator.type === 'time-shift') {
                            result.data = data.first + ' ' + data.second.id;
                        } else if (internal.operator.type === 'none') {
                            result.data = data;
                            if(result.operator != 'is_null') {
                                result.dataLabel = internal.operator.label;
                            }
                        } else {
                            throw new Error('Wrong data');
                        }
                        $scope.canAddCondition = true;
                        return result;
                    } catch(e) {
                        $scope.canAddCondition = false;
                        return null;
                    }
                }).filter(_.isObject).value();
                
                if($scope.internal.notNull) {
                    $scope.model.push({
                        notNull: true
                    });
                }
                
                if($scope.model.length === 0) {
                    $scope.model = undefined;
                }

                if ($scope.model && $scope.model.length === 1 &&
                    $scope.model[0].operator === 'contains' &&
                    $scope.model[0].type === 'text' &&
                    !$scope.model[0].data
                    ) {
                    $scope.model = undefined;
                }
            }

            $scope.$watch('internalModel', updateModel, true);
            $scope.$watch('internal', updateModel, true);
            $scope.$watch('disabled', updateModel, true);

            setInternalModel();

            $scope.$watch('model', function () {
                if ($scope.model === '') {
                    setInternalModel();
                }
            });
            
            $scope.removeCondition = function(index) {
                $scope.internalModel.splice(index, 1);  
            };
            
            $scope.addCondition = function() {
                if($scope.canAddCondition) {
                    $scope.internalModel.push(emptyCondition());
                }
            };
            
            $scope.onOptionClick = function(condition, option) {
                condition.options[option.id] = !condition.options[option.id];
            };
            
            $scope.optionClass = function(condition, option) {
                var result = {
                    'gray-text': !condition.options[option.id],
                    'black-text': condition.options[option.id]
                };
                if(_(option.class).isString()) {
                    result[option.class] = true;
                }
                if(_(option.class).isObject()) {
                    $.extend(result, option.class);
                }
                return result;
            };
            
            $scope.notNullClick = function(event) {
                event.preventDefault();
                $scope.internal.notNull = !$scope.internal.notNull;
            };
        }
    };
});
