var m = angular.module('widgets');

m.directive('metaInfo', function(path, widgetLogic) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('widgets').directive('meta-info').template(),
        scope: {
            model: '=metaInfo'
        },
        link: function($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);

            $scope.isDefined = function(data) {
                return !_(data).isUndefined();
            };

            function transformClass (c) {
                if (!c) {
                    return {};
                }
                if (!_(c).isObject()) {
                    return _(c.split(/\s+/)).chain().map(function (c) {
                        return [c, true];
                    }).object().value();
                }
                return c;
            }

            $scope.pairClass = function (data) {
                if (!data['class']) {
                    return $scope.model.pair['class'];
                }
                if (!$scope.model.pair['class']) {
                    return data['class'];
                }
                return $.extend(true, {}, transformClass(data['class']), transformClass($scope.model.pair['class']));
            };
        }
    };
});