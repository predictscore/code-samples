var m = angular.module('widgets');

m.directive('comboBox', function(widgetLogic) {
    return {
        restrict: 'A',
        replace: false,
        template: '<div selector="model.data" variants="model.variants"></div>',
        scope: {
            model: '=comboBox'
        },
        link: function($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);
        }
    };
});