var m = angular.module('widgets');

m.directive('actions', function(path, modals, widgetLogic, feature) {

    function fieldEqual(data, keys, value) {
        if (keys.length === 0) {
            return false;
        }
        if (keys.length === 1) {
            return data[keys[0]] === value;
        }
        if (_(data[keys[0]]).isUndefined()) {
            return false;
        }
        return fieldEqual(data[keys[0]], _(keys).rest(), value);
    }

    function valueByKeys(data, keys) {
        if (keys.length === 0) {
            return;
        }
        if (keys.length === 1) {
            return data[keys[0]];
        }
        if (_(data[keys[0]]).isUndefined()) {
            return;
        }
        return valueByKeys(data[keys[0]], _(keys).rest());
    }

    function fieldContains(data, key, value) {
        var keyData = valueByKeys(data, key);
        if (!_(keyData).isString() || !_(value).isString()) {
            return false;
        }
        return keyData.indexOf(value) !== -1;
    }

    function fieldMatches (data, key, compareWith) {
        var keys = key.split('.');
        if (compareWith && compareWith.src_key) {
            keys = compareWith.src_key.split('.');
        }
        var value = compareWith;
        if (compareWith && compareWith.operator) {
            if (compareWith.key) {
                value = valueByKeys(data, compareWith.key.split('.'));
            } else {
                value = compareWith.value;
            }
        }
        if (compareWith && compareWith.operator === 'not_equal') {
            return !fieldEqual(data, keys, value);
        }
        if (compareWith && compareWith.operator === 'equal') {
            return fieldEqual(data, keys, value);
        }
        if (compareWith && compareWith.operator === 'contains') {
            return fieldContains(data, keys, value);
        }
        if (compareWith && compareWith.operator === 'defined') {
            return !_(valueByKeys(data, keys)).isUndefined();
        }
        return fieldEqual(data, keys, compareWith);
    }

    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('widgets').directive('actions').template(),
        scope: {
            model: '=actions'
        },
        link: function($scope, element, attr) {
            widgetLogic($scope, $scope.model.logic);

            function isHidden(action) {
                if (!action.hiddenEntityAll && !action.hiddenEntityAny) {
                    return false;
                }
                if (!$scope.model.entityData) {
                    return true;
                }
                var hidden = false;
                if (action.hiddenEntityAny) {
                    hidden = !_(action.hiddenEntityAny).chain().find(function (value, key) {
                        return fieldMatches($scope.model.entityData, key, value);
                    }).isUndefined().value();
                }
                if (!hidden && action.hiddenEntityAll) {
                    hidden  = _(action.hiddenEntityAll).chain().find(function (value, key) {
                        return !fieldMatches($scope.model.entityData, key, value);
                    }).isUndefined().value();
                }
                return hidden;
            }

            $scope.isDisabled = function (action) {
                if (!$scope.model.entityData || !action.disabledEntityAll && !action.disabledEntityAny) {
                    return false;
                }
                var disabled = false;
                if (action.disabledEntityAny) {
                    disabled = !_(action.disabledEntityAny).chain().find(function (value, key) {
                        return fieldMatches($scope.model.entityData, key, value);
                    }).isUndefined().value();
                }
                if (!disabled && action.disabledEntityAll) {
                    disabled  = _(action.disabledEntityAll).chain().find(function (value, key) {
                        return !fieldMatches($scope.model.entityData, key, value);
                    }).isUndefined().value();
                }
                return disabled;
            };

            
            $scope.isActionVisible = function (action) {
                return (_(action.noFeature).isUndefined() || !feature[action.noFeature]) &&
                    (_(action.feature).isUndefined() || feature[action.feature]) &&
                    action.label && !isHidden(action);
            };
            $scope.processClick = function(action) {
                function process() {

                }

                if(!_(action.confirm).isUndefined()) {
                    modals.confirm(action.confirm.text).then(process);
                } else {
                    process();
                }
            };
        }
    };
});