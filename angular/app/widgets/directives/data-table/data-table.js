var m = angular.module('widgets');

m.directive('table', function(path, paginationHelper, columnsHelper, widgetLogic, $timeout) {
    var scrollbarWidth = (function getScrollbarWidth() {
        var outer = document.createElement("div");
        outer.style.visibility = "hidden";
        outer.style.width = "100px";
        outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps
    
        document.body.appendChild(outer);
    
        var widthNoScroll = outer.offsetWidth;
        // force scrollbars
        outer.style.overflow = "scroll";
    
        // add innerdiv
        var inner = document.createElement("div");
        inner.style.width = "100%";
        outer.appendChild(inner);        
    
        var widthWithScroll = inner.offsetWidth;
    
        // remove divs
        outer.parentNode.removeChild(outer);
    
        return widthNoScroll - widthWithScroll + 1;
    })();

    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('widgets').directive('data-table').template(),
        scope: {
            model: '=table',
            options: '=?'
        },
        link: function($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);

            $scope.options = $scope.options || {};
            $scope.options.pagination = $scope.options.pagination || {};
            $scope.options.orderDisplayMode = $scope.options.orderDisplayMode || 'header-sign';
            
            var oldTableWidth = -1;
            var oldHeaderCellsLength = -1;
            
            function updateHeadMargin() {
                $(element).find('thead').css({
                    'margin-left': -$(element).find('tbody').scrollLeft()
                });
            }
            
            var timeoutHandle = null;
            function recalculateColumnsAdv(force) {
                if(!$scope.options.bodyScroll) {
                    return;
                }
                var table = $(element).find('.table-wrapper > table');
                var header = table.find('> thead');
                var body = table.find('> tbody');
                var headerCells = header.find('tr:first').children();
                var firstRow = body.find('tr:first');
                var bodyCells = firstRow.children();
                
                var newTableWidth = $(element).width();
                if(!force && newTableWidth == oldTableWidth && headerCells.length == oldHeaderCellsLength) {
                    return;
                }
                oldHeaderCellsLength = headerCells.length;
                
                if(firstRow.length > 1 && (bodyCells.length === 0 || headerCells.length != bodyCells.length)) {
                    $timeout(recalculateColumns);
                    return;
                }
                oldTableWidth = newTableWidth;

                $(element).find('.table-wrapper').removeClass('show-scroll');
                
                var oldHeaderWidth = -1;
                function saveWidth() {
                    var headerCells = header.find('tr:first').children();
                    var firstRow = body.find('tr:first');
                    var bodyCells = firstRow.children();
                    
                    var headerWidth = headerCells.map(function() {
                        return $(this).outerWidth();
                    }).get();
                    var bodyWidth = bodyCells.map(function() {
                        return $(this).outerWidth();
                    }).get();
                    var colWidth = _(headerWidth).map(function(width, idx) {
                        return Math.max(width, bodyWidth[idx] || 0);
                    });
                    var widthSumm = _(colWidth).reduce(function(memo, width) {
                        return memo + width;
                    }, 0);
                    
                    if(widthSumm != header.width() && oldHeaderWidth != widthSumm) {
                        oldHeaderWidth = widthSumm;
                        timeoutHandle = $timeout(saveWidth);
                        return;
                    }
                    table.width('100%');
                    body.find('.width-helper').width(widthSumm);
                    body.find('tr').width(widthSumm);
                    
                    
                    headerCells.each(function(i, v) {
                        $(v).css({
                            'min-width': colWidth[i],
                            'max-width': colWidth[i]
                        });
                    });
                    body.find('tr').each(function() {
                        $(this).children().each(function(i, v) {
                            $(v).css({
                                'min-width': colWidth[i],
                                'max-width': colWidth[i]
                            });
                        });
                    });
                    $(element).find('.table-wrapper').addClass('show-scroll');
                    $scope.$emit($scope.options.bodyScrollAdjustEvent);
                    handleDropdownRightPosition();
                    updateHeadMargin();
                }

                table.width('calc(100% - ' + scrollbarWidth + 'px)');

                body.width('auto');
                body.find('.width-helper').width('auto');
                headerCells.each(function(i, v) {
                    if ($scope.options.bodyScrollAutoWidth) {
                        $(v).css({
                            'min-width': '0',
                            'max-width': 'none',
                            'width': 'auto'
                        });
                        return;
                    }
                    var minWidth = 100;
                    var maxWidth = 200;
                    if($(v).hasClass('table-checkbox')) {
                        minWidth = 0;
                    }
                    if($(v).hasClass('wide-column')) {
                        minWidth += 100;
                        maxWidth += 100;
                    }
                    $(v).css({
                        'min-width': minWidth + 'px',
                        'max-width': maxWidth + 'px'
                    });
                });
                body.find('tr').each(function() {
                    $(this).children().each(function(i, v) {
                        if ($scope.options.bodyScrollAutoWidth) {
                            $(v).css({
                                'min-width': '0',
                                'max-width': 'none',
                                'width': 'auto'
                            });
                            return;
                        }
                        var minWidth = 100;
                        var maxWidth = 200;
                        if($(v).hasClass('table-checkbox')) {
                            minWidth = 0;
                        }
                        if($(v).hasClass('wide-column')) {
                            minWidth += 100;
                            maxWidth += 100;
                        }
                        $(v).css({
                            'min-width': minWidth + 'px',
                            'max-width': maxWidth + 'px'
                        });
                    });
                });
                
                if(timeoutHandle) {
                    $timeout.cancel(timeoutHandle);
                }
                timeoutHandle = $timeout(saveWidth);
            }
            
            function recalculateColumns() {
                return recalculateColumnsAdv(false);
            }
            
            function handleDropdownRightPosition() {
                $(element).find('.dropdown-right, .dropdown-left').each(function() {
                    var dropdownWidth = $(this).width();
                    var parentWidth = $(this).closest('th').width();
                    $(this).css({
                        'margin-left': (-dropdownWidth + parentWidth + 5) + 'px'
                    });
                });
                $(element).find('.dropdown-right').off('resize', handleDropdownRightPosition);
                $(element).find('.dropdown-right').on('resize', handleDropdownRightPosition);
            }
            
            if($scope.options.bodyScroll) {
                $(element).on('resize', recalculateColumns);
                
                $(element).find('tbody').scroll(updateHeadMargin);
                
                $scope.$watch(function() {
                    if(!$scope.options.bodyScroll) {
                        return;
                    }
                    var refresh = false;
                    $(element).find('tbody > tr > td').each(function() {
                        var element = $(this)[0];
                        if (!$(this).find('.dropdown-menu').length && (element.offsetHeight < element.scrollHeight)) {
                            refresh = true;
                        }
                    });
                    if(refresh) {
                        recalculateColumnsAdv(true);
                    }
                }, _.noop);
            }
            
            var rows = [];
            $scope.getRows = function() {
                rows.length = 0;
                _($scope.model.data).each(function(row) {
                    rows.push(row);
                    _(row.subrows).each(function(subrow) {
                        rows.push(subrow);
                    });
                });
                return rows;
            };

            $scope.model.autoOrder = [];
            
            function rebuildIdToColumn() {
                $scope.model.idToColumn = {};
                var columnsOrder = [];
                _($scope.model.columns).each(function(column, idx) {
                    column.idx = idx;
                    $scope.model.idToColumn[column.id] = column;
                    column.cellDataClass = {};
                    if(_(column.dataClass).isObject()) {
                        $.extend(column.cellDataClass, column.dataClass);
                    } else if(_(column.dataClass).isString()) {
                        column.cellDataClass[column.dataClass] = true;
                    }
                    column.cellDataClass['id-' + column.id] = true;
                    if (columnsHelper.isColumnVisible(column, $scope.options, false, true)) {
                        columnsOrder.push(column.id);
                    }
                });
                $scope.model.autoOrder = _(columnsOrder).uniq();
                
            }

            $scope.$watch('model.columns', rebuildIdToColumn);
            $scope.$watch('model.columnsOrder', recalculateColumns);

            $scope.$watch('model', function() {
                rebuildIdToColumn();
                $scope.model.refreshGui = function() {
                    recalculateColumnsAdv(true);
                };
                $scope.model.data = $scope.model.data || [];
                $scope.model.globalSelection = true;
                _($scope.model.data).each(function(line) {
                    if(!$scope.lineSelected(line)()) {
                        $scope.model.globalSelection = false;
                    }
                });
                if(!$scope.model.data.length) {
                    $scope.model.globalSelection = false;
                }
                $scope.model.updateData = updateData;
                createOptionalFilter($scope.model.columns);
                oldTableWidth = -1;
                if($scope.options.bodyScroll) {
                    recalculateColumns();
                }
            });

            $scope.updateSelection = function() {
                _($scope.model.data).each(function(line) {
                    $scope.lineSelected(line)($scope.model.globalSelection);
                });
            };

            function updateData() {
                var self = this;
                if(_(self.dataUpdate).isArray()) {
                    _(self.dataUpdate).each(function(e) {
                        self.data.splice(0, 0, e);
                    });
                    this.options = this.options || {};
                    var maxData = this.options.maxData;
                    if(!_(maxData).isUndefined() && maxData < self.data.length) {
                        self.data.splice(maxData, self.data.length - maxData);
                    }
                }
                delete self.dataUpdate;
            }

            function createOptionalFilter (columns) {
                if (!_($scope.options.optionalFilter).isUndefined()) {
                    return;
                }
                var filter = _($scope.model.columns).chain().filter(function (column) {
                    return column.optional;
                }).map(function (column) {
                    return {
                        id: column.id,
                        text: column.text || column.id,
                        type: 'flag',
                        state: _(column.defaultOptional).isUndefined() || column.defaultOptional
                    };
                }).value();
                if (filter.length > 0) {
                    $scope.options.optionalFilter = filter;
                } else {
                    $scope.options.optionalFilter = false;
                }
            }

            $scope.changeColumnsFilters = function (filter, value) {
                if (_($scope.options.pagination).isUndefined()) {
                    return false;
                }
                $scope.options.pagination.columnsFilters = $scope.options.pagination.columnsFilters || {};
                var columnsFilters = $scope.options.pagination.columnsFilters;
                if (!value) {
                    if (filter in columnsFilters) {
                        delete columnsFilters[filter];
                        return true;
                    }
                } else {
                    if(!_(columnsFilters[filter]).isEqual(value)) {
                        columnsFilters[filter] = value;
                        return true;
                    }
                }
                return false;
            };


            $scope.$watch('model.pagination', function() {
                if(_($scope.model.pagination).isUndefined()) {
                    $scope.pages = null;
                } else {
                    $scope.pages = paginationHelper.generatePages({
                        total: $scope.model.pagination.total,
                        current: $scope.model.pagination.current,
                        haveNext: $scope.model.pagination.haveNext,
                        pagesAround: $scope.options.pagesAround
                    });
                    $scope.customPage = {value: $scope.model.pagination.current};
                }
            });

            $scope.paginationClicked = function(event, page) {
                if(page.enabled) {
                    $scope.options.pagination.page = page.value;
                }
                event.preventDefault();
            };

            $scope.paginationJump = function (event) {
                var pageNumber = parseInt($scope.customPage.value, 10);
                if (isFinite(pageNumber) && pageNumber > 0) {
                    if (_($scope.model.pagination.total).isNumber() && pageNumber > $scope.model.pagination.total) {
                        pageNumber = $scope.model.pagination.total;
                        $scope.customPage.value = pageNumber;
                    }
                    $scope.options.pagination.page = pageNumber;
                } else {
                    $scope.customPage.value = $scope.model.pagination.current;
                }
                event.preventDefault();
            };

            $scope.paginationSetPageSize = function (event, pageSize) {
                $scope.options.pageSize = pageSize;
                event.preventDefault();
            };

            var actionsCache = {};
            $scope.$watch('options.actions', function() {
                actionsCache = {};
            }, true);
            $scope.getActions = function(display) {
                if(!_(actionsCache[display]).isUndefined()) {
                    return actionsCache[display];
                }
                if(_($scope.options.actions).isUndefined()) {
                    return undefined;
                }
                var actions = _($scope.options.actions).filter(function(action) {
                    var actionDisplay = action.display || 'dropdown-button';
                    return actionDisplay == display;
                });
                if(!actions.length) {
                    return undefined;
                }
                actionsCache[display] = actions;
                return actions;
            };

            $scope.executeAction = function(event, action) {
                event.preventDefault();
                if(!$scope.actionActive(action)) {
                    return;
                }
                action.execute();
            };

            $scope.actionActive = function(action) {
                return !(_(action.active).isFunction() && !action.active());
            };

            $scope.getText = function(line, index) {
                var href = $scope.model.columns[index].href;
                if(_(href).isUndefined()) {
                    return line[index].text;
                }

                var result = {
                    text: line[index].text,
                    type: href.type,
                    qs: {}
                };

                var nullFound = false;
                _(href.params).each(function (param, key) {
                    var value = param;
                    _($scope.model.columns).each(function (column, idx) {
                        if (column.id === param) {
                            value = href.useOriginal ? line[idx].originalText : line[idx].text;
                        }
                    });
                    result[key] = value;
                    if(_(value).isNull()) {
                        nullFound = true;
                    }
                });
                _(href.qs).each(function(param, key) {
                    var value = param;
                    _($scope.model.columns).each(function (column, idx) {
                        if (column.id === param) {
                            value = line[idx].text;
                        }
                    });
                    result.qs[key] = value;
                    if(_(value).isNull()) {
                        nullFound = true;
                    }
                });
                if(nullFound) {
                    return result.text;
                }
                return '#' + JSON.stringify(result);
            };

            $scope.getLineSelection = function(line) {
                var primaryIdx = columnsHelper.getPrimaryIdx($scope.model.columns);
                var selectionIdx;
                _($scope.model.selected).each(function(s, idx) {
                    if(s[primaryIdx].text == line[primaryIdx].text) {
                        selectionIdx = idx;
                    }
                });
                return selectionIdx;
            };

            $scope.lineSelected = function(line) {
                return function(val) {
                    if(_(val).isUndefined()) {
                        return $scope.isSelected(line);
                    } else {
                        var selectIdx = $scope.getLineSelection(line);
                        if(_(selectIdx).isUndefined() && val) {
                            if($scope.model.options.singleSelection) {
                                $scope.model.selected.length = 0;
                            }
                            $scope.model.selected.push(line);
                        } else if(!_(selectIdx).isUndefined() && !val){
                            $scope.model.selected.splice(selectIdx, 1);
                        }
                    }
                };
            };

            $scope.lineClick = function(line) {
                if($scope.model.options.lineSelection) {
                    $scope.lineSelected(line)(!$scope.isSelected(line));
                }
            };

            $scope.getLineClass = function(row, line) {
                var rowClasses = $scope.model.rowClasses || {};
                var result = rowClasses[row] || {};
                result['selected-line'] = $scope.isSelected(line);
                return result;
            };

            $scope.isSelected = function(line) {
                return !_($scope.getLineSelection(line)).isUndefined();
            };

            $scope.isColumnVisible = function(column) {
                return columnsHelper.isColumnVisible(column, $scope.options);
            };

            $scope.visibleOrder = function () {
                return $scope.model.columnsOrder || $scope.model.options.columnsOrder || $scope.model.autoOrder;
                // return _(columns).filter(function (column) {
                //     return $scope.isColumnVisible(column);
                // });
            };

            $scope.columnDropdownClass = function (columns, index, columnId) {
                var visible = $scope.visibleOrder();
                var columnIndex = visible.indexOf(columnId);
                // var index = _(visible).indexOf(column);
                if (columnIndex > visible.length / 2 && columns[columnsHelper.getIdxById(columns, columnId)].dropdownPosition !== 'left') {
                    return "dropdown-right";
                } else {
                    return "dropdown-left";
                }
            };
            
            var buttonsCache = {};
            $scope.$watch('options.buttons', function() {
                buttonsCache = {};
            });
            
            $scope.buttons = function (position) {
                if (!buttonsCache[position]) {
                    buttonsCache[position] = _($scope.options.buttons).filter(function (button) {
                        return (button.position === position || position === 'main' && !button.position) && !button.hidden && (!$scope.minimized || button.showWhenMinimized);
                    });
                }
                return buttonsCache[position];
            };

            $scope.pageSizes = $scope.options.allowedPageSizes || [20, 50];

            $scope.showBottomPerPageSelector = function () {
                return !$scope.options.disablePerPageSelector &&
                    !$scope.options.disableBottom &&
                    !_($scope.options.pagination).isUndefined() &&
                    $scope.model.pagination &&
                    $scope.model.pagination.total > 0 &&
                    ($scope.model.pagination.total > 1 ||
                        $scope.model.pagination.haveNext ||
                        _($scope.pageSizes).indexOf($scope.options.pagination.pageSize) > 0
                    );
            };
            
            $scope.$on('$destroy', function() {
                if($scope.options.bodyScroll) {
                    $(element).off('resize', recalculateColumns);
                    $(element).find('.dropdown-right').off('resize', handleDropdownRightPosition);
                }
            });
        }
    };
});
