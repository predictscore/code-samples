var m = angular.module('widgets');

m.directive('multiWidget', function(path, sizeClasses, widgetLogic) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('widgets').directive('multi-widget').template(),
        scope: {
            model: '=multiWidget'
        },
        link: function ($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);
            $scope.getSizeClasses = sizeClasses;
        }
    };
});