var m = angular.module('widgets');

m.directive('fileSystemTree', function(path) {
    return {
        restriction: 'A',
        replace: true,
        templateUrl: path('widgets').directive('file-system-tree').template(),
        scope: {
            model: '=fileSystemTree',
            options: '='
        },
        link: function($scope, element, attr) {
            $scope.nodes = [];
            $scope.options.retrieveContent().then(function(response) {
                $scope.nodes = response.data.rows;
            });
        }
    };
});