var m = angular.module('widgets');

m.directive('widgetPanelButtons', function(path) {
    return {
        restrict: 'A',
        templateUrl: path('widgets').directive('widget-panel-buttons').template(),
        scope: {
            buttons: '=widgetPanelButtons'
        },
        link: function($scope, element, attrs) {
            $scope.getOptionClass = function(option) {
                option.compiledClass = {};
                if(_(option.class).isObject()) {
                 $.extend(option.compiledClass, option.class);
                } else if(_(option.class).isString()) {
                 option.compiledClass[option.class] = true;
                }
                option.compiledClass['dropdown-submenu'] = !!option.subMenus;
                return option.compiledClass;
            };
            
            $scope.execute = function(option, event, button) {
                if(option.noEvents) {
                    return;
                }
                if(!$scope.actionActive(option)) {
                    event.stopPropagation();
                    return;
                }
                if(option.isOpen) {
                    option.isOpen = false;
                }
                if (option.forceClose && button) {
                    button.isOpen = false;
                }
                (option.onclick || option.execute || function(event) {
                    event.stopPropagation();
                })(event);
            };
            
            $scope.actionActive = function(option) {
                return !option.active || option.active();
            };
        }
    };
});