var m = angular.module('widgets');

m.directive('entityDebug', function(path, widgetLogic) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('widgets').directive('entity-debug').template(),
        scope: {
            model: '=entityDebug'
        },
        link: function($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);

            $scope.entityDebug = {
                type: $scope.model.entityType,
                id: $scope.model.entityId
            };
        }
    };
});