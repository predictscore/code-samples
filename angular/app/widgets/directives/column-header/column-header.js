var m = angular.module('widgets');

m.directive('columnHeader', function(path, $timeout, columnsHelper, headerOptionsBuilder, $q) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('widgets').directive('column-header').template(),
        scope: {
            column: '=columnHeader',
            tableOptions: '=',
            tableModel: '=',
            changeColumnsFilters: '&',
            dropdownClass: '&'
        },
        link: function($scope, element, attrs) {
            $scope.haveDropdown = function() {
                if(!$scope.tableOptions.dropdownHeader) {
                    return false;
                }
                return $scope.column.sortable || $scope.column.optional || $scope.column.removable || $scope.column.filter || $scope.column.facets;
            };

            function fixColumnFilter () {
                if ($scope.column.filter && !_($scope.column.filter).isObject()) {
                    $scope.column.filter = {
                        name: $scope.column.filter,
                        type: 'text'
                    };
                }
            }
            fixColumnFilter();

            if ($scope.tableOptions.activeDropdown && $scope.tableOptions.activeDropdown.id === $scope.column.id) {
                $scope.isDropdownOpen = true;
                $scope.state = $scope.tableOptions.activeDropdown;
            } else {
                $scope.isDropdownOpen = false;

                $scope.state = {
                    id: $scope.column.id,
                    filter: undefined,
                    facets: {},
                    facetsFilter: '',
                    facetsUsed: {}
                };

                if ($scope.column.filter) {
                    setFilter();
                }
            }

            function setFilter() {
                if (!$scope.column.filter) {
                    return;
                }
                $scope.state.filter = $scope.tableOptions.pagination && $scope.tableOptions.pagination.columnsFilters && $scope.tableOptions.pagination.columnsFilters[$scope.column.filter.name];
            }

            function resetFacets() {
                $scope.state.facets = {};
                setFacets();
            }

            function setFacets() {
                if (!$scope.column.facets) {
                    return;
                }
                if ($scope.tableOptions.pagination && $scope.tableOptions.pagination.facets && $scope.tableOptions.pagination.facets[$scope.column.facets.include]) {
                    _($scope.tableOptions.pagination.facets[$scope.column.facets.include]).each(function (optionId) {
                        $scope.state.facets[optionId] = true;
                    });
                }
                if ($scope.tableOptions.pagination && $scope.tableOptions.pagination.facets && $scope.tableOptions.pagination.facets[$scope.column.facets.exclude]) {
                    _($scope.tableOptions.pagination.facets[$scope.column.facets.exclude]).each(function (optionId) {
                        $scope.state.facets[optionId] = false;
                    });
                }
                $scope.state.facetsUsed = $.extend({},$scope.state.facets);
                updateDynamicStyles();
            }

            $scope.$watch('column.facets', function (newValue, oldValue) {
                if (newValue) {
                    setFacets();
                }
            }, true);

            $scope.haveFilters = function () {
                if ($scope.state.filter) {
                    return true;
                }
                var haveFacets = _($scope.state.facets).chain().keys().find(function (id) {
                    return $scope.state.facets[id] === true || $scope.state.facets[id] === false;
                }).value();
                if (haveFacets) {
                    return true;
                }
                return false;
            };


            function updateDynamicStyles() {
                if ($scope.haveDropdown()) {
                    if ($scope.isDropdownOpen) {
                        columnClassAdd('column-expanded');
                    } else {
                        columnClassRemove('column-expanded');
                    }
                }
                var optionalColumn = _($scope.tableOptions.optionalFilter).find(function (filter) {
                    return filter.id === $scope.column.id;
                });

                var ordering = $scope.tableOptions.pagination.ordering;
                if ($scope.haveFilters() || $scope.tableOptions.orderDisplayMode == 'highlight-column' && ordering.column == $scope.columnId) {
                    columnClassAdd('column-have-filters');
                    if (optionalColumn) {
                        optionalColumn.class = "column-with-filter";
                    }
                } else {
                    columnClassRemove('column-have-filters');
                    if (optionalColumn) {
                        optionalColumn.class = "";
                    }
                }
                if ($scope.haveDropdown() || $scope.column.sortable) {
                    columnClassAdd('column-clickable');
                }
            }

            updateDynamicStyles();

            function columnClassAdd(className) {
                var classes = _($scope.column.class && $scope.column.class.split(/\s+/g) || []).chain()
                    .map(function(classElement) {
                        return [classElement, true];
                    }).object().value();
                if (classes[className]) {
                    return;
                }
                classes[className] = true;
                $scope.column.class = _(classes).keys().join(' ');
            }

            function columnClassRemove(className ) {
                var classes = _($scope.column.class && $scope.column.class.split(/\s+/g) || []).chain()
                    .map(function(classElement) {
                        return [classElement, true];
                    }).object().value();
                if (classes[className]) {
                    delete classes[className];
                }
                $scope.column.class = _(classes).keys().join(' ');
            }


            $scope.getColumnLabel = function() {
                if($scope.column.text === false) {
                    return '';
                }
                var overrided = null;
                if(_($scope.tableOptions.columnNamesOverride).isFunction()) {
                    overrided = $scope.tableOptions.columnNamesOverride($scope.column);
                }
                return overrided || $scope.column.text || $scope.column.id;
            };

            function changeOrdering (order) {
                var sortable = $scope.tableModel.columns[$scope.columnId].sortable;
                if(!sortable) {
                    return;
                }
                var ordering = $scope.tableOptions.pagination.ordering;
                
                if(ordering.column == $scope.columnId && ordering.order == order) {
                    order = null;
                }
                
                if(_(order).isNull()) {
                    delete ordering.column;
                    delete ordering.columnName;
                    delete ordering.order;
                } else if(ordering.column != $scope.columnId || order) {
                    ordering.column = $scope.columnId;
                    ordering.columnName = sortable.sortId || $scope.tableModel.columns[$scope.columnId].id;
                    ordering.order = order || 'desc';
                } else {
                    if(ordering.order == 'desc') {
                        ordering.order = 'asc';
                    } else {
                        delete ordering.column;
                        delete ordering.columnName;
                        delete ordering.order;
                    }
                }
            }

            $scope.columnClick = function () {
                if (!$scope.haveDropdown()) {
                    changeOrdering();
                }
            };

            $scope.order = function (order) {
                $timeout(function () {
                    changeOrdering(order);
                });
            };
            
            $scope.getOrder = function() {
                var ordering = $scope.tableOptions.pagination.ordering;
                if(ordering.column != $scope.columnId) {
                    return null;
                }
                return ordering.order;
            };

            $scope.orderingClass = function() {
                var ordering = $scope.tableOptions.pagination.ordering || {};
                var columnSorted = ordering.column == $scope.columnId;
                return {
                    'fa-sort-down': columnSorted && ordering.order == 'desc',
                    'fa-sort-up': columnSorted && ordering.order == 'asc',
                    'fa-sort': !columnSorted
                };
            };

            function updateParentFilters() {
                var filterUpdated = false;
                if ($scope.column.filter) {
                    filterUpdated = $scope.changeColumnsFilters({
                        filter: $scope.column.filter.name,
                        value: angular.copy($scope.state.filter)
                    });
                } else if($scope.column.filterBackup) {
                    filterUpdated = $scope.changeColumnsFilters({
                        filter: $scope.column.filterBackup.name,
                        value: undefined
                    });
                }
                if ($scope.column.facets) {
                    if (!_($scope.tableOptions.pagination).isUndefined()) {
                        $scope.tableOptions.pagination.facets = $scope.tableOptions.pagination.facets || {};

                        var includeName = $scope.column.facets.include;
                        var excludeName = $scope.column.facets.exclude;
                        var facets = $scope.tableOptions.pagination.facets;

                        var columnFacets = _($scope.column.facets.variants).chain().map(function (option) {
                          return [option.id, true];
                        }).object().value();

                        var newInclude = _(facets[includeName]).filter(function (optionId) {
                            return !columnFacets[optionId];
                        }).concat(_($scope.column.facets.variants).chain().filter(function (option) {
                            return $scope.state.facets[option.id] === true;
                        }).map(function (option) {
                            return option.id;
                        }).value());


                        if (newInclude.length > 0) {
                            facets[includeName] = newInclude;
                        } else {
                            if (includeName in facets) {
                                delete facets[includeName];
                            }
                        }
                        var newExclude = _(facets[excludeName]).filter(function (optionId) {
                            return !columnFacets[optionId];
                        }).concat(_($scope.column.facets.variants).chain().filter(function (option) {
                            return $scope.state.facets[option.id] === false;
                        }).map(function (option) {
                            return option.id;
                        }).value());

                        if (newExclude.length > 0) {
                            facets[excludeName] = newExclude;
                        } else {
                            if (excludeName in facets) {
                                delete facets[excludeName];
                            }
                        }

                    }
                }
                return filterUpdated;
            }

            $scope.hide = function () {
                if($scope.column.removable) {
                    if(_($scope.tableOptions.onColumnRemove).isFunction()) {
                        $scope.tableOptions.onColumnRemove($scope.columnId, $scope.column);
                    }
                } else {
                    var optionalColumn = _($scope.tableOptions.optionalFilter).find(function (filter) {
                        return filter.id === $scope.column.id;
                    });
                    if (optionalColumn) {
                        optionalColumn.state = false;
                    }
                }
            };

            $scope.applyFilter = function () {
                $q.all(_($scope.column.buttons).map(function(button) {
                    return $q.when((button.save || _.noop)($scope.tempContext)); 
                })).then(function(results) {
                    var found = _(results).find(function(r) {
                        return r;
                    });
                    var filterUpdated = updateParentFilters();
                    if($scope.tableOptions.eventOnFilterChange) {
                        if(found && !filterUpdated) {
                            $scope.$emit($scope.tableOptions.eventOnFilterChange, {
                                force: true,
                                columnsFilters: $scope.tableOptions.pagination.columnsFilters
                            });
                        }
                    } else {
                        $scope.tableOptions.reloadTable++;
                    }
                });
                
                
            };

            $scope.resetFilter = function () {
                resetFacets();
                setFilter();
                $scope.tempContext = {};
                _($scope.column.buttons).each(function(button) {
                    (button.init || _.noop)($scope.tempContext); 
                });
            };


            $scope.dropdownToggled = function (opened) {
                if ($scope.haveDropdown()) {
                    if (!opened) {
                        if ($scope.tableOptions.activeDropdown.id === $scope.column.id) {
                            $scope.tableOptions.activeDropdown = false;
                        }
                    } else {
                        $scope.resetFilter();
                        $scope.tableOptions.activeDropdown = $scope.state;
                    }
                }
                updateDynamicStyles();
            };

            $scope.clearAll = function (e) {
                e.stopPropagation();
                $scope.state.filter = '';
                $scope.state.facets = {};
            };
            
            $scope.execute = function(event, button) {
                button.execute(event, $scope.tempContext);
            };
            
            $scope.$watch('column', function() {
                $scope.columnId = columnsHelper.getIdxById($scope.tableModel.columns, $scope.column.id);
                fixColumnFilter();
                $scope.resetFilter();
                updateDynamicStyles();
            });
            
            $scope.$watch('tableOptions.headerOptions', function() {
                $scope.headerOptions = headerOptionsBuilder($scope.tableOptions.headerOptions, $scope.column.property);
            });
        }
    };
});