var m = angular.module('widgets');

m.directive('tableCellHover', function($parse, columnsHelper, $compile, $timeout, $q) {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            var hideDelay = 300;
            var tableModel = $parse(attrs.table)($scope);
            var hoverOptions = $parse(attrs.tableCellHover)($scope);
            var tableOptions = $parse(attrs.options)($scope);

            var hover = {
                root: $('#ng-view > div').first(),
                shown: false,
                element: $compile('<div class="table-cell-hover" style="display: none;"><div ng-if="tableCellHoverData" linked-text="tableCellHoverData" ng-class="tableCellHoverClass"></div>')($scope),
                hideTimer: null
            };
            hover.root.append(hover.element);

            var overMap = {};

            function getCellIndexes(obj) {
                var cell = $(obj).closest('td');
                if (cell.length !== 1 || !$.contains(element[0], cell[0])) {
                    return false;
                }
                var row = cell.parent();
                return {
                    cellIdx: row.children('td').index(cell),
                    rowIdx: row.parent().children('tr').index(row),
                    cell: cell
                };
            }

            element.on('mouseover.tablecellhover', function (e) {
                var indexes = getCellIndexes(e.target);
                if (!indexes) {
                    return;
                }
                cellOver(indexes);
            });

            element.on('mouseout.tablecellhover', function (e) {
                var indexes = getCellIndexes(e.target);
                if (!indexes) {
                    return;
                }
                cellOut(indexes);
            });
            hover.element.on('mouseover.tablecellhover', function () {
                hideCancel();
            });
            hover.element.on('mouseout.tablecellhover', function () {
                hideStart();
            });

            function buildOverMap () {
                var shift = (tableModel && tableModel.options && tableModel.options.checkboxes && 1 || 0);
                var columnsOrder = _(tableModel.columnsOrder || tableModel.options.columnsOrder || tableModel.autoOrder)
                    .chain()
                    .filter(function (column) {
                        return columnsHelper.isColumnVisible(column, tableOptions);
                    }).map(function (id, idx) {
                        return [id, idx + shift];
                    }).object().value();
                overMap = {};
                _(hoverOptions).each(function (el) {
                    overMap[columnsOrder[el.column]] = {
                        column: el.column,
                        dataColumn: el.dataColumn,
                        dataIdx: columnsHelper.getIdxById(tableModel.columns, el.dataColumn),
                        'class': el.class
                    };
                });
            }

            function cellOver(indexes) {
                if (!overMap[indexes.cellIdx]) {
                    return;
                }
                var cellData = tableModel.data[indexes.rowIdx][overMap[indexes.cellIdx].dataIdx];
                hideCancel();
                if (hover.shown && indexes.cellIdx === hover.indexes.cellIdx && indexes.rowIdx === hover.indexes.rowIdx) {
                    return;
                }
                if (hover.shown) {
                    hover.element.hide();
                    hover.shown = false;
                }
                $scope.tableCellHoverData = null;
                $timeout(function () {
                    $scope.tableCellHoverData = null;
                    $q.when(cellData.originalText).then(function (result) {
                        $timeout(function () {
                            $scope.tableCellHoverData = result;
                        }, 10);
                    });
                    $scope.tableCellHoverClass = overMap[indexes.cellIdx]['class'];
                    showHover(indexes);
                });
            }

            function showHover(indexes) {
                var rootOffset = hover.root.offset();
                var cellOffset = indexes.cell.offset();
                hover.element.css({
                    top: 'auto',
                    left: (cellOffset.left - rootOffset.left - 3) + 'px',
                    bottom: (hover.root.height() - cellOffset.top + rootOffset.top - 5) + 'px'
                });
                hover.element.fadeIn();
                hover.indexes = {
                    cellIdx: indexes.cellIdx,
                    rowIdx: indexes.rowIdx,
                    cell: indexes.cell
                };
                hover.shown = true;
            }

            function cellOut(indexes) {
                if (!overMap[indexes.cellIdx]) {
                    return;
                }
                hideStart();
            }

            function hideStart() {
                hideCancel();
                hover.hideTimer = $timeout(hideEnd, hideDelay);
            }

            function hideCancel() {
                if (hover.hideTimer) {
                    $timeout.cancel(hover.hideTimer);
                    hover.hideTimer = null;
                }
            }
            function hideEnd() {
                hideCancel();
                hover.element.fadeOut();
                hover.shown = false;
            }

            $scope.$watch(function () {
                return $parse(attrs.table)($scope);
            }, function (newValue, oldValue) {
                tableModel = $parse(attrs.table)($scope);
                buildOverMap();
            });



            $scope.$on('$destroy', function () {
                hover.element.remove();
                element.off('.tablecellhover');
            });
        }
    };
});