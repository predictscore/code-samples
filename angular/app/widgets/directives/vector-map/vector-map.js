var m = angular.module('widgets');

m.directive('vectorMap', function($timeout, widgetLogic) {
    L.Icon.Default.imagePath = '/assets/css/images/';
    return {
        restrict: 'A',
        scope: {
            model: '=vectorMap'
        },
        link: function ($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);
            var mapSingleton = null;
            var lastMarkerId = 0;

            function getMap() {
                if(_(mapSingleton).isNull()) {
                    mapSingleton = L.map(element[0]);//.fitWorld().zoomIn();
                    // L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(mapSingleton);
                    L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
                        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
                        subdomains: 'abcd',
                        maxZoom: 19,
                        minZoom: 1
                    }).addTo(mapSingleton);
                }
                return mapSingleton;
             }

            $scope.model.data = $scope.model.data || [];

            $scope.$watch('model', function() {
                $scope.model.data = $scope.model.data || [];
                $scope.model.updateData = updateData;
            });

            var markers = [];
            var zoomed = false;
            var zoomedEmpty = false;
            function updateData() {
                var self = this;
                var map = getMap();

                if(_(self.dataUpdate).isArray()) {
                    this.options = this.options || {};
                    var maxData = this.options.maxData;

                    _(self.dataUpdate).each(function(e) {
                        lastMarkerId++;
                        if(!_(maxData).isUndefined() && lastMarkerId >= maxData) {
                            lastMarkerId = 0;
                        }

                    });
                } else if(_(self.data).isArray()) {
                    _(markers).each(function(marker) {
                        map.removeLayer(marker);
                    });

                    _(self.data).each(function(m) {
                        m.latLng[0] = m.latLng[0] || '0';
                        m.latLng[1] = m.latLng[1] || '0';
                        var metadata = m.metadata;
                        var name = m.name;
                        if(_(metadata).isObject()) {
                            var html = '<div class="map-label">';
                            if (_(name).isString()) {
                                html += '<span>' + name + '</span>';
                            }
                            html += '<table><tbody>';
                            _(metadata).each(function (data) {
                                html += '<tr>';
                                html += '<td>' + data.label + ':</td>';
                                html += '<td>' + data.value + '</td>';
                                html += '</tr>';
                            });
                            html += '</tbody></table></div>';
                            metadata = html;
                        } else {
                            metadata = metadata || '';
                        }
                        
                        var marker = L.marker(m.latLng).bindPopup(metadata);
                        markers.push(marker);
                        marker.addTo(map);
                    });

                    var bounds = L.latLngBounds(_(self.data).map(function(m) {
                        return m.latLng;
                    }));
                    if(!zoomed && bounds.isValid()) {
                        zoomed = true;
                        map.fitBounds(bounds, {
                            maxZoom: 4
                        });
                    }

                    lastMarkerId = 0;
                }
                delete self.dataUpdate;
                if (!zoomed && !zoomedEmpty) {
                    zoomedEmpty = true;
                    map.fitWorld().zoomIn();
                }
            }

            $scope.$on('$destroy', function() {
                if(!_(mapSingleton).isNull()) {
                    mapSingleton.remove();
                }
            });
        }
    };
});