var m = angular.module('widgets');

m.directive('widgetPanel', function(path, widgetSettings) {
    return {
        restrict: 'A',
        replace: false,
        transclude: {
            'widgetHeader': '?widgetHeader',
            'widgetBody': 'widgetBody'
        },
        templateUrl: path('widgets').directive('widget-panel').template(),
        scope: {
            model: '=widgetPanel'
        },
        link: function($scope, element, attrs) {
            var unWatchModel = $scope.$watch('model', function(model) {
                if(!_(model).isUndefined()) {
                    init();
                    unWatchModel();
                }
            });
            
            function syncCookies(propertyName, propertyState, updateCallback) {
                if (!propertyName) {
                    return;
                }
                var savedState = widgetSettings.param($scope.model.uuid, propertyName);
                if (!_(savedState).isUndefined()) {
                    updateCallback(savedState);
                } else {
                    widgetSettings.param($scope.model.uuid, propertyName, propertyState);
                }
            }

            $scope.$watch('model.wrapper.filters', function() {
                _($scope.model.wrapper.filters).each(function (filter) {
                    syncCookies(filter.value, filter.state, function(fromCookies) {
                        filter.state = fromCookies;
                    });
                });
            });
            
            if($scope.model.wrapper && $scope.model.wrapper.resolution) {
                $scope.$watch('model.wrapper.resolution', function(resolution) {
                    _(['value', 'count', 'interval']).each(function(property) {
                        var name = 'resolution.' + property;
                        syncCookies(name, resolution[property], function(fromCookies) {
                            resolution[property] = fromCookies;
                        });
                    });
                });
                
                $scope.$watch('model.wrapper.resolution.count', function(count) {
                    widgetSettings.param($scope.model.uuid, 'resolution.count', $scope.model.wrapper.resolution.count);
                });
                
                $scope.$watch('model.wrapper.resolution.interval', function() {
                    widgetSettings.param($scope.model.uuid, 'resolution.interval', $scope.model.wrapper.resolution.interval);
                });
            }

            $scope.$watch('model.wrapper.buttons', function () {
                buttonsCache = {};
            }, true);
            
            $scope.$watch('minimized', function () {
                buttonsCache = {};
            });

            var buttonsCache = {};

            $scope.buttons = function (position) {
                if (!buttonsCache[position]) {
                    buttonsCache[position] = _($scope.model.wrapper.buttons).filter(function (button) {
                        return (button.position === position || position === 'main' && !button.position) && !button.hidden && (!$scope.minimized || button.showWhenMinimized);
                    });
                }
                return buttonsCache[position];
            };


            function init() {
                if (_($scope.model.wrapper).isUndefined()) {
                    $scope.model.wrapper = $scope.model;
                }
                $scope.model.wrapper.class = $scope.model.wrapper.class || 'panel-default';

                $scope.minimized = false;
                $scope.showSettings = function () {
                    $($scope.model.settings).modal("show");
                };

                $scope.headerClick = function (time) {
                    if (_($scope.model.wrapper.headerOnClick).isFunction()) {
                        $scope.model.wrapper.headerOnClick();
                    }
                    $scope.minimizeToggle(time, true);
                };

                $scope.minimizeToggle = function (time, header) {
                    if (!$scope.model.wrapper.minimize) {
                        return;
                    }
                    if (_(time).isUndefined()) {
                        time = "slow";
                    }
                    $scope.minimized = !$scope.minimized;
                    $scope.model.wrapper.minimized = $scope.minimized;
                    var body = element.find(".panel-body");
                    body.addClass('minimizing');
                    body.slideToggle(time, function() {
                        body.removeClass('minimizing');
                    });
                    $scope.model.wrapper.minimized = $scope.minimized;
                };
                if ($scope.model.wrapper.minimized) {
                    $scope.minimizeToggle(0);
                }

                $scope.close = function () {
                    element.fadeOut();
                };

                $scope.filterChanged = function (filter) {
                    if(filter) {
                        if (filter.value) {
                            widgetSettings.param($scope.model.uuid, filter.value, filter.state);
                            if (!filter.local) {
                                $scope.model.forceUpdate();
                            }
                        }
                        if (filter.execute) {
                            filter.execute();
                        }
                    } else {
                        var force = false;
                        _($scope.model.wrapper.filters).each(function(filter) {
                            if (!filter.value) {
                                return;
                            }
                            widgetSettings.param($scope.model.uuid, filter.value, filter.state);
                            if (!filter.local) {
                                force = true;
                            }
                        });
                        if(force) {
                            $scope.model.forceUpdate();
                        }
                    }
                };
                
                $scope.getResolutionLabel = function() {
                    return _($scope.model.wrapper.resolution.variants).find(function(variant) {
                        return variant.value == $scope.model.wrapper.resolution.value;
                    }).label;
                };
                
                $scope.setResolution = function(event, variant) {
                    event.preventDefault();
                    $scope.model.wrapper.resolution.value = variant.value;
                    widgetSettings.param($scope.model.uuid, 'resolution.value', variant.value);
                    $(element).find('.resolution-value').dropdown('toggle');
                };

                $scope.icons = $scope.model.icons || {
                    minimize: 'fa-chevron-up',
                    expand: 'fa-chevron-down'
                };
            }
        }
    };
});