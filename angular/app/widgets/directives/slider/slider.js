var m = angular.module('widgets');

m.directive('slider', function(path, widgetLogic) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('widgets').directive('slider').template(),
        scope: {
            model: '=slider'
        },
        link: function($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);
        }
    };
});