$(window).load(function() {
    //Stop preloading animation
    Pace.stop();

    // Fade out the overlay div
    $('#overlay').fadeOut(800);

    $('body').removeAttr('class');

    //Enable animation
    $('#wrapper').removeClass('preload');

    //Collapsible Active Menu
    if(!$('#wrapper').hasClass('sidebar-mini'))	{
        $('aside').find('.active.openable').children('.submenu').slideDown();
    }

    $(window).trigger('resize');
});

$(window).scroll(function(){

    var position = $(window).scrollTop();

    //Display a scroll to top button
    if(position >= 200)	{
        $('#scroll-to-top').attr('style','bottom:8px;');
    }
    else	{
        $('#scroll-to-top').removeAttr('style');
    }
});
