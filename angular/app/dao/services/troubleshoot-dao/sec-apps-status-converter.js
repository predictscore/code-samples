var m = angular.module('dao');

m.factory('secAppsStatusConverter', function($q, modulesManager, entityTypes) {
    return function(input, args) {
        var rows = _(input.data).map(function (info, appName) {
            info.app = angular.copy(modulesManager.app(appName));
            info.appLabel = info.app.label_short || info.app.label;
            info.appName = appName;
            info.last_match_saas = (info.last_match_file && info.last_match_file.file_entity_type &&
                entityTypes.toSaas(info.last_match_file.file_entity_type) || null);

            info.last_match_local_entity = (info.last_match_file && info.last_match_file.file_entity_type &&
                entityTypes.toLocal(info.last_match_file.file_entity_type) || null);

            info.last_unmatch_saas = (info.last_unmatch_file && info.last_unmatch_file.file_entity_type &&
                entityTypes.toSaas(info.last_unmatch_file.file_entity_type) || null);

            info.last_unmatch_local_entity = (info.last_unmatch_file && info.last_unmatch_file.file_entity_type &&
                entityTypes.toLocal(info.last_unmatch_file.file_entity_type) || null);

            return info;
        });

        return $q.when({
            data: {
                rows: rows,
                total_rows: rows.length
            }
        });

    };
});