var m = angular.module('dao');

m.factory('troubleshootDao', function(sqlQueries, http, saasSummaryConverter, defaultListConverter, secAppsStatusConverter, secAppsStatusConverterParallel, $q, $filter) {
    function twoDigitsNumber(input) {
        if (input < 10) {
            return '0' + input;
        }
        return input;
    }
    return {
        secAppStats: function(appName, after) {
            return http.get(sqlQueries('scan_per_day')).params({
                app_name: appName,
                after: after
            });
        },
        mainQueueStats: function(after, group) {
            return http.get(sqlQueries(group ? 'main_queue_stats_group' : 'main_queue_stats')).params({
                after: after
            });
        },
        downloadHistory: function(after) {
            return http.get(sqlQueries('download_history')).params({
                after: after
            });
        },
        entityCount: function(entityName, interval, count, intervalMode, mode) {
            return http.get('/api/v1/entity/entity_count_by_date/' + entityName).params({
                days: count,
                interval: interval,
                interval_mode: intervalMode,
                mode: mode
            });
        },
        saasSummary: function (columns) {
            return http.get('/api/v1/saas_summary').converter(saasSummaryConverter, _.identity)
                .converter(defaultListConverter, {
                    columns: columns,
                    converters: {
                        saasIconConverter: function (text) {
                            return '#' + JSON.stringify({
                                    display: 'img',
                                    path: text,
                                    'class': 'saas-icon'
                                });
                        },
                        tokenStatusConverter: function (text) {
                            if (text) {
                                return '#' + JSON.stringify({
                                        display: 'text',
                                        'class': 'fa fa-check green-text token-status'
                                    });
                            } else {
                                return '#' + JSON.stringify({
                                        display: 'text',
                                        'class': 'fa fa-times red-text token-status'
                                    });
                            }
                        },
                        fullScanStatusConverter: function (text) {
                            if (text === 'done') {
                                return '#' + JSON.stringify({
                                        display: 'text',
                                        'class': 'fa fa-check-circle green-text fullscan-status',
                                        tooltip: 'Done'
                                    });
                            }
                            if (text === 'scanning') {
                                return '#' + JSON.stringify({
                                        display: 'text',
                                        'class': 'fa fa-cog fa-spin gray-text fullscan-status',
                                        tooltip: 'Scanning'
                                    });
                            }
                            return '#' + JSON.stringify({
                                    display: 'text',
                                    'class': 'fa fa-question-circle red-text fullscan-status',
                                    tooltip: 'Unknown'
                                });
                        }
                    }
                });
        },
        secAppsStatus: function (columns) {
            return http.get('/api/v1/apps/sec_apps_status')
                .converter(secAppsStatusConverter, _.identity)
                .converter(defaultListConverter, {
                    columns: columns
                });
        },
        secAppsStatusParallel: function (columns, saas, timeoutPromise) {
            return http.get('/api/v1/apps/sec_apps_status/license')
                .timeout(timeoutPromise)
                .converter(secAppsStatusConverterParallel, function (preFetches) {
                    return {
                        columns: columns,
                        saas: saas,
                        timeoutPromise: timeoutPromise
                    };
                });
        },
        entityPerUser: function(saas, type, mode, from, to, powBase) {
            return http.get(sqlQueries('troubleshoot/' + type, null, saas)).params({
                mode: mode,
                from: from,
                to: to,
                pow_base: powBase
            });
        },
        inlineMessages: function (saas, columns) {
            return http.get(sqlQueries('inline_messages'))
            .converter(defaultListConverter, {
                columns: columns,
                converters: {
                    userModuleConverter: function () {
                        return (saas === 'google_mail' ? 'google_drive': saas);
                    },
                    intervalPrecision: function (text, columnId, columnsMap, args) {
                        if (!_(text).isString()) {
                            return text;
                        }
                        if (args.precision !== 'deciseconds') {
                            return text;
                        }
                        var durationMS = moment.duration(text.replace(/days* \-*/ig,'')).asMilliseconds();
                        if (durationMS === 0) {
                            return text;
                        }
                        var sign = '';
                        var  roundedMS= Math.round(durationMS / 100) * 100;
                        if (roundedMS  < 0) {
                            sign = '-';
                            roundedMS = -roundedMS;
                        }
                        var duration = moment.duration(roundedMS);

                        var days = duration.days();
                        return sign + (days > 0 ? (days + ' days ') : '') +
                            twoDigitsNumber(duration.hours()) + ':' +
                            twoDigitsNumber(duration.minutes()) + ':' +
                            twoDigitsNumber(duration.seconds()) +
                            '.' + Math.round(duration.milliseconds() / 100);
                    }
                }
            });
        },
        inlineMessagesActions: function () {
            return http.get(sqlQueries('inline_messages_actions'));
        },
        webNotifications: function (columns, messageId) {
            var result = http.get(sqlQueries('troubleshoot/web_notifications')).params({
                message_id: messageId
            });
            result.converter(function (input) {
                _(input.data.rows).each(function (row) {
                    row._fullData = row;
                });
                return $q.when(input);
            });
            return result.converter(defaultListConverter, function (preFetches) {
                return {
                    columns: columns,
                    converters: {
                        missingDataConverter: function (text, columnId, columnsMap, args) {
                            var result = '';
                            _(text).each(function (value, colId) {
                                if (!(colId in columnsMap)) {
                                    var valueText;
                                    if (_(value).isObject()) {
                                        valueText = '#' + JSON.stringify({
                                            'display': 'text',
                                            'class': 'white-space-pre display-inline-block',
                                            text: $filter('json')(value)
                                        });
                                    } else {
                                        valueText = '#' + JSON.stringify({
                                                'display': 'text',
                                                'class': 'missing-columns-data-text',
                                                text: value
                                            });
                                    }
                                    result += '#' + JSON.stringify({
                                            display: 'linked-text',
                                            'class': 'missing-columns-data-line',
                                            text: '#' + JSON.stringify({
                                                'display': 'text',
                                                'class': 'missing-columns-data-label',
                                                text: colId
                                            }) + valueText
                                        });
                                }
                            });
                            return result;
                        }
                    }
                };
            });

        }
    };
});