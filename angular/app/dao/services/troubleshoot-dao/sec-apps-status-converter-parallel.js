var m = angular.module('dao');

m.factory('secAppsStatusConverterParallel', function($q, modulesManager, entityTypes, secAppsStatusDao, $timeout, defaultListConverter) {

    var reloadOnErrorTimeout = 20000;

    function queryReloader(getQuery, timeoutPromise) {
        return getQuery().timeout(timeoutPromise).run().then(function (input) {
            return input;
        }, function (error) {
            if (error.status > 0) {
                return $timeout(reloadOnErrorTimeout).then(function () {
                    return queryReloader(getQuery, timeoutPromise);
                });
            }
            return $q.reject(error);
        });
    }

    function entityConverter(data) {
        if (!data || !data.file_entity_type) {
            return null;
        }
        var local = entityTypes.toLocal(data.file_entity_type);
        var saas = entityTypes.toSaas(data.file_entity_type);
        if (!local || !saas) {
            return null;
        }
        var time = moment.utc(data.update_time).local();
        return '#' + JSON.stringify({
            text: time.fromNow(),
            tooltip: time.format('YYYY-MM-DD HH:mm:ss'),
            display: 'link',
            type: local,
            module: saas,
            id: data.file_entity_id
        });
    }

    function setCountsColumns (info, appName, saas, type, timeoutPromise, hours, position, additional) {
        var query = queryReloader(function () {
            return secAppsStatusDao.counts(appName, type, hours, saas);
        }, timeoutPromise).then(function (response) {
            return response.data;
        });
        info[type].promises[position] = query;
        if (additional) {
            info[type + '_' + additional] = query;
        }
    }

    function initCountsColumns (info, type) {
        info[type] = {
            multiPromise: true,
            promises: [null, null, null]
        };
    }

    function setDefault(value, def) {
        if (_(value).isNull() || _(value).isUndefined()) {
            return def;
        }
        return value;
    }


    return function(input, args) {

        var columns = args.columns;
        var timeoutPromise = args.timeoutPromise;

        _(['solgate']).each(function (appName) {
            if (!input.data[appName]) {
                var app = modulesManager.app(appName);
                input.data[appName] = {
                    app_label: app.label,
                    license_type: app.license_info && app.license_info.license_type,
                    expire_at: app.license_info && app.license_info.license_type
                };
            }
        });

        // multiple rows processing to try to load fastest data first

        var rows = _(input.data).map(function (info, appName) {
            info.app = angular.copy(modulesManager.app(appName));
            info.appLabel = info.app.label_short || info.app.label;
            info.appName = appName;

            var lastMatchQuery = queryReloader(function () {
                return secAppsStatusDao.lastMatch(appName, args.saas);
            }, timeoutPromise);

            info.last_match_file_update_time = lastMatchQuery.then(function (response) {
                return response.data && response.data.update_time;
            });

            info.last_match_file = lastMatchQuery.then(function (response) {
                return entityConverter(response.data);
            });

            var lastUnmatchQuery = queryReloader(function () {
                return secAppsStatusDao.lastUnmatch(appName, args.saas);
            }, timeoutPromise);

            info.last_unmatch_file_update_time = lastUnmatchQuery.then(function (response) {
                return response.data && response.data.update_time;
            });

            info.last_unmatch_file = lastUnmatchQuery.then(function (response) {
                return entityConverter(response.data);
            });

            initCountsColumns(info, 'success_unmatch');
            initCountsColumns(info, 'success_match');
            initCountsColumns(info, 'error');
            initCountsColumns(info, 'vendor');

            return info;
        });

        _(rows).each(function (info) {
            var appName = info.appName;

            var queueQuery = queryReloader(function () {
                return secAppsStatusDao.queue(appName, args.saas);
            }, timeoutPromise);

            info.queue_count = queueQuery.then(function (response) {
                return response.data && response.data.queue_count;
            });

            info.oldest_queue_insert_time = queueQuery.then(function (response) {
                return response.data && response.data.oldest_queue_insert_time;
            });
        });

        _(rows).each(function (info) {
            var appName = info.appName;

            setCountsColumns(info, appName, args.saas, 'success_unmatch', timeoutPromise, 1, 2);
            setCountsColumns(info, appName, args.saas, 'success_match', timeoutPromise, 1, 2);
            setCountsColumns(info, appName, args.saas, 'error', timeoutPromise, 1, 2);
            setCountsColumns(info, appName, args.saas, 'vendor', timeoutPromise, 1, 2);
        });

        _(rows).each(function (info) {
            var appName = info.appName;

            var processTimeQuery = queryReloader(function () {
                return secAppsStatusDao.processTime(appName, args.saas);
            }, timeoutPromise);

            info.avg_process_time = processTimeQuery.then(function (response) {
                return response.data && response.data.avg_process_time;
            });

            info.combined_process_time = processTimeQuery.then(function (response) {
                return [
                    setDefault(response.data && response.data.avg_process_time, '-'),
                    setDefault(response.data && response.data.max_process_time, '-'),
                    setDefault(response.data && response.data.min_process_time, '-')
                ].join(' / ');
            });

            var vendorTimeQuery = queryReloader(function () {
                return secAppsStatusDao.vendorTime(appName, args.saas);
            }, timeoutPromise);

            info.avg_vendor_time = vendorTimeQuery.then(function (response) {
                return response.data && response.data.avg_vendor_time;
            });

            info.combined_vendor_time = vendorTimeQuery.then(function (response) {
                return [
                    setDefault(response.data && response.data.avg_vendor_time, '-'),
                    setDefault(response.data && response.data.max_vendor_time, '-'),
                    setDefault(response.data && response.data.min_vendor_time, '-')
                ].join(' / ');
            });

        });

        _(rows).each(function (info) {
            var appName = info.appName;

            setCountsColumns(info, appName, args.saas, 'success_unmatch', timeoutPromise, 24, 1);
            setCountsColumns(info, appName, args.saas, 'success_match', timeoutPromise, 24, 1);
            setCountsColumns(info, appName, args.saas, 'error', timeoutPromise, 24, 1);
            setCountsColumns(info, appName, args.saas, 'vendor', timeoutPromise, 24, 1);
        });


        _(rows).each(function (info) {
            var appName = info.appName;

            setCountsColumns(info, appName, args.saas, 'success_unmatch', timeoutPromise, 0, 0, 'total');
            setCountsColumns(info, appName, args.saas, 'success_match', timeoutPromise, 0, 0, 'total');
            setCountsColumns(info, appName, args.saas, 'error', timeoutPromise, 0, 0, 'total');
            setCountsColumns(info, appName, args.saas, 'vendor', timeoutPromise, 0, 0, 'total');
        });



        return defaultListConverter({
            data: {
                rows: rows,
                total_rows: rows.length
            }
        }, {
            columns: columns,
            converters: {
                emptyConverter: function () {
                    return '';
                }
            }
        }).then(function (data) {
            _(data.data.data).each(function (row) {
                _(row).each(function (col) {
                    if (col.originalText && col.originalText.multiPromise) {
                        col.originalText.values = [];
                        _(col.originalText.promises).each(function (promise, idx) {
                            col.originalText.values.push('Loading...');
                            promise.then(function (resolvedText) {
                                col.originalText.values[idx] = resolvedText;
                                col.text = col.originalText.values.join(' / ');
                            });
                        });
                        col.text = col.originalText.values.join(' / ');
                    }
                    if (col.text && col.text.then) {
                        col.text.then(function (resolvedText) {
                            col.text = resolvedText;
                        });
                    }
                });
            });
            return data;
        });

    };
});