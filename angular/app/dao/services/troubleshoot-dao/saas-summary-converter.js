var m = angular.module('dao');

m.factory('saasSummaryConverter', function($q, modulesManager, entityTypes) {
    return function(input, args) {

        var rows = _(input.data).chain().map(function (row, saas) {
            row.appName = saas;
            row.app = angular.copy(modulesManager.cloudApp(saas));
            if (!row.app) {
                return null;
            }
            row.local_entity = entityTypes.toLocal(row.entity_type) || null;
            return row;
        }).compact().value();
        return $q.when({
            data: {
                rows: rows,
                total_rows: rows.length
            }
        });
    };
});