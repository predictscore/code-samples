var m = angular.module('dao');

m.factory('policyReportV2Converter', function ($q, defaultListConverter, $filter, path, entityTypes, policyFilterTypes, dao, policyPropertyPathToId, defaultTableConverters, linkedText) {
    var root = path('policy');

    function normalizeExtension(ext) {
        if (!ext) {
            return ext;
        }
        if (/^\./.test(ext)) { //if 'ext' starts with dot (".") - return as is
            return ext;
        }
        return '.' + ext;
    }

    return function(input, args) {
        var policy = input.data.policy;
        var table = input.data.table;
        var rows = input.data.rows;
        var properties = args.properties;
        
        policy.policy_data.actions = policy.policy_data.actions || [];
        policy.policy_data.columnsToShow = policy.policy_data.columnsToShow || {};
        policy.policy_data.columnNames = policy.policy_data.columnNames || {};

        var colValueMap = {};
        var idToPath = {};
        var originalIdToColumnIdx = {};

        var columns = _(table.schema).chain().orderObject({key: 'id'}).map(function(col, idx) {
            var id = policyPropertyPathToId.toId(col.path);
            var propIdEnd = _(col.path.split('.')).last(2).join('.');
            var prop = args.properties[propIdEnd] || {};
            idToPath[col.id] = id;
            originalIdToColumnIdx[col.id] = idx;
            var isAggregated = (policy.policy_data.columnsToShow[id] || {}).aggregated || false;
            var result = {
                propName: col.field,
                id: id,
                colId: col.id,
                primary: false,
                primarySubId: !_(input.data.table.path_to_id_column[col.path]).isUndefined(),
                hidden: col.field_type == 'entity_id' || _(col.order).isNull(),
                sortable: false,
                sortableTooltip: col.aggregated ? undefined : 'Pending for policy counters',
                text: col.label,
                def: '',
                filter: {
                    name: id,
                    type: policyFilterTypes(prop),
                    'class': 'hide-on-system'
                },
                warning: {
                    'class': 'hide-on-custom',
                    text: '#' + JSON.stringify({
                        display: 'text',
                        text: 'Conditions disabled '
                    }) + '#' + JSON.stringify({
                        display: 'text',
                        'class': 'fa fa-question-circle',
                        tooltip: 'This is a system query and can not be altered. To change the query conditions, first save-as it and create a new, edit-enabled, query.'
                    })
                },
                clearAllClass: 'hide-on-system',
                path: col.path,
                removable: true,
                tooltip: {
                    content: col.path,
                    position: {
                        my: 'bottom left',
                        at: 'top left'
                    }
                },
                'class': 'cursor-pointer',
                aggregated: col.aggregated,
                dataClass: {
                    'hide-allow-buttons': isAggregated,
                    'policy-report-cell': true
                },
                mainProperty: col.path.indexOf(':') === -1,
                aggregatable: col.aggregatable
            };

            var valueMap = {};
            if(!result.hidden && _(col.path).isString()) {
                _(prop.variants).each(function(opt) {
                    if(_(opt.icon).isUndefined()) {
                        valueMap[opt.id] = opt.label;
                    } else if(_(opt.icon).isNull()) {
                        valueMap[opt.id] = '';
                    } else {
                        valueMap[opt.id] = '#' + JSON.stringify({
                            display: 'img',
                            path: root.img([
                                opt.icon
                            ].join('/')),
                            tooltip: {
                                content: opt.label,
                                position: {
                                    my: 'bottom right',
                                    at: 'top center'
                                }
                            }
                        });
                    }
                });
                if(!_(prop.defaultValue).isUndefined()) {
                    result.def = valueMap[prop.defaultValue] || prop.defaultValue;
                }
                result.entityLabel = prop.mainIdentifier;
                if(prop.isExternalLink) {
                    result.converter = function(text) {
                        return '#' + JSON.stringify({
                            display: 'external-link',
                            href: text,
                            text: prop.externalLinkLabel
                        });
                    };
                }
            }
            colValueMap[id] = valueMap;
            if(!_(valueMap).isEqual({})) {
                result.converter = function(text) {
                    return valueMap[text];  
                };
            }
            var useOriginalForActions = false;
            if (prop.showAsBytes) {
                result.format = 'bytes';
                result.bytesShortForm = true;
                useOriginalForActions = true;
            }

            if(col.value_type == 'DATE' || col.value_type == 'DATETIME') {
                result.format = 'time';
            }

            if(col.is_list) {
                result.converterArgs = {
                    countPostfix: 'items',
                    format: result.format,
                    itemConverter: result.converter
                };
                result.converter = 'listConverter';
            }

            function hrefConverter(type, module, entityType, column, prevConverter) {
                return function(text, columnId, columnsMap) {
                    return $q.when(columnsMap[column]).then(function(resolvedValue) {
                        text = prevConverter(text, columnId, columnsMap) || text;
                        if (_(resolvedValue).isNumber()) {
                            resolvedValue = '' + resolvedValue;
                        }
                        if(_(resolvedValue).isString()) {
                            var link = {
                                display: 'link',
                                type: type,
                                id: _(resolvedValue.split('/')).last(),
                                text: text,
                                'class': text ? '' : 'fa fa-link'
                            };
                            if (module) {
                                link.module = module;
                            } else {
                                link.entityType = entityType;
                            }
                            return '#' + JSON.stringify(link);
                        }
                        return text;
                    });
                };
            }

            if(!col.is_list && col.is_link) {
                result.link = {
                    id: idToPath[input.data.table.path_to_id_column[col.parent_path]]
                };
                if (entityTypes.toSaas(col.entity_type)) {
                    result.link.type = entityTypes.toLocal(col.entity_type);
                    result.link.saas = entityTypes.toSaas(col.entity_type);
                } else {
                    result.link.type = 'profile';
                    result.link.entityType = col.entity_type;
                }
                result.converter = hrefConverter(result.link.type, result.link.saas, result.link.entityType, result.link.id, result.converter || _.identity);
                result.convertDef = true;
            }

            function colIdtoId(colId) {
                return _(columns).find(function(c) {
                    return c.colId == colId;
                }).id;
            }

            if (!col.metadata && prop.showWithFolderIcon) {
                col.metadata = {
                    "type": "folder_icon"
                };
            }

            if(!col.is_list && col.metadata) {
                ({
                    'user_image': function(params) {
                        var prevConverter = result.converter || _.identity;
                        result.class = [result.class, 'wide-column'].join(' ');
                        result.dataClass['wide-column'] = true;
                        result.converter = function(text, columnId, columnsMap) {
                            return $q.all([
                                columnsMap[colIdtoId(params.image_url)],
                                prevConverter(text, columnId, columnsMap)
                            ]).then(function(resolves) {
                                var imgPath = resolves[0];
                                var prevConverterResolve = resolves[1];
                                return '#' + JSON.stringify({
                                    display: 'img',
                                    path: imgPath,
                                    defaultPath: path('profiles').img('user.png'),
                                    'class': 'row-icon'
                                }) + '#' + JSON.stringify({
                                    display: 'linked-text',
                                    'class': 'row-text',
                                    text: prevConverterResolve
                                });
                            });
                        };
                    },
                    'mime_icon': function(params) {
                        var prevConverter = result.converter || _.identity;
                        result.class = [result.class, 'wide-column'].join(' ');
                        result.dataClass['wide-column'] = true;
                        result.converter = function(text, columnId, columnsMap, converterArgs) {
                            return $q.all([
                                columnsMap[colIdtoId(params.mime_type)],
                                prevConverter(text, columnId, columnsMap, converterArgs)
                            ]).then(function(resolves) {
                                var mimeType = resolves[0];
                                var prevConverterResolve = resolves[1];
                                return '#' + JSON.stringify({
                                    display: 'img',
                                    path: args.mimeIcons(mimeType),
                                    'class': 'row-icon',
                                    tooltip: mimeType
                                }) + '#' + JSON.stringify({
                                    display: 'linked-text',
                                    'class': 'row-text',
                                    text: prevConverterResolve
                                });
                            });
                        };
                    },
                    'extension_icon': function(params) {
                        var prevConverter = result.converter || _.identity;
                        result.class = [result.class, 'wide-column'].join(' ');
                        result.dataClass['wide-column'] = true;
                        var extensions = _(rows).map(function(row) {
                            return normalizeExtension(row[params.extension]);
                        });
                        var extensionsPromise = dao.file().extensionsToMimeType(extensions);
                        result.converter = function(text, columnId, columnsMap, converterArgs) {
                            return $q.all([
                                extensionsPromise,
                                $q.when(columnsMap[colIdtoId(params.extension)]),
                                prevConverter(text, columnId, columnsMap, converterArgs)
                            ]).then(function(resolves) {
                                var extension = normalizeExtension(resolves[1]);
                                var found = _(resolves[0].data.rows).find(function(row) {
                                    return _(row.file_extension).contains(extension);
                                });
                                var prevConverterResolve = resolves[2];
                                var mimeType = 'unknown';
                                if(found) {
                                    mimeType = found.mime_type_string;
                                }
                                return '#' + JSON.stringify({
                                    display: 'img',
                                    path: args.mimeIcons(mimeType),
                                    'class': 'row-icon',
                                    tooltip: mimeType
                                }) + '#' + JSON.stringify({
                                    display: 'linked-text',
                                    'class': 'row-text',
                                    text: prevConverterResolve
                                });
                            });
                        };
                    },
                    'folder_icon': function() {
                        var prevConverter = result.converter || _.identity;
                        result.class = [result.class, 'wide-column'].join(' ');
                        result.dataClass['wide-column'] = true;
                        result.converter = function(text, columnId, columnsMap, converterArgs) {
                            return $q.when(prevConverter(text, columnId, columnsMap, converterArgs))
                                .then(function(prevConverterResolve) {
                                    return '#' + JSON.stringify({
                                            display: 'img',
                                            path: args.mimeIcons('folder'),
                                            'class': 'row-icon'
                                        }) + '#' + JSON.stringify({
                                            display: 'linked-text',
                                            'class': 'row-text',
                                            text: prevConverterResolve
                                        });
                                });
                        };
                    }
                }[col.metadata.type] || _.noop)(col.metadata.params);
            }

            if (!args.noAddToAllows) {
                var prevConverter =  result.converter || _.identity;
                result.converter = function(text, columnId, columnsMap, args) {
                    function addActions(value, list, aggregated) {
                        var textValue = linkedText.toText(value);
                        var columnName = policy.policy_data.columnNames[col.path] || col.label;
                        var type = properties[policy.entity_type].label;
                        var allowTooltip = "Exclude all " + type + " with '" + textValue + "' as " + columnName;
                        var filterTooltip = "Show only " + type + " with '" + textValue + "' as " + columnName;
                        if(result.link && result.link.id == policy.entity_type) {
                            allowTooltip = "Exclude this " + type;
                        }
                        if(aggregated) {
                            allowTooltip = "Select only " + type + " that do not have any " + columnName + " with value '" + textValue + "'";
                            filterTooltip = "Select only " + type + " that have at least one " + columnName + " with value '" + textValue + "'";
                        }
                        return '#' + JSON.stringify({
                            display: 'linked-text',
                            'class': list ? 'cell-list-actions' : 'cell-actions',
                            text: '#' + JSON.stringify({
                                display: 'text',
                                'class': 'fa fa-check dark-blue-text cell-allow cursor-pointer',
                                tooltip: allowTooltip,
                                clickEvent: {
                                    name: 'cell-action',
                                    action: aggregated ? 'allow-aggregated' : 'allow',
                                    text: value,
                                    aggregated: aggregated,
                                    columnId: columnId,
                                    linkValue: result.link ? columnsMap[result.link.id] : undefined
                                }
                            }) + '#' +JSON.stringify({
                                display: 'text',
                                'class': 'fa fa-filter dark-blue-text cell-filter cursor-pointer hide-on-system',
                                tooltip: filterTooltip,
                                clickEvent: {
                                    name: 'cell-action',
                                    action: aggregated ? 'filter-aggregated' : 'filter',
                                    text: value,
                                    aggregated: aggregated,
                                    columnId: columnId,
                                    linkValue: result.link ? columnsMap[result.link.id] : undefined
                                }
                            })
                        });
                    }
                    if(prevConverter == 'listConverter' && result.converterArgs.itemConverter) {
                        result.actionsItemConverter = result.converterArgs.itemConverter;
                        delete result.converterArgs.itemConverter;
                    }
                    if(_(prevConverter).isString()) {
                        var converters = defaultTableConverters(args);
                        prevConverter = converters[prevConverter];
                    }
                    if(_(text).isArray()/*&& text.length < 100*/) {
                        text = _(text).chain().reduce(function(memo, v) {
                            memo[v] = memo[v] || 0;
                            memo[v]++;
                            return memo;
                        }, {}).map(function(count, textVal) {
                            var itemsCount = count > 1 ? '#' + JSON.stringify({
                                display: 'text',
                                class: 'display-inline-block',
                                text: ' (' + count + ' Items)'
                            }) : '';
                            var textValWrap = '#' + JSON.stringify({
                                display: 'text',
                                class: 'word-break-all',
                                text: result.actionsItemConverter ? result.actionsItemConverter(textVal) : textVal
                            });
                            return [textValWrap + itemsCount + addActions(textVal, text.length != 1, true), count];
                        }).sortBy(function(e) {
                            return -e[1];
                        }).map(function(e) {
                            return e[0];
                        }).value();
                    }
                    var promise = $q.when(prevConverter(text, columnId, columnsMap, args));
                    if(_(text).isUndefined() || text === '' || (_(text).isArray()) || col.hidden) {
                        return promise;
                    }
                    return promise.then(function(value) {
                        return value + addActions(useOriginalForActions ? columnsMap[columnId] : text, false, false);
                    });
                };
            }
            return result;
        }).value();

        columns.push({
            id: 'rowPrimaryKey',
            hidden: true,
            converter: 'composition',
            converterArgs: _(input.data.table.path_to_id_column).chain().map(function(value, key) {
                var found = _(columns).find(function(column) {
                    return column.colId == value;
                });
                return [key, found.id];
            }).object().value(),
            primary: true
        });
        
        var deferreds = [];

        colValueMap.actions = {};

        var rowsAsObjects = _(rows).map(function(row) {
            var result = {};
            var deferred = null;
            var rowPromise = null;
            _(row).each(function(val, colId) {
                var column = columns[originalIdToColumnIdx[colId]];
                if(column && column.aggregated) {
                    deferred = deferred || $q.defer();
                    var deferredVar = $q.defer();
                    rowPromise = rowPromise || deferred.promise.then(function(val) {
                        return dao.policyReport().getRowAggregation(policy.policy_id, row, args.status.toLowerCase());
                    });
                    rowPromise.then(function(row) {
                        var colData = row.data[colId];
                        if(_(colData).isArray()) {
                            colData = _(colData).filter(function(data) {
                                return !_(data).isNull();
                            });
                        }
                        deferredVar.resolve(colData);  
                    }, deferredVar.reject);
                    result[idToPath[colId]] = deferredVar.promise;
                } else {
                    result[idToPath[colId]] = val;
                }
            });
            if(!_(deferred).isNull()) {
                deferreds.push(deferred);
            }
            return result;
        });
        
        var haveNext;
        if(input.data.total_rows === 0 && input.data.rows.length === input.data.per_page) {
            haveNext = true;
        }

        return defaultListConverter($.extend({}, input, {
            data: {
                total_rows: input.data.total_rows,
                page: input.data.page,
                haveNext: haveNext,
                rows: rowsAsObjects
            }
        }), {
            actions: args.actions,
            columns: columns,
            options: {
                autoUpdate: undefined,
                checkboxes: true,
                disableSelectAll: false
            },
            converters: args.converters
        }).then(function(response) {
            response.data.deferreds = deferreds;
            response.data.columns = columns;
            return $q.when(response);
        });
    };
});
