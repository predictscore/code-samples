var m = angular.module('dao');

m.factory('policyReportColumnsV2Converter', function ($q) {
    return function (input) {
        var columns = _(input.data.table.schema).sortBy(function(col, colId) {
            col.uniqueId = colId;
            return col.idx || 0;
        });
        var minIndex = -1;
        for (var i = 0; minIndex === -1 && i < columns.length; i++) {
            minIndex = columns[i].field_type != 'entity_id' ? i : -1;
        }
        input.columns = columns;
        if (minIndex !== -1) {
            input.defaultOrderIndex = minIndex;
        }
        return $q.when(input);
    };
});
