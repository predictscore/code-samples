var m = angular.module('dao');

m.factory('policyReportDao', function ($q, http, policyReportConverter, policyReportV2Converter, policyDao, sqlQueries, policyReportColumnsConverter, policyReportColumnsV2Converter, mimeIcons, feature) {
    var policyReportDao = {
        retrieve: function (args) {
            var policyId = args.policyId;
            var properties = args.properties;
            var actions = args.actions;
            var page = args.page;
            var perPage = args.perPage;
            var filter = args.filter;
            var columnsFilters = args.columnsFilters;
            var orderBy = args.orderBy;
            var status = args.status;
            var stateCallback = args.stateCallback || _.noop;
            return policyReportDao.getColumns(policyId)
                .converter(function(response, args) {
                    if (!_(response.defaultOrderIndex).isUndefined()) {
                        var fixOrderBy = false;
                        if (_(orderBy).isString()) {
                            var orderByCol = orderBy.split(' ')[0];
                            fixOrderBy = _(response.columns).chain().find(function(col) {
                                return col.uniqueId === orderByCol;
                            }).isUndefined().value();
                        } else {
                            fixOrderBy = true;
                        }
                        if (fixOrderBy) {
                            orderBy = response.columns[response.defaultOrderIndex].uniqueId + ' ASC';
                        }
                    }

                    var url = '/api/v1/policy/' + policyId + '/list/' + status.toLowerCase();
                    var params = {
                        page: page,
                        per_page: perPage,
                        filter: filter
                    };
                    var converter = policyReportV2Converter;
                    return http.get(url).timeout(args.http.timeoutPromise).onRequestFail(function() {
                        response.data.rows = [];
                        return $q.when(response);
                    }).params(params)
                        .preFetch(mimeIcons(), 'mimeIcons')
                            .converter(converter, function(preFetches) {
                                return {
                                    status: status,
                                    current: page,
                                    properties: properties,
                                    actions: actions,
                                    stateCallback: stateCallback,
                                    mimeIcons: preFetches.mimeIcons
                                };
                            }).then(function (response) {
                                return $q.when(response);
                            });
                    });
        },
        getRowAggregation: function(policyId, row, status) {
            return http.post('api/v1/policy/' + policyId + '/list/' + status + '/row/aggregate').body({
                row: row
            });
        },
        getColumns: function (policyId) {
            if(feature.newPolicyApi) {
                return http.get('/api/v1/policy/' + policyId + '/list/info')
                    .converter(policyReportColumnsV2Converter);
            }
            return http.get('/api/v1/policies/dynamic_policy_report/' + policyId).params({
                info_only: 1
            }).converter(policyReportColumnsConverter);
        },
        filePolicyRootCauses: function(policyId, rootCauseType) {
            return http.get(sqlQueries('viewer/file_policy_root_causes')).params({
                policy_id: policyId,
                root_cause_type: rootCauseType
            });
        },
        folderPolicyRootCauses: function(policyId, rootCauseType) {
            return http.get(sqlQueries('viewer/folder_policy_root_causes')).params({
                policy_id: policyId,
                root_cause_type: rootCauseType
            });
        },
        dataPolicyRootCauses: function(type, policyId, rootCauseType) {
            if(type == 'folder') {
                return policyReportDao.folderPolicyRootCauses(policyId, rootCauseType);
            } else if(type == 'file') {
                return policyReportDao.filePolicyRootCauses(policyId, rootCauseType);
            }
            return $q.when(null);
        },
        exportCsv: function(policyId) {
            return http.post('/api/v1/policies/policies_catalog/' + policyId + '/send_result').run();
        }
    };
    return policyReportDao;
});
