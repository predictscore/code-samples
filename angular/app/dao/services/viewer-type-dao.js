var m = angular.module('dao');

m.factory('viewerTypeDao', function ($http, http, path, lazyFuture, futureCache, $q, defaultListConverter, mimeIcons, singleValueConverter, sqlQueries, modulesManager, policyDao, advancedViewerDao, viewerColumnsFiltersConverter) {
    var root = path('viewer').ctrl('viewer');

    function retrieveTypes() {
        return futureCache.cache('viewer-types', 60000, function () {
            return root.json('types').retrieve();
        });
    }

    var viewerTypeDao = {
        types: retrieveTypes,
        count: function (type, params, saas) {
            return retrieveTypes().then(function (response) {
                var metaInfo = response.data[type];
                var dataType = metaInfo.type || 'viewer';
                if (dataType == 'viewer') {
                    return http.get(sqlQueries('viewer/' + metaInfo.url, false, saas)).params($.extend({
                        queryCount: 1
                    }, params)).converter(singleValueConverter, {
                        name: 'count'
                    });
                } else if (dataType == 'custom' && metaInfo.route == 'policy-root-cause-view') {
                    var internalName = metaInfo.params[modulesManager.currentModule()];
                    if(_(internalName).isObject()) {
                        internalName = internalName.id;
                    }
                    return policyDao.retrieveByInternalName(internalName).then(function (policy) {
                        return policyDao.findingsStatus(policy.data.policy_id, true).then(function (findingStatus) {
                            return $q.when({
                                data: findingStatus.data[policy.data.policy_id].Match
                            });
                        });
                    });
                } else if (dataType == 'custom' && metaInfo.route == 'advanced-viewer') {
                    var module = modulesManager.currentModule();
                    var viewerType = metaInfo.params[module].type;
                    var preFilter = metaInfo.params[module].preFilter;
                    return path('viewer').ctrl('advanced-viewer').json(module + '/' + viewerType).retrieve().then(function(response) {
                        var args = response.data.preFilters[preFilter];
                        return advancedViewerDao.count(response.data.options.dataUrl)
                            .params(viewerColumnsFiltersConverter(args.columnsFilters))
                            .params(_(args.facets).chain().map(function(value, key) {
                                return [key, value.join(',')];
                            }).object().value());
                    });
                }
            });
        },
        listColumns: function (type) {
            return root.json(modulesManager.currentModule() + '/' + type).retrieve();
        },
        emailExport: function (type, params, exportFormat) {
            return http.get(function (preFetches) {
                return sqlQueries('viewer/' + preFetches.types.data[type].url, exportFormat);
            }).params(params || {})
                .preFetch(retrieveTypes(), 'types');
        },
        list: function (columns, type, params, preConverters) {
            return http.get(function (preFetches) {
                return sqlQueries('viewer/' + preFetches.types.data[type].url);
            }).params(params || {})
                .preFetch(retrieveTypes(), 'types')
                .preFetch(mimeIcons(), 'mimeIcons')
                .dynamicConverter(function (preFetches) {
                    return preConverters[preFetches.types.data[type].preConverter];
                })
                .converter(defaultListConverter, function (preFetches) {
                    return {
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns,
                        converters: {
                            installedAppConverter: function (text, columnId, columnsMap) {
                                return lazyFuture(function () {
                                    return viewerTypeDao.count('users-with-app', {
                                        app_id: columnsMap.entity_id
                                    }).then(function (usersWithAppResponse) {
                                        return $q.when('#' + JSON.stringify({
                                            display: 'link',
                                            type: 'viewer',
                                            id: 'users-with-app',
                                            qs: {
                                                app_id: columnsMap.entity_id
                                            },
                                            text: usersWithAppResponse.data
                                        }));
                                    });
                                });
                            },
                            scopesListConverter: function (text, columnId, columnsMap) {
                                text = text || [];
                                return '#' + JSON.stringify({
                                        display: 'inline-list',
                                        'class': 'medium-width point-prefix',
                                        data: _(text).chain().map(function (label, idx) {
                                            return label + ' (' + columnsMap.scopes_risk[idx] + ')';
                                        }).sortBy(function (label, idx) {
                                            return -columnsMap.scopes_risk[idx] * 10;
                                        }).map(function (label, idx) {
                                            return '#' + JSON.stringify({
                                                    display: 'text',
                                                    text: label
                                                });
                                        }).value(),
                                        countText: text.length + ' scopes'
                                    });
                            },
                            applicationRiskFactorConverter: function (text, columnId, columnsMap) {
                                function sum(memo, val) {
                                    return memo + val;
                                }

                                var lowRisk = _(text).chain().filter(function (risk) {
                                    return risk <= 3;
                                }).reduce(sum, 0).value();

                                var mediumRisk = _(text).chain().filter(function (risk) {
                                    return risk > 3 && risk < 6;
                                }).reduce(sum, 0).value();

                                var highRisk = _(text).chain().filter(function (risk) {
                                    return risk >= 6;
                                }).reduce(sum, 0).value();

                                return '#' + JSON.stringify({
                                        display: 'stacked-progress',
                                        total: columnsMap.max_risk,
                                        data: [{
                                            value: lowRisk,
                                            'class': 'progress-bar-success'
                                        }, {
                                            value: mediumRisk,
                                            'class': 'progress-bar-warning'
                                        }, {
                                            value: highRisk,
                                            'class': 'progress-bar-danger'
                                        }]
                                    });
                            },
                            opswatFoundByConverter: function (text) {
                                var av = _(text).chain().map(function (value, key) {
                                    if (value.scan_result_i) {
                                        return key;
                                    }
                                    return null;
                                }).filter(function (value) {
                                    return !_(value).isNull();
                                }).value();
                                return '#' + JSON.stringify({
                                        display: 'inline-list',
                                        'class': 'small-width',
                                        data: av,
                                        countText: av.length + ' AVs'
                                    });
                            },
                            recipientsConverter: function(text) {
                                if(!_(text).isArray() || text.length === 0) {
                                    return '';
                                }
                                return '#' + JSON.stringify({
                                    display: 'inline-list',
                                    'class': 'medium-width',
                                    data: _(text).chain().sortBy(function(recipient) {
                                        return {
                                            'to': 0,
                                            'cc': 1,
                                            'bcc': 2
                                        }[recipient.type];
                                    }).map(function(recipient) {
                                        return recipient.type.toUpperCase() + ': ' + '#' + JSON.stringify({
                                            display: 'link',
                                            type: 'user',
                                            id: recipient.entity_id,
                                            text: recipient.displayName
                                        });
                                    }).value(),
                                    countText: text.length + ' recipients'
                                });
                            }
                        }
                    };
                });
        }
    };
    return viewerTypeDao;
});
