var m = angular.module('dao');

m.factory('groupDao', function(http, sqlQueries, singleRowConverter, defaultListConverter, mimeIcons) {
    return {
        events: function(columns, after, groupId) {
            return http.get(sqlQueries('group_events')).params({
                group_id: groupId,
                after: after
            }).preFetch(mimeIcons(), 'mimeIcons').converter(defaultListConverter, function(preFetches) {
                return {
                    mimeIcons: preFetches.mimeIcons,
                    columns: columns
                };
            }).showTotalRows(false);
        },
        retrieve: function(groupId, localType) {
            localType = localType || 'group';
            return http.get(sqlQueries('entity/' + localType)).params({
                entity_id: groupId
            }).converter(singleRowConverter);
        },
        groupChannels: function(columns, groupId) {
            return http.get(sqlQueries('group_channels')).params({
                group_id: groupId
            }).converter(defaultListConverter, {
                columns: columns
            });
        }
    };
});