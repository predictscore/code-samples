var m = angular.module('dao');

m.factory('wizardDao', function(http, $q, modulesDao, configDao, wizardAppsConverter) {
    return {
        getStatus: function () {
            return http.get('/api/v1/wizard/status')
                .converter(function (response) {
                    return $q.when(response.data.status);
                });
        },
        setStatus: function (status) {
            return http.post('/api/v1/wizard/status').body({
                status: status
            }).run();
        },
        getStep: function () {
            return http.get('/api/v1/wizard/step')
                .converter(function (response) {
                    return $q.when(response.data.step);
                });
        },
        setStep: function (step) {
            return http.post('/api/v1/wizard/step').body({
                step: step
            }).run();
        },
        getApps: function (args) {
            return modulesDao.getModules()
                .preFetch(configDao.param('wizard_opswat_package'), 'wizard_opswat_package')
                .preFetch(modulesDao.opswatLicense(), 'opswatLicense')
                .converter(wizardAppsConverter, function (preFetches) {
                    return $.extend({}, preFetches, args);
                });
        },
        startApps: function (args) {
            return  http.post('/api/v1/wizard/start_apps').body(args).run();
        },
        wizardApps: function () {
            return configDao.getConfJson('wizard_apps');
        }

    };
});