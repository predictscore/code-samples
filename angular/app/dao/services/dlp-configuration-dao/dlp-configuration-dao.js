var m = angular.module('configuration');

m.factory('dlpConfigurationDao', function(http, defaultListConverter, findIdConverter, dlpRuleTypes, $q) {
    function ruleSaveConverter(rule) {
        var copy = angular.copy(rule);
        var found = _(dlpRuleTypes).find(function(type) {
            return type.id == copy.type;
        });
        if (found && found.noValue) {
            copy.value = null;
            copy.ui_info.builder = {};
        }
        return copy;
    }
    return {
        retrieveRule: function(ruleId) {
            return http.get('/api/v1/dlp_rules').converter(findIdConverter, {
                id: ruleId
            });
        },
        createRule: function(rule) {
            return http.post('/api/v1/dlp_rules').body(ruleSaveConverter(rule)).run();
        },
        updateRule: function(ruleId, rule) {
            return http.put('/api/v1/dlp_rules/' + ruleId).body(ruleSaveConverter(rule)).run();
        },
        removeRule: function(ruleId) {
            return http.delete('/api/v1/dlp_rules/' + ruleId).run();
        },
        listRules: function(columns, excludePredefined) {
            var request = http.get('/api/v1/dlp_rules');
            if (excludePredefined) {
                request.converter(function (input) {
                    input.data = _(input.data).filter(function (row) {
                        return !row.is_predefined;
                    });
                    return $q.when(input);
                });
            }
            return request.converter(defaultListConverter, {
                columns: columns,
                converters: {
                    dlpRuleTypeConverter: function(text) {
                        var found = _(dlpRuleTypes).find(function(type) {
                            return type.id == text;
                        });
                        return found ? found.label : text;
                    }
                }
            });
        }
    };
});