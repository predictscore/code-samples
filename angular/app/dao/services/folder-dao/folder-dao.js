var m = angular.module('dao');

m.factory('folderDao', function (http, defaultListConverter, defaultMapConverter, $q, singleValueConverter, fileDao, sqlQueries, singleRowConverter, treeContentConverter, mimeIcons, countRowsConverter, $filter, path, errorStatusHolder, $injector) {
    var folderDao = {
        events: function(columns, after, folderId) {
            return http.get(sqlQueries('folder_events')).params({
                folder_id: folderId,
                after: after
            }).preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function (preFetches) {
                    return {
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns
                    };
                }).showTotalRows(false);
        },
        retrieve: function (folderId, linkConverterName) {
            var result = http.get(sqlQueries('entity/folder')).params({
                entity_id: folderId
            }).converter(singleRowConverter)
                .onRequestFail(function (error) {
                    if (error.status === 404) {
                        errorStatusHolder.errorStatus(404);
                    }
                    return $q.reject(error);
                });
            if (linkConverterName) {
                result = result.converter($injector.get(linkConverterName), {
                    type: 'folder'
                });
            }
            return result;
        },
        size: function (folderId) {
            return http.get(sqlQueries('folder_size')).params({
                folder_id: folderId
            }).converter(singleValueConverter, {
                name: 'sum'
            });
        },
        permissions: function (columns, folderId) {
            return http.get(sqlQueries('folder_permissions')).params({
                folder_id: folderId
            }).order('natural_order', 'asc')
                .preFetch(folderDao.retrieve(folderId), 'folder')
                .converter(function (input) {
                    var prevSection = '';
                    input.data.rows = _(input.data.rows).chain().sortBy('section').map(function (row) {
                        if (prevSection && prevSection !== row.section) {
                            row.rowClass = 'section-change';
                        }
                        prevSection = row.section;
                        return row;
                    }).value();
                    return $q.when(input);
                })
                .converter(defaultListConverter, function(preFetches) {
                    return {
                        columns: columns,
                        entity: preFetches.folder
                    };
                });
        },
        shares: function(columns, folderId) {
            return http.get(sqlQueries('folder_shares')).params({
                folder_id: folderId
            }).order('natural_order', 'asc').converter(defaultListConverter, {
                columns: columns
            });
        },
        usersRatio: function (folderId) {
            return http.get(sqlQueries('folder_permissions')).params({
                folder_id: folderId
            }).converter(countRowsConverter, {
                field: 'is_external_user'
            });
        },
        content: function(folderId, withFiles) {
            return http.get(sqlQueries('folder_content')).params({
                entity_id: folderId || '',
                with_files: withFiles
            }).preFetch(mimeIcons(), 'mimeIcons')
                .converter(treeContentConverter, function(preFetches) {
                    return preFetches;
                });
        },
        tableContent: function(columns, folderId) {
            return http.get(sqlQueries('folder_content_browse')).params({
                entity_id: folderId
            }).preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function(preFetches) {
                    return  {
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns,
                        converters: {
                            browserItemConverter: function(text, columnId, columnsMap, args) {
                                var linkIconConverters = {
                                    "box": function (columnsMap) {
                                        if (columnsMap.link_access_scope && columnsMap.link_access_permissions) {
                                            return '#' + JSON.stringify({
                                                    display: 'text',
                                                    'class': 'fa fa-link',
                                                    tooltip: 'Shared'
                                                });
                                        } else {
                                            return '';
                                        }
                                    },
                                    "google_drive": function (columnsMap) {
                                        if (columnsMap.link_sharing_scope != 'off') {
                                            return  '#' + JSON.stringify({
                                                    display: 'img',
                                                    'class': 'sharing-icon',
                                                    path: path('profiles').img('google_drive/permissions/' + columnsMap.link_sharing_scope + '.svg'),
                                                    defaultPath: path('profiles').img('share.png'),
                                                    tooltip: 'Sharing Link Scope: ' + {
                                                        "anyone_with_no_link": "Public on the web",
                                                        "anyone_with_link": "Anyone with a link",
                                                        "domain_with_no_link": "All people in the domain",
                                                        "domain_with_link": "All people in the domain with a link"

                                                    }[columnsMap.link_sharing_scope]
                                                });
                                        } else {
                                            return '';
                                        }
                                    },
                                    "egnyte": function (columnsMap) {
                                        if (columnsMap.have_link) {
                                            return '#' + JSON.stringify({
                                                    display: 'text',
                                                    'class': 'fa fa-link',
                                                    tooltip: 'Shared with link'
                                                });
                                        } else {
                                            return '';
                                        }
                                    }
                                };


                                var firstLine = '';
                                var secondLine = '';

                                if (args.ownerId && columnsMap[args.ownerId]) {
                                    firstLine += '#' + JSON.stringify({
                                            display: 'link',
                                            type: 'user',
                                            id: columnsMap.owner_id,
                                            text: 'Owned by ' + columnsMap.owner_name,
                                            'class': 'item-owner'
                                        });
                                }
                                firstLine += '#' + JSON.stringify({
                                        display: 'link',
                                        type: columnsMap.entity_type,
                                        id: columnsMap.entity_id,
                                        text: text,
                                        'class': 'item-title'
                                    });
                                if (args.isDeleted && columnsMap[args.isDeleted]) {
                                    firstLine += '#' + JSON.stringify({
                                            display: 'text',
                                            text: 'deleted',
                                            'class': 'item-deleted'
                                        });
                                }

                                var updatedText = 'Updated';
                                var updater = '';

                                if (columnsMap[args.updatedDate]) {
                                    if (args.creationEquality) {
                                        var creationNotEqual = _(args.creationEquality).find(function (key, value) {
                                            return columnsMap[key] !== columnsMap[value];
                                        });
                                        if (!creationNotEqual) {
                                            updatedText = (args.createdTextByEntityType && args.createdTextByEntityType[columnsMap.entity_type]) || 'Created';
                                        }
                                    }

                                    updatedText += ' ' + $filter('localTime')(columnsMap[args.updatedDate], 'MMM D, YYYY');

                                    if (args.modifierId && columnsMap[args.modifierId]) {
                                        updatedText += ' by ';
                                        updater = '#' + JSON.stringify({
                                                display: 'link',
                                                type: 'user',
                                                id: columnsMap[args.modifierId],
                                                text: columnsMap[args.modifierName]
                                            });
                                    }
                                    secondLine += '#' + JSON.stringify({
                                            display: 'text',
                                            text: updatedText
                                        });
                                    secondLine += updater;
                                }



                                if (args.size && (!args.sizeIf || (args.sizeIf && columnsMap[args.sizeIf]))) {
                                    secondLine += '#' + JSON.stringify({
                                            display: 'text',
                                            text: $filter('bytes')(columnsMap[args.size] || 0, 1, true)
                                        });
                                    if (args.quota) {
                                        secondLine += '#' + JSON.stringify({
                                                display: 'text',
                                                text: '(Quota used: ' + $filter('bytes')(columnsMap[args.quota] || 0, 1, true) + ')'
                                            });
                                    }
                                }

                                if (args.linkIcon && linkIconConverters[args.linkIcon]) {
                                    if (args.linkOnFirstLine) {
                                        firstLine += linkIconConverters[args.linkIcon](columnsMap);
                                    } else {
                                        secondLine += linkIconConverters[args.linkIcon](columnsMap);
                                    }
                                }

                                var result = '#' + JSON.stringify({
                                        display: 'linked-text',
                                        'class': 'display-block',
                                        text: firstLine
                                    }) + '#' + JSON.stringify({
                                        display: 'linked-text',
                                        'class': 'display-block',
                                        text: secondLine
                                    });

                                if (args.description && columnsMap[args.description]) {
                                    result += '#' + JSON.stringify({
                                            display: 'text',
                                            'class': 'item-description',
                                            text: columnsMap[args.description]
                                        });
                                }

                                return result;

                            }                            
                        }
                    };
                });
        }

    };
    return folderDao;
});