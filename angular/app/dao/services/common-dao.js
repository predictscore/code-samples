var m = angular.module('dao');

m.factory('commonDao', function(http, $q, $http) {
    return {
        entityIdentifier: function(entities, entityType) {
            if(!entities.length) {
                return $q.when([]);
            }
            return http.put('/api/v1/entity_identifier').body(_(entities).map(function(entity) {
                return {
                    entity_id: entity,
                    entity_type: entityType
                };
            })).converter(function(response) {
                return $q.when(_(response.data).map(function(entity) {
                    return {
                        id: entity.entity_id,
                        label: entity.main_identifier_value
                    };
                }));
            });
        },
        entityIdentifierRaw: function(entities) {
            if(!entities.length) {
                return $q.when([]);
            }
            return http.put('/api/v1/entity_identifier').body(entities);
        },
        entityIdentifierMulti: function(entities) {
            if(!entities.length) {
                return $q.when({});
            }
            var unique = {};
            _(entities).each(function (entity) {
                unique[entity.entity_type] = unique[entity.entity_type] || {};
                unique[entity.entity_type][entity.entity_id] = entity;
            });
            return http.put('/api/v1/entity_identifier')
                .body(_(unique).chain().map(function (data) {
                    return _(data).values();
                }).flatten().value()
                ).converter(function (response) {
                    var resultMap = {};
                    _(response.data).each(function (entity) {
                        resultMap[entity.entity_type] = resultMap[entity.entity_type] || {};
                        resultMap[entity.entity_type][entity.entity_id] = entity;
                    });
                    return $q.when(resultMap);
                });
        },
        uploadCustomIcon: function (data) {
            var fd = new FormData();
            fd.append('file', data);
            return $http.post('/api/v1/custom_icon', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            });
        },
        removeCustomIcon: function (data) {
            return http.delete('/api/v1/custom_icon').run();
        },
        getTermsAccepted: function () {
            return http.get('/api/v1/terms_accepted');
        },
        acceptTerms: function () {
            return http.post('/api/v1/terms_accepted').run();
        },
        genericLookup: function (endpoint, params) {
            return http.get(endpoint).params(params)
                .onRequestFail(function () {
                    return $q.when({
                        data: {
                            rows : []
                        }
                    });
                });
        },
        callCustomUrl: function (method, url) {
            return http[method.toLowerCase()](url).run();
        },
        hardReset: function () {
            return http.post('/api/v1/hard_reset').run().then(function (response) {
                if (!response.data || !response.data.success) {
                    return $q.reject(response);
                }
            });
        },
        genericVerify: function (url, value) {
            return http.get(url.replace('{{value}}', value));
        },
        customStateCheck: function (url) {
            return http.get(url).onRequestFail(function () {
                return $q.when({
                    data: false
                });
            }).converter(function (input) {
                var state = true;
                if (input.data === false || input.data === 'no') {
                    state = false;
                } else {
                    if (input.data.rows && !input.data.rows.length) {
                        state = false;
                    }
                }
                return $q.when({
                    state: state
                });
            });
        }
    };
});