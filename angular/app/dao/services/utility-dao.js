var m = angular.module('dao');

m.factory('utilityDao', function(http, $q) {
    return {
        sendSupportMessage: function (params) {
            return http.post('/api/v1/utility/send_mail').body($.extend(params, {
                to_email_type: 'support_email'
            })).run();
        }
    };
});