var m = angular.module('dao');

m.factory('policyDao', function ($q, $http, http, defaultListConverter, policyPrimaryFindingsConverter, policyDiscoveryConverter, path, rowsToObjectConverter, policyDiscoveriesConverter, sqlQueries, policyFindingsConverter, dao, modulesManager, singleRowConverter, rowsNameConverter, policyConverter, policySaveConverter, predefinedPolicyConverter, feature, policyFindingsV2Converter, infoToPolicyConverter, policyTemplatesConverter, objectTypeDao, policyInfoBoxesConverter, dataToRowsConverter, policySeverityTypes, timeOffsetHelper, timeVariants, scanStatusConverter, infoboxWithFindingsConverter) {
    var url = '/api/v1/policies/policy_catalog';
    var policyDao = {
        policyCatalog: function (saas, hidePaused, hideHidden, hideTemporary) {
            return http.get(sqlQueries('policy_catalog')).params({
                saas: saas,
                hide_paused: hidePaused,
                hide_hidden: hideHidden,
                hide_temporary: hideTemporary
            });
        },
        list: function (columns, sourcesInfo, actionLabels, hidePaused, hideHidden, hideTemporary) {
            var activeSaass = _(modulesManager.cloudApps()).map(function (app) {
                return app.name;
            }).join(',');
            return policyDao.policyCatalog(activeSaass, hidePaused, hideHidden, hideTemporary)
                //.preFetch(policyDao.discovery(7, undefined, hidePaused), 'discovery')
                //.preFetch(policyDao.findingsStatus(), 'findingsStatus')
                .converter(function (input) {
                    var rows = input.data.rows || [];
                    var q = $q.when({
                        data: {}
                    });
                    if (rows.length > 0) {
                        q = policyDao.findingsStatus(_(rows).pluck('policy_id'));
                    }
                    return q.then(function (response) {
                        return $.extend(true, input, {
                            data: {
                                findingsStatus: response.data
                            }
                        });
                    });
                })
                .dynamicConverter(function (preFetches) {
                    return function (input, args) {
                        //var discovery = preFetches.discovery.data;
                        var discovery = [];
                        var findingStatuses = input.data.findingsStatus;
                        var rows = input.data.rows || [];

                        _(rows).each(function (row) {
                            var findingStatus = findingStatuses[row.policy_id] || {};
                            row.entities_matches = findingStatus.Match || 0;
                            row.entities_unmatches = findingStatus.Unmatch || 0;
                            row.entities_pending = findingStatus.Pending || 0;
                            row.entities_total = findingStatus.total || 0;
                            row.last_match_time = (_(discovery).find(function (p) {
                                return p.id == row.policy_id;
                            }) || {}).last_match_time;
                        });

                        return defaultListConverter(input, {
                            http: args.http,
                            columns: columns,
                            converters: {
                                policyStatus: function (text, columnId, columnsMap) {
                                    var disableReason = columnsMap['policy_data.disableReason'];
                                    if (disableReason) {
                                        disableReason = disableReason.replace(/\n/g, '<br>');
                                    }
                                    return {
                                        "running": '#' + JSON.stringify({
                                            display: 'text',
                                            'class': 'fa fa-play'
                                        }) + ' Running',
                                        "pause": '#' + JSON.stringify({
                                            display: 'text',
                                            'class': 'fa fa-pause'
                                        }) + ' Paused',
                                        "disabled": '#' + JSON.stringify({
                                            display: 'text',
                                            'class': 'fa fa-ban',
                                            tooltip: disableReason
                                        }) + ' Disabled',
                                        "mark_for_deletion": "Marked for deletion"
                                    }[text];
                                },
                                policyName: function (text, columnId, columnsMap) {
                                    if(text.length === 0) {
                                        text = 'Policy #' + columnsMap.policy_id;
                                    }
                                    var disableReason = columnsMap['policy_data.disableReason'];
                                    var result = '';
                                    if(columnsMap.temporary) {
                                        result += '#' + JSON.stringify({
                                            display: 'text',
                                            'class': 'fa fa-exclamation-triangle red-text',
                                            tooltip: 'Temporary policy'
                                        });
                                    }
                                    if(!disableReason) {
                                        result += '#' + JSON.stringify({
                                            display: 'link',
                                            'class': 'policy-name display-inline-block',
                                            type: 'policy-edit',
                                            id: columnsMap.policy_id,
                                            text: text
                                        });
                                    } else {
                                        result += '#' + JSON.stringify({
                                            display: 'text',
                                            text: text
                                        }) + '#' + JSON.stringify({
                                            display: 'text',
                                            'class': 'fa fa-info-circle',
                                            tooltip: disableReason.replace(/\n/g, '<br>')
                                        });
                                    }
                                    result += '#' + JSON.stringify({
                                        display: 'text',
                                        'class': 'policy-description',
                                        text: columnsMap['policy_data.description']
                                    });
                                    return result;
                                },
                                entityType: function (text) {
                                    if (_(sourcesInfo[text]).isUndefined()) {
                                        return 'Unknown';
                                    }
                                    return sourcesInfo[text].label;
                                },
                                entityTypeImage: function (text) {
                                    return '#' + JSON.stringify({
                                            display: 'img',
                                            'class': 'entity-type-image',
                                            path: modulesManager.cloudApp(text).img
                                        });
                                },
                                lastMatchConverter: function (text) {
                                    if (moment().add(-1, 'days').isBefore(text)) {
                                        return '#' + JSON.stringify({
                                                display: 'text',
                                                'class': 'new-event',
                                                text: "New Event"
                                            }) + text;
                                    } else {
                                        return text;
                                    }
                                },
                                trendConverter: function (text, columnId, columnsMap, args) {
                                    var discoveries = _(discovery).find(function (p) {
                                        return p.id == text;
                                    });
                                    if (_(discoveries).isUndefined()) {
                                        return '';
                                    }
                                    return '#' + JSON.stringify({
                                            display: 'flot',
                                            'class': 'findings-flot',
                                            flot: {
                                                data: [discoveries.data],
                                                flotOptions: args.flotOptions
                                            }
                                        });
                                },
                                severityConverter: function(text) {
                                    return '#' + JSON.stringify({
                                            display: 'text',
                                            'class': 'alerts-tag ' + policySeverityTypes.idFixed(text) + '-policy-severity-tag',
                                            text: policySeverityTypes.idToLabel(text)
                                        });
                                },
                                appIconListConverter: function (text, columnId, columnsMap, args) {
                                    if(!_(text).isArray() || text.length === 0) {
                                        return '';
                                    }
                                    if (text.length === 1) {
                                        var icon = modulesManager.labelToIcon(text[0]);
                                        if (_(icon).isUndefined()) {
                                            return '#' + JSON.stringify({
                                                    display: 'text',
                                                    text: text[0]
                                                });
                                        } else {
                                            return icon;
                                        }
                                    }
                                    return '#' + JSON.stringify({
                                            display: 'inline-list',
                                            'class': 'medium-width',
                                            data: text,
                                            countText: text.length + ' ' + args.countPostfix
                                        });
                                },
                                standardActionLablesConverter: function (text, columnId, columnsMap, args) {
                                    if(!_(text).isArray() || text.length === 0) {
                                        return '';
                                    }
                                    text = _(text).map(function (action) {
                                        return actionLabels[action.name];
                                    });
                                    var countText;
                                    if (args.collapseRest) {
                                        countText = (text.length - (args.expanded || 1)) + ' more';
                                    } else {
                                        countText = text.length + ' ' + args.countPostfix;
                                    }
                                    return '#' + JSON.stringify({
                                            display: 'inline-list',
                                            'class': 'medium-width',
                                            data: text,
                                            countText: countText,
                                            expanded: args.expanded,
                                            collapseRest: args.collapseRest
                                        });
                                },
                                scheduledExportConverter: function(text, columnId, columnsMap) {
                                    var editButton = '#' + JSON.stringify({
                                        display: 'text',
                                        'class': 'fa fa-pencil dark-blue-text cursor-pointer',
                                        tooltip: 'Edit scheduled export',
                                        clickEvent: {
                                            name: 'edit-scheduled-export',
                                            policyId: columnsMap.policy_id
                                        }
                                    });
                                    var removeButton = '#' + JSON.stringify({
                                        display: 'text',
                                        'class': 'fa fa-times red-text cursor-pointer',
                                        tooltip: 'Remove scheduled export',
                                        clickEvent: {
                                            name: 'remove-scheduled-export',
                                            policyId: columnsMap.policy_id
                                        }
                                    });
                                    function bold(text) {
                                        return '#' + JSON.stringify({
                                            display: 'text',
                                            text: text,
                                            'class': 'bold-text'
                                        });
                                    }
                                    var result = 'None';
                                    if(text && text.frequency != 'none') {
                                        var weekDay = text.frequency == 'daily' ? 'day' : bold(timeVariants.weekDays.getLabelById(text.weeklyDay));
                                        var time = bold(timeOffsetHelper.offsetToLocal(text.timeSent));
                                        result = 'Every ' + weekDay + ' at ' + time;
                                        if(text.frequency == 'monthly') {
                                            result = 'Monthly on the ' + bold(timeVariants.monthWeeks.getLabelById(text.monthlyWeek).toLowerCase())+ ' ' + weekDay + ' at ' + time;
                                        } 
                                        
                                        if(!columnsMap.system) {
                                            result += editButton;
                                            result += removeButton;
                                        }
                                    } else {
                                        if(!columnsMap.system) {
                                            result += editButton;
                                        }
                                    }
                                    return result;
                                }
                            }
                        });
                    };
                });
        },
        listFacets: function() {
            return http.get(sqlQueries('policy_catalog_facets')).converter(singleRowConverter);  
        },
        retrieve: function (id, propertiesPromise) {
            propertiesPromise = propertiesPromise || objectTypeDao.retrieve(false);
            return http.get('/api/v1/policy/' + id + '/list/info')
                .preFetch(propertiesPromise, 'objectTypes')
                .converter(infoToPolicyConverter)
                .converter(policyConverter, function(preFetches) {
                    return preFetches;
                });
        },
        retrieveByInternalName: function(internalName) {
            return policyDao.policyCatalog().params({
                internal_name: internalName
            }).converter(singleRowConverter)
                .converter(policyConverter);
        },
        create: function (data) {
            return http.post(url).body(data).bodyConverter(policySaveConverter).run();
        },
        createRaw: function (data) {
            return http.post(url).body(data).run();
        },
        update: function (id, data) {
            return http.put(url + '/' + id).body(data).bodyConverter(policySaveConverter).run();
        },
        updateRaw: function (id, data) {
            return http.put(url + '/' + id).body(data).run();
        },
        recheck: function(id) {
            return http.post(url + '/' + id + '/recheck').run();
        },
        updateExclude: function (id, added, removed) {
            var promises = [];
            if (_(added).isArray() && added.length > 0) {
                promises.push($http.put(url + '/' + id + '/exclude', {
                    entity_id: added,
                    action: 'Add'
                }));
            }
            if (_(removed).isArray() && removed.length > 0) {
                promises.push($http.put(url + '/' + id + '/exclude', {
                    entity_id: removed,
                    action: 'Remove'
                }));
            }
            return $q.all(promises);
        },
        discovery: function (after, saas, hidePaused) {
            after--;
            return http.get(sqlQueries('policy_discovers')).params({
                after: after,
                saas: saas
            }).preFetch(policyDao.policyCatalog(saas, hidePaused), 'policyCatalog')
                .preFetch(policyDao.findingsStatus(), 'findingsStatus')
                .converter(policyDiscoveriesConverter, function (preFetches) {
                    return {
                        after: after,
                        policyCatalog: preFetches.policyCatalog,
                        findingsStatus: preFetches.findingsStatus
                    };
                });
        },
        findingsStatus: function (policyId, primaryEntities) {
            if(primaryEntities) {
                return http.get(sqlQueries('policy_primary_findings_status')).params({
                    policy_id: policyId
                }).converter(policyPrimaryFindingsConverter, {
                    policyId: policyId
                });
            }
            if(feature.newPolicyApi) {
                if (_(policyId).isUndefined()) {
                    policyId = 'all';
                }
                var policiesList;
                if (_(policyId).isArray()) {
                    policiesList = _(policyId).compact();
                    if (policiesList.length < 1) {
                        return $q.when({
                            data: {}
                        });
                    }
                    policiesList = policiesList.join(',');
                    policyId = 'some';
                }
                var urlV2 = '/api/v1/policy/' + policyId + '/count';
                return http.get(urlV2).params({
                    policies: policiesList
                }).converter(policyFindingsV2Converter, {
                    policyId: policyId
                });
            } else {
                var url = '/api/v1/policies/dynamic_policy_status_current_report';
                if (!_(policyId).isUndefined()) {
                    url += '/' + policyId;
                }
                return http.get(url).converter(policyFindingsConverter, {
                    policyId: policyId
                });
            }
        },
        getTemplatesList: function (columns, sourcesInfo) {
            return policyDao.getTemplates()
                .converter(rowsNameConverter, {
                    name: 'templates'
                })
                .converter(policyTemplatesConverter, {disableFilter: true})
                .converter(defaultListConverter, {
                    columns: columns,
                    converters: {
                        entityType: function (text) {
                            if (_(sourcesInfo[text]).isUndefined()) {
                                return 'Unknown';
                            }
                            return sourcesInfo[text].label;
                        }
                    }
                });
        },
        getTemplates: function() {
            return http.get('/api/v1/policies/policy_templates');
        },
        predefinedPolicy: function(saas, security) {
            return http.get('/api/v1/apps/policy_matrix/' + saas + '/' + security).converter(predefinedPolicyConverter);
        },
        modulesMatrix: function(policyCounts, showCustom) {
            if (feature.newSecurityMatrix) {
                return http.get('/api/v1/apps/matrix1').params({
                    policy_counts: policyCounts,
                    custom: showCustom
                });
            } else {
                return http.get('/api/v1/apps/matrix');
            }
        },
        policyMatrixOperation: function (saas, app, operation) {
            return http.post('/api/v1/apps/matrix1/' + saas + '/' + app + '/' + operation).run();
        },
        changeStatus: function (policyId, status) {
            return policyDao.changeBatchStatus([policyId], status);
        },
        changeBatchStatus: function(policyIds, status) {
            return http.put('/api/v1/policies/policy_catalog/operation').body({
                policies_ids: policyIds,
                operator: status
            }).run();
        },
        ping: function(policyId) {
            return http.get('/api/v1/policy/' + policyId + '/keep_alive').run();
        },
        infoBox: function (id) {
            var saas = modulesManager.currentModule();
            return http.get('/api/v1/infobox/' + id + '/' + saas);
        },
        infoBoxes: function () {
            return http.get('/api/v1/infobox');
        },
        infoBoxesFull: function () {
            return http.get(sqlQueries('policy_infoboxes'))
                .params({saas: modulesManager.currentModule()})
                .preFetch(policyDao.policyCatalog(modulesManager.currentModuleCatalog(), false, false), 'policyCatalog')
                .converter(policyInfoBoxesConverter, _.identity);
        },
        infoBoxSave: function (id, policyId, saas, configuration) {
            if (!saas) {
                saas = modulesManager.currentModule();
            }
            return http.post('/api/v1/infobox/' + id + '/' + saas).body({
                policy_id: policyId,
                configuration: configuration
            }).run();
        },
        infoBoxReset: function (id, saas) {
            return http.put('/api/v1/infobox/default/' + id + '/' + saas).run();
        },
        findingsHistory: function (policyId, period) {
            return http.get(sqlQueries('policy_findings_history'))
                .params({
                    policy_id: policyId,
                    period: period
                })
                .converter(policyFindingsV2Converter, {
                    policyId: policyId
                });
        },
        infoboxCatalog: function () {
            return policyDao.policyCatalog(modulesManager.currentModuleCatalog(), false, false)
                .converter(function (response) {
                    response.data.rows = _(response.data.rows).chain().filter(function (row) {
                        return row.policy_name && !row.temporary && row.policy_status !== 'disabled';
                    }).each(function (row) {
                        row.policy_data.name = row.policy_name;
                        row.policy_data.tags = row.tags;
                        row.policy_data.severity = row.severity;
                    }).value();
                    return $q.when(response);
                });
        },
        infoboxCatalogWithFindings: function () {
            return policyDao.infoboxCatalog()
                .preFetch(policyDao.findingsStatus(), 'findingsStatus')
                .converter(infoboxWithFindingsConverter, _.identity);
        },
        allSaasFilesFindings: function (saas) {
            var policies;
            var policyId;
            var converterArgs = {policyId: 'all'};
            return http.get(function (preFetches) {
                policies = _(preFetches.policiesCatalog.data.rows).chain().filter(function (policy) {
                    return _(policy.tags).contains('files_all');
                }).pluck('policy_id').value();
                if (policies.length === 0) {
                    policyId = 'all';
                } else {
                    policyId = policies[0];
                }
                converterArgs.policyId = policyId;
                return '/api/v1/policy/' + policyId + '/count';
            }).preFetch(policyDao.policyCatalog(saas, false, false, true), 'policiesCatalog')
                .converter(policyFindingsV2Converter, converterArgs)
                .converter(function (input) {
                    var data = {
                        total: 0
                    };
                    if (policies.length > 0) {
                        data.policyId = policies[0];
                        if (input.data[data.policyId]) {
                            data.total += input.data[data.policyId].Match;
                        }
                    }
                    return $q.when({
                        data: data
                    });
                });
        },
        scanStatus: function() {
            return http.get('/api/v1/matrix_policy_status').converter(scanStatusConverter);
        },
        graph: function(policyId, keyPath, valuePath, top) {
            return http.post('/api/v1/policy/' + policyId + '/graph').body({
                key_path: keyPath,
                value_path: valuePath,
                top: top
            }).converter(function(input) {
                return $q.when(input.data.rows);
            });
        },
        refreshCountSome: function (policies) {
            return http.post('/api/v1/policy/some/count/refresh').body({
                policies: policies
            }).run();
        },
        rulesRunningCount: function (hidePaused, hideHidden, hideTemporary) {
            var activeSaass = _(modulesManager.cloudApps()).map(function (app) {
                return app.name;
            }).join(',');
            return policyDao.policyCatalog(activeSaass, hidePaused, hideHidden, hideTemporary).params({
                queryCount: 1,
                filterBy_policy_status: 'running'
            });
        },
        rules: function (columns, sourcesInfo, actionLabels, hidePaused, hideHidden, hideTemporary) {
            var activeSaass = _(modulesManager.cloudApps()).map(function (app) {
                return app.name;
            }).join(',');
            return policyDao.policyCatalog(activeSaass, hidePaused, hideHidden, hideTemporary)
                .converter(defaultListConverter, {
                    columns: columns,
                    converters: {
                        policyStatus: function (text, columnId, columnsMap) {
                            var disableReason = columnsMap['policy_data.disableReason'];
                            if (disableReason) {
                                disableReason = disableReason.replace(/\n/g, '<br>');
                            }
                            return '#' + JSON.stringify({
                                    "display": "text",
                                    "class": "policy-status status-" + text,
                                    "text": {
                                        "running": "Running",
                                        "pause": "Stopped",
                                        "disabled": "Disabled",
                                        "mark_for_deletion": "Marked for deletion"
                                    }[text]
                                });
                        },
                        saasConverter: function (text) {
                            return '#' + JSON.stringify({
                                    display: 'img',
                                    'class': 'entity-type-image',
                                    path: modulesManager.cloudApp(text).img
                                }) + modulesManager.cloudApp(text).title;
                        },
                        standardActionLablesConverter: function (text, columnId, columnsMap, args) {
                            if(!_(text).isArray() || text.length === 0) {
                                return '';
                            }
                            text = _(text).map(function (action) {
                                return actionLabels[action.name];
                            });
                            var countText;
                            if (args.collapseRest) {
                                countText = (text.length - (args.expanded || 1)) + ' more';
                            } else {
                                countText = text.length + ' ' + args.countPostfix;
                            }
                            return '#' + JSON.stringify({
                                    display: 'inline-list',
                                    'class': 'medium-width',
                                    data: text,
                                    countText: countText,
                                    expanded: args.expanded,
                                    collapseRest: args.collapseRest
                                });
                        }
                    }
                });
        },
        scopes: function (saas) {
            return http.get(sqlQueries('policy_scopes', null, saas));
        }

    };
    return policyDao;
});