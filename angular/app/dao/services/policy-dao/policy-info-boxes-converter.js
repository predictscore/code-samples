var m = angular.module('dao');

m.factory('policyInfoBoxesConverter', function($q) {
    return function (input, args) {

        var data = {
            policyCatalog: args.policyCatalog.data.rows,
            settings: {}
        };

        function convertFindings(findings) {
            return {
                Match: findings.match_count,
                Unmatch: findings.unmatch_count,
                Pending: findings.pending_count,
                Allow: findings.allow_count,
                Irresolvable: findings.irresolvable_count,
                tested: findings.match_count + findings.unmatch_count + findings.allow_count,
                total: findings.pending_count + findings.match_count + findings.unmatch_count + findings.allow_count
            };
        }

        _(input.data.rows).each(function (row) {
            if (!data.settings[row.id]) {
                data.settings[row.id] = {
                    options: {
                        id: row.id,
                        policy_id: row.policy_id,
                        saas: row.saas,
                        configuration: row.configuration,
                        conf: row.conf
                    },
                    findings: {},
                    history: {},
                    policies: [],
                    rightData: {
                        findings: {},
                        policies: []
                    }
                };
            }
            if (row.found_policy_id) {
                var policyObj = {
                    policy_id: row.found_policy_id,
                    policy_name: row.policy_name,
                    findings: row.findings && convertFindings(row.findings),
                    history: row.history && convertFindings(row.history)
                };
                if (row.pos === '2') {
                    data.settings[row.id].rightData.policies.push(policyObj);
                } else {
                    data.settings[row.id].policies.push(policyObj);
                }
            }
            if (row.findings) {
                _(convertFindings(row.findings)).each(function (val, key) {
                    if (row.pos === '2') {
                        data.settings[row.id].rightData.findings[key] = val + (data.settings[row.id].rightData.findings[key] || 0);
                    } else {
                        data.settings[row.id].findings[key] = val + (data.settings[row.id].findings[key] || 0);
                    }
                });
            }
            if (row.history) {
                _(convertFindings(row.history)).each(function (val, key) {
                    data.settings[row.id].history[key] = val + (data.settings[row.id].history[key] || 0);
                });
            }
        });

        return $q.when($.extend({}, input, {
            data: data
        }));
    };
});