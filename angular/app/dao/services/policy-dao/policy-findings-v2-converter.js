var m = angular.module('dao');

m.factory('policyFindingsV2Converter', function($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var data = {};

        var rows = input.data.rows || input.data;
        if(args.policyId != 'all' && args.policyId !== 'some') {
            rows = _([[args.policyId, _(rows).isArray() ? rows[0] || {} : rows]]).object();
        }

        _(rows).each(function(row, policyId) {
            data[policyId] = {
                Match: row.match_count,
                Unmatch: row.unmatch_count,
                Pending: row.pending_count,
                Allow: row.allow_count,
                Irresorvable: row.irresolvable_count,
                tested: row.tested,
                total: row.total,
                isCalculating: row.show_calculating
            };
        });

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    };
});