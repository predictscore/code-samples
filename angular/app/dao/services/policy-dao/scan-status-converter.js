var m = angular.module('dao');

m.factory('scanStatusConverter', function($q) {
    return function (input, args) {
        return $q.when(_(input.data).map(function(entityData, entity) {
            var firstApp = _(entityData.sec_apps).chain().values().filter(function(appData) {
                return !_(appData.period_count).isUndefined();
            }).first().value() || {};
            return {
                name: entity,
                status: entityData.status,
                totalCount: entityData.total_count,
                periodCount: firstApp.period_count || 0,
                poc: entityData.poc_percent,
                daysCount: firstApp.days_count,
                secApps: _(entityData.sec_apps).map(function(appData, app) {
                    return {
                        name: app,
                        status: appData.matrix_policy_off ? 'matrix_policy_off' : appData.status,
                        poc: entityData.poc_percent,
                        scanned: appData.scanned_percent,
                        daysCount: appData.days_count,
                        pendingCount: appData.pending_count,
                        periodCount: appData.period_count,
                    };
                })
            };
        }));
    };
});