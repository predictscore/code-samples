var m = angular.module('dao');

m.factory('policyTemplatesConverter', function($q, modulesManager) {
    return function(input, args) {
        var module = modulesManager.currentModule();
        input.data.rows = _(input.data.rows).chain().filter(function(row) {
            return row.type !== 'entity_sectool' &&
                (args.disableFilter || (args.saas && row.policy.policy_desc.saas == args.saas) || (!args.saas && row.policy.policy_desc.saas == module));
        }).sortBy(function(row) {
            var order = row.type == 'single_entity' ? '0' : '1';
            return order + '-' + row.template_name;
        }).value();

        return $q.when(input);
    };
});