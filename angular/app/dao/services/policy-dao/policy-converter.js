var m = angular.module('dao');

m.factory('policyConverter', function($q, actionTagsProcessor, policyPropertyParser, feature, policyHeaderOptions, policyFilterTypes, policyPropertyPathToId, $filter, policyConditionsNormalizer, logging) {
    return function (input, args) {
        var deferred = $q.defer();

        if(!_(input.data.context.template).isUndefined()) {
            $.extend(input.data.policy_data, input.data.context.template);
        }

        input.data.old_policy_data = $.extend(true, {}, input.data.policy_data);
        
        if(input.data.edit_delta) {
            _(input.data.edit_delta.columnsToShowRemoved).each(function(columnId) {
                delete input.data.policy_data.columnsToShow[columnId];
            });
            var maxOrder = 0;
            _(input.data.policy_data.columnsToShow).each(function(column) {
                maxOrder = Math.max(maxOrder, column.order);
            });
            _(input.data.edit_delta.columnsToShowAdded).each(function(value, key) {
                input.data.policy_data.columnsToShow[key] = $.extend({
                    order: ++maxOrder
                }, value);
            });
            $.extend(input.data.policy_data.columnNames, input.data.edit_delta.columnNames);
            input.data.policy_data.columnsOrderOverride = input.data.edit_delta.columnsOrderOverride;
        }

        _(input.data.policy_data.actions).each(function(action) {
            actionTagsProcessor.replaceTags(action);
            delete action.tags;
        });

        input.data.policy_data.columnNames = input.data.policy_data.columnNames || {};
        input.data.policy_data.entityType = input.data.entity_type;
        input.data.policy_data.name = input.data.policy_name;
        input.data.policy_data.description = input.data.policy_description;

        delete input.data.entity_type;
        delete input.data.policy_name;
        delete input.data.policy_description;
        
        input.data.policy_data.tags = input.data.tags;
        input.data.policy_data.severity = input.data.severity;
        input.data.policy_data.alert = input.data.alert;
        input.data.policy_data.not_run_backwards = !input.data.policy_data.run_backwards;
        
        delete input.data.tags;
        delete input.data.severity;
        delete input.data.alert;
        delete input.data.policy_data.run_backwards;

        input.data.policy_data.isPolicy = (input.data.mode === 'policy');

        delete input.data.type;

        function getNodeInfo(property) {
            var nodeId = policyPropertyParser(property).nodeId;
            var nodeInfo = args.objectTypes.data.allSources[nodeId];
            return nodeInfo || {};
        }

        function processCondition(condition) {
            if(_(condition).isUndefined()) {
                return;
            }
            if(_(condition.conditions).isUndefined()) {
                var dataSplit;
                if(condition.data_is_property) {
                    condition.data = policyPropertyParser(condition.data.property);
                    condition.options = condition.options || {};
                    condition.options.data_is_property = true;
                }
                if(!_(condition.data_is_property).isUndefined()) {
                    delete condition.data_is_property;
                }
                if (_(['between', 'ip_between']).contains(condition.operator)) {
                    dataSplit = condition.data.split('~');
                    condition.data = {
                        first: _(dataSplit).first(),
                        second: _(dataSplit).last()
                    };
                }
                if(_(['date_after', 'date_before']).contains(condition.operator)) {
                    condition.data = $filter('localTime')(condition.data);
                }
                if('date_between' == condition.operator) {
                    dataSplit = _(condition.data.split('~')).map(function(d) {
                        return parseInt(d);
                    });
                    condition.data = {
                        first: $filter('localTime')(_(dataSplit).first()),
                        second: $filter('localTime')(_(dataSplit).last())
                    };
                }
                if('is' == condition.operator) {
                    if(getNodeInfo(condition.property).type == 'enum') {
                        condition.data = [condition.data];
                        condition.operator = 'in';
                    }
                }
                if('is_null' == condition.operator) {
                    if(getNodeInfo(condition.property).type == 'boolean') {
                        condition.data = null;
                        condition.operator = 'bool';
                    }
                }
            } else {
                _(condition.conditions).each(function(subCond) {
                    processCondition(subCond);
                });
            }
        }
        
        var includeCondition = input.data.policy_data.includeCondition;
        if(includeCondition.operator == 'OR') {
            if(includeCondition.conditions.length <= 1) {
                includeCondition.operator = 'AND';
            } else {
                input.data.policy_data.includeCondition = {
                    not: false,
                    operator: "AND",
                    type: "group",
                    conditions: includeCondition
                };
            }
        }
        
        processCondition(input.data.policy_data.includeCondition);
        if(_(args.objectTypes.data.sources(input.data.policy_data.entityType)).isUndefined()) {
            logging.log('condition_ui API do not contains info about ' + input.data.policy_data.entityType + ' entity type');
            logging.log('Only next entity types available: ' + _(args.objectTypes.data.sourcesInfo).keys().join(', '));
        }
        var normalized = policyConditionsNormalizer(args.objectTypes.data.sources(input.data.policy_data.entityType).properties, input.data.policy_data.includeCondition);
        if(normalized) {
            input.data.policy_data.includeCondition = normalized;
            input.data.policy_data.advancedConditions = false;
        } else {
            input.data.policy_data.advancedConditions = (normalized === false ? 'not_parsable' : true);
        }

        deferred.resolve(input);
        return deferred.promise;
    };
});