var m = angular.module('dao');

m.factory('policyPrimaryFindingsConverter', function($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var defaultStats = {
            Match: 0,
            Unmatch: 0,
            Pending: 0,
            Excluded: 0
        };

        var data = {};

        if(!_(args.policyId).isUndefined()) {
            data[args.policyId] = $.extend({}, defaultStats);
        }

        _(input.data.rows).each(function(row) {
            var policy = data[row.policy_id] || $.extend({}, defaultStats);
            policy[row.status] = row.count;
            data[row.policy_id] = policy;
        });

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    };
});