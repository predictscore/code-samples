var m = angular.module('dao');

m.factory('infoToPolicyConverter', function($q) {
    return function (input, args) {
        return $q.when({
            data: input.data.policy
        });
    };
});