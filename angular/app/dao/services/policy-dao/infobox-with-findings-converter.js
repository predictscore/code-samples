var m = angular.module('dao');

m.service('infoboxWithFindingsConverter', function($q, policiesRefreshHelper) {
    return function(input, args) {
        input.data.rows = _(input.data.rows).filter(function (row) {
            if (!args.findingsStatus.data[row.policy_id]) {
                return false;
            }
            row.findingsStatus = args.findingsStatus.data[row.policy_id];
            return true;
        });
        return $q.when(input);
    };
});