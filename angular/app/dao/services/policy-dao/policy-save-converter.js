var m = angular.module('dao');

m.factory('policySaveConverter', function($q, actionTagsProcessor, uuid, feature) {

    function columnsEqual(oldColumn, newColumn) {
        if (_(oldColumn.order).isNull() !== _(newColumn.order).isNull()) {
            return false;
        }
        return _(_(oldColumn).omit('order')).isEqual(_(newColumn).omit('order'));
    }

    return function (input, args) {
        var deferred = $q.defer();
        args = args || {};

        var result = $.extend(true, {}, input);
        
        if(result.policy_data.excludedBy) {
            delete result.policy_data.excludedBy;
        }

        _(result.policy_data.actions).each(function(action, idx) {
            if(!_(result.old_policy_data).isUndefined()) {
                var oldAction = _(result.old_policy_data.actions).find(function(oldAction) {
                    return action.id == oldAction.id;
                });
                if(_(oldAction).isObject()) {
                    var untagOldAction = $.extend(true, {}, oldAction);
                    actionTagsProcessor.replaceTags(untagOldAction);
                    delete untagOldAction.tags;
                    if (_(untagOldAction).isEqual(action)) {
                        result.policy_data.actions[idx] = oldAction;
                        return;
                    }
                }
            }
            actionTagsProcessor.generateTags(action, action.attributes);
            var oldId = action.id;
            action.id = uuid.random();
            var optional = _(result.policy_data.optionalActions).find(function(optional) {
                return optional.id == oldId;
            });
            if(!_(optional).isUndefined()) {
                optional.id = action.id;
            }
        });

        var fields = ['templateName', 'templateDesc', 'variables', 'optionalActions'];
        result.context.template = _(result.policy_data).pick(fields);
        result.policy_data = _(result.policy_data).omit(fields);
        if(feature.newPolicyApi) {
            //TODO in future remove all code that generate next columns
            result.policy_data = _(result.policy_data).omit('shown_columns', 'excludeCondition', 'columns');
        }

        result.entity_type = result.policy_data.entityType;
        result.policy_name = result.policy_data.name;
        result.policy_description = result.policy_data.description;

        //delete result.policy_data.entityType;
        //delete result.policy_data.name;
        //delete result.policy_data.description;
        
        result.tags = result.policy_data.tags;
        result.severity = result.policy_data.severity;
        result.alert = result.policy_data.alert;
        result.policy_data.run_backwards = !result.policy_data.not_run_backwards;
        
        delete result.policy_data.tags;
        delete result.policy_data.severity;
        delete result.policy_data.alert;
        delete result.policy_data.not_run_backwards;

        result.mode = (result.policy_data.isPolicy ? 'policy' : 'query');

        delete result.policy_data.isPolicy;
        
        if(!feature.showActiveResolver) {
            _(result.policy_data.columnsToShow).each(function(column) {
                if(column.resolve) {
                    column.resolve = false;
                }
            });
        }

        function processCondition(condition) {
            if(_(condition).isUndefined()) {
                return;
            }
            if(_(condition.conditions).isUndefined()) {
                if(_(condition.options).isObject() && !_(condition.options.data_is_property).isUndefined()) {
                    if(condition.options.data_is_property) {
                        condition.data = {
                            property: condition.data.fullId
                        };
                    }
                    condition.data_is_property = condition.options.data_is_property;
                    delete condition.options.data_is_property;
                    if (_(condition.options).isEqual({})) {
                        delete condition.options;
                    }
                }
                if(condition.operator == 'between') {
                    condition.data = {
                        first: Math.min(condition.data.first, condition.data.second),
                        second: Math.max(condition.data.first, condition.data.second)
                    };
                }
                if(_(['between', 'ip_between']).contains(condition.operator)) {
                    condition.data = condition.data.first + '~' + condition.data.second;
                }
                if(_(['date_after', 'date_before']).contains(condition.operator)) {
                    condition.data = moment(condition.data).unix();
                }
                if('date_between' == condition.operator) {
                    var firstUnix = moment(condition.data.first).unix();
                    var secondUnix = moment(condition.data.second).unix();
                    if(secondUnix < firstUnix) {
                        var temp = firstUnix;
                        firstUnix = secondUnix;
                        secondUnix = temp;
                    }
                    condition.data = firstUnix + '~' + secondUnix;
                }
                if('bool' == condition.operator && _(condition.data).isNull()) {
                    condition.operator = 'is_null';
                }
            } else {
                _(condition.conditions).each(function(subCond) {
                    processCondition(subCond);
                });
            }
        }
        
        function cleanMetaInfo(condition) {
            if(condition.generatedFrom) {
                delete condition.generatedFrom;
            }
            if(condition.conditions) {
                _(condition.conditions).each(function(c) {
                    cleanMetaInfo(c);
                });
            }
        }

        processCondition(result.policy_data.includeCondition);
        cleanMetaInfo(result.policy_data.includeCondition);
        
        if(!_(result.policy_data.advancedConditions).isUndefined()) {
            delete result.policy_data.advancedConditions;
        }
        
        if(result.system) {
            result.edit_delta = {
                columnsToShowRemoved: [],
                columnsToShowAdded: {},
                columnsOrderOverride: result.policy_data.columnsOrderOverride,
                columnNames: _(result.policy_data.columnNames).chain().map(function(value, key) {
                    return [key, value];
                }).filter(function(c) {
                    return result.old_policy_data.columnNames[c[0]] != c[1];
                }).object().value()
            };
            _(result.old_policy_data.columnsToShow).each(function(value, key) {
                if(!result.policy_data.columnsToShow[key]) {
                    result.edit_delta.columnsToShowRemoved.push(key);            
                }
            });
            _(result.policy_data.columnsToShow).each(function(value, key) {
                var valueNoOrder = _(value).omit('order');
                var exist = result.old_policy_data.columnsToShow[key];
                if(!exist || !columnsEqual(exist, value)) {
                    valueNoOrder.order = _(value.order).isNull() ? null : 1000000;
                    result.edit_delta.columnsToShowAdded[key] = valueNoOrder;
                }
            });
            
            result.policy_data = result.old_policy_data;
            
            if(!args.rawPolicy) {
                result = _(result).pick('edit_delta');
            }
        }
        if(!_(result.old_policy_data).isUndefined()) {
            delete result.old_policy_data;
        }

        deferred.resolve(result);
        return deferred.promise;
    };
});