var m = angular.module('dao');

m.factory('groupBySaasConverter', function($q, modulesManager, columnsHelper) {

    function pushHiddenColumns(amount, row) {
        _(amount).chain().range().each(function () {
            row.push({
                hidden: true
            });
        });
    }

    return function(input, args) {

        var saasList = modulesManager.activeSaasApps();
        var data = [];
        var total = 0;
        var saasColumnIdx = columnsHelper.getIdxById(args.columns, args.saasColumn);
        var columnsCount = 0;

        var visibleIdx = -1;
        _(args.columns).each(function (column, idx) {
            if (!column.hidden) {
                columnsCount++;
                if (visibleIdx === -1) {
                    visibleIdx = idx;
                }
            }
        });
        _(saasList).each(function (app) {
            var rows = _(input.data.data).filter(function (row) {
                return row[saasColumnIdx].text === app.name;
            });
            if (rows.length > 0) {
                var sectionRow = [];
                pushHiddenColumns(visibleIdx, sectionRow);
                sectionRow.push({
                    text: '#' + JSON.stringify({
                        display: 'linked-text',
                        text: '#' + JSON.stringify({
                            display: 'img',
                            path: app.img
                        }) + '#' + JSON.stringify({
                            display: 'text',
                            text: args.saasLablePrefix + app.title
                        }),
                        'class': 'grouped-by-saas-saas-title'
                    }),
                    alwaysShow: true,
                    colSpan: columnsCount,
                    unselectable: true
                });
                pushHiddenColumns(args.columns.length - 1 - visibleIdx, sectionRow);
                data.push(sectionRow);
                data = data.concat(rows);
                total += (1 + rows.length);
            }
        });
        input.data.pagination.total = total;
        $.extend(input.data, {data: data});
        return $q.when(input);
    };
});