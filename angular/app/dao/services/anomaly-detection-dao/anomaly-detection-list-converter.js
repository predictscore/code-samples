var m = angular.module('dao');

m.factory('anomalyDetectionListConverter', function($q) {
    return function(input, args) {

        var rows = _(input.data).map(function (item, key) {
            item.id = key;
            return item;
        });
        return $q.when({
            data: {
                rows: rows
            }
        });
    };
});