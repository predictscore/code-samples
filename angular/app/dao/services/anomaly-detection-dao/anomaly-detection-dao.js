var m = angular.module('configuration');

m.factory('anomalyDetectionDao', function(http, defaultListConverter, $q, anomalyDetectionListConverter, objectTypeDao, findIdConverter, groupBySaasConverter, commonDao, entityTypes) {
    return {
        retrieve: function (id) {
            return http.get('/api/v1/anomaly')
                .converter(anomalyDetectionListConverter)
                .converter(findIdConverter, {
                    id: id
                });
        },
        update: function (id, params) {
            return http.post('/api/v1/anomaly/' + id).body(params).run();
        },
        list: function(columns) {
            return http.get('/api/v1/anomaly')
                .preFetch(objectTypeDao.entities(), "entities")
                .converter(anomalyDetectionListConverter)
                .converter(defaultListConverter, function (preFetches) {
                    var entities = preFetches.entities.data.entities;
                    return {
                        columns: columns,
                        converters: {
                            entityType:function (text) {
                                if (_(entities[text]).isUndefined()) {
                                    return 'Unknown';
                                }
                                return entities[text].label;
                            },
                            getSaasFromEntity: function (text, columnId, columnsMap, args) {
                                var entity = columnsMap[args.entityColumn];
                                if (!entities[entity] || !entities[entity].saas) {
                                    return;
                                }
                                return entities[entity].saas;
                            }
                        }
                    };
                }).converter(groupBySaasConverter, {
                    columns: columns,
                    saasColumn: 'saas',
                    saasLablePrefix: 'Anomalies for '
                });
        },
        anomalyDetailsList: function (columns, id, anomalyEntity) {
            var resolved = {};
            return http.get('/api/v1/anomaly/' + anomalyEntity + '/' + id)
                .converter(function (input) {
                    var dataToResolve = [];
                    _(columns).each(function (column) {
                        var pointerConverter;
                        if (column.converter === 'entityPointerConverter') {
                            pointerConverter = column;
                        } else if (column.converter === 'multiConverter') {
                            pointerConverter = _(column.converterArgs.converters).find(function (converter) {
                                return converter.converter === 'entityPointerConverter';
                            });
                        }
                        if (pointerConverter) {
                            _(input.data.rows).each(function (row) {
                                if (row[column.id]) {
                                    if (_(row[column.id]).isArray()) {
                                        _(row[column.id]).each(function (id) {
                                            dataToResolve.push({
                                                entity_type: pointerConverter.converterArgs.entityType,
                                                entity_id: id
                                            });
                                        });
                                    } else {
                                        dataToResolve.push({
                                            entity_type: pointerConverter.converterArgs.entityType,
                                            entity_id: row[column.id]
                                        });
                                    }
                                }
                            });
                        }
                    });
                    return commonDao.entityIdentifierMulti(dataToResolve)
                        .then(function (resolvedMap) {
                            resolved = resolvedMap || {};
                            return input;
                        });
                }).converter(defaultListConverter, function (preFetches) {

                    function makePointerLink(text, local, saas, resolvedEntityType) {
                        var entity = resolvedEntityType[text];
                        if (!local || !saas) {
                            return entity && entity.main_identifier_value || text;
                        }
                        return '#' + JSON.stringify({
                                display: 'link',
                                type: local,
                                module: saas,
                                id: text,
                                text: entity && entity.main_identifier_value || text
                            });
                    }


                    return {
                        columns: columns,
                        converters: {
                            entityPointerConverter: function (text, columnId, columnsMap, args) {
                                if (!text || !args.entityType) {
                                    return text;
                                }
                                var local = entityTypes.toLocal(args.entityType);
                                var saas = entityTypes.toSaas(args.entityType);
                                var resolvedEntityType = resolved && resolved[args.entityType];
                                if (_(text).isArray()) {
                                    return _(text).map(function (id) {
                                        return makePointerLink(id, local, saas, resolvedEntityType);
                                    });
                                }
                                return makePointerLink(text, local, saas, resolvedEntityType);
                            },
                            optionsMapConverter: function (text, columnId, columnsMap, args) {
                                return args && args[text] || text;
                            }
                        }
                    };
                });
        }
    };
});