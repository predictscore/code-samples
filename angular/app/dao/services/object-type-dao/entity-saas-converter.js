var m = angular.module('dao');

m.factory('entitySaasConverter', function($q) {
    return function(input) {
        _(input.data.entities).each(function (entity) {
            if (!entity.saas && entity.depends_on && input.data.saas_list[entity.depends_on]) {
                entity.saas = entity.depends_on;
                entity.saasType = 'depends_on';
            }
        });
        return $q.when(input);
    };
});