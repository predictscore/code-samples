var m = angular.module('dao');

m.factory('obsoletePropertiesConverter', function($q) {
    return function(input) {
        var data = _(input.data).chain().map(function(info, property) {
            return {
                property: property,
                info: info
            };
        }).filter(function(p) {
            return p.info.is_obsolete;
        }).map(function(p) {
            return p.property;
        }).value();
        
        return $q.when({
            data: data
        });
    };
});