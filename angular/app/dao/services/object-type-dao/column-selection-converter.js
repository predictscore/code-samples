var m = angular.module('dao');

m.factory('columnSelectionConverter', function($q) {
    return function(input, args) {
        var data = {};
        _(input.data).each(function(saasData, saasId) {
            _(saasData).each(function(entity, entityId) {
                var groups = {};
                _(entity).chain().orderObject().each(function(column) {
                    var groupColumns = groups[column.group] || [];
                    groupColumns.push({
                        id: column._key,
                        label: column.label,
                        column: column
                    });
                    groups[column.group] = groupColumns;
                });
                data[entityId] = _(groups).chain().map(function(group, groupLabel) {
                    return {
                        label: groupLabel,
                        columns: group
                    };
                }).sortBy(function(group) {
                    return _(group.columns).first().order;
                }).value();
            });
        });
        
        return $q.when({
            data: data
        });
    };
});