var m = angular.module('dao');

m.factory('policyPropertiesConverter', function ($q, propertiesTreeCommon, $routeParams, conditionPropertyTypes, feature) {
    var propertyTypes = conditionPropertyTypes();
    function typeConverter(menu) {
        var type = menu.value_type;
        var multiSelect = menu.multi_select;
        var isPolicy = menu.is_policy;

        if (multiSelect && type != 'BOOLEAN') {
            return 'enum';
        }
        if(isPolicy) {
            menu.options = _(menu.options).filter(function(option) {
                return option.id != $routeParams.id;
            });
            return 'policies';
        }
        if (type == 'INT') {
            return 'number';
        }
        if (type == 'FLOAT') {
            return 'float';
        }
        if (type == 'BOOLEAN') {
            return 'boolean';
        }
        if (type == 'DATETIME' || type == 'DATE') {
            return 'date';
        }
        if (type == 'IP_ADDR') {
            return 'ipAddr';
        }
        return 'string';
    }

    function propertyTypeConverter(property) {
        if(property.is_entity) {
            return 'property';
        }
        if(property.is_entity_type) {
            return 'entity-type';
        }
        if(property.type == 'Boolean') {
            return 'boolean';
        }
        if(property.type == 'TEXT') {
            return 'property-string';
        }
        if(property.type == 'MULTILINE_TEXT') {
            return 'property-multiline-string';
        }
        if(property.type == 'RICH_TEXT_EDITOR') {
            return 'property-email-body';
        }
        return 'string';
    }

    return function (input, args) {
        var deferred = $q.defer();

        var saasList = input.data.saas_list;
        var entities = input.data.entities;
        var actions = input.data.actions;

        var data = {
            saasList: saasList,
            sources: _.noop,
            sourcesInfo: {},
            _sources: {},
            allSources: {},
            propertyTypes: $.extend(true, {}, propertyTypes),
            actionTypes: {},
            dataUrls: {}
        };

        _(entities).each(function (entity, entityName) {
            data.actionTypes[entityName] = _(actions).chain().map(function(action, actionId) {
                if(_(action.saas_apps).isUndefined() || _(action.saas_apps).contains(entity.saas)) {
                    return {
                        id: actionId,
                        label: action.label,
                        display: 'popup',
                        size: 'lg',
                        confirm: action.confirm,
                        attributes: _(action.attributes).chain().orderObject({key: 'id'}).map(function (property) {
                            var result = _(property).pick(['id', 'label','order']);
                            result.type = propertyTypeConverter(property);
                            if(result.type == 'property') {
                                result.propertyType = action.entities || property.type;
                                if(!_(result.propertyType).isArray()) {
                                    result.propertyType = [result.propertyType];
                                }
                            }
                            return result;
                        }).value(),
                        entities: action.entities,
                        inline_hide: action.inline_hide,
                        exclude_from_policy: action.exclude_from_policy,
                        label_disabled: action.label_disabled
                    };
                }
                return null;
            }).filter(function(action) {
                return !_(action).isNull();
            }).filter(function(action) {
                return true;// _(action.entities).isUndefined() || _(action.entities).contains(entityName);
            }).value();

            if(!_(entity.dataUrl).isUndefined()) {
                data.dataUrls[entityName] = entity.dataUrl;
            }
        });

        var properties = data.allSources;
        
        function buildVariants(menu) {
            if(menu.value_type == 'BOOLEAN') {
                if(!menu.options) {
                    return  [{
                        id: true,
                        label: 'True',
                        icon: 'check.svg'
                    }, {
                        id: false,
                        label: 'False',
                        icon: 'cross-faded.svg'
                    }];
                }
                return _(menu.options).map(function(o) {
                    return $.extend({}, o, {
                        id: {
                            'true': true,
                            'false': false
                        } [o.id]
                    });
                });
            }
            return menu.options;
        }

        _(entities).each(function (entity, entityName) {
            var entityProperty = properties[entityName] = {
                id: entityName,
                label: entity.label,
                saas: entity.saas,
                group: entity.group,
                groupOrder: entity.group_order,
                sub: [],
                selectable: false,
                parentEntity: entity.parent_entity
            };
            _(entity.properties).each(function (menu) {
                var propertyType = typeConverter(menu);
                var id = entityName + '.' + menu.name;
                properties[id] = {
                    id: id,
                    entityName: entityName,
                    entity: entityProperty,
                    name: menu.name,
                    label: menu.label,
                    description: menu.description,
                    pointer: menu.entity_pointer,
                    isPolicy: menu.is_policy,
                    isSaas: menu.is_saas,
                    isSecureApp: menu.is_secapp,
                    onlyOnRoot: menu.is_policy,
                    selectable: true,
                    type: propertyType,
                    originType: menu.value_type,
                    variants: buildVariants(menu),
                    mainIdentifier: menu.main_identifier,
                    showInCondition: !menu.gui_hidden,
                    showInExcludeBy: !menu.gui_hidden || menu.show_in_exclude_by,
                    advancePointer: menu.advance_pointer,
                    advanceProperty: menu.advance_property,
                    defaultValue: menu.default_value,
                    oneToMany: menu.is_one_to_many,
                    isExternalLink: menu.is_external_link,
                    externalLinkLabel: menu.link_label,
                    showAsBytes: menu.show_as_bytes,
                    showWithFolderIcon: menu.show_with_folder_icon
                };
                if(properties[id].pointer) {
                    properties[id].pointer_ref = _(properties[id].name.match(/^.*\((.*)\)$/m)).last();
                }
                var propertyOperators = data.propertyTypes[propertyType];
                properties[id].variants = properties[id].variants || propertyOperators[0].defOptions;
                properties[entityName].sub.push(id);
            });
        });
        
        var propertiesSort = function(subId) {
            var prefix = null;
            if(properties[subId].isSaas) {
                prefix = '2';
            } else if(properties[subId].isSecureApp) {
                prefix = '3';
            } else if(properties[subId].isPolicy) {
                prefix = '4';
            } else if(properties[subId].pointer) {
                prefix = '5';  
            } else {
                properties[subId].isCommon = true;
                prefix = '8';
            }
            properties[subId].priority = prefix;
            return prefix + '-' + properties[subId].label;
        };

        _(properties).chain().filter(function(property) {
            return !_(property.sub).isUndefined();
        }).each(function(property) {
            property.sub = _(property.sub).chain().sortBy(propertiesSort).reduce(function(memo, subId) {
                if(!memo.length) {
                    memo.push(subId);
                } else {
                    var last = properties[_(memo).last()];
                    var current = properties[subId];

                    if(last.priority != current.priority) {
                        memo.push({
                            separator: true,
                            onlyOnRoot: last.onlyOnRoot
                        });
                    }
                    memo.push(subId);
                }
                return memo;
            }, []).flatten().value();
        });

        _(properties).chain().filter(function (property) {
            return _(property.name).isString();
        }).each(function (property) {
            if (_(property.pointer).isString() && _(properties[property.pointer]).isObject()) {
                property.pointerLabel = properties[property.pointer].label;
                property.group = properties[property.pointer].group;
                property.groupOrder = properties[property.pointer].groupOrder;
                property.sub = _(properties[property.pointer].sub).map(function(sub) {
                    if(_(sub).isString()) {
                        var id = property.name + '.' + properties[sub].name;
                        properties[id] = properties[sub];
                        return id;
                    }
                    return sub;
                });
                property.selectable = false;
            }
        });

        function getSubProperties(subOverrides, entityName, propertyName, result, ignore) {
            result = result || {};
            if (_((properties[propertyName] || {}).sub).isUndefined()) {
                return {};
            }
            _(properties[propertyName].sub).chain().filter(function(id) {
                if(!properties[id]) {
                    return false;
                }
                ignore[properties[id].pointer] = ignore[properties[id].pointer] || 0;
                return true;
            }).each(function (id) {
                if(properties[id].group) {
                    var groupPropertyName = _(id.split('.')).first() + '~' + properties[id].group;
                    var groupProperty = result[groupPropertyName] = result[groupPropertyName] || {
                        label: properties[id].group,
                        pointerLabel: properties[id].group,
                        sub: [],
                        selectable: false,
                        showInCondition: true,
                        showInExcludeBy: true,
                        groupMenu: true,
                        advancePointer: true,
                        advanceProperty: true,
                    };
                    if(!_(groupProperty.sub).contains(id)) {
                        groupProperty.sub.push(id);
                    }
                    groupProperty.advancePointer = groupProperty.advancePointer && properties[id].advancePointer;
                    groupProperty.advanceProperty = groupProperty.advanceProperty && properties[id].advanceProperty;
                    
                    var sub = subOverrides[propertyName] || properties[propertyName].sub.slice();
                    var idx = _(sub).findIndex(function(subId) {
                        return subId == id;
                    });
                    sub.splice(idx, 1);
                    if(!_(sub).contains(groupPropertyName)) {
                        sub.splice(idx, 0, groupPropertyName);
                    }
                    subOverrides[propertyName] = sub;
                }
                if (_(result[id]).isUndefined()) {
                    result[id] = properties[id] || {};
                    var subIgnore = $.extend({}, ignore);
                    subIgnore[properties[id].pointer] = (subIgnore[properties[id].pointer] || 0) + 1;
                    getSubProperties(subOverrides, entityName, id, result, subIgnore);
                }
            });
            return result;
        }

        data.sources = function(entityName) {
            if(!data._sources[entityName]) { 
                var entity = input.data.entities[entityName];
                var subOverrides = {};
                if(entity.entity_type == 'base_entity') {
                    var ignore = {};
                    ignore[entityName] = 1;
                    var subProperties = $.extend(true, {}, getSubProperties(subOverrides, entityName, entityName, {}, ignore));
                    _(subProperties).each(function(property, id) {
                        if(id.indexOf('~') != -1) {
                            property.sub = _(property.sub).sortBy(function(sub) {
                                return subProperties[sub].groupOrder || 0;
                            });
                        }
                    });
                    var source = data._sources[entityName] = {
                        label: entity.label,
                        saas: entity.saas,
                        properties: subProperties
                    };
                    source.properties[0] = properties[entityName];
                    _(subOverrides).each(function(sub, propertyName) {
                        if(source.properties[propertyName]) {
                            source.properties[propertyName].sub = sub;
                        } else if(propertyName == entityName) {
                            source.properties[0].sub = sub;
                        }
                    });
                }
            }
            return data._sources[entityName];
        };
        _(input.data.entities).each(function(entity, entityName) {
            if(entity.entity_type == 'base_entity') {
                data.sourcesInfo[entityName] = {
                    label: entity.label,
                    saas: entity.saas
                };
            }
        });
        data.entities = input.data.entities;
        // _(input.data.entities).each(function (entity, entityName) {
        //     var subOverrides = {};
        //     if(entity.entity_type == 'base_entity') {
        //         var ignore = {};
        //         ignore[entityName] = 1;
        //         var subProperties = $.extend(true, {}, getSubProperties(subOverrides, entityName, entityName, {}, ignore));
        //         _(subProperties).each(function(property, id) {
        //             if(id.indexOf('~') != -1) {
        //                 property.sub = _(property.sub).sortBy(function(sub) {
        //                     return subProperties[sub].groupOrder || 0;
        //                 });
        //             }
        //         });
        //         var source = data.sources[entityName] = {
        //             label: entity.label,
        //             saas: entity.saas,
        //             properties: subProperties
        //         };
        //         source.properties[0] = properties[entityName];
        //         _(subOverrides).each(function(sub, propertyName) {
        //             if(source.properties[propertyName]) {
        //                 source.properties[propertyName].sub = sub;
        //             } else if(propertyName == entityName) {
        //                 source.properties[0].sub = sub;
        //             }
        //         });
        //     }
        // });

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    };
});
