var m = angular.module('dao');

m.factory('objectTypeDao', function($q, http, policyPropertiesConverter, policyPropertiesFilterConverter, columnSelectionConverter, obsoletePropertiesConverter, entitySaasConverter) {
    var objectTypeDao = {
        retrieve: function(filter, onlyEntities, onlySaas) {
            if (_(filter).isUndefined()) {
                filter = 'showInCondition';
            }
            var result = http.get('/api/v1/policies/policy_ui_conditions/').params({
                entities_only: onlyEntities ? 'true' : undefined,
                saas_only: onlySaas ? 'true' : undefined
            }).converter(entitySaasConverter)
                .converter(policyPropertiesConverter);
            if (filter) {
                result = result.converter(policyPropertiesFilterConverter, {
                    filter: filter
                });
            }
            return $q.when(result);
        },
        columnsSelection: function(saas) {
            return http.get('/api/v1/generic/column_selection')
                .converter(columnSelectionConverter, {
                    saas: saas
                });
        },
        obsoleteProperties: function(saas, entityType) {
            return http.get('/api/v1/entity/props/' + saas + '/' + entityType)
                .params({
                    obsolete: true
                })
                .converter(obsoletePropertiesConverter);
        },
        entities: function () {
            return http.get('/api/v1/policies/policy_ui_conditions/').params({
                entities_only: 'true'
            }).converter(entitySaasConverter);
        }
    };
    return objectTypeDao;
});