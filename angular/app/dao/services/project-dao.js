var m = angular.module('dao');

m.factory('projectDao', function(http, sqlQueries, singleRowConverter, defaultListConverter) {
    return {
        retrieve: function(entityId) {
            return http.get(sqlQueries('entity/project')).params({
                entity_id: entityId
            }).converter(singleRowConverter);
        },
        buckets: function (columns, projectId) {
            return http.get(sqlQueries('project_buckets')).params({
                project_id: projectId
            }).converter(defaultListConverter, function(preFetches) {
                    return {
                        columns: columns
                    };
                });
        }
    };
});