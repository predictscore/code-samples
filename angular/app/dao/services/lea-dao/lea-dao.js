var m = angular.module('dao');

m.factory('leaDao', function($q, http, sqlQueries, defaultListConverter, feature) {
    var leaDao = {
        topApps: function(columns, params) {
            return http.get(sqlQueries('top_apps'))
                .params(params).converter(defaultListConverter, {
                    columns: columns,
                    converters: {
                        leaApplicationConverter: function(text) {
                            var result = '#' + JSON.stringify({
                                    display: 'link',
                                    type: 'application',
                                    id: text,
                                    text: text
                                });
                            if(feature.isShowAnomalyTable()) {
                                result += '#' + JSON.stringify({
                                        display: 'text',
                                        text: 'Allow',
                                        'class': 'alerts-tag green-tag cursor-pointer'
                                    });
                            }
                            return result;
                        }
                    }
                });
        },
        appUsage: function(after, groupFormat, query, saas) {
            query = query || 'apps_usage';
            return http.get(sqlQueries(query, false, saas)).params({
                after: after,
                group_format: groupFormat
            });
        },
        appsByRisk: function(after) {
            return http.get(sqlQueries('apps_by_risk')).params({
                after: after
            });
        },
        appsByCategory: function(after) {
            return http.get(sqlQueries('apps_by_category')).params({
                after: after
            });
        },
        topDomains: function(after, saas, query) {
            return http.get(sqlQueries(query || 'shadow_it_top_saas', false, saas)).params({
                after: after
            });
        },
        saasLatestAccess: function(columns, saas, query) {
            return http.get(sqlQueries(query || 'shadow_it_saas_latest_access', false, saas))
                .converter(defaultListConverter, {
                    columns: columns,
                    converters: {
                        userListOrToConverter: function (text, columnId, columnsMap, args) {
                            var users = _(text).chain().map(function (userName, idx) {
                                if (userName && columnsMap.user_ids[idx] && columnsMap.user_emails[idx]) {
                                    return '#' + JSON.stringify({
                                            display: 'link',
                                            'class': 'user-name display-block',
                                            type: 'user',
                                            id: columnsMap.user_ids[idx],
                                            text: userName
                                        }) + '#' + JSON.stringify({
                                            display: 'text',
                                            'class': 'user-email',
                                            text: columnsMap.user_emails[idx]
                                        });
                                }
                            }).compact().value();
                            if (users.length) {
                                return '#' + JSON.stringify({
                                        display: 'inline-list',
                                        'class': 'medium-width',
                                        data: users,
                                        countText: users.length + ' recipients'
                                    });
                            } else {
                                return columnsMap.to;
                            }
                        }
                    }
                });
        },
        saasCategories: function(after, saas, query) {
            return http.get(sqlQueries(query || 'shadow_it_saas_categories', false, saas)).params({
                after: after
            });
        },
        ciscoTopDomainsBySessions: function(after) {
            return http.get(sqlQueries('cisco_top_domains_by_sessions')).params({
                after: after
            });
        }
    };
    return leaDao;
});