var m = angular.module('gui-v2');

m.factory('appsDao', function($q, http, modulesDao) {

    var appsDao = {
        getApps: function () {
            return modulesDao.getModules(true);
        }
    };

    return appsDao;
});