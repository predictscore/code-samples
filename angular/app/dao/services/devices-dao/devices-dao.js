var m = angular.module('dao');

m.factory('devicesDao', function(http, defaultListConverter) {
    return {
        list: function(columns) {
            return http.get('/api/v1/devices').converter(defaultListConverter, {
                columns: columns
            });
        },
        operation: function(device, operation) {
            return http.post('/api/v1/device/' + device + '/' + operation).run();
        }
    };
});