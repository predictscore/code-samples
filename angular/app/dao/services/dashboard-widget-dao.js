var m = angular.module('dao');

m.factory('dashboardWidgetDao', function ($q, http, singleRowConverter, dataToRowsConverter, policyDao, policyInfoBoxesConverter, modulesManager, sqlQueries) {
    var dashboardWidgetDao = {
        retrieve: function (id) {
            return http.post('/api/v1/dynamic/dashboard_widget')
                .body({
                    operation: 'select',
                    data: {
                        id: id
                    }
                }).converter(dataToRowsConverter)
                .converter(singleRowConverter, {
                    allowEmpty: true
                });
        },
        update: function (id, conf, isDefault) {
            return http.post('/api/v1/dynamic/dashboard_widget')
                .body({
                    operation: 'update',
                    data: {
                        search: {
                            id: id
                        },
                        newvalues: {
                            conf: conf,
                            is_default: !!isDefault
                        }
                    }
                }).run();
        },
        insert: function (id, conf, isDefault) {
            return http.post('/api/v1/dynamic/dashboard_widget')
                .body({
                    operation: 'insert',
                    data: {
                        id: id,
                        conf: conf,
                        is_default: !!isDefault
                    }
                }).run();
        },
        remove: function (id) {
            return http.post('/api/v1/dynamic/dashboard_widget')
                .body({
                    operation: 'delete',
                    data: {
                        id: id
                    }
                }).run();
        },
        save: function (id, conf, isDefault) {
            return dashboardWidgetDao.update(id, conf, isDefault)
                .then(function (response) {
                    if (response.data && response.data.rowcount === 0) {
                        return dashboardWidgetDao.insert(id, conf, isDefault);
                    }
                });
        },
        widgetsWithData: function (ids, disableCache) {
            var params = {ids: ids.join(',')};
            if (disableCache) {
                params.rnd = Math.random();
            }
            return http.get(sqlQueries('dashboard_widgets_data'))
                .params(params)
                .preFetch(policyDao.infoboxCatalog(), 'policyCatalog')
                .converter(policyInfoBoxesConverter, _.identity);
        },
        defaults: function () {
            return http.get('/api/v1/generic/dashboard_widget');
        }
    };
    return dashboardWidgetDao;
});