var m = angular.module('dao');

m.factory('saasStatusDao', function(http, $q) {
    return {
        basic: function (saas) {
            return http.get('/api/v1/saas_status/' + saas +'/basic');
        },
        counts: function (saas, hours) {
            return http.get('/api/v1/saas_status/' + saas + '/count/' + hours);
        },
        delay: function (saas) {
            return http.get('/api/v1/saas_status/' + saas + '/delay');
        },
        last_learn: function (saas) {
            return http.get('/api/v1/saas_status/' + saas + '/last_learn');
        },
        last_learn_ntf: function (saas) {
            return http.get('/api/v1/saas_status/' + saas + '/last_learn_ntf');
        },
        realtime_status: function (saas) {
            return http.get('/api/v1/saas_status/' + saas +'/realtime_status');
        }
    };
});