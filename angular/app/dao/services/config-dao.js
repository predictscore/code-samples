var m = angular.module('dao');

m.factory('configDao', function(http, $q, futureCache) {
    var configDao = {
        param: function (paramName, noCache) {
            var q = http.get('/api/v1/config/' + paramName);
            if (noCache) {
                q = q.addUuid();
            }
            return q.converter(function (response) {
                    return $q.when(response.data[paramName]);
                });
        },
        setParam: function (paramName, value) {
            return http.post('/api/v1/config/' + paramName)
                .body({
                    value: value
                }).run();
        },
        getTrialExpirationDays: function () {
            return configDao.getDeploymentStatus()
                .converter(function (data) {
                    if (data.deployment_mode !== 'poc') {
                        return $q.when(null);
                    }
                    return $q.when(data.pocExpiresInDays);
                });
        },
        getDeploymentStatus: function () {
            return http.get('/api/v1/deployment_config').addUuid()
                .converter(function (input) {
                    var data = input.data;

                    data.pocExpiresInDays = null;
                    data.pocExpirationDate = null;

                    if (data.association_date && data.poc_duration_in_days) {
                        var associationDate = moment.utc(data.association_date);
                        if (!associationDate.isValid()) {
                            associationDate = moment.utc(data.association_date, 'YYYY-MM-DD_HH-mm-ss');
                        }
                        if (associationDate.isValid()) {
                            var pocExpirationDate = associationDate.add(data.poc_duration_in_days, 'days').local();
                            var expireIn = pocExpirationDate - moment();
                            data.pocExpiresInDays = Math.ceil(expireIn / (1000 * 60 * 60 * 24));
                            if (expireIn < 0 && data.pocExpiresInDays > -1) {
                                data.pocExpiresInDays = -1;
                            }
                            data.pocExpirationDate = pocExpirationDate.format('YYYY-MM-DD HH:mm:ss');
                        }
                    }

                    data.paidExpiresInDays = null;
                    data.paidExpirationDate = null;

                    if (data.paid_start && data.paid_duration_in_days) {
                        var paidExpirationDate = moment.utc(data.paid_start).add(data.paid_duration_in_days, 'days').local();
                        var paidExpireIn = paidExpirationDate - moment();
                        data.paidExpiresInDays = Math.ceil(paidExpireIn / (1000 * 60 * 60 * 24));
                        if (paidExpireIn < 0 && data.paidExpiresInDays > -1) {
                            data.paidExpiresInDays = -1;
                        }
                        data.paidExpirationDate = paidExpirationDate.format('YYYY-MM-DD HH:mm:ss');
                    }

                    return $q.when(data);
            }, _.identity);
        },
        getLocalConfJson: function (fileName) {
            return http.get('/api/v1/json/' + fileName).onRequestFail(function () {
                return $q.when({
                    data: null
                });
            }).converter(function (response) {
                return $q.when(response.data);
            });
        },
        getGlobalConfJson: function (fileName) {
            return http.get('/api/v1/json_files/conf/' + fileName + '.json').onRequestFail(function () {
                return $q.when({
                    data: null
                });
            }).converter(function (response) {
                return $q.when(response.data);
            });
        },
        getConfJson: function (fileName) {
            return configDao.getLocalConfJson(fileName)
                .preFetch(configDao.getGlobalConfJson(fileName), "globalConf")
                .converter(function (response, args) {
                    return $q.when(response || args.globalConf);
                }, _.identity);
        },
        apAvananConfig: function () {
            return futureCache.cache('ap-avanan-config', 600000, function () {
                return http.get('/api/v1/json_files/modules/ap_avanan/conf/ui_categories.json')
                    .converter(function (response) {
                        return $q.when(response.data);
                    });
            });
        }
    };
    return configDao;
});