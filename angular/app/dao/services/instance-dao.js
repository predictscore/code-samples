var m = angular.module('dao');

m.factory('instanceDao', function(http, sqlQueries, singleRowConverter, defaultListConverter) {
    return {
        retrieve: function(entityId) {
            return http.get(sqlQueries('entity/instance')).params({
                entity_id: entityId
            }).converter(singleRowConverter);
        },
        users: function(columns, instanceId) {
            return http.get(sqlQueries('instance_users')).params({
                instance_id: instanceId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        groups: function(columns, instanceId) {
            return http.get(sqlQueries('instance_groups')).params({
                instance_id: instanceId
            }).converter(defaultListConverter, {
                columns: columns
            });
        }
    };
});