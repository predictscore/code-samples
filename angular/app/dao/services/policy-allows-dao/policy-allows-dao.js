var m = angular.module('dao');

m.factory('policyAllowsDao', function($q, http, defaultListConverter, filterConverter, feature) {
    return {
        retrieve: function (columns, policyId) {
            var promise = http.get('/api/v1/policies/allow_generic').params({
                policy_id: policyId
            });
            if(feature.newPolicyApi) {
                promise = http.get('/api/v1/policy/' + policyId + '/allow');
            }
            return promise.converter(filterConverter, {
                filter: function(row) {
                    return row.status != 'mark_for_deletion';
                }
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        create: function (policyId, label, path, pathValues) {
            if(feature.newPolicyApi) {
                return http.post('/api/v1/policy/' + policyId + '/allow').body({
                    allow_name: label,
                    rule_path: path,
                    path_values: pathValues
                }).run();
            }
            return http.post('/api/v1/policies/allow_generic').body({
                policy_id: policyId,
                allow_name: label,
                rule_path: path,
                entity_ids: pathValues
            }).run();
        },
        createExclude: function(policyId, entityIds) {
            return $q.all(_(entityIds).map(function(entityId) {
                return http.post('/api/v1/policies/allow_generic_root_entity').body({
                    policy_id: policyId,
                    entity_id: entityId
                }).run();
            }));
        },
        remove: function (allowId) {
            if(feature.newPolicyApi) {
                return http.delete('/api/v1/policy/allow/' + allowId).run();
            }
            return http.delete('/api/v1/policies/allow_generic/' + allowId).run();
        },
        types: function (section) {
            return http.get('/api/v1/policies/allows').params({
                section: section
            });
        },
        move: function(fromId, toId) {
            return http.get('/api/v1/policy/allow/move').params({
                from: fromId,
                to: toId
            }).run();
        }
    };
});

