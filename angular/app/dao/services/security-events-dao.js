var m = angular.module('dao');

m.service('securityEventsDao', function(http) {
    return {
        list: function() {
            return http.get('/api/v1/security_event');
        },
        action: function (eventId, actionId) {
            return http.post('/api/v1/security_event/' + eventId + '/action').body({actions: [actionId]}).run();
        },
        update: function (eventId, data) {
            return http.put('/api/v1/security_event/' + eventId).body(data).run();
        }
    };
});