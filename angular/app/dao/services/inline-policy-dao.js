var m = angular.module('dao');

m.factory('inlinePolicyDao', function ($q, http, modulesManager) {
    var inlinePolicyDao = {
        retrieveConfig: function (module) {
            module = (module || modulesManager.currentModule());
            return http.get('api/v1/' + module + '/inline_users').onRequestFail(function (input) {
                return $q.when({
                    data: {
                        rows: []
                    }
                });
            });
        },
        saveConfig: function (params, module) {
            module = (module || modulesManager.currentModule());
            return http.post('api/v1/' + module + '/inline_users').body(params).run();
        },
        retrieveSuspiciousUsersConfig: function (module) {
            module = (module || modulesManager.currentModule());
            return http.get('api/v1/' + module + '/suspicious_phishing_inline_users').onRequestFail(function (input) {
                return $q.when({
                    data: {
                        rows: []
                    }
                });
            });
        },
        saveSuspiciousUsersConfig: function (params, module) {
            module = (module || modulesManager.currentModule());
            return http.post('api/v1/' + module + '/suspicious_phishing_inline_users').body(params).run();
        }

    };
    return inlinePolicyDao;
});
