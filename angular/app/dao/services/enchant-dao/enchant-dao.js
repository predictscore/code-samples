var m = angular.module('dao');

m.factory('enchantDao', function(http, sqlQueries, defaultListConverter, mimeIcons, primaryKeyReorderConverter) {
    return {
        dataEntitiesByIds: function(columns, ids, actions, idsCounts, idsExpandable) {
            return http.get(sqlQueries('viewer/data_entities')).params({
                entity_ids: ids.join(',')
            }).preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function(preFetches) {
                    return {
                        actions: actions,
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns,
                        converters: {
                            isExpandableConverter: function(text, columnId, columnsMap) {
                                return idsExpandable[columnsMap.entity_id];
                            },
                            subItemsConverter: function(text, columnId, columnsMap) {
                                if(idsExpandable[columnsMap.entity_id]) {
                                    return idsCounts[columnsMap.entity_id];
                                }
                                return '';
                            }
                        }
                    };
                }).converter(primaryKeyReorderConverter, {
                    ids: ids,
                    columns: columns,
                    primaryKeyColumn: 'entity_id'
                });
        },
        dataEntitiesByRootCause: function(columns, policyId, rootCause, actions, args) {
            args = args || {};
            return http.get(sqlQueries('viewer/data_entities_under_root_cause')).params({
                policy_id: policyId,
                root_cause: rootCause,
                show_files: args.showFiles,
                show_folders: args.showFolders
            }).preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function(preFetches) {
                    return {
                        actions: actions,
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns,
                        converters: {
                            subItemsConverter: function(text, columnId, columnsMap) {
                                return '';
                            }
                        }
                    };
                });
        }
    };
});