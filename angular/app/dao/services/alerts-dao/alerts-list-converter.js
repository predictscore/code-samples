var m = angular.module('dashboard');

m.factory('alertsListConverter', function($q, entityTypes, policySeverityTypes, policyReportV2Converter) {
    return function(input, args) {
        var rowCounter = 0;
        input.data.rows = _(input.data.rows).filter(function (row) {
            var result;
            row.severity = policySeverityTypes.idFixed(row.severity);

            try {
                var source = args.objectTypes.data.sources(row.entity_type);
                if (!source) {
                    throw 'notfound';
                }
                var saasInfo = args.objectTypes.data.saasList[source.saas];
                if (!saasInfo) {
                    throw 'notfound';
                }
                var local = entityTypes.toLocal(row.entity_type);
                if (!local) {
                    throw 'notfound';
                }
                var mainIdentifier = _(source.properties).find(function (prop) {
                    return prop.mainIdentifier && (prop.entityName === row.entity_type);
                });
                if (!mainIdentifier) {
                    throw 'notfound';
                }
                var entity_id = (_(row.row_data).find(function(data) {
                    return (data.path === row.entity_type) && (data.field_type === 'entity_id');
                }) || {}).value;
                if (!entity_id) {
                    throw 'notfound';
                }
                var entity_title = (_(row.row_data).find(function(data) {
                    return (data.path === mainIdentifier.id);
                }) || {}).value;
                if (!entity_title) {
                    throw 'notfound';
                }
                result = 'A ' + saasInfo.label + ' ' + source.label + ' ';
                result += '#' + JSON.stringify({
                        type: local,
                        id: entity_id,
                        text: entity_title,
                        module: source.saas
                    });
                result += ' was detected by ';
                result += '#' + JSON.stringify({
                        type: 'policy-edit',
                        id: row.policy_id,
                        text: row.policy_name
                    });
            } catch (e) {
                result = 'A new match to ' + '#' + JSON.stringify({
                        type: 'policy-edit',
                        id: row.policy_id,
                        text: row.policy_name
                    });
            }
            row.description = result;
            if (args.showDetails) {
                _(row.row_data).each(function (el) {
                    var m;
                    if (!el.parent_path || !el.field) {
                        m = el.path.match(/(.+)\.([^\.:]+)$/);
                        if (m && m.length === 3) {
                            el.parent_path = el.parent_path || m[1];
                            el.field = el.field || m[2];
                        }
                    }
                    if (!el.parent_path) {
                        m = el.path.match(/(.+)\.[^\.:]+:/);
                        if (m && m.length === 2) {
                            el.parent_path = m[1];
                        }
                    }

                    if (!el.field) {
                        m = el.path.match(/\(([^\.:]+)\)$/);
                        if (m && m.length === 2) {
                            el.field = m[1];
                        }
                    }
                    if (row.policy_data && row.policy_data.columnNames && row.policy_data.columnNames[el.path]) {
                        el.label = row.policy_data.columnNames[el.path];
                    }
                });
                row.rowIdx = rowCounter;
                rowCounter++;
                row.reportDataPromise = policyReportV2Converter({
                    data: {
                        rows: [_(row.row_data).chain().map(function (data, key) {
                            return [key, data.value];
                        }).object().value()],
                        table: {
                            schema: row.row_data,
                            path_to_id_column: _(row.row_data).chain().map(function (data, key) {
                                if (data.field_type === 'entity_id') {
                                    return [data.path, key];
                                }
                            }).compact().object().value()
                        },
                        policy: {
                            policy_data: row.policy_data || {
                                columnsToShow: {}
                            }
                        }
                    }
                }, {
                    properties: args.objectTypes.data.allSources,
                    mimeIcons: args.mimeIcons,
                    noAddToAllows: true
                }).then(function (response) {
                    var promises = [];
                    _(response.data.data).each(function (row) {
                        _(row).each(function (col) {
                            promises.push($q.when(col.text).then(function (result) {
                                col.text = result;
                            }));
                        });
                    });
                    return $q.all(promises).then(function () {
                        return response;
                    }).then(function (response) {
                        return _(response.data.columns).chain().map(function (column, idx) {
                            if (column.hidden) {
                                return;
                            }
                            return '#' + JSON.stringify({
                                    display: 'linked-text',
                                    'class': 'alert-details-row',
                                    text: '#' + JSON.stringify({
                                        display: 'text',
                                        text: column.text,
                                        'class': 'col-title'
                                    }) + '#' + JSON.stringify({
                                        display: 'linked-text',
                                        text: response.data.data[0][idx].text,
                                        'class': 'col-value'
                                    })
                                });
                        }).compact().value().join('');
                    });
                });
            }
            return true;
        });
        return $q.when(input);
    };
});