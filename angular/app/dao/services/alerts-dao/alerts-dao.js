var m = angular.module('dao');

m.factory('alertsDao', function ($q, http, defaultListConverter, sqlQueries, alertsListConverter, objectTypeDao, singleRowConverter, futureCache, mimeIcons, configCache) {

    function retrieveObjectTypes() {
        return futureCache.cache('alertsDao-retrieveObjectTypes', 60000, function () {
            return objectTypeDao.retrieve(false);
        });
    }

    function alertsGetSql(query) {
        var q = http.get(function (preFetches) {
            if (preFetches.hideSystemAlerts) {
                q.params({
                    hide_system: true
                });
            }
            return sqlQueries(query);
        }).preFetch(configCache.get('hide_system_alerts'), 'hideSystemAlerts');
        return q;
    }

    var alertsDao = {
        list: function (columns, after, noDetails) {
            return alertsGetSql('alerts').params({
                after: after
            }).preFetch(retrieveObjectTypes(), 'objectTypes')
                .preFetch(mimeIcons(), 'mimeIcons')
                .converter(alertsListConverter, function (preFetches) {
                    return $.extend(preFetches, {
                        showDetails: !noDetails
                    });
                }).converter(defaultListConverter, function() {
                    return {
                        columns: columns
                    };
                });
        },
        retrieve: function (columns, after) {
            return alertsDao.list(columns, after)
                .showTotalRows(false);
        },
        newCount: function(afterMs) {
            return alertsGetSql('alerts').params({
                after: '2001-01-01',
                after_ms: afterMs
            }).count().converter(singleRowConverter);
        },
        update: function (id, params) {
            return http.post('/api/v1/alert/' + id + '/update')
                .body(params).run();
        },
        severityCounts: function () {
            return alertsGetSql('alert_severity_counts');
        },
        stateCounts: function () {
            return alertsGetSql('alert_state_counts');
        },
        appCounts: function () {
            return alertsGetSql('alert_app_counts');
        },
        policies: function () {
            return alertsGetSql('alert_policies');
        },
        severityGraph: function () {
            return alertsGetSql('alert_severity_graph');
        },
        assignees: function () {
            return alertsGetSql('alert_assignees');
        },
        updateByFilter: function (params) {
            return http.post('/api/v1/alert/update_by_filter').body(params).run();
        }


    };
    return alertsDao;
});