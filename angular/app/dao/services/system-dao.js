var m = angular.module('dao');

m.factory('systemDao', function(http, sqlQueries, singleRowConverter, defaultListConverter) {
    return {
        version: function() {
            return http.get('api/v1/system/version');
        },
        logList: function (columns) {
            return http.get(sqlQueries('system_log')).converter(defaultListConverter, {
                columns: columns
            });
        }
    };
});