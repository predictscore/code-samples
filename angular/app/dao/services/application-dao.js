var m = angular.module('dao');

m.factory('applicationDao', function(http, defaultListConverter, sqlQueries, singleRowConverter) {
    return {
        retrieve: function(appId) {
            return http.get(sqlQueries('entity/application')).params({
                entity_id: appId
            }).converter(singleRowConverter);
        },
        userList: function(columns, appId) {
            return http.get(sqlQueries('viewer/users_with_application')).params({
                app_id: appId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        policyFindings: function(columns, after, appId) {
            return http.get(sqlQueries('policy_findings')).params({
                after: after,
                entity_id: appId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        access: function(columns, appId) {
            return http.get(sqlQueries('application_scopes')).params({
                app_id: appId
            }).converter(defaultListConverter, {
                columns: columns
            });
        }
    };
});