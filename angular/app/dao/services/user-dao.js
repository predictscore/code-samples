var m = angular.module('dao');

m.factory('userDao', function(http, futureCache, userFileShareConverter, defaultListConverter, mimeIcons, sqlQueries, singleRowConverter, $q) {
    return {
        retrieve: function(userId, localType) {
            localType = localType || 'user';
            return http.get(sqlQueries('entity/' + localType)).params({
                entity_id: userId
            }).converter(singleRowConverter);
        },
        fileShare: function(after, userId) {
            return http.get(sqlQueries('events_per_day')).params({
                after: after,
                user_id: userId,
                event_name: 'change_user_access',
                event_type: 'acl_change'
            }).converter(userFileShareConverter, {
                after: after
            });
        },
        policyFindings: function(columns, after, userId) {
            return http.get(sqlQueries('policy_findings')).params({
                after: after,
                entity_id: userId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        applicationList: function(columns, userId) {
            return http.get(sqlQueries('user_applications')).params({
                user_id: userId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        filesOwnedList: function(columns, userId) {
            return http.get(sqlQueries('user_files')).params({
                user_id: userId
            }).preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function(preFetches) {
                    return  {
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns
                    };
                });
        },
        permissions: function(columns, userId) {
            return http.get(sqlQueries('user_permissions')).params({
                user_id: userId
            }).preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function(preFetches) {
                    return  {
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns
                    };
                });
        },
        shares: function(columns, userId) {
            return http.get(sqlQueries('user_shares')).params({
                user_id: userId
            }).order('natural_order', 'asc').preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function(preFetches) {
                    return  {
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns
                    };
                });
        },
        connections: function(columns, userId) {
            return http.get(sqlQueries('user_connected')).params({
                user_id: userId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        groupMembership: function(columns, groupId, localType) {
            localType = localType || 'group';
            return http.get(sqlQueries('users_in_' + localType)).params({
                group_id: groupId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        userGroups: function(columns, userId, localType) {
            localType = localType || 'user';
            return http.get(sqlQueries(localType + '_groups')).params({
                user_id: userId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        topUserCollaborators: function (userId) {
            return http.get(sqlQueries('top_user_collaborators')).params({
                user_id: userId
            });
        },
        topUsers: function (after) {
            return http.get(sqlQueries('top_users')).params({
                after: after
            });
        },
        userChannels: function(columns, userId) {
            return http.get(sqlQueries('user_channels')).params({
                user_id: userId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        userEmailAddresses: function (columns, userId) {
            return http.get(sqlQueries('user_email_addresses')).params({
                user_id: userId
            }).converter(defaultListConverter, {
                columns: columns
            });
        }
    };
});
