var m = angular.module('dao');

m.service('policyRulesDao', function(http, $q) {
    return {
        list: function() {
            return http.get('/api/v1/policy_rule');
        },
        count: function () {
            return http.get('/api/v1/policy_rule').pagination(0, 1);
        },
        create: function (rule) {
            return http.post('/api/v1/policy_rule').body(rule).run();
        },
        retrieve: function (ruleId) {
            return http.get('/api/v1/policy_rule/' + ruleId);
        },
        update: function (ruleId, rule) {
            return http.put('/api/v1/policy_rule/' + ruleId).body(rule).run();
        },
        ruleActions: function (saas) {
            return http.get('/api/v1/security_action_types/' + saas);
        },
        dlpRules: function () {
            return http.get('/api/v1/normalized_dlp_rule').onRequestFail(function (error) {
                return $q.when({
                    data: {
                        normalized_rules: []
                    }
                });
            });
        }
    };
});