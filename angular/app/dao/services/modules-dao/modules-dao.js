var m = angular.module('dao');

m.factory('modulesDao', function($q, http, appsConverter, modulesConverter, appStoreConverter, cloudAppsConverter, securityMatrixConverter, scriptConverter, policyDao, cloudAppsDashboardConverter, modulesStatsConverter, moduleLabelsConverter, feature, configDao, entityInfoConverter, servicenowAccountsConverter, modulePropsToOptions, saasStatusReportBuilder) {
    var appApiUrl = '/api/v1/apps';
    var categoriesApiUrl = '/api/v1/apps/categories';

    var appStoreOrder = {
        categorySort: 'appStoreOrder',
        moduleSort: 'app_store_order'
    };
    var securityStackOrder = {
        categorySort: 'securityStackOrder',
        moduleSort: 'security_stack_order'
    };

    function getOnlyAppsMap() {
        return configDao.getLocalConfJson('app_store_apps').converter(function (onlyAppsConfig) {
            var onlyApps = onlyAppsConfig && _(onlyAppsConfig).chain().map(function (category, categoryName) {
                    return category.apps;
                }).flatten().uniq().compact().value();
            if (!onlyApps || onlyApps.length < 1) {
                onlyApps = null;
            } else {
                onlyApps = _(onlyApps).chain().map(function (appName) {
                    return [appName, true];
                }).object().value();
            }
            return $q.when(onlyApps);
        });
    }

    function getTopCategories () {
        return http.get('/api/v1/apps/top_categories').onRequestFail(function () {
            return $q.when({
                data: []
            });
        });
    }

    var modulesDao = {
        getModules: function(includeSecEntityNames) {
            var q = http.get(appApiUrl)
                .preFetch(getTopCategories(), 'topCategories')
                .preFetch(http.get(categoriesApiUrl), 'appCategories')
                .preFetch(configDao.param('hide_beta_apps'), 'hideBetaApps')
                .preFetch(configDao.param('categories_stack_order_override'), 'categoriesStackOrderOverride')
                .preFetch(configDao.param('categories_app_store_order_override'), 'categoriesAppStoreOrderOverride')
                .preFetch(getOnlyAppsMap(), 'onlyApps')
                .converter(appsConverter, function(preFetches) {
                    return preFetches;
                });
            if (includeSecEntityNames) {
                q = q.preFetch(modulesDao.getSecEntityConfigs(), 'secEntityConfigs');
            }
            return q;
        },
        appStore: function(filter) {
            return modulesDao.getModules()
                .preFetch(http.get('/api/v1/apps/app_store_type'), 'appStoreType')
                .converter(modulesConverter, appStoreOrder)
                .dynamicConverter(function (preFetches) {
                    return appStoreConverter[preFetches.appStoreType.data.app_store_name] || appStoreConverter.all;
                }, {
                    filter: filter
                });
        },
        cloudApps: function() {
            return modulesDao.getModules()
                .converter(modulesConverter, appStoreOrder)
                .converter(cloudAppsConverter);
        },
        securityMatrix: function(noCache) {
            function params () {
                if (noCache) {
                    return {rnd: Math.random()};
                } else {
                    return {};
                }
            }
            if (feature.newSecurityMatrix) {
                return modulesDao.getModules().params(params())
                    .preFetch(policyDao.modulesMatrix().params(params()), 'modulesMatrix')
                    .converter(modulesConverter, securityStackOrder)
                    .converter(securityMatrixConverter, function (preFetches) {
                        return preFetches;
                    });
            } else {
                return modulesDao.getModules().params(params())
                    .preFetch(policyDao.policyCatalog(), 'policyCatalog')
                    .preFetch(policyDao.modulesMatrix().params(params()), 'modulesMatrix')
                    .converter(modulesConverter, securityStackOrder)
                    .converter(securityMatrixConverter, function (preFetches) {
                        return preFetches;
                    });
            }
        },
        cloudAppsDashboard: function() {
            return modulesDao.getModules(true)
                .converter(modulesConverter, appStoreOrder)
                .converter(cloudAppsDashboardConverter);
        },
        retrieveConfig: function (moduleName) {
            return http.get('/api/v1/apps/' + moduleName + '/config');
        },
        saveConfig: function(appName, data, disableAutoStart) {
            return http.put('/api/v1/apps/' + appName + '/config').params({
                not_start: disableAutoStart
            }).body(data).run();
        },
        operation: function(appName, operation, data) {
            return http.post('/api/v1/apps/' + appName + '/operation/' + operation).body(data).run();
        },
        updateState: function(categoryId, modules, state) {
            return http.put('/api/v1/app_change_state').body({
                app_category: categoryId,
                app_items: modules,
                operator: state
            }).run();
        },
        modulesStats: function(entityType) {
            return http.get('/api/v1/apps/statistics').params({
                entity_type: entityType
            }).preFetch(modulesDao.getModules(), 'modules')
                .converter(modulesStatsConverter, function (preFetches) {
                    return $.extend(preFetches, appStoreOrder);
                });
        },
        saveLicense: function(moduleName, data) {
            return http.post('/api/v1/apps/' + moduleName + '/license').body(data)
                .then(function (response) {
                    if (response && response.data && response.data.success === false) {
                        return $q.reject(response);
                    }
                    return response;
                });
        },
        opswatLicense: function () {
            return http.get('/api/v1/apps/opswat_license');
        },
        moduleLabels: function(module, entity) {
            return http.get('/api/v1/apps/' + module + '/entity/' + entity)
                .converter(moduleLabelsConverter, {
                    module: module,
                    entity: entity
                });
        },
        entityInfo: function(module, entity, asPolicy) {
            return http.get('/api/v1/apps/' + module + '/entity/' + entity)
                .params({
                    same_as_db: asPolicy ? 0 : 1
                })
                .converter(entityInfoConverter, {
                    module: module,
                    entity: entity
                });
        },
        entityInfoRaw: function(module, entity) {
            return http.get('/api/v1/apps/' + module + '/entity/' + entity)
                .params({
                    same_as_db: 1
                }).converter(function (response) {
                    return $q.when(response.data && response.data[entity] || {});
                });
        },
        getAppInfo: function (saas) {
            return http.get(appApiUrl).converter(function (input) {
                var result = {};
                var found = _(input.data).find(function (app) {
                    return app.name === saas;
                });
                if (found) {
                    result.data = modulePropsToOptions(found);
                }
                return $q.when(result);
            });
        },
        authLink: function (saas, params) {
            return http.get('/api/v1/' + saas + '/auth_link').params(params);
        },
        multiAccounts: function (app) {
            if (app === 'servicenow') {
                return http.get('/api/v1/servicenow/instances').params({
                        rnd: Math.random()
                    }).converter(servicenowAccountsConverter);
            }
            return $q.reject({});
        },
        multiAccountAction: function (app, accountId, action) {
            if (app === 'servicenow') {
                return http.post('/api/v1/servicenow/' + action + '/' + accountId).run();
            }
            return $q.when({});
        },
        multiAccountRemove: function (app, accountId) {
            if (app === 'servicenow') {
                return http.delete('/api/v1/servicenow/' + accountId).run();
            }
            return $q.when({});
        },
        confirmEmail: function (app, param, code) {
            var body = {};
            body[param] = code;
            return http.put('/api/v1/apps/' + app + '/email_confirm').body(body).run();
        },
        getSecEntityNames: function() {
            return http.get('/api/v1/apps/sec_entity_names');
        },
        getSecEntityConfigs: function() {
            return http.get('/api/v1/apps/sec_entity_configs').onRequestFail(function () {
                return $q.when(null);
            });
        },
        saasStatus: function (columns, timeoutPromise, fullReport, onlySaas) {
            return saasStatusReportBuilder({
                columns: columns,
                timeoutPromise: timeoutPromise,
                fullReport: fullReport,
                onlySaas: onlySaas
            });
        },
        learningModeEnable: function (app, targetState) {
            return http.post('/api/v1/apps/' + app + '/learn_mode/' + (targetState ? 'enable': 'disable')).run();
        },
        adminLicense: function (appName, params) {
            return http.post('/api/v1/apps/' + appName + '/license_adm').body(params).run();
        }
    };

    return modulesDao;
});