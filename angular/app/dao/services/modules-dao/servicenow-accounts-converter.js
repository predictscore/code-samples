var m = angular.module('dao');

m.factory('servicenowAccountsConverter', function($q) {
    return function(input, args) {
        return $q.when({
            data : {
                accounts: _(input.data).chain().map(function (account) {
                    return {
                        id: account.instance_domain,
                        label: account.instance_domain + ' ('+ account.company_domain + ')',
                        is_enabled: account.is_active,
                        stateToggleAction: account.is_active ? 'disable': 'enable'
                    };
                }).sortBy('id').value()
            }
        });
    };
});