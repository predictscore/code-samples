var m = angular.module('dao');

m.factory('saasStatusReportBuilder', function($q, modulesManager, entityTypes, defaultListConverter, saasStatusDao, policyDao, $timeout, $filter, logging, policiesRefreshHelper, slowdownHelper) {

    var reloadOnErrorTimeout = 10000;

    function queryReloader(getQuery, timeoutPromise) {
        return getQuery().timeout(timeoutPromise).run().then(function (input) {
            return input;
        }, function (error) {
            if (error.status > 0) {
                return $timeout(slowdownHelper.adjustInterval(reloadOnErrorTimeout)).then(function () {
                    return queryReloader(getQuery, timeoutPromise);
                });
            }
            return $q.reject(error);
        });
    }

    return function(args) {

        var columns = args.columns;
        var timeoutPromise = args.timeoutPromise;
        var onlySaas = args.onlySaas;

        var columnsMap = _(args.columns).chain().map(function (col) {
            return [col.id, col];
        }).object().value();

        var basicQueries = [];

        var usersPoliciesQuery = (columnsMap.internalUsers ?
                queryReloader(function () {
                    return policyDao.policyCatalog().params({
                        filterBy_tags: 'users_internal'
                    });
                }, timeoutPromise).then(function (response) {
                    return _(response.data.rows).chain().map(function (row) {
                        var saas = entityTypes.toSaas(row.entity_type);
                        if (!saas) {
                            return;
                        }
                        return [saas, row];
                    }).compact().object().value();
                }) : $q.when()
        );
        var basicQueryColumns = [
            'fullscan_status',
            'token_status',
            'last_learned'
        ];
        var loadBasicQueries = !!_(basicQueryColumns).find(function (columnId) {
            return columnsMap[columnId] && !columnsMap[columnId].hidden;
        });

        var rows = _(modulesManager.activeSaasApps()).chain().filter(function (saas) {
            return !onlySaas || _(onlySaas).contains(saas.name);
        }).map(function (saas) {
            return {
                saas: saas.name,
                saasForIcon: saas.name,
                saasLabel: saas.title
            };
        }).value();

        _(rows).each(function (row) {
            var saasName = row.saas;

            if (columnsMap.realtime_status) {
                row.realtime_status = queryReloader(function() {
                    return saasStatusDao.realtime_status(saasName);
                }, timeoutPromise).then(function (response) {
                    return response.data && response.data.realtime_status;
                });
            }
        });

        _(rows).each(function (row) {
            var saasName = row.saas;

            var basicQuery = (loadBasicQueries ?
                queryReloader(function () {
                    return saasStatusDao.basic(saasName);
                }, timeoutPromise).then(function (response) {
                    if (!response.data) {
                        logging.log('No SaaS status data for ' + saasName);
                        response.data = {};
                    }
                    response.data.saas = saasName;
                    return response;
                }) : $q.when({
                data: {}
            }));
            basicQueries.push(basicQuery);

            if (columnsMap.fullscan_status) {
                row.fullscan_status = basicQuery.then(function (response) {
                    return response.data.fullscan_status;
                });
            }

            if (columnsMap.internalUsers) {
                row.internalUsers = usersPoliciesQuery.then(function (policies) {
                    if (!policies) {
                        return;
                    }
                    var policy = policies[saasName];
                    if (!policy && saasName === 'google_mail' && policies.google_drive) {
                        policy = policies.google_drive;
                    }
                    if (policy) {
                        policiesRefreshHelper.addPolicies(policy.policy_id);
                        return '#' + JSON.stringify({
                                display: 'link',
                                type: 'policy-edit',
                                'id': policy.policy_id,
                                text: policy.counts_match_count
                            });
                    }
                });
            }
            if (columnsMap.total_count) {
                row.total_count = queryReloader(function () {
                    return saasStatusDao.counts(saasName, 0);
                }, timeoutPromise).then(function (response) {
                    return response.data && response.data.count;
                });
            }

            if (args.fullReport) {
                row.token_status = basicQuery.then(function (response) {
                    return response.data.token_status;
                });
                row.today_count = queryReloader(function () {
                    return saasStatusDao.counts(saasName, 24);
                }, timeoutPromise).then(function (response) {
                    return response.data && response.data.count;
                });
                row.last_hour_count = queryReloader(function () {
                    return saasStatusDao.counts(saasName, 1);
                }, timeoutPromise).then(function (response) {
                    return response.data && response.data.count;
                });

                row.saas_delay = queryReloader(function () {
                    return saasStatusDao.delay(saasName);
                }, timeoutPromise).then(function (response) {
                    return response.data && response.data.saas_delay;
                });

                row.saas_delay = queryReloader(function () {
                    return saasStatusDao.delay(saasName);
                }, timeoutPromise).then(function (response) {
                    return response.data && response.data.saas_delay;
                });

                row.last_learned = $q.all([
                    basicQuery,
                    queryReloader(function () {
                        return saasStatusDao.last_learn(saasName);
                    }, timeoutPromise)
                ]).then(function (responses) {
                    var entityType = responses[0].data.entity_type;
                    var last_learned = responses[1].data && responses[1].data.last_learned;
                    if (!last_learned) {
                        return;
                    }
                    var entitySaas = entityTypes.toSaas(entityType);
                    var local = entityTypes.toLocal(entityType);
                    if (!entitySaas || !local) {
                        return last_learned.insert_time;
                    }
                    return '#' + JSON.stringify({
                            display: 'link',
                            type: local,
                            module: entitySaas,
                            'id': last_learned.entity_id,
                            text: $filter('localTime')(last_learned.insert_time)
                        });
                });
                row.last_learned_notification = queryReloader(function () {
                    return saasStatusDao.last_learn_ntf(saasName);
                }, timeoutPromise).then(function (response) {
                    var last_learned_notification = response.data && response.data.last_learned_notification;
                    if (!last_learned_notification) {
                        return;
                    }
                    return last_learned_notification.time;
                });

            }
        });
        
        return defaultListConverter({
            data: {
                rows: rows,
                total_rows: rows.length
            }
        }, {
            columns: columns,
            converters: {
                fullScanStatusConverter: function (text) {
                    if (text === 'done') {
                        return '#' + JSON.stringify({
                                display: 'text',
                                'class': 'fa fa-check-circle green-text fullscan-status',
                                tooltip: 'Done'
                            });
                    }
                    if (text === 'scanning') {
                        return '#' + JSON.stringify({
                                display: 'text',
                                'class': 'fa fa-cog fa-spin gray-text fullscan-status',
                                tooltip: 'Scanning'
                            });
                    }
                    if (text === 'error') { // I think there's no such status now, just want to keep this icon
                        return '#' + JSON.stringify({
                                display: 'text',
                                'class': 'fa fa-question-circle red-text fullscan-status',
                                tooltip: 'Unknown'
                            });
                    }
                    return '';
                },
                realtimeStatusConverter: function (text) {
                    if (text === 100) {
                        return '#' + JSON.stringify({
                                display: 'text',
                                'class': 'fa fa-check green-text realtime-status',
                                tooltip: 'Ready'
                            });
                    }
                    if (_(text).isFinite()) {
                        return '#' + JSON.stringify({
                                display: 'text',
                                'class': 'fa fa-circle-o-notch fa-spin gray-text realtime-status',
                                tooltip: 'Getting ready...'
                            });
                    }
                    return '';
                },
                tokenStatusConverter: function (text) {
                    if (text) {
                        return '#' + JSON.stringify({
                                display: 'text',
                                'class': 'fa fa-check green-text token-status'
                            });
                    } else if (text === false) {
                        return '#' + JSON.stringify({
                                display: 'text',
                                'class': 'fa fa-times red-text token-status'
                            });
                    }
                }
            },
            info: {
                scanProgressPromise: $q.all(basicQueries).then(function (responses) {
                    return _(responses).chain().filter(function (response) {
                        return response.data && response.data.fullscan_status && response.data.fullscan_status !== 'done';
                    }).map(function (response) {
                        return response.data.saas;
                    }).value();
                })
            }
        });
    };
});