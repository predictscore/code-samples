var m = angular.module('dao');

m.factory('modulesStatsConverter', function($q, dao) {
    return function(input, args) {
        return $q.when({
            data: _(input.data).chain().keys().filter(function (moduleName) {
                    return !!input.data[moduleName];
                }).map(function (moduleName) {
                var module = _(args.modules.data.apps_items.modules).find(function (module) {
                    return module.name == moduleName;
                });
                return  $.extend(true, {
                    subModuleName: moduleName,
                    statistic: $.extend(input.data[moduleName], {
                        label: module.label,
                        all: input.data[moduleName].total,
                        success_match: input.data[moduleName].total_match,
                        success_unmatch: input.data[moduleName].total_unmatch,
                        error: input.data[moduleName].total_error
                    }),
                    category: _(args.modules.data.apps_categories.categories).find(function (category) {
                        return category.categoryId == module.category_id;
                    })
                }, module);
            }).sortBy(function (module) {
                return module.name;
            }).sortBy(function (module) {
                return parseInt(module[args.moduleSort]);
            }).sortBy(function (module) {
                return parseInt(module.category[args.categorySort]);
            }).value()
        });
    };
});