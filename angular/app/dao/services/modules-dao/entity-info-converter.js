var m = angular.module('dao');

m.factory('entityInfoConverter', function($q) {
    return function(input, args) {
        var data = input.data[args.entity].properties;
        _(data).each(function (property, propertyId) {
            if (property.options && property.value_type === 'BOOLEAN') {
                _(property.options).each(function (option) {
                    if (_(option.id).isString()) {
                        try {
                            option.id = JSON.parse(option.id);
                        } catch (e) {}
                    }
                });
            }
        });

        return $q.when({
            data: data
        });
    };
});