var m = angular.module('dao');

m.factory('moduleLabelsConverter', function($q) {
    return function(input, args) {
        var data = _(input.data[args.entity].properties).chain().map(function(property, propertyId) {
            return [propertyId, property.label || null];
        }).object().value();

        return $q.when({
            data: data
        });
    };
});