var m = angular.module('dao');

m.factory('appsConverter', function($q) {
    return function(input, args) {
        var appCategories = args.appCategories.data;
        var appItems = input.data;
        if (args.hideBetaApps) {
            appItems = _(appItems).filter(function (module) {
                return module.state !== 'display_only';
            });
        }

        var topCategories = args.topCategories && args.topCategories.data;

        _(appCategories).each(function (category) {
            category.main_module = false;
            category.multi_select = false;
            if (args.categoriesStackOrderOverride && !_(args.categoriesStackOrderOverride[category.categoryId]).isUndefined()) {
                category.securityStackOrder = args.categoriesStackOrderOverride[category.categoryId];
            }
            if (args.categoriesAppStoreOrderOverride && !_(args.categoriesAppStoreOrderOverride[category.categoryId]).isUndefined()) {
                category.appStoreOrder = args.categoriesAppStoreOrderOverride[category.categoryId];
            }

        });

        _(appItems).each(function (module) {
            if (args.secEntityConfigs && args.secEntityConfigs.data && args.secEntityConfigs.data[module.name]) {
                module.sec_entity = module.sec_entity || args.secEntityConfigs.data[module.name].name;
                module.sec_type = module.sec_type || args.secEntityConfigs.data[module.name].sec_type;
            }
            module.state = {
                'enabled': 'active',
                'disabled': 'nonactive'
            }[module.state] || module.state;
            if (_(module.start_demo_period).isUndefined()) {
                module.start_demo_period = 60;
            }
            if (module.license_info && module.license_info.license_type === 'demo') {
                module.isExpired = moment.utc(module.license_info.expire_at).isBefore();
            }
        });

        var categoriesMap = _(appCategories).chain().map(function (category) {
            return [category.categoryId, category];
        }).object().value();
        appItems = _(appItems).filter(function (module) {
            return module.state === 'active' || !args.onlyApps ||
                !categoriesMap[module.category_id] || categoriesMap[module.category_id].family !== 'security' ||
                args.onlyApps[module.name];
        });



        var data = {
            topCategories: topCategories,
            apps_categories: {
                categories: appCategories
            },
            apps_items: {
                modules: appItems
            }
        };
        return $q.when({
            data: data
        });
    };
});