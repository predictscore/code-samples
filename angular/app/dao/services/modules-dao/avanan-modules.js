var m = angular.module('dao');

m.factory('avananModules', function($q) {
    return [{
        name: 'dlp_avanan',
        stack_icon: 'avanan.png',
        label: 'Smart Text Search',
        backendLabel: 'Smart Search',
        state: 'active',
        category: {
            "categoryName": "Smart Text Search",
            "type": "smart text search"
        }
    // }, { // Temporarly commented out. Should be completely removed. Now not clear how it all should work.
    //     name: 'ap_avanan',
    //     stack_icon: 'avanan.png',
    //     label: 'Avanan Anti phishing',
    //     state: 'active',
    //     module_name: 'ap_avanan',
    //     sec_entity: 'avanan_ap_scan',
    //     category: {
    //         "categoryName": "Anti Phishing (Avanan)",
    //         "type": "ap"
    //     }
    }];
});