var m = angular.module('dao');

m.factory('localModuleDao', function(http, $q, $http) {
    return {
        retrieve: function() {
            return http.get('/api/v1/local_module');
        },
        uploadCsv: function (module, entity_id, data) {
            var fd = new FormData();
            fd.append('data', data);
            return $http.post('/api/v1/local_module/' + module + '/' + entity_id + '/csv', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            });
        }
    };
});