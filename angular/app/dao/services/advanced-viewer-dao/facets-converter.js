var m = angular.module('dao');

m.factory('facetsConverter', function($q) {
    return function(input) {
        return $q.when({
            data:_(input.data.rows).chain().map(function(row) {
                return [row.id, {
                    include: 'include_' + row.id,
                    exclude: 'exclude_' + row.id,
                    variants: _(row.ids).map(function(id, idx) {
                        return {
                            id: id,
                            label: row.labels[idx],
                            count: row.counts[idx]
                        };
                    })
                }];
            }).object().value()
        });
    };
});