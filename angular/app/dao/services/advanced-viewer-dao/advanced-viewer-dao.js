var m = angular.module('dao');

m.factory('advancedViewerDao', function(http, sqlQueries, defaultListConverter, mimeIcons, facetsConverter, $q) {
    return {
        count: function(queryName) {
            return http.get(sqlQueries('viewer/' + queryName))
                .returnRawFirst(true)
                .pagination(0, 1)
                .converter(function(input) {
                    return $q.when({
                        data: input.data.total_rows
                    });
                });
        },
        list: function(columns, queryName) {
            return http.get(sqlQueries('viewer/' + queryName))
                .returnRawFirst(true)
                .preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function (preFetches) {
                    return {
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns
                    };
                });
        },
        facets: function(queryName) {
            return http.get(sqlQueries('viewer/' + queryName))
                .converter(facetsConverter);
        }
    };
});