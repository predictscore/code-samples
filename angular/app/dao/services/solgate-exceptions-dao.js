var m = angular.module('dao');

m.factory('solgateExceptionsDao', function(http, sqlQueries, defaultListConverter) {
    return {
        list: function (columns, userMap) {
            return http.get(sqlQueries('solgate_exceptions'))
                .converter(defaultListConverter, {
                    columns: columns,
                    converters: {
                        avananUserInfoConverter: function (text, columnId, columnsMap, args, allConverters) {
                            var userInfo = userMap[text] || {};
                            return allConverters.userWithEmail(userInfo.name, columnId, userInfo, {
                                emailColumn: 'email',
                                userIdColumn: 'noIdNeeded'
                            });
                        }
                    }
                });
        },
        add: function (data) {
            return http.post('/api/v1/solgate/exceptions').body(data).run();
        },
        remove: function (data) {
            return http.post('/api/v1/solgate/exceptions/delete').body(data).run();
        }


    };
});