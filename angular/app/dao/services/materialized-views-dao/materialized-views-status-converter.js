var m = angular.module('dao');

m.service('materializedViewsStatusConverter', function($q) {
    return function(input, args) {
        var result = {
            view: {}
        };

        if (input.data.rows.length) {
            result.running = input.data.rows && !input.data.rows[0].ended && input.data.rows[0].name;
            if (!result.running) {
                result.lastEnded = input.data.rows[0].ended;
            }

            if (input.data.rows.length > 1) {
                result.view = {
                    running: !input.data.rows[1].ended
                };

                if (result.view.running) {
                    result.view.runningTime = moment(input.data.rows[1].server_time) - moment(input.data.rows[1].started);
                }

                if (input.data.rows.length > 2) {
                    result.view.lastRunTime = moment(input.data.rows[2].ended) - moment(input.data.rows[2].started);
                    result.view.lastEndedAgo = moment.duration(moment(input.data.rows[2].server_time) - moment(input.data.rows[2].ended));
                    result.view.lastEnded = input.data.rows[2].ended;
                }
            }
        }
        result.view.lastEnded = result.view.lastEnded || 'notfound';

        input.data = result;

        return $q.when(input);
    };
});