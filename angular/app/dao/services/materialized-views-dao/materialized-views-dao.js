var m = angular.module('dao');

m.factory('materializedViewsDao', function(http, sqlQueries, materializedViewsStatusConverter) {
    var materializedViewsDao = {
        getStatus: function(viewName) {
            return http.get(sqlQueries('materialized_views_status'))
                .params({view_name: viewName})
                .converter(materializedViewsStatusConverter, {
                    viewName: viewName
                });
        },
        refresh: function (viewName) {
            if (viewName === 'all') {
                return materializedViewsDao.refreshAll();
            }
            return http.put('/api/v1/materialized_view/'+ viewName + '/refresh').run();
        },
        refreshAll: function () {
            return http.put('/api/v1/materialized_views_refresh_all').run();
        }
    };
    return materializedViewsDao;
});