var m = angular.module('dao');

m.factory('emailDomainsDao', function(http, $q, sqlQueries, defaultListConverter) {

    function successFalseConverter (response) {
        if (response && response.data && response.data.success === false) {
            return $q.reject(response);
        }
        return response;
    }
    return {
        list: function (columns) {
            return http.get(sqlQueries('email_domains'))
                .converter(defaultListConverter, {
                    columns: columns
                });

        },
        update: function (domain, type) {
            return http.post('/api/v1/email_domains/update/').body({
                domain: domain,
                type: type
            }).then(successFalseConverter);
        },
        remove: function (domain) {
            return http.post('/api/v1/email_domains/delete/').body({
                domain: domain
            }).then(successFalseConverter);
        },
        create: function (domain, type) {
            return http.post('/api/v1/email_domains/create/').body({
                domain: domain,
                type: type
            }).then(successFalseConverter);
        },
        types: function () {
            return http.get(sqlQueries('email_domains_types'))
                .converter(function (response) {
                    return $q.when(_(response.data.rows).map(function (row) {
                        return row.type;
                    }));
                });
        }
    };
});