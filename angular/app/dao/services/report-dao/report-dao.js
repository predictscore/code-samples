var m = angular.module('dao');

m.factory('reportDao', function(http, $q, defaultListConverter, path, sqlQueries, mimeIcons, removeDisabledRowsConverter, pathConverters, activeSaasOnlyConverter) {
    return {
        list: function(columns) {
            return http.get(sqlQueries('reports')).converter(defaultListConverter, {
                columns: columns
            });
        },
        retrieve: function(reportId) {
            return http.get('/api/v1/report/definitions/' + reportId);
        },
        update: function(reportId, data) {
            return http.put('/api/v1/report/definitions/' + reportId).body(data).run();
        },
        save: function(data) {
            return http.post('/api/v1/report/definitions').body(data).run();
        },
        remove: function(reportId) {
            return http.delete('/api/v1/report/definitions/' + reportId).run();
        },
        templates: function(columns) {
            return http.get('/api/v1/report')
                .converter(activeSaasOnlyConverter, {
                    allowCommon: true
                }).converter(removeDisabledRowsConverter)
                .converter(defaultListConverter, {
                    columns: columns
                });
        },
        report: function(columns, reportPath, params) {
            return http.get(sqlQueries('/' + reportPath))
                .params(params)
                .preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function(preFetches) {
                    return {
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns,
                        converters: {
                            pathConverter: function (text, columnId, columnsMap, args) {
                                return pathConverters(columnsMap[args.idsColumnName], text, null, columnsMap[args.typeColumnName]);
                            }
                        }
                    };
            });
        }
    };
});