var m = angular.module('dao');

m.factory('activeSaasOnlyConverter', function ($q, modulesManager) {
    return function (input, args) {
        var deferred = $q.defer();

        var activeSaass = _(modulesManager.cloudApps()).chain().map(function (module) {
            return [module.name, true];
        }).object().value();

        var rows = input.data.rows || [];

        var result = _(rows).filter(function(row) {
            var rowSaas = row[args.saasColumn || 'saas'];
            return activeSaass[rowSaas] || (args.allowCommon && _(rowSaas).isNull());
        });

        deferred.resolve($.extend({}, input, {
            data: {
                rows: result
            }
        }));

        return deferred.promise;
    };
});