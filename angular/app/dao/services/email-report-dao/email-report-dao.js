var m = angular.module('dao');

m.factory('emailReportDao', function($q, http, defaultListConverter) {
    return {
        trigger: function(reportId) {
            return http.post('/api/v1/trigger_email_report/' + reportId).run();  
        },
        table: function(columns, url, params) {
            return http.get('/api/v1/sql_queries/email_reports/' + url + '?' + $.param(params)).converter(defaultListConverter, {
                columns: columns
            });
        },
        chart: function(url, params) {
            return http.get('/api/v1/sql_queries/email_reports/' + url + '?' + $.param(params)).converter(function(input) {
                return $q.when(input.data.rows);
            });
        }
    };
});