var m = angular.module('dao');

m.factory('bucketDao', function(http, sqlQueries, singleRowConverter, defaultListConverter, mimeIcons) {
    return {
        retrieve: function(entityId) {
            return http.get(sqlQueries('entity/bucket')).params({
                entity_id: entityId
            }).converter(singleRowConverter);
        },
        permissions: function (columns, bucketId) {
            return http.get(sqlQueries('bucket_permissions')).params({
                bucket_id: bucketId
            }).order('natural_order', 'asc')
            .converter(defaultListConverter, function(preFetches) {
                return {
                    columns: columns
                };
            });
        },
        content: function (columns, bucketId) {
            return http.get(sqlQueries('bucket_content')).params({
                bucket_id: bucketId
            }).preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function(preFetches) {
                    return {
                        columns: columns,
                        mimeIcons: preFetches.mimeIcons
                    };
                });
        }

    };
});