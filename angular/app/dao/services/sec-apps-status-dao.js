var m = angular.module('dao');

m.factory('secAppsStatusDao', function(http, $q) {
    return {
        license: function () {
            return http.get('/api/v1/apps/sec_apps_status/license');
        },
        queue: function (app, saas) {
            return http.get('/api/v1/apps/sec_apps_status/' + app +'/queue' + (saas && saas !== 'all' ? '/' + saas : ''));
        },
        lastMatch: function (app, saas) {
            return http.get('/api/v1/apps/sec_apps_status/' + app +'/last_match' + (saas && saas !== 'all' ? '/' + saas : ''));
        },
        lastUnmatch: function (app, saas) {
            return http.get('/api/v1/apps/sec_apps_status/' + app +'/last_unmatch' + (saas && saas !== 'all' ? '/' + saas : ''));
        },
        counts: function (app, type, hours, saas) {
            return http.get('/api/v1/apps/sec_apps_status/' + app +'/counts/' + type + '/' + hours + (saas && saas !== 'all' ? '/' + saas : ''));
        },
        processTime: function (app, saas) {
            return http.get('/api/v1/apps/sec_apps_status/' + app +'/process_time/1' + (saas && saas !== 'all' ? '/' + saas : ''));
        },
        vendorTime: function (app, saas) {
            return http.get('/api/v1/apps/sec_apps_status/' + app +'/vendor_time/1' + (saas && saas !== 'all' ? '/' + saas : ''));
        }
    };
});