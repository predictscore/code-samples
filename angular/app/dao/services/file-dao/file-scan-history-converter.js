var m = angular.module('dao');

m.factory('fileScanHistoryConverter', function($q, avananModules) {
    function parseStat(condition, obj) {
        var matches = condition.match(/([^=]+)='([^']+)'/); // scan_result_a='Infected' => scan_result_a, Infected
        if (matches && (matches.length === 3) && (obj[matches[1]] == matches[2])) {
            return 1;
        }
        return 0;
    }
    return function(input, args) {
        _(avananModules).each(function (avananModule) {
            if (_(args.modules.data.apps_items.modules).chain().find(function (module) {
                    return module.name === avananModule.name;
                }).isUndefined().value()) {
                args.modules.data.apps_items.modules.push(avananModule);
            }
        });

        var data = args.lastOnly ? {} : [];

        _(input.data).chain().filter(function (v) {
            return _(v.related_apps).isArray() && v.related_apps.length === 1 && v.history_entities.length;
        }).each(function (v) {
            var found = _(args.modules.data.apps_items.modules).find(function(module) {
                return module.name == v.related_apps[0];
            });
            if (!found) {
                return;
            }
            var conf = args.config.data.apps[found.name];
            if (_(conf).isUndefined()) {
                conf = args.config.data.modules[found.module_name];
            }
            if (!conf) {
                return;
            }
            var entityData = [];
            _(v.history_entities).each(function (row) {
                var statistics = {
                    total_match: parseStat(conf.statistics.success_match, row),
                    total_unmatch: parseStat(conf.statistics.success_unmatch, row),
                    total_error: parseStat(conf.statistics.error, row),
                    total_not_scaned: parseStat(conf.statistics.not_scaned, row)
                };
                statistics.total = statistics.total_match + statistics.total_unmatch + statistics.total_error + statistics.total_not_scaned;
                if (statistics.total_match || statistics.total_unmatch) {
                    row.found = found;
                    row.statistics = statistics;
                    row.isHistory = true;
                    row.av_fd_update_time = row.av_fd_update_time || row.update_time || row.av_update_time;
                    entityData.push(row);
                }
            });
            if (!entityData.length) {
                return;
            }
            if (args.lastOnly) {
                data[found.name] = _(entityData).chain().sortBy('av_fd_update_time').last().value();
            } else {
                _(entityData).each(function (row) {
                    data.push(row);
                });
            }
        });
        return $q.when({
            data: data
        });
    };
});