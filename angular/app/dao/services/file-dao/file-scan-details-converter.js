var m = angular.module('dao');

m.factory('fileScanDetailsConverter', function($q, avananModules, troubleshooting) {
    return function(input, args) {
        _(avananModules).each(function (avananModule) {
            if (_(args.modules.data.apps_items.modules).chain().find(function (module) {
                    return module.name === avananModule.name;
                }).isUndefined().value()) {
                args.modules.data.apps_items.modules.push(avananModule);
            }
        });

        if (args.apAvananConfig && input.data.ap_avanan) {
            input.data.ap_avanan.uiConfig = args.apAvananConfig;
        }

        var data = _(input.data).chain().map(function(v, moduleName) {
            var found = _(args.modules.data.apps_items.modules).find(function(module) {
                return module.name === moduleName && (!module.gui_hidden || troubleshooting.entityDebugEnabled()) && module.name !== 'cp_capsule';
            });
            if(v) {
                v.found = found;
                v.statistics = args.statistics.data[moduleName];
                v.av_fd_update_time = v.av_fd_update_time || v.update_time || v.av_update_time;
            }
            return [moduleName, v];
        }).filter(function(moduleData) {
            if(!moduleData[1]) {
                return false;
            }
            var found = moduleData[1].found;
            var statistics = moduleData[1].statistics;
            return found && found.state == 'active' && statistics && statistics.total && !statistics.total_not_scaned && !statistics.total_error;
        }).object().value();
        return $q.when({
            data: data
        });
    };
});