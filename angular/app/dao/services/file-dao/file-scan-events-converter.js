var m = angular.module('dao');

m.factory('fileScanEventsConverter', function($q) {
    return function(input, args) {

        var data = args.history.data;
        _(input.data).each(function (v) {
            data.push(v);
        });

        return $q.when({
            data: data
        });
    };
});