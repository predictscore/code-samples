var m = angular.module('dao');

m.factory('egnyteSaasLinkConverter', function($q, modulesDao) {
    return function(input, args) {

        return modulesDao.retrieveConfig('egnyte').then(function (config) {
            var idColumn = (args.type === 'folder' ? 'entity_id' : 'version_group_id');
            if (config.data && config.data.egnyte_host && config.data.egnyte_host.value && input.data[idColumn]) {
                input.data.link_to_saas = 'https://' + config.data.egnyte_host.value + '.egnyte.com/navigate/' + args.type + '/' + input.data[idColumn];
            }
            return input;
        });
    };
});