var m = angular.module('dao');

m.factory('fileScanHistoryElementConverter', function($q) {
    return function(input, args) {
        var data = [];
        var found = _(input.data).find(function (v) {
            return _(v.related_apps).isArray() && (v.related_apps.length === 1) && (v.related_apps[0] == args.appName);
        });
        if (found) {
            var element = _(found.history_entities).find(function (value) {
                return (value.av_fd_update_time || value.update_time || value.av_update_time) == args.historyTime;
            });
            if (element) {
                data.push(element);
            }
        }
        return $q.when({
            data: {
                rows: data
            }
        });
    };
});