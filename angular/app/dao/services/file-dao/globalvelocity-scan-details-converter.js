var m = angular.module('dao');

m.factory('globalvelocityScanDetailsConverter', function($q, limitedArrayConverter, $parse, $filter, troubleshooting) {
    return function(input) {
        var deferred = $q.defer();

        var scanDetails = input.data;

        var componentNames = _($parse('scan_details.matches_components')(scanDetails)).map(function(component) {
            return component.component_name;
        });

        if(componentNames) {
            scanDetails.component_name = componentNames;
        }

        deferred.resolve($.extend({}, input, {
            data: scanDetails
        }));

        return deferred.promise;
    };
});