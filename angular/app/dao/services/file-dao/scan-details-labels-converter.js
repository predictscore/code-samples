var m = angular.module('dao');

m.factory('scanDetailsLabelsConverter', function ($q, beautifyKey) {
    return function (input, args) {
        input.data.data = _(input.data.data).chain().map(function(pair) {
            var niceKey = args.moduleLabels.data[pair.key];
            if(_(niceKey).isNull()) {
                return {};
            }
            return {
                key: args.moduleLabels.data[pair.key] || beautifyKey(pair.key),
                value: pair.value
            };
        }).filter(function(pair) {
            return pair.key;
        }).value();

        return $q.when(input);
    };
});