var m = angular.module('dao');

m.factory('entityPointerPreConverter', function($q, commonDao) {
    return function(input, args) {
        var dataToResolve = [];
        _(args.columns).each(function (column) {
            var pointerConverter;
            if (column.converter === 'entityPointerConverter') {
                pointerConverter = column;
            } else if (column.converter === 'multiConverter') {
                pointerConverter = _(column.converterArgs.converters).find(function (converter) {
                    return converter.converter === 'entityPointerConverter';
                });
            }
            if (pointerConverter) {
                _(input.data.rows).each(function (row) {
                    if (row[column.id]) {
                        if (_(row[column.id]).isArray()) {
                            _(row[column.id]).each(function (id) {
                                dataToResolve.push({
                                    entity_type: pointerConverter.converterArgs.entityTypeColumn && row[pointerConverter.converterArgs.entityTypeColumn] || pointerConverter.converterArgs.entityType,
                                    entity_id: id
                                });
                            });
                        } else {
                            dataToResolve.push({
                                entity_type: pointerConverter.converterArgs.entityTypeColumn && row[pointerConverter.converterArgs.entityTypeColumn] || pointerConverter.converterArgs.entityType,
                                entity_id: row[column.id]
                            });
                        }
                    }
                });
            }
        });
        return commonDao.entityIdentifierMulti(dataToResolve)
            .then(function (resolvedMap) {
                args.preFetches.resolvedEntities = resolvedMap || {};
                return input;
            });
    };
});