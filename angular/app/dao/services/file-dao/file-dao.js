var m = angular.module('dao');

m.factory('fileDao', function(http, defaultListConverter, defaultMapConverter, $q, countRowsConverter, sqlQueries, singleRowConverter, comodoScanDetailsConverter, objectTypeDao, notEmptyConverter, globalvelocityScanDetailsConverter, modulesDao, allScanDetailsConverter, path, mimeIcons, fileScanDetailsConverter, avananDlpScanDetailsConverter, fileScanHistoryConverter, fileScanEventsConverter, fileScanHistoryElementConverter, detailsHiddenConverter, appsAddedValueConverter, fileSecurityStackConfigConverter, securityStackReportsLinksConverter, errorStatusHolder, $injector, entityPointerPreConverter, entityTypes, secToolsExceptionParams, configDao, $filter) {

    var fileDao = {
        events: function(columns, after, fileId, entityType) {
            return http.get(sqlQueries('file_events')).params({
                file_id: fileId,
                after: after
            }).converter(defaultListConverter, {
                columns: columns
            }).showTotalRows(false);
        },
        retrieve: function(fileId, linkConverterName) {
            var result = http.get(sqlQueries('entity/file')).params({
                entity_id: fileId
            }).converter(singleRowConverter)
                .cache('fileDao.retrieve-' + fileId, 5000)
                .onRequestFail(function (error) {
                    if (error.status === 404) {
                        errorStatusHolder.errorStatus(404);
                    }
                    return $q.reject(error);
                });
            if (linkConverterName) {
                result = result.converter($injector.get(linkConverterName), {
                    type: 'file'
                });
            }
            return result;
        },
        permissions: function(columns, fileId) {
            return http.get(sqlQueries('file_permissions')).params({
                file_id: fileId
            }).order('natural_order', 'asc')
                .preFetch(fileDao.retrieve(fileId), 'file')
                .converter(function (input) {
                    var prevSection = '';
                    input.data.rows = _(input.data.rows).chain().sortBy('section').map(function (row) {
                        if (prevSection && prevSection !== row.section) {
                            row.rowClass = 'section-change';
                        }
                        prevSection = row.section;
                        return row;
                    }).value();
                    return $q.when(input);
                })
                .converter(defaultListConverter, function(preFetches) {
                    return {
                        columns: columns,
                        entity: preFetches.file
                    };
                });
        },
        shares: function(columns, fileId) {
            return http.get(sqlQueries('file_shares')).params({
                file_id: fileId
            }).order('natural_order', 'asc').converter(defaultListConverter, {
                columns: columns
            });
        },
        activitySummary: function(columns, fileId) {
            return http.get(sqlQueries('file_activity_summary')).params({
                file_id: fileId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        usersRatio: function(fileId) {
            return http.get(sqlQueries('file_permissions')).params({
                file_id: fileId
            }).converter(countRowsConverter, {
                field: 'is_external_user'
            });
        },
        types: function(userId) {
            return http.get(sqlQueries('file_shared_types_count')).params({
                user_id: userId
            });
        },
        externalUserTypes: function(userId) {
            return http.get(sqlQueries('user_file_shared_types_count')).params({
                user_id: userId
            });
        },
        allScanDetails: function(fileId, entityType, returnType) {
            var entitySaas = entityTypes.toSaas(entityType);
            var result =  http.get('/api/v1/apps/resolve_entity/' + entityType + '/' + fileId)
                .preFetch(http.get('/api/v1/apps/statistics').params({
                    entity_type: entityType,
                    entity_id: fileId
                }), 'stat')
                .preFetch(modulesDao.getModules(true), 'modules');
            var config = path('profiles').ctrl('file-profile').json('security-config').retrieve().params({
                rid: null
            });
            if (returnType !== 'summary') {
                config = config.converter(fileSecurityStackConfigConverter);
                result = result.preFetch(fileDao.mainQueueRaw(fileId), 'mainQueue');
                result = result.preFetch(http.get('/api/v1/dlp_avanan/scan_entity/' + entityType + '/' + fileId)
                    .onRequestFail(function (data) {
                        return $q.when({});
                    }), 'dlpAvananScanResults')
                    .converter(securityStackReportsLinksConverter, {
                        fileId: fileId,
                        entityType: entityType
                    });
            }
            if (entitySaas) {
                result.preFetch(modulesDao.entityInfoRaw(entitySaas, entityType), 'entityInfo');
            }
            result = result.preFetch(config, 'config')
                .converter(allScanDetailsConverter, function(preFetches) {
                    return $.extend(preFetches, {
                        returnType: returnType,
                        mainQueue: preFetches.mainQueue || {data : {}},
                        entityId: fileId,
                        entityType: entityType
                    });
                });
            return result;
        },
        scanDetails: function(appName, fileId, entityType, historyTime) {
            var result;
            if (historyTime) {
                result = fileDao.fileScanHistoryElement(fileId, entityType, appName, historyTime);
            } else {
                result = http.get(sqlQueries('scan_details')).params({
                    app_name: appName,
                    file_id: fileId,
                    entity_type: entityType
                });
            }
            return result.converter(notEmptyConverter)
                .converter(singleRowConverter);
        },
        fileScanDetails: function(fileId, entityType) {
            return http.get('/api/v1/apps/sec_entities').params({
                entity_id: fileId,
                entity_type: entityType
            }).preFetch(modulesDao.getModules(true), 'modules')
            .preFetch(http.get('/api/v1/apps/statistics').params({
                    entity_type: entityType,
                    entity_id: fileId
            }), 'statistics')
            .converter(fileScanDetailsConverter, _.identity);
        },
        comodoScanDetails: function(columns, fileId, entityType) {
            return fileDao.scanDetails('comodo', fileId, entityType)
                .converter(comodoScanDetailsConverter)
                .converter(defaultListConverter, {
                    columns: columns
                });
        },
        checkpointTeScanDetails: function(fileId, entityType) {
            return fileDao.scanDetails('checkpoint_te', fileId, entityType)
                .preFetch(fileDao.checkpointTePdfReports(fileId, entityType), 'pdfReportImages');
        },
        checkpointTePdfReports: function (fileId, entityType) {
            return http.get('/api/v1/checkpoint/pdf_report_images/' + entityType + '/' + fileId);
        },
        globalvelocityScanDetails: function(fileId, entityType, historyTime) {
            return fileDao.scanDetails('dlp_globalvelocity', fileId, entityType, historyTime)
                .converter(globalvelocityScanDetailsConverter);
        },
        avananDlpScanDetails: function(columns, fileId, entityType) {
            return http.get('/api/v1/dlp_avanan/scan_entity/' + entityType + '/' + fileId)
                .preFetch(http.get('/api/v1/dlp_rules'), 'rules')
                .converter(avananDlpScanDetailsConverter, _.identity)
                .converter(defaultListConverter, {
                    columns: columns
                });
        },
        avananDlpTroubleshooting: function(fileId, entityType) {
            return http.get('/api/v1/dlp_avanan/scan_entity/' + entityType + '/' + fileId)
                .params({
                    as_dict: true
                });
        },
        applicationList: function(columns, fileId) {
            return http.get(sqlQueries('file_applications')).params({
                file_id: fileId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        topUsers: function(mode, limit) {
            return http.get(sqlQueries('top_users')).params({
                mode: mode
            }).pagination(0, limit);
        },
        extensionsToMimeType: function(extensions) {
            return http.get(sqlQueries('extensions-to-mimetype')).params({
                extensions: extensions.join(',')
            });
        },
        fileScanHistory: function(fileId, entityType, lastOnly) {
            return http.get('/api/v1/entity_history/' + fileId)
                .preFetch(modulesDao.getModules(true), 'modules')
                .preFetch(path('profiles').ctrl('file-profile').json('security-config').retrieve().params({
                    rid: null
                }), 'config')
                .converter(fileScanHistoryConverter, function (preFetches) {
                    return $.extend({
                        lastOnly: lastOnly
                    }, preFetches);
                });
        },
        fileScanEvents: function (fileId, entityType) {
            return fileDao.fileScanDetails(fileId, entityType)
                .preFetch(fileDao.fileScanHistory(fileId, entityType), 'history')
                .converter(fileScanEventsConverter, _.identity);
        },
        fileScanHistoryElement: function(fileId, entityType, appName, historyTime) {
            return http.get('/api/v1/entity_history/' + fileId)
                .converter(fileScanHistoryElementConverter, function (preFetches) {
                    return $.extend({
                        appName: appName,
                        historyTime: historyTime
                    }, preFetches);
                });
        },
        quarantine: function(columns) {
            return http.get(sqlQueries('quarantine'))
                .preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function (preFetches) {
                    return {
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns
                    };
                });
        },
        recheck: function(entityType, entityId, apps) {
            if(_(apps).isUndefined()) {
                apps = ["all"];
            }
            return http.post('/api/v1/entity/rescan_sec').body({
                src_entity_type: entityType,
                entity_id: entityId,
                app_names: apps
            }).run();
        },
        mainQueueRaw: function (entityId) {
            return http.get('api/v1/main_queue_entity/' + entityId)
            .converter(function (input) {
                var rows = input.data;
                input.data = {
                    rows: rows
                };
                return $q.when(input);
            });
        },
        mainQueue: function(columns, entityId) {
            return fileDao.mainQueueRaw(entityId)
                .converter(defaultListConverter, function (preFetches) {
                    return {
                        columns: columns
                    };
                });
        },
        fullSecurityInfo: function (columns, fileId, entityType, source) {
            var result;
            if (source === 'resolve_entity') {
                result =  http.get('/api/v1/apps/resolve_entity/' + entityType + '/' + fileId);
            } else {
                result = http.get('/api/v1/apps/sec_entities').params({
                    entity_id: fileId,
                    entity_type: entityType
                });
            }
            return result.preFetch(modulesDao.getModules(true), 'modules');
        },
        detailsHidden: function (module, entity) {
            return http.get('/api/v1/apps/' + module + '/entity/' + entity)
                .converter(detailsHiddenConverter, {
                    module: module,
                    entity: entity
                });
        },
        entityInfo: function (entityType, entityId) {
            return http.get('/api/v1/entity/' + entityType + '/' + entityId);
        },
        versions: function(columns, fileId) {
            return http.get(sqlQueries('file_versions')).params({
                file_id: fileId
            }).converter(defaultListConverter, function (preFetches) {
                return {
                    columns: columns
                };
            });
        },
        fileChannels: function(columns, fileId) {
            return http.get(sqlQueries('file_channels')).params({
                file_id: fileId
            }).converter(defaultListConverter, function (preFetches) {
                return {
                    columns: columns
                };
            });
        },
        cryptPair: function(fileId, entityType) {
            return http.get(sqlQueries('crypt_pair')).params({
                entity_id: fileId,
                entity_type: entityType
            }).onRequestFail(function() {
                return $q.when({
                    data: {
                        rows: []
                    }
                });
            });
        },
        appsAddedValue: function() {
            var request = http.get(function (preFetches) {
                var onlyEntities = [];
                _(preFetches.modules.data.apps_items.modules).each(function (module) {
                    if (module.state === 'active' && module.sec_entity) {
                        onlyEntities.push(module.sec_entity);
                    }
                });
                request.params({
                    sec_entities: onlyEntities.join(',')
                });
                return sqlQueries('apps_added_value');
            }).preFetch(modulesDao.getModules(true), 'modules')
                .preFetch(http.get('/api/v1/json/sectools_coverage').onRequestFail(function () {
                    return $q.when({});
                }), "predefined")
                .converter(appsAddedValueConverter, _.identity);
            return request;
        },
        ftpUpload: function (entityType, entityId) {
            return http.post('/api/v1/upload_ftp/' + entityType + '/' + entityId).run();
        },
        shareForAnalysis: function(entityType, entityId, mode, data) {
            return http.post('/api/v1/share_for_analysis').body($.extend({
                entity_type: entityType,
                entity_id: entityId,
                mode: mode
            }, data)).run();
        },
        suspiciousReport: function(entityType, entityId, data) {
            return http.post('/api/v1/suspicious_report').body($.extend({
                entity_type: entityType,
                entity_id: entityId
            }, data)).run();
        },
        exceptionsRemove: function (files) {
            return http.post('/api/v1/sectool_exception/delete').body({files: files}).run();
        },
        exceptionsAdd: function (files) {
            return http.post('/api/v1/sectool_exception').body({files: files}).run();
        },
        exceptionsList: function (querySaas, columns, userMap, queryPostfix) {
            return http.get(sqlQueries('sectool_exceptions' + (queryPostfix || ''), false, querySaas))
                .preFetch(mimeIcons(), 'mimeIcons')
                .converter(entityPointerPreConverter, function (preFetches) {
                    return {
                        columns: columns,
                        preFetches: preFetches
                    };
                })
                .converter(defaultListConverter, function (preFetches) {

                    function makePointerLink(text, local, saas, resolvedEntityType) {
                        var entity = resolvedEntityType[text];
                        if (!local || !saas) {
                            return entity && entity.main_identifier_value || text;
                        }
                        return '#' + JSON.stringify({
                                display: 'link',
                                type: local,
                                module: saas,
                                id: text,
                                text: entity && entity.main_identifier_value || text
                            });
                    }

                    return {
                        columns: columns,
                        mimeIcons: preFetches.mimeIcons,
                        converters: {
                            entityPointerConverter: function (text, columnId, columnsMap, args) {
                                var entityType = args.entityTypeColumn && columnsMap[args.entityTypeColumn] || args.entityType;
                                if (!text || !entityType) {
                                    return text;
                                }
                                var local = entityTypes.toLocal(entityType);
                                var saas = entityTypes.toSaas(entityType);
                                var resolvedEntityType = preFetches.resolvedEntities && preFetches.resolvedEntities[entityType];
                                if (_(text).isArray()) {
                                    return _(text).map(function (id) {
                                        return makePointerLink(id, local, saas, resolvedEntityType);
                                    });
                                }
                                return makePointerLink(text, local, saas, resolvedEntityType);
                            },
                            exceptionsTypeConverter: function (text) {
                                return secToolsExceptionParams.getLabel(text) || text;
                            },
                            avananUserInfoConverter: function (text, columnId, columnsMap, args, allConverters) {
                                var userInfo = userMap[text] || {};
                                return allConverters.userWithEmail(userInfo.name, columnId, userInfo, {
                                    emailColumn: 'email',
                                    userIdColumn: 'noIdNeeded'
                                });
                            }
                        }
                    };
                });
        },
        exceptionsStatus: function (entityType, entityId) {
            return http.get(sqlQueries('sectool_exceptions_status')).params({
                entity_type: entityType,
                entity_id: entityId
            }).converter(function (response) {
                return $q.when(_(response.data.rows).chain().map(function (row) {
                        return [row.sec_type, true];
                    }).object().value()
                );
            }).onRequestFail(function () {
                return $q.when({
                    data: {
                        rows: []
                    }
                });
            });
        },
        avananApLinks: function(columns, entityId, entityType, tooShortOnly) { //obsolete
            return http.get(sqlQueries('ap_avanan_links')).params({
                entity_id: entityId,
                entity_type: entityType,
                too_short_only: tooShortOnly || false
            }).converter(defaultListConverter, {
                columns: columns,
                converters: {
                    "domainWithDatesConverter": function (text, columnId, columnsMap, args) {
                        return '#' + JSON.stringify({
                                display: 'text',
                                'class': 'display-block',
                                text: text
                            }) + '#' + JSON.stringify({
                                display: 'text',
                                'class': 'display-block',
                                text: 'Created: ' + $filter('localTime')(columnsMap.creation_date)
                            }) + '#' + JSON.stringify({
                                display: 'text',
                                'class': 'display-block',
                                text: 'Updated: ' + $filter('localTime')(columnsMap.updated_date)
                            });
                    },
                    "domainAgeConverter": function (text) {
                        if (!text && text !== 0) {
                            return text;
                        }
                        return text + ' day' + (text === 1 ? '' : 's');
                    }
                }
            });
        },
        avananApDebugLinks: function(columns, entityId, entityType) {
            return http.get(sqlQueries('ap_avanan_debug_links')).params({
                entity_id: entityId,
                entity_type: entityType
            }).converter(defaultListConverter, {
                columns: columns,
                converters: {
                    clickForJSONConverter: function (text, columnId, columnsMap, args) {
                        return '#' + JSON.stringify({
                                display: 'text',
                                text: 'Click for full results',
                                'class': 'link-like',
                                clickEvent: {
                                    name: 'open-link-results-viewer',
                                    results: text,
                                    link: columnsMap.link
                                }
                            });
                    },
                    scanLinkWithVirusTotalConverter: function (text, columnId, columnsMap, args) {
                        if (!args && !args.linkColumn && !columnsMap[args.linkColumn]) {
                            return '';
                        }
                        return '#' + JSON.stringify({
                                display: 'external-link',
                                text: 'Scan with VirusTotal',
                                href: 'https://www.virustotal.com/en/url/submission/?force=1&url=' + encodeURIComponent(columnsMap[args.linkColumn]),
                                rel: "noreferrer"
                            });
                    }
                }
            });
        },
        apBitdefenderLinks: function(columns, entityId, entityType) { // obsolete
            return http.get(sqlQueries('ap_bitdefender_links')).params({
                entity_id: entityId,
                entity_type: entityType
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        sbCheckpointLinks: function(columns, entityId, entityType) { // obsolete
            return http.get(sqlQueries('sb_checkpoint_links')).params({
                entity_id: entityId,
                entity_type: entityType
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        solgateLinks: function(columns, entityId, entityType) {
            return http.get(sqlQueries('solgate_links')).params({
                entity_id: entityId,
                entity_type: entityType
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        cpCapsuleInfo: function(fileId, entityType) {
            return http.get(sqlQueries('cp_capsule_info')).params({
                entity_id: fileId,
                entity_type: entityType
            }).converter(singleRowConverter, {
                allowEmpty: true
            }).onRequestFail(function() {
                return $q.when({
                    data: {
                        rows: []
                    }
                });
            });
        },
        downloadMatchedUrl: function (entityId, entityType) {
            return '/api/v1/sec_match_file/' + entityType + '/' + entityId;
        },
        downloadMatchedCheck: function (entityId, entityType) {
            return http.get(fileDao.downloadMatchedUrl(entityId, entityType)).params({
                test_only: true
            }).ignore403Error().onRequestFail(function (error) {
                return $q.when({data: {
                    status: error.status
                }});
            });
        }
    };
    return fileDao;
});