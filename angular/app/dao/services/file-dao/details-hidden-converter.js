var m = angular.module('dao');

m.factory('detailsHiddenConverter', function($q) {
    return function(input, args) {
        var detailsHidden = [];
        _(input.data[args.entity].properties).each(function(property, propertyId) {
            if (property.hide_from_details) {
                detailsHidden.push(propertyId);
            }
        });

        return $q.when({
            data: detailsHidden
        });
    };
});