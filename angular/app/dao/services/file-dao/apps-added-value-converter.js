var m = angular.module('dao');

m.factory('appsAddedValueConverter', function($q, defaultListConverter) {

    function createPredefinedData(data) {

        var levels = _(data.levels).chain().map(function (info, idx) {
            return {
                idx: parseInt(idx),
                info: _(info).chain().map(function (value, key) {
                    return {
                        entity: key,
                        found: value
                    };
                }).sortBy('found').reverse().value()
            };
        }).sortBy('idx').map(function (item) {
            return item.info;
        }).value();

        var total = 0;
        var path = _(levels).map(function (level) {
            total += level[0].found;
            return level[0];
        });

        var found = 0;
        var totals = _(path).map(function (item) {
            var result = total - found;
            found += item.found;
            return result;
        });

        var rows = [];
        _(levels).each(function (items, idx) {
            var level = idx + 1;
            var prev = _(path).first(idx);
            _(items).each(function (item, itemIdx) {
                if (!item.found) {
                    return;
                }
                var row = {
                    level: level,
                    r: itemIdx + 1,
                    detects: item.found,
                    total: totals[idx],
                    vendor: item.entity,
                    all_vendors: _(prev).pluck('entity'),
                    detections: _(prev).pluck('found')
                };
                row.all_vendors.push(item.entity);
                row.detections.push(item.found);
                rows.push(row);
            });
        });
        return rows;
    }


    return function (input, args) {

        var moduleKey = 'sec_entity';
        if (args.predefined.data) {
            input.data.rows = createPredefinedData(args.predefined.data);
            moduleKey = 'name';
        }

        var categoriesMap = {};
        _(args.modules.data.apps_categories.categories).each(function (category) {
            categoriesMap[category.categoryId] = category;
        });

        var secEntityMap = {};
        _(args.modules.data.apps_items.modules).each(function (module) {
            if (module[moduleKey]) {
                secEntityMap[module[moduleKey]] = module;
            }
        });

        var levels = {};
        var maxLevel = 0;
        var levelTotal = {};
        var levelChoosedApps = {};
        var choosedPath = [];
        var totalThreats = 0;

        _(input.data.rows).chain().sortBy('level').each(function (row) {
            var levelBase = 0;
            if (row.level < 1) {
                return;
            }
            maxLevel = Math.max(maxLevel, row.level);

            if (row.level > 1) {
                var prevLevelChoosedVendor = [row.all_vendors[row.all_vendors.length - 2]];
                levels[row.level - 1][prevLevelChoosedVendor].choosed = true;
                levels[1][prevLevelChoosedVendor].chosenOnLevel = row.level - 1;
                if (!levelChoosedApps[row.level]) {
                    levelChoosedApps[row.level] = _(row.all_vendors).chain().initial().map(function (secEntity) {
                        return secEntityMap[secEntity].label;
                    }).value().join(', ');
                }
                if (choosedPath.length < row.level - 1) {
                    choosedPath.push({
                        data: levels[row.level - 1][prevLevelChoosedVendor],
                        module:  secEntityMap[prevLevelChoosedVendor]
                    });
                }
                if (levelBase === 0) {
                    levelBase = _(row.detections).chain().initial().reduce(function (memo, value) {
                        return memo + value;
                    }, 0).value();
                }
            } else {
                totalThreats = row.total;
            }

            levels[row.level] = levels[row.level] || {};
            levelTotal[row.level] = row.total;
            levels[row.level][row.vendor] = {
                levelBase: levelBase,
                detections: row.detections,
                level: row.level,
                found: row.detects,
                percent: Math.round(row.detects / row.total * 100),
                percentTotal: (row.detects + levelBase == totalThreats) ? 100 : Math.min(Math.round((row.detects + levelBase) / totalThreats * 100), 99),
                total: row.total,
                rank: row.r,
                tooltip: row.level == 1 ? 'Threats detected' : 'Threats detected that were missed by ' + levelChoosedApps[row.level],
                tooltipTotal: row.level == 1 ? 'Threats detected' : 'Total threats detected by ' + levelChoosedApps[row.level] + ' and ' + secEntityMap[row.vendor].label
            };
        });

        _(levels[maxLevel]).find(function (info, vendor) {
            if (info.rank !== 1) {
                return false;
            }
            choosedPath.push({
                data: info,
                module: secEntityMap[vendor]
            });
            info.choosed = true;
            levels[1][vendor].chosenOnLevel = maxLevel;
            return true;
        });

        var rows = [];
        var columns = [];

        if (maxLevel > 0) {

            columns = [{
                id: 'name',
                text: false,
                converter: 'addedValueNameConverter'
            }];
            _(_.range(1, maxLevel + 1)).each(function (level) {
                columns.push({
                    id: 'col' + level,
                    text: levelTotal[level],
                    converter: 'addedValueConverter',
                    dataClass: 'added-value-value-cell',
                    'class': 'added-value-value-cell-head',
                    tooltip: (level == 1) ? 'Total amount of detected threats' : 'Total amount of detected threats missed by ' + levelChoosedApps[level]
                });
            });

            rows = _(levels['1']).chain().map(function (value, key) {
                var row = {
                    name: secEntityMap[key].label,
                    col1: value
                };
                _(_.range(2, maxLevel + 1)).each(function (level) {
                    row['col' + level] = levels[level][key];
                });
                return row;
            }).sortBy(function (row) {
                return -row.col1.found;
            }).sortBy(function (row) {
                return row.col1.chosenOnLevel || (maxLevel + 1);
            }).value();
        }

        return defaultListConverter({
            data: {
                rows: rows
            }
        }, {
            columns: columns,
            converters: {
                addedValueConverter: function (text) {
                    if (!text) {
                        return;
                    }
                    var result = '';
                    if (text.choosed) {
                        result += '#' + JSON.stringify({
                                display: 'text',
                                'class': 'added-value-choosed-background'
                            });
                    }
                    result += '#' + JSON.stringify({
                            display: 'text',
                            'class': {
                                'added-value-percent': true,
                                'added-value-percent-top': (text.rank === 1)
                            },
                            text: text.percentTotal + '%',
                            tooltip: text.tooltipTotal
                        });
                    result += '#' + JSON.stringify({
                            display: 'text',
                            'class': 'added-value-found',
                            text: text.found + (text.level < 2 ? '' : (' (' + text.percent + '%)')),
                            tooltip: text.tooltip
                        });
                    return result;
                },
                addedValueNameConverter: function (text, columnId, columnsMap, args) {
                    if (columnsMap && columnsMap.col1 && columnsMap.col1.chosenOnLevel) {
                        return '#' + JSON.stringify({
                                display: 'text',
                                'class': 'added-value-app-chosen',
                                text: text
                            });
                    } else {
                        return text;
                    }
                }
            }
        }).then(function (tableOutput) {
            return {
                tableData: tableOutput.data,
                columns: columns,
                choosedPath: choosedPath
            };
        });
    };
});