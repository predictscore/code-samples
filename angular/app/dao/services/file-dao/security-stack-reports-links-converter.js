var m = angular.module('dao');

m.factory('securityStackReportsLinksConverter', function($q, $injector) {

    function makeLink(links, label) {
        if (!links || !links.length) {
            return;
        }
        if (links.length === 1) {
            return '#' + JSON.stringify({
                    display: 'external-link',
                    href: links[0].href,
                    text: (label || 'Download PDF') + ' Report'
                });
        }
        return '#' + JSON.stringify({
                display: 'inline-list',
                data: _(links).map(function(linkInfo) {
                    return '#' + JSON.stringify({
                            display: 'external-link',
                            text: linkInfo.label,
                            href: linkInfo.href,
                            tooltip: linkInfo.label
                        });
                }),
                'class': 'pdf-reports-links-container',
                countText: (label || 'Download PDF') + ' Reports'
            });
    }
    return function (input, args) {

        if (input.data.wildfire && input.data.wildfire.sha256) {
            input.data.wildfire.pdfReportsLinks = makeLink([{
                href: '/api/v1/wildfire/pdf_report/' + input.data.wildfire.sha256
            }]);
        }

        if (input.data.solgate && input.data.solgate.have_word_report) {
            input.data.solgate.pdfReportsLinks = makeLink([{
                href: '/api/v1/solgate/report/' + args.entityType + '/' + args.fileId
            }], 'Download Word');
        }

        if (input.data.av_cisco && input.data.av_cisco.have_pdf_report) {
            input.data.av_cisco.pdfReportsLinks = makeLink([{
                href: '/api/v1/av_cisco/report/' + args.entityType + '/' + args.fileId
            }]);
        }

        if (input.data.atp_fireeye && input.data.atp_fireeye.have_report) {
            input.data.atp_fireeye.pdfReportsLinks = makeLink([{
                href: '/api/v1/atp_fireeye/' + args.entityType + '/' + args.fileId
            }], 'View HTML');
        }


        if (input.data.checkpoint_te) {
            var fileDao = $injector.get('fileDao');
            return fileDao.checkpointTePdfReports(args.fileId, args.entityType).then(function (response) {
                if (response.data && response.data.images) {
                    input.data.checkpoint_te.pdfReportsLinks = makeLink(_(response.data.images).map(function (label, imageId) {
                        return {
                            href: '/api/v1/checkpoint/pdf_report/' + args.entityType + '/' + args.fileId + '/' + imageId,
                            label: label
                        };
                    }));
                }
                return $q.when(input);
            });
        }
        return $q.when(input);
    };
});