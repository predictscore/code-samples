var m = angular.module('dao');

m.factory('fileSecurityStackConfigConverter', function($q, modulesDao) {
    return function (input, args) {

        var configNeeded = [];
        _(input.data.apps).each(function (info, appName) {
           if (info.findings) {
               if (_(info.findings).find(function (field) {
                       return !_(field.showIfConfig).isUndefined();
                   })) {
                   configNeeded.push(appName);
               }
           }
        });
        if (!configNeeded) {
            return $q.when(input);
        }
        var configs = {};
        return $q.all(_(configNeeded).map(function (appName) {
            return modulesDao.retrieveConfig(appName).onRequestFail(function () {
                return $q.when({
                    data: {}
                });
            }).then(function (response) {
                configs[appName] = response.data;
            });
        })).then(function () {
            _(input.data.apps).each(function (info, appName) {
                if (info.findings) {
                    info.findings = _(info.findings).filter(function (field) {
                        if (_(field.showIfConfig).isUndefined()) {
                            return true;
                        }
                        var show = configs[appName] && !_(field.showIfConfig).find(function (value, param) {
                                return _(configs[appName][param]).isUndefined() || configs[appName][param].value !== value;
                            });
                        delete field.showIfConfig;
                        return show;
                    });
                }
            });
            return $q.when(input);
        });

    };
});