var m = angular.module('dao');

m.factory('avananDlpScanDetailsConverter', function($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var scanDetails = input.data.rules;

        var rulesMap = {};
        _(args.rules.data).each(function (rule) {
            rulesMap[rule.id] = rule;
        });

        var rows = [];

        if (input.data && input.data.rules) {
            _(input.data.rules).each(function (d, id) {
                if (d && rulesMap[id]) {
                    d.ruleName = rulesMap[id].name;
                    rows.push(d);
                }
            });
        }

        deferred.resolve($.extend({}, input, {
            data: {
                rows: rows
            }
        }));

        return deferred.promise;
    };
});