var m = angular.module('dao');

m.factory('allScanDetailsConverter', function($q, defaultListConverter, appScanStatusHelper, troubleshooting, avananModules, modulesDao, secToolsExceptionParams, apAvananExceptionsParams, columnsHelper, sideMenuManager, emailView) {
    var findingsConverters = {
        gvComponentNamesConverter: function (data) {
            if (!data || !data.matches_components) {
                return [];
            }
            return _(data.matches_components).pluck('component_name');
        },
        cylanceScoreConverter: function (data) {
            if (!data || data === '0') {
                return [];
            }
            return ['Score: ' + data];
        },
        defaultConverter: function (data) {
            if (!data) {
                return [];
            }
            if (!_(data).isArray()) {
                return [data];
            }
            return _(data).compact();
        },
        cyrenLinksConverter: function (data) {
            return _(data).chain().values().flatten().unique().value();
        },
        apAvananFindingsConverter: function (data, allData, args) {
            if (args.returnType === 'summary') {
                return findingsConverters.defaultConverter(data);
            } else {
                return [];
            }
        },
        avCiscoIndicatorsConverter: function (data, allData, args) {
            if (!data || !data.indicators) {
                return [];
            }
            return _(data.indicators).map(function (row) {
                return row.signatures;
            });
        }
    };

    var secondRowConverters = {
        apAvananSecondRowConverter: function (module, args) {
            if (!module.scanResult || !module.scanResult.reasons || !module.scanResult.reasons.length || module.scanStatus === 'clean') {
                return null;
            }
            var data = '#' + JSON.stringify({
                    display: 'text',
                    'class': 'section-title display-block',
                    text: module.scanResult.question && module.scanResult.question.ui_title || 'Summary'
                }) + _(module.scanResult.reasons).map(function (reason) {
                    return '#' + JSON.stringify({
                            display: 'text',
                            'class': 'display-block overflow-wrap-break-word',
                            text: reason
                        });
                }).join('');
            return '#' + JSON.stringify({
                    display: 'linked-text',
                    text: data,
                    'class': 'second-row-data'
                });
        }
    };

    return function(input, args) {
        var havePendingItems = false;
        var groups = _(args.modules.data.apps_categories.categories).chain().filter(function (category) {
            return category.family === 'security';
        }).sortBy(function (category) {
            return parseInt(category.appStoreOrder);
        }).value();

        var modulesMap = {};

        _(groups).each(function (group) {
            group.modules = _(args.modules.data.apps_items.modules).filter(function (module) {
                if (module.gui_hidden && !troubleshooting.entityDebugEnabled()) {
                    return false;
                }
                if (args.entityInfo && args.entityInfo.sec_type_connected && module.sec_type && !_(args.entityInfo.sec_type_connected).contains(module.sec_type)) {
                    return false;
                }
                if (module.name === 'cp_capsule') {
                    return false;
                }
                modulesMap[module.name] = module;
                return module.category_id === group.categoryId;
            });
        });

        _(avananModules).each(function (module) {
            modulesMap[module.name] = module;
            var group = _(groups).find(function (group) {
                return module.category && module.category.type === group.type;
            });
            if (group) {
                group.modules.push(module);
            } else {
                groups.push({
                    categoryName: module.category.categoryName,
                    modules: [module]
                });
            }
        });

        if (input.data.dlp_avanan && !input.data.dlp_avanan.found_text &&
            args.dlpAvananScanResults && args.dlpAvananScanResults.data && args.dlpAvananScanResults.data.rules) {
            input.data.dlp_avanan.found_text = [];
            _(args.dlpAvananScanResults.data.rules).each(function (rule) {
                if (rule && rule.found_text) {
                    _(rule.found_text).each(function (text) {
                        input.data.dlp_avanan.found_text.push(text);
                    });
                }
            });
        }

        if (args.apAvananConfig && input.data.ap_avanan) {
            input.data.ap_avanan.uiConfig = args.apAvananConfig;
        }

        var naStatusCodes = {
            mime_type_not_supported: {
                msg: 'File type is not supported'
            },
            password_protected: {
                msg: 'Encrypted Data'
            },
            encrypted_file: {
                msg: 'Encrypted Data'
            }
        };

        _(args.stat.data).each(function (value, key) {
            if (!modulesMap[key]) {
                return;
            }

            modulesMap[key].scanStat = value;

            if (!value) {
                modulesMap[key].scanStatus = 'hide';
                return;
            }

            modulesMap[key].scanStatus = appScanStatusHelper.statisticsToStatus(value);
        });

        _(input.data).each(function (value, key) {
            var module = modulesMap[key];
            if (!module) {
                return;
            }
            module.scanResult = value;
            module.scanKeyExists = true;
            module.scanStatus = appScanStatusHelper.scanResultStatusFix(module.scanStatus, key, value);
            if (!_(appScanStatusHelper.visibleStatuses).contains(module.scanStatus) &&
                value && naStatusCodes[value.status_code]) {
                module.scanStatus = 'na';
            }
        });

        _(args.mainQueue.data.rows).each(function (row) {
            _(modulesMap).each(function (module) {
                if (row.request_type === "resolving" && module.sec_entity === row.entity_type && module.state === 'active') {
                    module.isPending = true;

                    // Changed behaviour to always show 'pending' status if file is found in queue (see AV-628). See git history in case rollback is needed
                    module.scanStatus = 'pending';
                    delete module.scanResult;
                }
            });
        });

        if (args.history) {
            _(args.history.data).each(function (value, key) {
                if (!_(appScanStatusHelper.visibleStatuses).contains(modulesMap[key].scanStatus)) {
                    modulesMap[key].scanStat = value.statistics;
                    modulesMap[key].scanResult = value;
                    modulesMap[key].scanStatus = appScanStatusHelper.statisticsToStatus(value.statistics, key, value);
                }
            });
        }

        var showStatuses = _(appScanStatusHelper.visibleStatuses).chain().map(function (status) {
            return [status, true];
        }).object().value();

        _(groups).each(function (group) {
            group.modules = _(group.modules).chain().filter(function (module) {
                if (module.scanStatus === 'found') {
                    group.haveFound = true;
                }
                return (troubleshooting.enabled() && module.scanKeyExists) || showStatuses[module.scanStatus];
            }).sortBy(function (module) {
                return parseInt(module.app_store_order);
            }).value();

            group.rows = _(group.modules).map(function (module) {
                var findings, multiFindings = [];
                var conf = args.config.data.apps[module.name];
                if (_(conf).isUndefined()) {
                    conf = args.config.data.modules[module.module_name];
                }
                if (conf && conf.findings && module.scanResult) {
                    multiFindings = _(conf.findings).chain().filter(function (column) {
                        return _(column.showIfConfig).isUndefined();
                    }).map(function (column) {
                        return (findingsConverters[column.converter] || findingsConverters.defaultConverter)(module.scanResult[column.field], module.scanResult, args);
                    }).value();
                }
                if (multiFindings.length > 0) {
                    findings = multiFindings[0];
                } else {
                    findings = [];
                    multiFindings.push(findings);
                }
                if (module.scanStatus === 'notScanned') {
                    findings.push('File was not scanned');
                }
                if (module.scanStatus === 'pending') {
                    havePendingItems = true;
                    findings.push('Pending');
                }
                if (module.scanStatus === 'na') {
                    findings.push(naStatusCodes[module.scanResult.status_code].msg);
                }
                if(module.name == 'ap_cyren' && module.scanStatus === 'found-no-click') {
                        multiFindings = [['Phishing/Spam detected']];
                        findings = multiFindings[0];
                }

                var secondRowData = null;
                if (conf && conf.secondRowConverter && secondRowConverters[conf.secondRowConverter] && module.scanResult) {
                    secondRowData = secondRowConverters[conf.secondRowConverter](module, args);

                }

                var statusTip;
                if (troubleshooting.enabled()) {
                    if (findings.length === 0) {
                        if (_(module.scanResult).isUndefined()) {
                            findings.push('No scan info exists');
                        } else {
                            if (module.scanResult && module.scanResult.status_description) {
                                findings.push(module.scanResult.status_description);
                            }
                        }
                    }
                    if (!showStatuses[module.scanStatus]) {
                        module.scanStatus = 'troubleshooting';
                    }
                    statusTip = {
                        content: '',
                        position: {
                            at: 'bottom center',
                            my: 'top right',
                            viewport: true
                        },
                        style: {
                            classes: 'qtip-bootstrap scan-status-troubleshooting-tip'
                        }
                    };
                    statusTip.content += '<div>Is Pending: <strong>' + (module.isPending ? 'Yes' : 'No') + '</strong>';

                    statusTip.content += '<div>Statistics data: ';
                    if (!_(module.scanStat).isObject()) {
                        statusTip.content += module.scanStat;
                    } else {
                        statusTip.content += '<div class="status-tip-stats">' + _(module.scanStat).map(function (value, key) {
                                return '<div class="status-tip-stats-line' + (parseInt(value) ? ' not-empty' : '') + '"><span class="stat-key">' + key +':</span> <span class="stat-value">' + value + '</span></div>';
                            }).join('') + '</div>';
                    }
                    statusTip.content += '</div>';
                }

                var row = {
                    label: module.label,
                    name: module.name,
                    moduleName: module.module_name,
                    categoryName: group.categoryName,
                    findings: multiFindings,
                    icon: module.stack_icon,
                    status: module.scanStatus,
                    isHistory: module.scanResult && module.scanResult.isHistory,
                    updateTime: module.scanResult && module.scanResult.update_time,
                    statusTip: statusTip,
                    scanEntity: module.sec_entity || module.matrix_entity,
                    reportLinks: module.scanResult && module.scanResult.pdfReportsLinks,
                    isBeta: module.is_beta,
                    isExpired: module.isExpired,
                    secondRowData: secondRowData,
                    entityId: args.entityId,
                    entityType: args.entityType
                };

                if ((troubleshooting.entityDebugEnabled() || sideMenuManager.currentUser().admin) &&
                    (module.scanStatus === 'found' ||  module.scanStatus === 'found-no-click' ||
                    (module.scanStatus === 'suspicious' && module.name === 'ap_avanan'))
                ) {
                    if (module.sec_type) {
                        var secTypeAddTitle = secToolsExceptionParams.getAddTitle(module.sec_type, args.entityType);
                        if (secTypeAddTitle) {
                            row.exceptionLinks = (row.exceptionLinks || '') + '#' + JSON.stringify({
                                    display: 'text',
                                    'class': 'display-block text-align-right link-like',
                                    text: secTypeAddTitle,
                                    clickEvent: {
                                        name: 'add-sec-tool-exception',
                                        entityType: args.entityType,
                                        entityId: args.entityId,
                                        secType: module.sec_type
                                    }
                                });
                        }
                    }
                }
                if (module.name === 'ap_avanan' &&
                    (troubleshooting.entityDebugEnabled() ||
                        sideMenuManager.currentUser().admin && (
                        module.scanStatus === 'found' ||
                        module.scanStatus === 'found-no-click' ||
                        module.scanStatus === 'suspicious')
                    )
                ) {
                    row.exceptionLinks = (row.exceptionLinks || '') +  '#' + JSON.stringify({
                            display: 'inline-list',
                            'class': 'smart-phish-links-container',
                            countText: 'Smart-Phish Exceptions',
                            data: apAvananExceptionsParams.exceptionLinks(args.entityId, args.entityType, 'add-avanan-ap-exception')
                        });
                }

                if ((module.scanStatus === 'found' ||  module.scanStatus === 'found-no-click') &&
                    module.name === 'solgate' && module.scanResult && module.scanResult.macro_hashes && module.scanResult.macro_hashes.length) {
                    row.exceptionLinks = (row.exceptionLinks || '') +  '#' + JSON.stringify({
                            display: 'text',
                            'class': 'display-block text-align-right link-like',
                            text: 'Ignore macro' + (module.scanResult.macro_hashes.length > 1 ? 's' : '') + ' found in this file',
                            clickEvent: {
                                name: 'add-solgate-exception',
                                macro_hash: module.scanResult.macro_hashes
                            }
                        });
                }


                if (troubleshooting.entityDebugEnabled() &&
                    (module.name == 'ap_cyren' || module.name == 'ap_avanan')
                ) {
                    row.downloadLinks = '#' + JSON.stringify({
                            display: 'external-link',
                            href: '/api/v1/sec_match_file/' + args.entityType + '/' + module.name + '/' + args.entityId,
                            text: 'Download email',
                            'class': 'btn btn-blue btn-in-table small-btn display-block'
                        }) + emailView.viewSourceButton(module.name, args.entityType, args.entityId)  +
                        emailView.viewRenderedButton(module.name, args.entityType, args.entityId);
                }

                return row;
            });
        });

        groups = _(groups).filter(function (group) {
            return group.modules.length;
        });

        var widgetOptions = {
            "type": "data-table",
            "options": {
                "pagesAround": 2,
                "pageSize": 10000,
                "pagination": {
                    "page": 1
                }
            },
            "class": "file-security-stack-table vertical-middle",
            "wrapper": {
                "type": "widget-panel",
                "class": "file-security-stack-category"
            },
            "columns": [{
                "id": "name",
                "hidden": true
            }, {
                "id": "moduleName",
                "hidden": true
            }, {
                "id": "label",
                "hidden": true
            }, {
                "id": "icon",
                "converter": "appIconConverter",
                "dataClass": 'file-security-app-icon',
                "converterArgs": {
                    "isBetaColumn": "isBeta",
                    "isExpiredColumn": "isExpired"
                }
            }, {
                "id": "isBeta",
                "hidden": true
            }, {
                "id": "isExpired",
                "hidden": true
            }, {
                "id": "categoryName",
                "hidden": true
            }, {
                "id": "findings",
                "converter": "multiListConverter",
                "converterArgs": {
                    "countPostfix": "activities",
                    "collapseRest": true,
                    "expanded": 4,
                    "uniqueCountText": "times",
                    "uniqueTrim": true,
                    "elementAsTooltip": true
                },
                "dataClass": "file-security-findings-column"
            }, {
                "id": "status",
                "converter": "appStatusIcon",
                "dataClass": 'file-security-status-icon'
            }, {
                "id": "isHistory",
                "hidden": true
            }, {
                "id": "updateTime",
                "hidden": true
            }, {
                "id": "statusTip",
                "hidden": true
            }, {
                "id": "scanEntity",
                "hidden": true
            }, {
                "id": "reportLinks",
                "hidden": true
            }, {
                "id": "exceptionLinks",
                "hidden": true
            }, {
                "id": "downloadLinks",
                "hidden": true
            }, {
                "id": "secondRowData",
                "hidden": true
            }, {
                "id": "entityId",
                "hidden": true
            }, {
                "id": "entityType",
                "hidden": true
            }]
        };

        if (args.returnType === 'summary') {
            var foundApps = [];
            var maxFindings = 4;
            var foundStatusExists = false;
            _(groups).each(function (group) {
                _(group.rows).each(function (row) {
                    if (row.status === 'found' || row.status === 'suspicious' || row.status === 'found-no-click') {
                        foundStatusExists = (foundStatusExists || row.status === 'found' || row.status === 'found-no-click');
                        foundApps.push(row);
                    }
                });
            });
            if (!foundApps.length) {
                return $q.when({
                    data: null
                });
            }
            return $q.when({
                data: '#' + JSON.stringify({
                    display: 'text',
                    'class': {
                        'fa': true,
                        'big-icon': true,
                        'fa-exclamation-circle': foundStatusExists,
                        'red-text': foundStatusExists,
                        'fa-info-circle': !foundStatusExists,
                        'yellow-text': !foundStatusExists

                    },
                    tooltip: {
                        content: '<table>' + _(foundApps).map(function (row) {
                            return '<tr><td>' + row.label + ': </td><td>' + _(row.findings[0]).chain()
                                    .first(maxFindings).map(function (finding) {
                                        return '<div>' + finding + '</div>';
                                    }).value().join('') +
                                (row.findings[0].length > maxFindings ? ('<div>' + (row.findings[0].length - maxFindings) + ' more</div>') : '') +
                                '</td></tr>';
                        }).join('') + '</table>',
                        style: {
                            classes: 'scan-details-summary-qtip'
                        },
                        position: {
                            at: 'top center',
                            my: 'bottom center'
                        }
                    }
                })
            });
        }
        return $q.when({
            data: {
                widgets: _(groups).map(function (group) {
                    var widgetInfo = $.extend(true, {}, widgetOptions);
                    widgetInfo.wrapper.title = group.categoryName;
                    defaultListConverter({data: {rows: group.rows}}, {
                        columns: widgetInfo.columns
                    }).then(function (response) {
                        var secondRowIdx = columnsHelper.getIdxById(widgetInfo.columns, 'secondRowData');
                        var dataColumnIdx = columnsHelper.getIdxById(widgetInfo.columns, 'findings');
                        var statusColumnIdx = columnsHelper.getIdxById(widgetInfo.columns, 'status');
                        var rowClasses = {};
                        var resultData = [];
                        var rowIdx = 0;
                        _(response.data.data).each(function (row) {
                            resultData.push(row);
                            rowIdx++;
                            if (!row[secondRowIdx] || !row[secondRowIdx].text) {
                                return;
                            }
                            rowClasses[rowIdx - 1] = {
                                'first-in-pair': true
                            };
                            var newRow = _(row).map(function () {
                                return {
                                    text: null,
                                    originalText: null
                                };
                            });
                            newRow[dataColumnIdx].text = row[secondRowIdx].text;
                            newRow[dataColumnIdx].colSpan = 2;
                            newRow[statusColumnIdx].hidden = true;
                            resultData.push(newRow);
                            rowIdx++;
                        });
                        widgetInfo.rowClasses = rowClasses;
                        widgetInfo.data = resultData;
                    });
                    return widgetInfo;
                }),
                havePendingItems: havePendingItems
            }
        });
    };
});