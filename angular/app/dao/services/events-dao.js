var m = angular.module('dao');

m.factory('eventsDao', function(http, defaultListConverter, defaultMapConverter, $q, sqlQueries, arrayValueConverter, mimeIcons, feature, path) {
    var eventTags = {};
    function retrieveEvents(after, userId, ipId) {
        return http.get(sqlQueries('user_events')).params({
            after: after,
            user_id: userId,
            ip_id: _(ipId).isUndefined() ? undefined : ipId + '/%%'
        }).showTotalRows(false).cache('eventsDao.retrieveEvents-' + after + '-' + (userId || ''), 5000);
    }
    return {
        retrieve: function(columns, after, userId, ipId) {
            if(feature.isShowAnomalyTable() && _(userId).isUndefined() && _(ipId).isUndefined()) {
                return http.get(sqlQueries('user_events_demo0911'))
                    .showTotalRows(false).converter(defaultListConverter, function() {
                        return {
                            columns: columns,
                            converters: {
                                severityConverter: function(text) {
                                    return '#' + JSON.stringify({
                                        display: 'text',
                                        'class': 'alerts-tag ' + text + '-severity-tag',
                                        text: text
                                    });
                                }
                            }
                        };
                    });
            }
            return retrieveEvents(after, userId, ipId)
                .preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function(preFetches) {
                    return {
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns,
                        converters: {
                            boxFileFolderLinkConverter: function(text, columnId, columnsMap) {
                                if(columnsMap.source_type === 'file') {
                                    return '#' + JSON.stringify({
                                        display: 'link',
                                        type: 'file',
                                        id: columnsMap.source_id_file,
                                        text: columnsMap.file_name
                                    });
                                } else if(columnsMap.source_type === 'folder') {
                                    return '#' + JSON.stringify({
                                        display: 'link',
                                        type: 'folder',
                                        id: columnsMap.source_id_folder,
                                        text: columnsMap.folder_name
                                    });
                                }
                                return '';
                            },
                            boxSourceLinkConverter: function(text, columnId, columnsMap, args) {
                                if(columnsMap.source_type === 'file') {
                                    return '#' + JSON.stringify({
                                            display: 'link',
                                            type: 'file',
                                            id: columnsMap.source_id_file,
                                            text: columnsMap.file_name
                                        });
                                } else if(columnsMap.source_type === 'folder') {
                                    return '#' + JSON.stringify({
                                            display: 'link',
                                            type: 'folder',
                                            id: columnsMap.source_id_folder,
                                            text: columnsMap.folder_name
                                        });
                                }
                                if (columnsMap.source_type === 'user') {
                                    var result = '';
                                    if(!_(args.isExternalColumn).isUndefined() && columnsMap[args.isExternalColumn]) {
                                        result += '#' + JSON.stringify({
                                                display: 'img',
                                                path: path('common').img('external-user.png'),
                                                tooltip: 'External user',
                                                'class': 'icon-16px display-inline-block'
                                            });
                                    }
                                    if(!_(args.suspendedColumn).isUndefined() && columnsMap[args.suspendedColumn]) {
                                        result += '#' + JSON.stringify({
                                                display: 'text',
                                                text: 'suspended',
                                                'class': 'user-suspended'
                                            });
                                    }
                                    result += '#' + JSON.stringify({
                                            display: 'link',
                                            'class': 'user-name display-block',
                                            type: 'user',
                                            id: columnsMap.source_id_user,
                                            text: columnsMap.source_user_name || 'Anonymous'
                                        }) + '#' + JSON.stringify({
                                            display: 'text',
                                            'class': 'user-email',
                                            text: columnsMap.source_user_login
                                        });
                                    return result;
                                }
                                return '';
                            },
                            boxEventSourceIconConverter: function (text, columnId, columnsMap, args) {
                                if (columnsMap[args.sourceTypeColumn] === 'file') {
                                    return '#' + JSON.stringify({
                                            display: 'link',
                                            type: 'file',
                                            id: columnsMap.source_id_file,
                                            linkedText: '#' + JSON.stringify({
                                                display: 'img',
                                                path: preFetches.mimeIcons(columnsMap[args.mimeTypeColumn]),
                                                'class': 'row-icon',
                                                tooltip: columnsMap[args.niceMimeColumn]
                                            })
                                        });
                                }
                                if (columnsMap[args.sourceTypeColumn] === 'folder') {
                                    return '#' + JSON.stringify({
                                            display: 'link',
                                            type: 'folder',
                                            id: columnsMap.source_id_folder,
                                            linkedText: '#' + JSON.stringify({
                                                display: 'img',
                                                path: preFetches.mimeIcons(columnsMap[args.mimeTypeColumn]),
                                                'class': 'row-icon',
                                                tooltip: columnsMap[args.niceMimeColumn]
                                            })
                                        });
                                }

                                if (columnsMap[args.sourceTypeColumn] === 'user') {
                                    return '#' + JSON.stringify({
                                            display: 'img',
                                            'class': 'row-icon',
                                            path: columnsMap[args.userImageColumn],
                                            defaultPath: path('profiles').img('user.png')
                                        });
                                }
                                return '';
                            }
                        }
                    };
                });
        },
        retrieveMap: function(fields, after, userId) {
            return retrieveEvents(after, userId).converter(defaultMapConverter, {
                fields: fields
            });
        },
        retrieveTypes: function(params) {
            if(feature.isShowAnomalyTable()) {
                return http.get(sqlQueries('event_types_demo0911')).params(params).converter(arrayValueConverter, {
                    name: 'name'
                });
            }
            return http.get(sqlQueries('event_types')).params(params).converter(arrayValueConverter, {
                name: 'name'
            });
        },
        retrieveTags: function() {
            return $q.when({
                data: eventTags
            });
        },
        retrieveDelta: function (columns, after) {
            return http.get(sqlQueries('delta_events')).params({
                after: after
            }).showTotalRows(false)
                .converter(defaultListConverter, function() {
                    return {
                        columns: columns
                    };
                });
        }
    };
});