var m = angular.module('dao');

m.factory('friendlyDomainsDao', function($q, http, arrayValueConverter) {
    var friendlyDomainsDao = {
        list: function() {
            return http.get('/api/v1/friendly_domain').converter(arrayValueConverter, {
                name: 'domain'
            });
        },
        create: function(domain) {
            return http.post('/api/v1/friendly_domain').body({
                domain: domain
            });
        },
        remove: function(domainId) {
            return http.delete('/api/v1/friendly_domain/' + domainId);
        },
        update: function(domains, remove) {
            return http.get('/api/v1/friendly_domain').then(function(response) {
                var existDomains = _(response.data.rows).chain().map(function(row) {
                    return [row.domain, row.id];
                }).object().value();
                var newDomains = _.without.apply(_, [domains].concat(_(existDomains).keys()));
                var removeDomains = [];
                if(remove) {
                    removeDomains = _.without.apply(_, [_(existDomains).keys()].concat(domains));
                }
                return $q.all([
                    $q.all(_(newDomains).map(function(domain) {
                        return friendlyDomainsDao.create(domain);
                    })),
                    $q.all(_(removeDomains).map(function(domain) {
                        return friendlyDomainsDao.remove(existDomains[domain]);
                    }))
                ]);
            });
        }
    };
    return friendlyDomainsDao;
});