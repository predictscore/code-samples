var m = angular.module('dao');

m.service('advancedDataSearchFacetsConverter', function($q) {
    return function(input) {
        var result = [];

        _(input.data.rows).each(function(row) {
            result.push({
                id: row.id,
                options: _(row.ids).map(function(id, idx) {
                    return {
                        id: id,
                        label: row.labels[idx],
                        count: row.counts[idx]
                    };
                })
            });
        });

        input.data = result;

        return $q.when(input);
    };
});