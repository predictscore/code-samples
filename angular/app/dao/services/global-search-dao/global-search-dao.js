var m = angular.module('dao');

m.factory('globalSearchDao', function(http, defaultListConverter, mimeIcons, $q, sqlQueries, advancedDataSearchFacetsConverter, modulesManager, driveQueryConverter) {
    return {
        search: function(columns, keyword, categoryLimit, saas, query) {
            return http.get(sqlQueries('global_search' + (query ? ('_' + query) : ''), null, saas)).params({
                keyword: keyword.split('%').join('\\%%'),
                category_limit: categoryLimit
            }).preFetch(mimeIcons(), 'mimeIcons')
                .dynamicConverter(function(preFetches) {
                    return function (response) {
                        var result = {};
                        _(response.data.rows).each(function (row) {
                            row.saas = saas;
                            row.saasIcon = saas;
                            result[row.type] = null;
                        });
                        _(result).each(function (value, key) {
                            var tableResponse = $.extend({}, response, {
                                data: {
                                    rows: _(response.data.rows).filter(function (row) {
                                        return row.type == key;
                                    })
                                }
                            });
                            result[key] = defaultListConverter(tableResponse, {
                                mimeIcons: preFetches.mimeIcons,
                                columns: columns[key]
                            });
                        });
                        return $q.when(result);
                    };
                });
        },
        advancedDataSearch: function(columns, params) {
            return http.get(sqlQueries('viewer/advanced_data_search')).params(params)
                .returnRawFirst(true)
                .preFetch(mimeIcons(), 'mimeIcons')
                .converter(defaultListConverter, function(preFetches) {
                    return {
                        mimeIcons: preFetches.mimeIcons,
                        columns: columns
                    };
                });
        },
        advancedDataSearchEmailExport: function (params, format) {
            return http.get(sqlQueries('viewer/advanced_data_search_export', format)).params(params);
        },
        advancedDataSearchDynamicFacets: function(name, filter, ids) {
            return http.get(sqlQueries('viewer/' + name)).params({
                filter: filter,
                ids: ids
            });
        },
        advancedDataSearchFacets: function(facets, params, filter, timeout) {
            var empty = !filter && _(params).chain().find(function (param) {
                return param;
            }).isUndefined().value() ;
            var query = empty ? 'viewer/advanced_data_search_facets_cached' : 'viewer/advanced_data_search_facets';
            return http.get(sqlQueries(query)).params($.extend({
                facets: facets
            }, params))
                .timeout(timeout)
                .filter(filter)
                .converter(advancedDataSearchFacetsConverter);
        },
        advancedDataSearchAllFacets: function() {
            return http.get(sqlQueries('viewer/advanced_data_search_facets_cached'))
                .converter(advancedDataSearchFacetsConverter);
        },
        getSavedFilters: function () {
            localStorage.removeItem('advanced_search_filters');
            return http.get(sqlQueries('drive_query')).params({
                saas: modulesManager.currentModule()
            }).converter(driveQueryConverter);
        },
        createFilter: function (name, filter, desc) {
            return http.post('/api/v1/drive_query').body({
                query_name: name,
                query_data: filter,
                query_desc: desc || '',
                saas: modulesManager.currentModule()
            }).converter(driveQueryConverter).run();
        },
        saveFilter: function (id, name, filter, desc) {
            return http.put('/api/v1/drive_query/' + id).body({
                query_name: name,
                query_data: filter,
                query_desc: desc || '',
                saas: modulesManager.currentModule()
            }).converter(driveQueryConverter).run();
        },

        removeSavedFilter: function (id) {
            return http.delete('/api/v1/drive_query/' + id).run();
        }

    };
});