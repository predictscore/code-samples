var m = angular.module('dao');

m.service('driveQueryConverter', function($q) {
    return function(input) {
        var result = [];

        var rows = input.data.rows;
        if (_(rows).isUndefined() && input.data.id) {
            rows = [input.data];
        }

        _(rows).each(function(row) {
            result.push({
                id: row.id,
                queryName: row.query_name,
                queryDesc: row.query_desc,
                queryData: row.query_data,
                created: row.created,
                updated: row.updated,
                saas: row.saas,
                lastrun: row.lastrun
            });
        });

        input.data = result;

        return $q.when(input);
    };
});