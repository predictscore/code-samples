var m = angular.module('dao');

m.factory('apAvananExceptionsDao', function(http, sqlQueries, defaultListConverter) {
    return {
        list: function (type, columns, userMap) {
            return http.get(sqlQueries('ap_avanan_exceptions_' + type))
                .params({
                    type: type
                })
                .converter(defaultListConverter, {
                    columns: columns,
                    converters: {
                        avananUserInfoConverter: function (text, columnId, columnsMap, args, allConverters) {
                            var userInfo = userMap[text] || {};
                            return allConverters.userWithEmail(userInfo.name, columnId, userInfo, {
                                emailColumn: 'email',
                                userIdColumn: 'noIdNeeded'
                            });
                        }
                    }
                });
        },
        add: function (data) {
            return http.post('/api/v1/ap_avanan/exceptions').body(data).run();
        },
        remove: function (data) {
            return http.post('/api/v1/ap_avanan/exceptions/delete').body(data).run();
        }


    };
});