var m = angular.module('dao');

m.factory('avananUserDao', function($q, http, defaultListConverter, slowdownHelper, sqlQueries) {
    return {
        list: function(page, pageSize, columns) {
            return http.get('/api/v1/webuser')
                .preFetch(http.get(sqlQueries('last_webuser_logins')), "lastLogins")
                .converter(function (input, args) {
                    var lastLogins = _(args.lastLogins.data.rows).chain().map(function (row) {
                        return [row.id, row];
                    }).object().value();
                    _(input.data.rows).each(function (row) {
                        row.last_login_time = (lastLogins[row.id] || {}).last_login_time;
                    });
                    return $q.when(input);
                }, _.identity)
                .converter(defaultListConverter, {
                    columns: columns,
                    converters: {
                        authLockedConverter: function (text, columnId, columnsMap, args) {
                            if (text) {
                                var attributes = _(args.attributes).chain().map(function (attribute, name) {
                                    return [name, columnsMap[attribute]];
                                }).object().value();
                                return '#' + JSON.stringify({
                                        display: 'text',
                                        'class': 'fa fa-ban red-text cursor-pointer fa-2x',
                                        clickEvent: {
                                            name: args.eventName,
                                            attributes: attributes
                                        },
                                        tooltip: 'User ability to login with password was blocked due to many failed attempts. Click here to un-block'
                                    });

                            }
                        }
                    }
                });
        },
        retrieve: function(id) {
            return http.get('/api/v1/webuser/' + id);
        },
        create: function(data) {
            return http.post('/api/v1/webuser').body(data).run();
        },
        update: function(id, data) {
            return http.put('/api/v1/webuser/' + id).body(data).run();
        },
        remove: function(id) {
            return http.delete('/api/v1/webuser/' + id).run();
        },
        auth: function(email, password) {
            return http.post('/api/v1/webuser/login').body({
                email: email,
                current_password: password
            });
        },
        logout: function() {
            console.log('Logging out user');
            return http.put('/api/v1/current_user/logout').run();
        },
        current: function() {
            return http.get('/api/v1/current_user').converter(slowdownHelper.slowdownConverter);
        },
        currentProperties: function() {
            return http.get('/api/v1/current_user/properties');  
        },
        updateCurrentProperties: function(params) {
            return http.put('/api/v1/current_user/properties').body(params).run();
        },
        login: function(email, password) {
            return http.post('/api/v1/current_user/login').body({
                email: email,
                current_password: password
            });
        },
        forgot: function(email) {
            return http.put('/api/v1/current_user/forgot_password').body({
                email: email
            });
        },
        reset: function(token, newPassword) {
            return http.put('/api/v1/current_user/reset_password').body({
                token: token,
                new_password: newPassword
            });
        },
        changePassword: function(oldPassword, newPassword) {
            return http.put('/api/v1/current_user/change_password').body({
                current_password: oldPassword,
                new_password: newPassword
            });
        },
        count: function() {
            return http.get('/api/v1/webuser')
                .converter(function (input) {
                    return $q.when($.extend({}, input, {
                        data: input.data.rows.length
                    }));
                });
        },
        users: function() {
            return http.get('/api/v1/webuser');
        },
        authUnlock: function(id) {
            return http.post('/api/v1/webuser/' + id + '/auth_lock').body({
                is_locked: false
            }).run();
        }
    };
});