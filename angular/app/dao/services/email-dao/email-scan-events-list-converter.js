var m = angular.module('dao');

m.factory('emailScanEventsListConverter', function($q, fileDao, appScanStatusHelper, path, appIconHelper) {
    return function(input, args) {
        var deferred = $q.defer();
        var promises = [];
        var events = [];

        promises.push(fileDao.fileScanDetails(args.entityId, args.entityType)
            .then(function (response) {
                _(response.data).each(function (event, appName) {
                    var eventRow = {
                        name :'Avanan Service',
                        entity_id: null,
                        entity_type: args.entityType,
                        time: event.update_time,
                        file_name: 'Email Body',

                        description: 'Inspected by ' +
                        appIconHelper.appIconByApp(event.found, true) + '#' + JSON.stringify({
                            display: 'text',
                            'class': $.extend({
                                'float-right': true
                            }, appScanStatusHelper.statisticsToClass(event.statistics, event.found.module_name, event)),
                            clickEvent: {
                                name: 'open-app-status',
                                appName: appName,
                                moduleName: event.found.module_name,
                                scanStatus: appScanStatusHelper.statisticsToStatus(event.statistics, event.found.module_name, event),
                                entityId: args.entityId,
                                entityType: args.entityType,
                                scanEntity: event.found.sec_entity || event.found.matrix_entity,
                                appLabel: event.found.label
                            },
                            tooltip: appScanStatusHelper.eventViewTooltip(event.statistics, event.found.module_name, event)
                        })
                    };
                    events.push(eventRow);
                });
            }));

        _(input.data.rows).each(function (row) {
            promises.push(fileDao.fileScanDetails(row.entity_id, args.itemsEntityType)
                .then(function (response) {
                    _(response.data).each(function (event, appName) {
                        var eventRow = {
                            name :'Avanan Service',
                            entity_id: row.entity_id,
                            entity_type: args.itemsEntityType,
                            time: event.update_time,
                            file_name: row[args.itemNameColumn],
                            description: 'Inspected by ' + appIconHelper.appIconByApp(event.found, true) +'#' + JSON.stringify({
                                display: 'text',
                                'class': $.extend({
                                    'float-right': true
                                }, appScanStatusHelper.statisticsToClass(event.statistics, event.found.module_name, event)),
                                clickEvent: {
                                    name: 'open-app-status',
                                    appName: appName,
                                    moduleName: event.found.module_name,
                                    scanStatus: appScanStatusHelper.statisticsToStatus(event.statistics, event.found.module_name, event),
                                    entityId: row.entity_id,
                                    entityType: args.itemsEntityType,
                                    scanEntity: event.found.sec_entity || event.found.matrix_entity,
                                    appLabel: event.found.label
                                },
                                tooltip: appScanStatusHelper.eventViewTooltip(event.statistics, event.found.module_name, event)
                            })
                        };
                        events.push(eventRow);
                    });
                }));
        });
        $q.all(promises).then(function () {
            events = _(events).sortBy(function (event) {
                return 0-moment(event.time);
            });
            deferred.resolve($.extend(input, {
                data: {
                    rows: events
                }
            }));
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    };
});