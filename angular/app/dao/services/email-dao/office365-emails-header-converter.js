var m = angular.module('dao');

m.factory('office365EmailsHeaderConverter', function($q) {
    return function(input) {
        return $q.when({
            data: _(input.data).map(function(value, key) {
                return {
                    key: key,
                    value: _(value).isString() ? value.split('\r') : ['' + value]
                };
            })
        });  
    };
});