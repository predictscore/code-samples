var m = angular.module('dao');

m.factory('attachmentsListConverter', function($q, fileDao) {
    return function(input, args) {
        var deferred = $q.defer();
        var promises = [];
        _(input.data.rows).each(function (row) {
            promises.push(fileDao.allScanDetails(row.entity_id, args.entityType, 'summary')
                .then(function (response) {
                    row.scanStatus = response.data;
                }));
        });
        $q.all(promises).then(function () {
            deferred.resolve(input);
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    };
});