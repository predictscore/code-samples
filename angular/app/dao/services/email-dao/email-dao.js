var m = angular.module('dao');

m.factory('emailDao', function(http, singleRowConverter, sqlQueries, defaultListConverter, mimeIcons, attachmentsListConverter, emailScanEventsListConverter, errorStatusHolder, $q, modulesManager, office365EmailsHeaderConverter, googleMailHeaderConverter, troubleshooting, servicenowHeaderConverter, $timeout, slowdownHelper) {
    return {
        retrieve: function(emailId) {
            return http.get(sqlQueries('entity/email')).params({
                    entity_id: emailId
                }).converter(singleRowConverter)
                .cache('emailDao.retrieve-' + emailId, 5000)
                .onRequestFail(function (error) {
                    if (error.status === 404) {
                        errorStatusHolder.errorStatus(404);
                    }
                    return $q.reject(error);
                });
        },
        recipientsList: function(columns, emailId, groupByType) {
            var result = http.get(sqlQueries('email_recipients')).params({
                email_id: emailId
            });
            if (groupByType) {
                result.converter(function (input) {
                    var typesMap = {};
                    _(input.data.rows).each(function (row) {
                        typesMap[row.type] = typesMap[row.type] || {
                                type: row.type,
                                type_order: row.type_order,
                                users: []
                            };
                        typesMap[row.type].users.push(row);
                    });
                    return $q.when({
                        data: {
                            rows: _(typesMap).chain().map(function (data) {
                                return {
                                    type: data.type,
                                    type_order: data.type_order,
                                    users: data.users
                                };
                            }).sortBy('type_order').value()
                        }
                    });
                });
            }
            return result.converter(defaultListConverter, {
                columns: columns,
                converters: {
                    "inlineUserWithEmail": function (text, columnId, columnsMap, args) {
                        var result = text;
                        if (args.emailColumn && columnsMap[args.emailColumn]) {
                            result += ' (' + columnsMap.mail + ')';
                        }
                        if (args.userIdColumn && columnsMap[args.userIdColumn]) {
                            result = '#' + JSON.stringify({
                                display: 'link',
                                type: 'user',
                                id: columnsMap[args.userIdColumn],
                                text: result
                            });
                        }
                        if (args.addAllEmailsLink && troubleshooting.entityDebugEnabled() && columnsMap.type === 'From' && args.userIdColumn && columnsMap[args.userIdColumn]) {
                            result += '#' + JSON.stringify({
                                    display: 'text',
                                    text: 'Show all conversations',
                                    'class': 'link-like',
                                    clickEvent: {
                                        name: 'show-all-user-emails',
                                        userId: columnsMap[args.userIdColumn]
                                    }
                                });
                        }
                        return result;
                    }
                }
            });
        },
        attachmentsList: function(columns, emailId, entityType) {
            return http.get(sqlQueries('email_attachments')).params({
                    email_id: emailId
                }).preFetch(mimeIcons(), 'mimeIcons')
                .converter(attachmentsListConverter, function () {
                    return {
                        entityType: entityType
                    };
                });
        },
        conversationList: function(columns, emailId) {
            return http.get(sqlQueries('email_conversation')).params({
                email_id: emailId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        scanEvents: function (columns, emailId, options) {
            return http.get(sqlQueries('email_attachments')).params({
                email_id: emailId
            }).converter(emailScanEventsListConverter, function (preFetches) {
                return $.extend({
                    entityId: emailId
                }, options);
            }).converter(defaultListConverter, function(preFetches) {
                return {
                    columns: columns
                };
            });

        },
        topOutgoingDomains: function (after, internal) {
            return http.get(sqlQueries('top_outgoing_domains')).params({
                after: after,
                internal: internal
            });
        },
        retrieveHeader: function(entityId) {
            if(modulesManager.currentModule() == 'google_mail') {
                return http.get(sqlQueries('email_headers')).params({
                    email_id: entityId
                }).converter(googleMailHeaderConverter);
            } else if (modulesManager.currentModule() == 'office365_emails') {
                return http.get('/api/v1/entity/office365_emails_header/' + entityId).converter(office365EmailsHeaderConverter);
            } else if (modulesManager.currentModule() == 'servicenow') {
                return http.get(sqlQueries('entity/email')).params({
                    entity_id: entityId
                }).converter(servicenowHeaderConverter);
            }
            return $q.reject({
                status: 404
            });
        },
        quarantine: function(columns) {
            return http.get(sqlQueries('quarantine_emails'))
                .converter(defaultListConverter, function (preFetches) {
                    return {
                        columns: columns
                    };
                });
        },
        retrieveByQuarantineUuid: function (msg) {
            return http.get(sqlQueries('email_by_quarantine_uuid')).params({
                msg: msg
            }).converter(singleRowConverter);
        },
        allUserEmails: function (columns, userId) {
            return http.get(sqlQueries('user_emails')).params({
                user_id: userId
            }).converter(defaultListConverter, function (preFetches) {
                return {
                    columns: columns
                };
            });
        },
        emailSource: function (moduleName, entityType, entityId) {
            return http.get('/api/v1/sec_match_file/' + entityType + '/' + moduleName + '/' + entityId);
        },
        similarEmailCount: function (entityType, entityId, mode, timeoutPromise) {
            function requestSimilar() {
                return http.get('/api/v1/similar_email_count/' + entityType + '/' + entityId).params({
                    mode: mode
                });
            }

            var reloadOnErrorTimeout = 10000;

            function queryReloader(getQuery, timeoutPromise) {
                return getQuery().timeout(timeoutPromise).onRequestFail(function (error) {
                    if (error.status === 502 || error.status === 504) {
                        return $timeout(slowdownHelper.adjustInterval(reloadOnErrorTimeout)).then(function () {
                            return queryReloader(getQuery, timeoutPromise);
                        });
                    }
                    return $q.when({
                        data: {
                            count: 0
                        }
                    });
                });
            }
            return queryReloader(requestSimilar, timeoutPromise);
        }
    };
});
