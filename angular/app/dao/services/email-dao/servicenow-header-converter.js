var m = angular.module('dao');

m.factory('servicenowHeaderConverter', function($q) {
    function noHeaders() {
        return $q.reject({
            status: 404
        });
    }

    return function(input) {
        if(!input.data.rows.length || !input.data.rows[0].headers) {
            return noHeaders();
        }
        var headers = _(input.data.rows[0].headers.split('\n')).compact();
        if (!headers.length) {
            return noHeaders();
        }
        return $q.when({
            data: _(headers).map(function(row) {
                var parts = row.split(':');
                return {
                    key: _(parts).first(),
                    value: _(parts).rest().join(':')
                };
            })
        });
    };
});