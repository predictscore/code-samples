var m = angular.module('dao');

m.factory('googleMailHeaderConverter', function($q) {
    return function(input) {
        if(!input.data.rows.length) {
            return $q.reject({
                status: 404
            });
        }
        return $q.when({
            data: _(input.data.rows).map(function(row) {
                return {
                    key: row.name,
                    value: row.value
                };
            })
        });
    };
});