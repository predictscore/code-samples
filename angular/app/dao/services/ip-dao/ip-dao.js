var m = angular.module('dao');

m.factory('ipDao', function(http, defaultListConverter, sqlQueries, singleRowConverter, usersWithIpPerDayConverter, worldAccessMapConverter, $q) {
    return {
        retrieve: function (ipId) {
            return http.get(sqlQueries('entity/geo')).params({
                entity_id: ipId
            }).converter(singleRowConverter);
        },
        usersWithIp: function(columns, ipId) {
            return http.get(sqlQueries('user_with_ip')).params({
                ip_id: ipId
            }).converter(defaultListConverter, {
                columns: columns
            });
        },
        usersWithIpPerDay: function(ipId, after, timeColumnName) {
            return http.get(sqlQueries('user_with_ip_per_day')).params({
                ip_id: ipId,
                after: after
            }).converter(usersWithIpPerDayConverter, {
                after: after,
                timeColumnName: timeColumnName
            });
        },
        worldAccess: function(days, ids) {
            ids = ids || {};
            return http.get(sqlQueries('ip_events')).params({
               days: days,
               file_id: ids.fileId,
               folder_id: ids.folderId,
               user_id: ids.userId,
               group_id: ids.groupId
            }).converter(worldAccessMapConverter);
        },
        heatMap: function (days) {
            return http.get(sqlQueries('heat_map')).params({
                days: days
            });
        },
        heatMapRes: function (saas, resolution) {
            return http.get(sqlQueries('heat_map_res', null, saas)).params({
                resolution: resolution
            });
        }
    };
});