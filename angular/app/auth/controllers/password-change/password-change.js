var m = angular.module('auth');

m.controller('PasswordChangeController', function($scope, modals, avananUserDao, routeHelper) {

    modals.params([{
        lastPage: true,
        params: [{
            type: 'message',
            message: 'Password security policy requires regular password change. Your current password is too old. Please change it',
            paramClass: 'full-width-param'
        }, {
            'id': 'current_password',
            'type': 'password',
            'label': 'Current Password',
            'validation': {
                required: {
                    'message': 'Please enter current password',
                    value: true
                }
            }
        }, {
            'id': 'new_password',
            'type': 'password',
            'label': 'New Password',
            'validation': {
                required: {
                    'message': 'New password can not be empty',
                    value: true
                }
            }
        }, {
            'id': 're_password',
            'type': 'password',
            'label': 'Re-type New Password',
            'validation': {
                equalTo: {
                    'message': 'Re-entered password doesn\'t match',
                    value: 'new_password'
                }
            }
        }]
    }], 'Change password', undefined, {
        onCloseAction: function (data) {
            return avananUserDao.changePassword(data.current_password, data.new_password).then(function() {
            }, function(error) {
                modals.alert({
                        'wrong password': 'Current password is wrong'
                    }[error.data && error.data.message] || (error.data && error.data.message) || 'Password change failed', {
                    title: 'Error'
                });
            });
        },
        closeOnNavigate: true
    }).then(function (data) {
        return routeHelper.redirectTo('dashboard');
    }, function (data) {
        if (data === 'routeChanged') {
            return;
        }
        return avananUserDao.logout().then(function () {
            routeHelper.redirectTo('auth', {
                page: 'login'
            });
        });
    });
});