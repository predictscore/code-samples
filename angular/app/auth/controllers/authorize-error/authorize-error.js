var m = angular.module('auth');

m.controller('AuthorizeErrorController', function($scope, path, routeHelper, $routeParams, $location, modulesDao, authorizationLinkHelper) {
    $scope.path = path;
    var root = path('auth').ctrl('authorize-error');

    var params = $location.search();

    function boldify(str) {
        return '#' + JSON.stringify({
                display: 'text',
                'class': 'bold-text',
                text: str
            });
    }

    $scope.panelTitle = 'Service Authorization Error';

    var boxLogoutButton = {
        url: 'https://app.box.com/logout',
        target: '_blank',
        text: 'Logout from your Box account'
    };

    var retryButton = {
        text: 'Re-try authorization'
    };

    function addRetryButton(saas) {
        delete retryButton.url;
        delete retryButton.onClick;
        modulesDao.getAppInfo(saas).then(function (data) {
            if (data.data) {
                retryButton.onClick = function () {
                    authorizationLinkHelper.openLink(data.data, true, true);
                };
                $scope.buttons.push(retryButton);
            }
        });
    }

    function addEgnyteLogoutButton() {
        modulesDao.retrieveConfig('egnyte').then(function (config) {
            if (config.data && config.data.egnyte_host && config.data.egnyte_host.value) {
                $scope.buttons.push({
                    url: 'https://' + config.data.egnyte_host.value +'.egnyte.com/logout.do',
                    target: '_blank',
                    text: 'Logout from your Egnyte account'
                });
            }
        });
    }





    function processParams() {
        var params = $location.search();
        $scope.message = '';
        $scope.buttons = [];

        if (params.saas === 'box' && params.error === 'not_enterprise' && params.email) {
            $scope.message = 'Box account for user ' + boldify(params.email) + ' is not enterprise account and can not be used with Avanan services';
            $scope.buttons.push(boxLogoutButton);
            addRetryButton(params.saas);
            return;
        }

        if (params.saas === 'box' && params.error === 'bad_role' && params.email) {
            $scope.message = 'You are trying to authorize Avanan App at your Box account as user ' + boldify(params.email) + '. ' +
                'Your current role ' + boldify(params.role) + ' doesn\'t allow Avanan App authorization. Only user with role \'admin\' can authorize Avanan App.';
            $scope.buttons.push(boxLogoutButton);
            addRetryButton(params.saas);
            return;
        }

        if (params.saas === 'google_drive' && params.error === 'not_admin') {
            $scope.message = 'Admin API Access is disabled. Follow these steps to enable it:';
            $scope.message += '#' + JSON.stringify({
                    display: 'text',
                    'class': 'google-step',
                    text: '1. Login to Google Admin Console'
                });
            $scope.message += '#' + JSON.stringify({
                    display: 'img',
                    'class': 'google-step-img',
                    path: root.img('google-enable-admin-api-step-1.png')
                });
            $scope.message += '#' + JSON.stringify({
                    display: 'text',
                    'class': 'google-step',
                    text: '2. Under Security - Open the "API Reference" tab'
                });
            $scope.message += '#' + JSON.stringify({
                    display: 'img',
                    'class': 'google-step-img',
                    path: root.img('google-enable-admin-api-step-2.png')
                });
            $scope.message += '#' + JSON.stringify({
                    display: 'linked-text',
                    'class': 'google-step',
                    text: '3. Turn' + boldify('on') + 'the "Enable API access" checkbox'
                });
            $scope.message += '#' + JSON.stringify({
                    display: 'img',
                    'class': 'google-step-img',
                    path: root.img('google-enable-admin-api-step-3.png')
                });
            $scope.message += '#' + JSON.stringify({
                    display: 'text',
                    'class': 'google-step',
                    text: '4. Authorize Google in Avanan App Store'
                });
            return;
        }
        
        if (params.success === 'true') {
            $scope.message = 'Service successfully authorized';
            $scope.panelTitle = 'Successful Service Authorization';
            return;
        }

        if (params.saas === 'egnyte' && params.code === 'bad_role') {
            var msg = 'You are trying to authorize Avanan App at your Egnyte account';
            if (params.email || param.username) {
                msg += ' as user ' + boldify(params.email || params.username) + '. ';
            }
            msg += 'Your current role ';
            if (params.role) {
                msg += boldify(params.role) + ' ';
            }
            msg += 'doesn\'t allow Avanan App authorization.';
            if (params.expected_role) {
                msg += ' Only user with role \'' +params.expected_role + '\' can authorize Avanan App.';
            }
            $scope.message = msg;

            addEgnyteLogoutButton();
            addRetryButton(params.saas);
            return;
        }



        $scope.message = 'Authorization error. Please retry later or contact support.';
        var errorDetails = '';
        _(params).each(function (value, key) {
            errorDetails += '#' + JSON.stringify({
                    display: 'linked-text',
                    'class': 'display-block',
                    text: '#' + JSON.stringify({
                        display: 'text',
                        'class': 'error-details-key',
                        text: key + ': '
                    }) + '#' + JSON.stringify({
                        display: 'text',
                        'class': 'error-details-value',
                        text: value
                    })
                });
        });
        if (errorDetails) {
            $scope.message += '#' + JSON.stringify({
                    display: 'linked-text',
                    'class': 'error-details-outer',
                    text: '#' + JSON.stringify({
                        display: 'text',
                        'class': 'display-block',
                        text: 'Error details:'
                    }) + '#' + JSON.stringify({
                        display: 'linked-text',
                        'class': 'error-details',
                        text: errorDetails
                    })
                });
        }
    }

    processParams();


});