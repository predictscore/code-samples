var m = angular.module('auth');

m.factory('trialExpiredModal', function(modals) {
    return {
        show: function() {
            return  modals.alert('Dear User, Your Avanan trial has expired. Please contact your Avanan account manager.', {
                title: 'Trial Expired',
                disableCloseX: true,
                backdrop: 'static',
                keyboard: false
            });

        }
    };
});