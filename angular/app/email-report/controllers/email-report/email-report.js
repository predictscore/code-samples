var m = angular.module('email-report');

m.controller('EmailReportController', function($scope, $routeParams, widgetPolling, path, sizeClasses) {
    var reportId = $routeParams.reportId;
    widgetPolling.updateWidgets(
        path('email-report').ctrl('email-report').json(reportId).path
    ).then(function() {
        console.log('Report rendered');
    });
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
});
