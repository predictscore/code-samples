var m = angular.module('email-report');

m.factory('reportTableLogic', function(emailReportDao, tablePaginationHelper) {
    return function ($scope, logic, reinitLogic) {
        var options = {
            columns: $scope.model.columns
        };
        
        tablePaginationHelper($scope, $scope.model.options, options, function() {
            return emailReportDao.table(options.columns, logic.url, logic.params);
        }, function(tableModel) {
            if(!_(tableModel).isNull()) {
                $scope.model.data = tableModel.data;
                logic.resolver.resolve();
            }
        });
    };
});