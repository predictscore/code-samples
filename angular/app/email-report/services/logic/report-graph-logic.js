var m = angular.module('email-report');

m.factory('reportGraphLogic', function($q, emailReportDao, uuid) {
    return function ($scope, logic, reinitLogic) {
        $scope.model.top = 10;
        $scope.model.keyColumn = 'any';
        $scope.model.valueColumn = 'any';
        
        $scope.model.options.editor = false;
        $scope.model.options.hideControls = true;
        $scope.model.options.refreshEvent = 'refresh-report-' + uuid.random();
        $scope.model.onFlotRender = function() {
            logic.resolver.resolve();
            delete $scope.model.onFlotRender;
        };
        $scope.model.options.request = function() {
            return emailReportDao.chart(logic.url, logic.params);
        };
    };
});