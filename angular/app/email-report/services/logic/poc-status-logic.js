var m = angular.module('email-report');

m.factory('pocStatusLogic', function (configDao) {
    return function ($scope, logic, reinitLogic) {
        $scope.model.lines = [];
        configDao.getTrialExpirationDays().then(function(days) {
            if(!_(days).isNull()) {
                var expiresAt = moment().add(days, 'days').format('YYYY-MM-DD');
                var text = 'Poc expires at ' + expiresAt + ' (' + days + ' left)';
                if(days < 0) {
                    text = 'Poc expired at ' + expiresAt;
                }
                $scope.model.lines.push({
                    text: text
                });
            }
            logic.resolver.resolve();
        });
    };
});
