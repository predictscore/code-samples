var m = angular.module('app');

m.directive('widgetExample', function(path, $q, $timeout) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('example').directive('widget-example').template(),
        scope: {

        },
        link: function($scope, element, attrs, ctrl) {
            $scope.widgetApi = {};
            $scope.loaded = 0;
            $scope.loadData = function() {
                return $timeout(function() {
                    $scope.loaded++;
                }, 1000);
            };
        }
    };
});