var m = angular.module('utils');
m.factory('docTitleHelper', function($route, $rootScope, $timeout) {
    var titlePrefix = 'Avanan';
    $rootScope.$watch(function () {
        return $route.current && ($route.current.fullTitle || $route.current.title);
    }, function (newVal) {
        if (_(pendingTitle).isNull()) {
            windowTitleHelper.setTitle('');
        }
    });
    var pendingTitle = null;
    function updateTitle() {
        if (!_(pendingTitle).isNull()) {
            var newTitle = titlePrefix;
            if (pendingTitle) {
                newTitle += ' ' + pendingTitle;
            }
            document.title = newTitle;
            pendingTitle = null;
        }
    }
    var windowTitleHelper = {
        setTitle: function (title) {
            pendingTitle = title;
            $timeout(updateTitle);
        }
    };
    return windowTitleHelper;
});