var m = angular.module('utils');

m.factory('routeHelper', function($route, modulesManager, $location, entityTypes) {

    var transformRoutes = {
        anomaly: true,
        application: true,
        bucket: true,
        email: true,
        file: true,
        folder: true,
        group: true,
        instance: true,
        ip: true,
        organization: true,
        project: true,
        site: true,
        user: true
    };
    function transformParams (name, params) {
        if (window.guiVersion === 2 && transformRoutes[name]) {
            var entityType = entityTypes.toBackend(params.module, name);
            if (entityType) {
                params.entityType = entityType;
                return 'profile';
            }
        }
        return name;
    }
    var routes = {};
    _($route.routes).each(function(route) {
        if(_(route.name).isString()) {
            routes[route.name] = route;
        }
    });
    var lastParams = {};
    var savedRoute = null;
    var routeHelper = {
        getPath: function(name, params, qs) {
            params = $.extend({
                module: modulesManager.currentModule()
            }, params);
            qs = qs || {};
            if(_(name).isArray()) {
                name = name[0];
            }
            name = transformParams(name, params);
            if(_(routes[name]).isUndefined()) {
                console.log('Route with name ' + name + ' not defined');
                return '';
            }
            var path = routes[name].originalPath;
            path = path.replace(/(\/?):([A-Za-z]+)([\*\?]?)/g, function (match, slash, param, option) {
                if (_(params[param]).isUndefined()) {
                    if (option === '?') {
                        return '';
                    } else {
                        return match; // for compatibility with old version. May be we should return something else here.
                    }
                }
                return slash + encodeURIComponent(params[param]);
            });
            var query = _(qs).map(function(val, key) {
                val = encodeURIComponent(val);
                return key + '=' + val;
            }).join('&');
            if(query.length) {
                path = path + '?' + query;
            }
            return path;
        },
        isActive: function(name) {
            if(_(name).isArray()) {
                return _(name).find(function(n) {
                    return $route.current.name === n;
                }) ? true : false;
            }
            return $route.current.name === name;
        },
        redirectTo: function(name, params, options) {
            options = options || {};
            if(options.qs) {
                options.qs = _(options.qs).chain().map(function(val, key) {
                    return [key, val];
                }).filter(function(o) {
                    return !_(o[1]).isUndefined();
                }).object().value();
            }
            var oldPath = $location.path();
            var newPath = this.getPath(name, params, options.qs);
            if(newPath == oldPath) {
                return;
            }
            if(options.replace) {
                $location.replace();
            }
            if(options.saveQueryString) {
                $location.path(newPath, options.reload);
            } else {
                $location.url(newPath);
            }
            
            if(options.reloadRoute) {
                $route.reload();
            }

            lastParams = params;
        },
        getParams: function() {
            return lastParams || {};
        },
        saveRoute: function() {
            savedRoute = {
                params: $route.current.params,
                name: $route.current.name
            };
        },
        restoreRoute: function() {
            if(!savedRoute) {
                routeHelper.redirectTo('dashboard');
            } else {
                routeHelper.redirectTo(savedRoute.name, savedRoute.params);
                savedRoute = null;
            }
        },
        pathToHref: function (path) {
            return '#!' + path;
        },
        getHref: function (name, params, qs) {
            return routeHelper.pathToHref(routeHelper.getPath(name, params, qs));
        }
    };
    return routeHelper;
});