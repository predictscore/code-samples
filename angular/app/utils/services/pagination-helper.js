var m = angular.module('utils');

m.factory('paginationHelper', function() {
    return {
        generatePages: function(args) {
            var current = args.current;
            var total = Math.max(args.total, current);
            var haveNext = args.haveNext || _(total).isNumber() && current < total;
            var pagesAround = args.pagesAround;
            var valueGenerator = args.valueGenerator || function(val) {return val;};

            var pages = [];

            if(total === 0 || current === 0 && !haveNext) {
                return pages;
            }

            pages.push({
                title: 'First',
                value: valueGenerator(1),
                enabled: current > 1
            });
            pages.push({
                title: 'Previous',
                value: valueGenerator(current - 1),
                enabled: current > 1
            });

            var maxPage = _(total).isNumber() ? total : (haveNext ? current + 1 : current);
            var page = Math.max(1, Math.min(current - pagesAround, maxPage - (pagesAround * 2)));
            for(var i = 0; i < pagesAround * 2 + 1; i++) {
                pages.push({
                    title: page,
                    value: valueGenerator(page),
                    enabled: true,
                    active: page === current
                });
                if(++page > total) {
                    break;
                }
            }

            if (_(total).isNumber() && current < total && page <= total) {
                if (page < total) {
                    pages.push({
                        title: '..',
                        value: '',
                        enabled: false
                    });
                }
                pages.push({
                    title: total,
                    value: valueGenerator(total),
                    enabled: true
                });
            }

            pages.push({
                title: 'Next',
                value: valueGenerator(current + 1),
                enabled: haveNext
            });

            return pages;
        }
    };
});