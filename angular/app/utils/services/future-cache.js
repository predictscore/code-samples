var m = angular.module('utils');

m.factory('futureCache', function($timeout) {
    var cache = {};
    return {
        cache: function (id, timeout, future) {
            if (_(cache[id]).isUndefined()) {
                cache[id] = {
                    future: future()
                };
                if(!_(timeout).isNull()) {
                    cache[id].timeout = $timeout(function () {
                        delete cache[id];
                    }, timeout);
                }
            }
            return cache[id].future;
        },
        clear: function(regexp) {
            _(cache).each(function(val, key) {
                if(regexp.test(key)) {
                    if(!_(cache[key].timeout).isUndefined()) {
                        $timeout.cancel(cache[key].timeout);
                    }
                    delete cache[key];
                }
            });
        }
    };
});