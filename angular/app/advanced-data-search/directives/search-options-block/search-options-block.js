var m = angular.module('advanced-data-search');

m.directive('searchOptionsBlock', function(path, globalSearchDao, $q, $timeout) {
    var maxDynamicFilteredOptionsSize = 5;
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('advanced-data-search').directive('search-options-block').template(),
        scope: {
            model: '=searchOptionsBlock',
            blockOptions: '=',
            queryParams: '=',
            buildParams: '&'
        },
        link: function($scope, element, attrs) {
            $scope.options = [];

            $scope.visible = false;
            var initialized = false;
            var reloaded = true;
            var init = _.noop;
            var reload = _.noop;
            var loadedParams;
            $scope.isLoading = false;

            function updateOptionVisibility() {
                if(!$scope.dynamic) {
                    $scope.options = $scope.blockOptions.options;
                }
                $scope.buildedOptions = null;
            }

            $scope.isEmpty = function () {
                return _(_($scope.model.values).find(function (value, key) {
                    return !_(value).isNull() && !_(value).isUndefined();
                })).isUndefined();
            };

            $scope.blockPanel = {
                title: $scope.blockOptions.title,
                'class': 'avanan white-body',
                filter: {
                    placeholder: 'Filter...',
                    value: ''
                },
                bodyStyle: {
                    height: '200px'
                },
                icons : {
                    minimize: 'fa-chevron-up',
                    expand: 'fa-chevron-down'
                },
                minimize: true,
                minimized: $scope.isEmpty(),
                buttons: [{
                    'class': 'btn-icon',
                    iconClass: 'fa fa-trash',
                    tooltip: 'Clear filters',
                    execute: function() {
                        clearFilters();
                    }
                }, {
                    'class': {
                        'btn-icon': true
                    },
                    iconClass: 'fa fa-check-square-o',
                    tooltip: 'Filters selected',
                    showWhenMinimized: true,
                    hidden: true
                }]
            };

            function clearFilters() {
                $scope.model.values = {};
                $scope.model.operator = $scope.blockOptions.defaultOperator || 'AND';
            }

            $scope.operatorVariants = ["AND", "OR"];

            $scope.dynamic = false;
            $scope.dynamicOptions = [];
            $scope.tooMuchOptions = false;
            if(_($scope.blockOptions.queryName).isString()) {
                $scope.dynamic = true;
                $scope.visible = true;

                init = function () {
                    var ids = _($scope.model.values).chain().map(function (value, key) {
                        if (_(value).isBoolean()) {
                            return key;
                        }
                    }).compact().value();
                    if (ids.length) {
                        $scope.isLoading = true;
                        globalSearchDao.advancedDataSearchDynamicFacets($scope.blockOptions.queryName, undefined, ids.join(',')).then(function (response) {
                            $scope.options = response.data.rows;
                            $scope.isLoading = false;
                        });
                    }

                    $scope.$watch('blockPanel.filter.value', function (filter) {
                        $scope.isLoading = true;
                        globalSearchDao.advancedDataSearchDynamicFacets($scope.blockOptions.queryName, filter)
                            .pagination(0, maxDynamicFilteredOptionsSize).then(function (response) {
                                if (response.data.total_rows <= maxDynamicFilteredOptionsSize) {
                                    $scope.dynamicOptions = response.data.rows;
                                    $scope.tooMuchOptions = false;
                                } else {
                                    $scope.dynamicOptions = [];
                                    $scope.tooMuchOptions = true;
                                }
                                $scope.buildedOptions = null;
                                $scope.isLoading = false;
                            });
                    });

                    $scope.$watchCollection('model.values', function (values) {
                        var exist = {};
                        $scope.options = _($scope.options).filter(function (option) {
                            exist[option.id] = true;
                            return _(values[option.id]).isBoolean();
                        });
                        _($scope.dynamicOptions).each(function (option) {
                            if (!exist[option.id] && _(values[option.id]).isBoolean()) {
                                $scope.options.push(option);
                            }
                        });
                    });
                    initialized = true;
                };
            } else {
                var optionsRequestTimeout = $q.defer();
                $scope.isLoading = true;
                var allListPromise = globalSearchDao.advancedDataSearchAllFacets()
                    .then(function (response) {
                        var block = _(response.data).find(function (block) {
                            return block.id === $scope.blockOptions.id;
                        });
                        if (!_(block).isUndefined()) {
                            $scope.blockOptions.options = sortOptions(block.options);
                            if (block.options.length) {
                                $scope.visible = true;
                            }
                        }
                        $scope.isLoading = false;
                    });

                init = function () {
                    reloaded = false;
                    $scope.$watch('blockOptions.options', updateOptionVisibility);
                    $scope.$watch('blockPanel.filter.value', function (filter) {
                        $scope.buildedOptions = null;
                    });
                    $scope.$watch('queryParams', function () {
                        if (!$scope.blockPanel.minimized) {
                            reload();
                        } else {
                            reloaded = false;
                        }

                    });
                    initialized = true;
                };

                reload = function () {
                    var loadParams = $scope.buildParams({ignore: $scope.blockOptions.id});
                    if (!_(loadParams).isEqual(loadedParams)) {
                        loadedParams = loadParams;
                        optionsRequestTimeout.resolve();
                        optionsRequestTimeout = $q.defer();
                        $scope.isLoading = true;
                        globalSearchDao.advancedDataSearchFacets($scope.blockOptions.id, loadedParams.params, loadedParams.filter, optionsRequestTimeout.promise)
                            .preFetch(allListPromise, 'allFacets')
                            .then(function (response) {
                                updateOptions(response.data[0]);
                                $scope.isLoading = false;
                            });
                    }
                    reloaded = true;
                };
            }

            function sortOptions(options) {
                return _(options).chain().sortBy('label').sortBy(function (option) {
                    return -option.count;
                }).sortBy(function (option) {
                    if ($scope.model.values[option.id] === true) {
                        return 1;
                    }
                    if ($scope.model.values[option.id] === false) {
                        return 2;
                    }
                    return 3;
                }).value();
            }

            function updateOptions(block) {
                var options = block.options || [];
                _($scope.blockOptions.options).each(function (option) {
                    var found = _(options).find(function(o) {
                        return o.id == option.id;
                    });
                    if(_(found).isUndefined()) {
                        options.push({
                            id: option.id,
                            label: option.label,
                            count: 0
                        });
                    }
                });
                options = sortOptions(options);

                $scope.blockOptions.options = options;

                $timeout(function () {
                    element.find('.panel-body').scrollTop(0);
                }, 10);
            }

            $scope.buildedOptions = null;
            $scope.buildOptions = function() {
                if(!_($scope.buildedOptions).isNull()) {
                    return $scope.buildedOptions;
                }
                var exist = {};
                $scope.buildedOptions = _($scope.options).chain().filter(function (option) {
                    exist[option.id] = true;
                    return !_($scope.model.values[option.id]).isUndefined() && !_($scope.model.values[option.id]).isNull() || option.count > 0;
                }).filter(function (option) {
                    return option.label.toLowerCase().indexOf($scope.blockPanel.filter.value.toLowerCase()) >= 0;
                }).value();

                _($scope.dynamicOptions).each(function(option) {
                    if(!exist[option.id]) {
                        $scope.buildedOptions.push(option);
                    }
                });

                return $scope.buildedOptions;
            };

            function updateIndicatorVisibility() {
                $scope.blockPanel.buttons[1].hidden = $scope.isEmpty() || !$scope.blockPanel.minimized;
            }

            $scope.$watch('model.operator', updateOptionVisibility);
            $scope.$watchCollection('model.values', updateIndicatorVisibility);
            $scope.$watch('blockPanel.minimized', function () {
                updateIndicatorVisibility();
                if (!$scope.blockPanel.minimized) {
                    if (!initialized) {
                        init();
                    } else {
                        if (!reloaded) {
                            reload();
                        }
                    }
                }
            });
        }
    };
});