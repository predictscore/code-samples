var m = angular.module('advanced-data-search');

m.directive('searchOption', function(path) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('advanced-data-search').directive('search-option').template(),
        scope: {
            model: '=searchOption',
            option: '='
        },
        link: function($scope, element, attr) {
            $scope.getStateClass = function() {
                if($scope.model === true) {
                    return 'fa-check-square-o';
                }
                if($scope.model === false) {
                    return 'fa-minus-square-o';
                }
                return 'fa-square-o';
            };

            $scope.changeState = function(event) {
                event.preventDefault();
                if(_($scope.model).isUndefined() || _($scope.model).isNull()) {
                    $scope.model = true;
                } else if($scope.model === true) {
                    $scope.model = false;
                } else {
                    $scope.model = null;
                }
            };
        }
    };
});