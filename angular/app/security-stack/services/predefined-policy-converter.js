var m = angular.module('security-stack');

m.factory('predefinedPolicyConverter', function($q) {
    return function(input, args) {
        var deferred = $q.defer();

        if (!input.data.rows &&  _(input.data).isArray()) {
            input.data = {
                rows: input.data
            };
        }
        _(input.data.rows).each(function(row) {
            row.policy.context = {
                saas: row.saas,
                security: row.sec_app
            };
        });

        deferred.resolve(input);
        return deferred.promise;
    };
});