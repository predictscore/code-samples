var m = angular.module('security-stack');

m.factory('securityMatrixConverter', function($q, path, securityMatrixStatusConverter, troubleshooting) {
    var root = path('security-stack');
    return function(input, args) {
        var deferred = $q.defer();

        var groups = _(input.data.groups).chain().filter(function(group, index) {
            return index === 0 || !group.exclude_from_matrix && group.family === 'security';
        }).map(function(group) {
            if(group.family == 'security') {
                return {
                    categoryId: group.categoryId,
                    label: group.categoryName,
                    addRoute: {
                        name: 'app-store',
                        params: {
                            filter: group.type
                        }
                    },
                    items: _(group.modules).chain().filter(function(module) {
                        if (module.gui_hidden && !troubleshooting.entityDebugEnabled()) {
                            return false;
                        }
                        if(group.main_module) {
                            return module.state == 'active' && module.name == group.main_module;
                        }
                        return module.state == 'active';
                    }).map(function(module) {
                        var result = {
                            id: module.name,
                            label: module.label,
                            img: {
                                path: root.img('solutions/' + module.stack_icon)
                            },
                            isBeta: module.is_beta,
                            isExpired: module.isExpired
                        };
                        if(group.main_module) {
                            var allModules = _(group.modules).filter(function(module) {
                                return module.name != group.main_module;
                            });
                            var activeModules = _(allModules).filter(function(module) {
                                return module.state == 'active';
                            });
                            result.dynamicText = activeModules.length + '/' + allModules.length + ' enabled';
                        }
                        return result;
                    }).value()
                };
            } else {
                return {
                    categoryId: group.categoryId,
                    label: group.categoryName
                };
            }
        }).value();

        var applications = _(input.data.groups).chain().filter(function(group) {
            return group.family == 'saas';
        }).map(function(group) {
            return _(group.modules).map(function(app) {
                return {
                    label: app.label,
                    img: {
                        path: root.img('applications/' + app.stack_icon)
                    },
                    status: app.state,
                    groups: _(groups).chain().map(function(group) {
                        return _(group.items).map(function(module) {
                            if(app.state != 'active') {
                                return {
                                    state: 'none'
                                };
                            }
                            return {
                                saas: app.name,
                                saasLabel: app.label,
                                security: module.id,
                                securityLabel: module.label
                            };
                        });
                    }).flatten().value()
                };
            });
        }).flatten().value();

        securityMatrixStatusConverter(applications, args.modulesMatrix, args.policyCatalog);

        var data = {
            data: {
                applications: applications,
                groups: groups
            }
        };

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    };
});