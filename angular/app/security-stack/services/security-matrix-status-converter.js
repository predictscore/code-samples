var m = angular.module('security-stack');

m.factory('securityMatrixStatusConverter', function() {
    return function(applications, modulesMatrix, policyCatalog) {
        var policyMap = {};

        _(modulesMatrix.data).each(function(value, saas) {
            var saasPolicies = policyMap[saas] || {};
            _(value).each(function(policies, module) {
                saasPolicies[module] = _(policies).map(function(policy, policyId) {
                    return {
                        name: policy.name,
                        status: policy.status,
                        id: policyId
                    };
                });
            });
            policyMap[saas] = saasPolicies;
        });

        if (policyCatalog) {
            _(policyCatalog.data.rows).chain().filter(function(row) {
                return _(row.context).isObject() && _(row.context.security).isString();
            }).each(function(row) {
                var saasPolicies = policyMap[row.context.saas] || {};
                saasPolicies[row.context.security] = saasPolicies[row.context.security] || [];
                if(_(saasPolicies[row.context.security]).every(function(policy) {
                        return policy.id != row.policy_id;
                    })) {
                    saasPolicies[row.context.security].push({
                        status: row.policy_status,
                        name: row.policy_name,
                        id: row.policy_id
                    });
                }
                policyMap[row.context.saas] = saasPolicies;
            });
        }

        _(applications).each(function (application) {
            _(application.groups).each(function (item) {
                if (!item.saas || !item.security) {
                    return;
                }
                delete item.tooltip;
                var policies = (policyMap[item.saas] || {})[item.security];
                if(_(policies).isUndefined() || _(policies).isEqual({})) {
                    delete item.policies;
                    item.state = 'off';
                    return;
                }
                item.policies = policies;
                function isRunning(policy) {
                    return policy.status == 'running';
                }
                if(_(policies).every(isRunning)) {
                    item.state = 'on-green';
                } else if(_(policies).some(isRunning)) {
                    item.state = 'on-green';
                } else {
                    item.state = 'off';
                }
            });
        });
    };
});