var m = angular.module('security-stack');

m.controller('SecurityStackController', function($scope, $routeParams, widgetPolling, sizeClasses, path, feature, sideMenuManager) {
    widgetPolling.updateWidgets(path('security-stack').ctrl('security-stack').json('security-stack' + (feature.newSecurityMatrix ? '-v2' : '')).path);
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
    
    sideMenuManager.treeNavigationPath(['security-stack']);
});