var m = angular.module('security-stack');

m.directive('securityMatrix', function (path, routeHelper, widgetLogic, policyDao, $q, $timeout) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('security-stack').directive('security-matrix').template(),
        scope: {
            model: '=securityMatrix'
        },
        link: function ($scope, element, attrs) {

            var maxItems = 4;

            widgetLogic($scope, $scope.model.logic);

            $scope.defaultAppImage = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=='; // transparent 1 pixel gif

            function enchantGroup(group) {
                group.tooltip =  _(group.policies).map(function(p) {
                    return p.name + ' (' + p.status + ')';
                }).join('<br>');
            }

            function expandGroup (groupState) {
                groupState.collapsed = false;
                groupState.tooltip = 'Click to collapse';
                groupState.iconClass = 'fa-angle-left';
                groupState.leftText = '';
                groupState.rightText = 'less';
            }

            function collapseGroup (groupState) {
                groupState.collapsed = true;
                groupState.tooltip = 'Click to expand';
                groupState.iconClass = 'fa-angle-right';
                groupState.leftText = 'more';
                groupState.rightText = '';
            }


            $scope.toggleGroup = function (group) {
                var groupState = $scope.groupState(group);
                if (!groupState.collapseable) {
                    return;
                }
                if (groupState.collapsed) {
                    expandGroup(groupState);
                } else {
                    collapseGroup(groupState);
                }
            };

            $scope.groupState = function (group) {
                return $scope.tableModel.groupStates[group.categoryId] || ($scope.tableModel.groupStates[group.categoryId] = {});
            };

            $scope.itemCellClass = function (item) {
                if (!item.isHideable) {
                    return '';
                }
                return {
                    'collapsed': $scope.groupState(item.group).collapsed
                };
            };

            $scope.$watch('model', function () {
                $scope.tableModel = $.extend($scope.tableModel || {}, $scope.model.data);
                $scope.tableModel.items = [];
                $scope.tableModel.groupStates = $scope.tableModel.groupStates || {};
                _($scope.tableModel.groups).each(function (group) {
                    var groupState = $scope.groupState(group);
                    if (_(group.items).isUndefined() || !group.items.length) {
                        $scope.tableModel.items.push({
                            empty: true,
                            group: group
                        });
                        groupState.collapseable = false;
                    } else {
                        groupState.collapseable = (group.items.length > maxItems);
                        $scope.tableModel.items = $scope.tableModel.items.concat(_(group.items).map(function (item, idx) {
                            return $.extend({
                                group: group,
                                groupState: groupState,
                                isHideable: (idx >= maxItems)
                            }, item);
                        }));
                    }
                });

                _($scope.tableModel.groupStates).each(function (groupState) {
                    if (groupState.collapseable && _(groupState.collapsed).isUndefined()) {
                        collapseGroup(groupState);
                    }
                });

                _($scope.tableModel.applications).each(function(application) {
                    _(application.groups).each(function(group) {
                        enchantGroup(group);
                    });
                });
            }, true);

            $scope.groupWidth = function (group) {
                var groupState = $scope.groupState(group);
                var itemsLength = groupState.collapsed ? maxItems : (group.items || []).length;
                return Math.max(1, itemsLength);
            };

            $scope.getItemIndexForColumn = function (column) {
                var idx = 0;
                for (var i = 0; i < column; i++) {
                    if (!$scope.tableModel.items[i].empty) {
                        idx++;
                    }
                }
                return idx;
            };

            $scope.itemLink = function (item) {
                if (item.group.addRoute) {
                    return routeHelper.getPath(item.group.addRoute.name, item.group.addRoute.params);
                }
                return '/';
            };

            $scope.toAppStore = function (event, item) {
                if (!_(event).isUndefined()) {
                    event.preventDefault();
                }
                if (_(item).isUndefined()) {
                    routeHelper.redirectTo('cloud-apps');
                } else if (item.group.addRoute) {
                    routeHelper.redirectTo(item.group.addRoute.name, item.group.addRoute.params);
                }
            };

            function policyByIdx(application, index) {
                return application.groups[$scope.getItemIndexForColumn(index)];
            }

            var actionInProcess = false;
            $scope.switchPolicy = function (application, index) {
                var policy = policyByIdx(application, index);
                if(actionInProcess) {
                    return;
                }
                actionInProcess = true;
                if (_(policy.policies).isUndefined()) {
                    policyDao.predefinedPolicy(policy.saas, policy.security).then(function(response) {
                        var policyBody = {
                            "policy_data": {
                                "excludeCondition": {
                                    "operator": "AND",
                                    "not": false,
                                    "conditions": []
                                },
                                "description": "",
                                "entityType": "google_user",
                                "actions": [],
                                "includeCondition": {
                                    "operator": "AND",
                                    "not": false,
                                    "conditions": [{
                                        "type": "condition",
                                        "not": false,
                                        "operator": "bool",
                                        "property": "google_user.isAdmin",
                                        "data": true,
                                        "options": {}
                                    }]
                                }
                            },
                            "entity_type": "google_user",
                            "policy_status": "running",
                            "context": {
                                saas: policy.saas,
                                security: policy.security
                            }
                        };
                        if (!response.data.rows.length) {
                            response.data.rows.push({
                                demo: true,
                                policy: policyBody
                            });
                        }
                        $q.all(_(response.data.rows).map(function(row) {
                            policyBody = row.policy;

                            if(_(row.demo).isUndefined()) {
                                policyBody = $.extend(true, {
                                    "policy_name": policy.securityLabel + ' over ' + policy.saasLabel,
                                    "policy_data": {
                                        "name": policy.securityLabel + ' over ' + policy.saasLabel
                                    },
                                    "policy_status": 'running'
                                }, policyBody);
                                if (policyBody.policy_name != policyBody.policy_data.name) {
                                    policyBody.policy_data.name = policyBody.policy_name;
                                }
                            }
                            return policyDao.createRaw(policyBody);
                        }))['finally'](function() {
                            actionInProcess = false;
                        }).then(function(responses) {
                            policy = policyByIdx(application, index);
                            policy.policies = _(responses).map(function(response) {
                                return {
                                    id: response.data.policy_id,
                                    name: response.data.policy_name,
                                    status: response.data.policy_status
                                };
                            });
                            policy.state = 'on-green';
                            enchantGroup(policy);
                        });
                    });
                } else if (policy.state != 'off') {
                    $q.all(_(policy.policies).chain().filter(function(policy) {
                        return policy.status == 'running';
                    }).map(function(policy) {
                        return policyDao.retrieve(policy.id);
                    }).value()).then(function(responses) {
                        return $q.all(_(responses).map(function(response) {
                            var policyBody = response.data;
                            policyBody.policy_status = 'pause';
                            return policyDao.update(policyBody.policy_id, policyBody);
                        }));
                    })['finally'](function() {
                        actionInProcess = false;
                    }).then(function() {
                        policy = policyByIdx(application, index);
                        policy.state = 'off';
                        _(policy.policies).each(function(p) {
                            p.status = 'pause';
                        });
                        enchantGroup(policy);
                    });
                } else {
                    $q.all(_(policy.policies).map(function(policy) {
                        return policyDao.retrieve(policy.id);
                    })).then(function(responses) {
                        return $q.all(_(responses).map(function(response) {
                            var policyBody = response.data;
                            policyBody.policy_status = 'running';
                            return policyDao.update(policyBody.policy_id, policyBody);
                        }));
                    })['finally'](function() {
                        actionInProcess = false;
                    }).then(function() {
                        policy = policyByIdx(application, index);
                        policy.state = 'on-green';
                        _(policy.policies).each(function(p) {
                            p.status = 'running';
                        });
                        enchantGroup(policy);
                    });
                }
            };
        }
    };
});