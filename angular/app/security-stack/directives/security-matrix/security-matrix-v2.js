var m = angular.module('security-stack');

m.directive('securityMatrixV2', function (path, routeHelper, widgetLogic, policyDao, $q, securityMatrixStatusConverter, modals) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('security-stack').directive('security-matrix').template(),
        scope: {
            model: '=securityMatrixV2'
        },
        link: function ($scope, element, attrs) {
            var maxItems = 4;

            widgetLogic($scope, $scope.model.logic);

            $scope.defaultAppImage = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=='; // transparent 1 pixel gif

            function enchantGroup(group) {
                group.tooltip =  {
                    content: {
                        text: _(group.policies).map(function(p) {
                            return '<a href="/#!' + routeHelper.getPath('policy-edit', {id: p.id}) + '">' + p.name + ' (' + p.status + ')' + '</a>';
                        }).join('<br>')
                    },
                    position: {
                        at: 'bottom center',
                        my: 'top center',
                        viewport: true
                    },
                    hide: {
                        delay: 100,
                        fixed: true
                    },
                    style: {
                        classes: 'qtip-bootstrap qtip-security-matrix'
                    }
                };
            }

            function expandGroup (groupState) {
                groupState.collapsed = false;
                groupState.tooltip = 'Click to collapse';
                groupState.iconClass = 'fa-angle-left';
                groupState.leftText = '';
                groupState.rightText = 'less';
            }

            function collapseGroup (groupState) {
                groupState.collapsed = true;
                groupState.tooltip = 'Click to expand';
                groupState.iconClass = 'fa-angle-right';
                groupState.leftText = 'more';
                groupState.rightText = '';
            }


            $scope.toggleGroup = function (group) {
                var groupState = $scope.groupState(group);
                if (!groupState.collapseable) {
                    return;
                }
                if (groupState.collapsed) {
                    expandGroup(groupState);
                } else {
                    collapseGroup(groupState);
                }
            };

            $scope.groupState = function (group) {
                return $scope.tableModel.groupStates[group.categoryId] || ($scope.tableModel.groupStates[group.categoryId] = {});
            };

            $scope.cellClass = function (item, group) {
                var result = {
                    'collapsed': item.isHideable && $scope.groupState(item.group).collapsed,
                };
                if(!item.empty && group) {
                    result['saas-' + group.saas] = true;
                    result['security-' + group.security] = true;
                }
                return result;
            };

            $scope.$watch('model', function () {
                $scope.tableModel = $.extend($scope.tableModel || {}, $scope.model.data);
                $scope.tableModel.items = [];
                $scope.tableModel.groupStates = $scope.tableModel.groupStates || {};
                _($scope.tableModel.groups).each(function (group) {
                    var groupState = $scope.groupState(group);
                    if (_(group.items).isUndefined() || !group.items.length) {
                        $scope.tableModel.items.push({
                            empty: true,
                            group: group
                        });
                        groupState.collapseable = false;
                    } else {
                        groupState.collapseable = (group.items.length > maxItems);
                        $scope.tableModel.items = $scope.tableModel.items.concat(_(group.items).map(function (item, idx) {
                            return $.extend({
                                group: group,
                                groupState: groupState,
                                isHideable: (idx >= maxItems)
                            }, item);
                        }));
                    }
                });

                _($scope.tableModel.groupStates).each(function (groupState) {
                    if (groupState.collapseable && _(groupState.collapsed).isUndefined()) {
                        collapseGroup(groupState);
                    }
                });

                _($scope.tableModel.applications).each(function(application) {
                    _(application.groups).each(function(group) {
                        enchantGroup(group);
                    });
                });
            }, true);

            $scope.groupWidth = function (group) {
                var groupState = $scope.groupState(group);
                var itemsLength = groupState.collapsed ? maxItems : (group.items || []).length;
                return Math.max(1, itemsLength);
            };

            $scope.getItemIndexForColumn = function (column) {
                var idx = 0;
                for (var i = 0; i < column; i++) {
                    if (!$scope.tableModel.items[i].empty) {
                        idx++;
                    }
                }
                return idx;
            };

            $scope.itemLink = function (item) {
                if (item.group.addRoute) {
                    return routeHelper.getPath(item.group.addRoute.name, item.group.addRoute.params);
                }
                return '/';
            };

            $scope.toAppStore = function (event, item) {
                if (!_(event).isUndefined()) {
                    event.preventDefault();
                }
                if (_(item).isUndefined()) {
                    routeHelper.redirectTo('cloud-apps');
                } else if (item.group.addRoute) {
                    routeHelper.redirectTo(item.group.addRoute.name, item.group.addRoute.params);
                }
            };

            function policyByIdx(application, index) {
                return application.groups[$scope.getItemIndexForColumn(index)];
            }

            $scope.switchPolicy = function (application, index) {
                var policy = policyByIdx(application, index);
                if(policy.state === 'updating') {
                    return;
                }
                var savedItemState = policy.state;
                var savedTooltip = policy.tooltip;
                policy.state = 'updating';
                policy.tooltip = 'Updating... Please wait...';
                policyDao.policyMatrixOperation(policy.saas, policy.security, savedItemState === 'off' ? 'start' : 'stop')
                    .then(function (response) {
                        securityMatrixStatusConverter($scope.model.data.applications, response);
                    }, function (response) {
                        policy.state = savedItemState;
                        policy.tooltip = savedTooltip;
                        modals.alert('Unable to start requested policy. Looks like something is wrong with configuration.', {
                            title: 'Error'
                        });
                    });
            };
        }
    };
});