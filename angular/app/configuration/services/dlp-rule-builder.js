var m = angular.module('configuration');

m.factory('dlpRuleBuilder', function() {

    function appendOperator(str, op) {
        if (!str) {
            return;
        }
        return _(str.split(/\s+/)).map(function (w) {
            if (w) {
                return op + w;
            }
            return w;
        }).join(' ');
    }

    return function (info) {
        var res = [];
        var and = appendOperator(info.and, '+');
        if (and) {
            res.push(and);
        }
        var not = appendOperator(info.not, '-');
        if (not) {
            res.push(not);
        }
        var or = appendOperator(info.or, '');
        if (or) {
            res.push(or);
        }
        if (info.phrase) {
            res.push('"' + info.phrase + '"');
        }
        if (info.regexp) {
            res.push('/' + info.regexp + '/');
        }
        if (info.proxphrase) {
            res.push('"' + info.proxphrase + '"~' + (info.proximity || ''));
        }
        if (info.rangeStart && info.rangeEnd) {
            res.push('[' + info.rangeStart + ' TO ' + info.rangeEnd + ']');
        }
        return res.join(' ');
    };
});