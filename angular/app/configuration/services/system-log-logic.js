var m = angular.module('configuration');

m.factory('systemLogLogic', function(widgetPolling, simpleSortableLogicCore, systemDao) {
    return function($scope, logic) {
        return simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return systemDao.logList($scope.model.columns);
            }
        });
    };
});