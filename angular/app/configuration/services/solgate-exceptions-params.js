var m = angular.module('configuration');

m.factory('solgateExceptionsParams', function(modals, $q, solgateExceptionsDao) {
    return {
        addDialog: function(addParams) {
            var modalParams = [{
                id: 'macro_hash',
                type: "list",
                label: "Hashes of macros",
                validation: {
                    required: true
                },
                data: addParams
            }, {
                id: 'comment',
                type: 'text',
                label: 'Comment (optional)',
                paramClass: 'text-param-average'
            }];

            return modals.params([{
                lastPage: true,
                params: modalParams
            }], "Ignore macros with these hashes").then(function (data) {
                var addData = {};
                addData.macro_hash = _(data.macro_hash).map(function (id) {
                    return {
                        value: id,
                        comment: data.comment || null
                    };
                });
                return solgateExceptionsDao.add(addData).then(function (response) {
                    return response;
                }, function (error) {
                    return modals.alert('Error adding exceptions', {
                        title: 'Error'
                    }).then(function () {
                        return $q.reject(error);
                    }, function () {
                        return $q.reject(error);
                    });
                });
            });
        }
    };

});