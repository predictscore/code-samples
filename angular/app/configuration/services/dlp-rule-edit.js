var m = angular.module('configuration');

m.factory('dlpRuleEdit', function($q, modals, dlpConfigurationDao, sideMenuManager, dlpRuleTypes) {

    function showDlpRuleDialog(rule, options) {
        return modals.smartSearchRule(rule, {
            okButton : options.okButton,
            title: options.title
        }).then(function (rule) {
            return saveRule(rule, options);
        });
    }

    function saveRuleError(message, rule, options) {
        return modals.alert(message || 'Unknown error saving rule', {title: 'Error'}).then(function () {
            return showDlpRuleDialog(rule, options);
        });
    }

    function saveRule(rule, options) {
        var promise;
        if (options.ruleId) {
            promise = dlpConfigurationDao.updateRule(options.ruleId, rule);
        } else {
            promise = dlpConfigurationDao.createRule(rule);
        }
        return promise.then(function (response) {
            if (!response.data.success) {
                return saveRuleError(response.data.message, rule, options);
            }
        }, function (error) {
            return saveRuleError(error.data.message, rule, options);
        });
    }

    function dlpRuleDialog(rule) {
        var options = {};
        if (rule) {
            options.title = 'Update smart search rule';
            options.okButton = 'Update';
            options.ruleId = rule.id;
            if (!rule.ui_info) {
                rule.ui_info = {
                    mode: 'raw'
                };
            }
        } else {
            options.title = 'Create new smart search rule';
            options.okButton = 'Create';
            rule = {
                name: '',
                type: '',
                value: '',
                description: ''
            };
        }
        return showDlpRuleDialog(rule, options);
    }

    var ruleEdit = {
        createRule: function () {
            return dlpRuleDialog();
        },
        editRule: function (ruleId) {
            return dlpConfigurationDao.retrieveRule(ruleId).then(function(rule) {
                return dlpRuleDialog(rule.data);
            });
        }
    };

    return ruleEdit;

});