var m = angular.module('configuration');

m.factory('dlpRulesImportLogic', function (dlpConfigurationDao, dlpRuleBuilder, $q) {

    var importColumns = [{
        "title": "Name",
        "id": "name",
        "required": true,
        "type": "string"
    }, {
        "title": "Type",
        "id": "type",
        "required": true,
        "type": "string",
        "allowed": ['query_language', 'regexp']
    }, {
        "title": "Value",
        "id": "value",
        "required": true,
        "type": "string"
    }, {
        "title": "Description",
        "id": "description",
        "required": false,
        "type": "string"
    }, {
        "title": "Active",
        "id": "is_active",
        "required": false,
        "type": "boolean",
        "default": false
    }, {
        "title": "UI Info",
        "id": "ui_info",
        "required": false,
        "type": "json"
    }];

    return function ($scope, logic, reinitLogic) {

        function formatLine(info) {
            return {
                widget: {
                    type: 'info',
                    'class': 'rule-import-line',
                    lines: [{
                        'class': 'line-title',
                        text: '#' + JSON.stringify({
                            display: 'text',
                            text: info.title,
                            tooltip: info.titleTip
                        })
                    },
                        partIcon(info),
                        partStatusText(info)
                    ]
                }
            };
        }

        function partIcon(info) {
            return {
                'class': 'status-icon',
                text: '',
                icon: info.statusIcon
            };
        }

        function partStatusText(info) {
            return {
                'class': 'status-text',
                text: '#' + JSON.stringify({
                    display: 'text',
                    tooltip: info.statusFull,
                    text: info.statusText
                })
            };
        }

        function addLineSuccess(title) {
            var line = formatLine({
                title: title,
                statusText: 'OK',
                statusIcon: 'fa-check'
            });
            $scope.model.lines.push(line);
            return line;
        }

        function addLineError(title, error) {
            var line = formatLine({
                title: title,
                statusText: 'Error: ' + error,
                statusIcon: 'fa-ban',
                statusFull: '' + error
            });
            $scope.model.lines.push(line);
            return line;
        }

        function addProgressLine(title) {
            var line = formatLine({
                title: 'Import: ' + title,
                titleTip: title,
                statusText: 'Importing...',
                statusIcon: 'fa-spinner fa-pulse'
            });
            $scope.model.lines.push(line);
            return line;
        }


        function importLine(columnsMap, line, lineIdx) {
            var data = {};
            var errorColumns = [];
            _(importColumns).each(function (col) {
                if (_(columnsMap[col.id]).isUndefined()) {
                    if (col.default) {
                        data[col.id] = col.default;
                    }
                } else {
                    switch(col.type) {
                        case 'json':
                            try {
                                data[col.id] = JSON.parse(line[columnsMap[col.id]]);

                            } catch (e) {
                                errorColumns.push(col.title);
                            }
                            break;
                        case 'boolean':
                            try {
                                data[col.id] = JSON.parse(line[columnsMap[col.id]]);
                                if (!_(data[col.id]).isBoolean()) {
                                    throw 'Parsed value is not boolean';
                                }

                            } catch (e) {
                                errorColumns.push(col.title);
                            }
                            break;
                        case 'string':
                            data[col.id] = '' + line[columnsMap[col.id]];
                            break;
                        default:
                            data[col.id] = line[columnsMap[col.id]];
                    }
                    if (col.allowed && !_(col.allowed).contains(data[col.id])) {
                        errorColumns.push(col.title);
                    }
                }
            });
            var logTitle = (data.name || 'at line ' + (lineIdx + 1));
            if (errorColumns.length) {
                addLineError(logTitle, 'Wrong data in column(s): \'' + errorColumns.join('\', \'') + '\'');
                return;
            }
            if (data.ui_info && data.ui_info.mode === 'builder' && dlpRuleBuilder(data.ui_info.builder) != data.value) {
                addLineError(logTitle, 'Rule wizard configuration doesn\'t match query value');
                return;
            }
            var statusLine = addProgressLine(logTitle);

            return dlpConfigurationDao.createRule(data).then(function (response) {
                if (!response.data.success) {
                    return saveRuleError(response.data.message, statusLine);
                }
                var status = {
                    statusText: 'OK',
                    statusIcon: 'fa-check'
                };
                statusLine.widget.lines[1] = partIcon(status);
                statusLine.widget.lines[2] = partStatusText(status);
            }, function (error) {
                var msg = error.data.message;
                if (_(msg).isObject()) {
                    msg = _(error.data.message).map(function (val, key) {
                        return val;
                    }).join(',\n ') || 'Unkown error';
                }
                return saveRuleError(msg, statusLine);
            });
        }

        function saveRuleError (message, statusLine) {
            var status = {
                statusText: 'Error: ' + message,
                statusIcon: 'fa-ban',
                statusFull: '' + message
            };
            statusLine.widget.lines[1] = partIcon(status);
            statusLine.widget.lines[2] = partStatusText(status);
        }

        function importStart(content) {
            var columnsMap = {};
            var data;
            try {
                data = CSV.parse(content);
                addLineSuccess('Read import file');
            } catch (e) {
                addLineError('Read import file', e);
                return;
            }
            _(data[0]).each(function (title, idx) {
                var found = _(importColumns).find(function (col) {
                    return col.title == title;
                });
                if (found) {
                    columnsMap[found.id] = idx;
                }
            });
            var missingCols = [];
            _(importColumns).each(function (col) {
                if (_(columnsMap[col.id]).isUndefined() && col.required) {
                    missingCols.push(col.title);
                }
            });
            if (missingCols.length > 0) {
                addLineError('Required columns check', 'Missing column(s): ' + missingCols.join(', '));
                return;
            } else {
                addLineSuccess('Required columns check');
            }
            var promises = [];
            _(data).chain().rest().each(function (line, idx) {
                promises.push(importLine(columnsMap, line, idx));
            });
            if (logic.onDone) {
                $q.all(promises).then(function () {
                    logic.onDone();
                });
            }
        }

        $scope.model.lines = [];
        importStart(logic.content);



        return _.noop;

    };
});
