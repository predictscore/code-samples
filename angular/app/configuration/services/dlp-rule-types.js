var m = angular.module('configuration');

m.factory('dlpRuleTypes', function() {
    return [{
        id: 'simple_search',
        label: 'Simple Search'
    }, {
        id: 'query_language',
        label: 'Query Language'
    }, {
        id: 'regexp',
        label: 'Regular Expression'
    }, {
        id: 'password_protected',
        label: 'Password Protected',
        noValue: true
    }];
});