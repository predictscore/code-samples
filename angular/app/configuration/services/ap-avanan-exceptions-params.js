var m = angular.module('configuration');

m.factory('apAvananExceptionsParams', function(modals, $q, apAvananExceptionsDao, $injector) {

    var fileDao;

    function emailDomain(email) {
        if (!email) {
            return null;
        }
        var matched = email.match(/@([^@]+)$/);
        if (!matched || matched.length !== 2) {
            return null;
        }
        return matched[1];
    }

    var excludeTypes = [{
        id: 'emails',
        label: 'Excluded Emails',
        addButtonLabel: 'Add emails',
        paramsLabel: 'Emails',
        addTitle: "Add emails to exceptions list",
        typeName: 'Email',
        linkTitle: 'I trust this email address',
        ignoreSPFOption: true
    }, {
        id: 'ips',
        label: 'Excluded IPs',
        addButtonLabel: 'Add IPs',
        paramsLabel: 'IPs',
        addTitle: "Add IP addresses to exceptions list",
        typeName: 'IP',
        linkTitle: 'I trust all emails coming from this IP address'
    }, {
        id: 'domains',
        label: 'Excluded Domains',
        addButtonLabel: 'Add Domains',
        paramsLabel: 'Domains',
        addTitle: "Add domains to exceptions list",
        typeName: 'Domain',
        linkTitle: 'I trust all emails from this domain',
        ignoreSPFOption: true
    }, {
        id: 'nicknames',
        label: 'Excluded Nicknames',
        addButtonLabel: 'Add Nickname',
        paramsLabel: 'Nicknames',
        addTitle: "Add nicknames to exceptions list",
        typeName: 'Nickname',
        linkTitle: 'Ignore this nickname'
    }];

    var excludeTypesMap = _(excludeTypes).chain().map(function (type) {
        return [type.id, type];
    }).object().value();


    var apAvananExceptionsParams = {
        excludeTypes: excludeTypes,
        addDialog: function(type, addParams) {
            var typeInfo = excludeTypesMap[type];
            if (!typeInfo) {
                return $q.reject();
            }
            var modalParams = [{
                id: 'ids',
                type: "list",
                label: typeInfo.paramsLabel,
                validation: {
                    required: true
                },
                data: addParams && addParams.data
            }, {
                id: 'comment',
                type: 'text',
                label: 'Comment (optional)',
                paramClass: 'text-param-average'
            }];

            if (typeInfo.ignoreSPFOption) {
                modalParams.push({
                    id: 'ignoring_spf_check',
                    type: 'boolean',
                    rightLabel: 'Ignore SPF check',
                    emptyLabel: true
                });
            }

            if (addParams && addParams.message) {
                modalParams.unshift({
                    type: 'message',
                    message: addParams.message,
                    paramClass: 'full-width-param'
                });
            }
            return modals.params([{
                lastPage: true,
                params: modalParams
            }], typeInfo.addTitle).then(function (data) {
                var addData = {};
                addData[type] = _(data.ids).map(function (id) {
                    var item = {
                        value: id,
                        comment: data.comment || null
                    };
                    if (typeInfo.ignoreSPFOption) {
                        item.ignoring_spf_check = data.ignoring_spf_check;
                    }
                    return item;
                });
                return apAvananExceptionsDao.add(addData).then(function (response) {
                    return response;
                }, function (error) {
                    return modals.alert('Error adding ' + data.ids.join(', ') +  ' as ' + typeInfo.typeName + ' exception for Smart-Phish', {
                        title: 'Error'
                    }).then(function () {
                        return $q.reject(error);
                    }, function () {
                        return $q.reject(error);
                    });
                });
            });
        },
        addByEntity: function (type, entityType, entityId) {
            var typeInfo = excludeTypesMap[type];
            if (!typeInfo) {
                return $q.reject();
            }
            var addParams = {};
            if (!fileDao) {
                fileDao = $injector.get('fileDao');
            }
            return fileDao.entityInfo(entityType, entityId).then(function (response) {
                var entityInfo = response.data;
                var userQueries;
                if (!entityInfo) {
                    return $q.reject();
                }
                if (type === 'ips') {
                    if (entityInfo.sender_ip) {
                        addParams.data = [entityInfo.sender_ip];
                    } else {
                        addParams.message = 'Unable to find Sender\'s IP address. You can enter it manually if you know it';
                    }
                    return;
                }
                if (type === 'emails' || type === 'no_spf_emails') {
                    if (entityType === 'google_mail_email' && entityInfo.from) {
                        addParams.data = [entityInfo.from];
                        return;
                    }
                    if (entityType === 'office365_emails_email' && (entityInfo.Sender || entityInfo.From)) {
                        userQueries = [];
                        if (entityInfo.Sender) {
                            userQueries.push(fileDao.entityInfo('office365_emails_user', entityInfo.Sender));
                        }
                        if (entityInfo.From) {
                            userQueries.push(fileDao.entityInfo('office365_emails_user', entityInfo.From));
                        }
                        return $q.all(userQueries).then(function (responses) {
                            var emails = _(responses).chain().map(function (response) {
                                return [response.data.mail, response.data.primary_email];
                            }).flatten().compact().uniq().value();

                            if (emails.length) {
                                addParams.data = emails;
                            } else {
                                addParams.message = 'Unable to find Sender\'s Email address. You can enter it manually if you know it';
                            }
                        });
                    }
                    addParams.message = 'Unable to find Sender\'s Email address. You can enter it manually if you know it';
                }
                if (type === 'domains') {
                    if (entityType === 'google_mail_email' && entityInfo.from) {
                        addParams.data = _([emailDomain(entityInfo.from)]).compact();
                        if (addParams.data.length) {
                            return;
                        }
                    }
                    if (entityType === 'office365_emails_email' && entityInfo.From) {
                        userQueries = [];
                        if (entityInfo.From) {
                            userQueries.push(fileDao.entityInfo('office365_emails_user', entityInfo.From));
                        }
                        return $q.all(userQueries).then(function (responses) {
                            var emails = _(responses).chain().map(function (response) {
                                return [emailDomain(response.data.mail), emailDomain(response.data.primary_email)];
                            }).flatten().compact().uniq().value();

                            if (emails.length) {
                                addParams.data = emails;
                            } else {
                                addParams.message = 'Unable to find Sender\'s Domain. You can enter it manually if you know it';
                            }
                        });
                    }
                    addParams.message = 'Unable to find Sender\'s Domain. You can enter it manually if you know it';
                }
                if (type === 'nicknames') {
                    if (entityType === 'google_mail_email' && entityInfo.fromName) {
                        addParams.data = [entityInfo.fromName];
                        return;
                    }
                    if (entityType === 'office365_emails_email' && entityInfo.From) {
                        userQueries = [];
                        if (entityInfo.From) {
                            userQueries.push(fileDao.entityInfo('office365_emails_user', entityInfo.From));
                        }
                        return $q.all(userQueries).then(function (responses) {
                            var nicknames = _(responses).chain().map(function (response) {
                                return [entityInfo.fromName, response.data.displayName, response.data.mailNickname];
                            }).flatten().compact().uniq().value();

                            if (nicknames.length) {
                                addParams.data = nicknames;
                            } else {
                                addParams.message = 'Unable to find Sender\'s Nickname. You can enter it manually if you know it';
                            }
                        });
                    }
                    addParams.message = 'Unable to find Sender\'s Nickname. You can enter it manually if you know it';
                }
            }).then(function () {
                return apAvananExceptionsParams.addDialog(type, addParams);
            });
        },
        exceptionLinks: function (entityId, entityType, eventName) {
            return _(excludeTypes).map(function (typeInfo) {
                return '#' + JSON.stringify({
                    display: 'text',
                    'class': 'display-block link-like',
                    text: typeInfo.linkTitle,
                    clickEvent: {
                        name: eventName,
                        entityType: entityType,
                        entityId: entityId,
                        exceptionType: typeInfo.id
                    }
                });
            });
        }
    };

    return apAvananExceptionsParams;

});