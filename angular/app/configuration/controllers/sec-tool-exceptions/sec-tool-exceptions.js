var m = angular.module('configuration');

m.controller('SecToolExceptionsController', function($scope, path, modulesManager, tablePaginationHelper, fileDao, routeHelper, $q, modals, sideMenuManager, entityTypes, $route, feature, widgetSettings, columnsHelper, $routeParams, secToolsExceptionParams, avananUserDao) {

    $scope.enabledSaass = _(modulesManager.activeSaasApps()).map(function (saas) {
        return {
            img: saas.img,
            name: saas.name,
            title: saas.title,
            link: routeHelper.getPath('sec-tool-exceptions', {
                module: saas.name
            })
        };
    });

    $scope.currentModule = $routeParams.module;

    $scope.saasEntities = {
        'office365_emails': [{
            type: 'email',
            label: 'Emails'
        }, {
            type: 'file',
            label: 'Attachments'
        }],
        'google_mail': [{
            type: 'email',
            label: 'Emails'
        }, {
            type: 'file',
            label: 'Attachments'
        }],
        'google_drive': [{
            type: 'file',
            label: 'Files'
        }],
        'office365_onedrive': [{
            type: 'file',
            label: 'Files'
        }]
    };

    if ($scope.enabledSaass.length && !_($scope.enabledSaass).find(function (saas) {
            return saas.name === $scope.currentModule;
        })) {
        routeHelper.redirectTo('sec-tool-exceptions', {
            module: $scope.enabledSaass[0].name
        });
        return;
    }


    $scope.resultsPanel = {
        title: 'Loading...',
        'class': 'light white-body white-header',
        "bodyStyle": {
            "min-height": "500px"
        }
    };

    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 20,
        pagination: {
            page: 1,
            ordering: {}
        },
        saveState: true,
        showWorkingIndicator: true,
        disableTop: true
    };

    $scope.moduleEntityTypes = [];

    var staticFilePrefix = 'common';
    var queryModule = 'common';
    var queryPostfix = '';
    if ($scope.saasEntities[$scope.currentModule]) {
        staticFilePrefix = $scope.currentModule + '.';
        queryModule = $scope.currentModule;
        $scope.tableOptions.pagination.filter = '';
        if ($routeParams.type && _($scope.saasEntities[$scope.currentModule]).find(function (entity) {
                return entity.type === $routeParams.type;
            })) {
            $scope.currentEntity = $routeParams.type;
        } else {
            $scope.currentEntity = $scope.saasEntities[$scope.currentModule][0].type;
        }
        staticFilePrefix += $scope.currentEntity;

        _($scope.saasEntities[$scope.currentModule]).each(function (entity) {
            entity.link = routeHelper.getPath('sec-tool-exceptions', {
                module: $scope.currentModule,
                type: entity.type
            });
        });
        queryPostfix = '_' + $scope.currentEntity;
        $scope.moduleEntityTypes.push(entityTypes.toBackend($scope.currentModule, $scope.currentEntity));
    } else {
        _(['file', 'email']).each(function (localType) {
            var backendType = entityTypes.toBackend($scope.currentModule, localType);
            if (backendType) {
                $scope.moduleEntityTypes.push(backendType);
            }
        });
    }


    $q.all([
        path('configuration').ctrl('sec-tool-exceptions').json(staticFilePrefix + '.sec-tool-exceptions').retrieve(),
        avananUserDao.users()
    ]).then(function (responses) {
        $scope.options = responses[0].data;
        $scope.userMap = _(responses[1].data.rows).chain().map(function (row) {
            return [row.id, {
                email: row.email,
                name: _([row.first_name, row.last_name]).compact().join(' ')
            }];
        }).object().value();

        $scope.tableOptions.actions = [{
            label: 'Remove from exceptions...',
            display: 'button',
            active: function () {
                return $scope.tableHelper.getSelectedIds().length > 0;
            },
            execute: function() {
                var selectedEntities = $scope.tableHelper.getSelectedIds();

                return modals.confirm('Are you sure you want to remove selected items from exceptions?').then(function(data) {
                    return fileDao.exceptionsRemove(_(selectedEntities).map(function (entity) {
                        return {
                            entity_type: entity.entity_type,
                            entity_id: entity.entity_id,
                            sec_type: entity.sec_type
                        };
                    })).then(function () {
                        $scope.tableHelper.clearSelection();
                        $scope.tableHelper.reload();
                    });
                });
            }
        }];

        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
            var q = fileDao.exceptionsList(queryModule, $scope.options.columns, $scope.userMap, queryPostfix);
            q.params({
                entity_types: $scope.moduleEntityTypes.join(',')
            });

            if (args.columnsFilters) {
                q.params(columnsHelper.filtersToParams(args.columnsFilters));
            }

            return q.pagination(args.offset, args.limit).filter(args.filter)
                .order(args.ordering.columnName, args.ordering.order, 'last');
        }, function(tableModel) {
            $scope.tableModel = tableModel;
            $scope.resultsPanel.title = $scope.tableHelper.getTotal() + ' exceptions';
        });


    });

    sideMenuManager.treeNavigationPath(['configuration', 'sec-tool-exceptions']);
});
