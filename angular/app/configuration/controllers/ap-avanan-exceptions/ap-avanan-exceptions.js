var m = angular.module('configuration');

m.controller('ApAvananExceptionsController', function($scope, path, modulesManager, tablePaginationHelper, routeHelper, $q, modals, sideMenuManager, $route, columnsHelper, $routeParams, avananUserDao, apAvananExceptionsDao, apAvananExceptionsParams) {

    $scope.excludeTypes = angular.copy(apAvananExceptionsParams.excludeTypes);

    _($scope.excludeTypes).each(function (typeInfo, idx) {
        var params;
        if (idx > 0) {
            params = {
                type: typeInfo.id
            };
        }
        typeInfo.link = routeHelper.getPath('ap-avanan-exceptions', params);
    });

    $scope.excludeType = $routeParams.type;

    if (!$scope.excludeType || !($scope.excludeTypeInfo = _($scope.excludeTypes).find(function (typeInfo) {
            return typeInfo.id === $scope.excludeType;
        }))) {
        $scope.excludeTypeInfo = $scope.excludeTypes[0];
        $scope.excludeType = $scope.excludeTypeInfo.id;
    }

    $scope.resultsPanel = {
        title: 'Loading...',
        'class': 'light white-body white-header',
        "bodyStyle": {
            "min-height": "500px"
        }
    };

    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 20,
        pagination: {
            page: 1,
            ordering: {},
            filter: ''
        },
        saveState: true,
        showWorkingIndicator: true,
        disableTop: true,
        actions: [{
            label: 'Remove',
            display: 'button',
            active: function () {
                return $scope.tableHelper.getSelectedIds().length > 0;
            },
            execute: function() {
                var selectedEntities = $scope.tableHelper.getSelectedIds();

                return modals.confirm('Are you sure you want to remove selected items from exceptions?').then(function() {
                    var removeData = {};
                    _(selectedEntities).each(function (entity) {
                        removeData[entity.type] = removeData[entity.type] || [];
                        removeData[entity.type].push(entity.entity_id);
                    });
                    return apAvananExceptionsDao.remove(removeData).then(function () {
                        $scope.tableHelper.clearSelection();
                        $scope.tableHelper.reload();
                    });
                });
            }
        }, {
            label: $scope.excludeTypeInfo.addButtonLabel,
            display: 'button',
            execute: function() {
                return apAvananExceptionsParams.addDialog($scope.excludeType).then(function () {
                    $scope.tableHelper.clearSelection();
                    $scope.tableHelper.reload();
                });
            }
        }]
    };


    $q.all([
        path('configuration').ctrl('ap-avanan-exceptions').json('ap-avanan-exceptions' + ($scope.excludeTypeInfo.ignoreSPFOption ? '-with-spf': '')).retrieve(),
        avananUserDao.users()
    ]).then(function (responses) {
        $scope.options = responses[0].data;
        $scope.userMap = _(responses[1].data.rows).chain().map(function (row) {
            return [row.id, {
                email: row.email,
                name: row.first_name + ' ' + row.last_name
            }];
        }).object().value();
        $scope.options.columns[columnsHelper.getIdxById($scope.options.columns, 'entity_id')].text = $scope.excludeTypeInfo.typeName;

        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
            var q = apAvananExceptionsDao.list($scope.excludeType, $scope.options.columns, $scope.userMap);
            if (args.columnsFilters) {
                q.params(columnsHelper.filtersToParams(args.columnsFilters));
            }
            return q.pagination(args.offset, args.limit).filter(args.filter)
                .order(args.ordering.columnName, args.ordering.order, 'last');
        }, function(tableModel) {
            $scope.tableModel = tableModel;
            $scope.resultsPanel.title = $scope.tableHelper.getTotal() + ' exceptions';
        });

    });

    sideMenuManager.treeNavigationPath(['configuration', 'ap-avanan-exceptions']);
});
