var m = angular.module('configuration');

m.controller('AnomalyDetectionConfigController', function($scope, $q, anomalyDetectionDao, path, tablePaginationHelper, modals, $timeout, modulesDao, sideMenuManager, columnsHelper, scriptConverter) {
    $scope.configPanel = {
        title: "Anomaly Detection",
        'class': 'light white-body white-header'
    };

    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 10000,
        pagination: {
            page: 1,
            ordering: {},
            filter: ''
        },
        saveState: true
    };
    $scope.totalUsers = 0;

    path('configuration').ctrl('anomaly-detection-config').json('anomaly-detection-config').retrieve().then(function(response) {
        $scope.options = response.data;
        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
            return anomalyDetectionDao.list($scope.options.columns);
        }, function(tableModel) {
            $scope.tableModel = tableModel;
        });

        $scope.$watchCollection('tableHelper.getSelected()', function(selected) {
            if(_(selected).isUndefined() || selected.length === 0) {
                return;
            }
            var anomalyId = selected[0][columnsHelper.getIdxById($scope.options.columns, 'id')].originalText;
            $scope.tableHelper.clearSelection();
            if (!anomalyId) {
                return;
            }
            var item;
            anomalyDetectionDao.retrieve(anomalyId).then(function (response) {
                item = response.data;
                return scriptConverter({
                    data: item.arguments || {}
                });
            }).then(function (response) {
                _(response.data.pages).first().params.push({
                    type: 'boolean',
                    emptyLabel: true,
                    data: item.enable,
                    id: 'enable',
                    rightLabel: 'Enable this algorithm'
                });
                response.data.title = 'Configure ' + item.title;
                return modals.params(response.data.pages, response.data.title).then(function (params) {
                    return anomalyDetectionDao.update(item.id, {
                        arguments: _(params).chain().omit('enable').map(function (value, key) {
                            return [key, {
                                value: value
                            }];
                        }).object().value(),
                        enable: params.enable
                    });
                });

            }).then(function () {
                $scope.tableHelper.reload();
            });
        });

    });
    
    sideMenuManager.treeNavigationPath(['configuration', 'anomaly-detection']);
});