var m = angular.module('configuration');

m.controller('SystemLogController', function($scope, widgetPolling, sizeClasses, path) {
    widgetPolling.updateWidgets(
        path('configuration').ctrl('system-log').json('system-log').path
    );
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
});