var m = angular.module('configuration');

m.controller('DlpConfigurationListController', function($scope, $q, dlpConfigurationDao, path, tablePaginationHelper, dlpRuleEdit, modals, tableExportHelper, $timeout, dlpRulesImportLogic, modulesDao, scriptConverter, sideMenuManager) {
    $scope.configurationListPanel = {
        title: "Smart Search",
        'class': 'light white-body white-header',
        buttons: [{
            position: 'main',
            iconClass: 'fa fa-cogs',
            'class': 'btn-icon',
            execute: function () {
                return modulesDao.retrieveConfig('dlp_avanan').then(function (response) {
                    return scriptConverter({
                        data: response.data || {}
                    });
                }).then(function (response) {
                    return modals.params(response.data.pages, 'Smart Search Configuration');
                }).then(function(params) {
                    params = params || {};
                    return modulesDao.saveConfig('dlp_avanan', params);
                });
            }
        }]
    };
    $scope.$on('edit-rule', function(event, data) {
        dlpRuleEdit.editRule(data.attributes.ruleId).then(function () {
            $scope.tableHelper.reload();
        });
    });

    function removeError(message) {
        return modals.alert(message || 'Unknown error removing rule', {title: 'Error'});
    }
    
    $scope.$on('remove-rule', function(event, data) {
        modals.confirm({
            message:'Are you sure you want to delete this rule?',
            title: 'Delete smart search rule'
        }).then(function () {
            dlpConfigurationDao.removeRule(data.attributes.row.id).then(function (response) {
                if (!response.data.success) {
                    removeError(response.data.message);
                }
                $scope.tableHelper.reload();
            }, function (error) {
                removeError(error.data && error.data.message);
            });
        });
    });

    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 10000,
        pagination: {
            page: 1,
            ordering: {},
            filter: ''
        },
        saveState: true,
        actions: [{
            label: 'Export',
            display: 'button',
            execute: function () {
                tableExportHelper('csv', 'smart-search-rules', $scope.tableHelper);
            }
        }, {
            label: 'Import',
            display: 'button',
            execute: function () {
                $timeout(function() {
                    $('#dlp-import-file').trigger('click');
                });
            }
        }, {
            label: 'Create New Rule',
            display: 'button',
            execute: function() {
                return dlpRuleEdit.createRule().then(function () {
                    $scope.tableHelper.reload();
                });
            }
        }]
    };
    $scope.totalUsers = 0;

    $scope.importFileReadComplete = function (content) {
        var input = $('#dlp-import-file');
        input.replaceWith(input.val('').clone(true));
        modals.widget({
            panel: {
                "type": "info",
                "logic": {
                    "name": "dlpRulesImportLogic",
                    "updateTick": null,
                    "content": content,
                    "onDone": importFinished
                },
                "class": "dlp-rules-import-dialog",
                "wrapper": {
                    "type": "widget-panel",
                    "title": "Import Smart Search Rules",
                    "class": "avanan white-body overflow-auto"
                }
            }
        });
    };

    function importFinished() {
        $scope.tableHelper.reload();
    }

    path('configuration').ctrl('dlp-configuration-list').json('list-conf').retrieve().then(function(response) {
        $scope.options = response.data;
        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
            return dlpConfigurationDao.listRules($scope.options.columns, args.export);
        }, function(tableModel) {
            $scope.tableModel = tableModel;
        });
    });
    
    sideMenuManager.treeNavigationPath(['configuration', 'dlp-configuration']);
});