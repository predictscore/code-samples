var m = angular.module('configuration');

m.controller('SolgateExceptionsController', function($scope, path, modulesManager, tablePaginationHelper, routeHelper, $q, modals, sideMenuManager, $route, columnsHelper, $routeParams, avananUserDao, solgateExceptionsDao, solgateExceptionsParams) {

    $scope.resultsPanel = {
        title: 'Loading...',
        'class': 'light white-body white-header',
        "bodyStyle": {
            "min-height": "500px"
        }
    };

    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 20,
        pagination: {
            page: 1,
            ordering: {},
            filter: ''
        },
        saveState: true,
        showWorkingIndicator: true,
        disableTop: true,
        actions: [{
            label: 'Remove',
            display: 'button',
            active: function () {
                return $scope.tableHelper.getSelectedIds().length > 0;
            },
            execute: function() {
                var selectedEntities = $scope.tableHelper.getSelectedIds();
                return modals.confirm('Are you sure you want to remove selected hashes from exceptions?').then(function() {
                    var removeData = {
                        macro_hash: []
                    };
                    _(selectedEntities).each(function (entity_id) {
                        removeData.macro_hash.push(entity_id);
                    });
                    return solgateExceptionsDao.remove(removeData).then(function () {
                        $scope.tableHelper.clearSelection();
                        $scope.tableHelper.reload();
                    });
                });
            }
        }, {
            label: 'Add macro exception',
            display: 'button',
            execute: function() {
                return solgateExceptionsParams.addDialog().then(function () {
                    $scope.tableHelper.clearSelection();
                    $scope.tableHelper.reload();
                });
            }
        }]
    };


    $q.all([
        path('configuration').ctrl('solgate-exceptions').json('solgate-exceptions').retrieve(),
        avananUserDao.users()
    ]).then(function (responses) {
        $scope.options = responses[0].data;
        $scope.userMap = _(responses[1].data.rows).chain().map(function (row) {
            return [row.id, {
                email: row.email,
                name: row.first_name + ' ' + row.last_name
            }];
        }).object().value();

        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
            var q = solgateExceptionsDao.list($scope.options.columns, $scope.userMap);
            if (args.columnsFilters) {
                q.params(columnsHelper.filtersToParams(args.columnsFilters));
            }
            return q.pagination(args.offset, args.limit).filter(args.filter)
                .order(args.ordering.columnName, args.ordering.order, 'last');
        }, function(tableModel) {
            $scope.tableModel = tableModel;
            $scope.resultsPanel.title = $scope.tableHelper.getTotal() + ' exceptions';
        });

    });

    sideMenuManager.treeNavigationPath(['configuration', 'solgate-exceptions']);
});
