var m = angular.module('user-management');

m.controller('UserListController', function($scope, $q, tablePaginationHelper, routeHelper, path, avananUserDao, wizardHelper, sideMenuManager, userEdit, columnsHelper, modals) {
    if(!sideMenuManager.currentUser().admin) {
        routeHelper.redirectTo('dashboard', {});
    }
    $scope.wizardState = wizardHelper.getState();
    $scope.wizardUrl = $scope.userManagementUrl = '/#!' + routeHelper.getPath('wizard');

    $scope.userListPanel = {
        title: "Users",
        'class': 'light white-body white-header'
    };
    var actions = [{
        label: 'Delete',
        active: function() {
            var selected = $scope.tableHelper.getSelectedIds();
            return selected.length >= 1 && selected.length < $scope.totalUsers;
        },
        execute: function() {
            var emailColumnIdx = columnsHelper.getIdxById($scope.options.columns, 'email');
            var selected = $scope.tableHelper.getSelectedIds();
            return modals.confirm({
                message: 'Are you sure you want to delete user' + (selected.length > 1 ? 's' : '') +
                '<div class="paddingLR-sm">' + _($scope.tableHelper.getSelected()).map(function (row) {
                    return row[emailColumnIdx].originalText;
                }).join('<br>') + '</div>'
            }).then(function () {
                return $q.all(_(selected).map(function(selected) {
                    return avananUserDao.remove(selected);
                }));
            }).then(function() {
                $scope.tableHelper.clearSelection();
                $scope.tableHelper.reload();
            });
        }
    }, {
        label: 'Un-lock',
        active: function() {
            return !_($scope.authLockedColumnIdx).isUndefined() && !!$scope.tableHelper.getSelected().find(function (row) {
                    return row[$scope.authLockedColumnIdx] && row[$scope.authLockedColumnIdx].originalText;
                });
        },
        execute: function() {
            var rows = _($scope.tableHelper.getSelected()).filter(function (row) {
                return row[$scope.authLockedColumnIdx] && row[$scope.authLockedColumnIdx].originalText;
            });
            return unlockUsers(rows);
        }
    }];
    actions.push({
        label: 'Create New User',
        display: 'button',
        execute: function() {
            return userEdit.createUser().then(function () {
                $scope.tableHelper.reload();
            });
        }
    });
    $scope.$on('edit-user', function(event, data) {
        userEdit.editUser(data.attributes.userId).then(function () {
            $scope.tableHelper.reload();
        });
    });

    function unlockUsers(rows) {
        var emailColumnIdx = columnsHelper.getIdxById($scope.options.columns, 'email');
        return modals.confirm({
            message: 'Ability of user' + (rows.length > 1 ? 's' : '') +
            '<div class="paddingLR-sm">' + _(rows).map(function (row) {
                return row[emailColumnIdx].originalText;
            }).join('<br>') + '</div>' +
           'to login with password was blocked due to too many failed login attempts.' +
                '<br />Are you sure you want to un-lock ' + (rows.length > 1 ? 'these users' : 'this user') + '?',
            title: 'Un-lock User',
            okText: 'Un-lock'
        }).then(function () {
            var ids = _(rows).map(function (row) {
                return row[$scope.userIdColumnIdx].text;
            });
            return $q.all(_(ids).map(function(id) {
                return avananUserDao.authUnlock(id);
            }));
        }).then(function() {
            $scope.tableHelper.clearSelection();
            $scope.tableHelper.reload();
        });
    }

    $scope.$on('unlock-user', function(event, data) {
        var row = _($scope.tableModel.data).find(function (row) {
            return row[$scope.userIdColumnIdx].text === data.attributes.userId;
        });
        if (!row) {
            return;
        }
        return unlockUsers([row]);
    });


    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 10000,
        pagination: {
            page: 1,
            ordering: {},
            filter: ''
        },
        saveState: true,
        actions: actions
    };
    $scope.totalUsers = 0;

    path('user-management').ctrl('user-list').json('list-conf').retrieve().then(function(response) {
        $scope.options = response.data;
        $scope.authLockedColumnIdx = columnsHelper.getIdxById($scope.options.columns, 'auth_locked');
        $scope.userIdColumnIdx = columnsHelper.getIdxById($scope.options.columns, 'id');
        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
            return avananUserDao.list(args.offset / args.limit + 1, args.limit, $scope.options.columns)
                .preFetch(avananUserDao.count(), "totalUsers")
                .converter(function (input, args) {
                    $scope.totalUsers = args.totalUsers.data;
                    return $q.when(input);
                }, _.identity);
        }, function(tableModel) {
            $scope.tableModel = tableModel;
        });
    });
    
    sideMenuManager.treeNavigationPath(['configuration', 'user-management']);
});