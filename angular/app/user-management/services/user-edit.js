var m = angular.module('user-management');

m.factory('userEdit', function($q, modals, avananUserDao, sideMenuManager) {

    function showUserDialog(user, params, options, data) {
        var modalParams = angular.copy(params);
        if (data) {
            _(modalParams).each(function (param) {
                param.data = data[param.id];
            });
        }
        return modals.params([{
            lastPage: true,
            params: modalParams,
            okButton : options.okButton
        }], options.title).then(function (data) {
            return saveUser(user, params, options, data);
        });
    }

    function saveUser(user, params, options, data) {
        if (sideMenuManager.currentUser().admin) {
            var allow_only = _(userEdit.loginOptions).chain().map(function (option) {
                if (data['allowLogin' + option.id]) {
                    return option.id;
                }
            }).compact().value();
            if (allow_only.length === 0) {
                return modals.alert('Impossible to set no login options to user. Please select something.', {title: 'Error'})
                    .then(function () {
                        return showUserDialog(user, params, options, data);
                    });
            }
            user.allow_only = allow_only;
            user.admin = data.admin;
            user.view_private_data = data.view_private_data;
            if (options.userId && data.changePassword) {
                user.password = data.password;
            }
        }
        $.extend(user, _(data).pick('first_name', 'last_name', 'email', 'description'));
        var promise;
        if (options.userId) {
            promise = avananUserDao.update(options.userId, user);
        } else {
            promise = avananUserDao.create(user);
        }
        return promise.then(_.noop, function (error) {
            modals.alert(error.data.message, {title: 'Error'}).then(function () {
                return showUserDialog(user, params, options, data);
            });
        });
    }

    function userDialog(user) {
        var options = {};
        if (user) {
            options.title = 'Update user';
            options.okButton = 'Update';
            options.userId = user.id;
        } else {
            options.title = 'Create new user';
            options.okButton = 'Create';
            user = {
                first_name: '',
                last_name: '',
                email: '',
                description: '',
                admin: false,
                allow_only: []
            };
        }
        var params = [{
            type: 'string',
            id: 'first_name',
            label: 'First name',
            data: user.first_name
        }, {
            type: 'string',
            id: 'last_name',
            label: 'Last name',
            data: user.last_name
        }, {
            type: 'string',
            id: 'email',
            label: 'Email *',
            data: user.email,
            validation: {
                required: {
                    message: 'Email is required'
                },
                'regexp': {
                    message: 'Please enter valid e-mail',
                    value: /.+@.+\..+/
                }
            }
        }, {
            type: 'string',
            id: 'description',
            label: 'Description',
            data: user.description
        }];
        if (sideMenuManager.currentUser().admin) {
            params.push({
                type: 'boolean',
                id: 'admin',
                emptyLabel: true,
                rightLabel: 'Admin',
                data: user.admin
            });
            params.push({
                type: 'boolean',
                id: 'view_private_data',
                emptyLabel: true,
                rightLabel: 'Allow Private Data Access',
                data: user.view_private_data
            });
            _(userEdit.loginOptions).each(function (option) {
                var enabled = !user.allow_only ||
                    !_(user.allow_only).isArray() ||
                    user.allow_only.length === 0 ||
                    _(user.allow_only).contains(option.id);
                params.push({
                    type: 'boolean',
                    id: 'allowLogin' + option.id,
                    emptyLabel: true,
                    rightLabel: option.label,
                    data: enabled
                });
            });
            if (options.userId) {
                params.push({
                    type: 'boolean',
                    id: 'changePassword',
                    emptyLabel: true,
                    rightLabel: 'Change User Password',
                    data: false,
                    enabled: [{
                        param: 'allowLoginpassword',
                        equal: true
                    }]
                });
                params.push({
                    type: 'password',
                    id: 'password',
                    label: 'New User Password',
                    enabled: [{
                        param: 'allowLoginpassword',
                        equal: true
                    }, {
                        param: 'changePassword',
                        equal: true
                    }],
                    validation: {
                        required: {
                            message: 'Password can not be empty'
                        }
                    }
                });
                params.push({
                    type: 'password',
                    id: 'password',
                    label: 'Re-type New Password',
                    enabled: [{
                        param: 'allowLoginpassword',
                        equal: true
                    }, {
                        param: 'changePassword',
                        equal: true
                    }],
                    validation: {
                        equalTo: {
                            message: 'Re-typed password doesn\'t match',
                            value: 'password'
                        }
                    }
                });
            }
        }
        return showUserDialog(user, params, options);
    }

    var userEdit = {
        loginOptions : [{
            id: 'google',
            label: 'Allow Google oauth login'
        }, {
            id: 'box',
            label: 'Allow Box oauth login'
        }, {
            id: 'password',
            label: 'Allow password login'
        }],
        createUser: function () {
            return userDialog();
        },
        editUser: function (userId) {
            return avananUserDao.retrieve(userId).then(function(user) {
                return userDialog(user.data);
            });
        }
    };

    return userEdit;

});