var m = angular.module('app-store');

m.controller('AppStoreController', function(path, $scope, widgetPolling, sizeClasses, $routeParams, $route, modulesDao, sideMenuManager, $q, modulesManager) {
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
    $scope.wrapperClass = 'app-store';

    widgetPolling.reload(function() {
        if(!$route.current.accessor) {
            return $q.when();
        }
        return modulesDao[$route.current.accessor]($routeParams.filter).then(function(response) {
            widgetPolling.setLocal(response.data, true, true);
        });
    });
    $scope.$on('saas-authorized', function () {
        modulesManager.reloadModules();
        widgetPolling.reload();
    });
    
    sideMenuManager.treeNavigationPath(['configuration', $route.current.name]);
});
