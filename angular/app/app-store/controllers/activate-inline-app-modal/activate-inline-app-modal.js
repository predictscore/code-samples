var m = angular.module('common');

m.controller('ActivateInlineAppModalController', function($scope, $modalInstance, args) {
    $scope.args = $.extend({
        title: 'Authorization required',
        okText: 'Next',
        cancelText: 'Cancel'
    }, args);
    
    $scope.cancel = function() {
        $modalInstance.dismiss($scope.result);
    };

});

m.factory('activateInlineAppModal', function($modal, path) {
    function createInstance(args, size) {
        if(!_(args).isObject()) {
            args = {
                message: args
            };
        }
        return $modal.open({
            templateUrl: path('app-store').ctrl('activate-inline-app-modal').template(),
            controller: 'ActivateInlineAppModalController',
            size: size,
            resolve: {
                args: _.constant(args)
            }
        });
    }
    return {
        show: function (args, size) {
            return createInstance(args, size).result;
        },
        dialog: function (args, size) {
            return createInstance(args, size);
        }
    };
});