var m = angular.module('app-store');

m.factory('cloudAppsConverter', function($q, modulePropsToOptions) {
    return function(input, args) {
        var deferred = $q.defer();

        var appStore = input.data;
        appStore.groups = _(appStore.groups).filter(function(group) {
            return group.family == 'saas';
        });

        var data = {
            widgets: _(input.data.groups).map(function(group) {
                return {
                    "type": "multi-widget",
                    "size": "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                    "wrapper": {
                        "type": "widget-panel",
                        "class": {
                            "store-group": true
                        },
                        "title": group.categoryLongName
                    },
                    "widgets": _(group.modules).map(function(module) {
                        return {
                            "type": 'store-item',
                            "size": 'col-lg-3 col-md-4 col-sm-6 col-xs-6',
                            "wrapper": {
                                "type": "none"
                            },
                            "options": modulePropsToOptions(module, {
                                "licenseButton": {
                                    label: 'Secure this app',
                                    'class': 'btn-secure'
                                }
                            }, true)
                        };
                    })
                };
            })
        };

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    };
});