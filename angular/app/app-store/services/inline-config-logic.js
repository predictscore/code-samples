var m = angular.module('app-store');

m.factory('inlineConfigLogic', function (inlinePolicyDao, commonDao, $q, configDao) {
    return function ($scope, logic, reinitLogic) {
        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        $scope.model.wrapper.isLoading = true;

        var lookupTest;
        if ($scope.model.userSelectorSettings && $scope.model.userSelectorSettings.lookup) {
            lookupTest = commonDao.genericLookup($scope.model.userSelectorSettings.lookup.endpoint)
                .pagination(0, 1);
        } else {
            lookupTest = $q.when({
                data: {
                    rows: []
                }
            });
        }
        $scope.showEmptyQuarantineAddressError = false;
        $scope.showEmptyUsersError = false;

        $scope.model.validate = function (getParam, getEnabledParam) {
            $scope.showEmptyQuarantineAddressError = false;
            $scope.showEmptyUsersError = false;

            if (!$scope.model.initialized) {
                return true;
            }
            if (!$scope.model.inlineActive) {
                return true;
            }
            var quarantineEmailParam = getEnabledParam('quarantine_email');
            if (quarantineEmailParam) {
                if (!quarantineEmailParam.data) {
                    $scope.showEmptyQuarantineAddressError = true;
                    return false;
                }
            }
            if ($scope.model.fetchType === 'only' && !$scope.model.users.length) {
                $scope.showEmptyUsersError = true;
                return false;
            }
            return true;
        };
        $scope.model.onsave = function (isEnabled) {
            if (!$scope.model.initialized) {
                return true;
            }

            var data;
            if (!$scope.model.inlineActive) {
                data = {
                    users: []
                };
            } else {
                switch ($scope.model.fetchType) {
                    case 'only':
                        data = {
                            users: $scope.model.users
                        };
                        break;
                    case 'all':
                        data = {
                            all: true,
                            users: []
                        };
                        break;
                    case 'allExcept':
                        data = {
                            except: true,
                            users: $scope.model.users
                        };
                        break;
                    default:
                        data = {
                            users: []
                        };
                }
            }
            return inlinePolicyDao.saveConfig(data, logic.app);
        };

        $scope.excludeEmails = function () {
            return _($scope.model.exclude_email_params).chain().map(function (paramId) {
                return $scope.model.refParams && $scope.model.refParams[paramId] && $scope.model.refParams[paramId].data;
            }).compact().value();
        };

        $q.all([inlinePolicyDao.retrieveConfig(logic.app),
            lookupTest,
            configDao.param('max_inline_users'),
            configDao.param('deployment_mode')
        ]).then(function (responses) {
            var inlineConfig = responses[0].data;
            var testedLookup = responses[1].data.rows;
            var deploymentMode = responses[3];
            var maxInlineUsers = responses[2];
            if (deploymentMode === 'poc') {
                $scope.maxUsers = (_(maxInlineUsers).isNull() ? 5 : maxInlineUsers);
            } else {
                $scope.maxUsers = null;
            }
            $scope.model.initialized = true;
            $scope.model.wrapper.isLoading = false;
            $scope.model.users = _(inlineConfig.rows).pluck('user_id');
            $scope.typeVariants = [];
            if (testedLookup.length) {
                $scope.typeVariants.push({
                    id: 'only',
                    label: 'Selected users only'
                });
            }
            if (!$scope.model.options || !$scope.model.options.disableProtectAll) {
                $scope.typeVariants.push({
                    id: 'all',
                    label: 'All'
                });
            }

            $scope.model.inlineActive = false;
            if (inlineConfig.all) {
                $scope.model.fetchType = 'all';
                $scope.model.inlineActive = true;
            } else {
                if ($scope.model.users.length) {
                    $scope.model.fetchType = 'only';
                    $scope.model.inlineActive = true;
                }
            }
            $scope.model.impossible = false;
            if (!_($scope.typeVariants).find(function (variant) {
                return variant.id === $scope.model.fetchType;
                })) {

                if (!$scope.typeVariants.length) {
                    $scope.model.impossible = true;
                    $scope.model.impossibleReason = 'No internal users found. Impossible to activate inline protection at the moment';
                    $scope.model.inlineActive = false;
                } else {
                    $scope.model.inlineActive = false;
                    $scope.model.fetchType = $scope.typeVariants[0].id;
                }
            }
        });
        var inlineActiveWatchStop = $scope.$watch('model.inlineActive', function (newValue) {
            $scope.model.selfParam.data = newValue;
        });

        return function () {
            inlineActiveWatchStop();
        };
    };
});
