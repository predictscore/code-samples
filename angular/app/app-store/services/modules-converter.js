var m = angular.module('app-store');

m.factory('modulesConverter', function($q) {

    return function (input, args) {

        var deferred = $q.defer();

        var data = {
            groups: {}
        };

        var stateWeight = {
            active: 0,
            enabled: 0,
            nonactive: 1,
            disabled: 1,
            display_only: 2
        };

        var appCategories = input.data.apps_categories.categories;
        var appItems = input.data.apps_items.modules;


        _(appCategories).each(function (category) {
            data.groups[category.categoryId] = $.extend(category, {
                modules: []
            });
        });

        _(appItems).each(function (module) {
            if (module.category_id && data.groups[module.category_id]) {
                data.groups[module.category_id].modules.push(module);
            }
        });


        data.groups = _(appCategories).chain().filter(function (category) {
            return category.modules.length > 0;
        }).sortBy(function (category) {
            return parseInt(category[args.categorySort]);
        }).map(function (category) {
            category.modules = _(category.modules).chain().sortBy(function (module) {
                return parseInt(module[args.moduleSort]);
            }).sortBy(function (module) {
                if (module.options && module.options.license === 'contact_support') {
                    return 6;
                }
                if (!module.license_info) {
                    return 5;
                }
                if (module.license_info.license_type === 'paid') {
                    if (module.isExpired) {
                        return 2;
                    }
                    return 1;
                }
                if (module.license_info.license_type === 'demo') {
                    if (module.isExpired) {
                        return 4;
                    }
                    return 3;
                }
                return 5;
            }).sortBy(function (module) {
                return stateWeight[module.state];
            }).value();
            return category;
        }).value();

        deferred.resolve($.extend({}, input, {
            data: data
        }));

        return deferred.promise;
    };
});