var m = angular.module('app-store');

m.factory('appOperationHelper', function() {
    var inProgress = {};
    return {
        operationStart: function (appName) {
            inProgress[appName] = true;
        },
        operationEnd: function (appName) {
            inProgress[appName] = false;
        },
        operationInProgress: function (appName) {
            if(_(appName).isUndefined()) {
                return !!_(inProgress).find(function(val) {
                    return val;
                });
            }
            return inProgress[appName];
        }
    };
});