var m = angular.module('app-store');

m.factory('appGroupMultiSelectLogic', function(modulesDao) {
    return function ($scope, logic) {
        function find(active) {
            return _($scope.model.widgets).chain().filter(function(widget) {
                if(active === true) {
                    return widget.options.moduleStatus == 'active';
                }
                return widget.options.moduleStatus != 'active';
            }).map(function(widget) {
                return widget.options.moduleName;
            }).value();
        }

        function updateButton() {
            var nonActiveModules = find(false);
            if(nonActiveModules.length) {
                $scope.model.wrapper.button.label = _.constant('Activate All');
                $scope.model.wrapper.button.class = 'store-button';
                $scope.model.wrapper.class.active = false;
            } else {
                $scope.model.wrapper.button.label = _.constant('Deactivate All');
                $scope.model.wrapper.button.class = 'store-button';
                $scope.model.wrapper.class.active = true;
            }
        }

        $scope.model.wrapper.button = {
            onclick: function () {
                var newState = 'active';
                var modules = find(false);
                if(!modules.length) {
                    modules = find(true);
                    newState = 'nonactive';
                }
                modulesDao.updateState(logic.categoryId, modules, newState).then(function() {
                    _($scope.model.widgets).each(function(widget) {
                        widget.options.moduleStatus = newState;
                    });
                    updateButton();
                });
            }
        };

        $scope.$watchCollection(function() {
            return find(false);
        }, function() {
            updateButton();
        });
    };
});