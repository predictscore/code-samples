var m = angular.module('app-store');

m.factory('appConfigurator', function($q, modals, modulesDao, widgetPolling, modulesManager, path, configDao, http, licenseOptions, scriptConverter, workingIndicatorHelper, commonDao, avananUserDao, sideMenuManager, modulePropsToOptions, feature) {

    var multiAccountsWidget = {
        "type": "info",
        "logic": {
            "name": "multiAccountsLogic",
            "updateTick": null
        },
        "class": "multi-accounts",
        "wrapper": {
            "type": "none"
        }
    };

    function boldify(str) {
        return '#' + JSON.stringify({
                display: 'text',
                'class': 'bold-text',
                text: str
            });
    }


    var loadConfig = function (options) {
        if (options.configuration == 'enabled') {
            return modulesDao.retrieveConfig(options.moduleName);
        } else {
            return $q.when({});
        }
    };

    function oauth(options) {
        modals.confirm({
            title: '',
            message: options.authMessage || 'In order to secure this application you are asked to provide admin-level oAuth authentication',
            okText: 'Perform oauth Authentication'
        }).then(function () {
            return http.get(options.auth);
        }).then(function(response) {
            window.open(response.data.url, '_blank').focus();
        });
    }

    var multipleAccounts = {
        servicenow: true
    };


    var appConfigurator = {
        isAuth: function (options) {
            return _(options.auth).isString();
        },
        showConfigurationLink: function (options) {
            return (!options.options || options.options.license !== 'contact_support') &&
                (_(options.configurationLink).isString() || options.authLinkApi);
        },
        configAvailable: function (options, noAuth) {
            return options.moduleStatus != 'display_only' && options.moduleName &&
                (!options.options || options.options.license !== 'contact_support') &&
                (options.configuration == 'enabled' ||
                (appConfigurator.showConfigurationLink(options) && !noAuth) || multipleAccounts[options.moduleName]);
        },
        convertConfigParams: function (config, app, options) {
            var preProcessTasks = _(config).chain().map(function (param, paramId) {
                if (!param.pre_process) {
                    return false;
                }

                if (param.pre_process.alter) {
                    var wrongCondition = _(param.pre_process.alter.conditions).find(function (condition) {
                        switch (condition.operator) {
                            case 'is': {
                                return !config[condition.param] || config[condition.param].value !== condition.value;
                            }
                        }
                        return true;
                    });
                    if (!wrongCondition) {
                        $.extend(true, param, param.pre_process.alter.alterWith);
                    }
                }

                if (param.pre_process.setReadOnly) {
                    var action = param.pre_process.setReadOnly;
                    if (action.feature) {
                        if (!feature[action.feature]) {
                            return false;
                        }
                    }
                    if (action.value) {
                        action = action.value;
                    }
                    if (action === "notAuthUser") {
                        var currentUser = sideMenuManager.currentUser();
                        if (!currentUser) {
                            param.readOnly = true;
                            param.param_tip = 'Only authenticated user can edit this field';
                            return false;
                        }
                        if (/@avanan\.com$/i.test(currentUser.email)) {
                            return false;
                        }
                        if (!options) {
                            options = modulePropsToOptions(modulesManager.app(app));
                        }
                        if (!options.authEmail) {
                            return false;
                        }
                        if (options.authEmail !== sideMenuManager.currentUser().email) {
                            param.param_tip = 'Only the authorizing user (' + options.authEmail + ') can edit this field';
                            param.readOnly = true;
                            return false;
                        }
                    }
                }

                if (param.pre_process.setVariants === 'webuserAdmins') {
                    return avananUserDao.users().then(function (response) {
                        param.variants = _(response.data.rows).chain().filter(function (user) {
                            return user.admin;
                        }).map(function (user) {
                            return {
                                id: user.email,
                                label: user.email
                            };
                        }).value();
                    });
                }
            }).compact().value();
            return $q.all(preProcessTasks).then(function () {
                return scriptConverter({
                    data: config,
                    app: app
                });
            });
        },
        configure: function (options, noAuth, focusOn) {
            var originalConfig;
            return loadConfig(options).then(function (response) {
                originalConfig = response.data || {};
                return appConfigurator.convertConfigParams(originalConfig, options.moduleName, options);
            }).then(function(response) {
                if (multipleAccounts[options.moduleName]) {
                    var widget = angular.copy(multiAccountsWidget);
                    widget.logic.app = options.moduleName;
                    _(response.data.pages).first().params.unshift({
                        type: 'widget',
                        widget: widget
                    });
                }
                if (!noAuth && appConfigurator.isAuth(options)) {
                    _(response.data.pages).first().params.unshift({
                        type: 'button',
                        title: 'oauth',
                        'class': 'btn-info',
                        execute: function () {
                            oauth(options);
                        }
                    });
                }
                if(!noAuth && appConfigurator.showConfigurationLink(options)) {
                    _(response.data.pages).first().params.unshift({
                        type: 'message',
                        message: '#' + JSON.stringify({
                            display: 'external-link',
                            href: options.configurationLink,
                            text: (options.saasAuthorized && options.configurationLinkAuthorizedLabel) || options.configurationLinkLabel || 'Authorize',
                            authorization: options.moduleName,
                            authLinkApi: options.authLinkApi,
                            authLinkParams: options.authLinkParams,
                            authLinkParamsTitle: options.authLinkParamsTitle,
                            'class': 'btn btn-blue'
                        }),
                        paramClass: 'full-width-param'
                    });
                    if (options.configurationLinkSeperatorText) {
                        _(response.data.pages).first().params.unshift({
                            id: 'auth_link_seperator',
                            'label': options.configurationLinkSeperatorText,
                            labelClass: 'bold-text text-align-left',
                            type: 'none'
                        });
                    }
                }
                response.data.title = 'Configure ' + options.title + (options.isSaas ? ' Security' : '');
                var modalArgs = $.extend(licenseOptions.modalArgs(options), {
                    windowClass: 'configure-dialog',
                    dismissOn: 'close-app-configurator',
                    paramConfirm: function (param) {
                        appConfigurator.confirmEmail(options, noAuth, param.id, param.confirmPending, param.onConfirmFocus);
                    },
                    focusOn: focusOn,
                    noDataLabel: 'Nothing to configure'
                });
                return modals.params(response.data.pages, response.data.title, response.data.size, modalArgs);
            }).then(function(params) {
                params = params || {};

                var forceConfirm = false;
                if (params._forceConfirm) {
                    forceConfirm = true;
                    delete params._forceConfirm;
                }
                var dialogParams = params;
                params = _(params).pick(function (value, key) {
                    if (originalConfig && originalConfig[key] && originalConfig[key].noSave) {
                        return false;
                    }
                    return true;
                });
                var needIndicator = _(originalConfig).find(function (info, paramKey) {
                    if (info.test_with_email && info.value !== params[paramKey]) {
                        return true;
                    }
                });
                if (needIndicator) {
                    workingIndicatorHelper.show(false);
                }
                var changeQueries = _(originalConfig).chain().map(function (conf, paramKey) {
                    if (!conf.onChange || conf.value === dialogParams[paramKey]) {
                        return false;
                    }
                    var onChangeInfo  = _(conf.onChange).find(function (info) {
                        return info.value === dialogParams[paramKey];
                    });
                    if (!onChangeInfo) {
                        return false;
                    }
                    return commonDao.callCustomUrl(onChangeInfo.method, onChangeInfo.url);
                }).compact().value();
                return $q.all(changeQueries).finally(function () {
                    modulesDao.saveConfig(options.moduleName, params).then(function(response) {
                        workingIndicatorHelper.hide();
                        var newConfig = response.data;
                        _(newConfig).each(function (info, paramKey) {
                            if (info.additional_values && info.additional_values.confirm_required &&
                                (!originalConfig[paramKey].additional_values || forceConfirm ||
                                originalConfig[paramKey].additional_values.confirm_required !== info.additional_values.confirm_required)
                            ) {
                                appConfigurator.confirmEmail(options, noAuth, paramKey, info.additional_values.confirm_required, info.onConfirmFocus);
                            }
                        });
                        return modulesManager.reloadModules();
                    }, workingIndicatorHelper.hide);
                });
            });
        },
        confirmEmail: function (options, noAuth, param, email, onConfirmFocus, error) {
            var pages = [{
                lastPage: true,
                okButton: 'Verify',
                params: [{
                    type: 'message',
                    message: '#' + JSON.stringify({
                        display: 'linked-text',
                        text: 'An e-mail with verification code was sent to ' + boldify(email) + '. Please enter it below'
                    }),
                    paramClass: 'full-width-param'
                }, {
                    id: 'code',
                    type: 'string',
                    label: 'Verification Code',
                    validation: {
                        required: true
                    }
                }],
                buttons: [{
                    'label': 'Use a different email',
                    execute: function () {
                        appConfigurator.configure(options, noAuth, param);
                    }
                }]
            }];
            if (error) {
                pages[0].params.push({
                    type: 'message',
                    emptyLabel: true,
                    message: '#' + JSON.stringify({
                        display: 'text',
                        text: error,
                        "class": 'error-msg'
                    })
                });
            }
            return modals.params(pages, 'Confirm e-mail address').then(function (data) {
                return modulesDao.confirmEmail(options.moduleName, param, data.code).then(function () {
                    if (onConfirmFocus) {
                        appConfigurator.configure(options, noAuth, onConfirmFocus);
                    }
                }, function (error) {
                    if (error.status === 400 && error.data.message) {
                        return appConfigurator.confirmEmail(options, noAuth, param, email, onConfirmFocus, error.data.message);
                    }
                });
            });
        }

    };

    return appConfigurator;

});