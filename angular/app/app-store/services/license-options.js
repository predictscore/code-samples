var m = angular.module('app-store');

m.factory('licenseOptions', function($q, modals, modulesDao, widgetPolling, modulesManager, path, configDao, utilityDao, sideMenuManager) {

    var licenseTypes = {
        paid: {
            display: 'License: Paid',
            displayFull: 'License Type: Paid',
            hideRequestDate: true,
            operations: {}
        },
        demo: {
            display: 'License: Demo',
            displayFull: 'License Type: Demo',
            operations: {
                'request_license': true,
                'insert_license': true
            }
        },
        'none': {
            display: 'No License',
            displayFull: 'No License',
            operations: {
                'start_demo': true,
                'request_license': true,
                'insert_license': true
            }
        }
    };

    var licenseOperations = [{
        label: function (options) {
            // return 'Start a ' + options.start_demo_period + ' days Evaluation';
            return 'Start an Evaluation';
        },
        value: 'start_demo',
        shown: _.constant(true)
    }, {
        label: _.constant('Request License'),
        value: 'request_license',
        shown: function (options) {
            return !options.license_info || !options.license_info.request_paid_license_date;
        }
    }, {
        label: _.constant('Insert License'),
        value: 'insert_license',
        shown: _.constant(true)
    }];


    var licenseModels = {
        'paid_demo': {
            'execute': function(options) {
                var data = paidDemoParams(options);
                var modalArgs = licenseOptions.modalArgs(options);
                modalArgs.onCloseAction = function(params) {
                    if (!params.operation) {
                        return;
                    }
                    params.start_demo_period = options.start_demo_period;
                    params.start_matrix_policy = true;
                    return modulesDao.saveLicense(options.moduleName, params);
                };
                modalArgs.onCloseActionError = function (response) {
                    var message = response && response.data && response.data.message || 'Unknown error occurred';
                    modals.alert(message, {
                        title: 'Error'
                    });
                };
                modals.params(data.pages, data.title, data.size, modalArgs)
                    .then(function() {
                        widgetPolling.reload();
                        modulesManager.init();
                    });
            }
        },
        'contact_support': {
            'execute': function(options) {
                var data = {
                    title: 'Request ' + (options.title ? options.title + ' ' : '') + 'License',

                     pages: [{
                        lastPage: true,
                        params: [{
                            type: 'message',
                            message: '#' + JSON.stringify({
                                display: 'text',
                                text: 'Send a message to support:'
                            }),
                            paramClass: 'full-width-param'
                        }, {
                            id: 'message',
                            type: 'text',
                            data: 'Hello\nWe want to activate ' + (options.title || options.moduleName),
                            paramClass: 'text-param-average full-width-param'
                        }]
                    }],
                    args: $.extend(licenseOptions.modalArgs(options), {
                        okButton: 'Send'
                    })
                };

                modals.params(data.pages, data.title, data.size, data.args)
                    .then(function(params) {
                        var msg = '';
                        var subject = 'Contact support request is sent for ' + options.moduleName;
                        msg += '<p>This is the message sent via "Contact Support" feature</p>';
                        msg += '<table boder="0">';
                        msg += '<tr><td>Application name</td><td>' + options.moduleName + '</td></tr>';
                        msg += '<tr><td>Application title</td><td>' + options.title + '</td></tr>';
                        msg += '<tr><td>Domain</td><td>' + window.location.host + '</td></tr>';
                        msg += '<tr><td>User</td><td>' + sideMenuManager.currentUser().email + '</td></tr>';
                        msg += '<tr><td>Full page url</td><td>' + window.location.href + '</td></tr>';
                        msg += '</table>';
                        msg += '<p>User message:</p>';
                        msg += '<p>' + _(params.message).escape().replace(/[\r\n]/g, '<br />') + '</p>';
                        utilityDao.sendSupportMessage({
                            subject: subject,
                            body: msg
                        }).then(function () {
                            modals.alert('Your request is sent to the support team', {title: ' '});
                        });
                    });
            }
        },
        'opswat_demo_paid': {
            'execute': function (options) {
                $q.all([
                    configDao.param('wizard_opswat_package'),
                    modulesDao.opswatLicense()
                ]).then(function (responses) {

                    var trialPackage = responses[0] && responses[1].data && responses[1].data.packages && (responses[1].data.packages[responses[0]] || {}).content;
                    var moreOptions = {
                        operation: {
                            nextPages: {
                                'start_demo' : 0,
                                'request_license': 1,
                                'insert_license': 0
                            }
                        }
                    };
                    if (!trialPackage || !trialPackage[options.moduleName]) {
                        moreOptions.disableOperations = {
                            start_demo: true
                        };
                    }
                    var data = paidDemoParams(options, moreOptions);
                    data.pages[0].lastPage = false;
                    data.pages[0].nextPage = 0;

                    data.pages.push({
                        lastPage: true,
                        params: opswatParams(responses[1].data),
                        okButton: 'Send request'
                    });

                    return modals.params(data.pages, data.title, data.size, licenseOptions.modalArgs(options));
                }).then(function(params) {
                    params.start_demo_period = options.start_demo_period;
                    params.start_matrix_policy = true;
                    modulesDao.saveLicense(options.moduleName, params).then(function() {
                        widgetPolling.reload();
                        modulesManager.init();
                    });
                });
            }
        }
    };


    function opswatParams(data) {
        var params = [];

        params.push({
            id: 'mode',
            type: 'radio-group',
            paramClass: 'full-width-param radio-group-inline',
            buttons: [{
                label: 'Bundled Packages',
                value: 'package'
            }, {
                label: 'Single AV',
                value: 'single'
            }],
            validation: {
                required: {
                    message: 'Please select package or anti virus'
                }
            }
        });


        var buttons;

        if (data.packages) {

            buttons = _(data.packages).chain().orderObject().map(function (pack) {
                var avList = _(pack.content).chain().orderObject().map(function (av) {
                    return av._key;
                }).value().join(', ');
                return {
                    label: pack.label + ' - ' + avList,
                    value: pack._key
                };
            }).value();

            if (buttons.length > 0) {
                params.push({
                    id: 'package_name',
                    type: 'radio-group',
                    paramClass: 'full-width-param radio-group-package',
                    buttons: buttons,
                    enabled: {
                        param: 'mode',
                        equal: 'package'
                    },
                    validation: {
                        required: {
                            message: 'Please select a package'
                        }
                    }
                });
            }
        }

        if (data.single) {

            buttons = _(data.single).chain().orderObject().map(function (av) {
                return {
                    label: av._key,
                    value: av._key
                };
            }).value();

            if (buttons.length > 0) {
                params.push({
                    id: 'av_name',
                    type: 'radio-group',
                    paramClass: 'full-width-param radio-group-av',
                    buttons: buttons,
                    enabled: {
                        param: 'mode',
                        equal: 'single'
                    },
                    validation: {
                        required: {
                            message: 'Please select an anti virus'
                        }
                    }
                });
            }
        }

        return params;
    }

    function paidDemoParams (options, moreOptions) {
        var params = [];
        var licenseInfo = licenseOptions.licenseInfo(options, 'displayFull');
        _(licenseInfo).each(function (item) {
            params.push({
                type: 'message',
                message: '#' + JSON.stringify({
                    display: 'text',
                    text: item
                }),
                paramClass: 'full-width-param license-info-text'
            });
        });

        var licenseType = options.license_info && licenseTypes[options.license_info.license_type] || licenseTypes.none;
        var operations = [];
        _(licenseOperations).each(function (op) {
            operations.push({
                label: op.label(options),
                value: op.value,
                disabled: !(licenseType.operations[op.value] && op.shown(options)) || (moreOptions && moreOptions.disableOperations && moreOptions.disableOperations[op.value])
            });
        });

        var licenseOperation = {
            id: 'operation',
            type: 'radio-group',
            paramClass: 'full-width-param',
            buttons: operations

        };
        if (operations.length == 1) {
            licenseOperation.data = operations[0].value;
        }

        if (_(operations).find(function (op) {
                return !op.disabled;
            })) {
            licenseOperation.validation = {
                required: {
                    message: 'Please select action'
                }
            };
        }

        if (moreOptions && moreOptions.operation && moreOptions.operation.nextPages) {
            licenseOperation.nextPages = moreOptions.operation.nextPages;
        }

        params.push(licenseOperation);

        params.push({
            id: 'license_key',
            type: 'string',
            label: 'License Key',
            enabled: {
                param: 'operation',
                equal: 'insert_license'
            },
            labelClass: 'text-align-left',
            validation: {
                required: true,
                minLength: {
                    message: 'Valid license key is required',
                    value: 1
                }
            }
        });

        var data = {
            title: (options.title ? options.title + ' ' : '') + 'License',
            pages: [{
                    lastPage: true,
                    params: params
                }]
        };
        return data;
    }

    var licenseOptions = {

        modalArgs: function (options) {
            var leftText = '#' + JSON.stringify({
                    display: 'linked-text',
                    'class': 'app-logo-container',
                    text: '#' + JSON.stringify({
                        display: 'img',
                        'class': 'app-logo',
                        path: (window.guiVersion === 2 && options.img.path2) ? path('gui-v2').img(options.img.path2): path('app-store').img(options.img.path)
                    })
                }) +'#' + JSON.stringify({
                    display: 'text',
                    text: options.title,
                    'class': 'app-title'
                }) + '#' + JSON.stringify({
                    display: 'text',
                    text: options.description,
                    'class': 'app-description'
                });

            return {
                leftText: leftText,
                bodyClass: 'license-modal-body'
            };
        },
        button: function (options) {
            if (!options.options || !licenseModels[options.options.license]) {
                return {
                    label: '',
                    enabled: false,
                    execute: _.noop,
                    class: ''
                };
            } else {
                return {
                    label: options.licenseButton.label,
                    enabled: true,
                    execute: function () {
                        licenseModels[options.options.license].execute(options);
                    },
                    class: options.licenseButton.class
                };
            }
        },
        licenseInfo: function licenseInfo(options, displayType) {
            var info = [];
            if (!options.options || (options.options.license != 'paid_demo' && options.options.license != 'opswat_demo_paid')) {
                return info;
            }
            if (displayType != 'displayFull') {
                displayType = 'display';
            }

            if (!options.license_info) {
                info.push(licenseTypes.none[displayType]);
                return info;
            }
            var licenseType = licenseTypes[options.license_info.license_type] || licenseTypes.none;

            info.push(licenseType[displayType]);

            if (displayType === 'displayFull' && options.license_info.expire_at) {
                info.push('Expiration Date: ' + moment(options.license_info.expire_at).format('MMMM D, YYYY'));
            }

            if (displayType === 'display' && options.license_info.expire_at) {
                info.push('Valid Until: ' + moment(options.license_info.expire_at).format('MMMM D, YYYY'));
            }

            if (displayType === 'displayFull' && options.license_info.request_paid_license_date && !licenseType.hideRequestDate) {
                info.push("Paid license requested at: " + moment(options.license_info.request_paid_license_date).format('MMMM D, YYYY'));
            }
            return info;
        },
        contactSupportDialog: function (options) {
            licenseModels.contact_support.execute(options);
        }
    };
    return licenseOptions;

});