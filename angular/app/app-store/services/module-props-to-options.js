var m = angular.module('app-store');

m.factory('modulePropsToOptions', function() {
    return function (module, extendWith, processConfig) {
        if (!module) {
            return {};
        }
        var result = {
            "img": {
                "path": module.app_store_icon
            },
            "title": module.label,
            "description": module.description,
            "new": module.new,
            "moduleName": module.name,
            "moduleStatus": module.state,
            "configurationLink": module.configuration_link,
            "configurationLinkLabel": module.configuration_link_label,
            "configurationLinkAuthorizedLabel": module.configuration_link_authorized_label,
            "configurationLinkSeperatorText": module.configuration_link_seperator_text,
            "auth": module.authentication,
            "options": module.options,
            "license_info": module.license_info,
            "start_demo_period": module.start_demo_period,
            "authLinkApi": module.auth_link_api,
            "authLinkParams": module.auth_link_params,
            "authLinkParamsTitle": module.auth_link_params_title,
            "isBeta": module.is_beta,
            "isExpired": module.isExpired,

            //params used in cloud-store converter
            "script": module.configuration,
            "authEmail": module.auth_email,
            "isSaas": module.is_saas,
            "saasAuthorized": module.saas_authorized,

            //params used in app-store converter
            "authMessage": module.authentication_message,
            "uploadLogs": module.upload_logs
        };
        if (extendWith) {
            result = $.extend(result, extendWith);
        }
        if (processConfig) {
            if (_(module.configuration).isObject()) {
                result.script = module.configuration;
            } else {
                result.configuration = module.configuration;
            }
        }
        if (result.options && result.options.license === 'contact_support') {
            result.licenseButton = {
                label: 'Contact Support'
            };
            result.options.operation = 'hide';
        }
        if (module.app_store2_logo) {
            result.img.path2 = 'apps-logos/' + module.app_store2_logo;
        }

        return result;
    };
});