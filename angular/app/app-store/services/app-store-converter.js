var m = angular.module('app-store');

m.factory('appStoreConverter', function($q, troubleshooting, modulePropsToOptions) {

    function moduleWidget(module, group) {
        var result = {
            "type": 'store-item',
            "size": 'col-lg-3 col-md-4 col-sm-6 col-xs-6',
            "wrapper": {
                "type":"none"
            },
            "options": modulePropsToOptions(module, {
                "licenseButton": {
                    'class': 'btn-store-fixed',
                    label: 'License'
                },
                "select": group.multi_select,
                "categoryId": group.categoryId,
                "sortOrder": module.app_store_order
            }, true)
        };

        if(_(module.price_text).isString()) {
            result.options.price = module.price_text;
        } else {
            result.options.price = 'Free';
            result.class = 'free';
        }
        return result;
    }



    return {
        'all': function(input, args) {
            var deferred = $q.defer();

            var appStore = input.data;
            appStore.groups = _(appStore.groups).filter(function(group) {
                if(!_(args.filter).isUndefined()) {
                    return group.family == 'security' && group.type == args.filter;
                }
                return group.family == 'security';
            });

            var data = {
                widgets: _(input.data.groups).map(function(group) {
                    var result = {
                        "type": "multi-widget",
                        "size": "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                        "wrapper": {
                            "type": "widget-panel",
                            "class": {
                                "store-group": true
                            },
                            "title": group.categoryLongName
                        },
                        "widgets": _(group.modules).chain().filter(function(module) {
                            return !module.hidden && (!module.gui_hidden || troubleshooting.entityDebugEnabled());
                        }).map(function(module) {
                            return moduleWidget(module, group);
                        }).value()
                    };
                    if(group.multi_select) {
                        result.logic = {
                            name: 'appGroupMultiSelectLogic',
                            categoryId: group.categoryId
                        };
                    }
                    return result;
                })
            };


            deferred.resolve($.extend({}, input, {
                data: data
            }));
            return deferred.promise;
        },
        'checkpoint' : function(input, args) {
            var deferred = $q.defer();

            var appStore = input.data;
            appStore.groups = _(appStore.groups).filter(function(group) {
                if(!_(args.filter).isUndefined()) {
                    return group.family == 'security' && group.type == args.filter;
                }
                return group.family == 'security';
            });

            var stateWeight = {
                active: 0,
                enabled: 0,
                nonactive: 1,
                disabled: 1,
                display_only: 2
            };


            var data = {
                widgets: [{
                    "type": "multi-widget",
                    "size": "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                    "wrapper": {
                        "type": "widget-panel",
                        "class": {
                            "store-group": true,
                            "checkpoint-store-group": true
                        },
                        "title": 'Checkpoint'
                    },
                    "widgets": _(appStore.groups).chain().map(function(group) {
                        return _(group.modules).chain().filter(function (module) {
                            return !module.hidden;
                        }).map(function (module) {
                            return moduleWidget(module, group);
                        }).value();
                    }).flatten().sortBy(function (module) {
                        return parseInt(module.options.sortOrder);
                    }).sortBy(function (module) {
                        return stateWeight[module.options.moduleStatus];
                    }).value()
                }]
            };


            deferred.resolve($.extend({}, input, {
                data: data
            }));
            return deferred.promise;
        }
    };
});