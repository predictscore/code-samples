var m = angular.module('app-store');

m.factory('suspiciousInlineUsersLogic', function (inlinePolicyDao, commonDao, $q, configDao) {
    return function ($scope, logic, reinitLogic) {
        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        $scope.model.wrapper.isLoading = true;

        $scope.hideInlineModeSwitch = true;
        $scope.maxUsers = null;

        var savedConfig, lookupTest, testedLookup = [];
        if ($scope.model.userSelectorSettings && $scope.model.userSelectorSettings.lookup) {
            lookupTest = commonDao.genericLookup($scope.model.userSelectorSettings.lookup.endpoint)
                .pagination(0, 1);
        } else {
            lookupTest = $q.when({
                data: {
                    rows: []
                }
            });
        }




        $scope.model.validate = function (getParam, getEnabledParam) {
            if (!$scope.model.initialized) {
                return true;
            }
            if (!$scope.model.inlineActive) {
                return true;
            }
            return true;
        };
        $scope.model.onsave = function (isEnabled) {
            if (!$scope.model.initialized) {
                return true;
            }

            var data;
            if (!$scope.model.inlineActive) {
                data = {
                    users: []
                };
            } else {
                switch ($scope.model.fetchType) {
                    case 'only':
                        data = {
                            users: $scope.model.users
                        };
                        break;
                    case 'all':
                        data = {
                            all: true,
                            users: []
                        };
                        break;
                    case 'allExcept':
                        data = {
                            except: true,
                            users: $scope.model.users
                        };
                        break;
                    default:
                        data = {
                            users: []
                        };
                }
            }
            return inlinePolicyDao.saveSuspiciousUsersConfig(data, logic.app);
        };

        if (!$scope.model.refParams || !$scope.model.options || !$scope.model.options.inlineConfigParam ||
                !$scope.model.refParams[$scope.model.options.inlineConfigParam] ||
                !$scope.model.refParams[$scope.model.options.inlineConfigParam].widget
        ) {
            return _.noop;
        }
        $scope.inlineConfigWidget = $scope.model.refParams[$scope.model.options.inlineConfigParam].widget;

        function rebuildModel() {
            if (!$scope.inlineConfigWidget.initialized || !savedConfig) {
                return;
            }
            $scope.model.inlineActive = $scope.inlineConfigWidget.inlineActive;

            $scope.typeVariants = [{
                id: 'all',
                label: 'All'
            }];
            if (($scope.inlineConfigWidget.fetchType === 'all' && testedLookup.length) ||
                ($scope.inlineConfigWidget.fetchType === 'only' && $scope.inlineConfigWidget.users.length)) {
                $scope.typeVariants.push({
                    id: 'only',
                    label: 'Selected users only'
                });
            }
            if ($scope.inlineConfigWidget.fetchType === 'all') {
                $scope.excludeEmails = function () {
                    return _($scope.model.exclude_email_params).chain().map(function (paramId) {
                        return $scope.model.refParams && $scope.model.refParams[paramId] && $scope.model.refParams[paramId].data;
                    }).compact().value();
                };
                $scope.showOnlyUserIds = false;
            }
            if ($scope.inlineConfigWidget.fetchType === 'only') {
                $scope.excludeEmails = false;
                $scope.showOnlyUserIds = $scope.inlineConfigWidget.users;
                $scope.model.users = _($scope.inlineConfigWidget.users).intersection($scope.model.users);
            }

            if ($scope.inlineConfigWidget.fetchType === 'only' && !$scope.inlineConfigWidget.users.length) {
                $scope.model.selectorDisabled = true;
            } else {
                $scope.model.selectorDisabled = false;
            }


            if (!$scope.model.initialized) {
                $scope.model.fetchType = (savedConfig.all ? 'all' : 'only');
            }

            if (!_($scope.typeVariants).find(function (variant) {
                return variant.id === $scope.model.fetchType;
                })) {
                    $scope.model.fetchType = $scope.typeVariants[0].id;
            }
            $scope.model.initialized = true;
        }

        var inlineConfigStopWatch = $scope.$watchCollection('inlineConfigWidget', function () {
            rebuildModel();
        });


        $q.all([inlinePolicyDao.retrieveSuspiciousUsersConfig(logic.app),
            lookupTest,
            configDao.param('max_inline_users'),
            configDao.param('deployment_mode')
        ]).then(function (responses) {
            savedConfig = responses[0].data;
            testedLookup = responses[1].data.rows;

            $scope.model.wrapper.isLoading = false;
            $scope.model.users = _(savedConfig.rows).pluck('user_id');
            rebuildModel();

        });

        return function () {
            inlineConfigStopWatch();
        };
    };
});
