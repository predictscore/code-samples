var m = angular.module('app-store');

m.factory('multiAccountsLogic', function (modulesDao) {
    return function ($scope, logic, reinitLogic) {

        function buildInfo() {
            if ($scope.model.data && $scope.model.data.length) {
                $scope.model.lines = [{
                    class: 'title',
                    text: 'Accounts'
                }];
                _($scope.model.data).each(function (account) {
                    var accountLines = [{
                        class: 'account-name',
                        text: account.label
                    }];
                    var actions = '';

                    if (account.is_enabled) {
                        actions += '#' + JSON.stringify({
                                display: 'text',
                                'text': 'disable',
                                class: 'action',
                                clickEvent: {
                                    name: 'multi-account-toggle',
                                    id: account.id,
                                    app: logic.app
                                }
                            });
                    } else {
                        actions += '#' + JSON.stringify({
                                display: 'text',
                                text: 'enable',
                                'class': 'action',
                                clickEvent: {
                                    name: 'multi-account-toggle',
                                    id: account.id,
                                    app: logic.app
                                }
                            });
                    }

                    actions += '#' + JSON.stringify({
                            display: 'text',
                            text: 'remove',
                            'class': 'action',
                            clickEvent: {
                                name: 'multi-account-remove',
                                id: account.id,
                                app: logic.app
                            }
                        });

                    account.actionLine = {
                        'class': 'account-actions',
                        text: actions
                    };
                    account.workingLine = {
                        'class': 'account-working',
                        text: '',
                        icon: 'fa-spinner fa-pulse',
                        style: {
                            display: 'none'
                        }
                    };

                    accountLines.push(account.actionLine);
                    accountLines.push(account.workingLine);

                    $scope.model.lines.push({
                        widget: {
                            type: 'info',
                            class: 'account-line',
                            lines: accountLines
                        }
                    });

                });
            } else {
                $scope.model.lines = [];
            }
        }

        modulesDao.multiAccounts(logic.app).then(function (response) {
            $scope.model.data = response.data.accounts;
            buildInfo();
        });

        function accountSetWorking(account) {
            account.actionLine.style = {
                display: 'none'
            };
            account.workingLine.style = {
                display: 'inline-block'
            };
        }
        function accountResetWorking(account) {
            account.actionLine.style = {
                display: 'inline-block'
            };
            account.workingLine.style = {
                display: 'none'
            };
        }


        var removeListener = $scope.$on('multi-account-remove', function (event, data) {
            if (data.app !== logic.app) {
                return;
            }
            var account = _($scope.model.data).find(function (a) {
                return a.id === data.id;
            });
            if (!account) {
                return;
            }
            accountSetWorking(account);
            modulesDao.multiAccountRemove(logic.app, account.id)
                .then(function () {
                    reinitLogic();
                }, function (error) {
                    accountResetWorking(account);
                });
        });

        var toggleListener = $scope.$on('multi-account-toggle', function (event, data) {
            if (data.app !== logic.app) {
                return;
            }
            var account = _($scope.model.data).find(function (a) {
                return a.id === data.id;
            });
            if (!account) {
                return;
            }
            accountSetWorking(account);
            modulesDao.multiAccountAction(logic.app, account.id, account.is_enabled ? 'disable' : 'enable')
                .then(function () {
                    reinitLogic();
                }, function (error) {
                    accountResetWorking(account);
                });
        });

        if (!$scope.model.lines) {
            buildInfo();
        }

        return function () {
            removeListener();
            toggleListener();
        };
    };
});
