var m = angular.module('app-store');

m.factory('scriptConverter', function($q, feature, troubleshooting) {
    var configWidgets = {
        'inline-config': function (body, id, globalData) {
            return {
                id: id,
                type: 'widget',
                order: body.order,
                widget: {
                    "type": "inline-config",
                    "logic": {
                        "name": "inlineConfigLogic",
                        "updateTick": null,
                        app: globalData.app
                    },
                    "wrapper": {
                        "type": "none"
                    },
                    userSelectorSettings: {
                        lookup: body.userLookup
                    },
                    label: body.label,
                    exclude_email_params: body.exclude_email_params,
                    inline_app: body.inline_app,
                    options: body.options
                },
                broadcastOnSave: 'save-inline-config',
                refParams: body.exclude_email_params,
                enabled: body.enabled,
                visible: body.visible,
                section: body.section
            };
        },
        'suspicious-users-config': function (body, id, globalData) {
            return {
                id: id,
                type: 'widget',
                order: body.order,
                widget: {
                    "type": "inline-config",
                    "logic": {
                        "name": "suspiciousInlineUsersLogic",
                        "updateTick": null,
                        app: globalData.app
                    },
                    "wrapper": {
                        "type": "none"
                    },
                    userSelectorSettings: {
                        lookup: body.userLookup
                    },
                    label: body.label,
                    exclude_email_params: body.exclude_email_params,
                    options: body.options
                },
                refParams: _([body.options.inlineConfigParam, body.exclude_email_params]).chain().compact().flatten().value(),
                enabled: body.enabled,
                visible: body.visible,
                section: body.section
            };
        }
    };

    return function(input, args) {
        var deferred = $q.defer();

        function convertType(type) {
            if(type == 'INT') {
                return 'integer';
            }
            if(type == 'PASSWORD') {
                return 'password';
            }
            if(type == 'BOOLEAN') {
                return 'boolean';
            }
            if(type == 'SEPERATOR') {
                return 'none';
            }
            if (type == 'SELECTOR') {
                return 'selector';
            }
            if (type == 'RADIOGROUP') {
                return 'radio-group';
            }
            if (type == 'RADIOGROUPNOLABEL') {
                return 'radio-group';
            }
            if (type == 'SECTIONLABEL') {
                return 'section-label';
            }
            if (type === 'ARRAY') {
                return 'list';
            }
            if (type === 'LINK') {
                return 'message';
            }
            if (type === 'OBJECT_OF_ARRAYS') {
                return 'object-of-arrays';
            }
            if (type === 'DATETIME_INTERVAL') {
                return 'datetime-interval';
            }
            if (type === 'ARRAY_AS_CSV') {
                return 'list-as-csv';
            }
            if (type === 'TEXTAREA' || type === 'WIDETEXTAREA') {
                return 'text';
            }

            if (type === 'RICH_TEXT_EDITOR' || type === 'WIDE_RICH_TEXT_EDITOR') {
                return 'email-body';
            }

            return 'string';
        }

        function getClass(type, displayWide) {
            if(type == 'SEPERATOR') {
                return 'bold-text text-align-left';
            }
            if (displayWide || type === 'WIDETEXTAREA' || type === 'WIDETEXT' || type === 'WIDE_RICH_TEXT_EDITOR') {
                return 'text-align-left full-width-label';
            }
            return 'text-align-left';
        }

        var tags = {};

        var data = {
            notExist: _(input.data).isEqual({}),
            title: "Wizard",
            pages: [
                {
                    lastPage: true,
                    params: _(input.data).chain().map(function (body, id) {
                        if (body.feature && !feature[body.feature]) {
                            return null;
                        }
                        if (body.ui_debug_only && !troubleshooting.entityDebugEnabled()) {
                            return null;
                        }
                        if (body.enabled) {
                            var testStaticDisabled = _(body.enabled).isArray() ? body.enabled : [body.enabled];
                            _(testStaticDisabled).each(function (check) {
                                if (check.notEmptyStatic && (!input.data[check.param] || !input.data[check.param].value)) {
                                    check.alwaysHidden = true;
                                }
                            });
                        }
                        if (body.widget) {
                            if (configWidgets[body.widget]) {
                                return configWidgets[body.widget](body, id, input);
                            }
                            return null;
                        }

                        var type = convertType(body.type);
                        var label;
                        var paramClass;
                        if (body.type != 'RADIOGROUPNOLABEL' && body.type != 'LINK') {
                            label = body.label;
                        } else {
                            paramClass = 'full-width-param';
                        }
                        if (body.displayWide || body.type === 'WIDETEXTAREA' || body.type === 'WIDETEXT' || body.type === 'WIDE_RICH_TEXT_EDITOR') {
                            paramClass = 'full-width-param';
                        }

                        var param = {
                            id: id,
                            type: type,
                            data: _(body.value).isUndefined() ? body.default : body.value,
                            label: label,
                            hidden: _(label).isUndefined() && (type != 'radio-group') && (type !== 'message'),
                            order: body.order,
                            labelClass: getClass(body.type, body.displayWide),
                            variants: body.variants,
                            buttons: body.buttons,
                            enabled: body.enabled,
                            visible: body.visible,
                            section: body.section,
                            labelTip: body.label_tip,
                            paramTip: body.param_tip || body.description,
                            rightAddon: body.rightAddon,
                            paramClass: paramClass,
                            keysParamId: body.keys_param,
                            disabledValue: body.disabled_value,
                            onConfirmFocus: body.onConfirmFocus,
                            readOnly: body.readOnly
                        };
                        if (body.required) {
                            param.validation = {
                                required: true
                            };
                        }
                        if (body.type === 'LINK') {
                            if (body.url) {
                                param.message = '#' + JSON.stringify({
                                        display: 'external-link',
                                        href: body.url,
                                        text: body.label
                                    });
                            } else {
                                param.message = body.label;
                            }
                        }
                        if (body.lookup) {
                            param.settings = {
                                lookup: body.lookup
                            };
                            if (param.type === 'string') {
                                param.type = 'list';
                                param.settings.multiple = false;
                                if (!body.required) {
                                    param.settings.allowClear = true;
                                }
                            }
                            if (body.lookup.params) {
                                param.settings.lookup.params = _(body.lookup.params).chain().map(function (param) {
                                    if (param.type === 'static_param') {
                                        return [param.name, input.data[param.param] && input.data[param.param].value];
                                    }
                                }).compact().object().value();
                            }
                        }
                        if (body.additional_values && body.additional_values.confirm_required) {
                            param.confirmPending = body.additional_values.confirm_required;
                        }
                        if (body.test_with_email) {
                            param.confirmRequired = true;
                        }

                        if (body.tags) {
                            tags[id] = body.tags;
                            if (param.type === 'string') {
                                param.type = 'property-string';
                                param.propertyType = param.id;
                                param.propertiesHideFilter = true;
                                param.propertiesHideAdvancedFilter = true;
                            }
                            if (param.type === 'email-body') {
                                param.type = 'property-email-body';
                                param.propertyType = param.id;
                                param.propertiesHideFilter = true;
                                param.propertiesHideAdvancedFilter = true;
                                param.editorHeight = '300';
                            }
                        }

                        if (body.validate_with_api) {
                            param.onchange_validation = body.validate_with_api;
                            param.validation = param.validation || {};
                            param.validation.notInvalid = true;
                        }

                        return param;
                    }).compact().sortBy('order').value()
                }
            ]
        };
        if (!_(tags).isEmpty()) {
            data.pages[0].getProperties = function (type) {
                if (!tags || !tags[type]) {
                    return {};
                }
                var props = _(tags[type]).mapObject(function (label, id) {
                    return {
                        id: id,
                        label: label,
                        selectable: true
                    };
                });
                props['0'] = {
                    id: '',
                    sub: _(tags[type]).keys()
                };
                return props;
            };
        }

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    };
});