var m = angular.module('app-store');

m.directive('storeItem', function(path, http, modals, modulesDao, scriptConverter, widgetPolling, modulesManager, licenseOptions, $q, sideMenuManager, utilityDao, appOperationHelper, feature, authorizationLinkHelper, appConfigurator) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('app-store').directive('store-item').template(),
        scope: {
            model: '=storeItem'
        },
        link: function ($scope, element, attrs) {
            $scope.path = path('app-store');

            $scope.showComingSoonIcon = feature.showComingSoonIcon;
            
            $scope.storeItemClass = function() {
                var result = {};
                result['status-' + $scope.model.options.moduleStatus] = true;
                result['module-' + $scope.model.options.moduleName] = true;
                return result;
            };

            $scope.isConfigurable = function() {
                return !_(_($scope.model.options.script).find(function(property) {
                    return !_(property.label).isUndefined();
                })).isUndefined();
            };

            $scope.isAuth = function() {
                return _($scope.model.options.auth).isString();
            };

            function showConfigurationLink() {
                return appConfigurator.showConfigurationLink($scope.model.options);
            }

            $scope.configDialogShown = false;
            $scope.configure = function() {
                if ($scope.configDialogShown) {
                    return;
                }
                $scope.configDialogShown = true;
                return appConfigurator.configure($scope.model.options).then(function() {
                    widgetPolling.reload();
                }).finally(function () {
                    $scope.configDialogShown = false;
                });
            };
            $scope.$on('configure-app', function (event, args) {
                if (args.app === $scope.model.options.moduleName) {
                    $scope.configure();
                }
            });

            $scope.getDollars = function() {
                var splitted = $scope.model.options.price.split('.');
                if(splitted.length == 2) {
                    return splitted[0] + '.';
                }
                return $scope.model.options.price;
            };

            $scope.getCents = function() {
                var splitted = $scope.model.options.price.split('.');
                if(splitted.length == 2) {
                    return splitted[1];
                }
                return '';
            };

            $scope.isHavePrice = function() {
                return _($scope.model.options.price).isString();
            };

            $scope.isFree = function() {
                return $scope.model.options.price.toUpperCase() == 'FREE';
            };

            $scope.selectClick = function() {
                var newState = $scope.model.options.moduleStatus  == 'active' ? 'nonactive' : 'active';
                modulesDao.updateState($scope.model.options.categoryId, [$scope.model.options.moduleName], newState).then(function() {
                    $scope.model.options.moduleStatus = newState;
                });
            };

            $scope.getSelectLabel = function() {
                if($scope.model.options.moduleStatus  == 'active') {
                    return 'Deactivate';
                }
                return 'Activate';
            };


            function getOperation (options) {
                if (!options.options ||
                    !options.moduleName ||
                    options.moduleStatus == 'display_only' ||
                    !options.options.operation ||
                    options.options.operation == 'none'
                ) {
                    return 'hide';
                } else {
                    return options.options.operation;
                }
            }

            function executeOperation() {
                if ($scope.model.options.isSaas &&
                    !$scope.model.options.saasAuthorized &&
                    $scope.model.options.options.operation === 'start' &&
                    showConfigurationLink()
                    ) {
                    authorizationLinkHelper.openLink($scope.model.options);
                    return;
                }
                var moduleName = $scope.model.options.moduleName;
                var pages = [{
                    lastPage: true,
                    okButton: $scope.operationButton.label,
                    params: [{
                        type: 'message',
                        message: '#' + JSON.stringify({
                            display: 'text',
                            text: 'Are you sure you want to ' + $scope.operationButton.label +' ' + $scope.model.options.title + '?'
                        }),
                        paramClass: 'full-width-param'
                    }]
                }];

                if ($scope.model.options.options.operation === 'start') {
                    pages[0].params.push({
                        id: 'start_matrix_policy',
                        type: 'boolean',
                        rightLabel: 'Enable matrix queries',
                        data: true
                    });
                }
                
                function refresh() {
                    return $q.all([
                        modulesManager.reloadModules(),
                        widgetPolling.reload()
                    ]).then(function () {
                        appOperationHelper.operationEnd(moduleName);
                    }, function () {
                        appOperationHelper.operationEnd(moduleName);
                    });
                }

                modals.params(pages, $scope.operationButton.label + ' ' + $scope.model.options.title).then(function (params) {
                    appOperationHelper.operationStart(moduleName);
                    return modulesDao.operation($scope.model.options.moduleName, $scope.model.options.options.operation, params);
                }).then(refresh, refresh);
            }

            function showConfigButton (options) {
                return appConfigurator.configAvailable(options);
            }

            $scope.operationInProgress = function () {
                return appOperationHelper.operationInProgress($scope.model.options.moduleName);
            };

            var operations = {
                'start': {
                    labelClass: 'fa-play',
                    label: 'Start'
                },
                'stop': {
                    labelClass: 'fa-pause',
                    label: 'Stop'
                },
                'default': {
                    labelClass: 'fa-question'
                },
                'hide' : {
                    hidden: true
                }
            };

            $scope.notifyWhenAvailable = function () {
                var msg = '';
                var subject = 'Request to notify when \'coming soon\' app is available';
                msg += 'This is automatically generated message.\nUser pressed \'Notify me when available\' button\n\n';
                msg += 'Application name : ' + $scope.model.options.moduleName + '\n';
                msg += 'Application title: ' + $scope.model.options.title + '\n';
                msg += 'Domain           : ' + window.location.host + '\n';
                msg += 'User             : ' + sideMenuManager.currentUser().email + '\n';
                msg += 'Full page url    : ' + window.location.href + '\n';
                utilityDao.sendSupportMessage({
                    subject: subject,
                    body: msg
                }).then(function () {
                    modals.alert('This application is coming soon! We will notify you when it becomes available.', {title: ' '});
                });
            };

            $scope.uploadLogs = function () {
                var pages = [{
                    lastPage: true,
                    params: [{
                        id: 'logfile',
                        type: 'file',
                        label: 'Choose a file',
                        validation: {
                            required: {
                                message: 'Please select file'
                            }
                        }
                    }],
                    okButton: 'Upload'
                }];
                return modals.params(pages, 'Upload Logs for ' + $scope.model.options.title, undefined, licenseOptions.modalArgs($scope.model.options))
                    .then(function (data) {
                        $scope.$root.logsAnalyzing = $scope.$root.logsAnalyzing || {};
                        $scope.$root.logsAnalyzing[$scope.model.options.moduleName] = true;
                    });
            };
            $scope.analyzing = function () {
                return $scope.$root.logsAnalyzing && $scope.$root.logsAnalyzing[$scope.model.options.moduleName];
            };

            $scope.$watch('model', function() {
                var operation = getOperation($scope.model.options);
                if ($scope.model.options.moduleStatus === 'display_only') {
                    $scope.model.options.license = 'none';
                    $scope.model.options.options.license = 'none';
                }

                $scope.licenseInfo = licenseOptions.licenseInfo($scope.model.options);

                $scope.licenseButton = licenseOptions.button($scope.model.options);

                $scope.operationButton = {
                    label: (operations[operation] || operations.default).label || operation,
                    labelClass: (operations[operation] || operations.default).labelClass || '',
                    enabled: !(operations[operation] || operations.default).hidden,
                    execute: executeOperation
                };
                $scope.configureButton = {
                    enabled: showConfigButton($scope.model.options),
                    label: 'Configure',
                    execute: $scope.configure,
                    class: 'btn-store-fixed'
                };
            }, true);
        }
    };
});