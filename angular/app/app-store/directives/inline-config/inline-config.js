var m = angular.module('app-store');

m.directive('inlineConfig', function(path, widgetLogic, inlineUserSelectorModal, modals, $q, commonDao, activateInlineAppModal, $timeout) {

    function showTrialLimitDialog() {
        return modals.alert(['Dear user,',
            'This feature is disabled in trial version,',
            'please upgrade to allow inline protection for an entire account'], {
            title: 'Trial version',
            windowClass: 'inline-user-limit-dialog'
        }, 'sm');
    }

    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('app-store').directive('inline-config').template(),
        scope: {
            model: '=inlineConfig'
        },
        link: function ($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);

            $scope.showUserSelector = function () {
                return $scope.model.fetchType === 'only' || $scope.model.fetchType === 'allExcept';
            };

            $scope.fetchTypeChanged = function (newValue, oldValue) {
                if (!$scope.maxUsers) {
                    return;
                }
                if (newValue !== 'all') {
                    return;
                }
                $scope.model.fetchType = oldValue;
                return showTrialLimitDialog();
            };

            function stateCheckTrial (newValue) {
                if (!$scope.maxUsers) {
                    return false;
                }
                if (!newValue) {
                    return false;
                }
                if ($scope.model.fetchType !== 'all') {
                    return false;
                }
                var otherOption = _($scope.typeVariants).find(function (variant) {
                    return variant.id !== 'all';
                });
                if (otherOption) {
                    $scope.model.fetchType = otherOption.id;
                    return false;
                }
                return true;

            }

            function checkInlineAppStatus() {
                if (!$scope.model.inline_app) {
                    return $q.when();
                } else {
                    return commonDao.customStateCheck($scope.model.inline_app.check_api).then(function (result) {
                        $scope.inlineAppAuthRequired = !result.state;
                        if (!$scope.inlineAppAuthRequired && $scope.activateInlineModal) {
                            $scope.activateInlineModal.dismiss();
                        }
                    });
                }
            }

            var inlineAppRecheckTimer = false;

            function recheckInlineAppStatus() {
                inlineAppRecheckTimer = $timeout(function () {
                    checkInlineAppStatus().then(function() {
                        if ($scope.inlineAppAuthRequired && $scope.activateInlineModal) {
                            recheckInlineAppStatus();
                        }
                    });
                }, $scope.model.inline_app.recheck_interval || 5000);
            }

            function recheckInlineAppStatusStop() {
                if (inlineAppRecheckTimer) {
                    $timeout.cancel(inlineAppRecheckTimer);
                    inlineAppRecheckTimer = false;
                }
            }



            function showActivateDialog() {
                $scope.activateInlineModal = activateInlineAppModal.dialog({
                    title: $scope.model.inline_app.dialog_title,
                    message: $scope.model.inline_app.dialog_text,
                    okUrl: $scope.model.inline_app.auth_url
                });
                recheckInlineAppStatus();

                $scope.activateInlineModal.result.finally(function () {
                    recheckInlineAppStatusStop();
                    $scope.activateInlineModal = false;
                    if ($scope.inlineAppAuthRequired) {
                        $scope.model.inlineActive = false;
                    }
                });
            }

            $scope.inlineStateChanged = function () {
                var newValue = $scope.model.inlineActive;
                if (stateCheckTrial(newValue)) {
                    $scope.model.inlineActive = false;
                    showTrialLimitDialog();
                    return;
                }
                if ($scope.model.inline_app && newValue) {
                    checkInlineAppStatus().then(function () {
                        if ($scope.inlineAppAuthRequired) {
                            showActivateDialog();
                        }
                    });
                }
            };

            $scope.selectUsersLabel = function () {
                if (!$scope.model.users || !$scope.model.users.length) {
                    return 'Click here to choose users';
                }
                return $scope.model.users.length + ' user' + ($scope.model.users.length > 1 ? 's' : '') + ' selected';
            };

            $scope.openUserSelector = function (e) {
                e.stopPropagation();
                e.preventDefault();
                inlineUserSelectorModal.show($scope.model.users, {
                    lookupEndpoint: $scope.model.userSelectorSettings.lookup.endpoint,
                    excludeEmails: $scope.excludeEmails,
                    showOnlyUserIds: $scope.showOnlyUserIds,
                    maxUsers: $scope.maxUsers
                }).then(function (users) {
                    $scope.model.users = users;
                });
            };

            $scope.$on('$destroy', function () {
                recheckInlineAppStatusStop();
            });
        }
    };
});