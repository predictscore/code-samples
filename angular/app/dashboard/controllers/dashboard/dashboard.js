var m = angular.module('dashboard');

m.controller('DashboardController', function($scope, widgetPolling, sizeClasses, path, $routeParams, routeHelper, modulesManager, wizardHelper, $q, feature, logging, $route, sideMenuManager) {
    function updateTitle(module) {
        var title = 'Dashboard';
        var moduleApp = modulesManager.cloudApp(module);
        if (moduleApp) {
            title = (moduleApp.dashboardTitle || (moduleApp.title + ' Dashboard'));
        }
        $route.current.fullTitle =  '#' + JSON.stringify({
            display: 'html',
            text: '<strong>' + title + '</strong>'
        });
    }
    updateTitle($routeParams.module);
    $q.all([
        wizardHelper.reloadState(),
        modulesManager.reloadModules()
    ]).then(function(responses) {
        var wizardState = wizardHelper.getState();
        if (feature.onboardingWizard && wizardState.inProgress) {
            logging.log('Redirecting to wizard because it\'s still active. State: ' + JSON.stringify(wizardState));
            routeHelper.redirectTo('wizard');
            return;
        }
        var module = $routeParams.module;
        if(_(module).isUndefined() || module != modulesManager.currentModule()) {
            routeHelper.redirectTo('dashboard', {
                module: modulesManager.currentModule()
            });
            return;
        } else if (module == 'please-enable-saas') {
            logging.log('Redirecting to wizard because no saas seems to be active. State: ' + JSON.stringify(wizardState));
            routeHelper.redirectTo(feature.onboardingWizard ? 'wizard' : 'cloud-apps');
            return;
        }
        if (feature.onboardingWizard && !wizardState.inProgress && wizardState.status === 'enabled') {
            wizardHelper.setStatus('disabled');
        }
        var dashboardPostfix = '';
        if(feature.isShowAnomalyTable() && module == 'google_drive') {
            dashboardPostfix = '.feature-demo0911';
        }
        widgetPolling.updateWidgets(path('dashboard').ctrl('dashboard').json(module + '.dashboard' + dashboardPostfix).path);
        $scope.widgetPolling = widgetPolling;
        $scope.getSizeClasses = sizeClasses;
        $scope.wrapperClass = 'dashboard';
        
        sideMenuManager.treeNavigationPath(['dashboard', $routeParams.module]);
    });
});