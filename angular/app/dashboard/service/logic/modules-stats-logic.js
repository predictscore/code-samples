var m = angular.module('dashboard');

m.factory('modulesStatsLogic', function($q, policyDao, modulesDao, path, modulesManager, feature, $filter, routeHelper, troubleshooting, $rootScope, $timeout, policiesRefreshHelper) {

    var sectionsConfig = [{
        id: 'found',
        label: 'Found',
        'class': 'module-stats-found'
    }, {
        id: 'confident',
        label: 'Found',
        'class': 'module-stats-found'
    }, {
        id: 'suspicious',
        label: 'Suspected',
        'class': 'module-stats-suspected'
    }];

    function makeToolTip(policies, noPoliciesMessage) {
        if (policies.length < 1) {
            return noPoliciesMessage;
        }
        policiesRefreshHelper.addPolicies(_(policies).map(function(info) {
            return info.id;
        }));
        return {
            content: {
                text: '<div class="infobox-tooltip"><table>' +
                _(policies).map(function (info) {
                    var url = '/#!' + routeHelper.getPath('policy-edit', {id: info.id});
                    return '<tr>' +
                        '<td><a href="' + url +'">' + info.name +'</a></td>' +
                        '<td>' + (info.match_count ? $filter('number')(info.match_count): 'No threats found') + '</td></tr>';
                }).join('') +
                '</table></div>'
            },
            position: {
                at: 'bottom center',
                my: 'top center',
                viewport: true
            },
            hide: {
                delay: 200,
                fixed: true
            },
            style: {
                classes: 'qtip-bootstrap qtip-infobox'
            }
        };
    }

    function addFoundLines(lines, module) {
        if (module.info.learning_mode) {
            lines.push({
                text: '#' + JSON.stringify({
                    display: 'text',
                    text: 'Learning',
                    'class': 'display-block'
                }) + '#' + JSON.stringify({
                    display: 'text',
                    text: 'mode',
                    'class': 'display-block'
                }),
                'class': 'module-stats-found'
            });
            return;
        }
        _(sectionsConfig).each(function (config) {
            if (!module.sections[config.id]) {
                return;
            }
            lines.push(formatFoundLine(module.sections[config.id], config));
        });
    }

    function formatFoundLine(section, config) {

        if (!section.total) {
            return {
                text: config.label +': ' + '#' + JSON.stringify({
                    display: 'linked-text',
                    tooltip: makeToolTip(section.policies, 'No threats found'),
                    'class': 'display-block module-stats-check',
                    text: '#' + JSON.stringify({
                        display: 'linked-text',
                        'class': 'fa-stack fa-lg',
                        text: '#' + JSON.stringify({
                            display: 'text',
                            'class': 'fa fa-circle fa-stack-2x'
                        }) + '#' + JSON.stringify({
                            display: 'text',
                            'class': 'fa fa-check fa-stack-1x'
                        })
                    })
                }),
                'class': config['class']
            };
        }

        if (section.policies.length === 1) {
            return {
                text: config.label +': ' + '#' + JSON.stringify({
                    text: '' + section.total,
                    type: 'policy-edit',
                    id: section.policies[0].id,
                    tooltip: makeToolTip(section.policies),
                    'class': 'display-block module-stats-number'
                }),
                'class': config['class']
            };
        }

        return  {
            text: config.label +': ' + '#' + JSON.stringify({
                display: 'text',
                text: '' + section.total,
                tooltip: makeToolTip(section.policies),
                'class': 'display-block module-stats-number'
            }),
            'class': config['class']
        };
    }


    return function($scope, logic) {
        logic.updateTick = (feature.dashboardUpdateTicks && feature.dashboardUpdateTicks.modulesStats || 4) * 1000;
        function isSameWidgets(oldWidgets, newWidgets) {
            if(oldWidgets.length != newWidgets.length) {
                return false;
            }
            var found = _(oldWidgets).find(function(widget, idx) {
                return widget.moduleName != newWidgets[idx].moduleName;
            });
            return !found;
        }
        
        function mergeWidgets(oldWidgets, newWidgets) {
            _(oldWidgets).each(function(widget, idx) {
                _(newWidgets[idx].lines).each(function(line, lineIdx) {
                    _(widget.lines[lineIdx]).clearObject(line);
                });
            });
        }
        
        $q.all([
            policyDao.modulesMatrix(true, true),
            modulesDao.getModules()
        ]).then(function(responses) {
            var modulesForSaas = responses[0].data[modulesManager.currentModule()];

            var newWidgets = _(modulesForSaas).chain().pick(function (value, moduleName) {
                return modulesManager.isActive(moduleName);
            }).map(function(value, key) {
                var sections = {};
                _(value).each(function (policyInfo, id) {
                    var sectionId = policyInfo.section || 'found';
                    sections[sectionId] = sections[sectionId] || {
                            total: 0,
                            policies: []
                        };
                    sections[sectionId].total += policyInfo.match_count;
                    sections[sectionId].policies.push($.extend({
                        id: parseInt(id)
                    }, policyInfo));
                });
                _(sections).each(function (section) {
                    section.policies = _(section.policies).sortBy('id');
                });


                return {
                    name: key,
                    sections: sections,
                    info: _(responses[1].data.apps_items.modules).find(function (module) {
                        return module.name == key;
                    })
                };
            }).filter(function (module) {
                return !module.info.gui_hidden || troubleshooting.entityDebugEnabled();
            }).sortBy(function (module) {
                return parseInt(module.info.security_stack_order);
            }).sortBy(function (module) {
                var category = _(responses[1].data.apps_categories.categories).find(function (cat) {
                    return cat.categoryId === module.info.category_id;
                });
                if (category) {
                    return parseInt(category.securityStackOrder);
                }
            }).map(function(module, idx) {

                var lines = [{
                    text: '#' + JSON.stringify({
                        display: 'img-div',
                        path: path('security-stack').img('solutions/' + module.info.stack_icon),
                        'class': {
                            'module-stats-image': true
                        }
                    })
                }, {
                    text: '#' + JSON.stringify({
                        display: 'text',
                        text: module.info.label
                    }),
                    'class': 'module-stats-category blue-text'
                }];
                addFoundLines(lines, module);

                var labels = [];
                if (module.info.is_beta) {
                    labels.push('#' + JSON.stringify({
                            display: 'text',
                            text: 'BETA',
                            'class': 'beta-label'
                        }));
                }
                if (module.info.isExpired) {
                    labels.push('#' + JSON.stringify({
                            display: 'text',
                            text: 'EXPIRED',
                            'class': 'expired-label'
                        }));
                }
                if (labels.length) {
                    lines.push({
                        text: labels.join(''),
                        class: 'module-stats-labels'
                    });
                }
                if (idx) {
                    lines.push({
                        text: '',
                        class: 'module-stats-separator'
                    });
                }

                return {
                    type: 'info',
                    moduleName: module.name,
                    style: {
                        'height': '230px',
                        'padding': '5px'
                    },
                    lines: lines
                };
            }).value();
            
            if(!$scope.model.widgets || !isSameWidgets($scope.model.widgets, newWidgets)) {
                $scope.model.widgets = newWidgets;
                $timeout(function () {
                    $rootScope.$broadcast('slider-size-changed');
                });
            } else {
                mergeWidgets($scope.model.widgets, newWidgets);
            }
            
        });
    };
});