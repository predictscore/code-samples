var m = angular.module('dashboard');

m.factory('topUsersPlotLogic', function(fileDao, $filter) {
    return function($scope, logic) {
        fileDao.topUsers(logic.mode, logic.usersAmount).then(function(response) {
            var rows = response.data.rows || [];
            rows = _(rows).chain().sortBy('amount')
                .first(logic.usersAmount).value();
            var idx = 0;

            $scope.model.flotOptions.series.bars.numbers = {
                show: true,
                    font: '14px open-sans',
                    fontColor: '#444',
                    threshold: 0.20,
                    xAlign: _.identity,
                    yAlign: _.identity,
                    xOffset: 5,
                    formatter: function (x) {return $filter('number')(x);}
            };

            $scope.model.flotOptions.xaxis.transform = function (v) {
                return Math.log(v + 2);
            };
            $scope.model.flotOptions.xaxis.inverseTransform = function (v) {
                return Math.exp(v) - 2;
            };

            var ticks = $scope.model.flotOptions.yaxis.ticks = [];
            for(var i = 0; i < logic.usersAmount - rows.length; i++) {
                ticks.push([idx++, '']);
            }
            var userToIdx = {};
            _(rows).each(function(row) {
                userToIdx[row.user_id] = idx++;
                ticks.push([userToIdx[row.user_id], row.display_name || row.user_id]);
            });
            $scope.model.data = _(rows).map(function(row) {
                return {
                    label:  row.display_name || row.user_id,
                    data: [[row.amount, userToIdx[row.user_id]]]
                };
            });
            $scope.model.style = $scope.model.style || {};
            $.extend($scope.model.style, {
                height: idx * 30 + 'px'
            });

        });

        return _.noop;
    };
});