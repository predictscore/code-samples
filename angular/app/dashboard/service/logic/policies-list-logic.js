var m = angular.module('dashboard');

m.factory('policiesListLogic', function(policyDao, widgetSettings, modulesManager, $q, defaultListConverter, modals, dashboardWidgetDao) {
    var tagOnlyMode = true;

    var defaultPolicyNameTitle, defaultMatchTitle;

    return function($scope, logic, reinit) {
        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }

        if (_($scope.model.originalClass).isUndefined()) {
            $scope.model.originalClass = $scope.model['class'] || {};
        }
        if (_($scope.model.originalType).isUndefined()) {
            $scope.model.originalType = $scope.model.type;
        }

        var defaultTitle = ' ';
        if (!defaultPolicyNameTitle) {
            defaultPolicyNameTitle = (_($scope.model.columns).find(function (column) {
                return column.id === 'policy_name';
            }) || {}).text;
        }

        if (!defaultMatchTitle) {
            defaultMatchTitle = (_($scope.model.columns).find(function (column) {
                return column.id === 'Match';
            }) || {}).text;
        }

        $scope.model.loadedPolicies = $scope.model.loadedPolicies || [];

        function propertiesDialog () {
            var policyVariants = _($scope.model.loadedPolicies).map(function (policy) {
                return {
                    id: policy.policy_id,
                    label: policy.policy_name
                };
            });
            var tags = _($scope.model.loadedPolicies).chain().pluck('policy_data')
                .pluck('tags').flatten().compact().unique().value();
            var config = $scope.model.config || {};
            var params = [{
                type: 'string',
                id: 'title',
                label: 'Widget Title',
                data: config.title
            }, {
                type: 'string',
                id: 'policyNameTitle',
                label: 'First Column Title',
                data: config.policyNameTitle
            }, {
                type: 'string',
                id: 'matchTitle',
                label: 'Second Column Title',
                data: config.matchTitle
            }, {
                type: 'radio-group',
                id: 'mode',
                label: 'Query fetch mode',
                buttons: [{
                    value: 'tag',
                    label: 'By Tag'
                }, {
                    value: 'policy',
                    label: 'Explicit query'
                }],
                paramClass: 'radio-group-inline align-with-label',
                data: config.mode || 'policy'
            }, {
                type: 'string',
                id: 'tag',
                label: 'Query Tag',
                data: config.tag,
                variants: tags,
                validation: {
                    required: true
                },
                enabled: {
                    param: 'mode',
                    equal: 'tag'
                }
            }, {
                type: 'list',
                id: 'policies',
                label: 'Choose queries',
                variants: policyVariants,
                data: config.policies,
                validation: {
                    required: true
                },
                enabled: {
                    param: 'mode',
                    equal: 'policy'
                }
            }];

            if (tagOnlyMode) {
                params = _(params).filter(function (param) {
                    if (param.id === 'mode' || (param.enabled && param.enabled.param === 'mode' && param.enabled.equal !== 'tag')) {
                        return false;
                    }
                    if (param.enabled && param.enabled.param === 'mode') {
                        delete param.enabled;
                    }
                    return true;
                });
            }

            modals.params([{
                lastPage: true,
                okButton: 'Save',
                params: params,
                buttons: [{
                    label: 'Reset to default',
                    execute: function () {
                        if (!$scope.model.defaults) {
                            return;
                        }
                        if ($scope.model.defaults[logic.infoboxId]) {
                            $scope.model.config = $.extend(true, {}, $scope.model.defaults[logic.infoboxId].conf);
                            dashboardWidgetDao.save(logic.infoboxId, $scope.model.config, true);
                            buildTable();
                        } else {
                            delete $scope.model.config;
                            delete $scope.model.data;
                            $scope.model.wrapper.title = defaultTitle;
                            dashboardWidgetDao.remove(logic.infoboxId).then(function () {
                                reinit();
                            });
                        }
                    }
                }]
            }], 'Configure widget').then(function (data) {
                if (tagOnlyMode) {
                    data.mode = 'tag';
                }
                $scope.model.config = $.extend(true, {}, data);
                dashboardWidgetDao.save(logic.infoboxId, $scope.model.config);
                buildTable();
            });
        }

        if (!$scope.model.defaults) {
            dashboardWidgetDao.defaults().then(function (response) {
                $scope.model.defaults = response.data;
            });
        }


        $q.all([
            policyDao.infoboxCatalogWithFindings(),
            dashboardWidgetDao.retrieve(logic.infoboxId).then(function (response) {
                $scope.model.config = response.data.conf;
                return $q.when(response);
            }).then(function () {
                $scope.model.wrapper.title = $scope.model.config.title || defaultTitle;
            })
        ]).then(function (responses) {
            $scope.model.wrapper.isLoading = false;
            $scope.model.loadedPolicies = responses[0].data.rows;

            $scope.model.wrapper.buttons = [{
                'class': 'btn-icon',
                iconClass: 'fa fa-wrench',
                tooltip: 'Configuration',
                execute: function() {
                    propertiesDialog();
                }
            }];
            buildTable();
        });

        function buildTable() {
            if (!$scope.model.config) {
                return;
            }
            $scope.model.wrapper.title = $scope.model.config.title || defaultTitle;
            var col = _($scope.model.columns).find(function (column) {
                return column.id === 'policy_name';
            });
            if (col) {
                col.text = $scope.model.config.policyNameTitle || defaultPolicyNameTitle;
            }
            col = _($scope.model.columns).find(function (column) {
                return column.id === 'Match';
            });
            if (col) {
                col.text = $scope.model.config.matchTitle || defaultMatchTitle;
            }
            var rows = [];
            if ($scope.model.config.mode === 'tag') {
                rows = _($scope.model.loadedPolicies).filter(function (policy) {
                    return _(policy.policy_data.tags).contains($scope.model.config.tag);
                });
            } else {
                var policyMap = _($scope.model.config.policies).chain().map(function (policyId, idx) {
                    return [policyId, idx];
                }).object().value();

                rows = _($scope.model.loadedPolicies).chain().filter(function (policy) {
                    return !_(policyMap[policy.policy_id]).isUndefined();
                }).sortBy(function (policy) {
                    return policyMap[policy.policy_id];
                }).value();
            }

            var total = 0;

            var tableData = _(rows).map(function (row) {
                total += row.findingsStatus.Match;
                return {
                    policy_id: row.policy_id,
                    policy_name: row.policy_name,
                    Match: row.findingsStatus.Match,
                    saas: row.context.saas
                };
            });

            if (tableData.length) {
                tableData.unshift({
                    policy_name: 'Total',
                    Match: total,
                    policy_id: null,
                    saas: null
                });
                defaultListConverter({data: {rows: tableData}}, {
                    columns: $scope.model.columns
                }).then(function (response) {
                    $scope.model.data = response.data.data;
                });
                $scope.model.type = $scope.model.originalType;
                $scope.model['class'] = $scope.model.originalClass;
            } else {
                $scope.model.type = 'info';
                $scope.model.lines = [{
                    text: 'No queries match the current configurations.'
                }, {
                    text: 'Click here to configure.',
                    "class": "configure-link",
                    execute: function () {
                        propertiesDialog();
                    }
                }];
                $scope.model['class'] = 'policy-severity-empty';
            }

        }
        return _.noop;
    };
});