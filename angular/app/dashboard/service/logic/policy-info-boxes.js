var m = angular.module('dashboard');

m.factory('policyInfoBoxes', function(policyDao, $q, modulesManager, modals, feature, dashboardWidgetDao, policiesRefreshHelper) {
    var tagOnlyMode = false;
    var enabledModes = ['tag', 'list'];

    var goodColors = ['#2980b9','#16a085','#89b041','#f39c12','#f35428'];
    var showTypes = [{
        id: 'match',
        label: 'Match',
        expression: 'Match'
    }/* currently it's impossible to show anything, except match, but later it can return, so not removing now
    , {
        id: 'unmatch',
        label: 'Unmatch',
        expression: 'Unmatch',
        qs: {status: 'Unmatch'}
    }, {
     id: 'matchPercent',
     label: '% Match',
     expression: 'Match / total * 100',
        hideHistory: true
     }, {
        id: 'unmatchPercent',
        label: '% Unmatch',
        expression: 'Unmatch / total * 100',
        qs: {status: 'Unmatch'},
        hideHistory: true
     }, {
        id: 'pendingPercent',
        label: '% Pending',
        expression: 'Pending / total * 100',
        qs: {status: 'Pending'},
        hideHistory: true
    }*/];

    var periods = [{
        id: '1 hour',
        'label': '1 hour ago'
    }, {
        id: '1 day',
        'label': '1 day ago'
    }, {
        id: '7 day',
        'label': '7 days ago'
    }, {
        id: '1 month',
        'label': '1 month ago'
    }, {
        id: '3 month',
        'label': '3 months ago'
    }, {
        id: '1 year',
        'label': '1 year ago'
    }];

    var historyTypes = _(showTypes).chain().filter(function (type) {
        return !type.hideHistory;
    }).pluck('id').value();

    var policyVariants = [];
    var policyTags = [];
    var reinitFn;
    var defaults;
    var disableCache = false;

    function propertiesDialog (boxIdx, settings) {

        var titleModified = false;
        function modeOnChange(scope, newValue, oldValue) {
            if (!titleModified) {
                var titleParam = scope.findPageParamById('title');
                if (titleParam) {
                    if (newValue !== settings.conf.mode) {
                        titleParam.data = '';
                    } else {
                        titleParam.data = settings.conf.title;
                    }
                }
            }
        }
        function titleOnChange(scope, newValue, oldValue) {
            if (!titleModified) {
                if (newValue !== '' && newValue !== settings.conf.title) {
                    titleModified = true;
                }
            }
        }


        var params = [{
            type: 'string',
            id: 'title',
            label: 'Title',
            data: settings.conf.title,
            onChange: titleOnChange
        }, {
            type: 'color',
            id: 'color',
            label: 'Color',
            options: {
                showPalette: true,
                palette: _(goodColors).map(function (color) {return [color];})
            },
            data: settings.conf.color
        }, {
            type: 'selector',
            id: 'showType',
            label: 'Show',
            variants: showTypes,
            data: settings.conf.showType,
            validation: {
                required: true
            }
        }, {
            type: 'string',
            id: 'valueTitle',
            label: 'Value description',
            data: settings.conf.valueTitle
        }, {
            type: 'radio-group',
            id: 'mode',
            label: 'Query fetch mode',
            buttons: [{
                value: 'policy',
                label: 'Single Query'
            }, {
                value: 'tag',
                label: 'By Tag'
            }, {
                value: 'list',
                label: 'By Queries'
            }],
            data: settings.conf.mode || (enabledModes && enabledModes.length && enabledModes[0]) || 'policy',
            onChange: modeOnChange
        }, {
            type: 'list',
            id: 'policyId',
            label: 'Query',
            variants: policyVariants,
            data: settings.conf.policyId,
            settings: {
                multiple: false
            },
            validation: {
                required: true
            },
            enabled: [{
                param: 'mode',
                equal: 'policy'
            }]
        }, {
            type: 'string',
            id: 'tag',
            label: 'Query Tag',
            data: settings.conf.tag,
            variants: policyTags,
            validation: {
                required: true
            },
            enabled: [{
                param: 'mode',
                equal: 'tag'
            }]
        }, {
            type: 'list',
            id: 'policies',
            label: 'Choose queries',
            variants: policyVariants,
            data: settings.conf.policies,
            validation: {
                required: true
            },
            enabled: [{
                param: 'mode',
                equal: 'list'
            }]
        }, {
            type: 'radio-group',
            id: 'rightMode',
            label: 'Right value',
            buttons: [{
                value: 'none',
                label: 'none'
            }, {
                value: 'history',
                label: 'History'
            }, {
                value: 'value',
                label: 'Another value'
            }],
            paramClass: 'radio-group-inline align-with-label',
            data: settings.conf.rightMode || 'none'
        }, {
            type: 'selector',
            id: 'period',
            label: 'Show change with data',
            variants: periods,
            data: settings.conf.period,
            enabled: [{
                param: 'showType',
                'in': historyTypes
            }, {
                param: 'rightMode',
                'equal': 'history'
            }]
        }, {
            type: 'radio-group',
            id: 'uparrow',
            label: 'Change arrows colors',
            enabled: [{
                param: 'showType',
                'in': historyTypes
            }, {
                param: 'rightMode',
                'equal': 'history'
            }],
            buttons: [{
                value: 'green',
                label: '#' + JSON.stringify({
                    text: '<span class="infobox-trend-arrows"><span class="fa fa-long-arrow-up"></span><span class="fa fa-long-arrow-down"></span></span>',
                    display: 'html'
                })
            }, {
                value: 'red',
                label: '#' + JSON.stringify({
                    text: '<span class="infobox-trend-arrows uparrow-red"><span class="fa fa-long-arrow-up"></span><span class="fa fa-long-arrow-down"></span></span>',
                    display: 'html'
                })
            }],
            paramClass: 'radio-group-inline align-with-label infobox-param-arrows',
            data: settings.conf.uparrow
        }, {
            type: 'selector',
            id: 'showType2',
            label: 'Right Value Show Type',
            variants: showTypes,
            data: settings.conf.showType2,
            validation: {
                required: true
            },
            enabled: [{
                param: 'rightMode',
                'equal': 'value'
            }]
        }, {
            type: 'string',
            id: 'valueTitle2',
            label: 'Right Value description',
            data: settings.conf.valueTitle2,
            enabled: [{
                param: 'rightMode',
                'equal': 'value'
            }]
        }, {
            type: 'radio-group',
            id: 'mode2',
            label: 'Right Value Fetch Mode',
            buttons: [{
                value: 'policy',
                label: 'Single Query'
            }, {
                value: 'tag',
                label: 'By Tag'
            }, {
                value: 'list',
                label: 'By Queries'
            }],
            data: settings.conf.mode2 || (enabledModes && enabledModes.length && enabledModes[0]) || 'policy',
            enabled: [{
                param: 'rightMode',
                'equal': 'value'
            }]
        }, {
            type: 'list',
            id: 'policyId2',
            label: 'Right Value Query',
            variants: policyVariants,
            data: settings.conf.policyId2,
            settings: {
                multiple: false
            },
            validation: {
                required: true
            },
            enabled: [{
                param: 'mode2',
                equal: 'policy'
            }, {
                param: 'rightMode',
                'equal': 'value'
            }]
        }, {
            type: 'string',
            id: 'tag2',
            label: 'Right Value Query Tag',
            data: settings.conf.tag2,
            variants: policyTags,
            validation: {
                required: true
            },
            enabled: [{
                param: 'mode2',
                equal: 'tag'
            }, {
                param: 'rightMode',
                'equal': 'value'
            }]
        }, {
            type: 'list',
            id: 'policies2',
            label: 'Right Value Queries',
            variants: policyVariants,
            data: settings.conf.policies2,
            validation: {
                required: true
            },
            enabled: [{
                param: 'mode2',
                equal: 'list'
            }, {
                param: 'rightMode',
                'equal': 'value'
            }]
        }];

        if (tagOnlyMode) { //This code is out-dated now. Update it if tagOnlyMode is required (processing of mode2 and showtypes should be added)
            params = _(params).filter(function (param) {
                if (param.id === 'mode' || (param.enabled && param.enabled.param === 'mode' && param.enabled.equal !== 'tag')) {
                    return false;
                }
                if (param.enabled && param.enabled.param === 'mode') {
                    delete param.enabled;
                }
                return true;
            });
        } else {
            params = _(params).filter(function (param) {
                if ((param.id === 'mode' && enabledModes.length < 2) ||
                    (param.id === 'mode2' && enabledModes.length < 2)) {
                    return false;
                }
                if (param.id === 'mode' || param.id === 'mode2') {
                    param.buttons = _(param.buttons).filter(function (button) {
                        return _(enabledModes).contains(button.value);
                    });
                }
                if (showTypes.length === 1 && param.variants === showTypes) {
                    return false;
                }
                if (param.enabled) {
                    var modeParam = _(param.enabled).find(function (param) {
                        return param.param === 'mode' || param.param === 'mode2';
                    });
                    if (modeParam && !_(enabledModes).contains(modeParam.equal)) {
                        return false;
                    }
                    if (enabledModes.length < 2 && modeParam) {
                        param.enabled = _(param.enabled).filter(function (param) {
                            return param.param !== 'mode' && param.param !== 'mode2';
                        });
                        if (!param.enabled.length) {
                            delete param.enabled;
                        }
                    }
                }
                return true;
            });
        }

        modals.params([{
            lastPage: true,
            okButton: 'Save',
            params: params,
            buttons: [{
                label: 'Reset to default',
                execute: function () {
                    if (!defaults) {
                        return;
                    }
                    if (defaults[settings.id]) {
                        dashboardWidgetDao.save(settings.id, defaults[settings.id].conf, true)
                            .then(function () {
                                disableCache = true;
                                reinitFn();
                            });
                    } else {
                        dashboardWidgetDao.remove(settings.id)
                            .then(function () {
                                disableCache = true;
                                reinitFn();
                            });
                    }
                }
            }]
        }], 'Configure infobox',null , {
            bodyClass: 'policy-infobox-config-modal'
        }).then(function (data) {
            if (tagOnlyMode) {
                data.mode = 'tag';
            }
            if (data.rightMode !== 'history') {
                data.period = 0;
            }
            if (showTypes.length === 1) {
                data.showType = showTypes[0].id;
                data.showType2 = showTypes[0].id;
            }
            dashboardWidgetDao.save(settings.id, data)
                .then(function () {
                    disableCache = true;
                    reinitFn();
                });
        });
    }

    var defaultSettings = {
        updateSec: 60,
        showType: 'match',
        period: 0,
        uparrow: 'green',
        rightMode: 'none',
        showType2: 'match'
    };


    return function($scope, logic, reinit) {
        reinitFn = reinit;

        logic.updateTick = ((feature.dashboardUpdateTicks && feature.dashboardUpdateTicks.infoBox) || 60) * 1000;

        var idxToId = logic.boxesIdxToId || _(logic.boxesAmount || 8).chain().range().map(function (idx) {
            return '' + (idx + 1);
        }).value();

        var defaultColors = _(idxToId).map(function (id, idx) {
            return logic.defaultColors && logic.defaultColors[idx] || goodColors[idx % goodColors.length];
        });

        if (!$scope.model.defaults) {
            defaults = null;
            dashboardWidgetDao.defaults().then(function (response) {
                defaults = $scope.model.defaults = response.data;
            });
        } else {
            defaults = $scope.model.defaults;
        }


        dashboardWidgetDao.widgetsWithData(idxToId, disableCache).then(function (response) {
            disableCache = false;
            var tags = [];

            policyVariants = _(response.data.policyCatalog).chain().filter(function (row) {
                return row.policy_name;
            }).map(function (policy) {
                tags.push(policy.policy_data.tags);
                return {
                    id: policy.policy_id,
                    label: policy.policy_name
                };
            }).value();
            policyTags = _(tags).chain().flatten().compact().unique().value();

            var infoBoxesConfig = response.data.settings;


            function processSettings(boxIdx, settings) {
                var policy;
                var widgetTitle;
                var setDefault = true;
                var defaultTitle = '';


                if (settings) {
                    switch (settings.conf.mode) {
                        case 'list':
                            if (settings.conf.policies && settings.conf.policies.length) {
                                policy = _(policyVariants).find(function (variant) {
                                    return _(settings.conf.policies).contains(variant.id);
                                });
                                if (policy) {
                                    setDefault = false;
                                    defaultTitle = policy.label;
                                }
                            }
                            break;
                        case 'tag':
                            defaultTitle = 'Queries by tag total';
                            setDefault = false;
                            break;
                        default:
                            if (settings.conf.policyId) {
                                policy = _(policyVariants).find(function (variant) {
                                    return variant.id === settings.conf.policyId;
                                });
                                if (policy) {
                                    setDefault = false;
                                    defaultTitle = policy.label;
                                }
                            }
                    }
                }

                if (setDefault) {
                    settings = {
                        conf: $.extend({}, defaultSettings),
                        id: idxToId[boxIdx]
                    };
                    widgetTitle = 'Click to configure';
                } else {
                    _(settings.conf).defaults(defaultSettings);
                    widgetTitle = settings.conf.title || defaultTitle;
                    if (_(showTypes).chain().find(function (type) {
                            return type.id === settings.conf.showType;
                        }).isUndefined().value()) {
                        settings.conf.showType = showTypes[0].id;
                    }
                }

                if (!/#[0-9a-fA-F]{6}/.test(settings.conf.color)) {
                    settings.conf.color = defaultColors[boxIdx] || goodColors[boxIdx % goodColors.length];
                }
                settings.widgetTitle = widgetTitle;

                if (settings.conf.rightMode !== 'value') {
                    if (_(periods).find(function (period) {
                            return period.id === settings.conf.period;
                        })) {
                        settings.conf.rightMode = 'history';
                    } else {
                        settings.conf.rightMode = 'none';
                    }
                }
                if (!_(periods).find(function (period) {
                        return period.id === settings.conf.period;
                    })) {
                    settings.conf.period = periods[0].id;
                }

                if (settings.conf.rightMode === 'value') {
                    var resetRight = true;
                    switch (settings.conf.mode2) {
                        case 'list':
                            if (settings.conf.policies2 && settings.conf.policies2.length) {
                                policy = _(policyVariants).find(function (variant) {
                                    return _(settings.conf.policies2).contains(variant.id);
                                });
                                if (policy) {
                                    resetRight = false;
                                }
                            }
                            break;
                        case 'tag':
                            resetRight = false;
                            break;
                        default:
                            if (settings.conf.policyId2) {
                                policy = _(policyVariants).find(function (variant) {
                                    return variant.id === settings.conf.policyId2;
                                });
                                if (policy) {
                                    resetRight = false;
                                }
                            }
                    }
                    if (resetRight) {
                        settings.conf.rightMode = 'none';
                    }
                }

                return settings;
            }


            function createWidget(boxIdx, settings) {
                var widget = {
                    "type": "info",
                    "logic": {
                        "name": "policyInfoBox",
                        "settings": settings.conf,
                        "properties": {
                            showTypes: showTypes,
                            periods: periods
                        },
                        "updateTick": null
                    },
                    "settings": settings,
                    "size": logic.boxesSize,
                    "class": {
                        "avanan-info-box": true,
                        "policy-info-box": true
                    },
                    "wrapper": {
                        "type": "widget-panel",
                        "class": {
                            "avanan": true,
                            "policy-infobox": true,
                            "custom-color": true
                        },
                        "buttons": [{
                            position: 'right',
                            'class': 'policy-infobox-settings',
                            iconClass: 'fa fa-cogs',
                            execute: function() {
                                propertiesDialog(boxIdx, settings);
                            }
                        }],
                        "title": settings.widgetTitle
                    }
                };
                if (infoBoxesConfig[idxToId[boxIdx]]) {
                    _(infoBoxesConfig[idxToId[boxIdx]].policies).each(function(policy) {
                        widget.wrapper.class['policy_id-' + policy.policy_id] = true;
                    });
                }

                widget.wrapper.style = {'background-color':settings.conf.color};
                return widget;
            }

            $scope.model.widgets = $scope.model.widgets || _(idxToId).map(function () {return {};});

            _($scope.model.widgets).each(function (widget, idx) {
                updateWidget(idx);
            });

            function updateWidget(boxIdx) {
                var staticWidget = logic.staticBoxes && logic.staticBoxes[idxToId[boxIdx]];
                if (staticWidget) {
                    if (!_($scope.model.widgets[boxIdx]).isEmpty()) {
                        return;
                    }
                    $scope.model.widgets[boxIdx] = staticWidget;
                    return;
                }
                var settings = processSettings(boxIdx, (infoBoxesConfig[idxToId[boxIdx]] || {}).options);
                if (!_($scope.model.widgets[boxIdx].settings).isEqual(settings)) {
                    $scope.model.widgets[boxIdx] = createWidget(boxIdx, settings);
                }
                if (infoBoxesConfig[idxToId[boxIdx]]) {
                    $scope.model.widgets[boxIdx].data = {
                        findings: infoBoxesConfig[idxToId[boxIdx]].findings,
                        history: infoBoxesConfig[idxToId[boxIdx]].history,
                        policies: infoBoxesConfig[idxToId[boxIdx]].policies,
                        rightData: infoBoxesConfig[idxToId[boxIdx]].rightData
                    };
                }
            }
        });

        var configListener = $scope.$on('configure-infobox', function (event, data) {
            var infoboxIdx = _($scope.model.widgets).findIndex(function (widget) {
                return widget.settings && widget.settings.id === data.infoboxId;
            });
            if (infoboxIdx !== -1) {
                propertiesDialog(infoboxIdx, $scope.model.widgets[infoboxIdx].settings);
            }
        });
        return function () {
            configListener();
        };
    };
});