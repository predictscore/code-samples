var m = angular.module('dashboard');

m.factory('dashboardSecurityStatLogic', function(modulesManager, $q, objectTypeDao, $rootScope) {

    var modulesStatsWidget = {
        "type": "slider",
        "logic": {
            "name": "modulesStatsLogic",
            "updateTick": null,
            "entityType": "google_file"
        },
        "size": "no-padding",
        "style": {
            "height": "280px"
        },
        "wrapper": {
            "type": "widget-panel",
            "class": "solid-avanan slider-without-border",
            "title": "Security stack",
            "minimize": true
        },
        "slider": {
            "slideWidth": 150,
            "minSlides": 1,
            "maxSlides": 10,
            "slideMargin": 0,
            "useCSS": true,
            "infiniteLoop": false,
            "hideControlOnEnd": true,
            "speed": 0
        }
    };

    var avLabelWidget = {
        "type": "info",
        "size": "col-lg-6 col-md-6 col-sm-6 col-xs-6",
        "wrapper": {
            "type": "none"
        },
        "class": "multi-engine-label",
        "style": {
            "height": "30px"
        },
        "lines": [{
            "text": "MALICIOUS",
            "class": "multi-engine-label-left"
        }]
    };
    var avChartWidget = {
        "type": "flot",
        "logic": {
            "name": "multiFindingsPlotLogic",
            "updateTick": 60000,
            "tag": "vendors_avs"
        },
        "size": "col-lg-6 col-md-6 col-sm-6 col-xs-6",
        "style": {
            "height": "170px"
        },
        "wrapper": {
            "type": "none"
        },
        "flotOptions": {
            "series": {
                "funnel": {
                    "margin": {
                        "left": 0.3,
                        "right": 0
                    },
                    "label": {
                        "align": "outerLeft"
                    }
                }
            }
        }
    };

    var avLabelWidgetSingle = {
        "type": "info",
        "size": "col-lg-12 col-md-12 col-sm-12 col-xs-12",
        "wrapper": {
            "type": "none"
        },
        "class": "multi-engine-label",
        "style": {
            "height": "30px"
        },
        "lines": [{
            "text": "MALICIOUS",
            "class": "multi-engine-label-left single"
        }]
    };

    var avChartWidgetSingle = {
        "type": "flot",
        "logic": {
            "name": "multiFindingsPlotLogic",
            "updateTick": 60000,
            "tag": "vendors_avs"
        },
        "size": "col-lg-12 col-md-12 col-sm-12 col-xs-12",
        "style": {
            "height": "170px"
        },
        "wrapper": {
            "type": "none"
        },
        "flotOptions": {
            "series": {
                "funnel": {
                    "margin": {
                        "left": 0.2,
                        "right": 0.1
                    },
                    "label": {
                        "align": "outerLeft"
                    }
                }
            }
        }
    };


    var dlpLabelWidget = {
        "type": "info",
        "size": "col-lg-6 col-md-6 col-sm-6 col-xs-6",
        "wrapper": {
            "type": "none"
        },
        "class": "multi-engine-label",
        "style": {
            "height": "30px"
        },
        "lines": [{
            "text": "DLP",
            "class": "multi-engine-label-right"
        }]
    };

    var dlpChartWidget = {
        "type": "flot",
        "logic": {
            "name": "multiFindingsPlotLogic",
            "updateTick": 60000,
            "tag": "vendors_dlp"
        },
        "size": "col-lg-6 col-md-6 col-sm-6 col-xs-6",
        "style": {
            "height": "170px"
        },
        "wrapper": {
            "type": "none"
        },
        "flotOptions": {
            "series": {
                "funnel": {
                    "margin": {
                        "left": 0,
                        "right": 0.3
                    },
                    "label": {
                        "align": "outerRight"
                    }
                }
            }
        }
    };

    var dlpLabelWidgetSingle = {
        "type": "info",
        "size": "col-lg-12 col-md-12 col-sm-12 col-xs-12",
        "wrapper": {
            "type": "none"
        },
        "class": "multi-engine-label",
        "style": {
            "height": "30px"
        },
        "lines": [{
            "text": "DLP",
            "class": "multi-engine-label-right single"
        }]
    };


    var dlpChartWidgetSingle = {
        "type": "flot",
        "logic": {
            "name": "multiFindingsPlotLogic",
            "updateTick": 60000,
            "tag": "vendors_dlp"
        },
        "size": "col-lg-12 col-md-12 col-sm-12 col-xs-12",
        "style": {
            "height": "170px"
        },
        "wrapper": {
            "type": "none"
        },
        "flotOptions": {
            "series": {
                "funnel": {
                    "margin": {
                        "left": 0.1,
                        "right": 0.2
                    },
                    "label": {
                        "align": "outerRight"
                    }
                }
            }
        }
    };

    var multiContainer = {
        "type": "multi-widget",
        "style": {
            "height": "230px"
        },
        "bodyStyle": {
            "padding-bottom": "60px"
        },
        "wrapper": {
            "type": "widget-panel",
            "title": "Multi-engine detections",
            "class": "solid-avanan white-body overflow-auto multi-findings-chart"
        },
        "widgets": []
    };

    var sizes = {
        'half': {
            '1': {
                'stats': 'col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding'
            },
            '2': {
                'stats': 'col-lg-9 col-md-9 col-sm-8 col-xs-8 no-padding',
                'multi': 'col-lg-3 col-md-3 col-sm-4 col-xs-4 no-padding'
            },
            '3': {
                'stats': 'col-lg-7 col-md-8 col-sm-6 col-xs-12 no-padding',
                'multi': 'col-lg-5 col-md-4 col-sm-6 col-xs-12 no-padding'
            }
        },
        'full': {
            '1': {
                'stats': 'col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding'
            },
            '2': {
                'stats': 'col-lg-9 col-md-9 col-sm-8 col-xs-8 no-padding',
                'multi': 'col-lg-3 col-md-3 col-sm-4 col-xs-4 no-padding'
            },
            '3': {
                'stats': 'col-lg-8 col-md-8 col-sm-6 col-xs-12 no-padding',
                'multi': 'col-lg-4 col-md-4 col-sm-6 col-xs-12 no-padding'
            }
        }
    };

    return function ($scope, logic) {
        var statsWidget = angular.copy(modulesStatsWidget);
        var multiWidget = angular.copy(multiContainer);
        if (logic.multiEngineTitle) {
            multiWidget.wrapper.title = logic.multiEngineTitle;
        }
        statsWidget.size = sizes[logic.size]['1'].stats;
        $scope.model.widgets = [statsWidget];
        $scope.multiWidgets = {};
        $q.all([
            objectTypeDao.entities(),
            modulesManager.init()
        ]).then(function (responses) {
            var apps = modulesManager.activeSecAppsFull();
            var entities = responses[0].data.entities;
            var avApps = _(apps).filter(function (app) {
                return entities[app.sec_entity] && entities[app.sec_entity].sec_type === 'av';
            });
            var dlpApps = _(apps).filter(function (app) {
                return entities[app.sec_entity] && entities[app.sec_entity].sec_type === 'dlp';
            });
            if (avApps.length < 2 && dlpApps.length < 2) {
                return;
            }

            if (avApps.length > 1) {
                if (dlpApps.length > 1) {
                    multiWidget.widgets.push($scope.multiWidgets.avLabel = angular.copy(avLabelWidget));
                    multiWidget.widgets.push($scope.multiWidgets.dlpLabel = angular.copy(dlpLabelWidget));
                    multiWidget.widgets.push($scope.multiWidgets.avChart = angular.copy(avChartWidget));
                    multiWidget.widgets.push($scope.multiWidgets.dlpChart = angular.copy(dlpChartWidget));
                } else {
                    multiWidget.widgets.push($scope.multiWidgets.avLabel = angular.copy(avLabelWidgetSingle));
                    multiWidget.widgets.push($scope.multiWidgets.avChart = angular.copy(avChartWidgetSingle));
                }
            } else {
                multiWidget.widgets.push($scope.multiWidgets.dlpLabel = angular.copy(dlpLabelWidgetSingle));
                multiWidget.widgets.push($scope.multiWidgets.dlpChart = angular.copy(dlpChartWidgetSingle));
            }

            _(multiWidget.widgets).each(function (widget) {
                if (widget.logic) {
                    widget.logic.entityType = logic.entityType;
                    widget.logic.entityTitle = logic.entityTitle;
                }
            });
            $scope.currentMultiConfig = {av: false, dlp: false};
            multiWidget.size = 'display-none';

            $scope.model.widgets.push(multiWidget);

            function updateChart() {
                var config = {
                    av: $scope.multiWidgets.avChart && $scope.multiWidgets.avChart.notEmpty || false,
                    dlp: $scope.multiWidgets.dlpChart && $scope.multiWidgets.dlpChart.notEmpty || false
                };
                if (!_($scope.currentMultiConfig).isEqual(config)) {
                    $scope.currentMultiConfig = config;
                    if (!config.av && !config.dlp) {
                        multiWidget.size = 'display-none';
                        statsWidget.size = sizes[logic.size]['1'].stats;
                        return;
                    }
                    if (config.av) {
                        if (config.dlp) {
                            multiWidget.size = sizes[logic.size]['3'].multi;
                            statsWidget.size = sizes[logic.size]['3'].stats;


                            $.extend(true, $scope.multiWidgets.avLabel, avLabelWidget);
                            $.extend(true, $scope.multiWidgets.avChart, _(avChartWidget).pick('size', 'flotOptions'));

                            $.extend(true, $scope.multiWidgets.dlpLabel, dlpLabelWidget);
                            $.extend(true, $scope.multiWidgets.dlpChart, _(dlpChartWidget).pick('size', 'flotOptions'));

                        } else {
                            multiWidget.size = sizes[logic.size]['2'].multi;
                            statsWidget.size = sizes[logic.size]['2'].stats;

                            $.extend(true, $scope.multiWidgets.avLabel, avLabelWidgetSingle);
                            $.extend(true, $scope.multiWidgets.avChart, _(avChartWidgetSingle).pick('size', 'flotOptions'));

                            if ($scope.multiWidgets.dlpChart) {
                                $scope.multiWidgets.dlpLabel.size = 'display-none';
                                $scope.multiWidgets.dlpChart.size = 'display-none';
                            }
                        }
                    } else {
                        multiWidget.size = sizes[logic.size]['2'].multi;
                        statsWidget.size = sizes[logic.size]['2'].stats;

                        $.extend(true, $scope.multiWidgets.dlpLabel, dlpLabelWidgetSingle);
                        $.extend(true, $scope.multiWidgets.dlpChart, _(dlpChartWidgetSingle).pick('size', 'flotOptions'));

                        if ($scope.multiWidgets.avChart) {
                            $scope.multiWidgets.avLabel.size = 'display-none';
                            $scope.multiWidgets.avChart.size = 'display-none';
                        }

                    }
                    $rootScope.$broadcast('slider-size-changed');
                }
            }

            $scope.$watch('multiWidgets.avChart.notEmpty', updateChart);
            $scope.$watch('multiWidgets.dlpChart.notEmpty', updateChart);

        });
    };
});