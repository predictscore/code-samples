var m = angular.module('dashboard');

m.factory('worldAccessMapLogic', function(ipDao, worldAccessMapLogicCore) {
    return function($scope, logic) {
        return worldAccessMapLogicCore($scope, logic, {
            retrieve: function(days) {
                var q = ipDao.worldAccess(days);
                if (logic.ordering) {
                    q.order(logic.ordering.columnName, logic.ordering.order);
                }
                if (logic.limit) {
                    q.pagination(0, logic.limit);
                }
                return q;
            }
        });
    };
});