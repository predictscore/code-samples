var m = angular.module('dashboard');

m.factory('heatMapLogic', function(ipDao, widgetSettings, $timeout) {

    var heatDefaults = {
        radius: 12,
        blur: 12,
        minOpacity: 35,
        maxZoom: 7,
        ignoreMax: false
    };
    return function($scope, logic, reinit) {
        function updateData() {
            if (_($scope.model.updateData).isFunction()) {
                $scope.model.updateData();
            }
        }

        function loadData() {
            var params = widgetSettings.params($scope.model.uuid);
            if ($scope.model.heatParamsAdded) {
                if (params.resetHeatParams) {
                    _($scope.model.wrapper.filters).each(function (filter) {
                        if (filter.value in heatDefaults) {
                            filter.state = heatDefaults[filter.value];
                            params[filter.value] = heatDefaults[filter.value];
                        }
                        if (filter.value === 'resetHeatParams') {
                            filter.state = false;
                        }
                    });
                    params.resetHeatParams = false;
                    widgetSettings.params($scope.model.uuid, params);
                }
                $scope.model.heatParams = _(params).defaults(heatDefaults);

            }
            $timeout(function() {
                updateData();
                ipDao.heatMap(params.days).then(function(response) {
                    $scope.model.data = response.data.rows;
                    updateData();
                });
            });
        }
        if (!$scope.model.heatParamsAdded) {
            var savedParams = widgetSettings.params($scope.model.uuid);
            $scope.model.heatParamsAdded = true;
            if (!$scope.model.wrapper.filters) {
                $scope.model.wrapper.filters = [];
            }
            $scope.model.wrapper.filters.push({
                "type": "number",
                "value": "radius",
                "local": true,
                "text": "point radius",
                "state": savedParams.radius || heatDefaults.radius,
                "min": 1,
                "style": {
                    "width": "50px"
                }
            });
            $scope.model.wrapper.filters.push({
                "type": "number",
                "value": "blur",
                "local": true,
                "text": "point blur",
                "state": savedParams.blur || heatDefaults.blur,
                "min": 1,
                "style": {
                    "width": "50px"
                }
            });
            $scope.model.wrapper.filters.push({
                "type": "number",
                "value": "minOpacity",
                "local": true,
                "text": "min point opacity",
                "state": savedParams.minOpacity || heatDefaults.minOpacity,
                "min": 1,
                "style": {
                    "width": "50px"
                }
            });
            $scope.model.wrapper.filters.push({
                "type": "number",
                "value": "maxZoom",
                "local": true,
                "text": "zoom with max intensity",
                "state": savedParams.maxZoom || heatDefaults.maxZoom,
                "min": 1,
                "max": 19,
                "style": {
                    "width": "50px"
                }
            });
            $scope.model.wrapper.filters.push({
                "type": "flag",
                "value": "ignoreMax",
                "local": true,
                "text": "ignore point weight",
                "state": savedParams.ignoreMax || heatDefaults.ignoreMax
            });
            $scope.model.wrapper.filters.push({
                "type": "flag",
                "value": "resetHeatParams",
                "local": true,
                "text": "reset map params",
                "state": false
            });
        }

        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, function(params) {
            loadData();
        }, true);

        return function() {
            paramsWatch();
        };
    };
});