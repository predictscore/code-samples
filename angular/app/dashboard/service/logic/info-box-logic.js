var m = angular.module('dashboard');

m.factory('infoBoxLogic', function (viewerTypeDao, widgetSettings, $timeout, feature, widgetPolling) {
    var timebackWidgetUuid = "dashboard-info-boxes-timeback";
    return function ($scope, logic, reinitLogic) {
        if (feature.dashboardUpdateTicks && feature.dashboardUpdateTicks.infoBox) {
            logic.updateTick = feature.dashboardUpdateTicks.infoBox * 1000;
        }

        if (!_($scope.model.lines).isArray()) {
            $scope.model.lines = [{
                text: '0',
                'class': 'count zero-opacity'
            }, {
                text: logic.title,
                'class': 'message'
            }];
        }
        var after = 0;
        var periodWatch = _.noop;
        if (logic.usePollingPeriod) {
            var period = widgetPolling.scope().period;
            if (period && period.days) {
                after = period.days;
            }
            periodWatch = $scope.$watch(function () {
                return widgetPolling.scope().period && widgetPolling.scope().period.days;
            }, function (newValue, prevValue) {
                if (newValue != prevValue) {
                    reinitLogic();
                }
            });
        }
        var params = $.extend({
            after: after
        }, logic.params);
        viewerTypeDao.count(logic.id, params, logic.saas).then(function (countResponse) {
            $scope.model.lines = $.extend(true, $scope.model.lines || [], [{
                text: '#' + JSON.stringify({
                    text: countResponse.data,
                    type: 'viewer',
                    id: logic.id,
                    module: logic.saas,
                    qs: params
                })
            }, {
                text: logic.title
            }]);

            if(_($scope.model.lines).first().class.indexOf('zero-opacity') != -1) {
                $timeout(function () {
                    _($scope.model.lines).first().class = 'count';
                });
            }
        });

        var dataWatch = $scope.$watch(function () {
            return widgetSettings.param(timebackWidgetUuid, 'data');
        }, function (newValue, prevValue) {
            if (newValue != prevValue) {
                reinitLogic();
            }
        });

        return function () {
            dataWatch();
            periodWatch();
        };
    };
});
