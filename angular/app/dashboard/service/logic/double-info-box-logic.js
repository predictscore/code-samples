var m = angular.module('dashboard');

m.factory('doubleInfoBoxLogic', function (viewerTypeDao, widgetSettings, $timeout, $q, feature) {
    var timebackWidgetUuid = "dashboard-info-boxes-timeback";
    return function ($scope, logic, reinitLogic) {
        if (feature.dashboardUpdateTicks && feature.dashboardUpdateTicks.infoBox) {
            logic.updateTick = feature.dashboardUpdateTicks.infoBox * 1000;
        }

        if (!_($scope.model.lines).isArray()) {
            $scope.model.lines = [{
                text: '0',
                'class': 'count zero-opacity'
            }, {
                text: '0',
                'class': 'count right zero-opacity'
            }, {
                text: logic.first.label,
                'class': 'message'
            }, {
                text: logic.second.label,
                'class': 'message right'
            }];
        }
        var after = 0;
        var firstParams = $.extend({
            after: after
        }, logic.first.params);
        var secondParams = $.extend({
            after: after
        }, logic.second.params);
        $q.all([
            viewerTypeDao.count(logic.first.id, firstParams),
            viewerTypeDao.count(logic.second.id, secondParams)
        ]).then(function (responses) {
            $scope.model.lines = $.extend(true, $scope.model.lines || [], [{
                text: '#' + JSON.stringify({
                    text: responses[0].data,
                    type: 'viewer',
                    id: logic.first.id,
                    qs: firstParams
                })
            }, {
                text: '#' + JSON.stringify({
                    text: responses[1].data,
                    type: 'viewer',
                    id: logic.second.id,
                    qs: secondParams
                })
            }, {}, {}]);

            $timeout(function() {
                _($scope.model.lines).each(function(line) {
                    line.class = line.class.split(' zero-opacity').join('');
                });
            });
        });

        var dataWatch = $scope.$watch(function () {
            return widgetSettings.param(timebackWidgetUuid, 'data');
        }, function (newValue, prevValue) {
            if (newValue != prevValue) {
                reinitLogic();
            }
        });

        return function () {
            dataWatch();
        };
    };
});
