var m = angular.module('dashboard');

m.factory('saasStatusLogic', function($q, modulesDao, $timeout, columnsHelper, slowdownHelper, troubleshooting) {

    function processRealtimeStatuses($scope, logic, realtimeStatusIdx, primaryColumnIdx) {
        if ($scope.model.columns[realtimeStatusIdx].hidden || !logic.realtimeReloadTick || $scope.realtimeReloadTimer) {
            return;
        }

        var notReady = _($scope.model.data).chain().map(function (row) {
            if (_(row[realtimeStatusIdx].originalResolved).isFinite() && row[realtimeStatusIdx].originalResolved < 100) {
                return row[primaryColumnIdx].originalText;
            }
        }).compact().value();

        if (notReady.length) {
            $scope.realtimeReloadTimer = $timeout(function () {
                $scope.realtimeReloadTimer = null;
                if ($scope.realtimeTimeout) {
                    $scope.realtimeTimeout.resolve();
                }
                $scope.realtimeTimeout = $q.defer();
                var localColumns = [$scope.model.columns[primaryColumnIdx], $scope.model.columns[realtimeStatusIdx]];
                var localPrimaryIdx = 0;
                var localRealtimeIdx = 1;
                modulesDao.saasStatus(localColumns, $scope.realtimeTimeout.promise, false, notReady)
                    .then(function (response) {

                        var indexMap = _(response.data.data).chain().map(function (newRow, idx) {
                            return [idx, _($scope.model.data).findIndex(function (row) {
                                return row[primaryColumnIdx].text === newRow[localPrimaryIdx].text;
                            })];
                        }).object().value();

                        var promises = [];
                        _(response.data.data).each(function (row, rowIdx) {
                            promises.push($q.when(row[localRealtimeIdx].text).then(function (value) {
                                $scope.model.data[indexMap[rowIdx]][realtimeStatusIdx].text = value;
                            }));
                            promises.push($q.when(row[localRealtimeIdx].originalText).then(function (value) {
                                $scope.model.data[indexMap[rowIdx]][realtimeStatusIdx].originalResolved = value;
                            }));
                        });

                        $q.all(promises).then(function () {
                            processRealtimeStatuses($scope, logic, realtimeStatusIdx, primaryColumnIdx);
                        });
                    });
            }, slowdownHelper.adjustInterval(logic.realtimeReloadTick));
        }
    }

    return function($scope, logic, reinit) {

        var fixedColumns = _($scope.model.columns).chain().map(function (column) {
            if (!column.debugOnly || troubleshooting.entityDebugEnabled()) {
                return column;
            }
        }).compact().value();
        if ($scope.model.columns.length !== fixedColumns.length) {
            $scope.model.columns = fixedColumns;
        }

        var timeoutDeferred = $q.defer();

        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }

        var realtimeStatusIdx = columnsHelper.getIdxByIdStrict($scope.model.columns, 'realtime_status');

        modulesDao.saasStatus($scope.model.columns, timeoutDeferred.promise, logic.fullReport, $scope.model.scanInProgress).then(function (response) {
            $scope.model.wrapper.isLoading = false;
            var promises = [];
            var realTimePromises = [];
            if (!$scope.model.data) {
                $scope.model.data = [];
            }
            var primaryColumnIdx = columnsHelper.getPrimaryIdx($scope.model.columns);
            var indexMap = _(response.data.data).chain().map(function (newRow, idx) {
                var rowIndex = _($scope.model.data).findIndex(function (row) {
                    return row[primaryColumnIdx].text === newRow[primaryColumnIdx].text;
                });
                if (rowIndex === -1) {
                    $scope.model.data.push(newRow);
                    rowIndex = $scope.model.data.length - 1;
                }
                return [idx, rowIndex];
            }).object().value();

            _(response.data.data).each(function (row, rowIdx) {
                _(row).each(function (col, colIdx) {
                    promises.push($q.when(col.text).then(function (value) {
                        $scope.model.data[indexMap[rowIdx]][colIdx].text = value;
                    }));
                    if (realtimeStatusIdx !== -1 && colIdx === realtimeStatusIdx) {
                        realTimePromises.push($q.when(col.originalText).then(function (value) {
                            $scope.model.data[indexMap[rowIdx]][colIdx].originalResolved = value;
                        }));
                    }
                });
            });

            if (realtimeStatusIdx !== -1 && troubleshooting.entityDebugEnabled()) {
                $q.all(realTimePromises).then(function () {
                    if (!$scope.realTimeInitialized) {
                        if ($scope.model.columns[realtimeStatusIdx].hidden &&
                            _($scope.model.data).find(function (row) {
                                return row[realtimeStatusIdx].text;
                            })) {
                            $scope.model.columns[realtimeStatusIdx].hidden = false;
                            $scope.model.columns = _($scope.model.columns).map(_.identity);
                        }
                        if (!$scope.model.columns[realtimeStatusIdx].hidden) {
                            $scope.$on('$destroy', function () {
                                if ($scope.realtimeReloadTimer) {
                                    $timeout.cancel($scope.realtimeReloadTimer);
                                }
                                if ($scope.realtimeTimeout) {
                                    $scope.realtimeTimeout.resolve();
                                }
                            });
                        }
                    }
                    $scope.realTimeInitialized = true;
                    processRealtimeStatuses($scope, logic, realtimeStatusIdx, primaryColumnIdx);
                });
            }

            if (logic.scanningReloadTimeout) {
                $q.all(promises).then(function () {
                    response.data.info.scanProgressPromise.then(function (scanInProgress) {
                        $scope.model.scanInProgress = scanInProgress;
                        if (scanInProgress.length) {
                            logic.scanningReloadTimeoutHandle = $timeout(reinit, slowdownHelper.adjustInterval(logic.scanningReloadTimeout));
                        }
                    });
                });
            }
        }, function (error) {
            $scope.model.wrapper.isLoading = false;
        });

        var locationListener = $scope.$on('$locationChangeStart', function () {
            timeoutDeferred.resolve();
            if ($scope.realtimeTimeout) {
                $scope.realtimeTimeout.resolve();
            }
        });

        return function () {
            if(logic.scanningReloadTimeoutHandle) {
                $timeout.cancel(logic.scanningReloadTimeoutHandle);
            }
            timeoutDeferred.resolve();
            locationListener();
        };
    };
});