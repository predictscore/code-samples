var m = angular.module('dashboard');

m.factory('eventsListLogic', function(eventsDao, eventsListLogicCore, feature) {
    return function ($scope, logic) {
        if (feature.dashboardUpdateTicks && feature.dashboardUpdateTicks.eventsList) {
            logic.updateTick = feature.dashboardUpdateTicks.eventsList * 1000;
        }

        return eventsListLogicCore($scope, logic, {
            retrieveTypes: function() {
                return eventsDao.retrieveTypes({
                    drive: true,
                    login: true
                });
            },
            retrieve: function(after) {
                return eventsDao.retrieve($scope.model.columns, after);
            },
            timeColumnName: logic.timeColumnName
        });
    };
});