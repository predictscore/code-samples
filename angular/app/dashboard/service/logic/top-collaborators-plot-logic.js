var m = angular.module('dashboard');

m.factory('topCollaboratorsPlotLogic', function(userDao, widgetPolling, verticalPlotLogicCore, widgetSettings) {

    var displayOptions = {
        'recipients': {
            displayColumn: 'recipient_count'
        },
        'emails': {
            displayColumn: 'emails_count'
        }
    };
    var defaultMode = 'recipients';

    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        
        var destroyPlot = _.noop;
        function loadData() {
            var params = widgetSettings.params($scope.model.uuid);
            var mode = displayOptions[params.mode] ? params.mode : defaultMode;
            destroyPlot();
            destroyPlot = verticalPlotLogicCore($scope, logic, {
                retrieve: function() {
                    return userDao.topUsers(params.days).pagination(0, logic.lines).order(displayOptions[mode].displayColumn, 'desc');
                },
                idColumn: logic.idColumn,
                makeLabel: function (row) {
                    return row[logic.nameColumn];
                },
                linkTo: logic.linkTo,
                countColumn: displayOptions[mode].displayColumn
            });
        }

        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, function(params) {
            loadData();
        }, true);

        return function() {
            paramsWatch();
            destroyPlot();
        };
    };
});
