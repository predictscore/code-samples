var m = angular.module('dashboard');

m.factory('policyInfoBoxesOld', function(policyDao, $q, modulesManager, modals, feature) {


    var goodColors = ['#2980b9','#16a085','#89b041','#f39c12','#f35428'];
    var showTypes = [{
        id: 'match',
        label: 'Match',
        expression: 'Match'
    }, {
        id: 'unmatch',
        label: 'Unmatch',
        expression: 'Unmatch',
        qs: {status: 'Unmatch'}
    }, {
     id: 'matchPercent',
     label: '% Match',
     expression: 'Match / total * 100',
        hideHistory: true
     }, {
        id: 'unmatchPercent',
        label: '% Unmatch',
        expression: 'Unmatch / total * 100',
        qs: {status: 'Unmatch'},
        hideHistory: true
     }, {
        id: 'pendingPercent',
        label: '% Pending',
        expression: 'Pending / total * 100',
        qs: {status: 'Pending'},
        hideHistory: true
    }];

    var periods = [{
        id: 0,
        'label': 'Don\'t show history change'
    }, {
        id: '1 hour',
        'label': '1 hour ago'
    }, {
        id: '1 day',
        'label': '1 day ago'
    }, {
        id: '7 day',
        'label': '7 days ago'
    }, {
        id: '1 month',
        'label': '1 month ago'
    }, {
        id: '3 month',
        'label': '3 months ago'
    }, {
        id: '1 year',
        'label': '1 year ago'
    }];

    var historyTypes = _(showTypes).chain().filter(function (type) {
        return !type.hideHistory;
    }).pluck('id').value();

    var policyVariants = [];
    var reinitFn;

    function propertiesDialog (boxIdx, settings) {
        modals.params([{
            lastPage: true,
            okButton: 'Save',
            params: [{
                type: 'string',
                id: 'title',
                label: 'Title',
                placeholder: '[Leave this field blank to use policy name as title]',
                data: settings.configuration.title
            }, {
                type: 'color',
                id: 'color',
                label: 'Color',
                options: {
                    showPalette: true,
                    palette: _(goodColors).map(function (color) {return [color];})
                },
                data: settings.configuration.color
            }, {
                type: 'selector',
                id: 'showType',
                label: 'Show',
                variants: showTypes,
                data: settings.configuration.showType,
                validation: {
                    required: true
                }
            }, {
                type: 'list',
                id: 'policyId',
                label: 'Policy',
                variants: policyVariants,
                data: settings.policy_id,
                settings: {
                    multiple: false
                },
                validation: {
                    required: true
                }
            }, {
                type: 'selector',
                id: 'period',
                label: 'Show change with data',
                variants: periods,
                data: settings.configuration.period,
                enabled: {
                    param: 'showType',
                    'in': historyTypes
                }
            }, {
                type: 'radio-group',
                id: 'uparrow',
                label: 'Change arrows colors',
                enabled: {
                    param: 'showType',
                    'in': historyTypes
                },
                buttons: [{
                    value: 'green',
                    label: '#' + JSON.stringify({
                        text: '<span class="infobox-trend-arrows"><span class="fa fa-long-arrow-up"></span><span class="fa fa-long-arrow-down"></span></span>',
                        display: 'html'
                    })
                }, {
                    value: 'red',
                    label: '#' + JSON.stringify({
                        text: '<span class="infobox-trend-arrows uparrow-red"><span class="fa fa-long-arrow-up"></span><span class="fa fa-long-arrow-down"></span></span>',
                        display: 'html'
                    })
                }],
                paramClass: 'radio-group-inline align-with-label infobox-param-arrows',
                data: settings.configuration.uparrow
            }],
            buttons: [{
                label: 'Reset to default',
                execute: function () {
                    policyDao.infoBoxReset(settings.id, settings.saas).then(function (response) {
                        reinitFn();
                    });
                }
            }]
        }], 'Configure infobox').then(function (data) {
            var newSettings = $.extend(true, {}, settings, {
                policy_id: data.policyId,
                configuration: data
            });
            if (_(historyTypes).indexOf(newSettings.configuration.showType) === -1) {
                delete newSettings.configuration.period;
                delete newSettings.configuration.uparrow;
            }

            policyDao.infoBoxSave(newSettings.id, newSettings.policy_id, newSettings.saas, newSettings.configuration)
                .then(function () {
                    reinitFn();
                });
        });
    }

    var defaultSettings = {
        updateSec: 60,
        showType: 'match',
        period: 0,
        uparrow: 'green'
    };


    return function($scope, logic, reinit) {
        reinitFn = reinit;

        logic.updateTick = ((feature.dashboardUpdateTicks && feature.dashboardUpdateTicks.infoBox) || 60) * 1000;

        var idxToId = logic.boxesIdxToId || _(logic.boxesAmount || 8).chain().range().map(function (idx) {
            return '' + (idx + 1);
        }).value();


        policyDao.infoBoxesFull().then(function (response) {

            policyVariants = _(response.data.policyCatalog).chain().filter(function (row) {
                return row.policy_name;
            }).map(function (policy) {
                return {
                    id: policy.policy_id,
                    label: policy.policy_name
                };
            }).value();

            var infoBoxesConfig = response.data.settings;


            function processSettings(boxIdx, settings) {
                var policy;
                var widgetTitle;
                if (settings && settings.policy_id) {
                    policy = _(policyVariants).find(function (variant) {
                        return variant.id === settings.policy_id;
                    });
                }
                if (!policy) {
                    settings = {
                        configuration: $.extend({}, defaultSettings),
                        id: idxToId[boxIdx],
                        saas: modulesManager.currentModule()
                    };
                    widgetTitle = 'Click to configure';
                } else {
                    _(settings.configuration).defaults(defaultSettings);
                    widgetTitle = settings.configuration.title || policy.label;
                    if (_(showTypes).chain().find(function (type) {
                            return type.id === settings.configuration.showType;
                        }).isUndefined().value()) {
                        settings.configuration.showType = showTypes[0].id;
                    }
                }
                if (!/#[0-9a-fA-F]{6}/.test(settings.configuration.color)) {
                    settings.configuration.color = goodColors[boxIdx % goodColors.length];
                }
                settings.configuration.policyId = settings.policy_id;
                settings.widgetTitle = widgetTitle;
                return settings;
            }


            function createWidget(boxIdx, settings) {
                var widget = {
                    "type": "info",
                    "logic": {
                        "name": "policyInfoBox",
                        "settings": settings.configuration,
                        "properties": {
                            showTypes: showTypes,
                            periods: periods
                        },
                        "updateTick": null
                    },
                    "settings": settings,
                    "size": logic.boxesSize,
                    "class": "avanan-info-box",
                    "wrapper": {
                        "type": "widget-panel",
                        "class": {
                            "avanan": true,
                            "policy-infobox": true,
                            "custom-color": true
                        },
                        "title": settings.widgetTitle,
                        "tooltip": "Click to configure"
                    }
                };
                widget.wrapper.style = {'background-color':settings.configuration.color};
                widget.wrapper.headerOnClick = function () {
                    propertiesDialog(boxIdx, settings);
                };
                return widget;
            }

            $scope.model.widgets = $scope.model.widgets || _(idxToId).map(function () {return {};});

            _($scope.model.widgets).each(function (widget, idx) {
                updateWidget(idx);
            });

            function updateWidget(boxIdx) {
                var staticWidget = logic.staticBoxes && logic.staticBoxes[idxToId[boxIdx]];
                if (staticWidget) {
                    if (!_($scope.model.widgets[boxIdx]).isEmpty()) {
                        return;
                    }
                    $scope.model.widgets[boxIdx] = staticWidget;
                    return;
                }
                var settings = processSettings(boxIdx, (infoBoxesConfig[idxToId[boxIdx]] || {}).options);
                if (!_($scope.model.widgets[boxIdx].settings).isEqual(settings)) {
                    $scope.model.widgets[boxIdx] = createWidget(boxIdx, settings);
                }
                if (infoBoxesConfig[idxToId[boxIdx]]) {
                    $scope.model.widgets[boxIdx].data = {
                        findings: infoBoxesConfig[idxToId[boxIdx]].findings,
                        history: infoBoxesConfig[idxToId[boxIdx]].history
                    };
                }
            }
        });
    };
});