var m = angular.module('dashboard');

m.factory('summaryReportLogic', function(policyDao, modulesManager, $q, modals, workingIndicatorHelper, defaultListConverter, columnsHelper, modulesDao, entityTypes, policiesRefreshHelper) {

    var secTypes = [{
        tag: 'vendors_avs',
        title: 'Malware/AV/Predictive',
        'detected': 'with'
    }, {
        tag: 'vendors_dlp',
        title: 'DLP',
        'detected': 'by'
    }];


    var summaryWidget = {
        "type": "data-table",
        "size": "col-lg-12 col-md-12 col-sm-12 col-xm-12 no-padding summary-report-widget",
        "class": "light vertical-middle",
        "wrapper": {
            "type": "widget-panel",
            "class": "overflow-auto"
        },
        "columns": [{
            id: "id",
            "hidden": true,
            "primary": true
        }, {
            id: "sectionIdx",
            "hidden": true
        }, {
            id: "name",
            "text": false
        }, {
            id: "total",
            "text": false,
            "converter": "summaryReportValueConverter",
            "dataClass": "summary-value"
        }]
    };

    var riskSummaryCombinedWidget = {
        "type": "multi-widget",
        "size": "col-lg-12 col-md-12 col-sm-12 col-xm-12 no-padding summary-report-widget",
        "class": "vertical-middle",
        "wrapper": {
            "type": "widget-panel",
            "class": "overflow-auto"
        }
    };

    var riskSummaryTotalsWidget = {
        "type": "data-table",
        "size": "col-lg-12 col-md-12 col-sm-12 col-xm-12 no-padding summary-report-widget",
        "class": "light vertical-middle",
        "wrapper": {
            "type": "none"
        },
        "columns": [{
            id: "id",
            "hidden": true,
            "primary": true
        }, {
            id: "title",
            "text": false,
            "dataClass": "summary-label"
        }, {
            id: "total",
            "text": false,
            "converter": "summaryReportValueConverter",
            "dataClass": "summary-value"
        }]
    };


    var riskSummaryWidget = {
        "type": "data-table",
        "size": "col-lg-12 col-md-12 col-sm-12 col-xm-12 no-padding summary-report-widget",
        "class": "light medium-icons vertical-middle",
        "wrapper": {
            "type": "none"
        },
        "columns": [{
            id: "id",
            "hidden": true,
            "primary": true
        }, {
            id: "label",
            "text": false,
            "dataClass": "label-column"
        }, {
            id: "total",
            "text": false,
            "converter": "summaryReportValueConverter",
            "dataClass": "summary-value-total-cell summary-value"
        }, {
            id: "incoming",
            "text": false,
            "converter": "summaryReportValueConverter",
            "dataClass": "risk-summary-value"
        }, {
            id: "outgoing",
            "text": false,
            "converter": "summaryReportValueConverter",
            "dataClass": "risk-summary-value"
        }, {
            id: "inside",
            "text": false,
            "converter": "summaryReportValueConverter",
            "dataClass": "risk-summary-value"
        }, {
            "id": "rowType",
            "hidden": true
        }, {
            "id": "fullRow",
            "hidden": true
        }, {
            "id": "rowDataType",
            "hidden": true
        }]
    };

    var sections = {
        "box" : {
            "summary": [{
                "title": false,
                "rows": [{
                    "title": "Outgoing Files",
                    "tag": "files_outgoing"
                }, {
                    "title": "Incoming Files",
                    "tag": "files_incoming"
                }, {
                    "title": "Box Users",
                    "tag": "users_internal"
                }]
            }, {
                "title": "Share Summary",
                "rows": [{
                    "title": "Outgoing Folders",
                    "descr": "Shared with External Users",
                    "tag": "box_folder_owner_internal_shared_externally"
                }, {
                    "title": "Outgoing Folders",
                    "descr": "Anyone with a link",
                    "tag": "box_folder_owner_internal_shared_public_link"
                }, {
                    "title": "Outgoing Files",
                    "descr": "Anyone with a link",
                    "tag": "box_file_owner_internal_shared_public_link"
                }]
            }, {
                "title": "External Users",
                "rows": [{
                    "title": "Number of External Users",
                    "tag": "users_external"
                }]
            }]
        },
        "google_drive" : {
            "summary": [{
                "title": false,
                "rows": [{
                    "title": "Outgoing Files",
                    "tag": "files_outgoing"
                }, {
                    "title": "Incoming Files",
                    "tag": "files_incoming"
                }, {
                    "title": "Google Users",
                    "tag": "users_internal"
                }]
            }, {
                "title": "Share Summary",
                "rows": [{
                    "title": "Outgoing Files",
                    "descr": "Public Shared",
                    "tag": "google_file_owner_internal_shared_public_no_link"
                }, {
                    "title": "Outgoing Files",
                    "descr": "Shared with link",
                    "tag": "google_file_owner_internal_shared_public_link"
                }, {
                    "title": "Outgoing Files",
                    "descr": "Shared with External Users",
                    "tag": "google_file_owner_internal_shared_externally"
                }, {
                    "title": "Incoming Files",
                    "descr": "Shared Public",
                    "tag": "google_file_owner_external_shared_public_no_link"
                }, {
                    "title": "Incoming Files",
                    "descr": "Shared with Link",
                    "tag": "google_file_owner_external_shared_public_link"
                }]
            }, {
                "title": "External Users",
                "rows": [{
                    "title": "Number of External Users",
                    "tag": "users_external"
                }]
            }]
        },
        "office365_onedrive" : {
            "summary": [{
                "title": false,
                "rows": [{
                    "title": "Outgoing Files",
                    "tag": "files_outgoing"
                }, {
                    "title": "Incoming Files",
                    "tag": "files_incoming"
                }, {
                    "title": "Internal Users",
                    "tag": "users_internal"
                }]
            }, {
                "title": "External Users",
                "rows": [{
                    "title": "Number of External Users",
                    "tag": "users_external"
                }]
            }]
        },
        "all": {
            "riskSummary": [{
                "columns": [{
                    "id": "incoming",
                    "colName": "Incoming",
                    "tag": "malicious_incoming"
                    }, {
                        "id": "outgoing",
                        "colName": "Outgoing",
                        "tag": "malicious_outgoing"
                    }, {
                        "id": "inside",
                        "colName": "Internal",
                        "tag": "malicious_inside"
                    }, {
                        "id": "total",
                        "colName": "Total",
                        "type": "sum"
                        //"type": "matrix",
                        //"excludeCats": [5]
                    }
                ],
                "groupBy": [{
                    "type": "category",
                    "namePrefix": "Viruses Detected with "
                }, {
                    "type": "sec_app"
                }]
            }, {
                "columns":[{
                    "id": "incoming",
                    "colName": "Incoming",
                    "tag": "dlp_matches_rules_incoming"
                }, {
                    "id": "outgoing",
                    "colName": "Outgoing",
                    "tag": "dlp_matches_rules_outgoing"
                }, {
                    "id": "inside",
                    "colName": "Internal",
                    "tag": "dlp_matches_rules_inside"
                }, {
                    "id": "total",
                    "colName": "Total",
                    "type": "sum"
                    //"tag": "dlp_matches_rules_all"
                }],
                "groupBy": [{
                    "type": "category",
                    "namePrefix": "Sensitive Info Detected By "
                }, {
                    "type": "sec_app"
                }, {
                    "type": "conditionPropertyEnd",
                    "value": "matches_rules",
                    "totalRow": {
                        "title": "Totals",
                        "type": "policies",
                        "columns": [{
                            "id": "incoming",
                            "tag": "dlp_leak_incoming"
                        }, {
                            "id": "outgoing",
                            "tag": "dlp_leak_outgoing"
                        }, {
                            "id": "inside",
                            "tag": "dlp_leak_inside"
                        }, {
                            "id": "total",
                            "type": "sum"
                            //"type": "matrix",
                            //"includeCats": [5]
                        }]
                    }
                }]
            }]
        },
        "office365_emails": {
            "summary": [{
                "title": false,
                "rows": [{
                    "title": "Incoming Emails",
                    "tag": "office365_emails_email_incoming"
                }, {
                    "title": "Outgoing Emails",
                    "tag": "office365_emails_email_outgoing"
                }, {
                    "title": "Incoming Attachments",
                    "tag": "attachment_incoming"
                }, {
                    "title": "Outgoing Attachments",
                    "tag": "attachment_outgoing"
                }, {
                    "title": "Internal Users",
                    "tag": "users_internal"
                }]
            }, {
                "title": "External Users",
                "rows": [{
                    "title": "Number of External Users",
                    "tag": "users_external"
                }]
            }],
            "riskSummary": [{
                "columns": [{
                    "id": "incoming",
                    "colName": "Incoming",
                    "tag": "malicious_incoming"
                }, {
                    "id": "outgoing",
                    "colName": "Outgoing",
                    "tag": "malicious_outgoing"
                }, {
                    "id": "inside",
                    "colName": "Internal",
                    "tag": "malicious_inside"
                }, {
                    "id": "total",
                    "colName": "Total",
                    "type": "sum"
                    //"type": "matrix",
                    //"excludeCats": [5]
                }
                ],
                "groupBy": [{
                    "type": "category",
                    "namePrefix": "Viruses Detected with "
                }, {
                    "type": "sec_app"
                }]
            }, {
                "columns":[{
                    "id": "incoming",
                    "colName": "Incoming",
                    "tag": "dlp_matches_rules_incoming"
                }, {
                    "id": "outgoing",
                    "colName": "Outgoing",
                    "tag": "dlp_matches_rules_outgoing"
                }, {
                    "id": "inside",
                    "colName": "Internal",
                    "tag": "dlp_matches_rules_inside"
                }, {
                    "id": "total",
                    "colName": "Total",
                    "type": "sum"
                    //"tag": "dlp_matches_rules_all"
                }],
                "filters": [{
                    "type": "entityType",
                    "value": "office365_emails_email"
                }],
                "groupBy": [{
                    "type": "category",
                    "namePrefix": "Sensitive Info Detected By ",
                    "namePostfix": " in Email Bodies"
                }, {
                    "type": "sec_app"
                }, {
                    "type": "conditionPropertyEnd",
                    "value": "matches_rules",
                    "totalRow": {
                        "title": "Totals",
                        "type": "policies",
                        "columns": [{
                            "id": "incoming",
                            "tag": "dlp_leak_incoming"
                        }, {
                            "id": "outgoing",
                            "tag": "dlp_leak_outgoing"
                        }, {
                            "id": "inside",
                            "tag": "dlp_leak_inside"
                        }, {
                            "id": "total",
                            "type": "sum"
                            //"type": "matrix",
                            //"includeCats": [5]
                        }]
                    }
                }]
            }, {
                "columns":[{
                    "id": "incoming",
                    "colName": "Incoming",
                    "tag": "dlp_matches_rules_incoming"
                }, {
                    "id": "outgoing",
                    "colName": "Outgoing",
                    "tag": "dlp_matches_rules_outgoing"
                }, {
                    "id": "inside",
                    "colName": "Internal",
                    "tag": "dlp_matches_rules_inside"
                }, {
                    "id": "total",
                    "colName": "Total",
                    "type": "sum"
                    //"tag": "dlp_matches_rules_all"
                }],
                "filters": [{
                    "type": "entityType",
                    "value": "office365_emails_attachment"
                }],
                "groupBy": [{
                    "type": "category",
                    "namePrefix": "Sensitive Info Detected By ",
                    "namePostfix": " in Attachments"
                }, {
                    "type": "sec_app"
                }, {
                    "type": "conditionPropertyEnd",
                    "value": "matches_rules",
                    "totalRow": {
                        "title": "Totals",
                        "type": "policies",
                        "columns": [{
                            "id": "incoming",
                            "tag": "dlp_leak_incoming"
                        }, {
                            "id": "outgoing",
                            "tag": "dlp_leak_outgoing"
                        }, {
                            "id": "inside",
                            "tag": "dlp_leak_inside"
                        }, {
                            "id": "total",
                            "type": "sum"
                            //"type": "matrix",
                            //"includeCats": [5]
                        }]
                    }
                }]
            }]
        },
        "google_mail": {
            "summary": [{
                "title": false,
                "rows": [{
                    "title": "Incoming Emails",
                    "tag": "google_mail_email_incoming"
                }, {
                    "title": "Outgoing Emails",
                    "tag": "google_mail_email_outgoing"
                }, {
                    "title": "Incoming Attachments",
                    "tag": "google_mail_attachment_incoming"
                }, {
                    "title": "Outgoing Attachments",
                    "tag": "google_mail_attachment_outgoing"
                }]
            }],
            "riskSummary": [{
                "columns": [{
                    "id": "incoming",
                    "colName": "Incoming",
                    "tag": "malicious_incoming"
                }, {
                    "id": "outgoing",
                    "colName": "Outgoing",
                    "tag": "malicious_outgoing"
                }, {
                    "id": "inside",
                    "colName": "Internal",
                    "tag": "malicious_inside"
                }, {
                    "id": "total",
                    "colName": "Total",
                    "type": "sum"
                    //"type": "matrix",
                    //"excludeCats": [5]
                }
                ],
                "groupBy": [{
                    "type": "category",
                    "namePrefix": "Viruses Detected with "
                }, {
                    "type": "sec_app"
                }]
            }, {
                "columns":[{
                    "id": "incoming",
                    "colName": "Incoming",
                    "tag": "dlp_matches_rules_incoming"
                }, {
                    "id": "outgoing",
                    "colName": "Outgoing",
                    "tag": "dlp_matches_rules_outgoing"
                }, {
                    "id": "inside",
                    "colName": "Internal",
                    "tag": "dlp_matches_rules_inside"
                }, {
                    "id": "total",
                    "colName": "Total",
                    "type": "sum"
                    //"tag": "dlp_matches_rules_all"
                }],
                "filters": [{
                    "type": "entityType",
                    "value": "google_mail_email"
                }],
                "groupBy": [{
                    "type": "category",
                    "namePrefix": "Sensitive Info Detected By ",
                    "namePostfix": " in Email Bodies"
                }, {
                    "type": "sec_app"
                }, {
                    "type": "conditionPropertyEnd",
                    "value": "matches_rules",
                    "totalRow": {
                        "title": "Totals",
                        "type": "policies",
                        "columns": [{
                            "id": "incoming",
                            "tag": "dlp_leak_incoming"
                        }, {
                            "id": "outgoing",
                            "tag": "dlp_leak_outgoing"
                        }, {
                            "id": "inside",
                            "tag": "dlp_leak_inside"
                        }, {
                            "id": "total",
                            "type": "sum"
                            //"type": "matrix",
                            //"includeCats": [5]
                        }]
                    }
                }]
            }, {
                "columns":[{
                    "id": "incoming",
                    "colName": "Incoming",
                    "tag": "dlp_matches_rules_incoming"
                }, {
                    "id": "outgoing",
                    "colName": "Outgoing",
                    "tag": "dlp_matches_rules_outgoing"
                }, {
                    "id": "inside",
                    "colName": "Internal",
                    "tag": "dlp_matches_rules_inside"
                }, {
                    "id": "total",
                    "colName": "Total",
                    "type": "sum"
                    //"tag": "dlp_matches_rules_all"
                }],
                "filters": [{
                    "type": "entityType",
                    "value": "google_mail_attachment"
                }],
                "groupBy": [{
                    "type": "category",
                    "namePrefix": "Sensitive Info Detected By ",
                    "namePostfix": " in Attachments"
                }, {
                    "type": "sec_app"
                }, {
                    "type": "conditionPropertyEnd",
                    "value": "matches_rules",
                    "totalRow": {
                        "title": "Totals",
                        "type": "policies",
                        "columns": [{
                            "id": "incoming",
                            "tag": "dlp_leak_incoming"
                        }, {
                            "id": "outgoing",
                            "tag": "dlp_leak_outgoing"
                        }, {
                            "id": "inside",
                            "tag": "dlp_leak_inside"
                        }, {
                            "id": "total",
                            "type": "sum"
                            //"type": "matrix",
                            //"includeCats": [5]
                        }]
                    }
                }]
            }]
        }
    };

    function summaryReportValueConverter (text, columnId, columnsMap, args) {
        if (text && !_(text.value).isUndefined()) {
            if (text.policies && text.policies.length === 1) {
                policiesRefreshHelper.addPolicies(text.policies[0].id);
                return '#' + JSON.stringify({
                        display: 'link',
                        type: 'policy-edit',
                        id: text.policies[0].id,
                        text: text.value
                    });
            }
            return text.value;
        }
        return text;
    }

    var addedValueWidgetConfig = {
        "logic": {
            "name": "appsAddedValueLogic",
            "updateTick": null
        },
        "type": "multi-widget",
        "size": "col-lg-12 col-md-12 col-sm-12 col-xm-12 no-padding summary-report-widget security-coverage-widget",
        "wrapper": {
            "type": "widget-panel",
            "class": "overflow-auto",
            "title": "Security Stack Global Coverage Analysis"
        }
    };


    return function($scope, logic, reinit) {
        if (!$scope.policyCatalog) {
            workingIndicatorHelper.init($scope);
        }

        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }


        var queries = [
            policyDao.infoboxCatalogWithFindings(),
            modulesDao.getModules(),
            policyDao.modulesMatrix(true)
        ];

        $scope.addedValueWidget = $scope.addedValueWidget || angular.copy(addedValueWidgetConfig);
        
        $q.all(queries).then(function (responses) {
            workingIndicatorHelper.hide();
            $scope.model.wrapper.isLoading = false;

            $scope.modulesList = responses[1].data;
            $scope.modulesMatrix = responses[2].data;
            $scope.appByLabel = {};
            _($scope.modulesList.apps_items.modules).each(function (app) {
                $scope.appByLabel[app.label] = app;
                app.category = _($scope.modulesList.apps_categories.categories).find(function (category) {
                    return category.categoryId === app.category_id;
                });
            });

            $scope.policyCatalog = _(responses[0].data.rows).filter(function (policy) {
                // purpose of this filter is not clear now. may be it was added to filter out 'Smart Search'
                // right now it also filter's out multi-engine findings widget.
                // Multi-engine findings widget requires fixes now to not be shown if customer doesn't have multiple engines
                return policy.sec_apps.length < 1 || $scope.appByLabel[policy.sec_apps[0]];
            });
            $scope.policiesByTag = {};
            _($scope.policyCatalog).each(function (policy) {
                $scope.policiesByTag[policy.context.saas] = $scope.policiesByTag[policy.context.saas] || {};
                _(policy.tags).each(function (tag) {
                    $scope.policiesByTag[policy.context.saas][tag] = $scope.policiesByTag[policy.context.saas][tag] || [];
                    $scope.policiesByTag[policy.context.saas][tag].push(policy);
                });
            });

            $scope.appByLabel = {};
            _($scope.modulesList.apps_items.modules).each(function (app) {
                $scope.appByLabel[app.label] = app;
                app.category = _($scope.modulesList.apps_categories.categories).find(function (category) {
                    return category.categoryId === app.category_id;
                });
            });
            var saasList = modulesManager.activeSaasApps();

            $scope.saasWidgets =  $scope.saasWidgets || {};
            var widgets = [];
            
            _(saasList).each(function (saasApp) {
                var saas = saasApp.name;
                $scope.policiesByTag[saas] = $scope.policiesByTag[saas] || {};
                $scope.saasWidgets[saas] = $scope.saasWidgets[saas] || {};
                if (!$scope.saasWidgets[saas].summary) {
                    $scope.saasWidgets[saas].summary = angular.copy(summaryWidget);
                    $scope.saasWidgets[saas].summary.data = [];
                    $scope.saasWidgets[saas].summary.wrapper.title = saasApp.title + ' Summary';
                }

                var summaryRows = [];
                if (sections[saas] && sections[saas].summary) {
                    _(sections[saas].summary).each(function (section, sectionIdx) {
                        _(section.rows).each(function (row, rowIdx) {
                            var tag = row.tag;
                            if ($scope.policiesByTag[saas][tag]) {
                                var total = _($scope.policiesByTag[saas][tag]).reduce(function (memo, policy) {
                                    var policyInfo = {
                                        id: policy.policy_id,
                                        name: policy.policy_name,
                                        value: (policy.findingsStatus && policy.findingsStatus.Match || 0)
                                    };
                                    memo.value += policyInfo.value;
                                    memo.policies.push(policyInfo);
                                    return memo;
                                }, {
                                    value: 0,
                                    policies: []
                                });
                                if (total.value) {
                                    summaryRows.push({
                                        id: rowIdx,
                                        sectionIdx: sectionIdx,
                                        name: '#' + JSON.stringify({
                                            display: 'text',
                                            'class': 'summary-cell-title',
                                            text: row.title
                                        }) + '#' + JSON.stringify({
                                            display: 'text',
                                            'class': 'summary-cell-descr',
                                            text: row.descr
                                        }),
                                        total: total
                                    });
                                }
                            }
                        });
                    });
                }
                $scope.saasWidgets[saas].summary.rawData = summaryRows;
                if (summaryRows.length) {
                    defaultListConverter({
                        data: {
                            rows: summaryRows
                        }
                    }, {
                        columns: $scope.saasWidgets[saas].summary.columns,
                        converters: {
                            summaryReportValueConverter: summaryReportValueConverter
                        }
                    }).then(function (response) {
                        var sectionIdxIdx = columnsHelper.getIdxById($scope.saasWidgets[saas].summary.columns, 'sectionIdx');
                        var nameIdx = columnsHelper.getIdxById($scope.saasWidgets[saas].summary.columns, 'name');
                        var total = 0;
                        var rows = [];
                        var previousSection = -1;
                        var rowIdx = 0;
                        var rowClasses = {};
                        _(response.data.data).each(function (row) {
                            if (row[sectionIdxIdx].originalText !== previousSection) {
                                if (previousSection !== -1) {
                                    rows.push(
                                        _(nameIdx).chain().range().map(function () {
                                            return {
                                                hidden: true
                                            };
                                        }).value().concat(
                                        [{
                                        text: ' ',
                                        colSpan: 2
                                    }]).concat(_($scope.saasWidgets[saas].summary.columns.length - nameIdx - 1).chain().range().map(function () {
                                        return {
                                            hidden: true
                                        };
                                    }).value()));
                                    rowClasses[rowIdx] = {
                                        'empty-row': true
                                    };
                                    total++;
                                    rowIdx++;
                                }
                                if (sections[saas].summary[row[sectionIdxIdx].originalText].title) {
                                    rows.push(
                                        _(nameIdx).chain().range().map(function () {
                                            return {
                                                hidden: true
                                            };
                                        }).value().concat(
                                            [{
                                                text: sections[saas].summary[row[sectionIdxIdx].originalText].title,
                                                colSpan: 2
                                            }]).concat(_($scope.saasWidgets[saas].summary.columns.length - nameIdx - 1).chain().range().map(function () {
                                            return {
                                                hidden: true
                                            };
                                        }).value()));
                                    rowClasses[rowIdx] = {
                                        'summary-section-title': true
                                    };
                                    total++;
                                    rowIdx++;
                                }

                            }
                            rows.push(row);
                            total++;
                            rowIdx++;
                            previousSection = row[sectionIdxIdx].originalText;
                        });
                        $scope.saasWidgets[saas].summary.data = rows;
                        $scope.saasWidgets[saas].summary.rowClasses = rowClasses;
                    });
                }


                if (!$scope.saasWidgets[saas].riskSummaryCombined) {
                    $scope.saasWidgets[saas].riskSummaryCombined = angular.copy(riskSummaryCombinedWidget);
                    $scope.saasWidgets[saas].riskSummaryCombined.wrapper.title = saasApp.title + ' Risk Summary';
                }

                if (!$scope.saasWidgets[saas].riskSummaryTotals) {
                    $scope.saasWidgets[saas].riskSummaryTotals = angular.copy(riskSummaryTotalsWidget);
                    $scope.saasWidgets[saas].riskSummaryTotals.data = [];
                }


                var totalsRows = [];
                var emailEntity = entityTypes.toBackend(saas, 'email');
                var fileEntity = entityTypes.toBackend(saas, 'file');
                var saasEntities = [];
                if (fileEntity) {
                    saasEntities.push({
                        saasEntity: fileEntity,
                        title: emailEntity ? 'Attachments' : 'Files'
                    });
                }
                if (emailEntity) {
                    saasEntities.push({
                        saasEntity: emailEntity,
                        title: 'Emails'
                    });
                }

                _(saasEntities).each(function (entityInfo) {
                    _(secTypes).each(function (secTypeInfo) {
                        var tag = secTypeInfo.tag;
                        var filters = [{
                            type: 'entityType',
                            value: entityInfo.saasEntity
                        }];
                        var arr = [];
                        _($scope.policiesByTag[saas][tag]).each(function (policy) {
                            if (!isFiltersMatch(filters, policy)) {
                                return;
                            }
                            if (!policy.findingsStatus || !policy.findingsStatus.Match) {
                                return;
                            }
                            var foundCondition = findConditionByPropertyEnd([policy.policy_data.includeCondition], 'detections_numbers');
                            if (!foundCondition || foundCondition.operator !== 'greater_than') {
                                return;
                            }
                            var detections = foundCondition.data + 1;
                            arr.push({
                                title: policy.policy_name,
                                total: {
                                    value: policy.findingsStatus.Match,
                                    policies: [{
                                        id: policy.policy_id
                                    }]
                                },
                                detections: detections
                            });
                        });
                        if (arr.length) {
                            _(arr).chain().sortBy('detections').each(function (value) {
                                totalsRows.push(value);
                            });
                        }
                    });
                });


                $scope.saasWidgets[saas].riskSummaryTotals.rawData = totalsRows;
                if (totalsRows.length) {
                    totalsRows.unshift({
                        title: 'Multi-Engine Security Findings'
                    });
                    totalsRows.push({
                        "title": " "
                    });
                    defaultListConverter({
                        data: {
                            rows: totalsRows
                        }
                    }, {
                        columns: $scope.saasWidgets[saas].riskSummaryTotals.columns,
                        converters: {
                            summaryReportValueConverter: summaryReportValueConverter
                        }
                    }).then(function (response) {
                        $scope.saasWidgets[saas].riskSummaryTotals.data = response.data.data;
                        $scope.saasWidgets[saas].riskSummaryTotals.rowClasses = {
                            0: {
                                'summary-section-title': true
                            }
                        };
                        $scope.saasWidgets[saas].riskSummaryTotals.rowClasses[$scope.saasWidgets[saas].riskSummaryTotals.data.length - 1] = {
                            'risk-summary-row-emptyRow': true
                        };
                    });
                } else {
                    $scope.saasWidgets[saas].riskSummaryTotals.data = [];
                }


                if (!$scope.saasWidgets[saas].riskSummary) {
                    $scope.saasWidgets[saas].riskSummary = angular.copy(riskSummaryWidget);
                    $scope.saasWidgets[saas].riskSummary.data = [];
                }

                var widgetSections = (sections[saas] && sections[saas].riskSummary) || (sections.all && sections.all.riskSummary);
                var widgetData = [];
                if (widgetSections) {
                    _(widgetSections).each(function (section) {
                        var result = {};
                        calculateGroup(saas, result, section.columns, section.groupBy, section.filters);

                        var data = groupedObjectToArray(result);
                        data = filterArrayData(data);
                        sortArrayData(data);

                        arrayToTableRows(widgetData, data, 1, section);

                    });

                }
                $scope.saasWidgets[saas].riskSummary.rawData = widgetData;
                if (widgetData.length) {
                    defaultListConverter({
                        data: {
                            rows: widgetData
                        }
                    }, {
                        columns: $scope.saasWidgets[saas].riskSummary.columns,
                        converters: {
                            summaryReportValueConverter: summaryReportValueConverter
                        }
                    }).then(function (response) {
                        var columns = $scope.saasWidgets[saas].riskSummary.columns;
                        var colIdxCache = {};
                        function colIdx(columnId) {
                            if (_(colIdxCache[columnId]).isUndefined()) {
                                colIdxCache[columnId] = columnsHelper.getIdxById(columns, columnId);
                            }
                            return colIdxCache[columnId];
                        }

                        var rowTypeIdx = columnsHelper.getIdxById(columns, 'rowType');
                        var rows = response.data.data;
                        var rowClasses = {};
                        _(rows).each(function (row, idx) {
                            if (row[colIdx('fullRow')].text) {
                                var spanCount = 1;
                                var fullRowIdx = colIdx(row[colIdx('fullRow')].text);
                                _(row).chain().rest(fullRowIdx + 1).each(function (col, idx) {
                                    if (!columns[idx + fullRowIdx + 1].hidden) {
                                        spanCount++;
                                        col.hidden = true;
                                    }
                                });
                                row[fullRowIdx].colSpan = spanCount;
                            }
                            if (row[rowTypeIdx].originalText !== 'data') {
                                rowClasses[idx] = {};
                                rowClasses[idx]['risk-summary-row-' + row[rowTypeIdx].originalText] = true;
                                if (row[colIdx('rowDataType')].originalText) {
                                    rowClasses[idx]['risk-summary-row-data-type-' + row[colIdx('rowDataType')].originalText] = true;
                                }
                            }
                        });
                        $scope.saasWidgets[saas].riskSummary.data = rows;
                        $scope.saasWidgets[saas].riskSummary.rowClasses = rowClasses;
                    });
                } else {
                    $scope.saasWidgets[saas].riskSummary.data = [];
                }
            });

            function arrayToTableRows(dest, src, level, info) {
                var maxLevel = info.groupBy.length;
                var columns = info.columns;
                var totalRow = (level < maxLevel) && info.groupBy[level].totalRow;
                _(src).each(function (row, rowIdx) {
                    if (dest.length && level < maxLevel && (rowIdx || level === 1)) {
                        dest.push({
                            "label": " ",
                            "rowType": "emptyRow"
                        });
                    }
                    var dataRow = {
                        label: row.id,
                        "rowType": "value",
                        "rowDataType": row.type
                    };
                    if (level < maxLevel) {
                        dataRow.rowType = 'category-level-' + level;
                    }

                    if (row.rows && row.rows.length) {
                        if (level === 1) {
                            dataRow.fullRow = 'label';
                        }
                        if (level === maxLevel - 1) {
                            if (level === 1) {
                                dest.push(dataRow);
                                dataRow = {
                                    label: null,
                                    "rowType": "columnHeaders"
                                };
                            } else {
                                dataRow.rowType = "columnHeaders";
                            }
                            $.extend(dataRow, _(columns).chain().map(function (column) {
                                return [column.id, column.colName];
                            }).object().value());
                        }
                        dest.push(dataRow);

                        arrayToTableRows(dest, row.rows, level + 1, info);
                        if (totalRow && row.data) {
                            dataRow = {
                                label: totalRow.title,
                                rowType: 'totals'
                            };
                            $.extend(dataRow, row.data);
                            dest.push(dataRow);
                        }
                    } else {
                        if (row.data) {
                            $.extend(dataRow, row.data);
                            dest.push(dataRow);
                        }
                    }
                });
            }

            function sortArrayData(src) {
                if (src.rows) {
                    sortArrayData(src.rows);
                }
                src = _(src).sortBy(function (el) {
                    if (el.type === 'category') {
                        return parseInt(el.category.securityStackOrder);
                    }
                    if (el.type === 'sec_app') {
                        return parseInt(el.app.security_stack_order);
                    }
                    return el.id;
                });
            }

            function filterArrayData(src) {
                return _(src).filter(function (row) {
                    if (row.rows) {
                        row.rows = filterArrayData(row.rows);
                    }
                    return (row.rows && row.rows.length) || (row.type === 'sec_app') || (row.data && _(row.data).find(function (value) {
                            return value && value.value;
                        }));
                });
            }

            function groupedObjectToArray(obj) {
                return _(obj).map(function (row, key) {
                    if (row.rows) {
                        row.rows = groupedObjectToArray(row.rows);
                    }
                    row.id = key;
                    return row;
                });
            }

            function findConditionByPropertyEnd(conditions, value) {
                var found = false;
                _(conditions).find(function (condition) {
                    if (condition.property && (condition.property.lastIndexOf(value) === (condition.property.length - value.length))) {
                        found = condition;
                        return true;
                    }
                    if (condition.conditions) {
                        found = findConditionByPropertyEnd(condition.conditions, value);
                        return !!found;
                    }
                });
                return found;
            }

            function getGroups(policy, groupBy) {
                var groups = [];
                var policyWrong = _(groupBy).find(function (condition) {
                    if (condition.type === "category") {
                        if (policy.sec_apps.length !== 1) {
                            return true;
                        }
                        groups.push({
                            type: condition.type,
                            value: condition.namePrefix + $scope.appByLabel[policy.sec_apps[0]].category.categoryName + (condition.namePostfix || ''),
                            category: $scope.appByLabel[policy.sec_apps[0]].category
                        });
                        return false;
                    }
                    if (condition.type === "sec_app") {
                        if (policy.sec_apps.length !== 1) {
                            return true;
                        }
                        groups.push({
                            type: condition.type,
                            value: policy.sec_apps[0]
                        });
                        return false;
                    }
                    if (condition.type === "conditionPropertyEnd") {
                        var foundCondition = findConditionByPropertyEnd([policy.policy_data.includeCondition], condition.value);
                        if (!foundCondition) {
                            return true;
                        }
                        groups.push({
                            type: condition.type,
                            value: foundCondition.data
                        });
                        return false;
                    }
                });
                if (policyWrong) {
                    return false;
                }
                return groups;
            }

            function addGroupedData(obj, groups, field, value) {
                var group = groups[0].value;
                if (!obj[group]) {
                    obj[group] = {
                        type: groups[0].type
                    };
                    if (groups[0].type === 'sec_app') {
                        obj[group].app = $scope.appByLabel[group];
                    }
                    if (groups[0].type === 'category') {
                        obj[group].category = groups[0].category;
                    }
                }
                if (groups.length > 1) {
                    if (!obj[group].rows) {
                        obj[group].rows = {};
                    }
                    addGroupedData(obj[group].rows, _(groups).rest(), field, value);
                } else {
                    if (!obj[group].data) {
                        obj[group].data = {};
                    }
                    if (!obj[group].data[field]) {
                        obj[group].data[field] = {
                            value: 0
                        };
                    }
                    obj[group].data[field].value += value.value;
                    if (value.policy) {
                        obj[group].data[field].policies = obj[group].data[field].policies || [];
                        obj[group].data[field].policies.push(value.policy);
                    }
                }
            }

            function updateWithMatrixData(saas, result, columnId) {
                _(result).each(function (res, key) {
                    if (res.type !== 'sec_app' && res.rows) {
                        updateWithMatrixData(saas, res.rows, columnId);
                        return;
                    }
                    if ($scope.appByLabel[key] && $scope.modulesMatrix[saas][$scope.appByLabel[key].name]) {
                        _($scope.modulesMatrix[saas][$scope.appByLabel[key].name]).each(function (policy, policyId) {
                            var value = {
                                value: policy.match_count || 0
                            };
                            value.policy = {
                                id: policyId,
                                name: policy.name,
                                Match: value.value
                            };
                            addGroupedData(result, [{
                                type: 'sec_app',
                                value: key
                            }], columnId, value);
                        });
                    }
                });
            }

            function totalSumByLevel(result, columnId, level) {
                _(result).each(function (row) {
                    if (level !== 1) {
                        totalSumByLevel(row.rows, columnId, level - 1);
                        return;
                    }
                    if (row.data) {
                        row.data[columnId] = _(row.data).reduce(function (memo, value) {
                            memo.value += value.value;
                            return memo;
                        }, {
                            value: 0
                        });
                    }
                });
            }

            function isFiltersMatch(filters, policy) {
                if (!filters) {
                    return true;
                }
                var notMatched = _(filters).find(function (filter) {
                    if (filter.type === 'entityType') {
                        return policy.entity_type !== filter.value;
                    }
                    return false;
                });
                return !notMatched;
            }

            function calculateGroup(saas, result, columns, groupBy, filters) {
                _(columns).each(function (column) {
                    if (column.tag) {
                        _($scope.policiesByTag[saas][column.tag]).each(function (policy) {
                            if (!isFiltersMatch(filters, policy)) {
                                return;
                            }
                            var groups = getGroups(policy, groupBy);
                            if (groups) {
                                var value = {
                                    value: policy.findingsStatus && policy.findingsStatus.Match || 0
                                };
                                value.policy = {
                                    id: policy.policy_id,
                                    name: policy.policy_name,
                                    Match: value.value
                                };
                                addGroupedData(result, groups, column.id, value);
                            }
                        });
                    }
                });

                var last = _(groupBy).last();
                if (last.type === 'sec_app') {
                    _(columns).each(function (column) {
                        if (column.type === 'matrix') {
                            updateWithMatrixData(saas, result, column.id);
                        }
                    });
                }
                _(columns).each(function (column) {
                    if (column.type === 'sum') {
                        totalSumByLevel(result, column.id, groupBy.length);
                    }
                });

                if (groupBy.length > 1) {
                    if (last.totalRow && last.totalRow.type === 'policies' && last.totalRow.columns) {
                        calculateGroup(saas, result, last.totalRow.columns, _(groupBy).initial(), filters);
                    }
                }
            }



            _(saasList).each(function (saasApp) {
                var saas = saasApp.name;
                if ($scope.saasWidgets[saas].summary.rawData && $scope.saasWidgets[saas].summary.rawData.length) {
                    widgets.push($scope.saasWidgets[saas].summary);
                }
                $scope.saasWidgets[saas].riskSummaryCombined.widgets = [];
                if ($scope.saasWidgets[saas].riskSummaryTotals.rawData && $scope.saasWidgets[saas].riskSummaryTotals.rawData.length) {
                    $scope.saasWidgets[saas].riskSummaryCombined.widgets.push($scope.saasWidgets[saas].riskSummaryTotals);
                }
                if ($scope.saasWidgets[saas].riskSummary.rawData && $scope.saasWidgets[saas].riskSummary.rawData.length) {
                    $scope.saasWidgets[saas].riskSummaryCombined.widgets.push($scope.saasWidgets[saas].riskSummary);
                }
                if ($scope.saasWidgets[saas].riskSummaryCombined.widgets.length) {
                    widgets.push($scope.saasWidgets[saas].riskSummaryCombined);
                }

            });
            widgets.push($scope.addedValueWidget);


            $scope.model.widgets = widgets;

        }, function (error) {
            $scope.model.wrapper.isLoading = false;
        });


    };
});