var m = angular.module('dashboard');

m.factory('policiesPlotLogic', function(widgetSettings, policyDao, modulesManager) {
    return function($scope, logic) {
        $scope.discoveryData = $scope.discoveryData || [];

        function filterData() {
            var params = widgetSettings.params($scope.model.uuid);
            var filtered = _($scope.discoveryData).filter(function(series) {
                return _(params[series.id]).isUndefined() ? (series.policy_hidden ? false : true) : params[series.id];
            });
            return _(filtered).map(function(series, idx) {
                return {
                    label: series.label,
                    data: series.data
                };
            });
        }

        policyDao.discovery(logic.days, modulesManager.currentModule()).then(function(response) {
            $scope.discoveryData = response.data;
            $scope.model.data = filterData();
            var params = widgetSettings.params($scope.model.uuid);
            $scope.model.wrapper.filters = [];
            var newParams = {};
            _($scope.discoveryData).each(function (policy) {
                newParams[policy.id] = _(params[policy.id]).isUndefined() ? (policy.policy_hidden ? false : true) : params[policy.id];
                $scope.model.wrapper.filters.push({
                    "type": "flag",
                    "state": newParams[policy.id],
                    "text": policy.label,
                    "value": policy.id,
                    "local": true
                });
            });
            widgetSettings.params($scope.model.uuid, newParams);
        });

        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, function(params) {
            $scope.model.data = filterData();
        }, true);

        return function() {
            paramsWatch();
        };
    };
});