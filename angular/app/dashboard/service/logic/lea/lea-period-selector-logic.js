var m = angular.module('dashboard');

m.factory('leaPeriodSelectorLogic', function (widgetPolling, widgetSettings, leaPeriods) {
    return function ($scope, logic, reinitLogic) {

        if (!$scope.model.lines) {
            var params = widgetSettings.params($scope.model.uuid);
            if (!leaPeriods.periodsMap[params.periodId] && leaPeriods.periodsMap[leaPeriods.defaultPeriodId]) {
                params.periodId = leaPeriods.defaultPeriodId;
                widgetSettings.params($scope.model.uuid, params);
            }
            rebuildItems();
        }

        function rebuildItems(updateWidgets) {
            var params = widgetSettings.params($scope.model.uuid);
            $scope.model.lines = [{
                display: 'linked-text',
                class: 'lea-period-selector-container',
                text: _(leaPeriods.periods).map(function (period) {
                    var result = {
                        display: 'text',
                        class: {
                            'btn': true,
                            'btn-default': true,
                            'btn-sm': true,
                            'active': params.periodId === period.id
                        },
                        clickEvent: {
                            name: 'leaDashboardPeriodChange',
                            periodId: period.id
                        },
                        text: period.label
                    };
                    return '#' + JSON.stringify(result);
                }).join('')
            }];
            widgetPolling.scope().period = $.extend({}, leaPeriods.periodsMap[params.periodId], {
                update: updateWidgets
            });
        }

        var periodChangedStopWatch = $scope.$on('leaDashboardPeriodChange', function (event, data) {
            var params = widgetSettings.params($scope.model.uuid);
            params.periodId = data.periodId;
            widgetSettings.params($scope.model.uuid, params);
            rebuildItems(true);
        });

        return function () {
            periodChangedStopWatch();
        };
    };
});
