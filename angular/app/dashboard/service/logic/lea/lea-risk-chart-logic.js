var m = angular.module('dashboard');

m.factory('leaRiskChartLogic', function(leaDao, widgetSettings, routeHelper, $timeout) {
    return function($scope, logic) {
        var risks = {
            1: {
                title: 'Lowest',
                color: '#89b041'
            },
            2: {
                title: 'Low',
                color: '#41731F'
            },
            3: {
                title: 'Medium',
                color: '#f39c12'
            },
            4: {
                title: 'High',
                color: '#FF2828'
            },
            5: { 
                title: 'Highest',
                color: '#A00000'
            }
        };

        function loadData() {
            var params = widgetSettings.params($scope.model.uuid);
            leaDao.appsByRisk(params.days).then(function(response) {
                var rows = response.data.rows;
                $scope.model.flotOptions.xaxis.tickFormatter = function(text) {
                    return risks[text].title;
                };
                $scope.model.flotOptions.xaxis.ticks = [1, 2, 3, 4, 5];
                $scope.model.data = _(rows).map(function(row) {
                    return {
                        label: row.risk_level,
                        data: [[row.risk_level, row.count]],
                        color: risks[row.risk_level].color,
                        link: routeHelper.getPath('viewer', {id: 'applications'}, {risk_level: row.risk_level, after: params.days})
                    };
                });
                _($scope.model.flotOptions.xaxis.ticks).each(function(tick) {
                    var found = _($scope.model.data).find(function(d) {
                        return d.label == tick;
                    });
                    if(!found) {
                        $scope.model.data.push({
                            label: tick,
                            data: [[tick, 0]],
                            color: risks[tick].color
                        });
                    }
                });
            });
        }
        
        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, function(params) {
            loadData();
        }, true);
        
        return function() {
            paramsWatch();
        };
    };
});