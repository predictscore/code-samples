var m = angular.module('dashboard');

m.factory('leaSaasCategoriesLogic', function(leaDao, widgetSettings, widgetPolling, routeHelper) {
    return function ($scope, logic) {

        function loadData () {
            var params = widgetSettings.params($scope.model.uuid);
            if (logic.viewerId) {
                $scope.model.flotOptions.legend.labelFormatter = function (label, series) {
                    return '<a href="/#!' + series.link + '">' + label + '</a>';
                };
            }
            return leaDao.saasCategories(params.days, widgetPolling.scope().leaAppProperties.useModule, logic.query).then(function(response) {
                var rows = response.data.rows;
                $scope.model.data = _(rows).map(function(row) {
                    var item = {
                        label: row.category + ': ' + row.count,
                        data: row.count
                    };
                    if (logic.viewerId) {
                        item.link = routeHelper.getPath('viewer', {
                            id: logic.viewerId,
                            module: widgetPolling.scope().leaAppProperties.useModule
                        }, {
                            after: params.days,
                            category: row.category
                        });
                    }
                    return item;
                });
            });
        }

        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, function(params) {
            loadData();
        }, true);

        return function() {
            paramsWatch();
        };
    };
});
