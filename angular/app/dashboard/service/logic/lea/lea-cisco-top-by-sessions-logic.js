var m = angular.module('dashboard');

m.factory('leaCiscoTopBySessionsLogic', function(widgetSettings, verticalPlotLogicCore, leaDao, routeHelper) {

    var colors = {
        'encrypted': '#2980b9',
        'open': '#f39c12'
    };

    var flotOptions = {
        "series": {
            "stack": true,
            "bars": {
                "show": true,
                "horizontal": true,
                "align": "center",
                "barWidth": 0.7,
                numbers: {
                    show: false,
                    font: '14px open-sans',
                    fontColor: '#444',
                    threshold: 0,
                    xAlign: _.identity,
                    yAlign: _.identity,
                    xOffset: 5
                }
            },
            "color": '#2980b9'
        },
        "grid":{
            "hoverable": true,
            "clickable": true,
            "borderWidth": 1,
            "tickColor": "#eee",
            "borderColor": "#eee"
        },
        "legend": {
            "show": true,
            "position": "ne",
            "noColumns": 2
        },
        "yaxis": {
            "labelWidth": 100,
            "axisLabelUseCanvas": true,
            "tickSize": 1
        },
        "xaxis": {
            "minTickSize": 1
        },
        "shadowSize": 0,
        "tooltip": true,
        "tooltipOpts": {
            "defaultTheme": false,
            "content": "%s",
            "shifts": {
                "x": 0,
                "y": 20
            }
        }
    };
    return function($scope, logic, reinit) {

        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }

        if (!$scope.model.flotOptions) {
            $scope.model.flotOptions = angular.copy(flotOptions);

            $scope.model.flotOptions.legend.labelFormatter = function(label, series) {
                if (series.data[0][1] !== 0) {
                    return null;
                }
                return series.legend;
            };
            $scope.model.flotOptions.yaxis.labelWidth = logic.labelWidth || 200;

            $scope.model.flotOptions.xaxis.tickFormatter = function(text) {
                return parseInt(text);
            };

        }
        logic.lines = 30;

        var destroyPlot = _.noop;
        function loadData() {
            var params = widgetSettings.params($scope.model.uuid);
            leaDao.ciscoTopDomainsBySessions(params.days).pagination(0, logic.lines).order('total', 'desc')
                .then(function (response) {
                    $scope.model.wrapper.isLoading = false;

                    var rows = _(response.data.rows).sortBy('total');

                    var idToIdx = {};

                    $scope.model.flotOptions.yaxis.ticks = _(rows).map(function(row, idx) {
                        idToIdx[row.domain] = idx;
                        return [idx, row.domain + ' (' + row.av_category + ')'];
                    });

                    var chartData = [];
                    _(rows).each(function (row) {
                        var link = routeHelper.getPath('viewer', {
                            id: 'cisco-logger-saas-access-log'
                        }, {
                            saas: row.domain,
                            after: params.days
                        });
                        chartData.push({
                            label: row.domain + ' encrypted sessions: ' + row.encrypted,
                            data: [
                                [row.encrypted, idToIdx[row.domain]]
                            ],
                            color: colors.encrypted,
                            link: link,
                            legend: 'Encrypted sessions'
                        });
                        chartData.push({
                            label: row.domain + ' not encrypted sessions: ' + row.open,
                            data: [
                                [row.open, idToIdx[row.domain]]
                            ],
                            color: colors.open,
                            link: link,
                            legend: 'Not encrypted sessions'
                        });

                    });

                    $scope.model.data = chartData;
                    $scope.model.style.height = (rows.length * 30 + 20) + 'px';



                });
        }

        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, function(params) {
            loadData();
        }, true);

        return function() {
            paramsWatch();
        };
    };
});