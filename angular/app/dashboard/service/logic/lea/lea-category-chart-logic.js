var m = angular.module('dashboard');

m.factory('leaCategoryChartLogic', function(leaDao, widgetSettings, routeHelper, $timeout) {
    return function($scope, logic) {

        function loadData() {
            var params = widgetSettings.params($scope.model.uuid);
            leaDao.appsByCategory(params.days).then(function(response) {
                var rows = response.data.rows;
                $scope.model.flotOptions.xaxis.tickFormatter = function(text) {
                    return text;
                };
                var categoryToIdx = {};
                $scope.model.flotOptions.yaxis.ticks = _(response.data.rows).map(function(row, idx) {
                    categoryToIdx[row.category] = idx;
                    return [idx, row.category];
                });
                $scope.model.data = _(rows).map(function(row) {
                    return {
                        label: row.category,
                        data: [
                            [row.count, categoryToIdx[row.category]]
                        ],
                        link: routeHelper.getPath('viewer', {id: 'applications'}, {category: row.category, after: params.days})
                    };
                });
            });
        }

        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, function(params) {
            loadData();
        }, true);

        return function() {
            paramsWatch();
        };
    };
});