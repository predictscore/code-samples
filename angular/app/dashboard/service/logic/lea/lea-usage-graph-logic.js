var m = angular.module('dashboard');

/*jshint loopfunc: true */
m.factory('leaUsageGraphLogic', function(leaDao, widgetSettings, $q, widgetPolling, base64, leaPeriods) {
    var timeback = {
        week: {
            groupFormat: 'YYYY-MM-DD',
            after: 7
        },
        month: {
            groupFormat: 'YYYY-MM-DD',
            after: 30
        },
        year: {
            groupFormat: 'YYYY-MM',
            after: 365
        }
    };
    var totalLabel = 'Total';
    return function($scope, logic) {
        var alwaysShow;
        var appNameColumn = logic.appNameColumn || 'appi_name';
        if (logic.profileMode) {
            $scope.model.uuid += base64.encode(widgetPolling.scope().id);
            alwaysShow = widgetPolling.scope().id;
        }

        function rebuildButtons() {
            var params = widgetSettings.params($scope.model.uuid);
            var selected = params._id;
            if (!leaPeriods.periodsMap[params._id]) {
                selected = leaPeriods.defaultPeriodId;
            }
            if($scope.lastSelected == selected) {
                return;
            }
            $scope.lastSelected = selected;
            $scope.model.wrapper.buttons = [{
                position: 'left',
                'class': 'btn-icon',
                iconClass: 'fa fa-calendar gray-text',
                options: _(leaPeriods.periods).map(function(period) {
                    return {
                        label: '#' + JSON.stringify({
                            display: 'text',
                            'class': {
                                'fa': true,
                                'fa-circle-o': selected != period.id,
                                'fa-check-circle-o': selected == period.id
                            }
                        }) + ' ' + period.label,
                        execute: function() {
                            var params = widgetSettings.params($scope.model.uuid);
                            params._id = period.id;
                            widgetSettings.params($scope.model.uuid, params);
                            rebuildButtons();
                        }
                    };
                })
            }];
        }
        
        rebuildButtons();
        
        var lastResponse = null;
        var requestCancel = $q.defer();
        function loadData(force) {
            var params = widgetSettings.params($scope.model.uuid);
            var timebackId = params._id;
            if (!leaPeriods.periodsMap[params._id]) {
                timebackId = leaPeriods.defaultPeriodId;
            }
            var args = leaPeriods.periodsMap[timebackId];
            var promise = $q.when(lastResponse);
            if(force || _(lastResponse).isNull()) {
                requestCancel.resolve();
                requestCancel = $q.defer();
                $scope.model.data = undefined;
                promise = leaDao.appUsage(args.days, args.groupFormat, logic.query, widgetPolling.scope().leaAppProperties && widgetPolling.scope().leaAppProperties.useModule).timeout(requestCancel.promise);
            }
            promise.then(function(response) {
                lastResponse = response;
                var rows = response.data.rows;
                
                var now = moment().startOf(args.periodStart || args.groupPeriod);

                var first = moment().subtract(args.amount, args.type);
                
                var series = {};
                series[totalLabel] = {};
                var base;
                _(rows).each(function(row) {
                    if (row.type === 'base') {
                        if (!base) {
                            base = {};
                        }
                        base[moment(row.date).unix()] = row.count;
                    }
                    if (_(row[appNameColumn]).isNull()) {
                        return;
                    }
                    series[row[appNameColumn]] = series[row[appNameColumn]] || {};
                    series[row[appNameColumn]][moment(row.date).unix()] = row.count;
                    series[totalLabel][moment(row.date).unix()] = row.count + (series[totalLabel][moment(row.date).unix()] || 0);
                });
                if (!rows.length && logic.baseGaps) {
                    base = {};
                }

                delete $scope.model.flotOptions.xaxis.max;
                if(args.groupPeriod !== 'day') {
                    $scope.model.flotOptions.xaxis.max = now.unix();
                }
                
                while(now.isAfter(first)) {
                    now.subtract(1, args.groupPeriod);
                    var ts = now.unix();
                    _(series).each(function(s) {
                        s[ts] = s[ts] || 0;
                    });
                    if (_($scope.model.flotOptions.xaxis.max).isUndefined()) {
                        $scope.model.flotOptions.xaxis.max = now.unix();
                    }
                }
                $scope.model.flotOptions.xaxis.min = now.unix();

                $scope.model.flotOptions.xaxis.tickFormatter = function(text) {
                    return moment.unix(text).format('YYYY-MM-DD');
                };

                $scope.model.data = _(series).chain().map(function(data, label) {
                    return {
                        label: label,
                        data: _(data).chain().map(function(count, time) {
                            return [time, count];
                        }).sortBy(function(d) {
                            return d[0];
                        }).map(function (d) {
                            if (base && logic.baseGaps && d[1] === 0 && !base[d[0]]) {
                                return null;
                            }
                            return d;
                        }).value(),
                        points: {
                            radius: 1,
                            show: true
                        }
                    };
                }).filter(function(data) {
                    return params[data.label] || data.label === alwaysShow;
                }).sortBy('label').sortBy(function (value) {
                    if (value.label === totalLabel) {
                        return 0;
                    }
                    if (value.label === alwaysShow) {
                        return 1;
                    }
                    return 2;
                }).value();

                $scope.model.wrapper.filters = _(series).chain().map(function(data, label) {
                    return {
                        type: 'flag',
                        text: label,
                        local: true,
                        value: label,
                        state: !logic.profileMode
                    };
                }).filter(function (data) {
                    return data.value !== alwaysShow && data.value !== totalLabel;
                }).sortBy("text").value();
                
                $scope.model.wrapper.filters.splice(0, 0, {
                    type: 'select-all',
                    local: true
                }, {
                    type: 'flag',
                    text: totalLabel,
                    local: true,
                    value: totalLabel,
                    state: !logic.profileMode,
                    alwaysShow: true
                }, {
                    type: 'filter',
                    local: true
                });
            });
        }
        
        var lastId = null;
        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, function(params) {
            loadData(lastId != params._id);
            lastId = params._id;
        }, true);

        return function() {
            paramsWatch();
        };  
    };
});