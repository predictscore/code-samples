var m = angular.module('dashboard');

m.factory('leaInfoboxesLogic', function (path, modulesManager) {
    return function ($scope, logic, reinitLogic) {

        $scope.model.widgets = _($scope.model.widgets).filter(function (widget) {
            return widget.logic.app && modulesManager.isActive(widget.logic.app);
        });

        return _.noop;
    };
});
