var m = angular.module('dashboard');

m.factory('leaTopAppsLogic', function(leaDao, simpleSortableLogicCore, widgetSettings, widgetPolling) {
    return function($scope, logic, reinit) {

        var simpleLogic = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                var params = widgetSettings.params($scope.model.uuid);
                var queryParams = {
                    after: params.days
                };
                if (logic.profileMode) {
                    queryParams.id = widgetPolling.scope().id;
                }
                return leaDao.topApps($scope.model.columns, queryParams).pagination(0, params.amount || 10);
            }
        });

        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, function(newParams, oldParams) {
            if (_(newParams).isEqual(oldParams)) {
                return;
            }
            $scope.tableHelper.reload();
        }, true);
        return function() {
            simpleLogic();
            paramsWatch();
        };
    };
});