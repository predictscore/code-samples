var m = angular.module('dashboard');

m.factory('leaDashboardLogic', function (widgetPolling, widgetSettings, path) {
    return function ($scope, logic, reinitLogic) {

        var activeDashboardStopWatch = $scope.$watch(function () {
            return widgetPolling.scope().leaApp;
        }, function () {
            rebuildWidgets();
        });

        var periodUpdatePending = false;

        function rebuildWidgets() {
            var leaApp = widgetPolling.scope().leaApp;
            if ($scope.model.leaApp !== leaApp) {
                $scope.model.widgets = [];
                $scope.model.leaApp = leaApp;
            }
            if (!leaApp) {
                return;
            }
            periodUpdatePending = false;
            path('dashboard').ctrl('dashboard').json('shadow-it/' + leaApp + '.widgets').retrieve()
                .then(function (response) {
                    if (periodUpdatePending) {
                        setWidgetsPeriod(response.data.widgets, periodUpdatePending);
                        periodUpdatePending = false;
                    }
                    _(response.data.widgets).each(function (widget) {
                        if (_(widget.logic.updateTick).isUndefined()) {
                            widget.logic.updateTick = 1000 * 60 * 5;
                        }
                    });
                    $scope.model.widgets = response.data.widgets || [];
                });
        }

        function setWidgetsPeriod(widgets, period) {
            var updated = false;
            _(widgets).each(function (widget) {
                if (widget.uuid && widget.wrapper && widget.wrapper.filters) {
                    var param;
                    if (widget.logic.periodParam) {
                        param = widget.logic.periodParam;
                    }
                    if (!param) {
                        var daysFilter = _(widget.wrapper.filters).find(function (filter) {
                            return filter.value === 'days';
                        });
                        if (daysFilter) {
                            param = {
                                type: "days",
                                name: "days"
                            };
                        }
                    }
                    if (!param) {
                        return;
                    }
                    var value;
                    if (param.type === "id") {
                        value = period.id;
                    } else if (param.type === "days") {
                        value = period.days;
                    }

                    var settings = widgetSettings.params(widget.uuid);
                    if (_(settings[param.name]).isUndefined() || (period.update && settings[param.name] !== value)) {
                        updated = true;
                        settings[param.name] = value;
                        widgetSettings.params(widget.uuid, settings);
                    }
                }
            });
            return updated;
        }

        var periodStopWatch = $scope.$watch(function () {
            return widgetPolling.scope().period;
        }, function (period) {
            var updated = setWidgetsPeriod($scope.model.widgets, period);
            periodUpdatePending = period;
            _(widgetPolling.scope().activeApps).each(function (app) {
                var appName = app.app;
                if (appName === widgetPolling.scope().leaApp) {
                    return;
                }
                path('dashboard').ctrl('dashboard').json('shadow-it/' + appName + '.widgets').retrieve()
                    .then(function (response) {
                        setWidgetsPeriod(response.data.widgets, period);
                    });
            });
            if (updated) {
                rebuildWidgets();
            }
        });

        return function () {
            activeDashboardStopWatch();
            periodStopWatch();
        };
    };
});
