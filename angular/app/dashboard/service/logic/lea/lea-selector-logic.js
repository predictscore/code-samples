var m = angular.module('dashboard');

m.factory('leaSelectorLogic', function (path, modulesManager, widgetPolling, widgetSettings) {
    return function ($scope, logic, reinitLogic) {

        var availableApps = [{
            img: path('security-stack').img('applications/lea_module.jpg'),
            'label': 'Checkpoint',
            app: 'shadow_it_cp',
            "useModule": modulesManager.currentModule()
        }, {
            img: path('security-stack').img('applications/paloalto.png'),
            'label': 'Palo Alto',
            app: 'shadow_it_pan',
            "useModule": modulesManager.currentModule()
        }, {
            img: path('security-stack').img('applications/gmail.jpg'),
            'label': 'Google Emails',
            app: 'shadow_it_gmail',
            "useModule": "google_mail"
        }, {
            img: path('security-stack').img('applications/office365_emails.jpg'),
            'label': 'Office365 emails',
            app: 'shadow_it_office365_emails',
            "useModule": 'office365_emails'
        }, {
            img: path('security-stack').img('applications/cicso_logger_parser.png'),
            'label': 'Cisco Logger Parser',
            app: 'cisco_logger_parser',
            "useModule": modulesManager.currentModule()
        }, {
            'class': {
                'fa': true,
                'fa-sitemap': true,
                'lea-icon': true,
                'dns-logger-icon': true
            },
            'label': 'DNS Logger Parser',
            app: 'dns_logger_parser',
            "useModule": modulesManager.currentModule()
        }];

        var activeApps = _(availableApps).filter(function (icon) {
            return modulesManager.isActive(icon.app);
        });
        widgetPolling.scope().activeApps = activeApps;

        function setActiveApp(appName) {
            widgetPolling.scope().leaApp = appName;
            widgetPolling.scope().leaAppProperties = _(availableApps).find(function (app) {
                return app.app === appName;
            });
            widgetSettings.param($scope.model.uuid, 'leaApp', appName);
        }

        function appValid(appName) {
            return !!_(activeApps).find(function (app) {
                return app.app === appName;
            });
        }

        if (activeApps.length) {
            if (!widgetPolling.scope().leaApp || !appValid(widgetPolling.scope().leaApp)) {
                var setApp = widgetSettings.param($scope.model.uuid, 'leaApp');
                if (!setApp || !appValid(setApp)) {
                    setApp = activeApps[0].app;
                }
                setActiveApp(setApp);
            }
        }

        function rebuildIcons() {
            $scope.model.lines = [{
                display: 'linked-text',
                text: _(activeApps).map(function (icon) {
                    var result = {
                        tooltip: icon.label,
                        'class': {},
                        clickEvent: {
                            name: 'leaDashboardChange',
                            app: icon.app
                        }
                    };
                    if (icon.img) {
                        result.display ='img';
                        result.path = icon.img;
                    } else {
                        result.display = 'text';
                        result.text = icon.text || '';
                    }
                    if (icon.class) {
                        if (_(icon.class).isString()) {
                            result['class'][icon.class] = true;
                        } else {
                            result['class'] = angular.copy(icon.class);
                        }
                    }
                    if (icon.app === widgetPolling.scope().leaApp) {
                        result['class'].active = true;
                    }
                    return '#' + JSON.stringify(result);
                }).join('')
            }];
        }

        if (!_($scope.model.lines).isArray()) {
            rebuildIcons();
        }
        var dashboardChangeStopListen = $scope.$on('leaDashboardChange', function (event, data) {
            setActiveApp(data.app);
            rebuildIcons();
        });

        return function () {
            dashboardChangeStopListen();
        };
    };
});
