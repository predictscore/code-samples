var m = angular.module('dashboard');

m.factory('leaSaasLatestAccessLogic', function(leaDao, simpleSortableLogicCore, widgetSettings, widgetPolling) {
    return function($scope, logic, reinit) {

        var simpleLogic = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return leaDao.saasLatestAccess($scope.model.columns, widgetPolling.scope().leaAppProperties.useModule, logic.query);
            }
        });

        return function() {
            simpleLogic();
        };
    };
});