var m = angular.module('dashboard');

m.factory('staticInfoBoxLogic', function (viewerTypeDao, widgetSettings, $timeout, feature) {
    return function ($scope, logic, reinitLogic) {
        if (!_($scope.model.lines).isArray()) {
            $scope.model.lines = [{
                text: logic.staticNumber,
                'class': 'count'
            }, {
                text: logic.title,
                'class': 'message'
            }];
        }
        return _.noop;
    };
});
