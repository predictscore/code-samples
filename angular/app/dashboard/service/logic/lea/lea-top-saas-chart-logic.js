var m = angular.module('dashboard');

m.factory('leaTopSaasChartLogic', function(widgetSettings, verticalPlotLogicCore, leaDao, widgetPolling) {

    var flotOptions = {
        "series": {
            "stack": false,
            "bars": {
                "show": true,
                "horizontal": true,
                "align": "center",
                "barWidth": 0.7,
                numbers: {
                    show: true,
                    font: '14px open-sans',
                    fontColor: '#444',
                    threshold: 0.25,
                    xAlign: _.identity,
                    yAlign: _.identity,
                    xOffset: 5
                }
            },
            "color": '#2980b9'
        },
        "grid":{
            "hoverable": true,
            "clickable": true,
            "borderWidth": 1,
            "tickColor": "#eee",
            "borderColor": "#eee"
        },
        "legend": {
            "show": false,
            "position": "ne"
        },
        "yaxis": {
            "labelWidth": 100,
            "axisLabelUseCanvas": true,
            "tickSize": 1
        },
        "xaxis": {
            "minTickSize": 1
        },
        "shadowSize": 0,
        "tooltip": true,
        "tooltipOpts": {
            "defaultTheme": false,
            "content": "%s",
            "shifts": {
                "x": 0,
                "y": 20
            }
        }
    };
    return function($scope, logic, reinit) {
        if (!$scope.model.flotOptions) {
            $scope.model.flotOptions = angular.copy(flotOptions);

            $scope.model.flotOptions.legend.labelFormatter = function(label, series) {
                if (series.data[0][1] !== 0) {
                    return null;
                }
                return label;
            };
            $scope.model.flotOptions.yaxis.labelWidth = logic.labelWidth || 200;
        }
        logic.lines = 30;

        var destroyPlot = _.noop;
        function loadData() {
            var countColumn = logic.countColumn || 'count';
            var params = widgetSettings.params($scope.model.uuid);
            destroyPlot();
            destroyPlot = verticalPlotLogicCore($scope, logic, {
                retrieve: function() {
                    return leaDao.topDomains(params.days, widgetPolling.scope().leaAppProperties.useModule, logic.query)
                        .pagination(0, logic.lines).order(countColumn, 'desc');
                },
                idColumn: 'domain',
                countColumn: countColumn,
                makeLabel: function (row) {
                    return row.domain + ' (' + row.av_category + ')';
                },
                linkTo: 'viewer',
                linkParams: {
                    id: {
                        value: logic.link || 'shadow-it-saas-users'
                    },
                    module: {
                        value: widgetPolling.scope().leaAppProperties.useModule
                    }
                },
                linkQs: {
                    saas: {
                        column: 'domain'
                    },
                    after: {
                        value: params.days
                    }
                }
            });
        }

        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, function(params) {
            loadData();
        }, true);
        var appWatch = $scope.$watch(function () {
            return widgetPolling.scope().leaApp;
        }, function (newVal, oldVal) {
            if (oldVal && newVal != oldVal) {
                loadData();
            }
        });

        return function() {
            paramsWatch();
            appWatch();
        };
    };
});