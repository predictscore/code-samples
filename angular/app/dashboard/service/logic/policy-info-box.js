var m = angular.module('dashboard');

m.factory('policyInfoBox', function (policyDao, $timeout, feature, $parse, routeHelper, $filter, policiesRefreshHelper) {
    return function ($scope, logic) {

        var dataWatch = _.noop;
        var leftMessageText = '', rightMessageText = '';

        if ((logic.settings.mode && logic.settings.mode !== 'policy') || logic.settings.policyId) {
            if (_(logic.settings.mode).isUndefined() && logic.settings.policyId) {
                logic.settings.mode = 'policy';
            }
            var showType = _(logic.properties.showTypes).find(function (type) {
                return type.id === logic.settings.showType;
            });
            var period = _(logic.properties.periods).find(function (period) {
                return period.id === logic.settings.period;
            });

            var showType2 = _(logic.properties.showTypes).find(function (type) {
                return type.id === logic.settings.showType2;
            });

            leftMessageText = logic.settings.valueTitle || '';

            dataWatch = $scope.$watch('model.data', function () {
                if ($scope.model.data.policies.length === 0) {
                    if (logic.settings.rightMode !== 'value' || $scope.model.data.rightData.policies.length === 0) {
                        $scope.model.lines = [{
                            "class": "empty-infobox-area",
                            text: '#' + JSON.stringify({
                                display: 'text',
                                text: 'No queries match the current configurations.'
                            }) + '#' + JSON.stringify({
                                display: 'text',
                                text: 'Click here to configure.',
                                'class': 'link-like',
                                clickEvent: {
                                    name: 'configure-infobox',
                                    infoboxId: $scope.model.settings.id
                                }
                            })
                        }];
                        return;
                    }
                }

                var mainNumbers = calculateNumbers($scope.model.data, showType.expression);
                var rightCount = {display: 'text', text: ''};
                rightMessageText = '';
                var showHistoryOnTooltip = false;
                if (logic.settings.rightMode === 'history' && logic.settings.period) {
                    if (_(mainNumbers.change).isUndefined()) {
                        rightMessageText = 'History not available';
                    } else {
                        var historyArrow = '';
                        if (mainNumbers.change !== 0) {
                            if (mainNumbers.change < 0) {
                                historyArrow = '<span class="fa fa-long-arrow-down"></span>';
                            } else {
                                historyArrow = '<span class="fa fa-long-arrow-up"></span>';
                            }
                        }

                        rightCount = {
                            text: historyArrow + mainNumbers.change + '%',
                            display: 'html'
                        };
                        rightMessageText = 'Change since ' + period.label;
                        showHistoryOnTooltip = true;
                    }
                }
                if (logic.settings.rightMode === 'value') {
                    var rightNumbers = calculateNumbers($scope.model.data.rightData, showType2.expression);
                    rightCount = formatCount(rightNumbers, logic.settings.mode2, $scope.model.data.rightData.policies, logic.settings.policyId2, showType2, false);
                    rightMessageText = logic.settings.valueTitle2;
                }

                var mainCount = formatCount(mainNumbers, logic.settings.mode, $scope.model.data.policies, logic.settings.policyId, showType, showHistoryOnTooltip);
                
                var leftCover = {
                    display: 'div',
                    'class': 'left-cover',
                    clickEvent: mainCount.clickEvent
                };
                
                var rightCover = {
                    display: 'div',
                    'class': 'right-cover',
                    clickEvent: rightCount.clickEvent || mainCount.clickEvent
                };

                $scope.model.dataLines = $.extend(true, $scope.model.dataLines || [], [{
                    text: '#' + JSON.stringify({
                        display: 'text',
                        text: leftMessageText,
                        'class': 'message-left'
                    }) + '#' + JSON.stringify({
                        display: 'text',
                        text: rightMessageText,
                        'class': 'message-right'
                    })
                }, {
                    text: '#' + JSON.stringify(leftCover)
                }, {
                    text: '#' + JSON.stringify(rightCover)
                }, {
                    text: '#' + JSON.stringify(mainCount)
                }, {
                    text: '#' + JSON.stringify(rightCount)
                }]);

                $timeout(function() {
                    _($scope.model.dataLines).chain().last(2).each(function(line) {
                        line.class = line.class.split(' zero-opacity').join('');
                    });
                });
                $scope.model.lines = $scope.model.dataLines;

            });
        }

        if (!_($scope.model.dataLines).isArray()) {
            $scope.model.dataLines = [{
                'class': 'double-messages-line',
                text: ''
            }, {
                'class': 'zero-opacity',
                text: ''
            }, {
                'class': 'zero-opacity',
                text: ''
            }, {
                text: '0',
                'class': 'count zero-opacity'
            }, {
                text: '',
                'class': 'count right zero-opacity infobox-trend-arrows uparrow-' + logic.settings.uparrow
            }];
        }
        $scope.model.lines = $scope.model.dataLines;

        function calculateNumbers(obj, expression) {
            var res = {
                value: Math.round($parse(expression)(obj.findings))
            };
            if (obj.history && !_(obj.history.total).isUndefined()) {
                var historyValue = Math.round($parse(expression)(obj.history));
                res.change = 0;
                if (res.value == historyValue) {
                    return res;
                }
                if (historyValue === 0) {
                    res.change = '>999';
                    return res;
                }
                res.change = Math.round((res.value - historyValue) / historyValue * 100);
                if (res.change > 999) {
                    res.change = '>999';
                    return res;
                }
                if (res.change > 0) {
                    res.change = '+' + res.change;
                }
            }
            return res;
        }

        function toolTip(policies, expression, showHistory, qs) {
            var list = [];
            policiesRefreshHelper.addPolicies(_(policies).map(function (policy) {
                return policy.policy_id;
            }));
            _(policies).each(function (policy) {
                var info = calculateNumbers(policy, expression);
                info.policy_id = policy.policy_id;
                info.policy_name = policy.policy_name;
                list.push(info);
            });
            if (list.length < 1) {
                return;
            }
            return {
                content: {
                    text: '<div class="infobox-tooltip"><table>' +
                    _(list).map(function (info, idx) {
                        var url = '/#!' + routeHelper.getPath('policy-edit', {id: info.policy_id}, qs);
                        return '<tr>' +
                            '<td><a href="' + url +'">' + info.policy_name +'</a></td>' +
                            '<td>' + $filter('number')(info.value) + '</td>' +
                            (showHistory ? '<td>' +
                            (_(info.change).isUndefined() ? ' ': (info.change + '%')) +
                            '</td>' : '') +
                            '</tr>';
                    }).join('') +
                    '</table></div>'
                },
                position: {
                    at: 'bottom center',
                    my: 'top center',
                    viewport: true
                },
                hide: {
                    delay: 200,
                    fixed: true
                },
                style: {
                    classes: 'qtip-bootstrap qtip-infobox'
                }
            };
        }

        function formatCount(numbers, mode, policies, policyId, showType, showHistory, eventName) {
            var mainCount;
            if (mode !== 'policy') {
                var mainCountDisplay = {
                    text: numbers.value,
                    display: 'odometer',
                    odometer: {
                        duration: 10000,
                        format: '(,ddd)'
                    },
                    tooltip: toolTip(policies, showType.expression, showHistory, showType.qs)
                    
                };
                if (policies && policies.length === 1) {
                    mainCountDisplay.clickEvent = {
                        name: 'open-policy',
                        id: policies[0].policy_id,
                        qs: showType.qs,
                        type: 'policy-edit'
                    };
                } 
                mainCount = mainCountDisplay;
            } else {
                mainCount = {
                    text: numbers.value,
                    odometer: {
                        duration: 10000,
                        format: '(,ddd)'
                    },
                    clickEvent: {
                        name: 'open-policy',
                        id: policyId,
                        qs: showType.qs
                    }
                };
            }
            return mainCount;
        }
        
        var destroyOpenPolicy = $scope.$on('open-policy', function(event, data) {
            routeHelper.redirectTo('policy-edit', {
                id: data.id
            }, {
                qs: data.qs
            });
        });


        return function () {
            dataWatch();
            destroyOpenPolicy();
        };
    };
});
