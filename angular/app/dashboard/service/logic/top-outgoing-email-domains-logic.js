var m = angular.module('dashboard');

m.factory('topOutgoingEmailDomainsLogic', function(emailDao, widgetSettings, verticalPlotLogicCore, $q) {
    return function ($scope, logic) {


        var destroyPlot = _.noop;
        function loadData() {
            var params = widgetSettings.params($scope.model.uuid);
            var countColumn = params.mode + '_count';
            destroyPlot();
            destroyPlot = verticalPlotLogicCore($scope, logic, {
                retrieve: function() {
                    if (!_(params).isEqual($scope.model.dataParams)) {
                        $scope.model.wrapper.isLoading = true;
                    }
                    $scope.model.dataParams = angular.copy(params);
                    return emailDao.topOutgoingDomains(params.days, params.internal)
                        .pagination(0, logic.lines + 1)
                        .order(countColumn, 'desc')
                        .converter(function (input) {
                            input.data.rows = _(input.data.rows).filter(function (row) {
                                return row.domain !== 'total';
                            });
                            return $q.when(input);
                        });
                },
                idColumn: "domain",
                makeLabel: function (row) {
                    return row.domain;
                },
                countColumn: countColumn
            });
        }

        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, function(params) {
            loadData();
        }, true);

        return function() {
            paramsWatch();
        };
    };
});
