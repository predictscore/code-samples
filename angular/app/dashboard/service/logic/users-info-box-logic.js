var m = angular.module('dashboard');

m.factory('userInfoBoxLogic', function (viewerTypeDao, widgetSettings, $timeout, $q, feature) {
    var timebackWidgetUuid = "dashboard-info-boxes-timeback";
    return function ($scope, logic, reinitLogic) {
        if (feature.dashboardUpdateTicks && feature.dashboardUpdateTicks.infoBox) {
            logic.updateTick = feature.dashboardUpdateTicks.infoBox * 1000;
        }

        if (!_($scope.model.lines).isArray()) {
            $scope.model.lines = [{
                text: '0',
                'class': 'count zero-opacity'
            }, {
                text: '0/0',
                'class': 'count right zero-opacity'
            }, {
                text: logic.external.label,
                'class': 'message'
            }, {
                text: logic.internal.label,
                'class': 'message right'
            }];
        }
        var after = 0;
        var externalParams = $.extend({
            after: after
        }, logic.external.params);
        var activeParams = $.extend({
            after: after
        }, logic.internal.active.params);
        var totalParams = $.extend({
            after: after
        }, logic.internal.total.params);
        $q.all([
            viewerTypeDao.count(logic.external.id, externalParams),
            viewerTypeDao.count(logic.internal.active.id, activeParams),
            viewerTypeDao.count(logic.internal.total.id, totalParams)
        ]).then(function (responses) {
            $scope.model.lines = $.extend(true, $scope.model.lines || [], [{
                text: '#' + JSON.stringify({
                    text: responses[0].data,
                    type: 'viewer',
                    id: logic.external.id,
                    qs: externalParams
                })
            }, {
                text: '#' + JSON.stringify({
                    text: responses[1].data,
                    type: 'viewer',
                    id: logic.internal.active.id,
                    tooltip: logic.internal.active.tooltip,
                    qs: activeParams
                }) + '/#' + JSON.stringify({
                    text: responses[2].data,
                    type: 'viewer',
                    id: logic.internal.total.id,
                    tooltip: logic.internal.total.tooltip,
                    qs: totalParams
                })
            }, {}, {}]);

            $timeout(function() {
                _($scope.model.lines).each(function(line) {
                    line.class = line.class.split(' zero-opacity').join('');
                });
            });
        });

        var dataWatch = $scope.$watch(function () {
            return widgetSettings.param(timebackWidgetUuid, 'data');
        }, function (newValue, prevValue) {
            if (newValue != prevValue) {
                reinitLogic();
            }
        });

        return function () {
            dataWatch();
        };
    };
});
