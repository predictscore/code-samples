var m = angular.module('dashboard');

m.factory('appsAddedValueLogic', function(fileDao) {

    var tableWidgetConfig = {
        "type": "data-table",
        "size": "col-lg-12 col-md-12 col-sm-12 col-xm-12 no-padding summary-report-widget",
        "class": "light vertical-middle",
        "wrapper": {
            "type": "none"
        }
    };

    var plotWidgetConfig = {
        "type": "flot",
        "size": "col-lg-12 col-md-12 col-sm-12 col-xm-12 no-padding",
        "class": "light vertical-middle",
        "style": {
            "height": "300px"
        },
        "wrapper": {
            "type": "none"
        },
        "flotOptions": {
            "series": {
                "lines": {
                    "show": true,
                    "fill":false
                },
                "points": {
                    "show": true
                },
                "valueLabels": {
                    "show": true,
                    "valign": "below",
                    "fontcolor": "#666",
                    "font": "12pt sans-serif",
                    "showShadow": false,
                    "yoffset": 10
                }
            },
            "grid": {
                "hoverable": true,
                "clickable": false,
                "borderWidth": 1,
                "tickColor": "#eee",
                "borderColor": "#eee"
            },
            "colors": ["#579dbf"],
            "tooltip": true,
            "tooltipOpts": {
                "defaultTheme": false,
                "content": "Total %y% covered",
                "shifts": {
                    "x": 0,
                    "y": 20
                }
            },
            "yaxis": {
                "min": 0,
                "max": 120
            },
            "xaxis": {},
            "canvas": true
        }
    };

    var plotMessageWidget = {
        "type": "info",
        "size": "col-lg-12 col-md-12 col-sm-12 col-xm-12 no-padding summary-report-widget",
        "class": "light vertical-middle",
        "wrapper": {
            "type": "none"
        },
        "style": {
            "margin-top" : "60px",
            "margin-bottom": "10px"
        },
        lines: [{
            "class": "added-value-coverage-label",
            "text": "Coverage, %"
        }]
    };



    return function ($scope, logic) {

        $scope.tableWidget = $scope.tableWidget || angular.copy(tableWidgetConfig);
        $scope.plotWidget = $scope.plotWidget || angular.copy(plotWidgetConfig);


        if (!$scope.plotWidget.flotOptions.series.valueLabels.labelFormatter) {
            $scope.plotWidget.flotOptions.series.valueLabels.labelFormatter = function (v) {
                return v + '%';
            };
        }


        $scope.model.widgets = [$scope.tableWidget, plotMessageWidget, $scope.plotWidget];

        fileDao.appsAddedValue().then(function (response) {
            $scope.tableWidget.columns = response.columns;
            $scope.tableWidget.data = response.tableData.data;

            var choosedPath = response.choosedPath;
            var total = choosedPath[0].data.total;
            var toLevelFound = 0;
            var xTicks = [];
            $scope.plotWidget.data = [{
                data: _(choosedPath).map(function (item) {
                    xTicks.push([item.data.level,  (item.data.level > 1 ? 'adding ' : '') + item.module.label]);
                    toLevelFound += item.data.found;
                    return [item.data.level, Math.round(toLevelFound / total * 100)];
                })
            }];
            $scope.plotWidget.flotOptions.xaxis.ticks = xTicks;
        });

        return _.noop;
    };
});