var m = angular.module('dashboard');

m.factory('policySeverityChartLogic', function(widgetSettings, policyDao, modulesManager, $q, modals, dashboardWidgetDao, policySeverityTypes, policiesRefreshHelper) {

    var tagOnlyMode = true;

    var infoWidget = {
        "type": "info",
        "size": "col-lg-6 col-md-6 col-sm-6 col-xm-6",
        "wrapper": {
            "type": "none"
        },
        "class": "policy-severity-info",
        "style": {
            "height": "30px"
        }
    };

    var chartWidget = {
        "type": "flot",
        "size": "col-lg-6 col-md-6 col-sm-6 col-xm-6",
        "style": {
            "height": "50px"
        },
        "wrapper": {
            "type": "none"
        },
        "flotOptions": {
            "series": {
                "bars": {
                    "show": true,
                    "horizontal": true,
                    "align": "center",
                    "barWidth": 0.8,
                    numbers: {
                        show: true,
                        font: '14px open-sans',
                        fontColor: '#444',
                        threshold: 0.25,
                        xAlign: _.identity,
                        yAlign: _.identity,
                        xOffset: 5
                    }

                }
            },
            "grid":{
                "hoverable": true,
                "clickable": false,
                "borderWidth": 1,
                "tickColor": "#eee",
                "borderColor": "#eee"
            },
            "legend": {
                "show": false,
                "position": "ne"
            },
            "yaxis": {
                "axisLabelUseCanvas": true,
                "tickSize": 1
            },
            "xaxis": {
                "minTickSize": 1,
                "min": 0
            },
            "shadowSize": 0,
            "extendedHover": true
        }
    };

    var severities = _(policySeverityTypes.asVariants()).filter(function (item) {
            return item.id !== 'none';
        });

    var severityOrder = _(severities).pluck('id');
    var emptyMatches = _(severities).chain().map(function (item) {
        return [item.id, {
            value: 0,
            color: item.dashboardColor,
            label: item.label,
            texts: []
        }];
    }).object().value();

    return function($scope, logic, reinit) {
        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }

        var defaultTitle = 'Queries Severities';

        if (!$scope.model.dataWidgets) {
            $scope.model.dataWidgets = [
                $.extend(true, {}, infoWidget),
                $.extend(true, {}, infoWidget),
                $.extend(true, {}, chartWidget),
                $.extend(true, {}, chartWidget)
            ];
        }

        function propertiesDialog () {
            var policyVariants = _($scope.model.loadedPolicies).map(function (policy) {
                return {
                    id: policy.policy_id,
                    label: policy.policy_name
                };
            });
            var tags = _($scope.model.loadedPolicies).chain().pluck('policy_data')
                .pluck('tags').flatten().compact().unique().value();
            var config = $scope.model.config || {};
            var params = [{
                type: 'string',
                id: 'title',
                label: 'Widget Title',
                data: config.title
            }, {
                type: 'radio-group',
                id: 'mode',
                label: 'Query fetch mode',
                buttons: [{
                    value: 'tag',
                    label: 'By Tag'
                }, {
                    value: 'policy',
                    label: 'Explicit query'
                }],
                paramClass: 'radio-group-inline align-with-label',
                data: config.mode || 'policy'
            }, {
                type: 'string',
                id: 'leftTitle',
                label: 'Left Value Title',
                data: config.leftTitle
            }, {
                type: 'list',
                id: 'leftPolicyId',
                label: 'Left Total Query',
                variants: policyVariants,
                data: config.leftPolicyId,
                settings: {
                    multiple: false
                },
                validation: {
                    required: true
                },
                enabled: {
                    param: 'mode',
                    equal: 'policy'
                }
            }, {
                type: 'string',
                id: 'leftTag',
                label: 'Left Total Query Tag',
                data: config.leftTag,
                variants: tags,
                validation: {
                    required: true
                },
                enabled: {
                    param: 'mode',
                    equal: 'tag'
                }
            }, {
                type: 'list',
                id: 'leftPolicies',
                label: 'List of queries for the left graph',
                variants: policyVariants,
                data: config.leftPolicies,
                validation: {
                    required: true
                },
                enabled: {
                    param: 'mode',
                    equal: 'policy'
                }
            }, {
                type: 'string',
                id: 'leftListTag',
                label: 'Left Chart Queries Tag',
                data: config.leftListTag,
                variants: tags,
                validation: {
                    required: true
                },
                enabled: {
                    param: 'mode',
                    equal: 'tag'
                }
            }, {
                type: 'string',
                id: 'rightTitle',
                label: 'Right Value Title',
                data: config.rightTitle
            }, {
                type: 'list',
                id: 'rightPolicyId',
                label: 'Right Total Query',
                variants: policyVariants,
                data: config.rightPolicyId,
                settings: {
                    multiple: false
                },
                validation: {
                    required: true
                },
                enabled: {
                    param: 'mode',
                    equal: 'policy'
                }
            }, {
                type: 'string',
                id: 'rightTag',
                label: 'Right Query Tag',
                data: config.rightTag,
                variants: tags,
                validation: {
                    required: true
                },
                enabled: {
                    param: 'mode',
                    equal: 'tag'
                }
            }, {
                type: 'list',
                id: 'rightPolicies',
                label: 'List of queries for the right graph',
                variants: policyVariants,
                data: config.rightPolicies,
                validation: {
                    required: true
                },
                enabled: {
                    param: 'mode',
                    equal: 'policy'
                }
            }, {
                type: 'string',
                id: 'rightListTag',
                label: 'Right Chart Queries Tag',
                data: config.rightListTag,
                variants: tags,
                validation: {
                    required: true
                },
                enabled: {
                    param: 'mode',
                    equal: 'tag'
                }
            }];
            if (tagOnlyMode) {
                params = _(params).filter(function (param) {
                    if (param.id === 'mode' || (param.enabled && param.enabled.param === 'mode' && param.enabled.equal !== 'tag')) {
                        return false;
                    }
                    if (param.enabled && param.enabled.param === 'mode') {
                        delete param.enabled;
                    }
                    return true;
                });
            }
            var pages = [{
                lastPage: true,
                okButton: 'Save',
                params: params,
                buttons: [{
                    label: 'Reset to default',
                    execute: function () {
                        if (!$scope.model.defaults) {
                            return;
                        }
                        if ($scope.model.defaults[logic.infoboxId]) {
                            $scope.model.config = $.extend(true, {}, $scope.model.defaults[logic.infoboxId].conf);
                            dashboardWidgetDao.save(logic.infoboxId, $scope.model.config, true);
                            buildChart();
                        } else {
                            delete $scope.model.config;
                            delete $scope.model.dataWidgets;
                            $scope.model.wrapper.title = defaultTitle;
                            dashboardWidgetDao.remove(logic.infoboxId).then(function () {
                                reinit();
                            });
                        }
                    }
                }]
            }];
            modals.params(pages, 'Configure widget').then(function (data) {
                if (tagOnlyMode) {
                    data.mode = 'tag';
                }
                $scope.model.config = $.extend(true, {}, data);
                dashboardWidgetDao.save(logic.infoboxId, $scope.model.config);
                buildChart();

            });
        }

        if (!$scope.model.defaults) {
            dashboardWidgetDao.defaults().then(function (response) {
                $scope.model.defaults = response.data;
            });
        }

        $q.all([
            policyDao.infoboxCatalogWithFindings(),
            dashboardWidgetDao.retrieve(logic.infoboxId).then(function (response) {
                $scope.model.config = response.data.conf;
                return $q.when(response);
            })
        ]).then(function (responses) {
            $scope.model.wrapper.isLoading = false;
            $scope.model.loadedPolicies = responses[0].data.rows;

            $scope.model.wrapper.buttons = [{
                'class': 'btn-icon',
                iconClass: 'fa fa-wrench',
                tooltip: 'Configuration',
                execute: function() {
                    propertiesDialog();
                }
            }];

            buildChart();
        });

        function buildChart() {
            if (!$scope.model.config) {
                return;
            }
            var info = {
                'left': $scope.model.dataWidgets[0],
                'right': $scope.model.dataWidgets[1]
            };

            var chart = {
                'left': $scope.model.dataWidgets[2],
                'right': $scope.model.dataWidgets[3]
            } ;
            $scope.model.wrapper.title = $scope.model.config.title || defaultTitle;
            var matches = {
                'left': angular.copy(emptyMatches),
                'right': angular.copy(emptyMatches)
            };

            var totalPoliciesAmount = 0;

            _(matches).each(function (match, side) {
                var rows = [];
                var totalPolicies = [];
                if ($scope.model.config.mode === 'tag') {
                    rows = _($scope.model.loadedPolicies).filter(function (policy) {
                        return _(policy.policy_data.tags).contains($scope.model.config[side + 'ListTag']);
                    });

                    totalPolicies = _($scope.model.loadedPolicies).filter(function (policy) {
                        return _(policy.policy_data.tags).contains($scope.model.config[side + 'Tag']);
                    });

                } else {
                    var policyMap = _($scope.model.config[side + 'Policies']).chain().map(function (policyId) {
                        return [policyId, true];
                    }).object().value();
                    rows = _($scope.model.loadedPolicies).filter(function (policy) {
                        return policyMap[policy.policy_id];
                    });

                    totalPolicies = _($scope.model.loadedPolicies).find(function (policy) {
                        return policy.policy_id === $scope.model.config[side + 'PolicyId'];
                    });
                    totalPolicies = totalPolicies || [];
                }

                totalPoliciesAmount += rows.length;

                _(rows).each(function (policy) {
                    if (policy.policy_data.severity && match[policy.policy_data.severity] && policy.findingsStatus.Match) {
                        policiesRefreshHelper.addPolicies(policy.policy_id);
                        match[policy.policy_data.severity].value  += policy.findingsStatus.Match;
                        match[policy.policy_data.severity].texts.push('#' + JSON.stringify({
                                display: 'linked-text',
                                'class': 'policy-chart-hover-link',
                                text: '#' + JSON.stringify({
                                    display: 'link',
                                    type: 'policy-edit',
                                    id: policy.policy_id,
                                    text: policy.policy_data.name + ':'
                                }) + '#' + JSON.stringify({
                                    display: 'text',
                                    'class': 'policy-chart-count',
                                    text: policy.findingsStatus.Match
                                })
                            }));
                    }
                });

                chart[side].flotOptions.yaxis.ticks = _(severityOrder).map(function(severity, idx) {
                    return [idx, match[severity].label];
                });

                var max = 0;
                chart[side].data = _(severityOrder).map(function (severity, idx) {
                    max = Math.max(max, match[severity].value);
                    match[severity].texts.unshift('#' + JSON.stringify({
                            display: 'linked-text',
                            'class': 'policy-chart-hover-link policy-chart-hover-total',
                            text: '#' + JSON.stringify({
                                display: 'text',
                                text: 'Total:'
                            }) + '#' + JSON.stringify({
                                display: 'text',
                                'class': 'policy-chart-count',
                                text: match[severity].value
                            })
                        }));
                    return {
                        data: [[match[severity].value, idx]],
                        color: match[severity].color,
                        label: match[severity].label,
                        hoverText: '#' + JSON.stringify({
                            display: 'linked-text',
                            'class': 'policy-chart-hover-container',
                            text: match[severity].texts.join('')
                        })
                    };
                });
                chart[side].style.height = (severityOrder.length * 30 + 20) + 'px';

                var infoData = [];
                var totalMatch = _(totalPolicies).reduce(function (memo, policy) {
                    return memo + policy.findingsStatus.Match;
                }, 0);
                infoData.push({
                    text: $scope.model.config[side + 'Title'] || 'Total',
                    'class': 'policy-severity-info-left'
                });
                infoData.push({
                    text: totalMatch,
                    'class': 'policy-severity-info-right'
                });
                info[side].lines = infoData;
            });
            if (totalPoliciesAmount > 0) {
                $scope.model.widgets = $scope.model.dataWidgets;
            } else {
                $scope.model.widgets = [{
                    "type": "info",
                    "size": "col-lg-12 col-md-12 col-sm-12 col-xm-12",
                    "wrapper": {
                        "type": "none"
                    },
                    "class": "policy-severity-empty",
                    "style": {
                        "height": "30px"
                    },
                    lines: [{
                        text: 'No queries match the current configurations.'
                    }, {
                        text: 'Click here to configure.',
                        "class": "configure-link",
                        execute: function () {
                            propertiesDialog();
                        }
                    }]
                }];
            }
        }
        return function() {
        };


    };
});