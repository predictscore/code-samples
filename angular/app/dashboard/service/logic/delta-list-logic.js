var m = angular.module('dashboard');

m.factory('deltaListLogic', function(eventsDao, eventsListLogicCore, feature) {
    return function ($scope, logic) {
        if (feature.dashboardUpdateTicks && feature.dashboardUpdateTicks.eventsList) {
            logic.updateTick = feature.dashboardUpdateTicks.eventsList * 1000;
        }

        return eventsListLogicCore($scope, logic, {
            retrieve: function(after) {
                return eventsDao.retrieveDelta($scope.model.columns, after);
            },
            timeColumnName: logic.timeColumnName
        });
    };
});