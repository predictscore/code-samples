var m = angular.module('dashboard');

m.factory('policiesFindingsChartLogic', function(widgetSettings, policyDao, modulesManager, verticalPlotLogicCore, $q, routeHelper, modals, dashboardWidgetDao, policiesRefreshHelper) {
    var tagOnlyMode = true;

    var colors = {
        'Match': '#2980b9',
        'Unmatch': '#f39c12',
        'Pending': '#f35428'
    };

    var showElements = ['Match'];

    var flotOptions = {
        "series": {
            "stack": false, // temporary removed it since now graph is not stacked and numbers plugin can only center on stacked graphs
            "bars": {
                "show": true,
                "horizontal": true,
                "align": "center",
                "barWidth": 0.7,
                numbers: {
                    show: true,
                    font: '14px open-sans',
                    fontColor: '#444',
                    threshold: 0.25,
                    xAlign: _.identity,
                    yAlign: _.identity,
                    xOffset: 5
                }
            }
        },
        "grid":{
            "hoverable": true,
            "clickable": true,
            "borderWidth": 1,
            "tickColor": "#eee",
            "borderColor": "#eee"
        },
        "legend": {
            "show": true,
            "position": "ne"
        },
        "yaxis": {
            "axisLabelUseCanvas": true,
            "tickSize": 1,
            "autoscaleMargin": null
        },
        "xaxis": {
            "minTickSize": 1
        },
        "shadowSize": 0,
        "tooltip": true,
        "tooltipOpts": {
            "defaultTheme": false,
            "content": "%s: %x.0",
            "shifts": {
                "x": 0,
                "y": 20
            }
        }
    };
    return function($scope, logic, reinit) {
        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }

        if (_($scope.model.originalClass).isUndefined()) {
            $scope.model.originalClass = $scope.model['class'] || {};
        }

        var defaultTitle = ' ';

        if (!$scope.model.flotOptions) {
            $scope.model.flotOptions = angular.copy(flotOptions);
            if (showElements.length < 2) {
                $scope.model.flotOptions.legend.show = false;
            }
        }

        $scope.model.flotOptions.legend.labelFormatter = function(label, series) {
            if (series.data[0][1] !== 0) {
                return null;
            }
            return label;
        };

        $scope.model.loadedPolicies = $scope.model.loadedPolicies || [];

        function propertiesDialog () {
            var policyVariants = _($scope.model.loadedPolicies).map(function (policy) {
                return {
                    id: policy.policy_id,
                    label: policy.policy_name
                };
            });
            var tags = _($scope.model.loadedPolicies).chain().pluck('policy_data')
                .pluck('tags').flatten().compact().unique().value();
            var config = $scope.model.config || {};

            var params = [{
                type: 'string',
                id: 'title',
                label: 'Widget Title',
                data: config.title
            }, {
                type: 'radio-group',
                id: 'mode',
                label: 'Query fetch mode',
                buttons: [{
                    value: 'tag',
                    label: 'By Tag'
                }, {
                    value: 'policy',
                    label: 'Explicit query'
                }],
                paramClass: 'radio-group-inline align-with-label',
                data: config.mode || 'policy'
            }, {
                type: 'string',
                id: 'tag',
                label: 'Query Tag',
                data: config.tag,
                variants: tags,
                validation: {
                    required: true
                },
                enabled: {
                    param: 'mode',
                    equal: 'tag'
                }
            }, {
                type: 'list',
                id: 'policies',
                label: 'Choose queries',
                variants: policyVariants,
                data: config.policies,
                validation: {
                    required: true
                },
                enabled: {
                    param: 'mode',
                    equal: 'policy'
                }
            }];

            if (tagOnlyMode) {
                params = _(params).filter(function (param) {
                    if (param.id === 'mode' || (param.enabled && param.enabled.param === 'mode' && param.enabled.equal !== 'tag')) {
                        return false;
                    }
                    if (param.enabled && param.enabled.param === 'mode') {
                        delete param.enabled;
                    }
                    return true;
                });
            }

            modals.params([{
                lastPage: true,
                okButton: 'Save',
                params: params,
                buttons: [{
                    label: 'Reset to default',
                    execute: function () {
                        if (!$scope.model.defaults) {
                            return;
                        }
                        if ($scope.model.defaults[logic.infoboxId]) {
                            $scope.model.config = $.extend(true, {}, $scope.model.defaults[logic.infoboxId].conf);
                            dashboardWidgetDao.save(logic.infoboxId, $scope.model.config, true);
                            buildChart();
                        } else {
                            delete $scope.model.config;
                            delete $scope.model.flotOptions;
                            delete $scope.model.data;
                            $scope.model.wrapper.title = defaultTitle;
                            dashboardWidgetDao.remove(logic.infoboxId).then(function () {
                                reinit();
                            });
                        }
                    }
                }]
            }], 'Configure widget').then(function (data) {
                if (tagOnlyMode) {
                    data.mode = 'tag';
                }
                $scope.model.config = $.extend(true, {}, data);
                dashboardWidgetDao.save(logic.infoboxId, $scope.model.config);
                buildChart();
            });
        }

        if (!$scope.model.defaults) {
            dashboardWidgetDao.defaults().then(function (response) {
                $scope.model.defaults = response.data;
            });
        }

        $q.all([
            policyDao.infoboxCatalogWithFindings(),
            dashboardWidgetDao.retrieve(logic.infoboxId).then(function (response) {
                $scope.model.config = response.data.conf;
                return $q.when(response);
            }).then(function () {
                $scope.model.wrapper.title = $scope.model.config.title || defaultTitle;
            })
        ]).then(function (responses) {
            $scope.model.wrapper.isLoading = false;
            $scope.model.loadedPolicies = responses[0].data.rows;

            $scope.model.wrapper.buttons = [{
                'class': 'btn-icon',
                iconClass: 'fa fa-wrench',
                tooltip: 'Configuration',
                execute: function() {
                    propertiesDialog();
                }
            }];
            buildChart();
        });

        function buildChart() {
            if (!$scope.model.config) {
                return;
            }
            $scope.model.wrapper.title = $scope.model.config.title || defaultTitle;
            var rows = [];
            if ($scope.model.config.mode === 'tag') {
                rows = _($scope.model.loadedPolicies).filter(function (policy) {
                    return policy.findingsStatus && policy.findingsStatus.Match && _(policy.policy_data.tags).contains($scope.model.config.tag);
                });
            } else {
                var policyMap = _($scope.model.config.policies).chain().map(function (policyId, idx) {
                    return [policyId, idx];
                }).object().value();

                rows = _($scope.model.loadedPolicies).chain().filter(function (policy) {
                    return policy.findingsStatus && policy.findingsStatus.Match && !_(policyMap[policy.policy_id]).isUndefined();
                }).sortBy(function (policy) {
                    return policyMap[policy.policy_id];
                }).value();
            }

            rows = _(rows).sortBy(function (row) {
                return row.findingsStatus.Match;
            });

            var policyToIdx = {};
            $scope.model.flotOptions.yaxis.ticks = _(rows).map(function(row, idx) {
                policyToIdx[row.policy_id] = idx;
                return [idx, row.policy_name];
            });
            var chartData = [];
            var max = 0;
            var lastMax = 0;
            policiesRefreshHelper.addPolicies(_(rows).map(function (row) {
                return row.policy_id;
            }));
            _(rows).each(function(row) {
                max = Math.max(max, _(showElements).reduce(function (memo, type) {
                    return memo + row.findingsStatus[type];
                }, 0));
                if (policyToIdx[row.policy_id] > $scope.model.flotOptions.yaxis.ticks.length - 3) {
                    lastMax = Math.max(lastMax, _(showElements).reduce(function (memo, type) {
                        return memo + row.findingsStatus[type];
                    }, 0));
                }

                _(showElements).each(function (type) {
                    chartData.push({
                        label: (showElements.length > 1 ) ? type : row.policy_name,
                        data: [
                            [row.findingsStatus[type], policyToIdx[row.policy_id]]
                        ],
                        color: colors[type],
                        link: routeHelper.getPath('policy-edit', {id: row.policy_id}, {status: type})
                    });
                });
            });
            if ($scope.model.flotOptions.legend.show) {
                $scope.model.flotOptions.xaxis.max = Math.max(max, Math.ceil(lastMax * 1.2));
            } else {
                delete $scope.model.flotOptions.xaxis.max;
            }

            $scope.model.data = chartData;

            if (rows.length > 0) {
                $scope.model.type = 'flot';
                $scope.model.style.height = (rows.length * 33 + 15) + 'px';
                $scope.model['class'] = $scope.model.originalClass;
            } else {
                $scope.model.type = 'info';
                $scope.model.lines = [{
                    text: 'No queries match the current configurations.'
                }, {
                    text: 'Click here to configure.',
                    "class": "configure-link",
                    execute: function () {
                        propertiesDialog();
                    }
                }];
                $scope.model['class'] = 'policy-severity-empty';
                $scope.model.style.height = '300px';
            }

        }

        return _.noop;

    };
});