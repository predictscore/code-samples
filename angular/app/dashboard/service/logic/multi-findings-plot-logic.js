var m = angular.module('dashboard');

m.factory('multiFindingsPlotLogic', function(policyDao, modulesManager, routeHelper, $timeout) {

    function findConditionByPropertyEnd(conditions, value) {
        var found = false;
        _(conditions).find(function (condition) {
            if (condition.property && (condition.property.lastIndexOf(value) === (condition.property.length - value.length))) {
                found = condition;
                return true;
            }
            if (condition.conditions) {
                found = findConditionByPropertyEnd(condition.conditions, value);
                return !!found;
            }
        });
        return found;
    }


    return function($scope, logic) {


        $scope.model.flotOptions = $scope.model.flotOptions || {};

        $scope.model.flotOptions = $.extend(true, {
            legend: false,
            series: {
                funnel: {
                    show: true,
                    stem: {
                        height: 0,
                        width: 0.15
                    },
                    margin: {
                        left: 0.1,
                        right: 0.1,
                        top: 0,
                        bottom: 0
                    },
                    label: {
                        show: 'true',
                        threshold: 0.1,
                        formatter: function (label, slice) {
                            return slice.data[0][1];
                        }
                    },
                    minItemHeight: 0.11
                }
            },
            "grid": {
                "hoverable": true,
                "clickable": true,
                "tickColor": "#dcdcdc",
                "borderWidth": 1,
                "borderColor": "#dcdcdc"
            },
            "colors": [
                "#FF7D47", //"#FFDCDC", //"#FF7D47",
                "#F63715",
                "#E30000"
            ],
            "tooltip": true,
            "tooltipOpts": {
                "content": "%s: %n",
                "defaultTheme": false,
                "shifts": {
                    "x": 0,
                    "y": 20
                }
            }
        }, $scope.model.flotOptions);


        $scope.model.flotOptions.series = $scope.model.flotOptions.series || {};
        $scope.model.flotOptions.legend = false;

        $scope.model.flotOptions.series.funnel = $scope.model.flotOptions.series.funnel || {};

        _($scope.model.flotOptions.series.funnel).defaults();

        policyDao.infoboxCatalog().params({
            filterBy_tags: logic.tag,
            filterBy_entity_type: logic.entityType
        }).then(function (response) {

            $scope.policies = _(response.data.rows).filter(function (policy) {
                if (policy.context.saas !== modulesManager.currentModule() ||
                    !_(policy.tags).contains(logic.tag) ||
                    (logic.entityType && logic.entityType !== policy.entity_type)
                ) {
                    return false;
                }
                var foundCondition = findConditionByPropertyEnd([policy.policy_data.includeCondition], 'detections_numbers');
                if (!foundCondition || foundCondition.operator !== 'greater_than') {
                    return false;
                }
                policy.multiFindingsDetections = foundCondition.data + 1;
                return true;
            });

            policyDao.findingsStatus(_($scope.policies).pluck('policy_id')).then(function (response) {
                $scope.sourceData = {};
                var notEmpty = false;
                _($scope.policies).each(function (policy) {
                    policy.findingsStatus = response.data[policy.policy_id] || {
                            Match: 0
                        };
                    var detections = policy.multiFindingsDetections;
                    $scope.sourceData[detections] = $scope.sourceData[detections] || {
                            count: 0,
                            policies: [],
                            label: (logic.entityTitle || 'Files') + ' detected with ' + detections + ' or more engines',
                            detections: detections
                        };
                    $scope.sourceData[detections].policies.push(policy);
                    $scope.sourceData[detections].count += policy.findingsStatus.Match;
                    notEmpty = notEmpty || (policy.findingsStatus.Match > 0);
                });
                $scope.model.notEmpty = notEmpty;

                $scope.model.data = _($scope.sourceData).chain().map(function (item) {
                    return item;
                }).filter(function (item) {
                    return item.count;
                }).sortBy('detections').map(function (item) {
                    return {
                        label: item.label,
                        data: item.count,
                        link: routeHelper.getPath('policy-edit', {id: item.policies[0].policy_id})
                    };
                }).value();
            });
        });

        return _.noop;
    };
});