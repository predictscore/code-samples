var m = angular.module('dashboard');

m.factory('alertsListLogic', function(alertsDao, eventsListLogicCore, feature) {
    return function ($scope, logic) {
        if (feature.dashboardUpdateTicks && feature.dashboardUpdateTicks.alertsList) {
            logic.updateTick = feature.dashboardUpdateTicks.eventsList * 1000;
        }

        return eventsListLogicCore($scope, logic, {
            retrieve: function(after) {
                return alertsDao.retrieve($scope.model.columns, after);
            },
            timeColumnName: logic.timeColumnName
        });
    };
});