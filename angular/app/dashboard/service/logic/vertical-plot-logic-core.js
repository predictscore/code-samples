var m = angular.module('dashboard');

m.factory('verticalPlotLogicCore', function(routeHelper, $timeout, $q) {
    return function ($scope, logic, coreOptions) {

        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }


        var idColumn = coreOptions.idColumn;
        var countColumn = coreOptions.countColumn || 'count';
        var makeLabel = coreOptions.makeLabel;
        var makeTick = coreOptions.makeTick || coreOptions.makeLabel;
        var lines = logic.lines || 10;
        var retrieveCancel = $q.defer();

        coreOptions.retrieve().timeout(retrieveCancel.promise).then(function(response) {
            if (logic.showFlotNumbers) {
                if ($scope.model.flotOptions.series && $scope.model.flotOptions.series.bars && !$scope.model.flotOptions.series.bars.numbers) {
                    $scope.model.flotOptions.series.bars.numbers = {
                        show: true,
                        font: '14px open-sans',
                        fontColor: '#444',
                        threshold: 0.25,
                        xAlign: _.identity,
                        yAlign: _.identity,
                        xOffset: 5
                    };
                }
            }


            $scope.model.wrapper.isLoading = false;
            var rows = response.data.rows || [];
            if (logic.noEmptyLines) {
                lines = rows.length || 1;
            }
            rows = _(rows).chain().sortBy(function(row) {
                return row[countColumn];
            }).first(lines).value();
            var idx = 0;
            $scope.model.flotOptions.xaxis.tickFormatter = function(text) {
                return parseInt(text);
            };
            var ticks = $scope.model.flotOptions.yaxis.ticks = [];
            for(var i = 0; i < lines - rows.length; i++) {
                ticks.push([idx++, '']);
            }
            var idToIdx = {};
            _(rows).each(function(row) {
                idToIdx[row[idColumn]] = idx++;
                ticks.push([idToIdx[row[idColumn]], makeTick(row)]);
            });
            $scope.model.data = _(rows).map(function(row) {
                var item = {
                    label: makeLabel(row),
                    data: [[row[countColumn], idToIdx[row[idColumn]]]]
                };
                if (coreOptions.linkTo) {
                    var linkParams = {}, linkQs;
                    if (coreOptions.linkParams) {
                        _(coreOptions.linkParams).each(function (opts, param) {
                            if (opts.column) {
                                linkParams[param] = row[opts.column];
                                return;
                            }
                            if (opts.value) {
                                linkParams[param] = opts.value;
                            }
                        });
                    } else {
                        linkParams.id = row[idColumn];
                    }
                    if (coreOptions.linkQs) {
                        linkQs = {};
                        _(coreOptions.linkQs).each(function (opts, param) {
                            if (opts.column) {
                                linkQs[param] = row[opts.column];
                                return;
                            }
                            if (opts.value) {
                                linkQs[param] = opts.value;
                            }
                        });
                    }
                    item.link = routeHelper.getPath(coreOptions.linkTo, linkParams, linkQs);
                }
                return item;
            });
            $scope.model.style = $scope.model.style || {};
            $.extend($scope.model.style, {
                height: (idx * 33 + 20) + 'px'
            });

        });

        return function () {
            retrieveCancel.resolve(true);
        };
    };
});