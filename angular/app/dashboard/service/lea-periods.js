var m = angular.module('common');

m.factory('leaPeriods', function() {
    var periods = [{
        'label': '10 days',
        type: 'days',
        amount: 10,
        groupPeriod: 'day',
        groupFormat: 'YYYY-MM-DD'
    }, {
        'label': '30 days',
        type: 'days',
        amount: 30,
        groupPeriod: 'day',
        groupFormat: 'YYYY-MM-DD'
    }, {
        'label': '90 days',
        type: 'days',
        amount: 90,
        groupPeriod: 'day',
        groupFormat: 'YYYY-MM-DD'
    }, {
        'label': '180 days',
        type: 'days',
        amount: 180,
        groupPeriod: 'week',
        periodStart: 'isoWeek',
        groupFormat: 'IYYY"W"IW'
    }, {
        'label': '1 year',
        type: 'years',
        amount: 1,
        groupPeriod: 'month',
        groupFormat: 'YYYY-MM'
    }];

    var periodsMap = _(periods).chain().map(function (period) {
        var periodId = period.type + '_' + period.amount;
        period.id = periodId;
        period.days = moment().diff(moment().subtract(period.amount, period.type), 'days');
        return [periodId, period];
    }).object().value();

    return {
        periods: periods,
        periodsMap: periodsMap,
        defaultPeriodId: 'days_180'
    };
});