var m = angular.module('quarantine');

m.controller('EmailRestoreController', function($scope, $routeParams, emailDao, routeHelper) {
    var msg = $routeParams.msg;
    emailDao.retrieveByQuarantineUuid($routeParams.msg).then(function (response) {
        routeHelper.redirectTo('email', {
            id: response.data.entity_id
        });
    });
});
