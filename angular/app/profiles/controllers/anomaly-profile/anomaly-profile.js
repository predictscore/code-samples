var m = angular.module('profiles');

m.controller('AnomalyProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path, openAppStatusListener, entityTypes, objectTypeDao, modulesDao) {
    var module = $routeParams.module;
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;

    var anomalyEntity = entityTypes.toBackend(module, 'anomaly');

    var viewerEntity;
    var objectTypes;
    var viewerEntityInfo;
    var viewerColumns;
    var columnEntityIds;

    widgetPolling.reload(function () {
        viewerEntity = false;
        objectTypeDao.retrieve().then(function(response) {
            if (!response.data) {
                return;
            }
            objectTypes = response.data;
            if (!objectTypes.entities[anomalyEntity] || !objectTypes.entities[anomalyEntity].display_entity) {
                return;
            }
            viewerEntity = objectTypes.entities[anomalyEntity].display_entity;
            if (!objectTypes.entities[anomalyEntity].display_related_entities[viewerEntity]) {
                return;
            }
            viewerColumns = _(objectTypes.entities[anomalyEntity].display_related_entities[viewerEntity].related_fields).pluck(1);
            columnEntityIds = _(objectTypes.entities[anomalyEntity].display_related_entities[viewerEntity].related_fields).pluck(1);
            return modulesDao.entityInfo(module, viewerEntity, true).then(function (response) {
                viewerEntityInfo = response.data;
                if (objectTypes.entities[viewerEntity]) {
                    _(objectTypes.entities[viewerEntity].properties).each(function (property) {
                        if (property.entity_pointer) {
                            var matches = property.name.match(/\((.*)\)$/);
                            if (matches.length === 2 && viewerEntityInfo[matches[1]]) {
                                viewerEntityInfo[matches[1]].entity_pointer = property.entity_pointer;
                            }
                        }
                    });
                }
            });
        }).then(function () {
            if (!viewerEntityInfo) {
                widgetPolling.setLocal({
                    "widgets": [{
                        "type": "info",
                        "size": "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                        "class": "light high-light-new medium-icons",
                        "wrapper": {
                            "type": "widget-panel",
                            "title": "Anomaly details are not available for this anomaly",
                            "class": "light white-body white-header no-margin"
                        },
                        "bodyStyle": {
                            "display": "none"
                        }

                    }]
                });
            } else {
                var columns = _(columnEntityIds).chain().map(function (colId, idx) {
                    if (!viewerEntityInfo[colId]) {
                        return;
                    }
                    if (viewerEntityInfo[colId].gui_hidden) {
                        return {
                            id: viewerColumns[idx],
                            hidden: true
                        };
                    }
                    if (!viewerEntityInfo[colId].label) {
                        return;
                    }
                    var column = {
                        id: viewerColumns[idx],
                        text: viewerEntityInfo[colId].label
                    };
                    if (viewerEntityInfo[colId].entity_pointer) {
                        column.converter = 'entityPointerConverter';
                        column.converterArgs = {
                            entityType: viewerEntityInfo[colId].entity_pointer
                        };
                    }
                    if (viewerEntityInfo[colId].value_type === "DATETIME") {
                        column.format = "time";
                    }
                    if (viewerEntityInfo[colId].reference_id && viewerEntityInfo[colId].reference_type) {
                        var refColumnIdx = _(columnEntityIds).findIndex(function (columnId) {
                            return columnId === viewerEntityInfo[colId].reference_id;
                        });
                        var saas = entityTypes.toSaas(viewerEntityInfo[colId].reference_type);
                        var local = entityTypes.toLocal(viewerEntityInfo[colId].reference_type);
                        if ((refColumnIdx !== -1) && saas && local) {
                            column.href = {
                                useOriginal: true,
                                type: local,
                                params: {
                                    id: viewerColumns[refColumnIdx],
                                    module: saas
                                }
                            };
                        }
                    }
                    if (viewerEntityInfo[colId].options && !column.converter) {
                        column.converter = "optionsMapConverter";
                        column.converterArgs = _(viewerEntityInfo[colId].options).chain().map(function (option) {
                            return [option.id, option.label];
                        }).object().value();
                    }
                    if (viewerEntityInfo[colId].list) {
                        var listConverterArgs = {
                            "collapseRest": true,
                            "expanded": 4
                        };
                        if (column.converter) {
                            var multiConverters = [];
                            multiConverters.push({
                                converter: column.converter,
                                converterArgs: column.converterArgs
                            });
                            multiConverters.push({
                                converter: "listConverter",
                                converterArgs: listConverterArgs
                            });
                            column.converter = "multiConverter";
                            column.converterArgs = {
                                converters: multiConverters
                            };
                        } else {
                            column.converter = "listConverter";
                            column.converterArgs = listConverterArgs;
                        }
                    }
                    return column;
                }).compact().value();
                widgetPolling.setLocal({
                    "widgets": [{
                        "type": "data-table",
                        "logic": {
                            "name": "anomalyDetailsListLogic",
                            "updateTick": 60000
                        },
                        "uuid": "5c3a7acb-2a41-42fc-87c8-1b81a1fe77c5",
                        "size": "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                        "class": "light high-light-new medium-icons",
                        "wrapper": {
                            "type": "widget-panel",
                            "title": "Anomaly details",
                            "class": "light white-body white-header no-margin"
                        },
                        "style": {
                            "min-height": "600px"
                        },
                        "options": {
                            "pagesAround": 2,
                            "pageSize": 20,
                            "pagination": {
                                "page": 1
                            },
                            "disableTop": true
                        },
                        columns: columns
                    }]
                });
                widgetPolling.scope({
                    id: $routeParams.id,
                    anomalyEntity: anomalyEntity
                });
            }
        });
    });



});