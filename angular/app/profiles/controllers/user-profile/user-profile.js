var m = angular.module('profiles');

m.controller('UserProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path, userDao, $route, objectTypeDao, entityTypes, $q) {
    var module = $routeParams.module;
    var userId = $routeParams.id;
    var localType = $route.current.name || 'user';
    
    objectTypeDao.retrieve().then(function(response) {
        widgetPolling.scope({
            id: userId,
            localType: localType,
            objectTypes: response,
            entityType: entityTypes.toBackend(module, localType)
        });
    
        $scope.widgetPolling = widgetPolling;
        $scope.getSizeClasses = sizeClasses;
    
        var root = path('profiles').ctrl('user-profile');

        $q.when((function () {
            if (module == 'google_drive') {
                return userDao.retrieve(userId).then(function (response) {
                    return response.data.is_external;
                });
            }
            return false;
        })()).then(function (isExternal) {
            widgetPolling.updateWidgets(root.json(module + '.' + (isExternal ? 'external-': '') + localType + '-profile').path,
                root.json('common-widgets').path
            );
        });
    });
});