var m = angular.module('profiles');

m.controller('ApplicationProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path, objectTypeDao) {
    var module = $routeParams.module;
    
    objectTypeDao.retrieve().then(function(response) {
        widgetPolling.scope({
            id: $routeParams.id,
            objectTypes: response
        });
        widgetPolling.updateWidgets(path('profiles').ctrl('application-profile').json(module + '.application-profile').path);
        $scope.widgetPolling = widgetPolling;
        $scope.getSizeClasses = sizeClasses;
    });
});