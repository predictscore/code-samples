var m = angular.module('profiles');

m.controller('GroupProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path, $route, objectTypeDao) {
    var module = $routeParams.module;
    var localType = $route.current.name || 'group';
    
    objectTypeDao.retrieve().then(function(response) {
        widgetPolling.scope({
            id: $routeParams.id,
            localType: localType,
            objectTypes: response
        });
        widgetPolling.updateWidgets(path('profiles').ctrl('group-profile').json(module + '.' + localType +'-profile').path);
        $scope.widgetPolling = widgetPolling;
        $scope.getSizeClasses = sizeClasses;
    });
});