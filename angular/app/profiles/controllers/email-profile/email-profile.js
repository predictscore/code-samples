var m = angular.module('profiles');

m.controller('EmailProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path, openAppStatusListener, entityTypes, objectTypeDao, $q, emailDao, troubleshooting, modals, emailView) {
    var module = $routeParams.module;
    var id = $routeParams.id;

    var showAllEmailsListener = _.noop;

    $q.all([
        objectTypeDao.retrieve(),
        (module !== 'office365_emails') ? null :
            emailDao.retrieve(id).then(function (response) {
                if (response && response.data && response.data.pre_release_id && response.data.pre_release_id != 'NULL') {
                    return response.data.pre_release_id;
                }
                return false;
            }),
        troubleshooting.entityDebugEnabled() ? path('profiles').ctrl('email-profile').json(module + '.all-user-emails').retrieve()
            .onRequestFail(function () {
                return $q.when(null);
            }): null
    ]).then(function(responses) {
        widgetPolling.scope({
            id: id,
            entityType: entityTypes.toBackend(module, 'email'),
            objectTypes: responses[0],
            preReleaseId: responses[1]
        });

        widgetPolling.updateWidgets(
            path('profiles').ctrl('email-profile').json(module + '.email-profile').path,
            path('profiles').ctrl('email-profile').json('common-widgets').path
        );
        $scope.widgetPolling = widgetPolling;
        $scope.getSizeClasses = sizeClasses;
        if (responses[2] && responses[2].data) {
            var debugEmailsListWidget = responses[2].data;
            showAllEmailsListener = $scope.$on('show-all-user-emails', function (event, data) {
                if (debugEmailsListWidget.logic.userId !== data.userId) {
                    delete debugEmailsListWidget.data;
                    debugEmailsListWidget.logic.userId = data.userId;
                    debugEmailsListWidget.wrapper.Loading = true;
                }
                delete debugEmailsListWidget.options.pagination.columnsFilters;
                modals.widget({
                    panel: debugEmailsListWidget,
                    windowClass: 'size-xl',
                    closeOnNavigate: true
                });
            });
        }
    });

    var destroyAppStatusListener = openAppStatusListener($scope);
    var destroyEmailSourceViewListener = $scope.$on('view-email-source', function (event, data) {
        emailView.viewSource(data);
    });
    var destroyEmailRenderedViewListener = $scope.$on('view-email-rendered', function (event, data) {
        emailView.viewRendered(data);
    });


    $scope.$on('$destroy', function() {
        destroyAppStatusListener();
        showAllEmailsListener();
        destroyEmailSourceViewListener();
        destroyEmailRenderedViewListener();
    });

});
