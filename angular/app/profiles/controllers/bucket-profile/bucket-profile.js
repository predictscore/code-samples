var m = angular.module('profiles');

m.controller('BucketProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path, objectTypeDao) {
    var module = $routeParams.module;
    
    objectTypeDao.retrieve().then(function(response) {
        widgetPolling.scope({
            id: $routeParams.id,
            objectTypes: response
        });
        widgetPolling.updateWidgets(path('profiles').ctrl('bucket-profile').json(module + '.bucket-profile').path);
        $scope.widgetPolling = widgetPolling;
        $scope.getSizeClasses = sizeClasses;
    });
});