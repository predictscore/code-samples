var m = angular.module('profiles');

m.controller('IpProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path) {
    var module = $routeParams.module;
    widgetPolling.scope({
        id: $routeParams.id
    });
    widgetPolling.updateWidgets(path('profiles').ctrl('ip-profile').json(module + '.ip-profile').path);
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
});