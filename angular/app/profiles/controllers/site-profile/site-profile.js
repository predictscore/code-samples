var m = angular.module('profiles');

m.controller('SiteProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path, objectTypeDao) {
    var module = $routeParams.module;
    
    objectTypeDao.retrieve().then(function(response) {
        widgetPolling.scope({
            id: $routeParams.id,
            objectTypes: response
        });
        widgetPolling.updateWidgets(path('profiles').ctrl('site-profile').json(module + '.site-profile').path);
        $scope.widgetPolling = widgetPolling;
        $scope.getSizeClasses = sizeClasses;
    });
});