var m = angular.module('profiles');

m.controller('FileProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path, openAppStatusListener, entityTypes, objectTypeDao) {
    var module = $routeParams.module;
    
    objectTypeDao.retrieve().then(function(response) {
        widgetPolling.scope({
            id: $routeParams.id,
            entityType: entityTypes.toBackend(module, 'file'),
            objectTypes: response
        });
        widgetPolling.updateWidgets(
            path('profiles').ctrl('file-profile').json(module + '.file-profile').path,
            path('profiles').ctrl('file-profile').json('common-widgets').path
        );
        $scope.widgetPolling = widgetPolling;
        $scope.getSizeClasses = sizeClasses;
    });
    
    var destroyAppStatusListener = openAppStatusListener($scope);
    
    $scope.$on('$destroy', function() {
        destroyAppStatusListener();
    });
});