var m = angular.module('profiles');

m.controller('ProfileController', function($scope, $routeParams, widgetPolling, sizeClasses, path, objectTypeDao, modulesDao, $q, defaultHttpErrorHandler, openAppStatusListener, $injector) {

    var widgetsScope = {
        entityType: $routeParams.entityType,
        id: $routeParams.id
    };
    var widgetsPath = 'entity/' +widgetsScope.entityType;

    $q.all([
        objectTypeDao.retrieve(),
        path('profiles').ctrl('profile').json('entity/' +widgetsScope.entityType).retrieve(true).
            onRequestFail(function () {
            return $q.when({});
        })
    ]).then(function (responses) {
        widgetsScope.objectTypes = responses[0];
        var saasByEntityType = widgetsScope.objectTypes.data &&
            widgetsScope.objectTypes.data.entities &&
            widgetsScope.objectTypes.data.entities[widgetsScope.entityType] &&
            widgetsScope.objectTypes.data.entities[widgetsScope.entityType].saas;
        if (saasByEntityType) {
            widgetsScope.module = saasByEntityType;
            $injector.get('modulesManager').currentModule(widgetsScope.module === 'office365_mgmnt' ? 'office365_emails' : widgetsScope.module); //TODO: this line is for compatibility of v2 with v1
        }
        if (!responses[1].data) {
            if (!saasByEntityType) {
                return $q.reject({
                    status: 404
                });
            }
            return modulesDao.entityInfoRaw(widgetsScope.module, widgetsScope.entityType)
                .then(function (rawEntityInfo) {
                    widgetsScope.rawEntityInfo = rawEntityInfo;
                    if (rawEntityInfo.sec_type_connected && rawEntityInfo.sec_type_connected.length) {
                        widgetsPath = 'basic-scannable-profile';
                    } else {
                        widgetsPath = 'basic-profile';
                    }
                });
        }
    }).then(function () {
            widgetPolling.scope(widgetsScope);
            widgetPolling.updateWidgets(
                path('profiles').ctrl('profile').json(widgetsPath).path,
                path('profiles').ctrl('profile').json('common-widgets').path
            );
            $scope.widgetPolling = widgetPolling;
            $scope.getSizeClasses = sizeClasses;

    }, defaultHttpErrorHandler);

    var destroyAppStatusListener = openAppStatusListener($scope);
    
    $scope.$on('$destroy', function() {
        destroyAppStatusListener();
    });
});