var m = angular.module('profiles');

m.factory('quarantineStatusConverter', function() {
    return function(entityType, info) {
        if (entityType === "office365_emails_attachment") {
            // old version support, should be removed some time after moving to new version
            if (info.is_orig_quarantine) {
                if (info.is_orig_quarantine_restored) {
                    return 'This is the original attachment';
                }
                return 'This attachment was quarantined';
            }
            // end of old version support code. remove until here



            if (!info.is_quarantined) {
                if (!info.quarantine_is_restored_attachment) {
                    return '';
                }
                return 'Restored from Quarantine';
            }

            if (info.quarantine_is_original_attachment) {
                if (info.is_quarantined_restored) {
                    return 'Quarantined and Restored';
                }
                return 'Quarantined';
            }

            if (info.quarantine_is_quarantined_attachment) {
                if (info.is_quarantined_restored) {
                    return 'Quarantined and Restored - Secured Attachment';
                }
                return 'Quarantined - Secured Attachment';
            }

            if (info.quarantine_is_replacement_attachment) {
                if (info.is_quarantined_restored) {
                    return 'Quarantined and Restored - Replacement Attachment';
                }
                return 'Quarantined - Replacement Attachment';
            }
            if (info.container_eml_is_quarantined_restored) {
                return 'Quarantined and Restored by Email';
            }
            return 'Quarantined by Email';
        }
        if (entityType === "sharefile_file") {
            if (info.item_info && info.item_info.is_quarantined) {
                return 'Quarantined';
            }
        }
        if (entityType === "office365_onedrive_file" || entityType === "office365_sharepoint_file") {
            var infoMsg;
            if (info.is_in_quarantine) {
                if (info.restored_into_id) {
                    infoMsg = 'This is quarantine file. It was restored.';
                } else {
                    infoMsg = 'This is quarantine file';
                }
                infoMsg = '#' + JSON.stringify({
                        'class': 'quarantined-status',
                        display: 'text',
                        text: infoMsg
                    });
                if (info.quar_orig_file_id) {
                    infoMsg += '#' + JSON.stringify({
                            display: 'link',
                            text: "Original File",
                            type: 'file',
                            id: info.quar_orig_file_id,
                            'class': 'quarantined-entity-link'
                        });
                }
                if (info.restored_into_id) {
                    infoMsg += '#' + JSON.stringify({
                            display: 'link',
                            text: "Restored File",
                            type: 'file',
                            id: info.restored_into_id,
                            'class': 'quarantined-entity-link'
                        });
                }
                return infoMsg;
            } else {
                if (info.quarantined_into_id) {
                    return '#' + JSON.stringify({
                            'class': 'quarantined-status',
                            display: 'text',
                            text: 'Quarantined'
                        }) + '#' + JSON.stringify({
                            display: 'link',
                            text: "Quarantine File",
                            type: 'file',
                            id: info.quarantined_into_id,
                            'class': 'quarantined-entity-link'
                        });
                }
            }
        }
        return '';
    };
});