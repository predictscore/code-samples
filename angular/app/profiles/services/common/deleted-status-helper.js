var m = angular.module('profiles');

m.factory('deletedStatusHelper', function() {
    return {
        isVisible: function (entityType, info) {
            if (entityType === "office365_onedrive_file" || entityType === "office365_sharepoint_file") {
                if (info.quarantined_into_id || info.restored_into_id) {
                    return false;
                }
            }
            return true;
        }
    };
});