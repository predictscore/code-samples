var m = angular.module('profiles');

m.factory('revokeUserListener', function(objectTypeDao, manualActionDao, modals, remediationProperties, widgetPolling) {
    return function($scope, id, type, logic, properties) {
        return $scope.$on('revoke-user', function(event, data) {

            if (properties && properties.actionTypes && properties.actionTypes[data.eventName]) {
                var actionType = properties.actionTypes[data.eventName];
                var modal;
                var body = {
                    name: data.eventName,
                    attributes: $.extend(true, {}, data.attributes)
                };
                body.attributes[type] = '{' + logic.entityType + '}';

                var boolCheckedLabels = _(actionType.attributes).chain().filter(function(param) {
                    return body.attributes[param.id] === true && param.type === 'boolean' && _(param.label).isString();
                }).map(function (param) {
                    return param.label;
                }).value();

                modal = modals.params([{
                    params: _(actionType.attributes).chain().filter(function(param) {
                        return _(param.label).isString() && _(body.attributes[param.id]).isUndefined();
                    }).map(function(param) {
                        return $.extend(true, {}, param);
                    }).value(),
                    lastPage: true,
                    getProperties: _.memoize(remediationProperties(properties.properties))
                }], actionType.label, actionType.size, {
                    noDataLabel: boolCheckedLabels.length > 0 ? 'Are you sure you want to ' + boolCheckedLabels.join(' and ') + '?' : actionType.confirm
                }).then(function (dialogData) {
                    $.extend(true, body.attributes, dialogData);
                });
                modal.then(function () {
                    manualActionDao.create(logic.entityType, id, body).then(function () {
                        widgetPolling.emit('remediationActionPerformed');
                    });
                });
            }
        });
    };
});