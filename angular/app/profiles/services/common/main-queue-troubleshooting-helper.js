var m = angular.module('profiles');

m.factory('mainQueueTroubleshootingHelper', function(modals, troubleshooting) {

    var troubleshootingPanel = {
        "type": "data-table",
        "logic": {
            "name": "fileMainQueueLogic"
        },
        "uuid": "6e39a65c-cfd5-4b57-856f-d8faec0cf795",
        "wrapper": {
            "type": "widget-panel",
            "title": "Main Queue info",
            "class": "avanan white-body overflow-auto"
        },
        "class": "light table-header-nowrap",
        "options": {
            "checkboxes": false,
            "singleSelection": false,
            "forceServerProcessing": false,
            "pagination": {
                "page": 1,
                "ordering": {},
                "filter": ''
            }
        },
        "columns": [{
            "id": "id",
            "text": "id",
            "sortable": true
        }, {
            "id": "entity_id",
            "text": "entity_id",
            "sortable": true
        }, {
            "id": "entity_type",
            "text": "entity_type",
            "sortable": true
        }, {
            "id": "handled",
            "text": "handled",
            "sortable": true
        }, {
            "id": "insert_time",
            "text": "insert_time",
            "format": "time",
            "sortable": true
        }, {
            "id": "module_src",
            "text": "module_src",
            "sortable": true
        }, {
            "id": "module_dst",
            "text": "module_dst",
            "sortable": true
        }, {
            "id": "policy_id",
            "text": "policy_id",
            "sortable": true
        }, {
            "id": "priority",
            "text": "priority",
            "sortable": true
        }, {
            "id": "request_type",
            "text": "request_type",
            "sortable": true
        }, {
            "id": "src_entity_type",
            "text": "src_entity_type",
            "sortable": true
        }, {
            "id": "data_hash",
            "text": "data_hash",
            "sortable": true
        }, {
            "id": "data_pickle",
            "text": "data_pickle",
            "converter": "JSONConverter",
            "dataStyle": {
                "word-wrap": "break-word"
            }
        }]
    };


    var mainQueueTroubleshootingHelper = {
        button: function () {
            if (!troubleshooting.available()) {
                return false;
            }
            return {
                label: 'Troubleshooting - Main Queue',
                execute: function () {
                    return modals.widget({
                        panel: troubleshootingPanel,
                        windowClass: 'size-xl'
                    });
                }
            };
        },
        addTroubleshootingButton: function (wrapper) {
            if (troubleshooting.available()) {
                wrapper.buttons = wrapper.buttons || [];
                if (!_(wrapper.buttons).find(function (button) {
                        return button.id === 'main-queue-info-troubleshooting';
                    })) {
                    wrapper.buttons.unshift($.extend(mainQueueTroubleshootingHelper.button(), {
                        id: 'main-queue-info-troubleshooting',
                        'class': 'btn-blue',
                        "position": "main-left"
                    }));
                }
            }
        }
    };

    return mainQueueTroubleshootingHelper;
});