var m = angular.module('profiles');

m.factory('fileSecurityDebugHelper', function(modals) {

    var fullInfoPanel = {
        "type": "data-table",
        "logic": {
            "name": "fileFullSecurityInfoLogic",
            "source": "sec_entities"
        },
        "wrapper": {
            "type": "widget-panel",
            "title": "Full Info",
            "class": "avanan white-body overflow-auto"
        },
        "class": "light table-header-nowrap",
        "options": {
            "checkboxes": false,
            "singleSelection": false,
            "forceServerProcessing": false,
            "pagination": {
                "page": 1,
                "ordering": {},
                "filter": ''
            }
        },
        "columns": [{
            "id": "app",
            "text": "Key",
            "sortable": true,
            "converter": "clickEventConverter",
            "converterArgs": {
                "eventName": "open-full-info-details",
                "tooltip": "Show full info",
                "attributes": {
                    "app": "app"
                },
                "class": "link-like"
            }
        }, {
            "id": "status",
            "text": "Value / Status",
            "sortable": true
        }, {
            "id": "info",
            "hidden": true
        }]
    };


    return {
        showSecEntities: function (entityId, entityType) {
            delete fullInfoPanel.data;
            fullInfoPanel.logic.entityId = entityId;
            fullInfoPanel.logic.entityType = entityType;
            fullInfoPanel.logic.source = 'sec_entities';
            fullInfoPanel.logic.noNulls = true;
            fullInfoPanel.wrapper.title = 'Full output of sec_entities API';
            return modals.widget({
                panel: fullInfoPanel
            }, 'lg');

        },
        showResolveEntity: function (entityId, entityType) {
            fullInfoPanel.logic.entityId = entityId;
            fullInfoPanel.logic.entityType = entityType;
            fullInfoPanel.logic.source = 'resolve_entity';
            fullInfoPanel.logic.noNulls = false;
            fullInfoPanel.wrapper.title = 'Full output of resolve_entity API';
            return modals.widget({
                panel: fullInfoPanel
            }, 'lg');

        }
    };
});