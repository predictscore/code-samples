var m = angular.module('profiles');

m.factory('appScanStatusHelper', function(troubleshooting) {
    var appScanStatusHelper = {
        statisticsToStatus: function(statistics, moduleName, scanResult) {
            var scanStatus = 'hide';
            if(statistics.total === 0) {
                scanStatus = 'notScanned';
            } else if(statistics.total_match == 1) {
                scanStatus = 'found';
            } else if(statistics.total_unmatch == 1) {
                scanStatus = 'clean';
            }
            if (moduleName && scanResult) {
                return appScanStatusHelper.scanResultStatusFix(scanStatus, moduleName, scanResult);
            }
            return scanStatus;
        },
        statusToClass: function(status) {
            return {
                'fa': status !== 'na',
                'big-icon': true,
                'cursor-pointer': appScanStatusHelper.statusClickable(status),
                'fa-check-circle': status === 'clean',
                'green-text': status === 'clean',
                'fa-exclamation-circle': status === 'found' || status === 'found-no-click',
                'red-text': status === 'found' || status === 'found-no-click',
                'fa-question-circle': status === 'notScanned',
                'fa-hourglass': status === 'pending',
                'icon-in-circle': status === 'pending',
                'status-na': status === 'na',
                'fa-info-circle': status === 'troubleshooting' || status === 'suspicious',
                'yellow-text': status === 'troubleshooting' || status === 'suspicious'
            };
        },
        statisticsToClass: function(statistics, moduleName, scanResult) {
            var status = appScanStatusHelper.statisticsToStatus(statistics, moduleName, scanResult);
            return appScanStatusHelper.statusToClass(status);
        },
        visibleStatuses: ['found', 'clean', 'pending', 'na', 'suspicious', 'found-no-click'],
        statusClickable: function (status) {
            return troubleshooting.enabled() || status === 'clean' || status === 'found' || status === 'suspicious';
        },
        scanResultStatusFix: function (scanStatus, moduleName, scanResult) {
            if (moduleName === 'cylance' && scanResult && scanResult.result === 'suspicious') {
                return 'suspicious';
            }
            if (moduleName === 'wildfire' && scanResult && scanResult.result === 'grayware') {
                return 'suspicious';
            }
            if (moduleName === 'solgate' && scanStatus === 'clean' && scanResult && scanResult.tags && scanResult.tags.length) {
                return 'suspicious';
            }
            if (moduleName === 'ap_cyren' && scanStatus === 'found' && scanResult && scanResult.phishing_links === 'clean') {
                return 'found-no-click';
            }
            if (moduleName === 'av_cisco' && scanResult && scanResult.result === 'suspicious') {
                return 'suspicious';
            }
            if (moduleName === 'ap_avanan' && scanResult && scanResult.phishing === 'suspicious') {
                return 'suspicious';
            }
            return scanStatus;
        },
        eventViewTooltip: function (scanStatus, moduleName, scanResult) {
            if (moduleName === 'solgate' && scanResult && scanResult.tags && scanResult.tags.length) {
                return scanResult.tags.join(', ');
            }
        }
    };
    return appScanStatusHelper;
});