var m = angular.module('profiles');

m.factory('emailView', function(modals, $injector, $sanitize) {
    var emailDao;
    function getEmailDao () {
        if (!emailDao) {
            emailDao = $injector.get('emailDao');
        }
        return emailDao;
    }
    var warningText = 'Warning!<br />' +
        'You\'re going to view sanitized version of the e-mail but with all images shown<br />' +
        'This will allow sender to track this "view" event and record your IP address<br />';

    function downloadEmail (args) {
        window.open('/api/v1/sec_match_file/' + args.entityType + '/' + args.moduleName + '/' + args.entityId, '_blank');
    }

    function showRendered (args) {
        var win = window.open('', '_blank');
        getEmailDao().emailSource(args.moduleName, args.entityType, args.entityId).then(function (data) {
            var emailText = $sanitize(data.data);
            win.document.title = 'View Email';
            win.document.documentElement.innerHTML = emailText;
        }, function (error) {
            win.close();
            modals.alert((error && error.status === 404) ? 'Message not found':
            'Error getting message. HTTP status code: ' + (error && error.status), {
                title: 'Error'
            });
        });
    }

    function showSource (args) {
        var win = window.open('', '_blank');
        getEmailDao().emailSource(args.moduleName, args.entityType, args.entityId).then(function (data) {
            win.document.title = 'View Email Source';
            var bodies = win.document.getElementsByTagName('body');
            var winBody;
            if (bodies.length) {
                winBody = bodies[0];
            } else {
                winBody = win.document.createElement('body');
                win.document.documentElement.appendChild(winBody);
            }
            winBody.style = 'font-family: "Lucida Console", Monaco, monospace; white-space: pre-wrap; font-size: 12px;';
            winBody.textContent = data.data;
        }, function (error) {
            win.close();
            modals.alert((error && error.status === 404) ? 'Message not found':
            'Error getting message. HTTP status code: ' + (error && error.status), {
                title: 'Error'
            });
        });
    }

    return {
        viewSourceButton: function (moduleName, entityType, entityId) {
            return '#' + JSON.stringify({
                    display: 'text',
                    text: 'View email source',
                    clickEvent: {
                        name: 'view-email-source',
                        moduleName: moduleName,
                        entityType: entityType,
                        entityId: entityId
                    },
                    'class': 'btn btn-blue btn-in-table small-btn display-block'
                });
        },
        viewRenderedButton: function (moduleName, entityType, entityId) {

            var tooltip;

            if (window.localStorage.getItem('view-email-rendered-no-confirmation')) {
                window.restoreViewEmailConfirmation = function () {
                    window.localStorage.removeItem('view-email-rendered-no-confirmation');
                    modals.alert('Confirmation dialog restored', {
                        title: 'Done'
                    });
                };
                var tooltipText = '<div class="warning-content">Remote content will be loaded allowing to track email view event';
                tooltipText += '<div><a href="javascript:restoreViewEmailConfirmation()">Click here to restore confirmation dialog with more info</a>';
                tooltipText += '</div>';
                tooltip = {
                    content: {
                        title: '<div class="danger-text"><span class="fa fa-exclamation-triangle yellow-text"></span>' +
                        'Warning!</div>',
                        text: tooltipText
                    },
                    position: {
                        at: 'top center',
                        my: 'bottom center',
                        viewport: true
                    },
                    hide: {
                        delay: 200,
                        fixed: true,
                        event: 'unfocus mouseleave click'
                    },
                    style: {
                        classes: 'qtip-bootstrap email-view-warning'
                    }
                };
            }

            return '#' + JSON.stringify({
                    display: 'text',
                    text: 'View email rendered',
                    clickEvent: {
                        name: 'view-email-rendered',
                        moduleName: moduleName,
                        entityType: entityType,
                        entityId: entityId
                    },
                    tooltip: tooltip,
                    'class': 'btn btn-blue btn-in-table small-btn display-block'
                });
        },
        viewSource: function (args) {
            showSource(args);
        },
        viewRendered: function (args) {
            var noConfirm = window.localStorage.getItem('view-email-rendered-no-confirmation');
            if (noConfirm) {
                return showRendered(args);
            }
            modals.confirm({
                title: 'Danger!!!',
                message: warningText,
                okText: 'View rendered Email',
                okClass: 'btn-danger',
                cancelClass: 'btn-success',
                remember: 'Don\'t show this warning message again'
            }).then(function (data) {
                if (data.remember) {
                    window.localStorage.setItem('view-email-rendered-no-confirmation', true);
                }
                showRendered(args);
            });
        }

    };
});