var m = angular.module('profiles');

m.factory('bucketContentLogic', function(widgetPolling, bucketDao, simpleSortableLogicCore) {
    return function($scope, logic, reinit) {
        var id = widgetPolling.scope().id;
        var simpleFree = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return bucketDao.content($scope.model.columns, id);
            }
        });

        return function() {
            simpleFree();
        };
    };
});