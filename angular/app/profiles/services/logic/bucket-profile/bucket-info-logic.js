var m = angular.module('profiles');

m.factory('bucketInfoLogic', function($q, bucketDao, mimeIcons, widgetPolling, modals, pathConverters) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        $q.all([
            bucketDao.retrieve(id),
            mimeIcons()
        ]).then(function(responses) {
            var entityInfo = responses[0];
            var icons = responses[1];
            $scope.model.lines = [
                {
                    "img": icons('folder'),
                    "class": "profile-image"
                }, {
                    "text": entityInfo.data[logic.titleColumn],
                    "class": "folder-name"
                }
            ];
            if (logic.parentIdsColumn && logic.parentTitlesColumn) {
                var parentIds = _([entityInfo.data[logic.parentIdsColumn]]).flatten();
                var parentTitles = _([entityInfo.data[logic.parentTitlesColumn]]).flatten();
                $scope.model.lines.push({
                    "text": pathConverters[logic.pathConverter && logic.pathConverter.type || 'list'](parentIds, parentTitles, logic.pathConverter && logic.pathConverter.args),
                    'class': 'folder-parents'
                });
            }

            _($scope.model.wrapper.buttons).find(function(button) {
                return button.id == 'show-meta-info';
            }).execute = function() {
                modals.widget({
                    panel: logic.metaInfo,
                    closeOnNavigate: true
                });
            };

        });
    };
});