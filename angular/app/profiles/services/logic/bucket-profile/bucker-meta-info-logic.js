var m = angular.module('profiles');

m.factory('bucketMetaInfoLogic', function(widgetPolling, bucketDao, metaInfoLogicCore) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;

        return metaInfoLogicCore($scope, logic, {
            advanced: logic.advanced,
            whiteList: logic.whiteList,
            blackList: logic.blackList,
            autoWidget: true,
            ordering: 'widgetViewOrder',
            entityType: 'bucket',
            retrieve: function() {
                return bucketDao.retrieve(id);
            }
        });
    };
});