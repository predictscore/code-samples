var m = angular.module('profiles');

m.factory('bucketPermissionLogic', function(widgetPolling, bucketDao, simpleSortableLogicCore) {
    return function($scope, logic, reinit) {
        var id = widgetPolling.scope().id;
        var simpleFree = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return bucketDao.permissions($scope.model.columns, id);
            }
        });

        return function() {
            simpleFree();
        };
    };
});