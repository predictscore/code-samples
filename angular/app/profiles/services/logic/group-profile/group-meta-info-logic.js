var m = angular.module('profiles');

m.factory('groupMetaInfoLogic', function(metaInfoLogicCore, widgetPolling, groupDao) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;
        var localType = widgetPolling.scope().localType || 'group';

        return metaInfoLogicCore($scope, logic, {
            blackList: logic.blackList,
            autoWidget: true,
            ordering: 'widgetViewOrder',
            entityType: localType,
            retrieve: function() {
                return groupDao.retrieve(id, localType);
            }
        });
    };
});