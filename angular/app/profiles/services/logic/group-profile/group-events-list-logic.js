var m = angular.module('profiles');

m.factory('groupEventsListLogic', function(widgetPolling, groupDao, eventsListLogicCore, eventsDao) {
    return function($scope, logic) {
        var groupId = widgetPolling.scope().id;

        return eventsListLogicCore($scope, logic, {
            retrieveTypes: function() {
                return eventsDao.retrieveTypes(logic.typesParams);
            },
            retrieve: function(after) {
                return groupDao.events($scope.model.columns, after, groupId);
            },
            timeColumnName: logic.timeColumnName
        });
    };
});