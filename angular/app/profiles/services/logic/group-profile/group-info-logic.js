var m = angular.module('profiles');

m.factory('groupInfoLogic', function(groupDao, userDao, widgetPolling, path, $q, modals) {
    function members(count) {
        if(count == 1) {
            return count + ' Member';
        }
        return count + ' Members';
    }

    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        var localType = widgetPolling.scope().localType || 'group';
        $q.all([
            groupDao.retrieve(id, localType),
            logic.disableMembership ? $q.when(null) : userDao.groupMembership(undefined, id, localType)
        ]).then(function(responses) {
            var group = responses[0].data;
            $scope.model.lines = [
                {
                    "img": path('profiles').img('group.png'),
                    "class": "group-image"
                }, {
                    "text": group[logic.nameColumn],
                    "class": "group-name"
                }, {
                    "text": group[logic.emailColumn],
                    "class": "group-email"
                }
            ];
            if(!logic.disableMembership) {
                var membership = responses[1].data;
                $scope.model.lines.push({
                    "text": members((membership.data || membership.rows || {}).length || 0),
                    "class": "group-members"
                });
            }

            var metaInfoButton =  _($scope.model.wrapper.buttons).find(function(button) {
                return button.id == 'show-meta-info';
            });
            if (metaInfoButton) {
                metaInfoButton.execute = function() {
                    modals.widget({
                        panel: logic.metaInfo,
                        closeOnNavigate: true
                    });
                };
            }
        });
    };
});