var m = angular.module('profiles');

m.factory('groupChannelsLogic', function(widgetPolling, groupDao, simpleSortableLogicCore) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;
        return simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return groupDao.groupChannels($scope.model.columns, id);
            }
        });
    };
});