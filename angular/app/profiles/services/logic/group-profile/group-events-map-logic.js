var m = angular.module('profiles');

m.factory('userIpEventsMapLogic', function(ipDao, widgetPolling, worldAccessMapLogicCore) {
    return function($scope, logic) {
        var userId = widgetPolling.scope().id;
        
        return worldAccessMapLogicCore($scope, logic, {
            retrieve: function(days) {
                return ipDao.worldAccess(days, {
                    userId: userId
                });
            }
        });
    };
});