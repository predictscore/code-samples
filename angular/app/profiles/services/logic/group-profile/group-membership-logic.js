var m = angular.module('profiles');

m.factory('groupMembershipLogic', function(widgetPolling, userDao, simpleSortableLogicCore) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;
        var localType = widgetPolling.scope().localType || 'group';
        return simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return userDao.groupMembership($scope.model.columns, id, localType);
            }
        });
    };
});