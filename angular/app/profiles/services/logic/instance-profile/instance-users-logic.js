var m = angular.module('profiles');

m.factory('instanceUsersLogic', function(widgetPolling, instanceDao, simpleSortableLogicCore) {
    return function($scope, logic, reinit) {
        var id = widgetPolling.scope().id;
        var simpleFree = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return instanceDao.users($scope.model.columns, id);
            }
        });

        return function() {
            simpleFree();
        };
    };
});