var m = angular.module('profiles');

m.factory('instanceInfoLogic', function($q, instanceDao, mimeIcons, widgetPolling, modals) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        $q.all([
            instanceDao.retrieve(id),
            mimeIcons()
        ]).then(function(responses) {
            var entityInfo = responses[0];
            var icons = responses[1];
            $scope.model.lines = [
                {
                    "img": icons('folder'),
                    "class": "profile-image"
                }, {
                    "text": entityInfo.data[logic.titleColumn],
                    "class": "folder-name"
                }
            ];
        });
    };
});