var m = angular.module('profiles');

m.factory('userMetaInfoLogic', function(metaInfoLogicCore, widgetPolling, userDao, troubleshooting) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;
        var localType = widgetPolling.scope().localType || 'user';

        var coreOptions = {
            blackList: _(['installed_apps'].concat(logic.blackList)).compact(),
            entityType: localType,
            autoWidget: true,
            ordering: 'widgetViewOrder',
            whiteList: logic.whiteList,
            advanced: logic.advanced,
            retrieve: function() {
                return userDao.retrieve(id, localType);
            }
        };

        if (troubleshooting.available()) {
            $scope.model.wrapper.filters = $scope.model.wrapper.filters || [];
            if (!_($scope.model.wrapper.filters).find(function (filter) {
                    return filter.value === 'troubleshooting';
                })) {
                $scope.model.wrapper.filters.push({
                    "type": "flag",
                    "text": "Troubleshooting Mode",
                    "value": "troubleshooting",
                    "local": true
                });
            }
            coreOptions.processParams = function (params) {
                if (params && params.troubleshooting) {
                    delete coreOptions.whiteList;
                    coreOptions.blackList = logic.extendedData || [];
                    coreOptions.rawOutput = true;
                    delete coreOptions.autoWidget;
                    coreOptions.ordering = 'key';
                } else {
                    coreOptions.whiteList = logic.whiteList;
                    coreOptions.blackList = _(['installed_apps'].concat(logic.blackList)).compact();
                    coreOptions.autoWidget = true;
                    coreOptions.ordering = 'widgetViewOrder';
                    delete coreOptions.rawOutput;
                }
            };

        }

        return metaInfoLogicCore($scope, logic, coreOptions);
    };
});
