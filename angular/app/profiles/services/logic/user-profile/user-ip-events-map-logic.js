var m = angular.module('profiles');

m.factory('groupEventsMapLogic', function(ipDao, widgetPolling, worldAccessMapLogicCore) {
    return function($scope, logic) {
        var groupId = widgetPolling.scope().id;
        
        return worldAccessMapLogicCore($scope, logic, {
            retrieve: function(days) {
                return ipDao.worldAccess(days, {
                    groupId: groupId
                });
            }
        });
    };
});