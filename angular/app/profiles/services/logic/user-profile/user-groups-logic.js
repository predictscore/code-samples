var m = angular.module('profiles');

m.factory('userGroupsLogic', function(userDao, widgetPolling, simpleLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        var localType = widgetPolling.scope().localType || 'user';
        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return userDao.userGroups($scope.model.columns, id, localType);
            }
        });
    };
});