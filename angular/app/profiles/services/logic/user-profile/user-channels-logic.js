var m = angular.module('profiles');

m.factory('userChannelsLogic', function(userDao, widgetPolling, simpleLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return userDao.userChannels($scope.model.columns, id);
            }
        });
    };
});