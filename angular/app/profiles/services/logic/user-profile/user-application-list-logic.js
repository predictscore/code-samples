var m = angular.module('profiles');

m.factory('userApplicationListLogic', function(userDao, widgetPolling, simpleLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return userDao.applicationList($scope.model.columns, id);
            }
        });
    };
});