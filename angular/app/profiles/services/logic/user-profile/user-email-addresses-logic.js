var m = angular.module('profiles');

m.factory('userEmailAddressesLogic', function(userDao, widgetPolling, simpleLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return userDao.userEmailAddresses($scope.model.columns, id).order('entity_id', 'asc');
            }
        });
    };
});