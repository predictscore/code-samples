var m = angular.module('dashboard');

m.factory('userTopCollaboratorsPlotLogic', function(userDao, widgetPolling, verticalPlotLogicCore, widgetSettings, $q) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;

        var destroyPlot = _.noop;
        function loadData() {
            var params = widgetSettings.params($scope.model.uuid);
            destroyPlot();
            destroyPlot = verticalPlotLogicCore($scope, logic, {
                retrieve: function() {
                    return userDao.topUserCollaborators(id).pagination(0, logic.lines).order('count', 'desc')
                        .converter(function (input) {
                            input.data.rows = _(input.data.rows).filter(function (row) {
                                return (row.is_external_user && logic.showExternal) ||
                                    (!row.is_external_user && logic.showInternal);
                            });
                            return $q.when(input);
                        });
                },
                idColumn: 'entity_id',
                makeLabel: function (row) {
                    return row.display_name;
                },
                linkTo: 'user'
            });
        }

        var paramsWatch = $scope.$watch(function() {
            return widgetSettings.params($scope.model.uuid);
        }, function(params) {
            loadData();
        }, true);

        return function() {
            paramsWatch();
            destroyPlot();
        };
    };
});
