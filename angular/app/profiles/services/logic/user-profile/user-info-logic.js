var m = angular.module('profiles');

m.factory('userInfoLogic', function(userDao, widgetPolling, path, modals) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        var localType = widgetPolling.scope().localType;
        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }
        userDao.retrieve(id, localType).then(function(response) {
            $scope.model.wrapper.isLoading = false;
            var photoUrl = path('profiles').img('user.png');
            if(_(response.data[logic.imgColumn]).isString()) {
                photoUrl = response.data[logic.imgColumn];
            }
            $scope.model.lines = [];
            $scope.model.lines.push({
                "img": photoUrl,
                "defaultImg": path('profiles').img('user.png'),
                "class": "profile-image"
            });
            if (logic.suspendedColumn && response.data[logic.suspendedColumn]) {
                $scope.model.lines.push({
                    "text": "suspended",
                    "class": "user-suspended"
                });
            }
            $scope.model.lines.push({
                "text": response.data[logic.nameColumn],
                "class": "user-name"
            });
            $scope.model.lines.push({
                "text": response.data[logic.emailColumn],
                "class": "user-email"
            });

            if (logic.instanceColumn && response.data[logic.instanceColumn]) {
                $scope.model.lines.push({
                    "class": "user-instance",
                    "text": "Instance: #" + JSON.stringify({
                        "type": "instance",
                        "id": response.data[logic.instanceColumn],
                        "text": response.data[logic.instanceColumn]
                    })
                });
            }

            var metaInfoButton = _($scope.model.wrapper.buttons).find(function(button) {
                return button.id == 'show-meta-info';
            });
            if (metaInfoButton) {
                metaInfoButton.execute = function() {
                    modals.widget({
                        panel: logic.metaInfo,
                        closeOnNavigate: true
                    });
                };
            }
        });
    };
});
