var m = angular.module('profiles');

m.factory('userFileShareTypesLogic', function(fileDao, widgetPolling, userFileShareTypesLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;

        return userFileShareTypesLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.types(id);
            }
        });
    };
});
