var m = angular.module('profiles');

m.factory('folderEventsMapLogic', function(ipDao, widgetPolling, worldAccessMapLogicCore) {
    return function($scope, logic) {
        var folderId = widgetPolling.scope().id;
        
        return worldAccessMapLogicCore($scope, logic, {
            retrieve: function(days) {
                return ipDao.worldAccess(days, {
                    folderId: folderId
                });
            }
        });
    };
});