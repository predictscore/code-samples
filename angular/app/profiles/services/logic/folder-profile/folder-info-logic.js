var m = angular.module('profiles');

m.factory('folderInfoLogic', function(widgetPolling, mimeIcons, $filter, $q, folderDao, pathConverters, modals) {
    return function($scope, logic, reinit) {
        var id = widgetPolling.scope().id;

        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }


        var onActionUnsubscribe = widgetPolling.on('remediationActionPerformed', function () {
            logic.updateTick = 2000;
            logic.restoreUpdateTickAfter = moment().add(1,'minutes');
            reinit();
        });

        if (logic.restoreUpdateTickAfter && logic.restoreUpdateTickAfter.isBefore(moment())) {
            logic.updateTick = logic.originalUpdateTick;
            logic.restoreUpdateTickAfter = null;
        }

        $q.all([
            folderDao.retrieve(id, logic.viewLinkConverter),
            mimeIcons()
        ]).then(function(responses) {
            $scope.model.wrapper.isLoading = false;
            var folderInfo = responses[0];
            var icons = responses[1];

            var parentIds = _([folderInfo.data[logic.parentIdsColumn]]).flatten();
            var parentTitles = _([folderInfo.data[logic.parentTitlesColumn]]).flatten();

            $scope.model.lines = [
                {
                    "img": icons(folderInfo.data[logic.mimeTypeColumn] || 'folder'),
                    "class": "profile-image"
                }, {
                    "text": folderInfo.data[logic.titleColumn] || logic.noTitle || '',
                    "class": "folder-name"
                }, {
                    "text": pathConverters[logic.pathConverter && logic.pathConverter.type || 'list'](parentIds, parentTitles, logic.pathConverter && logic.pathConverter.args),
                    'class': 'folder-parents'
                }
            ];

            if(!_(folderInfo.data[logic.sizeColumn]).isUndefined()) {
                $scope.model.lines.push({
                    "text": $filter('bytes')(folderInfo.data[logic.sizeColumn]),
                    "class": "folder-size"
                });
            }

            if(!logic.isDeletedColumn || !folderInfo.data[logic.isDeletedColumn]) {
                if (!_(folderInfo.data[logic.linkColumn]).isUndefined()) {
                    $scope.model.lines.push({
                        "text": '#' + JSON.stringify({
                            display: 'external-link',
                            text: logic.linkColumnTitle || 'View folder',
                            href: folderInfo.data[logic.linkColumn]
                        }),
                        "class": "folder-link"
                    });
                }
            }

            if(logic.isDeletedColumn && folderInfo.data[logic.isDeletedColumn]) {
                $scope.model.lines.push({
                    display: 'text',
                    'text': 'Deleted',
                    'class': 'folder-deleted'
                });
            }

            _(logic.widgets).each(function (widget) {
                $scope.model.lines.push({
                    "widget": widget
                });
            });

            _($scope.model.wrapper.buttons).find(function(button) {
                return button.id == 'show-meta-info';
            }).execute = function() {
                modals.widget({
                    panel: logic.metaInfo,
                    closeOnNavigate: true
                });
            };
        });
        return function() {
            onActionUnsubscribe();
        };
    };
});