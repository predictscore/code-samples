var m = angular.module('profiles');

m.factory('folderEventsListLogic', function(widgetPolling, folderDao, eventsListLogicCore, eventsDao) {
    return function($scope, logic) {
        var folderId = widgetPolling.scope().id;

        return eventsListLogicCore($scope, logic, {
            retrieveTypes: function() {
                return eventsDao.retrieveTypes(logic.typesParams);
            },
            retrieve: function(after) {
                return folderDao.events($scope.model.columns, after, folderId);
            },
            timeColumnName: logic.timeColumnName
        });
    };
});