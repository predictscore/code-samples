var m = angular.module('profiles');

m.factory('folderBrowserLogic', function(widgetPolling, folderDao, simpleSortableLogicCore) {
    return function($scope, logic) {
        var folderId = widgetPolling.scope().id;

        var sortingOptions = logic.customOrderingOptions;

        function reSort(column) {
            var orderingObj = $scope.model.customOrdering;
            var options = _(sortingOptions).find(function (item) {
                return item.column === column;
            });
            if (_(options).isUndefined()) {
                return;
            }
            if (orderingObj.columnName === column) {
                orderingObj.order = (orderingObj.order === 'asc' ? 'desc': 'asc');
            } else {
                orderingObj.columnName = column;
                orderingObj.order = options.defaultOrder || 'asc';
            }
            processCustomOrdering();
        }

        function processCustomOrdering() {
            updateSortButton();
            updateSortParams();
        }

        function updateSortButton() {
            var orderingObj = $scope.model.customOrdering;
            var sortButton = $scope.model.wrapper.buttons[0];
            var optionOffset = 1;
            _(sortingOptions).each(function (opts, idx) {
                if (orderingObj.columnName === opts.column) {
                    sortButton.options[idx + optionOffset].label = '#' + JSON.stringify({
                            display: 'text',
                            'class': 'widget-button-option-sort-mark fa ' + (orderingObj.order === 'asc' ? 'fa-sort-asc' : 'fa-sort-desc')
                        });
                } else {
                    sortButton.options[idx + optionOffset].label = '#' + JSON.stringify({
                            display: 'text',
                            'class': 'widget-button-option-sort-mark'
                        });
                }
                sortButton.options[idx + optionOffset].label += '#' + JSON.stringify({
                        display: 'text',
                        text: opts.label
                    });
            });
        }

        function updateSortParams() {
            var orderingObj = $scope.model.customOrdering;
            var cols = ['entity_type'];
            var ords = ['desc'];
            if (orderingObj.columnName) {
                cols.push(orderingObj.columnName);
                ords.push(orderingObj.order || 'asc');
            }
            _(logic.orderAfter).each(function (order) {
                cols.push(order.column);
                ords.push(order.order);
            });
            $scope.model.options.pagination.ordering.columnName = cols.join(',');
            $scope.model.options.pagination.ordering.order = ords.join(',');
            $scope.model.options.pagination.page = 1;
        }

        $scope.model.wrapper.buttons = [{
            "class": "btn-icon",
            "iconClass": "fa fa-cogs",
            "position": "main",
            "options": [{
                label: '#' + JSON.stringify({
                    display: 'text',
                    text: 'Sort by'
                }),
                noEvents: true
            }].concat(_(sortingOptions).map(function (opts) {
                return {
                    label: opts.label,
                    forceClose: true,
                    execute: function (event) {
                        reSort(opts.column);
                        return true;
                    }
                };
            }))
        }];


        if (!$scope.model.options) {
            $scope.model.options = {};
        }

        if (!$scope.model.options.pagination) {
            $scope.model.options.pagination = {
                page: 1
            };
        }

        $scope.model.options.pagination.ordering = {};
        $scope.model.customOrdering = {
            columnName: $scope.options.defaultOrdering.columnName,
            order: $scope.options.defaultOrdering.order
        };
        processCustomOrdering();

        var simpleFree = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return folderDao.tableContent($scope.model.columns, folderId);
            }
        });
        return function() {
            simpleFree();
        };

    };
});