var m = angular.module('profiles');

m.factory('folderMetaInfoLogic', function(widgetPolling, folderDao, metaInfoLogicCore) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;

        return metaInfoLogicCore($scope, logic, {
            advanced: logic.advanced,
            whiteList: logic.whiteList,
            blackList: logic.blackList,
            autoWidget: true,
            ordering: 'widgetViewOrder',
            entityType: 'folder',
            retrieve: function() {
                return folderDao.retrieve(id);
            }
        });
    };
});