var m = angular.module('profiles');

m.factory('avCiscoScanDetailsLogic', function(defaultListConverter, columnsHelper, $filter) {

    function sortingNumber(text) {
        var prefixLength = 32;
        var postfixLength = 8;
        var result = '' + text;
        if (result.indexOf('.') == -1) {
            result += '.';
        }
        while(result.indexOf('.') < prefixLength) {
            result = '0' + result;
        }
        while (result.length < prefixLength + 1 + postfixLength) {
            result += '0';
        }
        return result;
    }

    function infoWidget () {
        return {
            "type": "meta-info",
            "size": "margin-bottom-20 col-lg-12 col-md-12 col-sm-12 col-xs-12",
            "class": "light",
            "wrapper": {
                "type": "none"
            },
            "pair": {
                "class": "default-scan-details"
            }
        };
    }

    function indicatorsWidget () {
        return {
            "id": "indicators-widget",
            "type": "data-table",
            "size": "large",
            "class": "light",
            "wrapper": {
                "type": "widget-panel",
                "title": "Behavioral indicators",
                "class": "light white-body white-header"
            },
            "bodyStyle": {
                "border-bottom": "none"
            },
            "columns": [{
                "id": "originalIndex",
                "hidden": true
            }, {
                "id": "level",
                "text": "Level",
                "sortable": true,
                "orderMap": {
                    "Very High": 4,
                    "High": 3,
                    "Medium": 2
                },
                "orderMapMissing": 1
            }, {
                "id": "signatures",
                "text": "Signatures",
                "sortable": true
            }, {
                "id": "factor",
                "text": "Factor",
                "sortable": true
            }],
            "options": {
                "pagination": {
                    "ordering": {
                        "columnName": "level",
                        "order": "desc",
                        "column": 1
                    }
                }
            }
        };
    }

    function updateWidgets($scope) {
        $scope.model.widgets = [];
        $scope.model.widgets.push($scope.infoWidget);
        if ($scope.indicatorsWidget.data && $scope.indicatorsWidget.data.length) {
            $scope.model.widgets.push($scope.indicatorsWidget);
        }
    }

    return function($scope, logic) {

        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }

        if (!$scope.infoWidget) {
            $scope.infoWidget = infoWidget();
            $scope.infoWidget.logic = angular.copy(logic);
            $scope.infoWidget.logic.name = 'defaultScanDetailsLogic';
            $scope.infoWidget.uuid = $scope.model.uuid;
            $scope.infoWidget.modalWrapper = $scope.model.wrapper;
        }
        $scope.indicatorsWidget = $scope.indicatorsWidget || indicatorsWidget();
        updateWidgets($scope);

        var filtersWatch = $scope.$watch('infoWidget.wrapper.filters', function () {
            $scope.model.wrapper.filters = $scope.infoWidget.wrapper.filters;
        });

        var dataWatch = $scope.$watch('infoWidget.entityData', function (data) {
            if (data || $scope.infoWidget.entityInfo) {
                $scope.model.wrapper.isLoading = false;
            }

            if (data && data.behavioral_indicators && data.behavioral_indicators.indicators) {
                defaultListConverter({
                    data: {
                        rows: _(data.behavioral_indicators.indicators).map(function (row, idx) {
                            return $.extend({
                                originalIndex: idx
                            }, row);
                        })
                    }
                }, {
                    columns: $scope.indicatorsWidget.columns
                }).then(function (input) {
                    $scope.indicatorsWidget.data = input.data && input.data.data;
                    orderIndicatorsData();
                    updateWidgets($scope);
                });
            } else {
                delete $scope.indicatorsWidget.data;
            }

            updateWidgets($scope);
        });

        function orderIndicatorsData() {
            var ordering = $scope.indicatorsWidget.options.pagination.ordering;
            if ((!ordering || !ordering.columnName) && $scope.indicatorsWidget.options.defaultOrdering) {
                ordering = $scope.indicatorsWidget.options.defaultOrdering;
            }
            var orderIdx = 0;
            var orderingColumnName =  (ordering && ordering.columnName);
            if (orderingColumnName) {
                orderIdx = columnsHelper.getIdxById($scope.indicatorsWidget.columns, orderingColumnName);
            }
            var columnInfo = $scope.indicatorsWidget.columns[orderIdx];
            if ($scope.indicatorsWidget.data) {

                $scope.indicatorsWidget.data = $filter('orderBy')($scope.indicatorsWidget.data, function (val) {
                    var orderPrefix = val[orderIdx].text;

                    if (columnInfo && columnInfo.orderMap) {
                        if (!_(columnInfo.orderMap[val[orderIdx].text]).isUndefined()) {
                            orderPrefix = columnInfo.orderMap[val[orderIdx].text];
                        } else {
                            if (columnInfo.orderMapMissing) {
                                orderPrefix = columnInfo.orderMapMissing;
                            }
                        }
                    }
                    if (_(orderPrefix).isNumber()) {
                        orderPrefix = sortingNumber(orderPrefix);
                    }
                    if(_(orderPrefix).isNull() || _(orderPrefix).isUndefined()) {
                        orderPrefix = '~';
                    }
                    if(_(orderPrefix).isArray()) {
                        orderPrefix = orderPrefix.join('/');
                    }
                    orderPrefix = orderPrefix + '-';

                    return orderPrefix + sortingNumber(val[0].text);
                }, ordering && ordering.order === 'desc' || false);
            }
        }

        var orderingWatch = $scope.$watch('indicatorsWidget.options.pagination.ordering', orderIndicatorsData, true);
        logic.updateTick = null;

        return function () {
            filtersWatch();
            dataWatch();
            orderingWatch();
        };
    };
});