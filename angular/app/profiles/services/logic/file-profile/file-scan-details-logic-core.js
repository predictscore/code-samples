var m = angular.module('profiles');

m.factory('fileScanDetailsLogicCore', function(fileDao, simpleLogicCore, modulesManager, $q, troubleshooting, modulesDao) {

    var appVisibleColumnsByConfig = {
        "dlp_globalvelocity": {
            "component_name": {
                "show_found_text": true
            }
        },
        "dlp_symantec": {
            "found_text": {
                "show_found_text": true
            }
        },
        "dlp_checkpoint": {
            "found_text": {
                "show_found_text": true
            }
        },
        "dlp_mcafee": {
            "found_text": {
                "show_found_text": true
            }
        },
        "dlp_websense": {
            "found_text": {
                "show_found_text": true
            }
        },
        "dlp_gtb": {
            "found_text": {
                "show_found_text": true
            }
        }
    };


    return function ($scope, logic, coreOptions) {
        var simpleFree = _.noop;
        $scope.originalSize = $scope.originalSize || $scope.model.size;
        $scope.originalType = $scope.originalType || $scope.model.type;
        $scope.originalClasses = $scope.originalClasses || $scope.model.wrapper.class;

        function isActive() {
            if(logic.app) {
                return modulesManager.isActive(logic.app);
            } else if(logic.module) {
                return modulesManager.isModuleActive(logic.module);
            }
            return false;
        }

        var coreLogic = coreOptions.coreLogic || simpleLogicCore;

        function loadData() {
            var appActive = isActive();
            if(appActive || logic.forceInactive) {
                simpleFree();
                var options = _(coreOptions).omit('retrieve');
                options.retrieve = function() {
                    var deferred = $q.defer();
                    var retrieve = coreOptions.retrieve();
                    if (!troubleshooting.enabled() && options.moduleLabels && options.blackList && retrieve.converter) {
                        retrieve = retrieve.preFetch(fileDao.detailsHidden(options.moduleLabels.module, options.moduleLabels.entity), "detailsHidden")
                            .converter(function (input, args) {
                                if (args.detailsHidden && args.detailsHidden.data && args.detailsHidden.data.length) {
                                    options.blackList = _.union(options.blackList, args.detailsHidden.data);
                                }
                                return $q.when(input);
                            }, _.identity);
                    }
                    if (!options.rawOutput && retrieve.converter && options.moduleLabels && appVisibleColumnsByConfig[options.moduleLabels.module]) {
                        retrieve = retrieve.preFetch(modulesDao.retrieveConfig(options.moduleLabels.module), 'appConfig')
                            .converter(function (input, args) {
                                _(appVisibleColumnsByConfig[options.moduleLabels.module]).each(function (params, field) {
                                    if (!args.appConfig.data || _(params).find(function (value, param) {
                                            return _(args.appConfig.data[param]).isUndefined() ||
                                                args.appConfig.data[param].value !== value;
                                        })
                                    ) {
                                        delete input.data[field];
                                    }
                                });
                                return $q.when(input);
                            }, _.identity);
                    }
                    retrieve.then(function(response) {
                        $scope.model.type = $scope.originalType;
                        deferred.resolve(response);
                    }, function() {
                        $scope.model.type = 'info';
                        $scope.model.lines = [{
                            text: 'File was not scanned'
                        }];
                        deferred.reject();
                    });
                    return deferred.promise;
                };
                simpleFree = coreLogic($scope, logic, options);
                $scope.model.size = $scope.originalSize;
            } else {
                $scope.model.size = 'display-none';
            }
        }

        var isActiveWatch = $scope.$watch(isActive, function(newVal, oldVal) {
            loadData();
        });

        return function() {
            simpleFree();
            isActiveWatch();
        };
    };
});