var m = angular.module('profiles');

m.factory('fileMetaInfoLogic', function(widgetPolling, fileDao, metaInfoLogicCore, modals, modulesManager, entityTypes, troubleshooting, $rootScope, recheckParams, submitForAnalysisParams, suspiciousReportParams) {

    function recheckFile(entityType, entityId) {
        return recheckParams.show('Do you want to recheck this file?', entityType).then(function (data) {
            return fileDao.recheck(entityType, entityId, data.selectApplications ? data.apps : undefined);
        }).then(function () {
            $rootScope.$broadcast('file-security-stack-reload');
        });
    }

    function submitForAnalysis(entityType, entityId) {
        return submitForAnalysisParams.show('Submit file for analysis', 'file').then(function (data) {
            return fileDao.shareForAnalysis(entityType, entityId, 'file', data);
        });
    }

    function suspiciousReport(entityType, entityId) {
        return suspiciousReportParams.show('Report this file', entityType, entityId, {
            objectTypesData: widgetPolling.scope().objectTypes && widgetPolling.scope().objectTypes.data
        }).then(function (data) {
            return fileDao.suspiciousReport(entityType, entityId, data);
        });
    }

    return function($scope, logic) {
        var id = widgetPolling.scope().id;
        var buttons = [{
            label: 'Recheck',
            'class': 'btn btn-blue',
            onClick: function () {
                return recheckFile(entityTypes.toBackend(modulesManager.currentModule(), 'file'), id);
            },
            advanced: true
        }, {
            label: 'Submit for analysis',
            'class': 'btn btn-blue',
            onClick: function () {
                return submitForAnalysis(entityTypes.toBackend(modulesManager.currentModule(), 'file'), id);
            },
            advanced: true
        }];

        if (troubleshooting.entityDebugEnabled()) {
            buttons.push({
                label: 'Report this file',
                'class': 'btn btn-blue',
                onClick: function () {
                    return suspiciousReport(entityTypes.toBackend(modulesManager.currentModule(), 'file'), id);
                },
                advanced: true
            });
        }

        var coreOptions = {
            advanced: logic.advanced,
            whiteList: logic.whiteList,
            blackList: logic.blackList,
            autoWidget: true,
            ordering: 'widgetViewOrder',
            entityType: 'file',
            retrieve: function() {
                return fileDao.retrieve(id);
            },
            buttons: buttons
        };
        if (troubleshooting.available()) {
            var filterFound = false;
            var filters = _($scope.model.wrapper.filters).map(function (filter) {
                if (filter.value === 'troubleshooting') {
                    filterFound = true;
                }
                return filter;
            });
            if (!filterFound) {
                filters.push({
                    "type": "flag",
                    "text": "Troubleshooting Mode",
                    "value": "troubleshooting",
                    "local": true
                });
                $scope.model.wrapper.filters = filters;
            }
            coreOptions.processParams = function (params) {
                if (params && params.troubleshooting) {
                    delete coreOptions.whiteList;
                    delete coreOptions.autoWidget;
                    coreOptions.blackList = logic.extendedData || [];
                    coreOptions.rawOutput = true;
                    coreOptions.ordering = 'key';
                } else {
                    coreOptions.whiteList = logic.whiteList;
                    coreOptions.blackList = logic.blackList;
                    coreOptions.autoWidget = true;
                    coreOptions.ordering = 'widgetViewOrder';
                    delete coreOptions.rawOutput;
                }
            };
        }
        return metaInfoLogicCore($scope, logic, coreOptions);
    };
});
