var m = angular.module('profiles');

m.factory('fileInfoLogic', function(widgetPolling, fileDao, mimeIcons, $filter, $q, pathConverters, modals, mainQueueTroubleshootingHelper, quarantineStatusConverter, deletedStatusHelper, modulesDao, modulesManager, entityTypes) {

    return function($scope, logic, reinit) {
        var id = widgetPolling.scope().id;

        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }

        var onActionUnsubscribe = widgetPolling.on('remediationActionPerformed', function () {
            logic.updateTick = 2000;
            logic.restoreUpdateTickAfter = moment().add(1,'minutes');
            reinit();
        });

        if (logic.restoreUpdateTickAfter && logic.restoreUpdateTickAfter.isBefore(moment())) {
            logic.updateTick = logic.originalUpdateTick;
            logic.restoreUpdateTickAfter = null;
        }
        var queries = [
            fileDao.retrieve(id, logic.viewLinkConverter),
            mimeIcons()
        ];
        var moduleConfig;
        if (logic.loadModuleConfig) {
            queries.push(modulesDao.retrieveConfig(modulesManager.currentModule()).then(function (response) {
                moduleConfig = response.data;
            }));
        }

        var cpCapsuleInfo;
        if (logic.loadCpCapsuleInfo) {
            queries.push(fileDao.cpCapsuleInfo(id, entityTypes.toBackend(modulesManager.currentModule(), 'file')).then(function (response) {
                cpCapsuleInfo = response.data;
            }));
        }


        $q.all(queries).then(function(responses) {
            $scope.model.wrapper.isLoading = false;
            var fileInfo = responses[0];
            var icons = responses[1];
            fileInfo.data.moduleConfig = moduleConfig;
            fileInfo.data.cpCapsuleInfo = cpCapsuleInfo;

            var parentIds = _([fileInfo.data[logic.parentIdsColumn]]).flatten();
            var parentTitles = _([fileInfo.data[logic.parentTitlesColumn]]).flatten();

            var oldWidgetLines = _($scope.model.lines).filter(function (line) {
                return line.widget;
            });


            $scope.model.lines = [
                {
                    "img": icons((logic.mimeTypeColumns && _(fileInfo.data).chain().pick(logic.mimeTypeColumns).values().value()) || fileInfo.data[logic.mimeTypeColumn]),
                    "class": "profile-image"
                }, {
                    "text": fileInfo.data[logic.titleColumn],
                    "class": "file-name"
                }, {
                    "text": logic.sizeColumn ? $filter('bytes')(fileInfo.data[logic.sizeColumn]) : '',
                    "class": "file-size"
                }, {
                    "text": pathConverters[logic.pathConverter && logic.pathConverter.type || 'list'](parentIds, parentTitles, logic.pathConverter && logic.pathConverter.args),
                    'class': 'file-parents'
                }
            ];

            if (!_(fileInfo.data[logic.parentUserIdColumn]).isUndefined()) {
                $scope.model.lines.push({
                    'text': '#' + JSON.stringify({
                        display: 'link',
                        text: fileInfo.data[logic.parentUserTitleColumn],
                        type: 'user',
                        id: fileInfo.data[logic.parentUserIdColumn]
                    }),
                    'class': 'file-parents'
                });
            }
            
            if(!_(fileInfo.data[logic.fileTypeColumn]).isUndefined()) {
                $scope.model.lines.push({
                    display: 'text',
                    'text': 'Type: ' + fileInfo.data[logic.fileTypeColumn],
                    'class': 'file-type'
                });
            }
            
            if (fileInfo.data.av_fd_encrypted) {
                $scope.model.lines.push({
                    "class": "file-encrypted-icon",
                    "text": '#' + JSON.stringify({
                        display: 'text',
                        tooltip: 'File is encrypted',
                        "class": "fa fa-lock"
                    })
                });
            }

            if (!logic.isDeletedColumn || !fileInfo.data[logic.isDeletedColumn]) {
                if (logic.staticViewLink) {
                    $scope.model.lines.push({
                        "text": '#' + JSON.stringify({
                            display: 'external-link',
                            text: 'View file',
                            href: logic.staticViewLink
                        }),
                        "class": "file-link"
                    });
                } else {
                    if(_(fileInfo.data[logic.linkColumn]).isString()) {
                        $scope.model.lines.push({
                            "text": '#' + JSON.stringify({
                                display: 'external-link',
                                text: logic.linkColumnTitle || 'View file',
                                href: fileInfo.data[logic.linkColumn]
                            }),
                            "class": "file-link"
                        });
                    }
                }
            }

            if(logic.isDeletedColumn && fileInfo.data[logic.isDeletedColumn]) {
                if (!logic.deletedStatusEntity || deletedStatusHelper.isVisible(logic.deletedStatusEntity, fileInfo.data)) {
                    $scope.model.lines.push({
                        display: 'text',
                        'text': 'Deleted',
                        'class': 'file-deleted'
                    });
                }
            }

            if(!_(logic.emailColumn).isUndefined() && !_(logic.emailIdColumn).isUndefined()) {
                $scope.model.lines.push({
                    "text": 'Attached to #' + JSON.stringify({
                        display: 'link',
                        text: fileInfo.data[logic.emailColumn],
                        type: 'email',
                        id: fileInfo.data[logic.emailIdColumn],
                        emptyLinkText: 'Missing subject - click here for the email'
                    }),
                    "class": "email-link"
                });
            }

            if(!_(logic.emailSentColumn).isUndefined()) {
                $scope.model.lines.push({
                    "text": 'Email sent at ' + $filter('localTime')(fileInfo.data[logic.emailSentColumn]),
                    "class": "email-sent"
                });
            }

            if(!_(logic.emailReceivedColumn).isUndefined()) {
                $scope.model.lines.push({
                    "text": 'Email received at ' + $filter('localTime')(fileInfo.data[logic.emailReceivedColumn]),
                    "class": "email-sent"
                });
            }
            if(!_(logic.emailDeletedColumn).isUndefined() && fileInfo.data[logic.emailDeletedColumn]) {
                $scope.model.lines.push({
                    "text": 'Email Deleted',
                    "class": "email-deleted"
                });
            }

            if (logic.quarantineStatusEntity) {
                var quarantineMsg = quarantineStatusConverter(logic.quarantineStatusEntity, fileInfo.data);
                if (quarantineMsg) {
                    $scope.model.lines.push({
                        "text": quarantineMsg,
                        "class": "quarantined"
                    });
                }
            }

            if(!_(logic.isQuarantineColumn).isUndefined() && fileInfo.data[logic.isQuarantineColumn]) {
                if(!_(logic.isRestoredColumn).isUndefined() && fileInfo.data[logic.isRestoredColumn]) {
                    $scope.model.lines.push({
                        "text": logic.restoredMessage || 'This is the original file',
                        "class": "quarantined"
                    });
                } else {
                    $scope.model.lines.push({
                        "text": logic.quarantinedMessage || 'This file was quarantined',
                        "class": "quarantined"
                    });
                }
            }

            if(!_(logic.originalAttachmentIdColumn).isUndefined() && fileInfo.data[logic.originalAttachmentIdColumn]) {
                $scope.model.lines.push({
                    "text": '#' + JSON.stringify({
                        display: 'link',
                        text: "Original Attachment",
                        type: 'file',
                        id: fileInfo.data[logic.originalAttachmentIdColumn]
                    }),
                    "class": "email-link"
                });
            }

            if(!_(logic.originalEmailIdColumn).isUndefined() && fileInfo.data[logic.originalEmailIdColumn]) {
                $scope.model.lines.push({
                    "text": '#' + JSON.stringify({
                        display: 'link',
                        text: "Original Email",
                        type: 'email',
                        id: fileInfo.data[logic.originalEmailIdColumn]
                    }),
                    "class": "email-link"
                });
            }



            _(logic.widgets).each(function (widget) {
                widget.entityData = fileInfo.data;
                var line = _(oldWidgetLines).find(function (line) {
                    return line.widget === widget;
                }) || {
                        "widget": widget
                    };
                $scope.model.lines.push(line);
            });

            _($scope.model.wrapper.buttons).find(function(button) {
                return button.id == 'show-meta-info';
            }).execute = function() {
                modals.widget({
                    panel: logic.metaInfo,
                    closeOnNavigate: true
                });
            };
            mainQueueTroubleshootingHelper.addTroubleshootingButton($scope.model.wrapper);
        });

        return function() {
            onActionUnsubscribe();
        };
    };
});
