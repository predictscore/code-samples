var m = angular.module('profiles');

m.factory('debugEntityLogic', function($q, fileScanDetailsLogicCore, fileDao, widgetPolling, metaInfoLogicCore, troubleshooting, fileSecurityDebugHelper, entityTypes, modals, emailDao, limitedArrayConverter) {

    function showSecurityInfo(id, type, noConfirm) {
        var p = $q.when();
        if (!noConfirm) {
            var localEntityType = entityTypes.toLocal(type);
            if (localEntityType !== 'file' && localEntityType !== 'email') {
                p = modals.confirm({
                    message: 'Selected entity type "' + type + '" is not known file or email type entity. Security info usually exists for files or emails. Are you sure you want to continue?',
                    okText: 'Continue',
                    title: 'Please check selected entity type'
                });
            }
        }
        return p.then(function() {
            return fileSecurityDebugHelper.showSecEntities(id, type);
        });
    }

    return function($scope, logic) {
        $scope.model.entityId = widgetPolling.scope().id;
        $scope.model.preReleaseId = widgetPolling.scope().preReleaseId;
        if (logic.usePollingEntityType) {
            $scope.model.entityType = widgetPolling.scope().entityType;
        }

        if(troubleshooting.entityDebugEnabled()) {
            var objectTypes = widgetPolling.scope().objectTypes || $scope.model.objectTypes;
            $scope.loadEntityInfo = function(customEntityType) {
                $scope.entityData = [];
                $scope.ftpUploadData = false;
                var entityType = customEntityType || $scope.entityDebug.type;
                var entityId = $scope.entityDebug.id;
                fileDao.entityInfo(entityType, entityId).then(function(response) {
                    var localEntityType = entityTypes.toLocal(entityType);
                    if (localEntityType === 'file') {
                        $scope.ftpUploadData = {
                            type: entityType,
                            id: entityId
                        };
                    }
                    $scope.entityData = _(response.data).map(function(value, key) {
                        return {
                            key: key,
                            value: _(value).isObject() ? JSON.stringify(value) : ('' + value)
                        };
                    });
                }, function(error) {
                    if (error.status === 404) {
                        $scope.entityData = [{
                            value: 'Entity not found'
                        }];
                    }
                    else {
                        $scope.entityData = [{
                            value: error.data && error.data.message || 'Error loading entity'
                        }];
                    }
                });
            };

            $scope.entitySecurityInfo = function() {
                return showSecurityInfo($scope.entityDebug.id, $scope.entityDebug.type, logic.noSecurityInfoConfirmation);
            };

            $scope.showOriginalSecurityInfo = function () {
                return showSecurityInfo($scope.model.preReleaseId, $scope.entityDebug.type, logic.noSecurityInfoConfirmation);
            };

            $scope.uploadFile = function() {
                var p = $q.when();
                p = modals.confirm({
                    message: 'Are you sure you want to upload "' + $scope.ftpUploadData.type + '" "' + $scope.ftpUploadData.id + '" to FTP server?',
                    okText: 'Continue',
                    title: 'Confirm'
                });
                return p.then(function() {
                    return fileDao.ftpUpload($scope.ftpUploadData.type, $scope.ftpUploadData.id);
                });
            };

            $scope.entityTypes = _(objectTypes.data.entities).chain().map(function(entity, entityId) {
                return {
                    id: entityId,
                    text: entityId
                };
            }).sortBy('id').value();
            
            $scope.model.wrapper.class['display-none'] = false;

            $scope.downloadLinkClick = function (event) {
                if ($scope.isDeleted) {
                    event.preventDefault();
                }
            };

            $scope.$watch(function () {
                return widgetPolling.scope().isDeleted;
            }, function (newValue, oldValue) {
                $scope.isDeleted = newValue;
                $scope.downloadLinkToolTip = $scope.isDeleted ? 'This message is deleted' : '';
            });
        }
    };
});