var m = angular.module('profiles');

m.factory('avananApDebugLinksLogic', function(simpleSortableLogicCore, fileDao, widgetPolling) {
    return function($scope, logic) {
        var id = logic.entityId || widgetPolling.scope().id;
        return simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.avananApDebugLinks($scope.model.columns, id, logic.entityType);
            }
        });
    };
});