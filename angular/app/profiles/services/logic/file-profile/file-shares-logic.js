var m = angular.module('profiles');

m.factory('fileSharesLogic', function(widgetPolling, fileDao, simpleSortableLogicCore, revokeUserListener, objectTypeDao, $q) {
    return function($scope, logic, reinit) {
        var id = widgetPolling.scope().id;
        var objectTypes = widgetPolling.scope().objectTypes;

        var onActionUnsubscribe = widgetPolling.on('remediationActionPerformed', function () {
            logic.updateTick = 2000;
            logic.restoreUpdateTickAfter = moment().add(1,'minutes');
            reinit();
        });

        if (logic.restoreUpdateTickAfter && logic.restoreUpdateTickAfter.isBefore(moment())) {
            logic.restoreUpdateTickAfter = logic.originalUpdateTick;
            logic.restoreUpdateTickAfter = null;
        }

        var properties = {
            actionTypes: {},
            properties: {}
        };

        var revokeUserListenerFree = revokeUserListener($scope, id, 'file', logic, properties);

        var simpleFree = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                if (logic.restoreUpdateTickAfter && logic.restoreUpdateTickAfter.isBefore(moment())) {
                    logic.updateTick = logic.originalUpdateTick;
                    logic.restoreUpdateTickAfter = null;
                    reinit();
                    return $q.reject({status: 0});
                }
                return fileDao.shares($scope.model.columns, id);
            }
        });

        properties.actionTypes = _(objectTypes.data.actionTypes[logic.entityType]).chain().map(function(action) {
            return [action.id, action];
        }).object().value();
        properties.properties = objectTypes.data.sources(logic.entityType).properties;

        return function() {
            revokeUserListenerFree();
            simpleFree();
            onActionUnsubscribe();
        };
    };
});