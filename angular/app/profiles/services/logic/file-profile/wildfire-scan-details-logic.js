var m = angular.module('profiles');

m.factory('wildfireScanDetailsLogic', function(fileScanDetailsLogicCore, fileDao, widgetPolling, metaInfoLogicCore, $q) {
    function openWildfireReport() {
        window.open('/api/v1/wildfire/pdf_report/' + this.sha256, '_blank');
    }
    return function($scope, logic) {
        var id = logic.entityId || widgetPolling.scope().id;

        if (!$scope.wildfireReportButton) {
            $scope.wildfireReportButton = {
                onClick: openWildfireReport
            };
        }

        var coreOptions = {
            retrieve: function() {
                return fileDao.scanDetails(logic.app, id, logic.entityType, logic.historyTime)
                    .converter(function (input) {
                        if (input.data.sha256) {
                            $scope.wildfireReportButton.sha256 = input.data.sha256;
                            $scope.wildfireReportButton.label = 'Download report';
                            $scope.wildfireReportButton['class']= 'btn btn-blue';
                        } else {
                            delete $scope.wildfireReportButton.label;
                        }
                        return $q.when(input);
                    });
            },
            advanced: logic.advanced,
            whiteList: logic.whiteList,
            blackList: logic.blackList || [],
            coreLogic: metaInfoLogicCore,
            moduleLabels: logic.moduleLabels,
            buttons: [$scope.wildfireReportButton],
            autoByEntityInfo: logic.autoByEntityInfo,
            ordering: 'entityOrder'
        };
        return fileScanDetailsLogicCore($scope, logic, coreOptions);

    };
});