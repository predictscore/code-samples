var m = angular.module('profiles');

m.factory('fileFullSecurityInfoLogic', function(fileDao, simpleSortableLogicCore, $q, defaultListConverter, modals, $filter, modulesDao) {
    var infoDetailsPanel = {
        "type": "meta-info",
        "size": "medium",
        "wrapper": {
            "type": "widget-panel",
            "class": "avanan white-body"
        },
        "pair": {
            "class": "application-meta-data"
        }
    };

    return function($scope, logic, reinit) {
        var id = logic.entityId;

        var rawData = {};
        var modulesData = {};

        var simpleFree = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.fullSecurityInfo($scope.model.columns, id, logic.entityType, logic.source)
                    .converter(function (input, args) {
                        _(args.modules.data.apps_items.modules).each(function (module) {
                            modulesData[module.name] = module;
                        });
                        rawData = input.data;
                        var rows = [];
                        _(input.data).each(function (value, key) {
                            if (logic.noNulls && _(value).isNull()) {
                                return;
                            }
                            var row = {
                                app: key
                            };
                            if (_(value).isObject()) {
                                row.status = 'status_code: ' + value.status_code;
                            } else {
                                row.status = '' + value;
                            }
                            rows.push(row);
                        });
                        return $q.when({
                            data: {
                                rows: rows
                            }
                        });
                    }, _.identity)
                    .converter(defaultListConverter, function (preFetches) {
                        return {
                            columns: $scope.model.columns
                        };
                    });
            }
        });

        $scope.$on('open-full-info-details', function(event, data) {
            var app = data.attributes.app;
            infoDetailsPanel.wrapper.title = 'Detailed info for ' + app;
            var p = $q.when();
            if (rawData[app]) {
                if (modulesData[app] && modulesData[app].sec_entity && !modulesData[app].secEntityInfo) {
                    p = p.then(function () {
                       return modulesDao.entityInfo(modulesData[app].module_name, modulesData[app].sec_entity);
                    }).then(function (response) {
                        modulesData[app].secEntityInfo = response.data;
                    });
                }
                p = p.then(function () {
                    infoDetailsPanel.data =_(rawData[data.attributes.app]).chain().map(function (value, key) {
                        if (modulesData[app] && modulesData[app].sec_entity && modulesData[app].secEntityInfo &&
                            modulesData[app].secEntityInfo[key] && modulesData[app].secEntityInfo[key].troubleshoot_hidden
                        ) {
                            return;
                        }
                        return {
                            key: key,
                            value:  _(value).isObject() ? JSON.stringify(value) : $filter('localTimeIfIsoString')(value)
                        };
                    }).compact().value();
                });
            } else {
                infoDetailsPanel.data = [{
                    key: 'Value for key "' + data.attributes.app + '"',
                    value : '' + rawData[data.attributes.app]
                }];
            }
            return p.then(function () {
                return modals.widget({
                    panel: infoDetailsPanel
                }, 'lg');
            });

        });

        return function() {
            simpleFree();
        };
    };
});