var m = angular.module('profiles');

m.factory('avananApScanDetailsLogic', function(defaultListConverter, widgetSettings, $q, configDao, modulesDao, troubleshooting, modals) {


    function infoWidget () {
        var widget = {
            "type": "meta-info",
            "size": "margin-bottom-20 col-lg-12 col-md-12 col-sm-12 col-xs-12",
            "class": "light",
            "wrapper": {
                "type": "none"
            },
            "pair": {
                "class": "default-scan-details"
            }
        };
        if (troubleshooting.entityDebugEnabled()) {
            widget.wrapper.filters = [{
                "type": "flag",
                "text": "All Info (debug only)",
                "value": "advanced",
                "local": true
            }];
        }
        return widget;
    }

    function debugLinksWidget () {
        return {
            "id": "links-widget",
            "type": "data-table",
            "size": "large",
            "class": "light avanan-ap-links-table",
            "wrapper": {
                "type": "widget-panel",
                "title": "Links",
                "class": "light white-body white-header",
                "showLoading": true
            },
            "logic": {
              "name": "avananApDebugLinksLogic"
            },
            "bodyStyle": {
                "border-bottom": "none"
            },
            "columns": [{
                "id": "link",
                "text": "Link",
                "sortable": true,
                "dataClass": "word-break-all"
            }, {
                "id": "domain",
                "text": "Domain",
                "sortable": true
            }, {
                "id": "detections",
                "text": "Detections",
                "converter": "listConverter",
                "converterArgs": {
                    "countPostfix": "detections",
                    "collapseRest": true,
                    "expanded": 10
                }
            }, {
                "id": "results",
                "text": "Results",
                "converter": "clickForJSONConverter"
            }, {
                "id": "update_time",
                "text": "Update Time",
                "format": "time"
            }, {
                "id": "scanLink",
                "text": "Scan with VirusTotal",
                "converter": "scanLinkWithVirusTotalConverter",
                "converterArgs": {
                    "linkColumn": "link"
                }
            }]
        };
    }


    function updateWidgets($scope) {
        if (!$scope.infoWidget) {
            return;
        }
        $scope.model.widgets = [];
        $scope.model.widgets.push($scope.infoWidget);
        if ($scope.advanced && $scope.debugLinksWidget) {
            $scope.model.widgets.push($scope.debugLinksWidget);
        }
    }

    return function($scope, logic) {

        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        $scope.model.wrapper.isLoading = true;
        $scope.extendEntityInfo = {};

        var filtersWatch = _.noop;
        var advancedWatch = _.noop;
        var dataWatch = _.noop;

        logic.originalUpdateTick = logic.updateTick;

        modulesDao.entityInfo(logic.moduleLabels.module, logic.moduleLabels.entity).then(function (response) {
            $scope.entityInfo = response.data;

            if (!$scope.infoWidget) {
                $scope.infoWidget = infoWidget();
                $scope.infoWidget.logic = angular.copy(logic);
                $scope.infoWidget.logic.name = 'defaultScanDetailsLogic';
                $scope.infoWidget.logic.whiteList = ['phishing', 'reasons'];
                $scope.infoWidget.logic.updateTick = logic.originalUpdateTick;
                $scope.infoWidget.uuid = $scope.model.uuid;

                if (!troubleshooting.entityDebugEnabled()) {
                    $scope.infoWidget.logic.advanced = false;
                    $scope.infoWidget.logic.onlyWithLabels = true;
                    $scope.infoWidget.logic.blackList = _($scope.entityInfo).chain()
                        .omit($scope.infoWidget.logic.whiteList).keys().value();
                } else {
                    $scope.infoWidget.logic.advanced = true;
                }

            }

            filtersWatch = $scope.$watch('infoWidget.wrapper.filters', function () {
                $scope.model.wrapper.filters = $scope.infoWidget.wrapper.filters;
            });

            advancedWatch = $scope.$watch(function () {
                return widgetSettings.param($scope.model.uuid, 'advanced');
            }, function (newVal) {
                $scope.advanced = newVal;
                updateWidgets($scope);
            });

            dataWatch = $scope.$watch('infoWidget.data', function (modelData) {
                if (!$scope.infoWidget) {
                    return;
                }
                var data = $scope.infoWidget.entityData;
                if (data || $scope.infoWidget.entityInfo) {
                    $scope.model.wrapper.isLoading = false;
                }
                if (data && troubleshooting.entityDebugEnabled() && !$scope.debugLinksWidget) {
                    $scope.debugLinksWidget = debugLinksWidget();
                    $scope.debugLinksWidget.logic.entityId = data.av_fd_aggregate_by_parent || data.file_entity_id;
                    $scope.debugLinksWidget.logic.entityType = data.file_entity_type;
                }
                updateWidgets($scope);
            });

            updateWidgets($scope);
        });

        var linkResultsListener = $scope.$on('open-link-results-viewer', function (event, data) {
            modals.alert([
                'Link: ' + data.link,
                data.results
            ], {
                display: 'json',
                title: 'Link Scan Results'
            }, 'lg');
        });

        logic.updateTick = null;

        return function () {
            filtersWatch();
            dataWatch();
            advancedWatch();
            linkResultsListener();
        };
    };
});