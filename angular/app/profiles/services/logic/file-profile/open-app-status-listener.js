var m = angular.module('profiles');

m.factory('openAppStatusListener', function(modals, appScanStatusHelper, troubleshooting, fileDao) {
    var items = [{
        "type": "data-table",
        "logic": {
            "name": "comodoScanDetailsLogic",
            "app": "comodo"
        },
        "size": "medium",
        "bodyStyle": {
            "height": "415px"
        },
        "class": "light",
        "wrapper": {
            "type": "widget-panel",
            "title": "Comodo scan details",
            "class": "avanan white-body overflow-auto"
        },
        "options": {
            "pagesAround": 2,
            "pageSize": 20,
            "pagination": {
                "page": 1
            }
        },
        "columns": [{
            "id": "matcher",
            "text": "Matcher"
        }, {
            "id": "index",
            "text": "Index"
        }, {
            "id": "pattern",
            "text": "Pattern"
        }, {
            "id": "count",
            "text": "Count"
        }]
    }, {
        "type": "data-table",
        "logic": {
            "name": "avananDlpScanDetailsLogic",
            "app": "dlp_avanan",
            "forceInactive": true,
            "updateTick": 90000
        },
        "size": "medium",
        "modalSize": "lg",
        "bodyStyle": {
            "height": "415px"
        },
        "class": "light",
        "wrapper": {
            "type": "widget-panel",
            "title": "Smart text search details",
            "class": "avanan white-body overflow-auto"
        },
        "options": {
            "pagesAround": 2,
            "pageSize": 20,
            "pagination": {
                "page": 1
            }
        },
        "columns": [{
            "id": "av_update_time",
            "text": "Update Time",
            "format": "time"
        }, {
            "id": "ruleName",
            "text": "Rule"
        }, {
            "id": "leak",
            "text": "Leak"
        }, {
            "id": "score",
            "text": "Score"
        }, {
            "id": "hit_count",
            "text": "Hit Count"
        }, {
            "id": "status_code",
            "text": "Status Code"
        }, {
            "id": "status_description",
            "text": "Status Description"
        }, {
            "id": "found_text",
            "text": "Found Text",
            "converter": "listConverter",
            "converterArgs": {
                "countPostfix": "tags",
                "collapseRest": true,
                "expanded": 5,
                "uniqueCountText": "times",
                "uniqueTrim": true
            }
        }]
    }, {
        "type": "meta-info",
        "logic": {
            "name": "wildfireScanDetailsLogic",
            "app": "wildfire",
            "setApp": true,
            "setLabels": true,
            "setTitlePostfix": " scan details",
            "autoByEntityInfo": true,
            "allArraysConverter": {
                "converter": "limitedArrayConverter",
                "args": {
                    "collapseRest": true,
                    "expanded": 10,
                    "countPostfix": 'matches'
                }
            }
        },
        "uuid": "82b8f215-a10c-4a2d-a474-f67da180cfad",
        "size": "medium",
        "bodyStyle": {
            "height": "415px"
        },
        "class": "light",
        "wrapper": {
            "type": "widget-panel",
            "title": "Wildfire scan details",
            "class": "avanan white-body overflow-auto"
        },
        "pair": {
            "class": "wildfire-scan-details"
        }
    }, {
        "type": "meta-info",
        "logic": {
            "name": "checkpointTeScanDetailsLogic",
            "app": "checkpoint_te",
            "beautifyKey": true,
            "moduleLabels": {
                "module": "checkpoint",
                "entity": "checkpoint_te"
            },
            "converters": {
                "suspicious_activities": {
                    "converter": "limitedArrayConverter",
                    "args": {
                        "collapseRest": true,
                        "expanded": 100,
                        "countPostfix": 'activities'
                    }
                }
            },
            "allArraysConverter": {
                "converter": "limitedArrayConverter",
                "args": {
                    "collapseRest": true,
                    "expanded": 10,
                    "countPostfix": 'matches'
                }
            },
            "autoByEntityInfo": true
        },
        "uuid": "2a495f3c-d364-4353-b031-526586cc3a86",
        "size": "medium",
        "bodyStyle": {
            "height": "415px"
        },
        "class": "light",
        "wrapper": {
            "type": "widget-panel",
            "title": "CheckPoint Threat Emulation",
            "class": "avanan white-body overflow-auto"
        },
        "pair": {
            "class": "checkpoint-te-scan-details"
        }
    }, {
        "type": "meta-info",
        "logic": {
            "name": "globalvelocityScanDetailsLogic",
            "app": "dlp_globalvelocity",
            "allArraysConverter": {
                "converter": "limitedArrayConverter",
                "args": {
                    "collapseRest": true,
                    "expanded": 10,
                    "countPostfix": 'matches'
                }
            },
            "moduleLabels": {
                "module": "dlp_globalvelocity",
                "entity": "globalvelocity_scan"
            },
            "autoByEntityInfo": true,
            "extendEntityInfo": {
                "component_name": {
                    "label": "Found Text"
                }
            }
        },
        "uuid": "554df429-1850-4d2c-93c5-cd848bf6331a",
        "size": "medium",
        "bodyStyle": {
            "height": "415px"
        },
        "class": "light",
        "wrapper": {
            "type": "widget-panel",
            "title": "GlobalVelocity scan results",
            "class": "avanan white-body overflow-auto"
        },
        "pair": {
            "class": "globalvelocity-scan-details"
        }
    }, {
        "type": "meta-info",
        "logic": {
            "name": "defaultScanDetailsLogic",
            "app": "ap_cyren",
            "setApp": true,
            "setLabels": true,
            "setTitlePostfix": " scan details",
            "beautifyKey": true,
            "autoByEntityInfo": true,
            "allArraysConverter": {
                "converter": "limitedArrayConverter",
                "args": {
                    "collapseRest": true,
                    "expanded": 10,
                    "countPostfix": 'links'
                }
            },
            "converters": {
                "links_categories": {
                    "converter": "cyrenFoundLinksConverter"
                }
            }
        },
        "uuid": "733b7676-63b6-4c79-9e8f-cb81893d5d1b",
        "modalSize": "lg",
        "bodyStyle": {
            "min-height": "415px"
        },
        "class": "light",
        "wrapper": {
            "type": "widget-panel",
            "title": "Scan details",
            "class": "avanan white-body overflow-auto"
        },
        "pair": {
            "class": "globalvelocity-scan-details"
        }
    }, {
        "type": "multi-widget",
        "logic": {
            "name": "avananApScanDetailsLogic",
            "app": "ap_avanan",
            "forceInactive": true,
            "setApp": true,
            "setLabels": true,
            "setTitlePostfix": " scan details",
            "beautifyKey": true,
            "autoByEntityInfo": false,
            "allArraysConverter": {
                "converter": "limitedArrayConverter",
                "args": {
                    "collapseRest": true,
                    "expanded": 10,
                    "countPostfix": 'matches'
                }
            }
        },
        "uuid": "adb899ae-4aa3-4b04-8ed6-9455d0386008",
        "size": "medium",
        "modalSize": "lg",
        "class": "light",
        "wrapper": {
            "type": "widget-panel",
            "title": "Scan results",
            "class": "avanan white-body overflow-auto"
        },
        "pair": {
            "class": "default-scan-details"
        }
    }, {
        "type": "multi-widget",
        "logic": {
            "name": "avCiscoScanDetailsLogic",
            "app": "av_cisco",
            "setApp": true,
            "setLabels": true,
            "setTitlePostfix": " scan details",
            "beautifyKey": true,
            "autoByEntityInfo": true,
            "allArraysConverter": {
                "converter": "limitedArrayConverter",
                "args": {
                    "collapseRest": true,
                    "expanded": 10,
                    "countPostfix": 'matches'
                }
            }
        },
        "uuid": "f1262261-16bc-45bf-8598-885b14c2de88",
        "size": "medium",
        "modalSize": "lg",
        "class": "light",
        "wrapper": {
            "type": "widget-panel",
            "title": "Scan results",
            "class": "avanan white-body overflow-auto"
        },
        "pair": {
            "class": "default-scan-details"
        }
    }, {
        "type": "multi-widget",
        "logic": {
            "name": "apBitdefenderScanDetailsLogic",
            "app": "ap_bitdefender",
            "setApp": true,
            "setLabels": true,
            "setTitlePostfix": " scan details",
            "beautifyKey": true,
            "autoByEntityInfo": true,
            "allArraysConverter": {
                "converter": "limitedArrayConverter",
                "args": {
                    "collapseRest": true,
                    "expanded": 10,
                    "countPostfix": 'matches'
                }
            }
        },
        "uuid": "f7e31975-bb7e-4f47-8896-c56ab388aeb9",
        "modalSize": "lg",
        "class": "light",
        "wrapper": {
            "type": "widget-panel",
            "title": "Scan results",
            "class": "avanan white-body overflow-auto"
        },
        "pair": {
            "class": "default-scan-details"
        }
    }, {
        "type": "multi-widget",
        "logic": {
            "name": "sbCheckpointScanDetailsLogic",
            "app": "sb_checkpoint",
            "setApp": true,
            "setLabels": true,
            "setTitlePostfix": " scan details",
            "beautifyKey": true,
            "autoByEntityInfo": true,
            "allArraysConverter": {
                "converter": "limitedArrayConverter",
                "args": {
                    "collapseRest": true,
                    "expanded": 10,
                    "countPostfix": 'matches'
                }
            }
        },
        "uuid": "d2e0c93a-c04e-4b15-9cb4-ea789371cdec",
        "modalSize": "lg",
        "class": "light",
        "wrapper": {
            "type": "widget-panel",
            "title": "Scan results",
            "class": "avanan white-body overflow-auto"
        },
        "pair": {
            "class": "default-scan-details"
        }
    }, {
        "type": "multi-widget",
        "logic": {
            "name": "solgateScanDetailsLogic",
            "app": "solgate",
            "setApp": true,
            "setLabels": true,
            "setTitlePostfix": " scan details",
            "beautifyKey": true,
            "autoByEntityInfo": true,
            "allArraysConverter": {
                "converter": "limitedArrayConverter",
                "args": {
                    "collapseRest": true,
                    "expanded": 10,
                    "countPostfix": 'matches'
                }
            }
        },
        "uuid": "bb98e224-66af-40f7-8e5f-843cdefc0cdd",
        "modalSize": "lg",
        "class": "light",
        "wrapper": {
            "type": "widget-panel",
            "title": "Scan results",
            "class": "avanan white-body overflow-auto"
        },
        "pair": {
            "class": "default-scan-details"
        }
    }, {
        "type": "meta-info",
        "logic": {
            "name": "defaultScanDetailsLogic",
            "isDefault": true,
            "setApp": true,
            "setLabels": true,
            "setTitlePostfix": " scan details",
            "beautifyKey": true,
            "autoByEntityInfo": true,
            "allArraysConverter": {
                "converter": "limitedArrayConverter",
                "args": {
                    "collapseRest": true,
                    "expanded": 10,
                    "countPostfix": 'matches'
                }
            }
        },
        "uuid": "adb899ae-4aa3-4b04-8ed6-9455d0386008",
        "size": "medium",
        "bodyStyle": {
            "height": "415px"
        },
        "class": "light",
        "wrapper": {
            "type": "widget-panel",
            "title": "Scan results",
            "class": "avanan white-body overflow-auto"
        },
        "pair": {
            "class": "default-scan-details"
        }
    }];
    return function($scope) {

        function dlpAvananTroubleShootingOptions(panel) {
            if (troubleshooting.available() && panel.logic.app === 'dlp_avanan') {
                panel.wrapper.buttons = [{
                    "label": "Troubleshooting",
                    "class": "btn-blue",
                    "execute": function () {
                        modals.widget({
                            panel: {
                                "type": "info",
                                "logic": {
                                    "name": "avananDlpTroubleshootingLogic",
                                    "entityType": panel.logic.entityType,
                                    "updateTick": null
                                },
                                "bodyStyle": {
                                    "height": "415px"
                                },
                                "class": "light",
                                "wrapper": {
                                    "type": "widget-panel",
                                    "title": "Smart Text Search Troubleshooting",
                                    "class": "avanan white-body overflow-auto"
                                },
                                "pair": {
                                    "class": "avanan-dlp-troubleshooting"
                                }
                            }
                        });
                    }
                }];
            }
        }

        return $scope.$on('open-app-status', function(event, data) {
            if (!appScanStatusHelper.statusClickable(data.scanStatus)) {
                return;
            }
            var panel = _(items).find(function(item) {
                return item.logic.app && (item.logic.app == data.appName);
            });
            if (_(panel).isUndefined()) {
                panel = _(items).find(function(item) {
                    return item.logic.module && (item.logic.module == data.moduleName);
                });
            }
            if (_(panel).isUndefined()) {
                panel = _(items).find(function(item) {
                    return item.logic.isDefault;
                });
            }

            if (_(panel).isUndefined()) {
                return;
            }

            panel = angular.copy(panel);

            if (panel.logic.setApp) {
                panel.logic.app = data.appName;
                delete panel.data;
                if (panel.logic.setTitlePostfix) {
                    panel.wrapper.title = (data.appLabel || data.appName) + panel.logic.setTitlePostfix;
                }
            }

            if (panel.logic.setLabels && data.moduleName && data.scanEntity) {
                panel.logic.moduleLabels = {
                    module: data.moduleName,
                    entity: data.scanEntity
                };
            }

            if (data.entityId) {
                panel.logic.entityId = data.entityId;
            }
            if (data.entityType) {
                panel.logic.entityType = data.entityType;
            } else {
                if ($scope.widgetPolling) {
                    panel.logic.entityType = $scope.widgetPolling.scope().entityType;
                }
            }

            if (data.isHistory && data.time) {
                panel.logic.historyTime = data.time;
            }
            dlpAvananTroubleShootingOptions(panel);

            modals.widget({
                panel: panel,
                closeOnNavigate: true
            }, panel.modalSize);
        });
    };
});