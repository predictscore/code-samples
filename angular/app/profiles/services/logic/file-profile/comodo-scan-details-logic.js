var m = angular.module('profiles');

m.factory('comodoScanDetailsLogic', function(fileScanDetailsLogicCore, fileDao, widgetPolling) {
    return function($scope, logic) {
        var id = logic.entityId || widgetPolling.scope().id;
        return fileScanDetailsLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.comodoScanDetails($scope.model.columns, id);
            }
        });
    };
});