var m = angular.module('profiles');

m.factory('solgateLinksLogic', function(simpleSortableLogicCore, fileDao, widgetPolling) {
    return function($scope, logic) {
        var id = logic.entityId || widgetPolling.scope().id;
        return simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.solgateLinks($scope.model.columns, id, logic.entityType);
            }
        });
    };
});