var m = angular.module('profiles');

m.factory('sbCheckpointScanDetailsLogic', function() {


    function infoWidget () {
        return {
            "type": "meta-info",
            "size": "margin-bottom-20 col-lg-12 col-md-12 col-sm-12 col-xs-12",
            "class": "light",
            "wrapper": {
                "type": "none"
            },
            "pair": {
                "class": "default-scan-details"
            }
        };
    }

    function linksWidget () {
        return {
            "id": "links-widget",
            "type": "data-table",
            "size": "large",
            "class": "light avanan-ap-links-table",
            "wrapper": {
                "type": "widget-panel",
                "title": "Found links",
                "class": {
                    "light white-body white-header no-inner-padding": true,
                    "display-none": true
                }
            },
            "logic": {
              "name": "sbCheckpointLinksLogic"
            },
            "bodyStyle": {
                "border-bottom": "none"
            },
            "columns": [{
                "id": "link",
                "text": "Link",
                "sortable": true
            }, {
                "id": "domain",
                "text": "Domain",
                "sortable": true
            },{
                "id": "orderByStatus",
                "hidden": true
            }, {
                "id": "status",
                "text": "Status",
                "sortable": {
                    "id": "orderByStatus"
                }
            }],
            "options": {
                "defaultOrdering": {
                    "columnName": "orderByStatus",
                    "order": "asc"
                }
            }
        };
    }

    function updateWidgets($scope) {
        $scope.linksWidget.wrapper["class"]["display-none"] = !($scope.linksWidget.data && $scope.linksWidget.data.length);
    }

    return function($scope, logic) {

        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }

        if (!$scope.infoWidget) {
            $scope.infoWidget = infoWidget();
            $scope.infoWidget.logic = angular.copy(logic);
            $scope.infoWidget.logic.name = 'defaultScanDetailsLogic';
            $scope.infoWidget.uuid = $scope.model.uuid;
            $scope.infoWidget.modalWrapper = $scope.model.wrapper;
        }
        if (!$scope.linksWidget) {
            $scope.linksWidget = linksWidget();
            $scope.linksWidget.logic.entityType = logic.entityType;
        }
        updateWidgets($scope);
        $scope.model.widgets = [$scope.infoWidget, $scope.linksWidget];


        var filtersWatch = $scope.$watch('infoWidget.wrapper.filters', function () {
            $scope.model.wrapper.filters = $scope.infoWidget.wrapper.filters;
        });
        var dataWatch = $scope.$watchCollection(function () {
            return [$scope.infoWidget.data, $scope.linksWidget.data];
        }, function () {
            if ($scope.infoWidget.data && $scope.linksWidget.data) {
                $scope.model.wrapper.isLoading = false;
            }
            updateWidgets($scope);
        });



        return function () {
            filtersWatch();
            dataWatch();
        };
    };
});