var m = angular.module('profiles');

m.factory('fileActivitySummaryLogic', function(widgetPolling, fileDao, simpleSortableLogicCore, $q) {
    return function($scope, logic, reinit) {
        var id = widgetPolling.scope().id;
        var objectTypes = widgetPolling.scope().objectTypes;

        var simpleFree = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.activitySummary($scope.model.columns, id);
            }
        });

        return function() {
            simpleFree();
        };
    };
});