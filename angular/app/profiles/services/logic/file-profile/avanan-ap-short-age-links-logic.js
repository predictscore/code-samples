var m = angular.module('profiles');

m.factory('avananApShortAgeLinksLogic', function(simpleSortableLogicCore, fileDao, widgetPolling) { // obsolete
    return function($scope, logic) {
        var id = logic.entityId || widgetPolling.scope().id;
        return simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.avananApLinks($scope.model.columns, id, logic.entityType, true);
            }
        });
    };
});