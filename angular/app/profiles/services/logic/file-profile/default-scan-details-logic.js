var m = angular.module('profiles');

m.factory('defaultScanDetailsLogic', function(fileScanDetailsLogicCore, fileDao, widgetPolling, metaInfoLogicCore, $q) {

    var downloadReportButtonId = 'download-report-button';

    function addDownloadReportButton(wrapper, link, label) {
        wrapper.buttons = wrapper.buttons || [];
        var downloadButton = _(wrapper.buttons).find(function (button) {
            return button.id === downloadReportButtonId;
        });
        if (!downloadButton) {
            downloadButton = {
                'class': 'btn-blue',
                label: label || 'Download Report',
                position: 'right',
                id: downloadReportButtonId
            };
            wrapper.buttons.push(downloadButton);
        }
        downloadButton.onclick = function () {
            window.open(link, '_blank');
        };
    }

    function removeDownloadReportButton(wrapper) {
        if (!wrapper.buttons) {
            return;
        }
        var btnIdx = _(wrapper.buttons).findIndex(function (button) {
            return button.id === downloadReportButtonId;
        });
        if (btnIdx === -1) {
            return;
        }
        wrapper.buttons.splice(btnIdx, 1);
    }

    return function($scope, logic) {
        var id = logic.entityId || widgetPolling.scope().id;
        var coreOptions = {
            retrieve: function() {
                var query = fileDao.scanDetails(logic.app, id, logic.entityType, logic.historyTime)
                    .converter(function (input) {
                        var wrapper = $scope.model.modalWrapper || $scope.model.wrapper;
                        var haveDownloadLink = false;
                        if (logic.app === 'solgate' && input.data.have_word_report) {
                            haveDownloadLink = true;
                            addDownloadReportButton(wrapper, '/api/v1/solgate/report/' + logic.entityType + '/' + id);
                        }
                        if (logic.app === 'av_cisco' && input.data.have_pdf_report) {
                            haveDownloadLink = true;
                            addDownloadReportButton(wrapper, '/api/v1/av_cisco/report/' + logic.entityType + '/' + id);
                        }
                        if (logic.app === 'atp_fireeye' && input.data.have_report) {
                            haveDownloadLink = true;
                            addDownloadReportButton(wrapper, '/api/v1/atp_fireeye/' + logic.entityType + '/' + id, 'View Report');
                        }
                        if (!haveDownloadLink) {
                            removeDownloadReportButton(wrapper);
                        }
                        return $q.when(input);
                    });
                if (logic.resultsConverter) {
                    query.converter(logic.resultsConverter);
                }
                return query;
            },
            advanced: logic.advanced,
            whiteList: logic.whiteList,
            blackList: logic.blackList || [],
            coreLogic: metaInfoLogicCore,
            moduleLabels: logic.moduleLabels,
            autoByEntityInfo: logic.autoByEntityInfo,
            extendEntityInfo: logic.extendEntityInfo,
            ordering: 'entityOrder'
        };
        return fileScanDetailsLogicCore($scope, logic, coreOptions);
    };
});