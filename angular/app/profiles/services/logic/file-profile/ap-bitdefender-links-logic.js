var m = angular.module('profiles');

m.factory('apBitdefenderLinksLogic', function(simpleSortableLogicCore, fileDao, widgetPolling) {
    return function($scope, logic) {
        var id = logic.entityId || widgetPolling.scope().id;
        return simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.apBitdefenderLinks($scope.model.columns, id, logic.entityType);
            }
        });
    };
});