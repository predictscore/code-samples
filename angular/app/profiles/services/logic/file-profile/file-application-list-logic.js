var m = angular.module('profiles');

m.factory('fileApplicationListLogic', function(fileDao, widgetPolling, simpleLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.applicationList($scope.model.columns, id);
            }
        });
    };
});
