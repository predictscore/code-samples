var m = angular.module('profiles');

m.factory('fileVersionsLogic', function(widgetPolling, fileDao, simpleLogicCore) {
    return function($scope, logic, reinit) {
        var id = widgetPolling.scope().id;
        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.versions($scope.model.columns, id);
            }
        });
    };
});