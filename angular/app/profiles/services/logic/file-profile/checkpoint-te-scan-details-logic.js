var m = angular.module('profiles');

m.factory('checkpointTeScanDetailsLogic', function($q, fileScanDetailsLogicCore, fileDao, widgetPolling, metaInfoLogicCore) {
    return function($scope, logic) {
        var id = logic.entityId || widgetPolling.scope().id;

        var pdfButton = $scope.model.wrapper.buttons && $scope.model.wrapper.buttons[0] || {
            'class': 'btn-blue',
            label: 'View full Report',
            hidden: true,
                position: 'right'
        };

        $scope.model.wrapper.buttons = [
            pdfButton
        ];

        $scope.coreOptionButtons = $scope.coreOptionButtons || [];

        function setButtons (xmlReport, pdfReportImages) {
            var xmlButtonIdx = _(coreOptions.buttons).findIndex(function (button) {
                return button.id === 'view-xml-report-button';
            });

            if (xmlReport) {
                var xmlButton = {
                    id: 'view-xml-report-button',
                    label: 'View XML report',
                    'class': 'download-xml-button',
                    onClick: function() {
                        window.open('data:text/xml,' + encodeURIComponent(xmlReport));
                    }
                };
                if (xmlButtonIdx !== -1) {
                    coreOptions.buttons[xmlButtonIdx] = xmlButton;
                } else {
                    coreOptions.buttons.push(xmlButton);
                }
            } else {
                if (xmlButtonIdx !== -1) {
                    coreOptions.splice(xmlButtonIdx, 1);
                }
            }
            var options = _(pdfReportImages).chain().keys().map(function (imageId) {
                return {
                    imageId: imageId,
                    label: pdfReportImages[imageId],
                    onclick: function () {
                        window.open('/api/v1/checkpoint/pdf_report/' + logic.entityType + '/' + id + '/' + imageId);
                    }
                };
            }).value();
            if (options.length > 0) {
                pdfButton.options = options;
                pdfButton.hidden = false;
            } else {
                pdfButton.hidden = true;
            }
        }

        var coreOptions = {
            retrieve: function() {
                return fileDao.checkpointTeScanDetails(id, logic.entityType)
                    .converter(function(input, args) {
                        setButtons(input.data.xml_report, args.pdfReportImages.data.images);
                        delete input.data.xml_report;
                        return $q.when(input);
                    }, function (preFetches) {
                        return {
                            pdfReportImages: preFetches.pdfReportImages
                        };
                    });
            },
            advanced: logic.advanced,
            whiteList: logic.whiteList,
            blackList: logic.blackList || [],
            coreLogic: metaInfoLogicCore,
            moduleLabels: logic.moduleLabels,
            autoByEntityInfo: logic.autoByEntityInfo,
            ordering: 'entityOrder',
            buttons: $scope.coreOptionButtons
        };
        return fileScanDetailsLogicCore($scope, logic, coreOptions);

    };
});