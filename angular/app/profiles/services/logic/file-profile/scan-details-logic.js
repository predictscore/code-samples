var m = angular.module('profiles');

m.factory('scanDetailsLogic', function(widgetPolling, fileDao, troubleshooting, widgetSettings, modals, fileSecurityDebugHelper, $q, entityTypes, secToolsExceptionParams, $rootScope, apAvananExceptionsParams, solgateExceptionsParams, sideMenuManager) {

    var pendingUpdateTick = 4000;
    function downloadEntity(entityId, entityType) {
        window.open(fileDao.downloadMatchedUrl(entityId, entityType), '_blank');
    }

    return function($scope, logic, reinit) {
        var id = widgetPolling.scope().id;
        var entityType = logic.entityType || widgetPolling.scope().entityType;
        var localEntityLabel = entityTypes.toLocal(entityType) || _(entityType.split('_')).last() || 'item';

        var originalId = widgetPolling.scope().preReleaseId;
        if (originalId) {
            id = originalId;
            if (!$scope.model.wrapper.originalTitle) {
                $scope.model.wrapper.originalTitle = $scope.model.wrapper.title;
            }
            $scope.model.wrapper.title = $scope.model.wrapper.originalTitle + ' of original email';
        }

        _($scope.model.items).each(function(widget) {
            widget.logic.entityType = entityType;
        });
        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }

        if (troubleshooting.available()) {
            var params = widgetSettings.params($scope.model.uuid);
            var troubleshootingEnabled = troubleshooting.enabled();
            if (params.troubleshooting !== troubleshootingEnabled) {
                params.troubleshooting = troubleshootingEnabled;
                widgetSettings.params($scope.model.uuid, params);
            }

            $scope.model.wrapper.filters = $scope.model.wrapper.filters || [];
            if (!_($scope.model.wrapper.filters).find(function (option) {
                    return option.value === 'troubleshooting';
                })) {
                $scope.model.wrapper.filters.push({
                    "type": "flag",
                    "text": "Troubleshooting Mode",
                    "value": "troubleshooting",
                    "local": true
                });
            }

            $scope.model.wrapper.buttons = $scope.model.wrapper.buttons || [];
            if (!_($scope.model.wrapper.buttons).find(function (button) {
                    return button.id === 'raw-info-sec-entities';
                })) {
                $scope.model.wrapper.buttons.push({
                    id: 'raw-info-sec-entities',
                    label: 'Full Info (sec_entities)', //'Full Info (sec_entities) (events)',
                    'class': 'btn-blue',
                    execute: function () {
                        return fileSecurityDebugHelper.showSecEntities(id, entityType);
                    },
                    "position": "right"
                });
            }

        }

        function addFiltersItem(item) {
            if (!$scope.model.wrapper.filters) {
                $scope.model.wrapper.filters = [];
            }
            if (!item.id || !_($scope.model.wrapper.filters).find(function (option) {
                    return option.id === item.id;
                })) {
                $scope.model.wrapper.filters.push(item);
            }
        }

        function loadData() {
            $q.all([
                fileDao.allScanDetails(id, entityType),
                fileDao.cpCapsuleInfo(id, entityType),
                entityTypes.toLocal(entityType) === 'file' ? fileDao.retrieve(id) : $q.when({data: null}),
                sideMenuManager.currentUser().view_private_data ? fileDao.downloadMatchedCheck(id, entityType): $q.when({data: null})
            ]).then(function (responses) {
                var scanDetails = responses[0].data;
                var cpCapsuleInfo = responses[1].data;
                var fileData = responses[2].data;
                var downloadTest = responses[3].data || {};
                $scope.model.wrapper.isLoading = false;
                $scope.model.widgets = scanDetails.widgets;
                var cryptPair;
                if (cpCapsuleInfo.encryptedEntities && cpCapsuleInfo.encryptedEntities.length) {
                    cryptPair = {
                        type: 'encrypted',
                        pair_entity_id: cpCapsuleInfo.encryptedEntities[0].entity_id
                    };
                } else if (cpCapsuleInfo.orig_file_id) {
                    cryptPair = {
                        type: 'original',
                        pair_entity_id: cpCapsuleInfo.orig_file_id
                    };
                }
                if(cryptPair) {
                    $scope.model.widgets.push({
                        wrapper: {
                            class: "file-security-stack-category",
                            title: "Encryption",
                            type: "widget-panel"
                        },
                        type: 'info',
                        lines: [{
                            text: '#' + JSON.stringify({
                                display: 'link',
                                text: 'View ' + cryptPair.type + ' file',
                                type: 'file',
                                id: cryptPair.pair_entity_id,
                                class: 'crypt-pair-link'
                            })
                        }]
                    });
                }   
                var updateTick = scanDetails.havePendingItems ? pendingUpdateTick : logic.originalUpdateTick;
                var currentUpdateTick = logic.updateTick;
                if (currentUpdateTick !== updateTick) {
                    logic.updateTick = updateTick;
                    if (currentUpdateTick > updateTick) {
                        reinit();
                    }
                }
                if (fileData) {
                    var hashTypes = ['md5','sha1','sha256'];
                    var virusTotalLinks = [];
                    _(hashTypes).each(function (hashType) {
                        var av_hash_exists = false;
                        if (fileData['av_file_hash_' + hashType]) {
                            av_hash_exists = true;
                            virusTotalLinks.push({
                                id: 'virustotal-link-' + hashType,
                                href: 'https://www.virustotal.com/en/search/?query=' + fileData['av_file_hash_' + hashType],
                                text: 'Search this ' + hashType + ' on VirusTotal',
                                type: 'external-link',
                                local: true
                            });
                        }
                        if (logic.hashColumn && logic.hashTitle === hashType && fileData[logic.hashColumn]) {
                            if (!av_hash_exists) {
                                virusTotalLinks.push({
                                    id: 'virustotal-link-' + hashType,
                                    href: 'https://www.virustotal.com/en/search/?query=' + fileData[logic.hashColumn],
                                    text: 'Search this ' + hashType + ' on VirusTotal',
                                    type: 'external-link',
                                    local: true
                                });
                            } else {
                                if (fileData[logic.hashColumn].toLowerCase() !== fileData['av_file_hash_' + hashType].toLowerCase()) {
                                    virusTotalLinks.push({
                                        id: 'virustotal-link-original-' + hashType,
                                        href: 'https://www.virustotal.com/en/search/?query=' + fileData[logic.hashColumn],
                                        text: 'Search original ' + hashType + ' of this file on VirusTotal',
                                        type: 'external-link',
                                        local: true
                                    });
                                }
                            }
                        }
                    });
                    if (virusTotalLinks.length) {
                        _(virusTotalLinks).each(function (link) {
                            addFiltersItem(link);
                        });
                    }
                }
                if (downloadTest.success) {
                    addFiltersItem({
                        id: 'download-entity',
                        execute: function () {
                            var noConfirm = window.localStorage.getItem('download-entity-no-confirm');
                            if (noConfirm) {
                                return downloadEntity(id, entityType);
                            }
                            modals.confirm({
                                title: 'Notice',
                                message: 'You have requested to download this ' + localEntityLabel + '. This action is recorded in the audit log.',
                                okText: 'Download',
                                remember: 'Don\'t show this message again'
                            }).then(function (data) {
                                if (data.remember) {
                                    window.localStorage.setItem('download-entity-no-confirm', true);
                                }
                                return downloadEntity(id, entityType);
                            });
                        },
                        text: 'Download this ' + localEntityLabel,
                        type: 'button',
                        local: true
                    });
                } else if (downloadTest.status === 404) {
                    addFiltersItem({
                        id: 'download-entity-impossible',
                        text: 'Download this ' + localEntityLabel,
                        type: 'button',
                        local: true,
                        disabled: true
                    });
                }
                if (logic.attachmentsStatusWidget) {
                    $scope.model.widgets.push(logic.attachmentsStatusWidget);
                }
            });
        }

        var reloadListener = $scope.$on('file-security-stack-reload', function () {
            $scope.model.wrapper.isLoading = true;
            reinit();
        });

        var secToolExceptionListener = $scope.$on('add-sec-tool-exception', function (event, data) {
            var localEntity = entityTypes.toLocal(entityType);
            modals.confirm({
                message: 'Are you sure you want to add this ' + (localEntity === 'email' ? 'message' : localEntity) + ' to ' + secToolsExceptionParams.getLabel(data.secType) + '?',
                title: 'Add Security Tools Exception',
                okText: 'Add exception'
            }).then(function () {
                return fileDao.exceptionsAdd([{
                    entity_type: data.entityType,
                    entity_id: data.entityId,
                    sec_type: data.secType
                }]);
            }).then(function () {
                $rootScope.$broadcast('file-security-stack-reload');
            });
        });

        var solgateExceptionListener = $scope.$on('add-solgate-exception', function (event, data) {
            return solgateExceptionsParams.addDialog(data.macro_hash).then(function () {
                $rootScope.$broadcast('file-security-stack-reload');
            });
        });


        var avananApExeceptionsListener = $scope.$on('add-avanan-ap-exception', function (event, data) {
            return apAvananExceptionsParams.addByEntity(data.exceptionType, data.entityType, data.entityId).then(function () {
                $rootScope.$broadcast('file-security-stack-reload');
            });
        });

        var paramsWatch = $scope.$watch(function () {
            return widgetSettings.params($scope.model.uuid);
        }, function (newValue, oldValue) {
            if (troubleshooting.available()) {
                troubleshooting.enabled(widgetSettings.params($scope.model.uuid).troubleshooting);
            }
            if (!_(newValue).isEqual(oldValue)) {
                $scope.model.wrapper.isLoading = true;
            }
            loadData();
        }, true);

        return function() {
            paramsWatch();
            reloadListener();
            secToolExceptionListener();
            avananApExeceptionsListener();
            solgateExceptionListener();
        };
    };
});