var m = angular.module('profiles');

m.factory('fileEventsListLogic', function(widgetPolling, fileDao, eventsListLogicCore, eventsDao) {
    return function($scope, logic) {
        var fileId = widgetPolling.scope().id;
        var entityType = logic.entityType || widgetPolling.scope().entityType;
        var coreOptions = {
            permanentEvents: function() {
                return fileDao.fileScanEvents(fileId, entityType);
            },
            permanentTimeColumn: 'av_fd_update_time',
            descriptionColumn: logic.descriptionColumn,
            idColumn: logic.idColumn,
            imageColumn: logic.imageColumn,
            timeColumnName: logic.timeColumnName,
            checkboxColumn: logic.checkboxColumn
        };
        if (!logic.noTypes) {
            coreOptions.retrieveTypes = function() {
                return eventsDao.retrieveTypes(logic.typesParams);
            };
        }
        if (!logic.noEvents) {
            coreOptions.retrieve = function(after) {
                return fileDao.events($scope.model.columns, after, fileId);
            };
        }

        return eventsListLogicCore($scope, logic, coreOptions);
    };
});