var m = angular.module('profiles');

m.factory('fileEventsMapLogic', function(ipDao, widgetPolling, worldAccessMapLogicCore) {
    return function($scope, logic) {
        var fileId = widgetPolling.scope().id;
        
        return worldAccessMapLogicCore($scope, logic, {
            retrieve: function(days) {
                return ipDao.worldAccess(days, {
                    fileId: fileId
                });
            }
        });
    };
});