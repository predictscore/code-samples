var m = angular.module('profiles');

m.factory('solgateScanDetailsLogic', function(troubleshooting) {


    function infoWidget () {
        return {
            "type": "meta-info",
            "size": "margin-bottom-20 col-lg-12 col-md-12 col-sm-12 col-xs-12",
            "class": "light",
            "wrapper": {
                "type": "none"
            },
            "pair": {
                "class": "default-scan-details"
            }
        };
    }

    function linksWidget () {
        return {
            "id": "links-widget",
            "type": "data-table",
            "size": "large",
            "class": "light avanan-ap-links-table",
            "wrapper": {
                "type": "widget-panel",
                "title": "Found links",
                "class": {
                    "light white-body white-header no-inner-padding": true
                }
            },
            "logic": {
              "name": "solgateLinksLogic"
            },
            "bodyStyle": {
                "border-bottom": "none"
            },
            "columns": [{
                "id": "link",
                "text": "Link",
                "sortable": true
            }, {
                "id": "domain",
                "text": "Domain",
                "sortable": true
            }, {
                "id": "update_time",
                "text": "Update Time",
                "format": "time",
                "sortable": true
            }]
        };
    }

    return function($scope, logic) {

        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }

        if (!$scope.infoWidget) {
            $scope.infoWidget = infoWidget();
            $scope.infoWidget.logic = angular.copy(logic);
            $scope.infoWidget.logic.name = 'defaultScanDetailsLogic';
            $scope.infoWidget.uuid = $scope.model.uuid;
            $scope.infoWidget.modalWrapper = $scope.model.wrapper;
            $scope.model.widgets = [$scope.infoWidget];
        }


        var filtersWatch = $scope.$watch('infoWidget.wrapper.filters', function () {
            $scope.model.wrapper.filters = $scope.infoWidget.wrapper.filters;
        });
        var dataWatch = $scope.$watchCollection('infoWidget.data', function () {
            if ($scope.infoWidget.data) {
                $scope.model.wrapper.isLoading = false;
            }
            var data = $scope.infoWidget.entityData;
            if (data && troubleshooting.entityDebugEnabled() && !$scope.linksWidget) {
                $scope.linksWidget = linksWidget();
                $scope.linksWidget.logic.entityId = data.av_fd_aggregate_by_parent || data.file_entity_id;
                $scope.linksWidget.logic.entityType = data.file_entity_type;
                $scope.model.widgets.push($scope.linksWidget);
            }
        });



        return function () {
            filtersWatch();
            dataWatch();
        };
    };
});