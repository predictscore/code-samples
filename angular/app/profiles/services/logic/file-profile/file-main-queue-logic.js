var m = angular.module('profiles');

m.factory('fileMainQueueLogic', function(widgetPolling, fileDao, simpleSortableLogicCore, $q) {
    return function($scope, logic, reinit) {
        var id = widgetPolling.scope().id;

        var simpleFree = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.mainQueue($scope.model.columns, id);
            }
        });

        return function() {
            simpleFree();
        };
    };
});