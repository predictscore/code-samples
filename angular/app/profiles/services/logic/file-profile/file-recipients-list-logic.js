var m = angular.module('profiles');

m.factory('fileRecipientsListLogic', function(emailDao, widgetPolling, simpleLogicCore, fileDao) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        if(_($scope.messageIdPromise).isUndefined()) {
            $scope.messageIdPromise = fileDao.retrieve(id).then(function(response) {
                return response.data.message_id;
            });
        }
        
        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return $scope.messageIdPromise.then(function(messageId) {
                    return emailDao.recipientsList($scope.model.columns, messageId);
                });
            }
        });
    };
});
