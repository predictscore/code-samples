var m = angular.module('profiles');

m.factory('avananDlpTroubleshootingLogic', function(metaInfoLogicCore, fileDao, widgetPolling, $filter) {

    function formatValue(value) {
        if (_(value).isArray()) {
            return _(value).map(function (val) {
                return '#' + JSON.stringify({
                        'class': 'display-block',
                        display: 'text',
                        text: $filter('localTimeIfIsoString')('' + val)
                    });
            }).join('');
        } else {
            return $filter('localTimeIfIsoString')('' + value);
        }
    }

    return function($scope, logic) {
        var id = logic.entityId || widgetPolling.scope().id;

        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }
        if (_($scope.model.wrapper.isLoading).isUndefined()) {
            $scope.model.wrapper.isLoading = true;
        }

        fileDao.avananDlpTroubleshooting(id, logic.entityType).then(function (data) {
            $scope.model.wrapper.isLoading = false;
            $scope.model.lines = _(data.data).reduce(function (memo, value, key) {
                memo.push({
                    text: key + ':',
                    'class': 'avanan-dlp-troubleshooting-pair'
                });
                if (_(value).isObject()) {
                    _(value).each(function (value, key) {
                        memo.push({
                            text:  '#' + JSON.stringify({
                                display: 'text',
                                'class': 'key',
                                text: key + ':'
                            }) + '#' + JSON.stringify({
                                display: 'linked-text',
                                'class': 'value',
                                text: formatValue(value)
                            }),
                            'class': 'avanan-dlp-troubleshooting-pair'
                        });
                    });
                }

                return memo;
            }, []);
        });
    };
});