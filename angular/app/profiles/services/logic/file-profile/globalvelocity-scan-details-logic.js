var m = angular.module('profiles');

m.factory('globalvelocityScanDetailsLogic', function(fileScanDetailsLogicCore, fileDao, widgetPolling, metaInfoLogicCore) {
    return function($scope, logic) {
        var id = logic.entityId || widgetPolling.scope().id;

        var coreOptions = {
            retrieve: function() {
                return fileDao.globalvelocityScanDetails(id, logic.entityType, logic.historyTime);
            },
            advanced: logic.advanced,
            whiteList: logic.whiteList,
            blackList: logic.blackList || [],
            coreLogic: metaInfoLogicCore,
            moduleLabels: logic.moduleLabels,
            autoByEntityInfo: logic.autoByEntityInfo,
            extendEntityInfo: logic.extendEntityInfo,
            ordering: 'entityOrder'
        };
        return fileScanDetailsLogicCore($scope, logic, coreOptions);

    };
});