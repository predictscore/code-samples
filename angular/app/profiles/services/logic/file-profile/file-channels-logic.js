var m = angular.module('profiles');

m.factory('fileChannelsLogic', function(widgetPolling, simpleLogicCore, fileDao) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;

        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.fileChannels($scope.model.columns, id);
            }
        });
    };
});
