var m = angular.module('profiles');

m.factory('avananDlpScanDetailsLogic', function(fileScanDetailsLogicCore, fileDao, widgetPolling) {
    return function($scope, logic) {
        var id = logic.entityId || widgetPolling.scope().id;
        return fileScanDetailsLogicCore($scope, logic, {
            retrieve: function() {
                return fileDao.avananDlpScanDetails($scope.model.columns, id, logic.entityType);
            }
        });
    };
});