var m = angular.module('profiles');

m.factory('entityInfoLogic', function(widgetPolling, metaInfoLogicCore, fileDao, $rootScope, recheckParams) {

    return function($scope, logic) {
        var id = widgetPolling.scope().id;
        var entityType = widgetPolling.scope().entityType;
        var entityDisplayName = _(entityType.split('_')).last();
        var secTypes = widgetPolling.scope().rawEntityInfo.sec_type_connected;

        var buttons;
        if (secTypes && secTypes.length) {
            buttons = [{
                label: 'Recheck',
                'class': 'btn btn-blue',
                onClick: function () {
                    return recheckParams.show('Do you want to recheck this ' + entityDisplayName + '?', entityType, secTypes).then(function (data) {
                        return fileDao.recheck(entityType, id, data.selectApplications ? data.apps : undefined);
                    }).then(function () {
                        $rootScope.$broadcast('file-security-stack-reload');
                    });
                },
                advanced: true
            }];
        }

        var coreOptions = {
            autoWidget: true,
            ordering: 'widgetViewOrder',
            moduleLabels: {
                module: widgetPolling.scope().module,
                entity: entityType
            },
            retrieve: function() {
                return fileDao.entityInfo(entityType, id);
            },
            buttons: buttons
        };
        return metaInfoLogicCore($scope, logic, coreOptions);
    };
});
