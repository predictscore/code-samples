var m = angular.module('profiles');

m.factory('applicationUsersLogic', function(applicationDao, widgetPolling, simpleSortableLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;

        return simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return applicationDao.userList($scope.model.columns, id);
            }
        });
    };
});