var m = angular.module('profiles');

m.factory('applicationMetaInfoLogic', function(metaInfoLogicCore, widgetPolling, applicationDao) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;

        return metaInfoLogicCore($scope, logic, {
            advanced: true,
            whiteList: logic.whiteList, 
            entityType: 'application',
            autoWidget: true,
            ordering: 'widgetViewOrder',
            retrieve: function() {
                return applicationDao.retrieve(id);
            }
        });
    };
});