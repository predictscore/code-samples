var m = angular.module('profiles');

m.factory('projectBucketsLogic', function(widgetPolling, projectDao, simpleSortableLogicCore) {
    return function($scope, logic, reinit) {
        var id = widgetPolling.scope().id;
        var simpleFree = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return projectDao.buckets($scope.model.columns, id);
            }
        });

        return function() {
            simpleFree();
        };
    };
});