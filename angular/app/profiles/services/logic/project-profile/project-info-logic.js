var m = angular.module('profiles');

m.factory('projectInfoLogic', function($q, projectDao, mimeIcons, widgetPolling, modals) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        $q.all([
            projectDao.retrieve(id),
            mimeIcons()
        ]).then(function(responses) {
            var entityInfo = responses[0];
            var icons = responses[1];
            $scope.model.lines = [
                {
                    "img": icons('folder'),
                    "class": "profile-image"
                }, {
                    "text": entityInfo.data[logic.titleColumn],
                    "class": "folder-name"
                }
            ];

            _($scope.model.wrapper.buttons).find(function(button) {
                return button.id == 'show-meta-info';
            }).execute = function() {
                modals.widget({
                    panel: logic.metaInfo,
                    closeOnNavigate: true
                });
            };

        });
    };
});