var m = angular.module('profiles');

m.factory('projectMetaInfoLogic', function(widgetPolling, projectDao, metaInfoLogicCore) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;

        return metaInfoLogicCore($scope, logic, {
            advanced: logic.advanced,
            whiteList: logic.whiteList,
            blackList: logic.blackList,
            autoWidget: true,
            ordering: 'widgetViewOrder',
            entityType: 'project',
            retrieve: function() {
                return projectDao.retrieve(id);
            }
        });
    };
});