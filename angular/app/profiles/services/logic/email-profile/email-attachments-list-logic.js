var m = angular.module('profiles');

m.factory('emailAttachmentsListLogic', function(emailDao, widgetPolling, simpleLogicCore, defaultListConverter, $q, quarantineStatusConverter) {
    return function ($scope, logic, reinit) {
        var id = widgetPolling.scope().id;
        var originalId = widgetPolling.scope().preReleaseId;
        if (originalId) {
            id = originalId;
            if (!$scope.model.wrapper.originalTitle) {
                $scope.model.wrapper.originalTitle = $scope.model.wrapper.title;
            }
            $scope.model.wrapper.title = 'Original ' +$scope.model.wrapper.originalTitle;
        }

        var onActionUnsubscribe = widgetPolling.on('remediationActionPerformed', function () {
            logic.updateTick = 2000;
            logic.restoreUpdateTickAfter = moment().add(1,'minutes');
            reinit();
        });

        if (logic.restoreUpdateTickAfter && logic.restoreUpdateTickAfter.isBefore(moment())) {
            logic.updateTick = logic.originalUpdateTick;
            logic.restoreUpdateTickAfter = null;
        }

        var coreDestroy = simpleLogicCore($scope, logic, {
            retrieve: function() {
                return emailDao.attachmentsList($scope.model.columns, id, logic.entityType)
                    .converter(function (input, args) {
                        var insecureAttachments = _(input.data.rows).filter(function (row) {
                            return row.scanStatus;
                        });
                        if (!widgetPolling.scope().insecureAttachments ||
                            !angular.equals(widgetPolling.scope().insecureAttachments.rows, insecureAttachments)) {
                            widgetPolling.scope().insecureAttachments = {
                                rows: insecureAttachments,
                                mimeIcons: args.mimeIcons
                            };
                        }
                        return $q.when(input);
                    }, _.identity).converter(defaultListConverter, function(preFetches) {
                        return {
                            columns: $scope.model.columns,
                            mimeIcons: preFetches.mimeIcons,
                            converters: {
                                attachmentNameConverter: function(text, columnId, columnsMap, args) {
                                    var result = '#' + JSON.stringify({
                                            display: 'link',
                                            type: 'file',
                                            id: columnsMap[args.idColumn],
                                            text: text
                                        });
                                    if (args.quarantineStatusEntity) {
                                        var quarantineMsg = quarantineStatusConverter(args.quarantineStatusEntity, columnsMap);
                                        if (quarantineMsg) {
                                            result += '#' + JSON.stringify({
                                                    display: 'text',
                                                    'class': 'quarantined-msg',
                                                    text: quarantineMsg
                                                });
                                        }
                                    }
                                    return result;
                                }
                            }
                        };
                    });
            }
        });

        return function () {
            coreDestroy();
            onActionUnsubscribe();
        };
    };
});
