var m = angular.module('profiles');

m.factory('emailRestoreRequestLogic', function() {

    function localTimeWithAgo(text) {
        if (!text) {
            return '';
        }
        var time = moment.utc(text).local();
        return time.format('YYYY-MM-DD HH:mm:ss') + ' (' + time.fromNow() + ')';
    }

    return function ($scope, logic, reinit) {
        var dataWatch = $scope.$watch('model.entityData', function (data) {
            var lines = [];
            if (data && data.restoration_requested) {
                lines.push({
                    text: 'User requested email restore' + (data.restoration_request_dt ? ' on ' + '#' +JSON.stringify({
                        display: 'text',
                        text: localTimeWithAgo(data.restoration_request_dt),
                        'class': 'user-message'
                    }): ''),
                    'class': 'email-restore-date'
                });
                if (data.restore_commentary) {
                    lines.push({
                        text: 'User comment: ' + '#' + JSON.stringify({
                            display: 'text',
                            text: data.restore_commentary,
                            'class': 'user-message'
                        }),
                        'class': 'email-restore-text'
                    });
                } else {
                    lines.push({
                        text: 'User left no comment for the request',
                        'class': 'email-restore-text'
                    });
                }
                if (data.is_restore_declined) {
                    lines.push({
                        text: 'DECLINED' + (data.restore_decline_dt ? ' on ' + '#' +JSON.stringify({
                            display: 'text',
                            text: localTimeWithAgo(data.restore_decline_dt),
                            'class': 'user-message'
                        }): ''),
                        'class': 'email-restore-declined'
                    });
                }
            }
            if (!angular.equals($scope.model.lines, lines)) {
                $scope.model.lines = lines;
                if (lines.length > 0) {
                    $scope.model.style.display = 'block';
                } else {
                    $scope.model.style.display = 'none';
                }
            }
        });
        return dataWatch;
    };
});
