var m = angular.module('profiles');

m.factory('debugWebNotificationsLogic', function(troubleshooting) {
    return function ($scope, logic) {
        var dataWatcher = _.noop;
        if(troubleshooting.entityDebugEnabled()) {
            $scope.originalHeader = $scope.originalHeader || $scope.model.wrapper.title;
            $scope.model.wrapper.class['display-none'] = false;
            var toggleButton = _($scope.model.wrapper.buttons).find(function (button) {
                return button.id === 'toggle-notifications';
            });
            dataWatcher = $scope.$watch('model.widgets[0].totalRows', function (data) {
                if (_(data).isUndefined()) {
                    return;
                }
                $scope.model.wrapper.title = data + ' total notifications found';
            });

            if (toggleButton && !toggleButton.onclick) {
                toggleButton.onclick = function () {
                    if (!$scope.model.widgets || !$scope.model.widgets.length) {
                        $scope.model.widgets = [angular.copy($scope.model.listWidget)];
                        toggleButton.label = 'Hide notifications';
                        $scope.model.wrapper.title = 'Loading...';
                    } else {
                        $scope.model.widgets = [];
                        toggleButton.label = 'Show notifications';
                        $scope.model.wrapper.title = $scope.originalHeader;
                    }
                };
            }
        }
        return function () {
            dataWatcher();
        };
    };
});
