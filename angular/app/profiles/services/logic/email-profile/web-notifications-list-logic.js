var m = angular.module('profiles');

m.factory('webNotificationsListLogic', function(troubleshootDao, widgetPolling, simpleSortableLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        return simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return troubleshootDao.webNotifications($scope.model.columns, id);
            }
        });
    };
});
