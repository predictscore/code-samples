var m = angular.module('profiles');

m.factory('emailConversationListLogic', function(emailDao, widgetPolling, simpleLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return emailDao.conversationList($scope.model.columns, id);
            }
        });
    };
});
