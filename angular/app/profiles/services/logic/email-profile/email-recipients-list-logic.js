var m = angular.module('profiles');

m.factory('emailRecipientsListLogic', function(emailDao, widgetPolling, simpleLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return emailDao.recipientsList($scope.model.columns, id, true);
            }
        });
    };
});
