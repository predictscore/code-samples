var m = angular.module('profiles');

m.factory('emailMetaInfoLogic', function(widgetPolling, emailDao, metaInfoLogicCore, modals, modulesManager, entityTypes, fileDao, mainQueueTroubleshootingHelper, recheckParams, $rootScope, limitedArrayConverter, submitForAnalysisParams, suspiciousReportParams, troubleshooting) {
    function recheckEmail(entityType, entityId) {
        return recheckParams.show('Do you want to recheck this email?', entityType).then(function (data) {
            return fileDao.recheck(entityType, entityId, data.selectApplications ? data.apps : undefined);
        }).then(function () {
            $rootScope.$broadcast('file-security-stack-reload');
        });
    }

    function submitForAnalysis(entityType, entityId) {
        return submitForAnalysisParams.show('Submit email for analysis', 'email').then(function (data) {
            return fileDao.shareForAnalysis(entityType, entityId, 'email', data);
        });
    }

    function suspiciousReport(entityType, entityId) {
        return suspiciousReportParams.show('Report this email', entityType, entityId, {
            objectTypesData: widgetPolling.scope().objectTypes && widgetPolling.scope().objectTypes.data
        }).then(function (data) {
            return fileDao.suspiciousReport(entityType, entityId, data);
        });
    }


    return function($scope, logic, reinit) {
        var id = widgetPolling.scope().id;

        var onActionUnsubscribe = widgetPolling.on('remediationActionPerformed', function () {
            logic.updateTick = 2000;
            logic.restoreUpdateTickAfter = moment().add(1,'minutes');
            reinit();
        });

        if (logic.restoreUpdateTickAfter && logic.restoreUpdateTickAfter.isBefore(moment())) {
            logic.updateTick = logic.originalUpdateTick;
            logic.restoreUpdateTickAfter = null;
        }


        mainQueueTroubleshootingHelper.addTroubleshootingButton($scope.model.wrapper);

        var coreOptions = {
            advanced: logic.advanced,
            whiteList: logic.whiteList,
            blackList: logic.blackList,
            autoWidget: true,
            ordering: 'widgetViewOrder',
            entityType: 'email',
            extendEntityInfo: logic.extendEntityInfo,
            retrieve: function() {
                return emailDao.retrieve(id).after(function(response) {
                    $scope.isDeleted = response.data.is_deleted_message;
                    widgetPolling.scope().isDeleted = $scope.isDeleted;
                    if (logic.subjectTitle !== false) {
                        $scope.model.wrapper.title = response.data[logic.subjectField || 'Subject'];
                    }
                });
            },
            buttons: [{
                label: 'Recheck',
                'class': 'btn btn-blue',
                onClick: function () {
                    return recheckEmail(entityTypes.toBackend(modulesManager.currentModule(), 'email'), id);
                },
                advanced: true
            }, {
                label: 'Email headers',
                'class': 'btn btn-blue',
                onClick: function() {
                    return emailDao.retrieveHeader(id).then(function(response) {
                        return modals.widget({
                            panel: {
                                type: 'meta-info',
                                "wrapper": {
                                    "type": "widget-panel",
                                    "title": "Email headers",
                                    "class": "avanan white-body overflow-auto"
                                },
                                data: _(response.data).map(function(row) {
                                    return {
                                        key: row.key,
                                        value: limitedArrayConverter(row.value, {
                                            expanded: 10000,
                                            collapseRest: true
                                        })
                                    };
                                }),
                                "pair": {
                                    "class": "email-meta-data"
                                }
                            },
                            addCloseX: 'true'
                        }, 'lg');
                    }, function() {
                        var alertMessage = 'Headers are unavailable';
                        if($scope.isDeleted) {
                            alertMessage = 'Email is already deleted and headers are unavailable';
                        }
                        modals.alert(alertMessage, {
                            title: 'Headers are unavailable'
                        });
                    });
                }
            }, {
                label: 'Submit for analysis',
                'class': 'btn btn-blue',
                onClick: function () {
                    return submitForAnalysis(entityTypes.toBackend(modulesManager.currentModule(), 'email'), id);
                },
                advanced: true
            }]
        };

        if (troubleshooting.entityDebugEnabled()) {
            coreOptions.buttons.push({
                label: 'Report this email',
                'class': 'btn btn-blue',
                onClick: function () {
                    return suspiciousReport(entityTypes.toBackend(modulesManager.currentModule(), 'email'), id);
                },
                advanced: true
            });
        }

        var coreDestroy = metaInfoLogicCore($scope, logic, coreOptions);

        return function () {
            coreDestroy();
            onActionUnsubscribe();
        };
    };
});
