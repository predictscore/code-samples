var m = angular.module('profiles');

m.factory('emailCombinedInfoLogic', function(mainQueueTroubleshootingHelper) {
    return function($scope, logic) {
        mainQueueTroubleshootingHelper.addTroubleshootingButton($scope.model.wrapper);

        if (_($scope.model.wrapper.showLoading).isUndefined()) {
            $scope.model.wrapper.showLoading = true;
        }

        var titleWatch = $scope.$watch('model.widgets[' + logic.titleWidgetIndex + '].wrapper.title', function (newVal, oldVal) {
            if (newVal) {
                $scope.model.wrapper.title = $scope.model.widgets[logic.titleWidgetIndex].wrapper.title;
            }
        });

        function loadingUpdate() {
            $scope.model.wrapper.isLoading = _($scope.model.widgets).find(function (widget) {
                return widget.wrapper.isLoading;
            });
        }

        var watchEntityData = _.noop;
        var entityDataMasterIndex = _($scope.model.widgets).findIndex(function (widget) {
            return widget.logic.entityDataMaster;
        });
        if (entityDataMasterIndex !== -1) {
            watchEntityData = $scope.$watch('model.widgets[' + entityDataMasterIndex + '].entityData', function (info) {
              _($scope.model.widgets).each(function (widget) {
                  if (widget.logic.entityDataSlave) {
                      widget.entityData = $scope.model.widgets[entityDataMasterIndex].entityData;
                  }
              });
            });
        }


        var loadingWatches = _($scope.model.widgets).map(function (widget, idx) {
            return $scope.$watch('model.widgets[' + idx + '].wrapper.isLoading', loadingUpdate);
        });

        return function () {
            titleWatch();
            watchEntityData();
            _(loadingWatches).each(function (fn) {
                fn();
            });
        };
    };
});
