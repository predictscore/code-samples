var m = angular.module('profiles');

m.factory('emailEventsListLogic', function(emailDao, widgetPolling, simpleLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        var originalId = widgetPolling.scope().preReleaseId;
        if (originalId) {
            id = originalId;
            if (!$scope.model.wrapper.originalTitle) {
                $scope.model.wrapper.originalTitle = $scope.model.wrapper.title;
            }
            $scope.model.wrapper.title = $scope.model.wrapper.originalTitle + ' of original email';
        }

        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return emailDao.scanEvents($scope.model.columns, id,{
                    entityType: logic.entityType,
                    itemsEntityType: logic.itemsEntityType,
                    itemNameColumn: logic.itemNameColumn
                });
            }
        });
    };
});
