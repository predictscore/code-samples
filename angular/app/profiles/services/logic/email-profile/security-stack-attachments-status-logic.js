var m = angular.module('profiles');

m.factory('securityStackAttachmentsStatusLogic', function(widgetPolling, defaultListConverter, $q) {
    return function ($scope, logic, reinit) {
        var dataWatch = $scope.$watch(function () {
            return widgetPolling.scope().insecureAttachments;
        }, function (insecureAttachments) {
            if (!insecureAttachments || !insecureAttachments.rows || !insecureAttachments.rows.length) {
                $scope.model.size = 'display-none';
                $scope.model.data = [];
                return;
            }
            defaultListConverter({
                data: {
                    rows: insecureAttachments.rows
                }
            }, {
                columns: $scope.model.columns,
                mimeIcons: insecureAttachments.mimeIcons,
                converters: {
                    'attachmentScanStatusConverter': function (text) {
                        try {
                            var data = JSON.parse(text.slice(1));
                            data.tooltip.position.my = 'bottom right';
                            return '#' + JSON.stringify(data);
                        } catch (e) {
                            return text;
                        }
                    }
                }
            }).then(function (response) {
                $scope.model.data = response.data.data;
                $scope.model.size = '';
            });
        });
        return dataWatch;
    };
});
