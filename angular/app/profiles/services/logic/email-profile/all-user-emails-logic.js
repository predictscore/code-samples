var m = angular.module('profiles');

m.factory('allUserEmailsLogic', function(emailDao, simpleSortableLogicCore) {
    return function ($scope, logic) {
        return simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return emailDao.allUserEmails($scope.model.columns, logic.userId);
            }
        });
    };
});
