var m = angular.module('profiles');

m.factory('ipLocationLogic', function(widgetPolling, ipDao) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;

        ipDao.retrieve(id).then(function(response) {
            var data = response.data;
            if(_(data).isEqual({})) {
                return;
            }
            var location = {
                latLng: [data.geo_lat, data.geo_lon],
                metadata: {
                    ip: {
                        label: "IP address",
                        value: id
                    }
                }
            };

            if(_(data.geo_country).isString()) {
                location.metadata.country = {
                    label: "Country",
                    value: data.geo_country
                };
            }
            if(_(data.geo_city).isString()) {
                location.metadata.city = {
                    label: "City",
                    value: data.geo_city
                };
            }

            $scope.model.data = [location];
            if(_($scope.model.updateData).isFunction()) {
                $scope.model.updateData();
            }
        });
    };
});