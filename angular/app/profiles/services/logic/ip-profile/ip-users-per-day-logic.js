var m = angular.module('profiles');

m.factory('ipUsersPerDayLogic', function(ipDao, widgetPolling, simpleLogicCore) {
    return function ($scope, logic) {
        var id = widgetPolling.scope().id;
        return simpleLogicCore($scope, logic, {
            retrieve: function() {
                return ipDao.usersWithIpPerDay(id, 30, logic.timeColumnName);
            }
        });
    };
});