var m = angular.module('profiles');

m.factory('ipEventsListLogic', function(eventsDao, eventsListLogicCore, widgetPolling) {
    return function ($scope, logic) {
        var ipId = widgetPolling.scope().id;

        return eventsListLogicCore($scope, logic, {
            retrieve: function(after) {
                return eventsDao.retrieve($scope.model.columns, after, undefined, ipId);
            },
            retrieveTypes: function() {
                return eventsDao.retrieveTypes({
                    drive: true,
                    login: true
                });
            },
            retrieveTags: function() {
                return eventsDao.retrieveTags();
            },
            timeColumnName: logic.timeColumnName || 'time',
            filter: true
        });
    };
});