var m = angular.module('profiles');

m.factory('siteMetaInfoLogic', function(widgetPolling, siteDao, metaInfoLogicCore) {
    return function($scope, logic) {
        var id = widgetPolling.scope().id;

        return metaInfoLogicCore($scope, logic, {
            advanced: logic.advanced,
            whiteList: logic.whiteList,
            blackList: logic.blackList,
            autoWidget: true,
            ordering: 'widgetViewOrder',
            entityType: 'site',
            retrieve: function() {
                return siteDao.retrieve(id);
            }
        });
    };
});