var m = angular.module('profiles');

m.factory('remediationActionsLogic', function($routeParams, widgetPolling, objectTypeDao, modals, manualActionDao, remediationProperties, feature) {
    return function($scope, logic) {
        var fileId = widgetPolling.scope().id;
        var objectTypes = widgetPolling.scope().objectTypes;
        
        var actionTypes = _(objectTypes.data.actionTypes[logic.entityType]).chain().map(function(action) {
            return [action.id, action];
        }).object().value();
        _($scope.model.actions).each(function(action) {
            if(!action.actionType) {
                return;
            }
            if(action.feature && !feature[action.feature]) {
                return;
            }
            if (action.matchEntityType &&
                (!actionTypes[action.actionType] || !_(actionTypes[action.actionType].entities).contains(logic.entityType))) {
                return;
            }
            action.label = actionTypes[action.actionType] && actionTypes[action.actionType].label;
            action.label_disabled = actionTypes[action.actionType] && actionTypes[action.actionType].label_disabled;
            action.onClick = function() {
                var actionType = actionTypes[action.actionType];
                modals.params([{
                    params: _(actionType.attributes).map(function(param) {
                        return $.extend(true, {}, param);
                    }),
                    lastPage: true,
                    getProperties: _.memoize(remediationProperties(objectTypes.data.sources(logic.entityType)))
                }], actionType.label, actionType.size, {
                    noDataLabel: actionType.confirm
                }).then(function(data) {
                    var body = {
                        name: action.actionType,
                        attributes: data
                    };
                    manualActionDao.create(logic.entityType, fileId, body).then(function () {
                        widgetPolling.emit('remediationActionPerformed');
                    });

                });
            };
        });
    };
});