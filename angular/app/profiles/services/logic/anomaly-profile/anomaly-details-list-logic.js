var m = angular.module('profiles');

m.factory('anomalyDetailsListLogic', function(widgetPolling, anomalyDetectionDao, simpleSortableLogicCore) {
    return function($scope, logic, reinit) {
        var id = widgetPolling.scope().id;
        var anomalyEntity = widgetPolling.scope().anomalyEntity;
        var simpleFree = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return anomalyDetectionDao.anomalyDetailsList($scope.model.columns, id, anomalyEntity);
            }
        });

        return function() {
            simpleFree();
        };
    };
});