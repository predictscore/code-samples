var m = angular.module('profiles');

m.factory('pathConverters', function() {
    var pathConverters = {
        stringTree : function (ids, titlesString) {
            if (titlesString.length !== 1 || !titlesString[0]) {
                return '';
            }
            var titles = titlesString[0].split('/');
            if (titles.length > 0 && titles[0] === '') {
                titles.shift();
            }
            titles.pop();
            return '#' + JSON.stringify({
                    display: 'linked-text',
                    text: _(titles).map(function (title, idx) {
                        return '#' + JSON.stringify({
                                display: 'linked-text',
                                text: '/' + title,
                                'class': 'display-block',
                                style: {'padding-left': (idx * 5) + 'px'}
                            });
                    }).join(''),
                    'class': 'no-ws-small'
                });
        },
        tree : function (ids, titles, args, lastItemType) {
            return '#' + JSON.stringify({
                    display: 'linked-text',
                    text: _(titles).chain().map(function (title, idx) {
                        if (!ids[idx]) {
                            return;
                        }
                        return '#' + JSON.stringify({
                                display: 'link',
                                type: (idx === titles.length - 1) && lastItemType || 'folder',
                                id: ids[idx],
                                text: title || (args && args.noTitle) || ''
                            });
                    }).compact().value().join('/') || '/',
                    'class': 'no-ws-small'
                });
        },
        list: function (ids, titles, args) {
            return ids.length === 0 ? '/' : '#' + JSON.stringify({
                display: 'inline-list',
                data: _(ids).chain().map(function(id, idx) {
                    var title = titles[idx];
                    if (title === '' && args && args.noTitle) {
                        title = args.noTitle;
                    }
                    if(_(title).isString() && title.length) {
                        return '#' + JSON.stringify({
                                display: 'link',
                                text: title,
                                type: args && args.linkType || 'folder',
                                id: id
                            });
                    }
                    return null;
                }).filter(function(link) {
                    return !_(link).isNull();
                }).value(),
                countText: 'Parents'
            });
        },
        treeWithBucket : function (ids, titles, args) {
            var pathItems = _(titles).chain().map(function (title, idx) {
                if (!ids[idx]) {
                    return;
                }
                return '#' + JSON.stringify({
                        display: 'link',
                        type: (idx === 0) ? 'bucket' : 'folder',
                        id: ids[idx],
                        text: title || (args && args.noTitle) || ''
                    });
            }).compact().value();

            return '#' + JSON.stringify({
                    display: 'linked-text',
                    text: pathItems[0] + ': ' + (_(pathItems).rest().join('/') || '/'),
                    'class': 'no-ws-small'
                });
        },
        simplePath : function (ids, pathString) {
            if (pathString.length !== 1 || !pathString[0]) {
                return '';
            }
            return pathString[0].replace(/[^\/]+$/, '');
        }

    };
    return pathConverters;

});