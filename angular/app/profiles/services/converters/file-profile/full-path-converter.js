var m = angular.module('profiles');

m.factory('fullPathConverter', function($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var result = _(input.data.rows).map(function(dir) {
            return {
                title: dir.title,
                folderId: dir.entity_id
            };
        }).reverse();

        deferred.resolve($.extend({}, input, {
            data: result
        }));

        return deferred.promise;
    };
});