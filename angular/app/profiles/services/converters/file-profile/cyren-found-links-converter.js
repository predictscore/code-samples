var m = angular.module('profiles');

m.factory('cyrenFoundLinksConverter', function() {
    return function (value, args) {
        return '#' + JSON.stringify({
                display: 'linked-text',
                'class': 'cyren-found-links',
                text: _(value).map(function (categories, link) {
                    return '#' + JSON.stringify({
                            display: 'linked-text',
                            'class': 'cyren-found-link-element',
                            text: '#' + JSON.stringify({
                                display: 'text',
                                'class': 'cyren-link-categories',
                                text: categories.join(', ')
                            }) + ': #' + JSON.stringify({
                                display: 'text',
                                'class': 'cyren-link-link',
                                text: link,
                                tooltip: {
                                    content: {
                                        text: link
                                    },
                                    position: {
                                        my: 'bottom center',
                                        at: 'top center'
                                    },
                                    hide: {
                                        delay: 200,
                                        fixed: true
                                    },
                                    style: {
                                        classes: 'qtip-cyren-link'
                                    }
                                }
                            })
                        });
                }).join('')
            });
    };
});