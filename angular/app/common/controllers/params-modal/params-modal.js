var m = angular.module('common');

m.controller('ParamsModalController', function($scope, $modalInstance, pages, title, args, policyPropertyParser, $rootScope, commonDao, $timeout) {
    var paramTypes = {
        'string': {
            def: ''
        },
        'integer': {
            outConverter: function(param) {
                if (_(param.data).isNull()) {
                    return;
                }
                param.data = parseInt(param.data, 10);
            }
        },
        'password': {
            def: ''
        },
        'email-body': {
            def: ''
        },
        'email': {
            def: {
                to: '',
                cc: '',
                bcc: '',
                subject: '',
                body: ''
            }
        },
        'cef-items': {
            def: [],
            inConverter: function(param) {
                param.data = _(param.data).map(function(e) {
                    if(e.type == 'property') {
                        e.data = policyPropertyParser(e.data);
                    }
                    return e;
                });
            },
            outConverter: function(param) {
                param.data = _(param.data).map(function(e) {
                    if(e.type == 'property') {
                        e.data = e.data.fullId;
                    }
                    return e;
                });
            }
        },
        'property': {
            inConverter: function(param, page) {
                var input = param.data;

                var properties = page.getProperties(param.propertyType);
                if(properties[0].sub.length == 1) {
                    var firstProperty = properties[_(properties[0].sub).first()];
                    if(firstProperty.selectable){
                        input = firstProperty.id;
                        param.hidden = true;
                    }
                }

                if(_(input).isString()) {
                    input = input.replace(/^\{/, '').replace(/}$/, '');
                    if(input.length) {
                        input = '.' + input;
                    }
                }
                param.data = policyPropertyParser(input);
            },
            outConverter: function(param) {
                var input = param.data;
                if(_(input.fullId).isString()) {
                    if(input.fullId[0] == '.') {
                        param.data = '{' + input.fullId.slice(1) + '}';
                    } else {
                        param.data = '{' + input.fullId + '}';
                    }
                } else {
                    param.data = undefined;
                }
            }
        },
        'entity-type': {
            inConverter: function(param) {
                param.hidden = true;
            },
            outConverter: function(param) {
                var found = null;
                _(pageSeq).each(function(page) {
                    if(_(page).isUndefined()) {
                        return;
                    }
                    _($scope.pages[page].params).each(function(param) {
                        if(!found && param.type == 'property') {
                            found = param;
                        }
                    });
                });
                if(found) {
                    var property = found.data;
                    if(_(property).isObject()) {
                        property = property.fullId;
                    }
                    property = property.replace(/^\{/, '').replace(/}$/, '');
                    property = _(property.split('.')).last();
                    if(property.indexOf(':') != -1) {
                        property = _(property.split(':')).first();
                    }
                    param.data = property;
                } else {
                    param.data = undefined;
                } 
            }  
        },
        'boolean': {
            def: false
        },
        'object-of-arrays': {
            inConverter: function (param) {
              if (!_(param.data).isObject()) {
                  param.data = {};
              }
            },
            outConverter: function (param) {
                if (!param.keysParam || !param.keysParam.data || !_(param.keysParam.data).isArray()) {
                    param.data = {};
                }
                var newData = {};
                _(param.keysParam.data).each(function (key) {
                    newData[key] = param.data[key];
                });
                param.data = newData;
            }
        },
        'list-as-csv': {
            inConverter: function (param) {
                if (!param.data || !param.data.split) {
                    param.data = [];
                } else {
                    param.data = param.data.split(',');
                }
            },
            outConverter: function (param) {
                param.data = param.data.join(',');
            }
        },
        'list': {
            outConverter: function (param) {
                if (param.settings && param.settings.multiple === false && !param.data) {
                    param.data = '';
                }
            }
        }
    };
    var resolveOnClose = [];
    function resolveTimeoutsOnClose() {
        _(resolveOnClose).each(function (d) {
            d.resolve();
        });
    }
    if (args && args.paramConfirm) {
        $scope.paramConfirmNow = function (param) {
            resolveTimeoutsOnClose();
            $modalInstance.dismiss();
            return args.paramConfirm(param);
        };
    }

    $scope.paramConfirmCancel = function (param) {
        param.confirmPending = false;
    };

    $scope.title = title;
    $scope.args = $.extend({
        noDataLabel: 'Are you sure you want to continue?'
    }, args);

    $scope.pages = _(pages).map(function(page) {
        var sections = {};
        _(page.params).each(function (param) {
           if (param.type === 'section-label') {
               sections[param.id] = {
                   label: param.label,
                   opened: false,
                   params: []
               };
           }
        });
        var params = [];
        _(page.params).each(function(param) {
            param = $.extend({}, param, true);
            addPagesConfigWatch(param);
            if (!_(paramTypes[param.type]).isUndefined()) {
                if ((_(param.data).isUndefined() || _(param.data).isNull()) && !_(paramTypes[param.type].def).isUndefined()) {
                    param.data = paramTypes[param.type].def;
                }
                var inConverter = paramTypes[param.type].inConverter || _.identity;
                inConverter(param, page);
            }
            param.originalValue = param.data;

            if (param.resolve) {
                param.resolve().then(function (newParam) {
                    $.extend(param, newParam);
                });
            }
            if (param.resolveTimeout) {
                resolveOnClose.push(param.resolveTimeout);
            }

            if (param.section && sections[param.section]) {
                sections[param.section].params.push(param);
            } else {
                params.push(param);
            }
        });


        return $.extend({}, page, {
            params: params,
            sections: sections
        });
    });

    function updateParams(params) {
        _(params).each(function (param) {
            if (param.keysParamId) {
                param.keysParam = findParamOnAllPages(param.keysParamId);
            }
            if (param.widget) {
                param.widget.selfParam = param;
            }
            if (param.widget && param.refParams) {
                param.widget.refParams = _(param.refParams).chain().map(function (paramId) {
                    var foundParam = findParamOnAllPages(paramId);
                    return foundParam && [paramId, foundParam];
                }).compact().object().value();
            }
        });
    }
    _($scope.pages).each(function (page) {
        updateParams(page.params);
        _(page.sections).each(function (section) {
            updateParams(section.params);
        });
    });


    var pageSeq = [0, $scope.pages[0].nextPage];
    var pageSeqNumber = 0;

    $scope.okButton = function () {
        return $scope.pages[$scope.page()].okButton || (args && args.okButton) || 'Ok';
    };

    $scope.radioClick = function(button) {
        if(!_(button.nextPage).isUndefined()) {
            if(pageSeq[pageSeqNumber + 1] != button.nextPage) {
                pageSeq = pageSeq.slice(0, pageSeqNumber + 1);
                pageSeq.push(button.nextPage);
            }
        }
    };

    function setParamValidationStatus (param, status) {
        param.onchange_validation.status = status;
        var defaultTips = {
            'notchanged': null,
            'empty': null,
            'valid': '{{value}} is valid',
            'invalid': '{{value}} is invalid',
            'progress': 'Verifying...'
        };

        var statusTip = param.onchange_validation.statuses &&
            param.onchange_validation.statuses[param.onchange_validation.status] ||
            defaultTips[param.onchange_validation.status];
        param.onchange_validation.tooltip = null;
        $timeout(function () {
            if (param.onchange_validation.status !== 'invalid') {
                param.onchange_validation.error = '';
            }
            if (!statusTip) {
                param.onchange_validation.tooltip = null;
                return;
            }
            param.onchange_validation.tooltip = {
                content: statusTip.replace('{{value}}', '<strong>' + param.data + '</strong>'),
                show: {
                    solo: true
                }
            };
            if (param.onchange_validation.status === 'invalid') {
                param.onchange_validation.error = statusTip.replace('{{value}}', '"' + param.data + '"');
            }
        });
    }

    function addPagesConfigWatch(param) {
        if (param.nextPages || param.onChange || param.onchange_validation) {
            $scope.$watch(function () {
                return param.data;
            }, function (newValue, oldValue) {
                if (param.nextPages && !_(param.nextPages[newValue]).isUndefined()) {
                    if(pageSeq[pageSeqNumber + 1] != param.nextPages[newValue]) {
                        pageSeq = pageSeq.slice(0, pageSeqNumber + 1);
                        pageSeq.push(param.nextPages[newValue]);
                    }
                }
                if (param.onChange && newValue !== oldValue) {
                    param.onChange($scope, newValue, oldValue);
                }
                if (param.onchange_validation && newValue !== oldValue) {
                    if (!newValue) {
                        param.onchange_validation.status = param.onchange_validation.empty_status || 'empty';
                        return;
                    }
                    if (!param.onchange_validation.api) {
                        setParamValidationStatus(param, 'empty');
                        return;
                    }
                    setParamValidationStatus(param, 'progress');
                    return commonDao.genericVerify(param.onchange_validation.api, newValue).then(function (response) {
                        setParamValidationStatus(param, 'valid');
                    }, function (response) {
                        setParamValidationStatus(param, 'invalid');
                    });
                }
            });
            if (param.onchange_validation) {
                param.modelOptions = {
                    updateOn: 'default blur',
                    debounce: {
                        default: 1500,
                        blur: 0
                    }
                };
                setParamValidationStatus(param, 'notchanged');
            }
        }
    }

    $scope.addToList = function(param, type) {
        param.data = param.data || [];
        param.data.push({
            type: type
        });
    };

    $scope.removeFromList = function(param, index) {
        param.data.splice(index, 1);
    };

    $scope.isVisibleParamsExist = function(params) {
        var visibleParam = _(params).find(function(param) {
            return !param.hidden;
        });
        return !_(visibleParam).isUndefined();
    };
    if (args && args.dismissOn) {
        $scope.$on(args.dismissOn, function (event, args) {
            resolveTimeoutsOnClose();
            $modalInstance.dismiss();
        });
    }


    $scope.cancel = function() {
        resolveTimeoutsOnClose();
        $modalInstance.dismiss();
    };
    $scope.executeButton = function (button) {
        resolveTimeoutsOnClose();
        $modalInstance.dismiss();
        button.execute();
    };

    function validateParam (param) {
        if (!$scope.isParamEnabled(param, $scope.pages[$scope.page()])) {
            return false;
        }
        var data = param.data;
        if (param.onchange_validation && param.onchange_validation.status === 'progress') {
            param.error = 'Value verification is in progress, please wait';
            return true;
        }

        var error = _(param.validation || {}).find(function(value, key) {
            var message, error;
            if(_(value).isObject()) {
                message = value.message;
                value = value.value;
            }
            if(key === 'required' && (_(data).isUndefined() || _(data).isNull() || data === '' || (_(data).isArray() && data.length === 0))) {
                error =  message || ('Field required');
            }
            if(key === 'defined' && (_(data).isUndefined())) {
                error =  message || ('Field required');
            }
            if(key === 'minLength' && (!_(data).isString() || data.length < value)) {
                error =  message || ('Minimum ' + value + ' symbols');
            }
            if(key === 'maxLength' && (!_(data).isString() || data.length > value)) {
                error =  message || ('Maximum ' + value + ' symbols');
            }
            if(key === 'regexp' && _(data).isString() && !data.match(value)) {
                error = message || ('Regexp ' + value + ' is not pass');
            }
            if(key === 'equalTo') {
                var equalToParam = $scope.findPageParamById(value);
                if (equalToParam && equalToParam.data !== data) {
                    error = message || ('Value should match value of ' + equalToParam.label);
                }
            }
            if (key === 'notInvalid' && param.onchange_validation && param.onchange_validation.status === 'invalid') {
                error = message || 'Invalid values are not allowed';
            }
            param.error = error;
            return !_(error).isUndefined();
        });
        return !_(error).isUndefined();
    }

    function findParamOnAllPages (id) {
        var foundParam;
        _($scope.pages).find(function (page) {
            foundParam = findParamOnPage(id, page);
            return foundParam;
        });
        return foundParam;
    }

    function findEnabledParamOnAllPages (id) {
        var foundParam;
        var foundPage;
        _($scope.pages).find(function (page) {
            foundParam = findParamOnPage(id, page);
            foundPage = page;
            return foundParam;
        });
        if (!foundParam || !$scope.isParamEnabled(foundParam, foundPage)) {
            return false;
        }
        return foundParam;
    }

    function findParamOnPage (id, page) {
        var found;
        _(page.params).find(function (param) {
            if (param.type === 'section-label') {
                return _(page.sections[param.id].params).find(function (param) {
                    if (param.id === id) {
                        found = param;
                        return true;
                    }
                    return false;
                });
            }
            if (param.id === id) {
                found = param;
                return true;
            }
            return false;
        });
        return found;
    }

    $scope.findPageParamById = function (id) {
        return findParamOnPage(id, $scope.pages[$scope.page()]);
    };

    function validate() {
        var result = _($scope.pages[$scope.page()].params).find(function(param) {
            if (param.type === 'widget') {
                if (param.widget && param.widget.validate) {
                    return !param.widget.validate(findParamOnAllPages, findEnabledParamOnAllPages);
                }
                return false;
            }
            if (param.type != 'section-label') {
                return validateParam(param);
            } else {
                var error = _($scope.pages[$scope.page()].sections[param.id].params).find(function (param) {
                    return validateParam(param);
                });
                return !_(error).isUndefined();
            }
        });
        return _(result).isUndefined();
    }

    function reduceParam (memo, param) {
        if(!_(param.id).isUndefined() && !param.readOnly) {
            if (!_(paramTypes[param.type]).isUndefined()) {
                var outConverter = paramTypes[param.type].outConverter || _.identity;
                outConverter(param);
            }
            memo[param.id] = param.data;
        }
        return memo;
    }

    $scope.skip = function () {
        resolveTimeoutsOnClose();
        $modalInstance.close();
    };

    $scope.showSkip = (args && args.showSkip);

    $scope.ok = function(additionalData) {
        if(!validate()) {
            return;
        }

        var dialogParams =  _(pageSeq).reduce(function(memo, page) {
            if(_(page).isUndefined() || _(page).isNull()) {
                return memo;
            }
            return _($scope.pages[page].params).reduce(function(memo, param){
                if (param.broadcastOnSave) {
                    $rootScope.$broadcast(param.broadcastOnSave);
                }
                if (param.type === 'widget' && param.widget && param.widget.onsave) {
                    param.widget.onsave($scope.isParamEnabled(param, $scope.pages[page]));
                }
                if (param.type !== 'widget' && !_(param.id).isUndefined()) {
                    if($scope.isParamEnabled(param, $scope.pages[page])) {
                        if (param.type == 'section-label') {
                            _($scope.pages[page].sections[param.id].params).reduce(function (memo, param) {
                                if($scope.isParamEnabled(param, $scope.pages[page])) {
                                    return reduceParam(memo, param);
                                } else {
                                    if (!_(param.disabledValue).isUndefined()) {
                                        memo[param.id] = param.disabledValue;
                                    }
                                }
                                return memo;
                            }, memo);
                        } else {
                            reduceParam(memo, param);
                        }
                    } else {
                        if (!_(param.disabledValue).isUndefined()) {
                            memo[param.id] = param.disabledValue;
                        }
                    }
                }
                return memo;
            }, memo);
        }, additionalData || {});

        if (args && args.onCloseAction) {
            $scope.isLoading = true;
            return args.onCloseAction(dialogParams).then(function (response) {
                $scope.isLoading = false;
                resolveTimeoutsOnClose();
                $modalInstance.close(response);
            }, function (response) {
                $scope.isLoading = false;
                if (args && args.onCloseActionError) {
                    args.onCloseActionError(response);
                }
            });
        }
        resolveTimeoutsOnClose();
        $modalInstance.close(dialogParams);
    };

    $scope.page = function() {
        return pageSeq[pageSeqNumber];
    };

    $scope.prev = function() {
        if(!$scope.isFirstPage()) {
            pageSeqNumber--;
        }
    };

    $scope.next = function() {
        if(!validate()) {
            return;
        }
        if($scope.isNextPageEnable()) {
            pageSeqNumber++;
            if(pageSeqNumber == pageSeq.length - 1) {
                pageSeq.push($scope.pages[$scope.page()].nextPage);
            }
        }
    };

    $scope.isNextPageEnable = function() {
        return !_(pageSeq[pageSeqNumber + 1]).isNull();
    };

    $scope.isFirstPage = function() {
        return pageSeqNumber === 0;
    };

    $scope.isLastPage = function() {
        return $scope.pages[$scope.page()].lastPage || pageSeq[pageSeqNumber + 1] == $scope.page();
    };

    $scope.isSectionVisible = function (section, page) {
        return _(section.params).find(function (param) {
            return $scope.isParamVisible(param, page);
        });
    };

    $scope.isParamVisible = function (param, page) {
        if (!$scope.isParamEnabled(param, page)) {
            return false;
        }
        if (param.visible === false) {
            return false;
        }

        if (!_(param.visible).isObject()) {
            return true;
        }
        var checkArray = _(param.visible).isArray() ? param.visible : [param.visible];

        var checked = _(checkArray).map(function (check) {
            if (check.alwaysHidden) {
                return false;
            }
            if (!check.param) {
                return true;
            }
            var refParam = findParamOnPage(check.param, page);
            if (refParam && !_(check.equal).isUndefined()) {
                return refParam.data === check.equal;
            }
            if (refParam && check.in) {
                return _(check.in).contains(refParam.data);
            }
            if (refParam && check.notEmpty) {
                return !!refParam.data;
            }
            if (refParam && check.notChanged) {
                return refParam.data === refParam.originalValue;
            }
            return true;
        });
        return _(checked).chain().find(function (check) {
            return check === false;
        }).isUndefined().value();

    };

    $scope.isParamEnabled = function (param, page) {
        if (param.keysParamId) {
            if (!param.keysParam || !param.keysParam.data || !param.keysParam.data.length || !_(param.keysParam.data).isArray()) {
                return false;
            }
        }
        if (param.enabled === false) {
            return false;
        }
        if (!_(param.enabled).isObject()) {
            return true;
        }
        var checkArray = _(param.enabled).isArray() ? param.enabled : [param.enabled];

        var checked = _(checkArray).map(function (check) {
            if (check.alwaysHidden) {
                return false;
            }
            if (!check.param) {
                return true;
            }
            var refParam = findParamOnPage(check.param, page);
            if (!refParam) {
                return true;
            }
            var paramData = refParam.data;
            if (!$scope.isParamEnabled(refParam, page)) {
                if (_(param.disabledValue).isUndefined()) {
                    return false;
                }
                paramData = param.disabledValue;
            }
            if (!_(check.equal).isUndefined()) {
                return paramData === check.equal;
            }
            if (check.in) {
                return _(check.in).contains(paramData);
            }
            if (check.notEmpty) {
                return !!paramData;
            }
            return true;
        });
        return _(checked).chain().find(function (check) {
            return check === false;
        }).isUndefined().value();
    };

    $scope.resetParam = function (param) {
        param.data = param.originalValue;
    };

    $scope.closeWithForceConfirm = function () {
        return $scope.ok({_forceConfirm: true});
    };

    if (args && args.focusOn) {
        var param = findParamOnPage(args.focusOn, $scope.pages[$scope.page()]);
        if (param.confirmPending) {
            param.confirmPending = false;
        }
        param.focusOnLoad = true;
        if (param && param.section) {
            $scope.pages[$scope.page()].sections[param.section].opened = true;
        }
    }

    if (args && args.closeOnNavigate) {
        $scope.$on('$routeChangeSuccess', function (event, data) {
            resolveTimeoutsOnClose();
            $modalInstance.dismiss('routeChanged');
        });
    }

});

m.factory('paramsModal', function($modal, path) {
    return {
        show: function(pages, title, size, args) {
            var modalConfig = {
                templateUrl: path('common').ctrl('params-modal').template(),
                controller: 'ParamsModalController',
                size: size,
                resolve: {
                    pages: _.constant(pages),
                    title: _.constant(title),
                    args: _.constant(args)
                }
            };
            if (args && args.windowClass) {
                modalConfig.windowClass = args.windowClass;
            }
            var modalInstance = $modal.open(modalConfig);
            return modalInstance.result;
        }
    };
});
