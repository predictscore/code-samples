var m = angular.module('common');

m.controller('SmartSearchRuleModalController', function($scope, $modalInstance, rule, options, modals, dlpRuleBuilder) {
    // Notice: password_protected mode is now deprecated.
    // Right now only html code of it is commented out. All other code stays to support transition period while such rules can still exists on existing servers
    // After transition period, everything about this mode can be completely removed
    $scope.options = options;

    $scope.ok = function() {
        $scope.model.type = modeToType[$scope.model.ui_info.mode];
        if (validate()) {
            $modalInstance.close($scope.model);
        }
    };

    $scope.close = function() {
        $modalInstance.dismiss();
    };

    function validate() {
        $scope.errors = {};
        if (!$scope.model.name) {
            $scope.errors.name = 'Name is required';
        }
        if ($scope.model.type !== 'password_protected' && !$scope.model.value) {
            $scope.errors.value = 'Query is required';
        }
        return _($scope.errors).isEmpty();
    }

    var modeToType = {
        'builder': 'query_language',
        'raw': 'query_language',
        'regexp': 'regexp',
        'password_protected': 'password_protected'
    };

    var ui_info = $.extend(true, {
            mode: 'builder',
            builder: {}
        }, rule.ui_info);

    $scope.model = {
        name: rule.name,
        description: rule.description,
        ui_info: ui_info,
        value: rule.value,
        is_active: rule.is_active || false,
        is_predefined: rule.is_predefined
    };
    if (rule.type === 'regexp') {
        $scope.model.ui_info.mode = 'regexp';
    }

    if (rule.type === 'password_protected') {
        $scope.model.ui_info.mode = 'password_protected';
    }


    $scope.$watchCollection('model.ui_info.builder', function () {
        if ($scope.model.ui_info.mode === 'builder') {
            $scope.model.value = dlpRuleBuilder($scope.model.ui_info.builder);
        }
    });

    $scope.$watchCollection('model.ui_info', function (newVal, oldVal) {
        if (newVal.mode === 'builder' && oldVal.mode !== 'builder') {
            var built = dlpRuleBuilder($scope.model.ui_info.builder);
            if ($scope.model.value && $scope.model.value !== built) {
                modals.confirm('Your current query will be reset by builded query. Are you sure you want to continue?')
                    .then(function () {
                        $scope.model.value = built;
                    }, function () {
                        $scope.model.ui_info.mode = oldVal.mode;
                    });
            }
        }
    });
});

m.factory('smartSearchRuleModal', function($modal, path) {
    return {
        show: function(rule, options, size) {
            var modalInstance = $modal.open({
                templateUrl: path('common').ctrl('smart-search-rule-modal').template(),
                controller: 'SmartSearchRuleModalController',
                size: size,
                backdrop: _(options).isObject() && !_(options.backdrop).isUndefined() ? options.backdrop : true,
                keyboard: _(options).isObject() && !_(options.keyboard).isUndefined() ? options.keyboard : true,
                resolve: {
                    rule: _.constant(rule),
                    options: _.constant(options)
                }
            });
            return modalInstance.result;
        }
    };
});