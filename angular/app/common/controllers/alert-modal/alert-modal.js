var m = angular.module('common');

m.controller('AlertModalController', function($scope, $modalInstance, message, options, modalSingletons) {
    if(!_(message).isArray()) {
        message = [message];
    }
    $scope.messages = message;
    $scope.options = $.extend({
        display: 'text'
    }, options, true);

    $scope.close = function() {
        $modalInstance.close();
        modalSingletons.close(options.singleton);
    };
});

m.factory('alertModal', function($modal, path, modalSingletons) {
    return {
        show: function(message, options, size) {
            options = $.extend(true, {}, options);
            if(options.singleton) {
                if(modalSingletons.isOpen(options.singleton)) {
                    return modalSingletons.getInstance(options.singleton);
                }
            }
            var modalInstance = $modal.open({
                templateUrl: path('common').ctrl('alert-modal').template(),
                controller: 'AlertModalController',
                size: size,
                backdrop: _(options).isObject() && !_(options.backdrop).isUndefined() ? options.backdrop : true,
                keyboard: _(options).isObject() && !_(options.keyboard).isUndefined() ? options.keyboard : true,
                resolve: {
                    message: _.constant(message),
                    options: _.constant(options)
                },
                windowClass: options.windowClass
            });
            if(options.singleton) {
                modalSingletons.open(options.singleton, modalInstance.result);
            }
            return modalInstance.result;
        }
    };
});