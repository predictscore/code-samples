var m = angular.module('common');

m.controller('KeyValueGraphModalController', function($scope, $modalInstance, model, options, args) {
    $scope.args = $.extend({
        title: 'Key value graph',
        okText: 'Save',
        cancelText: 'Cancel'
    }, args);
    
    $scope.model = model;
    $scope.options = options;

    $scope.cancel = function() {
        $modalInstance.dismiss($scope.model);
    };

    $scope.ok = function() {
        $modalInstance.close($scope.model);
    };
});

m.factory('keyValueGraphModal', function($modal, path) {
    return {
        show: function(model, options, args, size) {
            if(!_(args).isObject()) {
                args = {
                    message: args
                };
            }
            var modalInstance = $modal.open({
                templateUrl: path('common').ctrl('key-value-graph-modal').template(),
                controller: 'KeyValueGraphModalController',
                size: size,
                resolve: {
                    args: _.constant(args),
                    model: _.constant($.extend(true, {}, model)),
                    options: _.constant(options)
                }
            });
            return modalInstance.result;
        }
    };
});