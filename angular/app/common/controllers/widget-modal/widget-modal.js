var m = angular.module('common');

m.controller('WidgetModalController', function($scope, $modalInstance, message, options, $route) {
    $scope.message = message;
    $scope.options = $.extend({
        display: 'text'
    }, options, true);

    $scope.close = function() {
        $modalInstance.close();
    };

    var removeButtons = false;
    var removeCloseButton = false;

    if (options.addCloseX) {
        if (!options.panel.wrapper.buttons) {
            removeButtons = true;
            options.panel.wrapper.buttons = [];
        }
        if (!_(options.panel.wrapper.buttons).find(function (button) {
                return button.id === 'modal-close-x';
            })) {
            removeCloseButton = true;
            options.panel.wrapper.buttons.push({
                "class": "btn-icon widget-modal-close-x",
                "iconClass": "fa fa-times",
                "id": "modal-close-x",
                execute: function () {
                    $scope.close();
                }
            });
        }
    }

    $scope.$on('$destroy', function () {
        if (removeButtons) {
            delete options.panel.wrapper.buttons;
        } else {
            if (removeCloseButton) {
                var closeIdx = _(options.panel.wrapper.buttons).findIndex(function (button) {
                    return button.id === 'modal-close-x';
                });
                if (closeIdx !== -1) {
                    options.panel.wrapper.buttons.splice(closeIdx, 1);
                }
            }
        }
    });


    if (options.closeOnNavigate) {
        var routeChangeWatcher = $scope.$watch(function () {
            return $route.current;
        }, function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $modalInstance.close();
            }
        });
        $scope.$on('$destroy', function () {
            routeChangeWatcher();
        });
    }
});

m.factory('widgetModal', function($modal, path) {
    return {
        show: function(options, size) {
            var modalConfig = {
                templateUrl: path('common').ctrl('widget-modal').template(),
                controller: 'WidgetModalController',
                size: size,
                backdrop: _(options).isObject() && !_(options.backdrop).isUndefined() ? options.backdrop : true,
                keyboard: _(options).isObject() && !_(options.keyboard).isUndefined() ? options.keyboard : true,
                resolve: {
                    options: _.constant(options)
                }
            };
            if (options.windowClass) {
                modalConfig.windowClass = options.windowClass;
            }
            var modalInstance = $modal.open(modalConfig);
            return modalInstance.result;
        }
    };
});