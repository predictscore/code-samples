var m = angular.module('common');

m.controller('ConfirmModalController', function($scope, $modalInstance, args) {
    $scope.args = $.extend({
        title: 'Confirmation dialog',
        okText: 'Ok',
        cancelText: 'Cancel',
        remember: null
    }, args);
    
    $scope.result = {
        remember: false
    };

    if (_(args.okClass).isObject()) {
        $scope.okClass = $.extend({
            'btn': true,
            'btn-sm': true
        }, args.okClass);
    } else {
        if (_(args.okClass).isString()) {
            $scope.okClass = 'btn btn-sm ' + args.okClass;
        } else {
            $scope.okClass = 'btn btn-sm btn-success';
        }
    }

    if (_(args.cancelClass).isObject()) {
        $scope.cancelClass = $.extend({
            'btn': true,
            'btn-sm': true
        }, args.cancelClass);
    } else {
        if (_(args.cancelClass).isString()) {
            $scope.cancelClass = 'btn btn-sm ' + args.cancelClass;
        } else {
            $scope.cancelClass = 'btn btn-sm btn-danger';
        }
    }


    $scope.cancel = function() {
        $modalInstance.dismiss($scope.result);
    };

    $scope.ok = function() {
        $modalInstance.close($scope.result);
    };
});

m.factory('confirmModal', function($modal, path) {
    return {
        show: function(args, size) {
            if(!_(args).isObject()) {
                args = {
                    message: args
                };
            }
            var modalInstance = $modal.open({
                templateUrl: path('common').ctrl('confirm-modal').template(),
                controller: 'ConfirmModalController',
                size: size,
                resolve: {
                    args: _.constant(args)
                }
            });
            return modalInstance.result;
        }
    };
});