var m = angular.module('common');

m.controller('TableModalController', function($scope, $modalInstance, args, tablePaginationHelper) {
    $scope.args = $.extend({
        title: 'Table dialog',
        okText: 'Ok'
    }, args);

    $scope.tableHelper = tablePaginationHelper($scope, $scope.args.tableOptions, $scope.args.options, function(args) {
        return $scope.args.retrieve(args);
    }, function(tableModel) {
        $scope.tableModel = tableModel;
    });
    
    _($scope.args.on).each(function(listener, eventName) {
        $scope.$on(eventName, function(event, data) {
            listener(event, data, $scope);
        });
    });

    $scope.cancel = function() {
        $modalInstance.dismiss();
    };

    $scope.ok = function() {
        $modalInstance.close($scope.tableHelper.getSelected());
    };

    $scope.haveSelection = function() {
        return $scope.tableHelper.getSelected().length > 0;
    };

    if (args.closeOnNavigate) {
        $scope.$on('$routeChangeSuccess', function (event, data) {
            $modalInstance.dismiss();
        });
    }

});

m.factory('tableModal', function($modal, path) {
    return {
        show: function(args, size) {
            if(!_(args).isObject()) {
                args = {
                    message: args
                };
            }
            var modalInstance = $modal.open({
                templateUrl: path('common').ctrl('table-modal').template(),
                controller: 'TableModalController',
                size: size,
                resolve: {
                    args: _.constant(args)
                }
            });
            return modalInstance.result;
        }
    };
});