var m = angular.module('common');

m.controller('ConditionsEditorModalController', function($scope, $modalInstance, conditions, options) {
    $scope.conditions = conditions;
    $scope.options = options;

    $scope.ok = function() {
        if($scope.conditions.check()) {
            $modalInstance.close($scope.conditions);
        }
    };
    
    $scope.close = function() {
        $modalInstance.dismiss();
    };
});

m.factory('conditionsEditorModal', function($modal, path) {
    return {
        show: function(conditions, options, size) {
            var modalInstance = $modal.open({
                templateUrl: path('common').ctrl('conditions-editor-modal').template(),
                controller: 'ConditionsEditorModalController',
                size: size,
                backdrop: _(options).isObject() && !_(options.backdrop).isUndefined() ? options.backdrop : true,
                keyboard: _(options).isObject() && !_(options.keyboard).isUndefined() ? options.keyboard : true,
                resolve: {
                    conditions: _.constant($.extend(true, {}, conditions)),
                    options: _.constant(options)
                }
            });
            return modalInstance.result;
        }
    };
});