var m = angular.module('common');

m.factory('configCache', function($q, configDao) {
    var cachedValues = {};
    var configCache = {
        get: function (paramName) {
            if (_(cachedValues[paramName]).isUndefined()) {
                return configCache.reload(paramName);
            }
            return $q.when(cachedValues[paramName]);
        },
        reload: function (paramName) {
            return configDao.param(paramName, true).then(function (value) {
                cachedValues[paramName] = value;
                return value;
            });
        },
        purge: function () {
            cachedValues = {};
        }
    };
    return configCache;
});