var m = angular.module('common');

m.factory('timeVariants', function() {
    var weekDaysVariants = _(['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']).map(function(day) {
        return {
            id: day.toLowerCase(),
            label: day
        };
    });
    var monthWeeksVariants = _(['First', 'Second', 'Third', 'Fourth']).map(function(week, weekNumber) {
        return {
            id: weekNumber + 1,
            label: week
        };
    });
    function makeApi(variants) {
        return {
            getVariants: function() {
                return variants;
            },
            getLabelById: function(id) {
                var found = _(variants).find(function(v) {
                    return v.id == id;
                });
                return found ? found.label : null;
            }
        };
    }
    return {
        weekDays: makeApi(weekDaysVariants),
        monthWeeks: makeApi(monthWeeksVariants)
    };
});