var m = angular.module('common');

m.factory('modals', function(confirmModal, alertModal, paramsModal, tableModal, treeModal, widgetModal, conditionsEditorModal, smartSearchRuleModal, keyValueGraphModal) {
    return {
        confirm: confirmModal.show,
        alert: alertModal.show,
        params: paramsModal.show,
        table: tableModal.show,
        tree: treeModal.show,
        widget: widgetModal.show,
        conditionsEditor: conditionsEditorModal.show,
        smartSearchRule: smartSearchRuleModal.show,
        keyValueGraph: keyValueGraphModal.show
    };
});