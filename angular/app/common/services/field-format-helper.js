var m = angular.module('common');

m.factory('fieldFormatHelper', function($filter) {
    function processItem (text, options) {
        if (options.format == 'time') {
            if (options.timeNullIsEmpty && _(text).isNull()) {
                return '';
            }
            return $filter('localTime')(text, options.timeFormat);
        } else if (options.format == 'bytes') {
            return $filter('bytes')(text, options.bytesPrecision, options.bytesShortForm);
        }
        return text;
    }

    return function(text, options) {
        if (options.format !== 'time' && options.format !== 'bytes') {
            return text;
        }
        if (_(text).isArray()) {
            return _(text).map(function (item) {
                return processItem(item, options);
            });
        }
        return processItem(text, options);
    };
});