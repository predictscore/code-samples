var m = angular.module('common');

m.factory('dataToRowsConverter', function ($q) {
    return function (input, args) {
        input.data.rows = input.data.data;
        delete input.data.data;
        return $q.when(input);
    };
});