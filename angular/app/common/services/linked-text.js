var m = angular.module('common');

m.factory('linkedText', function(routeHelper) {
    function findEnd(text) {
        var count = 1;
        var pos = 1;
        var inQuotes = false;
        while(count > 0) {
            pos++;
            if (text[pos] === '"' && text[pos-1] !== '\\') {
                inQuotes = !inQuotes;
            }
            if (!inQuotes) {
                if(text[pos] == '}') {
                    count--;
                } else if(text[pos] == '{') {
                    count++;
                }
            }
            if(pos == text.length) {
                return -1;
            }
        }
        return pos;
    }
    var linkedText = {
        toSimpleParts: function (text) {
            if(_(text).isNull() || _(text).isUndefined()) {
                text = '';
            }
            text = '' + text;
            var parts = [];
            while (text.length) {
                var index = text.indexOf('#{');
                if (index === -1) {
                    parts.push({
                        display: 'text',
                        text: text
                    });
                    text = '';
                } else if (index > 0) {
                    parts.push({
                        display: 'text',
                        text: text.slice(0, index)
                    });
                    text = text.slice(index);
                } else {
                    var end = findEnd(text);
                    var parsingData = text.slice(1, end + 1);
                    if(parsingData.indexOf("{'") === 0) {
                        //There bug if data contains single quote
                        parsingData = parsingData.replace(/'/g, '"');
                    }
                    parts.push(JSON.parse(parsingData));
                    text = text.slice(end + 1);
                }
            }
            return parts;
        },
        toParts: function (text) {
            var parts = linkedText.toSimpleParts(text);
            return _(parts).map(function (data) {
                var part = {
                    data: data,
                    display: data.display || data.type && 'link' || 'text',
                    'class': data['class']
                };
                if(part.display === 'link') {
                    part.data.path = routeHelper.getHref(part.data.type, part.data, part.data.qs);
                }
                return part;
            });
        },
        toText: function(text) {
            var parts = this.toParts(text);
            return _(parts).reduce(function(memo, part) {
                var partText = part.data.text;
                if (!_(part.data.asText).isUndefined()) {
                    partText = part.data.asText;
                }
                if(part.display == 'inline-list') {
                    partText = _(part.data.data).map(function (el) {
                        return linkedText.toText(el);
                    }).join('\n');
                }
                partText = partText || '';
                if(partText.indexOf('#{') === 0) {
                    memo.push(linkedText.toText(partText));
                    return memo;
                }
                memo.push(partText);
                return memo;
            }, []).join(' ');
        }
    };
    return linkedText;
});