var m = angular.module('common');

m.factory('modalSingletons', function() {
    var data = {};
    return {
        open: function(name, instance) {
            data[name] = instance;
        },
        close: function(name) {
            if(data[name]) {
                delete data[name];
            }
        },
        isOpen: function(name) {
            return !!data[name];
        },
        getInstance: function(name) {
            return data[name];
        }
    };
});