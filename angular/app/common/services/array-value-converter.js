var m = angular.module('common');

m.factory('arrayValueConverter', function ($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var rows = input.data.rows || [{}];

        var result = _(rows).map(function(row) {
            return row[args.name];
        });

        deferred.resolve($.extend({}, input, {
            data: result
        }));

        return deferred.promise;
    };
});