var m = angular.module('common');

m.factory('submitForAnalysisParams', function(modals, modulesManager) {
    return {
        show: function(title, type) {

            var apps = _(modulesManager.activeSecAppsFull()).filter(function (app) {
                return app.matrix_entity;
            });
            var params = _(apps).map(function (app) {
                return {
                    id: app.name,
                    rightLabel: app.label,
                    emptyLabel: true,
                    type: 'boolean',
                    data: true,
                    paramClass: 'low-height-param'
                };
            });
            params[0].label  = 'Allow Avanan to share ' + type + ' with the following vendors';
            params[0].labelClass = 'label-with-overflow';
            params.push({
                id: 'submitReason',
                type: 'text',
                label: 'Reason for sharing',
                paramClass: 'text-param-average'
            });

            return modals.params([{
                lastPage: true,
                params: params
            }], title).then(function (data) {
                return {
                    reason_for_sharing: data.submitReason,
                    sec_apps: _(data).chain().omit('submitReason').pick(function (status, app) {
                        return status;
                    }).keys().value()
                };
            });
        }
    };
});