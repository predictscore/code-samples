var m = angular.module('common');

m.factory('beautifyKey', function () {
    return function(str) {
        return str.replace(/[\_\-]/g, ' ')// replaces hyphen and lowdash with space: 'entity_id' -> 'entity id'
            .replace(/([a-z0-9])([A-Z])/g, '$1 $2')// splits cammelCaseWords: 'isDelegatedAdmin' -> 'is Delegated Admin'
            .replace(/(^| )([a-z])/g, function (full, space, chr) { // capitalizes every first character: 'entity id' -> 'Entity Id'
                return space + chr.toUpperCase();
            });
    };
});