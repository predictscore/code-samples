var m = angular.module('common');

m.factory('findIdConverter', function($q) {
    return function(input, args) {
        args = $.extend({
            idName: 'id'
        }, args);
        
        var rows = input.data;
        if(input.data.rows) {
            rows = input.data.rows;
        }
        if(!rows) {
            rows = [];
        }
        
        var found = _(rows).find(function(row) {
            return row[args.idName] == args.id; 
        });
        var deferred = $q.defer();
        if(found) {
            deferred.resolve({
                data: found
            });
        } else {
            deferred.reject();
        }
        return deferred.promise;
    };
});