var m = angular.module('common');

m.service('tableBodyAdjuster', function() {
    return function($scope, rootSelector, outerSelectors) {
        function adjustHeight() {
            var rootElement = $(rootSelector);
            var panelHeight = rootElement.find('.panel-body').outerHeight();
            
            var outerSumm = 0;
            _(outerSelectors).each(function(selector) {
                if(_(selector).isString()) {
                    outerSumm += rootElement.find(selector).outerHeight();
                } else if(_(selector).isNumber()) {
                    outerSumm += selector;
                }
            });
            
            rootElement.find('.panel-body .data-table tbody').height(panelHeight - outerSumm);
        }
        
        $(window).on('resize', adjustHeight);
        $scope.$on('$destroy', function() {
            $(window).off('resize', adjustHeight);
        });
        
        return {
            adjust: adjustHeight
        };
    };
});