var m = angular.module('common');

m.factory('limitedArrayConverter', function () {
    return function (text, args)  {
        if(!_(text).isArray() || text.length === 0) {
            return text;
        }

        if (args.uniqueCountText) {
            var uniqueValues = {};
            _(text).each(function (el, idx) {
                if (args.uniqueTrim) {
                    el = $.trim(el);
                }
                if (!uniqueValues[el]) {
                    uniqueValues[el] = {
                        text: el,
                        count: 0,
                        first: idx
                    };
                }
                uniqueValues[el].count++;
            });
            text = _(uniqueValues).chain().map(function (el) {
                return el;
            }).sortBy('firstIdx').map(function (el) {
                return el.text + (el.count > 1 ? (' (' + el.count + ' ' + args.uniqueCountText + ')') : '');
            }).value();
        }

        var countText;
        if (args.collapseRest) {
            countText = (text.length - (args.expanded || 1)) + ' more';
        } else {
            countText = text.length + ' ' + args.countPostfix;
        }
        return '#' + JSON.stringify({
                display: 'inline-list',
                data: _(text).map(function(label) {
                    return label;
                }),
                countText: countText,
                expanded: args.expanded,
                collapseRest: args.collapseRest
            });
    };
});