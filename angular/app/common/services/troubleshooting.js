var m = angular.module('common');

m.factory('troubleshooting', function(feature, sideMenuManager) {
    var troubleshooting = {
        available: function () {
            return feature.troubleshooting;
        },
        enabled: function (newState) {
            if (!troubleshooting.available()) {
                return false;
            }
            if (_(newState).isBoolean()) {
                localStorage.setItem('troubleshootingEnabled', JSON.stringify(newState));
                return newState;
            }
            var stored = localStorage.getItem('troubleshootingEnabled');
            if (stored) {
                return JSON.parse(stored);
            }
            return false;
        },
        entityDebugEnabled: function (newState) {
            var currentUser = sideMenuManager.currentUser();
            if (!currentUser || !currentUser.email) {
                return false;
            }
            if (!/@avanan\.com$/i.test(currentUser.email)) {
                return false;
            }
            if (_(newState).isBoolean()) {
                localStorage.setItem('entityDebugEnabled', JSON.stringify(newState));
                return newState;
            }
            var stored = localStorage.getItem('entityDebugEnabled');
            if (stored) {
                return JSON.parse(stored);
            }
            return false;
        }
    };

    return troubleshooting;
});