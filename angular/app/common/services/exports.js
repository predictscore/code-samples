var m = angular.module('common');

m.factory('exports', function (columnsHelper, linkedText, base64, $q, $timeout) {

    var mimeTypes = {
        xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        csv: 'text/csv'
    };

    var csvDelimiter = ",";
    var csvNewLine = "\r\n";


    function isColumnVisible (column, tableOptions) {
        if(!_(column.export).isUndefined() && !_(column.export.hidden).isUndefined() && column.export.hidden) {
            return false;
        }
        if(!_(column.export).isUndefined() && column.export.visible) {
            return true;
        }
        return columnsHelper.isColumnVisible(column, tableOptions, true);
    }

    function fixCSVField (value) {
        var fixedValue = value;
        var addQuotes = (value.indexOf(csvDelimiter) !== -1) || (value.indexOf('\r') !== -1) || (value.indexOf('\n') !== -1);
        var replaceDoubleQuotes = (value.indexOf('"') !== -1);

        if (replaceDoubleQuotes) {
            fixedValue = fixedValue.replace(/"/g, '""');
        }
        if (addQuotes || replaceDoubleQuotes) {
            fixedValue = '"' + fixedValue + '"';
        }
        return fixedValue;
    }


    function exportObject(mime, data, filename, type) {
        this.mime = mime;
        this.data = data;
        this.filename = filename;
        this.type = type || 'text';
    }

    exportObject.prototype.getDataUrl = function (cb) {
        var self = this;
        // this method doesn't work now for xls
        $timeout(function () {
            cb('data:' + self.mime + ';base64,' + base64.encode(self.data));
        });
    };

    exportObject.prototype.save = function () {
        return saveAs(new Blob([this.type === 'binary' ? s2ab(this.data) : this.data], {type: this.mime}), this.filename);
    };


    function getColumns (data, options) {
        return _(data.columns).isUndefined() ? options.columns : data.columns;
    }




    function datenum(v, date1904) {
        if(date1904) v+=1462;
        var epoch = Date.parse(v);
        return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
    }

    function sheet_from_array_of_arrays(data, opts) {
        var ws = {};
        var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
        for(var R = 0; R != data.length; ++R) {
            for(var C = 0; C != data[R].length; ++C) {
                if(range.s.r > R) range.s.r = R;
                if(range.s.c > C) range.s.c = C;
                if(range.e.r < R) range.e.r = R;
                if(range.e.c < C) range.e.c = C;
                var cell = {v: data[R][C] };
                if(cell.v === null) continue;
                var cell_ref = XLSX.utils.encode_cell({c:C,r:R});

                if(typeof cell.v === 'number') cell.t = 'n';
                else if(typeof cell.v === 'boolean') cell.t = 'b';
                else if(cell.v instanceof Date) {
                    cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                    cell.v = datenum(cell.v);
                }
                else cell.t = 's';

                ws[cell_ref] = cell;
            }
        }
        if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
        return ws;
    }


    function Workbook() {
        if(!(this instanceof Workbook)) return new Workbook();
        this.SheetNames = [];
        this.Sheets = {};
    }

    function s2ab(s) {
        var buf = new ArrayBuffer(s.length);
        var view = new Uint8Array(buf);
        for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    }




    return {
        csv: function (title, data, tableOptions, options) {

            if (!title) {
                title = 'report';
            }
            var textData = '';

            var columns = getColumns(data, options);
            var isVisibleCache = [];

            var visibleIdx = 0;
            _(columns).each(function(column, idx) {
                if (!(isVisibleCache[idx] = isColumnVisible(column, tableOptions))) {
                    return;
                }
                textData = textData + (visibleIdx++ ? csvDelimiter : '') + fixCSVField(column.text || '');
            });
            textData = textData + csvNewLine;


            _(data.data).each(function(row, rowIdx) {
                visibleIdx = 0;
                _(row).each(function (column, idx) {
                    if(!isVisibleCache[idx]) {
                        return;
                    }
                    textData = textData + (visibleIdx++ ? csvDelimiter : '') + fixCSVField(linkedText.toText(row[idx].text) || '');
                });
                textData = textData + csvNewLine;
            });

            return new exportObject(mimeTypes.csv, textData, title + '.csv');
        },

        xlsx: function (title, data, tableOptions, options) {

            var columns = getColumns(data, options);

            if (!title) {
                title = 'report';
            }

            var table = [];
            var header = [];
            table.push(header);

            _(data.data).each(function(row) {
                table.push([]);
            });

            _(columns).each(function(column, idx) {
                if (!isColumnVisible(column, tableOptions)) {
                    return;
                }
                header.push(column.text || '');
                _(data.data).each(function(row, rowIdx) {
                    table[rowIdx + 1].push(linkedText.toText(row[idx].text) || '');
                });
            });
            var wb = new Workbook(), ws = sheet_from_array_of_arrays(table);

            wb.SheetNames.push(title);
            wb.Sheets[title] = ws;

            return new exportObject(mimeTypes.xlsx, XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'}), title + '.xlsx', 'binary');
        },

        pdf: function (title, data, tableOptions, options) {
            var table = {
                headerRows: 1,
                widths: [],
                body: []
            };

            var pdf = {
                pageOrientation: 'landscape',
                content: [{
                    text: title,
                    fontSize: 22,
                    bold: true
                }, {
                    table: table
                }]
            };
            var columns = getColumns(data, options);

            var header = [];
            table.body.push(header);
            _(data.data).each(function(row) {
                table.body.push([]);
            });

            _(columns).each(function(column, idx) {
                if (!isColumnVisible(column, tableOptions)) {
                    return;
                }
                table.widths.push('auto');
                header.push({
                    text: column.text || '',
                    bold: true,
                    fillColor: '#f0f0f0'
                });

                _(data.data).each(function(row, rowIdx) {
                    table.body[rowIdx + 1].push({
                        text: linkedText.toText(row[idx].text) || '',
                        fillColor: rowIdx % 2 ? '#f6f6f6' : '#ffffff'
                    });
                });
            });
            return pdfMake.createPdf(pdf);
        }
    };
});