var m = angular.module('common');

m.factory('defaultHttpErrorHandler', function(errorStatusHolder) {
    return function(error) {
        if(error.status == 404) {
            errorStatusHolder.errorStatus(404);
        } else {
            errorStatusHolder.errorStatus(500);
        }
    };
});