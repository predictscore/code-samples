var m = angular.module('common');

m.factory('notificationCenter', function(alertsDao, $timeout, modulesManager) {
    var lastNotificationTime = localStorage.getItem('lastNotificationTime');
    // if(!lastNotificationTime) {
        lastNotificationTime = moment().valueOf();
        localStorage.setItem('lastNotificationTime', lastNotificationTime);
    // }
    var count = 0;
    function retrieveNotification() {
        if (window.guiVersion === 2) {
            return;
        }
        alertsDao.newCount(lastNotificationTime).then(function(response) {
            count = response.data.count;
            $timeout(retrieveNotification, 1000);
        }, function() {
            $timeout(retrieveNotification, 30000);
        });
    }
    retrieveNotification();
    return {
        getNotificationsCount: function() {
            return count;
        },
        clearNotifications: function() {
            count = 0;
            lastNotificationTime = moment().valueOf();
            localStorage.setItem('lastNotificationTime', lastNotificationTime);
        }
    };
});