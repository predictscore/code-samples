var m = angular.module('common');

m.factory('singleRowConverter', function ($q, errorStatusHolder) {
    return function (input, args) {
        var deferred = $q.defer();

        if(!input.data.rows.length) {
            if (args.allowEmpty) {
                deferred.resolve($.extend({}, input, {
                    data: {}
                }));
            } else {
                errorStatusHolder.errorStatus(404);
                deferred.reject({
                    status: 404
                });
            }
        } else {
            var result = _(input.data.rows).first();
    
            deferred.resolve($.extend({}, input, {
                data: result
            }));
        }

        return deferred.promise;
    };
});