var m = angular.module('common');

m.factory('filterConverter', function ($q) {
    return function (input, args) {
        var deferred = $q.defer();

        input.data.rows = _(input.data.rows).filter(args.filter);

        deferred.resolve($.extend({}, input));

        return deferred.promise;
    };
});