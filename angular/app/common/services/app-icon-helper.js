var m = angular.module('common');

m.factory('appIconHelper', function($injector) {
    var path;

    function innerIcon(img, isBeta, isExpired, tooltip) {
        if (!path) {
            path = $injector.get('path');
        }
        var iconImg = '#' + JSON.stringify({
                display: 'img',
                path: path('security-stack').img('solutions/' + img),
                tooltip: tooltip
            });
        if (!isBeta && !isExpired) {
            return iconImg;
        }
        if (isExpired) {
            return '#' + JSON.stringify({
                    display: 'linked-text',
                    'class': 'app-img-with-label expired-app-img',
                    text: iconImg
                });
        }

        return '#' + JSON.stringify({
                display: 'linked-text',
                'class': 'app-img-with-label beta-app-img',
                text: iconImg
            });
    }

    var appIconHelper = {
        appIconWithLabels: function (img, isBeta, isExpired, tooltip, addContainer) {
            var icon = innerIcon(img, isBeta, isExpired, tooltip);
            if (!addContainer) {
                return icon;
            } else {
                return '#' + JSON.stringify({
                        display: 'linked-text',
                        'class': 'file-security-app-icon file-security-app-icon-container',
                        text: icon
                    });
            }
        },
        appIconByApp: function (app, addContainer) {
            if (!app.stack_icon) {
                return;
            }
            return appIconHelper.appIconWithLabels(app.stack_icon, app.is_beta, app.isExpired, app.label, addContainer);
        }
    };
    return appIconHelper;
});