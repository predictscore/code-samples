var m = angular.module('common');

m.factory('defaultTableConverters', function(path, modulesManager, $filter, appScanStatusHelper, fieldFormatHelper, appIconHelper) {
    function makeLinkParams (columnsMap, linkArgs) {
        if (!linkArgs) {
            return null;
        }
        var linkParams = {
            display: 'link',
            type: linkArgs.type,
            qs: {}
        };
        var nullFound = false;
        _(linkArgs.params).each(function (param, key) {
            var value = param;
            if (param in columnsMap) {
                value = columnsMap[param];
            }
            linkParams[key] = value;
            if (_(value).isNull()) {
                nullFound = true;
            }
        });
        _(linkArgs.qs).each(function (param, key) {
            var value = param;
            if (param in columnsMap) {
                value = columnsMap[param];
            }
            linkParams.qs[key] = value;
            if (_(value).isNull()) {
                nullFound = true;
            }
        });
        if (nullFound) {
            return null;
        }
        return linkParams;
    }

    function processParams(target, src, columnsMap) {
        var nullFound = false;
        _(src).each(function (param, key) {
            var value = null;
            if (param.column) {
                if (!(param.column in columnsMap)) {
                    return;
                }
                value = columnsMap[param.column];
            } else {
                value = param.value;
            }
            if (param.convert === 'sqlString') {
                if (value && value.replace) {
                    value = value.replace(/\'/g,'\'\'');
                }
            }
            target[key] = value;
            if (_(value).isNull()) {
                nullFound = true;
            }
        });
        return nullFound;
    }

    function makeLinkParams2 (columnsMap, linkArgs) {
        if (!linkArgs) {
            return null;
        }
        var linkParams = {
            display: 'link',
            type: linkArgs.type,
            qs: {}
        };
        var nullFound = processParams(linkParams, linkArgs.params, columnsMap);
        nullFound = nullFound || processParams(linkParams.qs, linkArgs.qs, columnsMap);
        if (nullFound) {
            return null;
        }
        return linkParams;
    }

    return function(globalArgs) {
        var converters = {
            imageConverter: function (text) {
                return '#' + JSON.stringify({
                        display: 'img',
                        path: text,
                        'class': 'row-icon'
                    });
            },
            policyImageConverter: function(text) {
                return '#' + JSON.stringify({
                    display: 'text',
                    'class': 'fa fa-shield policy-icon'
                });
            },
            userImageConverter: function (text, columnId, columnsMap, args) {
                if(args.exist && _(columnsMap[args.exist]).isNull()) {
                    return '';
                }
                var defaultPath = path('profiles').img('user.png');
                if(args.type) {
                    defaultPath = path('profiles').img(columnsMap[args.type] + '.png');
                }
                if(columnsMap[args.type] == 'share' || columnsMap[args.type] == 'boxshare') {
                    text = path('profiles').img(args.saas + '/permissions/' + text + '.svg');
                }
                return '#' + JSON.stringify({
                        display: 'img',
                        'class': 'row-icon',
                        path: text,
                        defaultPath: defaultPath
                    });
            },
            groupImageConverter: function(text) {
                return '#' + JSON.stringify({
                        display: 'img',
                        'class': 'row-icon',
                        path: path('profiles').img('group.png')
                    });
            },
            applicationImageConverter: function (text) {
                return '#' + JSON.stringify({
                        display: 'img',
                        path: text,
                        'class': 'row-icon',
                        defaultPath: path('profiles').img('application.png')
                    });
            },
            userConverter: function (text, columnId, columnsMap, args) {
                var userId = columnsMap[args.id];
                if (_(userId).isString() && userId.length || _(userId).isNumber() && userId > 0) {
                    return '#' + JSON.stringify({
                            display: 'link',
                            type: args.linkType || 'user',
                            id: userId,
                            text: text || 'Anonymous'
                        });
                }
                return 'Anonymous';
            },
            mimeIconConverter: function (text, columnId, columnsMap, args) {
                if(args.exist && _(columnsMap[args.exist]).isNull()) {
                    return '';
                }
                var icon = '#' + JSON.stringify({
                        display: 'img',
                        path: globalArgs.mimeIcons(text || args.defaultMimeType),
                        'class': 'row-icon',
                        tooltip: args.tooltip || columnsMap[args.niceMimeColumn]
                    });
                if (!args.typeColumn) {
                    return icon;
                }
                return '#' + JSON.stringify({
                        display: 'link',
                        type: columnsMap[args.typeColumn],
                        id: columnsMap[args.idColumn],
                        linkedText: icon
                    });
            },
            remoteActivityExplanationConverter: function (text, columnId, columnsMap, args) {
                var firstEventName = columnsMap[args.event1];
                if (_(firstEventName).isString() && firstEventName.length) {
                    var explanation ='User ' +
                            columnsMap[args.event1] +
                                ' from ' +
                            columnsMap[args.location1] +
                                ' at ' +
                            columnsMap[args.time1] +
                                ' and ' +
                            columnsMap[args.event2] +
                                ' at ' +
                            columnsMap[args.location2] +
                                ', at ' +
                            columnsMap[args.time2] +
                                ', located ' +
                            columnsMap[args.distance] + ' KM apart';
                    return '#' + JSON.stringify({
                        display: 'text',
                        text: explanation
                    });
                }
                return 'Anonymous';
            },
            mimeTypeConverter: function(text, columnId, columnsMap) {
                return text || columnsMap.mimeType;
            },
            fileOrFolderTitleConverter: function(text, columnId, columnMap, args) {
                if(columnMap[args.typeColumn] == args.fileType) {
                    if (!columnMap[args.fileIdColumn]) {
                        return text;
                    }
                    return '#' + JSON.stringify({
                            display: 'link',
                            text: columnMap[args.fileTitleColumn],
                            type: 'file',
                            id: columnMap[args.fileIdColumn]
                        });
                } else if(columnMap[args.typeColumn] == args.folderType){
                    if (!columnMap[args.folderIdColumn]) {
                        return text;
                    }
                    return '#' + JSON.stringify({
                            display: 'link',
                            text: columnMap[args.folderTitleColumn],
                            type: 'folder',
                            id: columnMap[args.folderIdColumn]
                        });
                }
                return '';
            },
            saasIcon: function (text) {
                if(_(text).isNull()) {
                    return '';
                }
                return '#' + JSON.stringify({
                        display: 'img',
                        'class': 'entity-type-image',
                        path: modulesManager.cloudApp(text).img
                    });
            },
            listConverter: function(text, columnId, columnsMap, args, allConverters) {
                if(!_(text).isArray() || text.length === 0) {
                    return '';
                }
                if (args.uniqueCountText) {
                    var uniqueValues = {};
                    _(text).each(function (el, idx) {
                        if (args.uniqueTrim) {
                            el = $.trim(el);
                        }
                        if (!uniqueValues[el]) {
                            uniqueValues[el] = {
                                text: el,
                                count: 0,
                                first: idx
                            };
                        }
                        uniqueValues[el].count++;
                    });
                    text = _(uniqueValues).chain().map(function (el) {
                        return el;
                    }).sortBy('first').map(function (el) {
                        return el.text + (el.count > 1 ? (' (' + el.count + ' ' + args.uniqueCountText + ')') : '');
                    }).value();
                }
                var countText;
                if (args.collapseRest) {
                    countText = (text.length - (args.expanded || 1)) + ' more';
                } else {
                    countText = text.length + ' ' + args.countPostfix;
                }
                return '#' + JSON.stringify({
                        display: 'inline-list',
                        'class': args.class || 'medium-width',
                        data: _(text).map(function(label, idx) {
                            var result = '';
                            if (args.itemConverter) {
                                var itemConverter = args.itemConverter;
                                if(_(itemConverter).isString()) {
                                    itemConverter = allConverters[itemConverter];
                                }
                                if (!itemConverter) {
                                    console.log('converter ' + args.itemConverter + ' not defined');
                                } else {
                                    var itemColumnsMap;
                                    if (args.itemConverterScope === 'element') {
                                        itemColumnsMap = label;
                                    } else {
                                        itemColumnsMap = columnsMap;
                                    }
                                    var itemText = (args.itemConverterTextColumn ? itemColumnsMap[args.itemConverterTextColumn] : label);
                                    label = itemConverter(itemText, columnId, itemColumnsMap, args.itemConverterArgs);
                                }
                            }
                            if (args.objectField && _(label).isObject()) {
                                label = label[args.objectField];
                            }
                            label = fieldFormatHelper(label, args);
                            if(_(args.ids).isUndefined()) {
                                return label;
                            }
                            if(args.isExternal && columnsMap[args.isExternal][idx]) {
                                result += '#' + JSON.stringify({
                                    display: 'img',
                                    path: path('common').img('external-user.png'),
                                    tooltip: 'External user',
                                    'class': 'icon-16px display-inline-block'
                                });
                            }
                            if(_(columnsMap[args.ids][idx]).isNull()) {
                                return label;
                            }
                            return result + '#' + JSON.stringify({
                                    display: 'link',
                                    type: args.type,
                                    id: columnsMap[args.ids][idx],
                                    text: label
                                });
                        }),
                        countText: countText,
                        expanded: args.expanded,
                        collapseRest: args.collapseRest,
                        elementAsTooltip: args.elementAsTooltip,
                        moreInPopup: args.moreInPopup
                    });
            },
            multiListConverter: function(text, columnId, columnsMap, args) {
                var result = '';
                if (!columnsMap.secondRowData) {
                    result += '#' + JSON.stringify({
                            display: 'linked-text',
                            text: _(text).map(function (element) {
                                return converters.listConverter(element, columnId, columnsMap, args);
                            }).join(''),
                            'class': 'multi-list-container'
                        });
                }
                if (columnsMap.reportLinks) {
                    result += columnsMap.reportLinks;
                }
                if (columnsMap.downloadLinks) {
                    result += '#' + JSON.stringify({
                            display: 'linked-text',
                            text: columnsMap.downloadLinks,
                            'class': 'all-scan-download-links'
                        });

                }
                if (columnsMap.exceptionLinks) {
                    result += '#' + JSON.stringify({
                            display: 'linked-text',
                            text: columnsMap.exceptionLinks,
                            'class': 'all-scan-additional-links'
                        });
                }
                return result;
            },
            userWithEmailOrGroup: function(text, columnId, columnsMap, args) {
                if(args.entityTypeColumn && columnsMap[args.entityTypeColumn] == 'group') {
                    return '#' + JSON.stringify({
                        display: 'link',
                        'class': 'group-name display-block',
                        type: 'group',
                        id: columnsMap[args.userIdColumn],
                        text: text || 'Anonymous group'
                    });
                }  
                return converters.userWithEmail(text, columnId, columnsMap, args);
            },
            userWithEmail: function(text, columnId, columnsMap, args) {
                var result = '';
                if (columnsMap.type === 'boxshare') {
                    if (text) {
                        if (columnsMap[args.folderIdColumn]) {
                            result += '#' + JSON.stringify({
                                    display: 'link',
                                    'class': 'user-name display-block',
                                    type: 'folder',
                                    id: columnsMap[args.folderIdColumn],
                                    text: text,
                                    tooltip: 'Shared link on parent folder<br />/' + _(columnsMap[args.pathNamesColumn]).map(function (name, idx) {
                                        if (columnsMap[args.pathIdsColumn][idx+1] === columnsMap[args.folderIdColumn]) {
                                            return '<strong>' + name + '</strong>';
                                        } else {
                                            return name;
                                        }
                                    }).join('/')
                                });
                        } else {
                            result += '#' + JSON.stringify({
                                    display: 'text',
                                    'class': 'user-name display-block',
                                    text: text
                                });

                        }
                    }
                    if (columnsMap[args.shareInfoColumn]) {
                        result += '#' + JSON.stringify({
                                display: 'text',
                                'class': 'user-email',
                                text: columnsMap[args.shareInfoColumn]
                            });
                    }
                    return result;
                }
                if(columnsMap.type === 'share') {
                    result = '#' + JSON.stringify({
                            display: 'text',
                            text: text,
                            'class': 'display-block'
                        });
                    if (args.entityLinkColumn) {
                        result += '#' + JSON.stringify({
                                display: 'external-link',
                                href: globalArgs.entity.data[args.entityLinkColumn],
                                text: 'Change'
                            });
                    }
                    return result;
                }
                if(columnsMap.type === 'team') {
                    return '#' + JSON.stringify({
                            display: 'text',
                            text: text,
                            'class': 'display-block'
                        });
                }
                if(!_(args.isExternalColumn).isUndefined() && columnsMap[args.isExternalColumn]) {
                    result += '#' + JSON.stringify({
                        display: 'img',
                        path: path('common').img('external-user.png'),
                        tooltip: 'External user',
                        'class': 'icon-16px display-inline-block'
                    });
                }
                if(!_(args.suspendedColumn).isUndefined() && columnsMap[args.suspendedColumn]) {
                    result += '#' + JSON.stringify({
                            display: 'text',
                            text: 'suspended',
                            'class': 'user-suspended'
                        });
                }
                if(columnsMap[args.userIdColumn]) {
                    if(args.noLinkAnonymous && !text) {
                        result += '#' + JSON.stringify({
                            display: 'text',
                            'class': 'user-name display-block',
                            text: 'Anonymous'
                        });
                    } else {
                        result += '#' + JSON.stringify({
                            display: 'link',
                            'class': 'user-name display-block',
                            type: args.userLinkType || columnsMap.type || 'user',
                            entityType: args.userLinkEntityType,
                            id: columnsMap[args.userIdColumn],
                            text: text || 'Anonymous'
                        });
                    }
                    result += '#' + JSON.stringify({
                        display: 'text',
                        'class': 'user-email',
                        text: columnsMap[args.emailColumn]
                    });
                    if (args.groupNamesColumn && args.groupIdsColumn && columnsMap[args.groupNamesColumn] && columnsMap[args.groupIdsColumn] && columnsMap[args.groupNamesColumn].length && columnsMap[args.groupNamesColumn].length === columnsMap[args.groupIdsColumn].length) {
                        result += '#' + JSON.stringify({
                                display: 'inline-list',
                                'class': 'medium-width',
                                data: _(columnsMap[args.groupNamesColumn]).map(function(label, idx) {
                                    var result = '';
                                    return result + '#' + JSON.stringify({
                                            display: 'link',
                                            type: 'group',
                                            id: columnsMap[args.groupIdsColumn][idx],
                                            text: label
                                        });
                                }),
                                countText: columnsMap[args.groupNamesColumn].length + ' groups'
                            });
                    }
                } else {
                    result += '#' + JSON.stringify({
                            display: 'text',
                            'class': 'user-name display-block',
                            text: text
                        }) + '#' + JSON.stringify({
                            display: 'text',
                            'class': 'user-email',
                            text: columnsMap[args.emailColumn]
                        });
                }
                return result;
            },
            composition: function(text, columnId, columnsMap, args) {
                return {
                    text: _(args).chain().map(function (value, key) {
                        return [key, columnsMap[value]];
                    }).object().value()
                };
            },
            fileParentsListConverter: function(text, columnId, columnsMap, args) {
                if(text.length === 0) {
                    return '/';
                }
                if (!args.clickIcon) {
                    return '#' + JSON.stringify({
                            display: 'inline-list',
                            'class': 'medium-width',
                            data: _(text).map(function (label, idx) {
                                return '#' + JSON.stringify({
                                        display: 'link',
                                        type: 'folder',
                                        id: columnsMap.parent_ids[idx],
                                        text: label
                                    });
                            }),
                            countText: text.length + ' parents'
                        });
                }
                return '#' + JSON.stringify({
                        display: 'inline-list',
                        'class': 'medium-width',
                        data: _(text).map(function (label, idx) {
                            var inner = '#' + JSON.stringify({
                                    display: 'link',
                                    type: 'folder',
                                    id: columnsMap.parent_ids[idx],
                                    text: label
                                })   + '#' + JSON.stringify({
                                    display: 'text',
                                    'class': args.clickIcon.iconClass,
                                    clickEvent: {
                                        name: args.clickIcon.eventName,
                                        attributes: {
                                            "entityId": columnsMap.parent_ids[idx],
                                            "entityType": 'folder',
                                            "mimeType": 'folder',
                                            title: label
                                        }
                                    },
                                    tooltip: args.clickIcon.tooltip
                                });


                            return '#' + JSON.stringify({
                                    display: 'linked-text',
                                    'class':  'click-icon-container',
                                    text: inner
                                });
                        }),
                        countText: text.length + ' parents'
                    });
            },
            rowPendingLink: function (text, columnId, columnsMap, args) {
                if (!columnsMap[args.typeColumnName]) {
                    return text || '';
                }
                return '#' + JSON.stringify({
                        display: 'link',
                        type: columnsMap[args.typeColumnName],
                        id: columnsMap[args.idColumnName],
                        text: text
                    });
            },
            actionsIconsConverter: function(text, columnId, columnsMap, args) {
                text = text || [];
                return _(text).map(function(action) {
                    var status = action.error_code === 0 ? 'Success' : 'Failure';
                    if(action.status == 'pending') {
                        status = 'Pending';
                    }
                    var actionObj = _(globalArgs.actions[args.entity_type]).find(function(a) {
                            return a.id == action.name;
                        }) || {};
                    var actionLabel = actionObj.label || action.name;

                    var result = '#' + JSON.stringify({
                            display: 'img',
                            path: path('policy').ctrl('policy').img(action.name + '.png'),
                            'class': 'action-img ' + action.status,
                            tooltip: {
                                content: '<div>Action: ' + actionLabel + '</div>' +
                                '<div>Time: ' + $filter('localTime')(action.time_start) + '</div>' +
                                '<div>Status: ' + status + '</div>',
                                position: {
                                    my: 'bottom right',
                                    at: 'top center'
                                }
                            }
                        });
                    if(action.status == 'done') {
                        var image = action.error_code === 0 ? 'success' : 'failure';
                        result += '#' + JSON.stringify({
                            display: 'img',
                            path: path('policy').ctrl('policy').img(image + '.png'),
                            'class': 'action-status'
                        });
                    }
                    result = '#' + JSON.stringify({
                        display: 'linked-text',
                        text: result,
                        'class': 'action-img-with-status'
                    });
                    return result;
                }).join('');
            },
            arrayLengthConverter: function(text) {
                return text.length;
            },
            booleanCheckConverter: function(text, columnId, columnsMap, args) {
                var checkClass = "green-check";
                if (args && !_(args.checkClass).isUndefined()) {
                    checkClass = args.checkClass;
                }
                if(text) {
                    return '#' + JSON.stringify({
                            display: 'html',
                            text: '<div class="fa fa-check ' + checkClass + '"></div>'
                        });
                }
                return '';
            },
            revokePermissionConverter: function(text, columnId, columnsMap, args) {
                if (!columnsMap.type || !args[columnsMap.type]) {
                    return;
                }
                var ignoreRoles = args[columnsMap.type].ignoreRoles || ['owner', 'creator'];

                if (_(columnsMap.role).isString() && _(ignoreRoles).contains(columnsMap.role)) {
                    return;
                }
                if (_(columnsMap.role).isArray() && _(ignoreRoles).intersection(columnsMap.role).length) {
                    return;
                }

                var attributes;
                if (_(args[columnsMap.type].attributes).isString()) {
                    attributes = columnsMap[args[columnsMap.type].attributes];
                } else {
                    attributes = _(args[columnsMap.type].attributes).chain().map(function (attribute, name) {
                        return [name, columnsMap[attribute]];
                    }).object().value();
                }
                return '#' + JSON.stringify({
                        display: 'text',
                        'class': 'fa fa-times blue-times cursor-pointer',
                        clickEvent: {
                            name: 'revoke-user',
                            attributes: attributes,
                            eventName: args[columnsMap.type].eventName
                        },
                        tooltip: 'Revoke access'
                    });
            },
            clickEventConverter: function(text, columnId, columnsMap, args) {
                var attributes = _(args.attributes).chain().map(function (attribute, name) {
                    return [name, columnsMap[attribute]];
                }).object().value();
                return '#' + JSON.stringify({
                        display: 'text',
                        text: text,
                        'class': args.class,
                        clickEvent: {
                            name: args.eventName,
                            attributes: attributes
                        },
                        tooltip: args.tooltip
                    });
            },
            ipWithGeo: function(text, columnId, columnsMap, args) {
                return '#' + JSON.stringify({
                        display: 'link',
                        'class': 'ip-address display-block',
                        type: 'ip',
                        id: text,
                        text: text
                    }) + '#' + JSON.stringify({
                        display: 'text',
                        'class': 'ip-country',
                        text: _([columnsMap[args.cityColumn], columnsMap[args.countryColumn]]).compact().join(', ') || ' '
                    });
            },
            applicationNameConverter: function (text, columnId, columnsMap) {
                if (columnsMap.type == 'token') {
                    return text;
                }
                return '#' + JSON.stringify({
                        display: 'link',
                        type: 'application',
                        id: columnsMap.entity_id,
                        text: text
                    });
            },
            suspendedMarkConverter: function(text) {
                if(text) {
                    return '#' + JSON.stringify({
                            display: 'html',
                            text: '<div class="fa fa-pause"></div>'
                        });
                }
                return '';
            },
            applicationDriveAccessConverter: function(text) {
                if(text) {
                    return '#' + JSON.stringify({
                            display: 'text',
                            tooltip: 'Application has drive access',
                            class: 'fa fa-exclamation-triangle'
                        });
                }
                return '';
            },
            appIconConverter: function(text, columnId, columnsMap, args) {
                return appIconHelper.appIconWithLabels(text, args.isBetaColumn && columnsMap[args.isBetaColumn],
                    args.isExpiredColumn && columnsMap[args.isExpiredColumn]);
            },
            appStatusIcon: function(text, columnId, columnsMap) {
                return '#' + JSON.stringify({
                        display: 'text',
                        'class': appScanStatusHelper.statusToClass(text),
                        clickEvent: {
                            name: 'open-app-status',
                            appName: columnsMap.name,
                            moduleName: columnsMap.moduleName,
                            appLabel: columnsMap.label,
                            scanStatus: text,
                            isHistory: columnsMap.isHistory,
                            time: columnsMap.updateTime,
                            scanEntity: columnsMap.scanEntity,
                            entityId: columnsMap.entityId,
                            entityType: columnsMap.entityType
                        },
                        tooltip: columnsMap.statusTip
                    });
            },
            googleShareTypeConverter: function(text) {
                var result = '';
                if(_(text).isString()) {
                    text = [text];
                }
                if(text[0] != 'other') {
                    result += '#' + JSON.stringify({
                        display: 'text',
                        text: {
                            'anyone_with_no_link': 'Public share',
                            'anyone_with_link': 'Anyone with link',
                            'shared_external': 'Shared external'
                        }[text[0]],
                        'class': 'display-block'
                    });
                }
                if(text.length >= 2 && text[1] == '3rd_party_apps_access') {
                    result += '#' + JSON.stringify({
                        display: 'text',
                        text: '3rd party apps access',
                        'class': 'display-block'
                    });
                }
                return result;
            },
            boxShareTypeConverter: function(text) {
                var result = '';
                if(text != 'other') {
                    result += '#' + JSON.stringify({
                        display: 'text',
                        text: {
                            'anyone': 'Public share',
                            'shared_external': 'Shared external'
                        }[text],
                        'class': 'display-block'
                    });
                }
                return result;
            },
            boxPathConverter: function(text, columnId, columnsMap, args) {
                function folderElement(id, title, elementClass, style, showExclude) {
                    if (!showExclude) {
                        return '#' + JSON.stringify({
                                display: 'link',
                                'class': elementClass,
                                style: style,
                                type: 'folder',
                                id: id,
                                text: '/' + title
                            });
                    }
                    var inner = '#' + JSON.stringify({
                            display: 'link',
                            type: 'folder',
                            id: id,
                            text: '/' + title
                        })  + '#' + JSON.stringify({
                            display: 'text',
                            'class': 'fa fa-times cursor-pointer blue-times',
                            clickEvent: {
                                name: 'add-to-exclude',
                                attributes: {
                                    "entityId": id,
                                    "entityType": 'folder',
                                    "mimeType": 'folder',
                                    title: title
                                }
                            },
                            tooltip: 'Exclude this folder with it\'s children'
                        });
                    return '#' + JSON.stringify({
                            display: 'linked-text',
                            'class': $.extend(elementClass, {'click-icon-container': true}),
                            style: style,
                            text: inner
                        });

                }

                if(text.length === 0) {
                    return '#' + JSON.stringify({
                            display: 'text',
                            'class': 'expanded-path-item',
                            text: '/'
                        });
                }
                if (text.length < 3) {
                    return _(text).map(function(label,idx) {
                        return folderElement(columnsMap[args.idsColumn][idx], label, {'expanded-path-item': true}, undefined, args.showExclude);
                    }).join('');
                }
                return '#' + JSON.stringify({
                        display: 'inline-list',
                        'class': 'medium-width',
                        data: _(text).map(function(label, idx) {
                            return folderElement(columnsMap[args.idsColumn][idx], label, {}, {'padding-left': ((args.showExclude ? 10 : 0) + idx * 10) + 'px'}, args.showExclude);
                        }),
                        countText: '#' + JSON.stringify({
                            display: 'text',
                            'class': 'expanded-path-item',
                            text: '/' + text[0]
                        }) + '#' + JSON.stringify({
                            display: 'text',
                            'class': 'expanded-path-item',
                            text: '/... /' + text[text.length - 1]
                        })
                    });
            },
            externalServiceIcon: function(text) {
                var defaultPath = path('common').img('external-services/empty-icon.gif');
                var iconPath = path('common').img('external-services/' + (text || '').toLowerCase());
                if(!text) {
                    iconPath = defaultPath;
                }
                return '#' + JSON.stringify({
                    display: 'img',
                    path: iconPath,
                    defaultPath: defaultPath
                });
            },
            externalLink: function(text, columnId, columnsMap, args) {
                return '#' + JSON.stringify({
                    display: 'external-link',
                    text: text,
                    href: columnsMap[args.href]
                });
            },
            inArrayCheckConverter: function(text, columnId, columnsMap, args) {
                if (!args) {
                    return '';
                }
                var state = !!args.default;
                if (args.column && _(columnsMap[args.column]).isArray() && args.value) {
                    state = _(columnsMap[args.column]).contains(args.value);
                }
                if (!state) {
                    return '';
                }
                var checkClass = args.checkClass || '';
                if(state) {
                    return '#' + JSON.stringify({
                            display: 'html',
                            text: '<div class="fa fa-check ' + checkClass + '"></div>'
                        });
                }
            },
            removeLineConverter: function(text, columnId, columnsMap, args) {
                if (args.noRemove) {
                    if (_(args.noRemove).find(function (value, name) {
                            return columnsMap[name] === value;
                        })) {
                        return '';
                    }
                }
                return '#' + JSON.stringify({
                    display: 'text',
                    'class': 'fa fa-times red-text cursor-pointer',
                    clickEvent: {
                        name: args.eventName,
                        attributes: {
                            row: columnsMap
                        }
                    },
                    tooltip: args.tooltip
                });
            },
            editableConverter: function(text, columnId, columnsMap, args) {
                var attributes = _(args.attributes).chain().map(function (attribute, name) {
                    return [name, columnsMap[attribute]];
                }).object().value();
                var result = {
                    display: 'editable',
                    text: text,
                    editType: args.editType || 'text',
                    'class': args.class,
                    changedEvent: {
                        name: args.eventName,
                        attributes: attributes
                    },
                    tooltip: args.tooltip,
                    emptyTooltip: args.emptyTooltip,
                    emptyClass: args.emptyClass,
                    emptyText: args.emptyText,
                    variants: args.variants
                };
                return '#' + JSON.stringify(result);
            },
            JSONConverter: function(text) {
                return JSON.stringify(text);
            },
            formattedJSONConverter: function(text) {
                return '#' + JSON.stringify({
                        display: 'text',
                        'class': 'white-space-pre',
                        text: $filter('json')(text, 4)
                    });
            },
            eventType: function(text) {
                return text;
            },
            emailReadStatusConverter: function(text, columnId, columnsMap, args) {
                if (text) {
                    return '#' + JSON.stringify({
                            display: 'img',
                            path: path('common').img('envelope-open-red.svg'),
                            class: 'email-read-status-icon',
                            "tooltip": "Email is read"
                        });
                }
                return '';
            },
            emailFolderNameConverter: function(text, columnId, columnsMap, args) {
                if (_(text).isArray()) {
                    return converters.listConverter(_(text).chain().map(function (label) {
                        if (label.toLowerCase() === 'inbox') {
                            return '#' + JSON.stringify({
                                    display: 'text',
                                    text: label,
                                    class: 'red-text'
                                });
                        } else {
                            return label;
                        }
                    }).sortBy(function (label) {
                        if (label.toLowerCase() === 'inbox') {
                            return 1;
                        }
                        return 2;
                    }).value(), columnId, columnsMap, $.extend({
                        collapseRest: true,
                        expanded: 4
                    }, args));
                }
                if (text && text.toLowerCase && text.toLowerCase() === 'inbox') {
                    return '#' + JSON.stringify({
                            display: 'text',
                            text: text,
                            class: 'red-text'
                        });
                }
                return text;
            },
            multiConverter: function(text, columnId, columnsMap, args, allConverters) {
                if (!args.converters) {
                    return text;
                }
                return _(args.converters).reduce(function (text, converterInfo) {
                    var converter = allConverters[converterInfo.converter] || converterInfo.converter;
                    if(_(converter).isString()) {
                        console.log('converter ' + converter + ' not defined');
                        return text;
                    }
                    return converter(text, columnId, columnsMap, converterInfo.converterArgs || {}, allConverters);
                }, text);
            },
            concatenation: function(text, columnId, columnsMap, args) {
                return {
                    text: _(args.columns).map(function (colId) {
                        if (!_(args.def).isUndefined() && (_(columnsMap[colId]).isNull() || _(columnsMap[colId]).isUndefined())) {
                            return args.def;
                        }
                        return columnsMap[colId];
                    }).join(args.concatWith)
                };
            },
            timeAgo: function(text, columnId, columnsMap, args) {
                if (!text) {
                    return ' ';
                }
                var time = moment.utc(text).local();
                var resText = time.fromNow();
                if (!args.href && !args.timeAsTooltip) {
                    return resText;
                }
                var result = {
                    text: resText,
                    display: 'text'
                };
                if (args.timeAsTooltip) {
                    result.tooltip = time.format('YYYY-MM-DD HH:mm:ss');
                }
                if (args.href) {
                    var linkParams = makeLinkParams(columnsMap, args.href);
                    if (linkParams) {
                        $.extend(result, linkParams);
                    }
                }
                return '#' + JSON.stringify(result);
            },
            "addClientMarkConverter": function (text, columnId, columnsMap) {
                if (columnsMap.user_type === 'client') {
                    return text + '#' + JSON.stringify({
                            display: 'linked-text',
                            text: '#' + JSON.stringify({
                                display: 'text',
                                text: 'client',
                                class: 'user-is-client-mark'
                            }),
                            class: 'display-block'
                        });
                } else {
                    return text;
                }
            },
            "linkConverter": function (text, columnId, columnsMap, args) {
                if (!args || ! text) {
                    return text;
                }
                var linkParams = makeLinkParams(columnsMap, args);
                if (!linkParams) {
                    return text;
                }
                linkParams.text = text;
                return '#' + JSON.stringify(linkParams);
            },
            "linkConverter2": function (text, columnId, columnsMap, args) {
                if (!args || ! text) {
                    return text;
                }
                var linkParams = makeLinkParams2(columnsMap, args);
                if (!linkParams) {
                    return text;
                }
                linkParams.text = text;
                return '#' + JSON.stringify(linkParams);
            },
            "arrayWithMoreConverter": function (text, columnId, columnsMap, args, allConverters) {
                if (!_(text).isArray()) {
                    text = [text];
                }
                var elements = _(text).map(function (item) {
                    if (args.itemConverter) {
                        var itemConverter = args.itemConverter;
                        if(_(itemConverter).isString()) {
                            itemConverter = allConverters[itemConverter];
                        }
                        if (!itemConverter) {
                            console.log('converter ' + args.itemConverter + ' not defined');
                        } else {
                            var itemColumnsMap;
                            if (args.itemConverterScope === 'element') {
                                itemColumnsMap = item;
                            } else {
                                itemColumnsMap = columnsMap;
                            }
                            var itemText = (args.itemConverterTextColumn ? itemColumnsMap[args.itemConverterTextColumn] : item);
                            item = itemConverter(itemText, columnId, itemColumnsMap, args.itemConverterArgs);
                        }
                    }
                    return '#' + JSON.stringify({
                            display: 'linked-text',
                            'class': 'display-block',
                            text: item
                        });

                });
                if (args.totalColumn && columnsMap[args.totalColumn] > elements.length) {
                    var moreText = (columnsMap[args.totalColumn] - elements.length) + ' more';
                    if (args.allHref) {
                        var linkParams = makeLinkParams(columnsMap, args.allHref);
                        if (linkParams) {
                            linkParams.text = 'View all';
                            moreText += '. #' + JSON.stringify(linkParams);
                        }
                    }
                    elements.push('#' + JSON.stringify({
                            display: 'linked-text',
                            'class': 'display-block',
                            text: moreText
                        }));
                }
                return elements.join('');
            },
            aspectConverter: function(text, columnId, columnsMap, args) {
                var oldValue = parseInt(columnsMap[args.oldValue]);
                var newValue = parseInt(columnsMap[args.newValue]);
                var result;
                if(oldValue === 0 && newValue === 0) {
                    result = 0;
                } else if(oldValue === 0) {
                    result = 100;
                } else {
                    result = newValue / (oldValue / 100) - 100;
                }
                result = Math.round(result * 100) / 100;
                var positive = result >= 0;
                return '#' + JSON.stringify({
                    display: 'text',
                    text: (positive ? '+' : '') + result + '%',
                    'class': {
                        'aspect': true,
                        'positive': positive,
                        'negative': !positive
                    }
                });
            },
            timeWithAgo: function (text) {
                if (!text) {
                    return ' ';
                }
                var time = moment.utc(text).local();
                return time.format('YYYY-MM-DD HH:mm:ss') + ' (' + time.fromNow() + ')';
            },
            utcDateOnly: function (text, columnId, columnsMap, args) {
                if (!text) {
                    return ' ';
                }
                var time = moment.utc(text);
                return time.format('YYYY-MM-DD') + ((args && args.showFromNow) ? (' (' + time.fromNow() + ')') : '');
            }
        };
        return converters;
    };
});
