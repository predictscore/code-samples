var m = angular.module('common');

m.factory('materializedViewsReindexHelper', function($timeout, $q, materializedViewsDao, modals) {
    return function($scope, viewName) {
        var destroyed = false;
        var statusRefreshDelayActive = 5000;
        var statusRefreshDelayIdle = 30000;
        var lastRefreshEnded;

        var isActive = false;
        var statusText, statusTextShort = 'Re-index data';
        var status;

        var statusRefreshTimer = null;

        function getStatus () {
            if (statusRefreshTimer) {
                $timeout.cancel(statusRefreshTimer);
                statusRefreshTimer = null;
            }
            return materializedViewsDao.getStatus(viewName).then(function (response) {
                status = response.data;
                api.lastRefreshed = status.view.lastEnded;
                if (lastRefreshEnded && status.lastEnded === lastRefreshEnded) {
                    isActive = true;
                    statusTextShort = statusText = 'Re-index request is scheduled. It will begin soon.';
                    return response;
                }
                if (status.running && status.running !== viewName) {
                    isActive = true;
                    statusTextShort = statusText = 'Another view re-index is in process. Please try again later.';
                    return response;
                }
                if (status.running && status.running === viewName) {
                    isActive = true;
                    statusTextShort = statusText = 'Re-index is in process.';
                    if (status.view.lastRunTime) {

                        var progress = Math.round(status.view.runningTime / status.view.lastRunTime * 100);
                        if (progress > 120) {
                            statusText += 'It\'s taking more time than expected.';
                            statusTextShort += 'It\'s taking more time than expected.';
                        } else {
                            progress = Math.min(99, progress);
                            statusText += ' ' + progress + '% done.';
                            statusTextShort += ' ' + progress + '% done.';
                            if (status.view.lastRunTime > status.view.runningTime) {
                                var left = moment.duration(status.view.lastRunTime - status.view.runningTime);
                                statusText += ' Approximately ' + left.humanize() + ' left.';
                                statusTextShort += ' Approximately ' + left.humanize() + ' left.';
                            }
                        }
                    }
                    return response;
                }
                isActive = false;
                if (status.view && status.view.lastEndedAgo) {
                    statusText =  'Last re-index finished ' + status.view.lastEndedAgo.humanize() + ' ago';
                    statusTextShort = 'Re-index data (' + statusText + ')';
                } else {
                    statusTextShort = 'Re-index data';
                }
                return response;
            }).then(function () {
                updateIconButton();
                statusRefreshTimer = $timeout(getStatus, isActive ? statusRefreshDelayActive : statusRefreshDelayIdle);
            });
        }

        $scope.$on('$destroy', function() {
            api.destroy();
        });

        function refreshDialog() {
            return getStatus().then(function () {
                if (isActive) {
                    modals.alert(statusText);
                    return;
                }
                var message = 'Please note that re-indexing the drive data might take a few minutes. Do you wish to continue?';
                message += statusText ? (' (' + statusText + ')') : '';
                return modals.confirm({
                    message: message,
                    title: 'Re-index drive data',
                    okText: 'Yes'
                }).then(function () {
                    return materializedViewsDao.refresh(viewName).then(function () {
                        if (status.lastEnded) {
                            lastRefreshEnded = status.lastEnded;
                        }
                    });
                });
            }).then(function () {
                return getStatus();
            });
        }

        var iconButton = {
            'class': 'btn-icon',
            iconClass : {
                'fa': true,
                'fa-refresh': true,
                'fa-spin': isActive
            },
            tooltip: statusTextShort,
            execute: refreshDialog
        };

        function updateIconButton() {
            iconButton.iconClass['fa-spin'] = isActive;
            iconButton.tooltip = statusTextShort;
        }

        getStatus();

        var api = {
            destroy: function() {
                destroyed = true;
                if (statusRefreshTimer) {
                    $timeout.cancel(statusRefreshTimer);
                    statusRefreshTimer = null;
                }
            },
            refresh: refreshDialog,
            isActive: function () {
                return isActive;
            },
            iconButton: function () {
                return iconButton;
            },
            lastRefreshed: ''
        };
        return api;
    };
});