var m = angular.module('common');

m.service('policiesRefreshHelper', function(dao, $timeout, slowdownHelper) {
    var timeoutHandle = null;
    var refreshInterval = 60 * 1000;
    var policies = [];

    function refreshPolicies() {
        policies = _(policies).filter(function(policy) {
            return policy.validUntil.isAfter(moment());
        });
        var ids = _(policies).map(function(policy) {
            return policy.id;
        });
        if(ids.length) {
            dao.policy().refreshCountSome(ids).then(function () {
                timeoutHandle = $timeout(refreshPolicies, slowdownHelper.adjustInterval(refreshInterval));
            });
        }
    }

    return {
        clearPolicies: function () {
            if(timeoutHandle) {
                $timeout.cancel(timeoutHandle);
            }
            policies = [];
        },
        addPolicies: function(ids) {
            if(!_(ids).isArray()) {
                ids = [ids];
            }
            var jobRunning = !!policies.length;
            _(ids).each(function(id) {
                var found = _(policies).find(function(policy) {
                    return policy.id == id;
                });
                var validUntil = moment().add('millisecond', slowdownHelper.adjustInterval(refreshInterval * 1.5));
                if(found && found.validUntil.isBefore(validUntil)) {
                    found.validUntil = validUntil;
                } else {
                    policies.push({
                        id: id,
                        validUntil: validUntil
                    });
                }
            });
            if(policies.length && !jobRunning) {
                if(timeoutHandle) {
                    $timeout.cancel(timeoutHandle);
                }
                timeoutHandle = $timeout(refreshPolicies, 1000);
            }
        }
    };
});