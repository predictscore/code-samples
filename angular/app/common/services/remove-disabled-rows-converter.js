var m = angular.module('common');

m.factory('removeDisabledRowsConverter', function ($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var rows = input.data.rows || [];

        var result = _(rows).filter(function(row) {
            return !row[args.disabledColumn || 'disabled'];
        });

        deferred.resolve($.extend({}, input, {
            data: {
                rows: result
            }
        }));

        return deferred.promise;
    };
});