var m = angular.module('common');

m.factory('viewAllEmailsDebugLinkConverter', function (troubleshooting) {
    return function (text, args)  {
        if (!troubleshooting.entityDebugEnabled()) {
            return text;
        }
        return '#' + JSON.stringify({
                display: 'linked-text',
                text: text + ' #' + JSON.stringify({
                    display: 'text',
                    text: 'Show all conversations',
                    'class': 'link-like',
                    clickEvent: {
                        name: 'show-all-user-emails',
                        userId: text
                    }
                })
            });
    };
});