var m = angular.module('common');

m.factory('sqlQueries', function(modulesManager) {
    var prefixQuery = '/api/v1/sql_queries';
    var prefixExport = '/api/v1/sql_export';
    var commonQueries = [
        'policy_catalog',
        'policy_discovers',
        'policy_primary_findings_status',
        'entity/geo',
        'scan_details',
        'scan_details_opswat',
        'opswat_total',
        'opswat_infected',
        'materialized_views_status',
        'drive_query',
        'policy_findings_history',
        'policy_infoboxes',
        'dashboard_widgets_data',
        'extensions-to-mimetype',
        'policy_catalog_facets',
        'main_queue',
        'alerts',
        'alert_severity_counts',
        'alert_severity_graph',
        'alert_state_counts',
        'alert_app_counts',
        'alert_policies',
        'alert_assignees',
        'reports',
        'crypt_pair',
        'inline_policy',
        'scan_per_day',
        'main_queue_stats',
        'main_queue_stats_group',
        'apps_added_value',
        'download_history',
        'system_log',
        'email_domains',
        'email_domains_types',
        'sectool_exceptions_status',
        'ap_avanan_exceptions_ips',
        'ap_avanan_exceptions_emails',
        'ap_avanan_exceptions_domains',
        'ap_avanan_exceptions_nicknames',
        'ap_avanan_exceptions_no_spf_emails',
        'ap_avanan_links', // obsolete
        'ap_avanan_debug_links', // obsolete
        'last_webuser_logins',
        'policies_change_by_tags',
        'solgate_exceptions',
        'ap_bitdefender_links', // obsolete
        'sb_checkpoint_links', // obsolete
        'solgate_links',
        'cp_capsule_info'
    ];
    var saasQueriesMap = {
        sharefile: {
            'folder_events': 'user_events',
            'file_events': 'user_events',
            'group_events': 'user_events'
        },
        salesforce: {
            'group_events': 'user_events'  
        },
        egnyte: {
            'folder_events': 'user_events'
        },
        slack: {
            'file_events': 'user_events',
            'folder_events': 'user_events',
            'group_events': 'user_events'
        },
        office365_sharepoint: {
            'file_events': 'user_events',
            'folder_events': 'user_events'
        }
    };
    return function(name, exportFormat, saas) {
        saas = saas || modulesManager.currentModule();
        if(saasQueriesMap[saas]) {
            name = saasQueriesMap[saas][name] || name;
        }
        
        var prefix = exportFormat ? (prefixExport + '/' + exportFormat) : prefixQuery;
        if(name.indexOf('/') === 0) {
            return prefix + name;
        }
        if(_(commonQueries).contains(name)) {
            return prefix + '/common/' + name;
        }
        
        return prefix + '/' + saas + '/' + name;
    };
});