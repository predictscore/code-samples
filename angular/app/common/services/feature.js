var m = angular.module('common');

m.factory('feature', function($q, http, $routeParams, analytics) {
    var deferred = null;
    var result = {
        init: function() {
            if(!deferred) {
                deferred = $q.defer();
                http.get('/api/v1/generic/ui_features').then(function (response) {
                    $.extend(true, result, {
                        showActiveResolver: true,
                        maxRowsInPolicyForSorting: 1000
                    }, response.data);
                    if (window.guiVersion === 2) {
                        $.extend(result, {
                            guiV2: true,
                            inline_o365: false,
                            inline_gmail: false
                        });
                    }
                    analytics.init(result);
                    deferred.resolve(response);
                }, function(error) {
                    deferred.reject(error);
                    deferred = null;
                });
            }
            return deferred.promise;
        },
        isShowAnomalyTable: function() {
            return result.showAnomalyTable && _(['shadow-it', 'google_drive']).contains($routeParams.module);
        },
        policyPropertiesRecursionLimit: 2
    };

    return result;
});