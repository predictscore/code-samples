var m = angular.module('common');

m.factory('analytics', function() {
    var initDone = false;
    return {
        init: function(feature) {
            if(feature && feature.gaTrackingID) {
                /* jshint ignore:start */
                (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments);},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m);
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
                ga('create', feature.gaTrackingID, 'auto');
                /* jshint ignore:end */
                initDone = true;
            }
        },
        pageView: function(url, title) {
            if(initDone) {
                /* jshint ignore:start */
                ga('set', {
                    page: url,
                    title: title
                });
                ga('send', 'pageview');
                /* jshint ignore:end */
            }
        }
    };
});