var m = angular.module('common');

m.factory('mimeIcons', function (path, $q) {
    var root = path('common');
    var iconsPromise = root.staticFile('mime-icons.txt').retrieve().then(function(response) {
        var result = {};
        _(response.data.split(/\n/)).chain().filter(function(fileName) {
            return _(fileName).isString() && fileName.length;
        }).each(function(fileName) {
            var mimeType = fileName
                .replace('.svg', '')
                .replace('gnome-mime-', '')
                .replace('application-', 'application/')
                .replace('image-', 'image/')
                .replace('text-', 'text/')
                .replace('message-', 'message/')
                .replace('video-', 'video/');
            result[mimeType] = root.img('mime/' + fileName);
        }).value();
        return result;
    });

    return function () {
        return iconsPromise.then(function(icons) {
            return $q.when(function(mimeType) {
                if (_(mimeType).isArray()) {
                    var types = _(mimeType).compact();
                    if (!types.length) {
                        return icons.unknown;
                    }
                    var foundType = _(types).find(function (type) {
                        return icons[type];
                    });
                    if (foundType) {
                        return icons[foundType];
                    }
                    var foundIcon;
                    _(types).find(function (type) {
                        foundIcon = _(icons).find(function (icon, name) {
                            return name.indexOf('/') == -1 && _(type).isString() && type.indexOf(name) >= 0;
                        });
                        return foundIcon;
                    });
                    return foundIcon || icons.unknown;
                }
                mimeType = mimeType || '';
                return icons[mimeType] || _(icons).find(function (icon, name) {
                        return name.indexOf('/') == -1 && _(mimeType).isString() && mimeType.indexOf(name) >= 0;
                    }) || icons.unknown;
            });
        });
    };
});