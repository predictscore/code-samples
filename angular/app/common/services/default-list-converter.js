var m = angular.module('common');

m.factory('defaultListConverter', function ($q, $filter, $injector, fieldFormatHelper) {
    return function(input, args) {
        if(_(args.columns).isUndefined()) {
            return $q.when(input);
        }
        
        var deferred = $q.defer();

        var defaultTableConverters = $injector.get('defaultTableConverters');
        var converters = $.extend({}, args.converters, defaultTableConverters(args));

        var rows = input.data;
        if(input.data.rows) {
            rows = input.data.rows;
        }
        if(!rows) {
            rows = [];
        }

        function getText(data, id) {
            if(_(data).isUndefined()) {
                return undefined;
            }
            if(_(id).isNull()) {
                return data;
            }
            if (_(data).isNull()) {
                return null;
            }
            var dot = id.indexOf('.');
            if(~dot) {
                return getText(data[id.slice(0, dot)], id.slice(dot + 1));
            }
            return data[id];
        }

        function buildColumnsMap(row) {
            var result = {};
            _(args.columns).each(function(column) {
                result[column.id] = getText(row, column.id);
            });
            return result;
        }

        var data = _(rows).map(function(row) {
            var columnsMap = buildColumnsMap(row);
            return _(args.columns).map(function(column) {
                var original = columnsMap[column.id];
                var tags;
                if(column.type === 'tag') {
                    tags = original;
                    if(!_(tags).isArray()) {
                        tags = [tags];
                    }
                    if (_(tags).isNull() || _(tags).isUndefined()) {
                        tags = [column.def];
                    } else {
                        if (!_(column.converter).isUndefined()) {
                            tags = _(tags).map(function (tag) {
                                var converter = converters[column.converter] || column.converter;
                                return converter(tag, column.id, columnsMap, column.converterArgs || {}, converters);
                            });
                        }
                    }
                    return {
                        tags: tags,
                        originalTags: tags
                    };
                } else {
                    var result = {};
                    var getText = function(original) {
                        var defUsed = false;
                        var text = original;
                        if (!_(column.def).isUndefined() && (_(text).isNull() || _(text).isUndefined())) {
                            text = column.def;
                            defUsed = true;
                        } 
                        if(!defUsed || column.convertDef) {
                            text = fieldFormatHelper(text, column);
                            if (!_(column.converter).isUndefined()) {
                                var converter = converters[column.converter] || column.converter;
                                if(_(converter).isString()) {
                                    alert('converter ' + converter + ' not defined');
                                }
                                text = converter(text, column.id, columnsMap, column.converterArgs || {}, converters);
                                if (_(text).isObject() && _(text.then).isUndefined()) {
                                    tags = text.tags;
                                    text = text.text;
                                }
                            }
                        }
                        return text;
                    };
                    var text = null;
                    //We don't support promising for tags, we should remove it and replace with text
                    if(original && _(original.then).isFunction()) {
                        var textDeferred = $q.defer();
                        original.then(function(original) {
                            result.originalResolved = original;
                            $q.when(getText(original)).then(function(text) {
                                textDeferred.resolve(text);
                            }, textDeferred.reject);
                        }, textDeferred.reject);
                        text = textDeferred.promise;
                    } else {
                        //Get text set internally tags, right now it seems like hack, 
                        //but after removing tags - this function will be ok
                        text = getText(original);
                    }

                    result.tags = tags;
                    result.text = text;
                    result.originalText = original;
                    return result;
                }
            });
        });

        var currentPage;
        if(_(args.http).isObject() && _(args.http.pagination).isObject()) {
            currentPage = args.http.pagination.offset / args.http.pagination.limit + 1;
        }

        var result = {
            pagination: {
                total: input.data.total_rows || rows.length,
                current: input.data.page || currentPage,
                haveNext: input.data.haveNext
            },
            info: args.info,
            options: args.options,
            data: data
        };

        deferred.resolve($.extend({}, input, {
            data: result
        }));

        return deferred.promise;
    };
});