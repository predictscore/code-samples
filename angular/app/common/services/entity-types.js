var m = angular.module('common');

m.factory('entityTypes', function() {
    var data = {
        google_drive: {
            application: 'google_app',
            file: 'google_file',
            folder: 'google_folder',
            user: 'google_user',
            group: 'google_group',
            anomaly: 'google_anomaly_event'
        },
        box: {
            file: 'box_file',
            folder: 'box_folder',
            user: 'box_user',
            group: 'box_group',
            anomaly: 'box_anomaly_event'
        },
        dropbox: {
            file: 'dropbox_file',
            folder: 'dropbox_folder',
            user: 'dropbox_user',
            group: 'dropbox_group',
            application: 'dropbox_api_app',
            anomaly: 'dropbox_anomaly_event'
        },
        office365_onedrive: {
            application: 'office365_onedrive_application',
            file: 'office365_onedrive_file',
            folder: 'office365_onedrive_folder',
            user: 'office365_onedrive_user',
            site: 'office365_onedrive_site'
        },
        office365_emails: {
            folder: 'office365_emails_folder',
            file: 'office365_emails_attachment',
            user: 'office365_emails_user',
            email: 'office365_emails_email',
            anomaly: 'office365_emails_anomaly_event',
            header: 'office365_emails_header'
        },
        google_mail: {
            file: 'google_mail_attachment',
            email: 'google_mail_email',
            header: 'google_mail_header'
        },
        sharefile: {
            file: 'sharefile_file',
            folder: 'sharefile_folder',
            user: 'sharefile_user',
            group: 'sharefile_group'
        },
        egnyte: {
            file: 'egnyte_file',
            folder: 'egnyte_folder',
            user: 'egnyte_user',
            group: 'egnyte_group'
        },
        slack: {
            file: 'slack_file',
            user: 'slack_user',
            folder: 'slack_channel',
            group: 'slack_usergroup'
        },
        amazon_s3: {
            file: 'amazon_s3_object',
            user: 'amazon_s3_user',
            group: 'amazon_s3_group',
            folder: 'amazon_s3_folder',
            bucket: 'amazon_s3_bucket',
            'iam-user': 'amazon_s3_iam_user',
            'iam-group': 'amazon_s3_iam_group'
        },
        servicenow: {
            file: 'servicenow_attachment',
            user: 'servicenow_user',
            group: 'servicenow_group',
            email: 'servicenow_email'

        },
        salesforce: {
            user: 'salesforce_user',
            file: 'salesforce_file',
            folder: 'salesforce_container',
            group: 'salesforce_group'
        },
        office365_sharepoint: {
            application: 'office365_sharepoint_application',
            file: 'office365_sharepoint_file',
            folder: 'office365_sharepoint_folder',
            user: 'office365_sharepoint_user',
            site: 'office365_sharepoint_site'
        },
        google_storage: {
            file: 'google_storage_object',
            bucket: 'google_storage_bucket',
            project: 'google_storage_project',
            user: 'google_storage_iam_account'
        }
    };

    var reverse = {};
    var saasReverse = {};
    _(data).each(function(saas, saasName) {
        _(saas).each(function(value, key) {
            reverse[value] = key;
            saasReverse[value] = saasName;
        });
    });

    return {
        toBackend: function (saas, type) {
            return (data[saas] || {})[type];
        },
        toLocal: function(type) {
            return reverse[type];
        },
        toSaas: function(type) {
            return saasReverse[type];
        },
        getTypes: function() {
            return _(saasReverse).keys();
        }
    };
});