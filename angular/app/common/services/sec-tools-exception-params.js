var m = angular.module('common');

m.factory('secToolsExceptionParams', function(modals, entityTypes) {

    var securityTypes = [{
        id: 'av',
        label: 'Malicious Exception',
        addTitle: 'Report as not malicious'
    }, {
        id: 'ap',
        label: 'Phishing Exception',
        addTitle: 'Report as not a phishing'
    }, {
        id: 'dlp',
        label: 'DLP Exception',
        addTitle: 'Report as not a Data Leakage'
    }];

    var typesMap = _(securityTypes).chain().map(function (typeInfo) {
        return [typeInfo.id, typeInfo];
    }).object().value();

    var secToolsExceptionParams = {
        show: function(title, status) {
            status = status || {};
            return modals.params([{
                lastPage: true,
                params: _(secToolsExceptionParams.securityTypes).map(function (typeInfo, idx) {
                    return  {
                        id: typeInfo.id,
                        data: status[typeInfo.id],
                        rightLabel: typeInfo.label,
                        type: 'boolean',
                        emptyLabel: true
                    };
                })
            }], title);
        },
        securityTypes: securityTypes,
        getLabel: function (id) {
            return typesMap[id].label;
        },
        getAddTitle: function (id, entityType) {
            var localEntity = entityTypes.toLocal(entityType) || 'file';
            return typesMap[id].addTitle.replace(/\{\{([^\}]*)\}\}/g, function (a, key) {
                switch (key) {
                    case 'entityName': return (localEntity === 'email' ? 'message' : localEntity);
                }
                return '';
            });
        }
    };
    return secToolsExceptionParams;
});