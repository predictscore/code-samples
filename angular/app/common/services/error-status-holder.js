var m = angular.module('common');

m.factory('errorStatusHolder', function() {
    var status = null;
    return {
        errorStatus: function(newStatus) {
            if(!_(newStatus).isUndefined()) {
                status = newStatus;
            }
            return status;
        }
    };
});