var m = angular.module('common');

m.factory('timeOffsetHelper', function() {
    var tzOffset = new Date().getTimezoneOffset();
    return {
        offsetToLocal: function(offset) {
            var time = offset - tzOffset;
            var local = '';
            if(time < 0) {
                time += 24 * 60;
            } else if(time >= 24 * 60) {
                time -= 24 * 60;
            }
            var hour = Math.floor(time / 60);
            if(hour < 10) {
                local += '0';
            }
            local += hour;
            local += ':';
            var minute = time % 60;
            if(minute < 10) {
                local += '0';
            }
            local += minute;
            return local;
        },
        localToOffset: function(local) {
            var splitted = local.split(':');
            var hour = parseInt(splitted[0]);
            var minute = parseInt(splitted[1]);
            var time = hour * 60 + minute;
            time += tzOffset;
            if(time < 0) {
                time += 24 * 60;
            } else if(time > 24 * 60) {
                time -= 24 * 60;
            }
            return time;
        }
    };
});