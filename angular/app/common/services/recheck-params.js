var m = angular.module('common');

m.factory('recheckParams', function(modals, modulesManager, modulesDao, entityTypes, $q) {
    return {
        show: function(title, entityType, secTypes) {
            var getEntityInfo;
            var appVariants;

            if (secTypes) {
                getEntityInfo = $q.when({
                        sec_type_connected: secTypes
                });
            } else {
                var entitySaas = entityTypes.toSaas(entityType);
                if (entitySaas) {
                    getEntityInfo = modulesDao.entityInfoRaw(entitySaas, entityType);
                } else {
                    getEntityInfo = $q.when();
                }
            }

            return getEntityInfo.then(function (data) {
                if (data && data.sec_type_connected) {
                    appVariants = _(modulesManager.activeSecAppsFull()).chain().filter(function (app) {
                        return app.sec_type && _(data.sec_type_connected).contains(app.sec_type);
                    }).map(function(app) {
                        return {
                            id: app.name,
                            label: app.label
                        };
                    }).value();
                }
            }).then(function () {
                if (!appVariants) {
                    appVariants = _(modulesManager.activeSecAppsFull()).map(function(app) {
                        return {
                            id: app.name,
                            label: app.label
                        };
                    });
                }
                return modals.params([{
                    lastPage: true,
                    params: [{
                        id: 'selectApplications',
                        type: 'radio-group',
                        emptyLabel: true,
                        buttons: [{
                            label: 'Scan with all security tools',
                            value: false
                        }, {
                            label: 'Scan with specific security tools',
                            value: true
                        }],
                        data: false
                    }, {
                        id: 'apps',
                        type: 'list',
                        label: 'Scan with',
                        variants: appVariants,
                        enabled: [{
                            param: 'selectApplications',
                            equal: true
                        }]
                    }]
                }], title);
            });
        }
    };
});