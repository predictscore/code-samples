var m = angular.module('common');

//Workaround to recreate dropdown on close/open
m.factory('recreateDropdownWorkaround', function($timeout) {
    return function($scope, element, options) {
        options = $.extend({
            onOpen: _.noop
        }, options);
        var timeoutHandle = null;

        function processVisibility() {
            if ($(element).hasClass('open')) {
                $scope.showDropdown = true;
                timeoutHandle = $timeout(processVisibility, 1000);
            }
            else {
                $scope.showDropdown = false;
            }
        }
        processVisibility();

        $scope.buttonClicked = function() {
            $timeout.cancel(timeoutHandle);
            $timeout(function() {
                processVisibility();
            });
            if (!$scope.showDropdown) {
                options.onOpen();
            }
        };

        $scope.$on('$destroy', function() {
            $timeout.cancel(timeoutHandle);
        });
    };
});