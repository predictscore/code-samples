var m = angular.module('common');

m.factory('authorizationLinkHelper', function ($q, modals, scriptConverter, modulesDao, $rootScope, appConfigurator, commonDao) {

    function setWindowParams() {
        window.onSaaSAuthSuccess = function () {
            if (window.configureOnSaaSAuthSuccess) {
                window.sendConfigureRequest(window.configureOnSaaSAuthSuccess);
            }
            $rootScope.$broadcast('saas-authorized');
        };
        window.sendConfigureRequest = function (saas) {
            if (!window.configureOnSaaSAuthSuccess) {
                $rootScope.$broadcast('configure-app', {
                    'app': saas
                });
            }
        };
        window.configureOnSaaSAuthSuccess = false;
    }

    function openBoxLink(url) {
        var img = document.createElement('img');
        var win = window.open('', '_blank');

        function logoutDone() {
            win.location.href = url;
        }

        img.onload = logoutDone;
        img.onerror = logoutDone;
        img.src = 'https://app.box.com/logout';
        win.focus();
    }

    function openAuthLink(saas, originalAuthParams, authApi, authTitle, linkUrl, sameWindow) {

        var authParams;
        var haveConfigParams = !!_(originalAuthParams).find(function (param) {
            return param.config;
        });
        var originalConfig;

        var p = $q.when();
        var win;
        if (haveConfigParams) {
            p = p.then(function () {
                return modulesDao.retrieveConfig(saas).onRequestFail(function () {
                    return $q.when({
                        data: {}
                    });
                });
            }).then(function (response) {
                originalConfig = response.data || {};
                authParams = {};
                _(originalAuthParams).each(function (param, key) {
                    authParams[key] = $.extend({}, response.data[key], param);
                });
            });
        } else {
            authParams = originalAuthParams;
        }
        p = p.then(function () {
            return appConfigurator.convertConfigParams(authParams, saas);
        }).then(function (input) {
            if (input.data.notExist) {
                return null;
            }
            input.data.title = authTitle || 'Authorization Parameters';
            var haveRequired = !!_(input.data.pages).find(function (page) {
                return _(page.params).find(function (param) {
                    if (param.validation && param.validation.required) {
                        return true;
                    }
                });
            });

            return modals.params(input.data.pages, input.data.title, undefined, {
                showSkip: !haveRequired,
                okButton: 'Next'
            });
        }).then(function (params) {
            if (sameWindow) {
                win = window;
            } else {
                win = window.open('', '_blank');
                win.focus();
            }
            return params;
        });
        if (haveConfigParams) {
            p = p.then(function (params) {
                if (params) {
                    var configParams = {};
                    _(originalAuthParams).each(function (param, key) {
                        if (param.config) {
                            configParams[key] = params[key];
                        }
                    });
                    var saveConfigParams = _(configParams).pick(function (value, key) {
                        if (originalConfig && originalConfig[key] && originalConfig[key].noSave) {
                            return false;
                        }
                        return true;
                    });
                    var changeQueries = _(originalConfig).chain().map(function (conf, paramKey) {
                        if (!conf.onChange || conf.value === configParams[paramKey]) {
                            return false;
                        }
                        var onChangeInfo  = _(conf.onChange).find(function (info) {
                            return info.value === configParams[paramKey];
                        });
                        if (!onChangeInfo) {
                            return false;
                        }
                        return commonDao.callCustomUrl(onChangeInfo.method, onChangeInfo.url).catch(function () {
                            return null;
                        });
                    }).compact().value();
                    return $q.all(changeQueries).then(function () {
                        return modulesDao.saveConfig(saas, saveConfigParams, true).then(function() {
                            return params;
                        });
                    });
                }
                return params;
            });
        }
        if (authApi) {
            p = p.then(function (params) {
                return modulesDao.authLink(saas, params).then(function (data) {
                    if (data.data.configureOnSuccess) {
                        window.configureOnSaaSAuthSuccess = saas;
                    }
                    linkUrl = data.data.url;
                });
            });
        }
        return p.then(function () {
            $rootScope.$broadcast('close-app-configurator');
            win.location.href = linkUrl;
        });
    }

    return {
        onClick: function (event, data) {
            setWindowParams();
            if (data.authLinkApi || data.authLinkParams) {
                event.preventDefault();
                openAuthLink(data.authorization, data.authLinkParams, data.authLinkApi, data.authLinkParamsTitle, data.href);
                return;
            }

            if (data.authorization === 'box') {
                event.preventDefault();
                openBoxLink(data.href);
                return;
            }
        },
        openLink: function (options, sameWindow, noBoxLogout) {
            setWindowParams();
            var saas = options.moduleName;
            var href = options.configurationLink;

            if (options.authLinkApi || options.authLinkParams) {
                openAuthLink(saas, options.authLinkParams, options.authLinkApi, options.authLinkParamsTitle, href, sameWindow);
                return;
            }

            if (saas === 'box' && !noBoxLogout) {
                openBoxLink(href);
                return;
            }
            if (sameWindow) {
                window.location.href = href;
            } else {
                window.open(href, '_blank').focus();
            }
        }
    };
});