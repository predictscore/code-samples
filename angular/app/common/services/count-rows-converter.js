var m = angular.module('common');

m.factory('countRowsConverter', function ($q) {
    return function (input, args) {
        var deferred = $q.defer();

        var result = {};

        _(input.data.rows).each(function(row) {
            var count = result[row[args.field]] || 0;
            count++;
            result[row[args.field]] = count;
        });

        deferred.resolve($.extend({}, input, {
            data: result
        }));

        return deferred.promise;
    };
});