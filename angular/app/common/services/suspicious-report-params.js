var m = angular.module('common');

m.factory('suspiciousReportParams', function(modals, emailDao, $q, modulesDao, entityTypes) {
    return {
        show: function(title, entityType, entityId, args) {

            var entitySaas = entityTypes.toSaas(entityType) || args.saas ||
                args.objectTypesData && args.objectTypesData.entities[entityType].saas;
            var localEntity = entityTypes.toLocal(entityType);

            var getEntityInfo;
            if (args.secTypes) {
                getEntityInfo = $q.when({
                    sec_type_connected: secTypes
                });
            } else {
                if (localEntity === 'file' || localEntity === 'email') {
                    getEntityInfo = $q.when();
                } else {
                    if (entitySaas) {
                        getEntityInfo = modulesDao.entityInfoRaw(entitySaas, entityType);
                    } else {
                        getEntityInfo = $q.when();
                    }
                }
            }

            return getEntityInfo.then(function (data) {
                var secTypes = data && data.sec_type_connected || [];
                var variants = [{
                    id: "clean",
                    label: "Clean"
                }, {
                    id: "unknown",
                    label: "Unknown"
                }];
                if (localEntity === 'email' || _(secTypes).contains('ap')) {
                    variants.unshift({
                        id: "spam",
                        label: "Spam"
                    });
                    variants.unshift({
                        id: "phishing",
                        label: "Phishing"
                    });
                }
                if (localEntity === 'file' || _(secTypes).contains('av')) {
                    variants.unshift({
                        id: "malicious",
                        label: "Malicious"
                    });
                }
                var params =  [{
                    id: 'status',
                    type: 'selector',
                    label: 'Report as ',
                    variants: variants,
                    data: variants[0].id
                }];

                if (entityType && entityId && (localEntity === 'email' || _(secTypes).contains('ap'))) {
                    var resolveTimeout = $q.defer();
                    params.push({
                        type: 'message',
                        message: 'Searching for similar emails...',
                        emptyLabel: true,
                        resolveTimeout: resolveTimeout,
                        resolve: function () {
                            return $q.all([
                                emailDao.similarEmailCount(entityType, entityId, 'subject_sender', resolveTimeout.promise),
                                emailDao.similarEmailCount(entityType, entityId, 'subject', resolveTimeout.promise),
                            ]).then(function (responses) {
                                var counts = _([{
                                    'type': 'subject_sender',
                                    label: 'emails with same Subject and Sender',
                                    count: responses[0].data.count
                                }, {
                                    'type': 'subject',
                                    label: 'emails with same Subject',
                                    count: responses[1].data.count
                                }]).filter(function (data) {
                                    return data.count;
                                });

                                if (!counts.length) {
                                    return {
                                        type: 'message',
                                        message: 'No similar emails found',
                                        emptyLabel: true,
                                    };
                                }
                                var options = _(counts).map(function (item) {
                                    return {
                                        value: item.type,
                                        label: 'Report also ' + item.label + ' (' + item.count + ' emails)'
                                    };
                                });
                                options.unshift({
                                    label: 'Report only this e-mail'
                                });

                                return {
                                    id: 'similar_mode',
                                    type: 'radio-group',
                                    emptyLabel: true,
                                    buttons: options
                                };
                            });
                        }
                    });
                }
                return modals.params([{
                    lastPage: true,
                    params: params,
                    okButton: 'Send Report'
                }], title);
            });
        }
    };
});