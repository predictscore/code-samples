var m = angular.module('common');

m.filter('bytes', function() {
    var units = ['Bytes', 'Kilobytes', 'Megabytes', 'Gigabytes', 'Terabytes', 'Petabytes'];
    var unitsShort = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
    return function(bytes, precision, shortForm) {
        var displayUnits = shortForm ? unitsShort : units;
        if (_(parseFloat(bytes)).isNaN() || !_(bytes).isFinite()) {
            return '-';
        }
        if(bytes < 1024) {
            return bytes + ' ' + displayUnits[0];
        }
        if (typeof precision === 'undefined') {
            precision = 1;
        }
        var number = Math.floor(Math.log(bytes) / Math.log(1024));
        return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + displayUnits[number];
    };
});