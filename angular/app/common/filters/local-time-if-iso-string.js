var m = angular.module('common');

m.filter('localTimeIfIsoString', function() {
    var defaultFormat = 'YYYY-MM-DD HH:mm:ss';
    return function(val, dateFormat) {
        dateFormat = dateFormat || defaultFormat;
        if (!_(val).isString() || !/^\d{4}-\d{2}-\d{2}T[\d\.:\+\-]+$/.test(val)) { //This should match dates in iso format returned by backend
            return val;
        }
        return moment.utc(val).local().format(dateFormat);
    };
});