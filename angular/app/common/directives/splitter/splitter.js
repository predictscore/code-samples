var m = angular.module('common');

m.directive('splitter', function($parse, $timeout) {
    return {
        restrict: 'A',
        replace: false,
        link: function($scope, element, attrs) {
            var leftPanel = element.children(":first");
            var rightPanel = leftPanel.next();
            var timeout = null;
            $scope.$watch(function() {
                return $parse(attrs.splitter)($scope).disabled;
            }, function(disabled) {
                if(disabled) {
                    if(_(element.destroy).isFunction()) {
                        element.destroy();
                    }
                } else {
                    element.split($parse(attrs.splitter)($scope));
                }
            });
            $scope.$on('$destroy', function () {
                if(_(element.destroy).isFunction()) {
                    element.destroy();
                }
            });
        }
    };
});