var m = angular.module('common');

m.directive('bxSlider', function($parse, path, $timeout) {
    return {
        restrict: 'A',
        replace: false,
        transclude: true,
        templateUrl: path('common').directive('bx-slider').template(),
        link: function($scope, element, attrs, ctrl, transclude) {
            var slider = null;

            function destroy() {
                if(!_(slider).isNull()) {
                    slider.destroySlider();
                }
            }

            var options = $parse(attrs.bxSlider)($scope);

            transclude($scope.$parent.$parent, function(clone) {
                var body = element.find('.bx-slider-body');
                body.html(clone);

                $timeout(function() {
                    if(_(slider).isNull()) {
                        slider = body.bxSlider(options);
                    } else {
                        slider.reloadSlider();
                    }
                });
            });

            $scope.$on('slider-size-changed', function () {
                if(!_(slider).isNull()) {
                    // redrawSlider (function that is called when window size changes) works not very good with our slider,
                    // so doing reload on event where we definitely know size changed (should not happen often)
                    slider.reloadSlider();
                }
            });

            $scope.$on('$destroy', destroy);
        }
    };
});