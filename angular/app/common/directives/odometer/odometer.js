var m = angular.module('common');

m.directive('odometer', function() {
    return {
        restrict: 'A',
        replace: false,
        scope: {
            model: '=odometer',
            options: '=?'
        },
        link: function($scope, element, attr) {
            var od = null;
            $scope.$watch('model', function() {
                if(_(od).isNull()) {
                    od = new Odometer($.extend(true, {
                        el: element[0],
                        value: $scope.model,

                        // Any option (other than auto and selector) can be passed in here
                        format: 'd',
                        theme: 'minimal'
                    }, $scope.options || {}));
                } else {
                    od.update($scope.model);
                }
            });
        }
    };
});