var m = angular.module('common');

m.directive('datetimeInterval', function(path) {

    var periodTypes = [{
        id: 'seconds',
        label: 'seconds'
    }, {
        id: 'minutes',
        label: 'minutes'
    }, {
        id: 'hours',
        label: 'hours'
    }, {
        id: 'days',
        label: 'days'
    }, {
        id: 'months',
        label: 'months'
    }, {
        id: 'years',
        label: 'years'
    }];

    function parseInterval(interval) {
        var pair = interval.split(/\s+/);
        var result = {
            amount: parseInt(pair[0]),
            type: 'seconds'
        };
        if (pair.length > 1) {
            var lowerCased = pair[1].toLowerCase();
            var lowerCaseds = lowerCased + 's';
            var periodType = _(periodTypes).find(function (period) {
                return period.id === lowerCased || period.id === lowerCaseds;
            });
            if (periodType) {
                result.type = periodType.id;
            }
        }
        return result;
    }

    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('common').directive('datetime-interval').template(),
        scope: {
            model: '=datetimeInterval'
        },
        link: function($scope) {
            $scope.periodTypes = periodTypes;
            $scope.internalModel = {};
            $scope.$watch('model', function(newVal, oldVal) {
                var parsed = parseInterval(newVal);
                $scope.internalModel.amount = parsed.amount;
                $scope.internalModel.type = parsed.type;
            });

            $scope.$watchCollection('internalModel', function () {
                if (!$scope.internalModel.amount || !$scope.internalModel.type) {
                    return;
                }
                $scope.model = $scope.internalModel.amount + ' ' + $scope.internalModel.type;
            });
        }
    };
});