var m = angular.module('common');

m.directive('timePicker', function(path, timeOffsetHelper) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('common').directive('time-picker').template(),
        scope: {
            model: '=timePicker'
        },
        link: function($scope) {
            $scope.$watch('model', function() {
                $scope.internalModel = timeOffsetHelper.offsetToLocal($scope.model);
            });
            
            $scope.onTimeChange = function() {
                $scope.model = timeOffsetHelper.localToOffset($scope.internalModel);
            };
        }
    };
});