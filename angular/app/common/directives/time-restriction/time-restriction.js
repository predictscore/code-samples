var m = angular.module('common');

m.directive('timeRestriction', function($timeout, $parse) {
    return {
        restrict: 'A',
        replace: false,
        scope: false,
        link: function($scope, element, attrs) {
            element.bind('keypress', function(e) {
                var key;
                var keychar;

                if (window.event) {
                    key = window.event.keyCode;
                } else if (e) {
                    key = e.which;
                } else {
                    return true;
                }
                keychar = String.fromCharCode(key);

                if (_([null, 0, 8, 9, 13, 27]).contains(key) || ("0123456789:").indexOf(keychar) > -1) {
                    return true;
                }
                return false;
            });
            
            function init() {
                var model = $parse(attrs.ngModel);
                var onChange = attrs.ngTimeChange && $parse(attrs.ngTimeChange) || _.noop;
                var modelValue = model($scope) || '00:00';
                
                if(modelValue.length >= 2 && modelValue.indexOf(':') == -1) {
                    modelValue = modelValue.slice(0, 2) + ':' + modelValue.slice(2);
                }
                var splitted = modelValue.split(':');
                var hour = parseInt(splitted[0]);
                var minute = 0;
                if(splitted.length > 1) {
                    minute = parseInt(splitted[1] || '0');
                }
                hour = Math.max(0, Math.min(hour, 23));
                minute = Math.max(0, Math.min(minute, 59));
                var newModel = '';
                if(hour < 10) {
                    newModel += '0';
                }
                newModel += hour;
                newModel += ':';
                if(minute < 10) {
                    newModel += '0';
                }
                newModel += minute;
                if(newModel != model) {
                    model.assign($scope, newModel);
                    onChange($scope);
                }
            }

            element.bind('blur', function() {
                $timeout(init);
            });
        }
    };
});