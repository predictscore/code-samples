var m = angular.module('common');

m.directive('stackedProgress', function(path) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('common').directive('stacked-progress').template(),
        scope: {
            model: '=stackedProgress'
        },
        link: function($scope, element, attrs) {
            $scope.getWidth = function(bar) {
                if($scope.model.total === 0) {
                    return 100;
                }
                return bar.value * 100 / $scope.model.total;
            };
        }
    };
});