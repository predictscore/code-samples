var m = angular.module('common');

m.directive('inlineList', function(path, recreateDropdownWorkaround, modals, defaultListConverter) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('common').directive('inline-list').template(),
        scope: {
            model: '=inlineList'
        },
        link: function($scope, element, attrs) {
            $scope.expanded = function () {
                var expanded = $scope.model.expanded || 1;
                if ($scope.model.collapseRest) {
                    return expanded;
                }
                return ($scope.model.data.length > expanded) ? 0 : expanded;
            };
            
            $scope.initialData = _($scope.model.data).first(10);
            $scope.elementTooltip = function (element) {
                return $scope.model.elementAsTooltip && !/^#\{/.test(element) && element;
            };
            
            var options = {
                "options": {
                    "checkboxes": false,
                    "singleSelection": false,
                    "autoUpdate": 60000,
                    "forceServerProcessing": false
                },
                "columns": [{
                    "id": "value",
                    "text": "Value"
                }]
            };
            
            var tableOptions = {
                pagesAround: 2,
                pageSize: 20,
                pagination: {
                    page: 1,
                    ordering: {},
                    filter: ''
                },
                disableTop: true,
            };
            $scope.openMore = function() {
                modals.table({
                    tableOptions: tableOptions,
                    options: options,
                    'class': 'avanan-table', 
                    retrieve: function(args) {
                        var response = $scope.model.data;
                        if(args.filter) {
                            response = _(response).filter(function(d) {
                                return d && d.indexOf(args.filter) != -1;
                            });
                        }
                        return defaultListConverter({
                            data: {
                                total_rows: response.length,
                                page: Math.floor(args.offset / args.limit) + 1,
                                rows: _(response).chain().rest(args.offset).first(args.limit).map(function(value) {
                                    return {
                                        value: value
                                    };
                                }).value()
                            }
                        }, options);
                    },
                    cancelText: 'Close',
                    okText: null,
                    title: 'List items',
                    closeOnNavigate: true
                }, 'lg');
            };
            
            recreateDropdownWorkaround($scope, $(element).find('> .dropdown').get(0));
        }
    };
});