var m = angular.module('common');

m.directive('linkedEditable', function(path, $timeout) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('common').directive('linked-editable').template(),
        scope: {
            model: '=linkedEditable'
        },
        link: function($scope, element, attrs) {

            $scope.internalModel = {};

            function updateModelText() {
                $scope.internalModel.display = $scope.model.text;
                if ($scope.model.editType === 'select' && $scope.model.variants) {
                    var variant = _($scope.model.variants).find(function (el) {
                        return el.id === (_($scope.model.value).isUndefined() ? $scope.model.text : $scope.model.value);
                    });
                    if (variant) {
                        $scope.internalModel.display = variant.label;
                    }
                }
            }
            updateModelText();

            $scope.isDropdownOpen = false;
            $scope.dropdownToggled = function (open) {
                if (open) {
                    $scope.internalModel.text = _($scope.model.value).isUndefined() ? $scope.model.text : $scope.model.value;
                }
                if (open && $scope.model.editType === 'text') {
                    $timeout(function () {
                        element.find('textarea').focus();
                    });
                }
                if (open && $scope.model.editType === 'string') {
                    $timeout(function () {
                        element.find('input').focus();
                    });
                }
            };

            $scope.applyChanges = function () {
                var newVal = _($scope.internalModel.text).isString() ? $.trim($scope.internalModel.text) : $scope.internalModel.text;
                var oldVal = _($scope.model.value).isUndefined() ? $scope.model.text : $scope.model.value;
                if (newVal !== oldVal && (newVal || oldVal)) {
                    if (_($scope.model.value).isUndefined()) {
                        $scope.model.text = newVal;
                    } else {
                        $scope.model.value = newVal;
                    }
                    updateModelText();
                    if(!_($scope.model.changedEvent).isUndefined()) {
                        var eventData = angular.copy($scope.model.changedEvent);
                        eventData.newValue = newVal;
                        eventData.oldValue = oldVal;
                        $scope.$emit(eventData.name, eventData);
                    }
                }
                $timeout(function () {
                    $scope.closeDropdown();
                });
            };

            $scope.closeDropdown = function () {
                $scope.isDropdownOpen = false;
            };

            $scope.variantSelected = function (variant) {
                $scope.internalModel.text = variant.id;
                $scope.applyChanges();
            };
        }
    };
});