var m = angular.module('common');

m.directive('dnd', function($parse, $timeout) {
    var dragData;
    return {
        restrict: 'A',
        replace: false,
        scope: false,
        link: function($scope, element, attrs) {
            var dnd = $parse(attrs.dnd)($scope);
            if(_(dnd).isUndefined()) {
                return;
            }
            var options = $.extend(true, {
                data: undefined, //This is unique data that identify this object
                draggable: {
                    revert: "invalid",
                    delay: 300,
                    zIndex: 100,
                    opacity: 0.9, 
                    helper: function(){
                        var clone = $(this).clone();
                        clone.width(element[0].clientWidth);
                        clone.height(element[0].clientHeight);
                        $(element).draggable("option", "cursorAt", {
                            left: Math.floor(element[0].clientWidth / 2),
                            top: Math.floor(element[0].clientHeight / 2)
                        }); 
                        return clone;
                    },
                    cursor: "move",
                    start: function(event, ui) {
                        dragData = options.data;
                    },
                    stop: function() {
                        dragData = undefined;
                    }
                },
                droppable: {
                    accept: undefined,
                    hoverClass: 'drag-hover',
                    activeClass: 'drag-active',
                    drop: function(event, ui) {
                        var data = dragData;
                        dragData = undefined;
                        $timeout(function() {
                            options.droppable.onDrop(data, options.data);
                        });
                    },
                    onDrop: _.noop //This function should be overrided to process drop
                }
            }, dnd);
            
            if(options.draggable) {
                $(element).draggable(options.draggable);
            }
            if(options.droppable) {
                $(element).droppable(options.droppable);
            }
        }
    };
});