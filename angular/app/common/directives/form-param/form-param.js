var m = angular.module('common');

m.directive('formParam', function(path, modals, $timeout) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('common').directive('form-param').template(),
        scope: {
            param: '=formParam',
            model: '=ngModel',
            properties: '&?'
        },
        link: function($scope, element, attrs) {
            $scope.properties = $scope.properties || _.noop;

            $scope.internal = {
                model: $scope.model
            };
            $scope.resolvedProperties = $scope.properties({propertyType: $scope.param.propertyType});
            $scope.subButtonModel = {
                menus: $scope.resolvedProperties,
                'class': 'fa fa-tag'
            };
            if ($scope.param.propertiesHideFilter || $scope.param.propertiesHideAdvancedFilter) {
                $scope.subButtonModel.menuOptions = {
                    hideFilter: $scope.param.propertiesHideFilter,
                    hideAdvancedFilter: $scope.param.propertiesHideAdvancedFilter
                };
            }

            $scope.emailOptions = {
                script_url : '/assets/javascript/tinymce/tinymce.min.js',
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code",
                    "insertdatetime media table contextmenu paste textcolor"
                ],
                menubar: "format",
                toolbar: "undo redo | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image tags",
                statusbar: false,
                height: $scope.param.editorHeight // looks like if control is hidden when tinymce attaches it can't correctly calculate height. this is a hack for it
            };

            $scope.addProperty = function(menuIds, withTag) {
                $scope.internal.model = $scope.internal.model || '';
                if(withTag) {
                    $scope.internal.model += '<p>';
                }
                var fullId = _(menuIds).chain().keys().first().value();
                $scope.internal.model += '{' + fullId + '}';
                if(withTag) {
                    $scope.internal.model += '</p>';
                }
            };

            $scope.openTreeModal = function() {
                var popup = $scope.param.popup || {};
                modals.tree({
                    title: popup.title
                }, $scope.param.options, popup.size).then(function(selected) {
                    $scope.internal.model = selected;
                });
            };

            function fileUploadChanged (e) {
                if (e.target.files && e.target.files.length) {
                    $timeout(function () {
                        $scope.model = e.target.files[0];
                    });
                }
            }

            $scope.paramDisabled = function (param) {
                if (param.confirmPending) {
                    return true;
                }
                if (param.readOnly) {
                    return true;
                }
                return false;
            };

            if ($scope.param.type === 'file') {
                $timeout(function () {
                    element.find('input[type="file"]').on('change', fileUploadChanged);
                });
            }

            $scope.$watch('internal.model', function(m) {
                $scope.model = $scope.internal.model;
            }, true);

            $scope.$watch('model', function(m) {
                $scope.internal.model = $scope.model;
            });

        }
    };
});