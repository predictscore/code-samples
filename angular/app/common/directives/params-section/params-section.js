var m = angular.module('common');

m.directive('paramsSection', function (path) {
    return {
        restrict: 'A',
        scope: {
            section: "=paramsSection"
        },
        transclude: true,
        templateUrl: path('common').directive('params-section').template(),
        link: function($scope, element, attrs) {

            $scope.opened = true;
            $scope.openToggle = function (time) {
                if (_(time).isUndefined()) {
                    time = "slow";
                }
                $scope.opened = !$scope.opened;
                var body = element.find(".params-section-body");
                body.addClass('minimizing');
                body.slideToggle(time, function() {
                    body.removeClass('minimizing');
                });
            };
            if (!$scope.section.opened) {
                $scope.openToggle(0);
            }
        }
    };
});