var m = angular.module('common');

m.directive('integerRestriction', function($timeout, $parse) {
    return {
        restrict: 'A',
        replace: false,
        scope: false,
        link: function($scope, element, attrs) {
            element.bind('keypress', function(e) {
                var key;
                var keychar;

                if (window.event) {
                    key = window.event.keyCode;
                } else if (e) {
                    key = e.which;
                } else {
                    return true;
                }
                keychar = String.fromCharCode(key);

                if (_([null, 0, 8, 9, 13, 27]).contains(key) || ("0123456789").indexOf(keychar) > -1) {
                    return true;
                }
                return false;
            });

            element.bind('blur', function() {
                $timeout(function() {
                    var model = $parse(attrs.ngModel);
                    var onChange = attrs.ngChange && $parse(attrs.ngChange) || _.noop;
                    var minValue = parseInt(attrs.min);
                    var maxValue = parseInt(attrs.max);
                    var modelValue = parseInt(model($scope));

                    if (attrs.type === 'number' && !isFinite(minValue) && !isFinite(maxValue)) {
                        if (_(model($scope)).isNull() || model($scope) === '') {
                            return;
                        }
                    }

                    if(model($scope) === '' || !isFinite(modelValue) || isFinite(minValue) && modelValue < minValue) {
                        model.assign($scope, (attrs.type === 'number' ? 0 : '') + (isFinite(minValue) ? minValue : 0));
                        onChange($scope);
                    }
                    if (isFinite(maxValue) && modelValue > maxValue) {
                        model.assign($scope, (attrs.type === 'number' ? 0 : '') + maxValue);
                        onChange($scope);
                    }
                });
            });
        }
    };
});