var m = angular.module('common');

m.directive('ngTranscludeReplace', ['$log', function ($log) {
    return {
        restrict: 'EA',

        link: function ($scope, $element, $attrs, ctrl, $transclude) {
            if ($attrs.ngTransclude === $attrs.$attr.ngTransclude) {
              // If the attribute is of the form: `ng-transclude="ng-transclude"`
              // then treat it like the default
              $attrs.ngTransclude = '';
            }
        
            function ngTranscludeCloneAttachFn(clone) {
                if (clone.length) {
                    $element.replaceWith(clone);
                }
                else {
                    $element.remove();
                }
            }
        
            if (!$transclude) {
              $log.error('orphan',
               'Illegal use of ngTranscludeReplace directive in the template! ' +
               'No parent directive that requires a transclusion found. ');
            }
        
            // If there is no slot name defined or the slot name is not optional
            // then transclude the slot
            var slotName = $attrs.ngTranscludeReplace || $attrs.ngTranscludeSlot;
            $transclude($scope, ngTranscludeCloneAttachFn, null, slotName);
        }
    };
}]);