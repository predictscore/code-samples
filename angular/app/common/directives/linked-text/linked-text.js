var m = angular.module('common');

m.directive('linkedText', function($location, path, routeHelper, linkedText, recursionHelper, $parse, fnName, authorizationLinkHelper) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('common').directive('linked-text').template(),
        scope: false,
        compile: function(element) {
            return recursionHelper.compile(element, this.link);
        },
        link: function($scope, element, attrs) {
            $scope.routeHelper = routeHelper;
            $scope.textParts = [];
            var cached = null;
            var maxCount = 10;
            var count = maxCount;
            function resetCount() {
                count = maxCount;
                cached = null;
            }
            $scope.$watch(function() {
                var parsed = $parse(attrs.linkedText);
                if(fnName(parsed) != '$parseFunctionCall') {
                    return parsed($scope);
                }
                if(count === 0) {
                    resetCount();
                }
                if(cached !== null) {
                    count--;
                    return cached;
                }
                cached = parsed($scope);
                return cached;
            }, function(text) {
                if(_(text).isNull() || _(text).isUndefined()) {
                    text = '';
                }
                if(_(text.then).isFunction()) {
                    if($scope.textParts.length === 0) {
                        $scope.textParts = linkedText.toParts('Loading...');
                    }
                    text.then(function(resolvedText) {
                        $scope.textParts = linkedText.toParts(resolvedText);
                        resetCount();
                    });
                } else {
                    var parts = linkedText.toParts(text);
                    if(parts.length == $scope.textParts.length) {
                        $.extend(true, $scope.textParts, linkedText.toParts(text));
                    } else {
                        $scope.textParts = parts;
                    }
                }
            }, true);

            $scope.onClick = function(event, part) {
                if(!_(part.data.clickEvent).isUndefined()) {
                    event.preventDefault();
                    $scope.$emit(part.data.clickEvent.name, part.data.clickEvent);
                }
            };

            $scope.externalLinkClick = function (event, part) {
                if (part.data.authorization) {
                    authorizationLinkHelper.onClick(event, part.data);
                }
            };
        }
    };
});