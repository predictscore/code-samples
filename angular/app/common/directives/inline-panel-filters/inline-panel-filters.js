var m = angular.module('common');

m.directive('inlinePanelFilters', function(path, widgetSettings) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('common').directive('inline-panel-filters').template(),
        scope: {
            model: '=inlinePanelFilters',
            onChange: '&?',
            uuid: '=?'
        },
        link: function ($scope, element, attrs) {
            _($scope.model).each(function (filter, idx) {
                if (_(filter.value).isUndefined() && filter.id) {
                    var savedState;
                    if ($scope.uuid) {
                        savedState = widgetSettings.param($scope.uuid, filter.id);
                    }
                    if (_(savedState).isUndefined()) {
                        filter.value = filter.default;
                    } else {
                        filter.value = savedState;
                    }
                }
                if ($scope.uuid && filter.id) {
                    $scope.$watch('model[' + idx + '].value', function (newValue, oldValue) {
                        widgetSettings.param($scope.uuid, filter.id, newValue);
                    });
                }
            });
            $scope.getSelectLabel = function (filter) {
                var found = _(filter.variants).find(function (variant) {
                    return variant.id === filter.value;
                });
                return found && found.label;
            };
            $scope.getSelectLabel = function (filter) {
                var found = _(filter.variants).find(function (variant) {
                    return variant.id === filter.value;
                });
                return found && found.label;
            };
            $scope.setSelect = function (event, filter, variant) {
                event.preventDefault();
                filter.value = variant.id;
                $(element).find('.inline-select-value').dropdown('toggle');
            };
        }
    };
});