var m = angular.module('common');

m.directive('squareList', function(path) {
    return {
        restrict: 'A',
        replace: true,
        transclude: {
            'square': '?square',
            'listLabel': '?listLabel'
        },
        templateUrl: path('common').directive('square-list').template(),
        scope: {
            items: '=squareList',
            options: '=?'
        },
        link: function($scope, element, attrs) {
            $scope.options = $.extend(true, {
                squareListClass: 'default-square-list',
                label: null
            }, $scope.options);
            $scope.hideable = function () {
                return !_($scope.options.hideIfMore).isUndefined() && $scope.items.length > $scope.options.hideIfMore;
            };
            $scope.itemsHidden = false;
            $scope.toggleVisibility = function () {
                $scope.itemsHidden = !$scope.itemsHidden;
            };
            $scope.showHideLabel = function () {
                return $scope.itemsHidden ? ($scope.options.showLinkLabel || 'Show') : ($scope.options.hideLinkLabel || 'Hide');
            };

            $scope.getLabelClass = function () {
                var labelClass =  $.extend({}, $scope.options.labelClass);
                labelClass['label-hidden'] = $scope.options.hideLabel && $scope.itemsHidden;
                return labelClass;
            };
        }
    };
});