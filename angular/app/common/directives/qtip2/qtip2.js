var m = angular.module('common');

m.directive('qtip2', function($parse) {

    function _dotNotationRecursion(obj, prefix, res) {
        for (var i in obj) {
            if (i === 'position') { //looks like there's a bug updating position property of qtip
                continue;
            }
            if (_(obj[i]).isObject()) {
                _dotNotationRecursion(obj[i], prefix + i + '.', res);
            } else {
                res[prefix + i] = obj[i];
            }
        }
        return res;
    }

    function toDotNotation(obj) {
        return _dotNotationRecursion(obj, '', {});
    }

    return {
        restrict: 'A',
        replace: false,
        scope: false,
        link: function($scope, element, attrs) {
            var api = null;
            function destroy() {
                if(api) {
                    api.destroy();
                    //element.qtip('destroy', true);
                    api = null;
                }
            }
            $scope.$watch(function() {
                return $parse(attrs.qtip2)($scope);
            }, function(params) {
                if (!params) {
                    destroy();
                    return;
                }
                if (_(params).isString()) {
                    params = {
                        content: {
                            text: params
                        },
                        position: {
                            my: 'bottom right',
                            at: 'top center'
                        }
                    };
                }
                if(api) {
                    var dotNotation = toDotNotation(params);
                    api.set(dotNotation);
                } else {
                    var tooltip = element.qtip(params);
                    api = tooltip.qtip('api');
                }
            }, true);

            $scope.$on('$destroy', destroy);
        }
    };
});