var m = angular.module('common');

m.directive('colorPicker', function($timeout) {
    return {
        restrict: 'A',
        replace: false,
        scope: {
            options: '=colorPicker',
            model:'=ngModel'
        },
        link: function($scope, element, attrs) {
            var options = $scope.options || {};
            _(options).defaults({
                preferredFormat: "hex",
                clickoutFiresChange: true
            });
            options.color = $scope.model;
            var api = element.spectrum(options);

            $scope.$on('$destroy', function () {
                element.spectrum('destroy');
            });
        }
    };
});