var m = angular.module('common');

m.directive('relativeStyle', function(markedDom, $parse) {
    return {
        restrict: 'A',
        replace: false,
        scope: false,
        link: function($scope, element, attrs) {
            $scope.$watch(function() {
                return $parse(attrs.relativeStyle)($scope);
            }, function(relativeStyle) {
                _(relativeStyle).each(function(value, key) {

                });
            });
        }
    };
});