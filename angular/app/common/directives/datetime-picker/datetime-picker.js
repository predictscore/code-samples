var m = angular.module('common');

m.directive('datetimePicker', function(path, $timeout) {
    var timeFormat = 'YYYY-MM-DD HH:mm:ss';
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('common').directive('datetime-picker').template(),
        scope: {
            model: '=datetimePicker',
            options: '=?'
        },
        link: function($scope, element, attrs) {
            var pickerInput = $(element).find('.picker-input');
            var pickerWidget = $(element).find('.picker-widget');
            var defaultOptions = {
                altField: pickerInput,
                altFieldTimeOnly: false,
                showButtonPanel: false,
                dateFormat: 'yy-mm-dd',
                timeFormat: 'HH:mm:ss',
                timeInput: true,
                unixtime: false
            };
            var internalFormat = timeFormat;
            if ($scope.options && $scope.options.dateOnly) {
                //this option is not supported by control, so just emulating needed behaviour with supported options
                //to support it, it's also required changing of 'internalFormat', that's why special option introduced
                //Notice: right now all code makes displaying date in UTC
                defaultOptions.showTimepicker = false;
                defaultOptions.timeFormat = '';
                internalFormat = 'YYYY-MM-DD';
            }
            var options = $.extend(defaultOptions, $scope.options);
            var inputField = $(element).find('input');
            
            pickerWidget.datetimepicker(options);
            
            function resetInternalModel() {
                var date = moment().milliseconds(0).seconds(0).minutes(0).hours(0);
                if($scope.model && moment($scope.model, timeFormat, true).isValid()) {
                    date = moment($scope.model);
                }
                if (options.dateOnly) {
                    date = date.utc();
                }
                var newInternalValue = date.format(internalFormat);
                if (newInternalValue != $scope.internalModel) {
                    $scope.internalModel = newInternalValue;
                    pickerWidget.datetimepicker('setDate', $scope.internalModel);
                }
            }
            // resetInternalModel();
            
            $scope.$watch('model', resetInternalModel);
            
            $scope.$watch(function() {
                var time = moment(pickerWidget.datetimepicker('getDate'));
                if (options.dateOnly) {
                    time = moment.utc(time.format(internalFormat)).local();
                }
                // TODO: looks like unixtime is supported only here. I think resetInternalModel and onInputBlur should be changed a bit
                if(options.unixtime) {
                    return time.valueOf();
                }
                return time.format(timeFormat);
            }, function(time) {
                $scope.model = time;
            });

            function processInternalModel (ignoreErrors) {
                if(moment($scope.internalModel, internalFormat, true).isValid()) {
                    var time;
                    if (options.dateOnly) {
                        time = moment.utc($scope.internalModel, internalFormat, true).local();
                    } else {
                        time = moment($scope.internalModel, internalFormat, true);
                    }
                    $scope.model = time.format(timeFormat);
                } else {
                    if (!ignoreErrors) {
                        resetInternalModel();
                    }
                }
            }
            // this additional watch is needed to update model when user manually input data before 'ok' in the dialog is pressed
            $scope.$watch('internalModel', function () {
                processInternalModel(true);
            });

            function onInputBlur() {
                $timeout(processInternalModel);
            }
            
            inputField.on('blur', onInputBlur);
            
            $scope.backgroundClick = function(e) {
                e.preventDefault();
                e.stopPropagation();
                $scope.widgetOpen = false;
            };
            
            $scope.togglePicker = function() {
                inputField.blur();
                $scope.widgetOpen = !$scope.widgetOpen;
            };
            
            $scope.$on('$destroy', function() {
                inputField.off('blur', onInputBlur);
            });
        }
    };
});