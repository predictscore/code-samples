var m = angular.module('wizard');

m.controller('TermsModalController', function($scope, $modalInstance, args) {
    $scope.cancel = function() {
        $modalInstance.dismiss();
    };
    $scope.ok = function() {
        $modalInstance.close();
    };

    $scope.termsUrl = args.termsUrl;
});

m.factory('termsModal', function($modal, path) {
    return {
        show: function(args) {
            var modalConfig = {
                templateUrl: path('wizard').ctrl('terms-modal').template(),
                controller: 'TermsModalController',
                resolve: {
                    args: _.constant(args)
                }
            };
            var modalInstance = $modal.open(modalConfig);
            return modalInstance.result;
        }
    };
});
