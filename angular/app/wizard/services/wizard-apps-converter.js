var m = angular.module('wizard');

m.factory('wizardAppsConverter', function($q, $parse, modulePropsToOptions) {
    return function(input, args) {

        function getCategories(categories, params) {
            return _(categories).filter(function (category) {
                return _(params).reduce(function (memo, value, key) {
                    return memo && category[key] === value;
                }, true);
            });
        }

        var enabledMap = _(input.data.apps_categories.categories).map(function (category) {
            category.haveActiveItems = !_(input.data.apps_items.modules).chain().filter(function (module) {
                return module.category_id === category.categoryId;
            }).find(function (module) {
                return module.state === 'active';
            }).isUndefined().value();
            return category;
        });



        var categories = getCategories(input.data.apps_categories.categories, args.categoryFilter);

        var categoryMap = {};
        var categoryIds = _(categories).map(function (category) {
            categoryMap[category.categoryId] = category;
            return category.categoryId;
        });

        var stateWeights = {
            active: 0,
            nonactive: 1,
            display_only: 2,
            require_license: 3
        };
        var opswatPackage =  $parse('opswatLicense.data.packages.' + args.wizard_opswat_package + '.content')(args);

        var apps = _(input.data.apps_items.modules).chain().filter(function (module) {
            return !module.gui_hidden && _(categoryIds).indexOf(module.category_id) !== -1;
        }).each(function (module) {
            if (!module.options) {
                return true;
            }
            if (module.options.license !== 'opswat_demo_paid') {
                return true;
            }
            if (module.state === 'nonactive' && (!opswatPackage || !opswatPackage[module.name])) {
                module.state = 'require_license';
            }
        }).sortBy(function (module) {
            return parseInt(module.app_store_order);
        }).sortBy(function (module) {
            return (module.category_id && categoryMap[module.category_id]) ? parseInt(categoryMap[module.category_id].appStoreOrder)  : 2000000000;
        }).sortBy(function (module) {
            if (module.state !== 'nonactive') {
                return 1;
            }
            if (module.options && module.options.license === 'contact_support') {
                return 3;
            }
            if (!module.license_info || module.license_info.license_type === 'none') {
                return 2;
            }
            return 1;
        }).sortBy(function (module) {
            return stateWeights[module.state];
        }).value();

        var data = {
            "widgets": _(apps).map(function(module) {
                return {
                    "type": 'wizard-item',
                    "size": 'wizard-app-col',
                    "wrapper": {
                        "type": "none"
                    },
                    "options": modulePropsToOptions(module, {
                        "category": categoryMap[module.category_id]
                    })
                };
            }),
            categories: enabledMap
        };

        return $q.when($.extend({}, input, {
            data: data
        }));
    };
});