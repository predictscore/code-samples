var m = angular.module('wizard');

m.factory('wizardHelper', function ($q, $timeout, $injector, authorizationLinkHelper, termsModal) {
    var widgets = [];
    var reloadFn = $q.when();
    var updateTick = null;
    var updateTimer = null;
    var modulesManager;
    var modulesDao;
    var appConfigurator;
    var wizardScope;
    var beingActivated = {};
    var state;
    var initialized = false;

    setState(null, 'disabled');

    function getAppWidget(appName) {
        return _(widgets).find(function (widget) {
            return widget.options.moduleName === appName;
        });
    }

    function activateApp(appName, checkOnly) {
        modulesDao = modulesDao || $injector.get('modulesDao');
        modulesManager = modulesManager || $injector.get('modulesManager');
        appConfigurator = appConfigurator || $injector.get('appConfigurator');

        var widget = getAppWidget(appName);
        if (widget.options.moduleStatus !== 'nonactive') {
            return false;
        }

        if (widget.options.category.family === 'saas') {
            if (!widget.options.saasAuthorized) {
                if (appConfigurator.showConfigurationLink(widget.options)) {
                    if (checkOnly) {
                        return true;
                    }
                    authorizationLinkHelper.openLink(widget.options);
                }
                return false;
            }
        }

        if (!widget.options.options) {
            return false;
        }

        if (widget.options.options.operation === 'start') {
            if (checkOnly) {
                return true;
            }
            beingActivated[appName] = true;
            modulesDao.operation(appName, 'start', {
                start_matrix_policy: true
            }).then(function () {
                if (widget.options.category.family === 'saas') {
                    modulesManager.reloadModules();
                }
                doReload({rnd: Math.random()}).then(function () {
                    beingActivated[appName] = false;
                });
            });
            return false;
        }

        if (widget.options.options.license === 'paid_demo' || widget.options.options.license === 'opswat_demo_paid') {
            if (!widget.options.license_info) {
                if (checkOnly) {
                    return true;
                }
                beingActivated[appName] = true;
                modulesDao.operation(appName, 'start', {
                    start_demo_license: true,
                    start_matrix_policy: true
                }).then(function () {
                    doReload({rnd: Math.random()}).then(function () {
                        beingActivated[appName] = false;
                    });
                });
            }
        }
        return false;
    }

    function setReloadTimer () {
        var slowdownHelper = $injector.get('slowdownHelper');
        var adjustedUpdateTick = slowdownHelper.adjustInterval(updateTick);
        if (adjustedUpdateTick) {
            updateTimer = $timeout(function () {
                wizardHelper.reload();
            }, adjustedUpdateTick);
        }
    }

    function stopTimer() {
        if (updateTimer) {
            $timeout.cancel(updateTimer);
            updateTimer = false;
        }
    }

    function setState(step, status) {
        var newState = {
            savedStep: step,
            status: status
        };
        newState.inProgress = (newState.status === 'enabled' && !!wizardHelper.steps[newState.savedStep] && !wizardHelper.steps[newState.savedStep].isLast);
        state = newState;
    }

    function doReload (param) {
        if (!_(reloadFn).isFunction()) {
            return;
        }
        return reloadFn(param).then(function () {
            setReloadTimer();
        }, function () {
            setReloadTimer();
        });
    }

    var wizardHelper = {
        scope: function (newScope) {
            if (!_(newScope).isUndefined()) {
                wizardScope = newScope;
            }
            return wizardScope;
        },
        reset: function () {
            stopTimer();
            reloadFn = $q.when();
            widgets = [];
        },
        reload: function (newReloadFn, newUpdateTick) {
            if (_(newReloadFn).isFunction()) {
                reloadFn = newReloadFn;
                updateTick = newUpdateTick;
                widgets = [];
            }
            stopTimer();
            return doReload();
        },
        setWidgets: function (data) {
            widgets = data;
        },
        getWidgets: function () {
            return widgets;
        },
        activateApp: function () {
            var fnThis = this;
            var fnArgs = arguments;
            if (wizardScope.stepInfo.terms && !wizardScope.terms.accepted) {
                return termsModal.show({
                    termsUrl: wizardScope.stepInfo.termsUrl
                }).then(function () {
                    wizardScope.acceptTerms();
                    return activateApp.apply(fnThis, fnArgs);
                });
            }
            return activateApp.apply(fnThis, fnArgs);
        },
        showActivateButton: function (appName) {
            return activateApp(appName, true);
        },
        operationInProgress: function (appName) {
            return beingActivated[appName];
        },
        steps: [{
            type: 'info',
            title: 'Welcome to Avanan',
            message: 'Please follow this short wizard to activate Avanan’s Cloud-Security platform. In this short wizard you will be asked to choose the SAAS service you wish the secure as well as the best-of-breed security-stack you wish to use for securing it.  These options can be changed later from our app-store. Ready? Let’s get started!',
            nextButton: 'Let\'s Get Started',
            titleAddUser: true
        }, {
            type: 'select-app',
            title: 'SAAS Selection',
            titleNavAllowed: 'Selecting additional SAAS (optional)',
            message: 'In this screen you need to select the cloud-service you wish to secure. You will be required to authorize access to Avanan using a SAML admin-level authentication to your SAAS. To choose – click on “Activate” next to to the SAAS icon, and follow the authentication instructions. Please note – without proper admin-level authentication Avanan will not be able to secure the service. You can choose multiple SAAS products to have Avanan secure all of them and you can change this selection later on.',
            categoryFilter: {
                family: 'saas'
            },
            updateTick: 5000,
            modulesRecheck: true,
            icon: 'circle-saas.png',
            timelineLong: 'SaaS Services',
            timelineShort: 'SaaS',
            terms: true,
            termsUrl: "http://www.avanan.com/avanan-terms-of-service"
        }, {
            title: 'Security Categories',
            message: 'Activate the security categories of your choice to protect your data.',
            type: 'category-selector',
            icon: 'circle-av.png',
            timelineLong: 'Security Categories',
            timelineShort: 'Security',
            categories: [{
                type: "malicious",
                title: "Activate Malware Protection",
                label: "Malware Protection",
                img: "circle-av-transp.png",
                selected: true
            }, {
                type: "dlp",
                title: "Activate Data-Leakage Prevention",
                label: "Data-Leakage Prevention",
                img: "circle-dlp-transp.png"
            }]
        }, {
            title: 'You’re all set',
            message: 'In a few short minutes we will be setting up the service, connecting to your SAAS and securing it with the security-stack you have created. You can always add more cloud services and change the security-stack from the app-store in the UI.  For any questions, please contact support@avanan.com.',
            type: 'info',
            linkText: 'Start Now',
            linkGoTo: 'dashboard',
            linkGoToParams: {
                'module': 'super-dashboard'
            },
            isLast: true,
            disableWizard: true
        }],
        getState: function () {
            return $.extend({}, state);
        },
        reloadState: function () {
            var wizardDao = $injector.get('wizardDao');
            return $q.all([
                wizardDao.getStep(),
                wizardDao.getStatus()
            ]).then(function(responses) {
                setState(responses[0], responses[1]);
            }, function (error) { // code that will work in case needed API is missing on server
                setState(null, 'disabled');
            });
        },
        init: function () {
            if (initialized) {
                return $q.when();
            }
            initialized = true;
            return wizardHelper.reloadState();
        },
        setStatus: function (status) {
            var wizardDao = $injector.get('wizardDao');
            setState(state.savedStep, status);
            return wizardDao.setStatus(status);
        },
        setStep: function (step) {
            var wizardDao = $injector.get('wizardDao');
            setState(step, state.status);
            return wizardDao.setStep(step);
        }
    };

    return wizardHelper;
});
