var m = angular.module('alerts');

m.factory('alertsHelper', function() {
    var scope = {};
    var periods =  [{
        label: '24 hours',
        period: '24 hours',
        defaultGraphGrouping: 'hour'
    }, {
        label: '3 days',
        period: '3 days',
        defaultGraphGrouping: 'hour'
    }, {
        label: 'Week',
        period: '1 weeks',
        defaultGraphGrouping: 'day'
    }, {
        label: 'Month',
        period: '1 months',
        defaultGraphGrouping: 'day'
    }, {
        label: 'All',
        period: '',
        defaultGraphGrouping: 'day'
    }];

    var graphModes =  [{
        id: 'pie',
        'class': 'fa fa-pie-chart'
    }, {
        id: 'history',
        'class': 'fa fa-area-chart'
    }];

    var graphMode = graphModes[0];


    return {
        scope: function (s) {
            if (!_(s).isUndefined()) {
                scope = s;
            }
            return scope;
        },
        periods: function () {
            return periods;
        },
        chartsUpdateTick: function () {
            return 60000;
        },
        graphModes: graphModes,
        graphMode: function (m) {
            if (!_(m).isUndefined() && _(graphModes).contains(m)) {
                graphMode = m;
            }
            return graphMode;
        },
        getParams: function () {
            var params = {};
            _(scope.model.filters).each(function (filter) {
                params[filter.type] = params[filter.type] || [];
                params[filter.type].push(filter.id);
            });
            _(params).each(function (value, key) {
                params[key] = value.join(',');
            });
            return params;
        }

    };
});