var m = angular.module('alerts');

m.factory('alertsHistoryGraphLogic', function(alertsDao, alertsHelper, policySeverityTypes) {
    return function ($scope, logic) {
        var alertsScope = alertsHelper.scope();
        logic.updateTick = alertsHelper.chartsUpdateTick();


        function getLimits(period, grouping) {
            if (!period) {
                period = '3 months';
            }
            var periodArr = period.split(' ');
            var endTime = moment().startOf(grouping);
            var startTime = moment(endTime).subtract(parseInt(periodArr[0]), periodArr[1]);
            return {
                start: startTime,
                end: endTime
            };
        }

        function makeTimesArray(period, grouping) {
            var limits = getLimits(period, grouping);
            var t = moment(limits.start);
            var times = [];
            while (t <= limits.end) {
                times.push(t.valueOf());
                t.add(1, grouping + 's');
            }
            return times;
        }

        function axisFormat(period, grouping) {
            if (grouping === 'day') {
                return '%b %d';
            }
            return '%b %d %H:%M';
        }

        function axisMinTickSize(period, grouping) {
            if (grouping === 'day') {
                return [1, 'day'];
            }
            return [1, 'hour'];
        }


        function loadData () {
            var period = _(alertsHelper.periods()).find(function (p) {
                return p.period === alertsScope.model.period;
            });
            if (!period) {
                period = {
                    period: '',
                    defaultGraphGrouping: 'day'
                };
            }
            return alertsDao.severityGraph()
                .params(alertsHelper.getParams())
                .params({
                    after_period: alertsScope.model.period,
                    tz_offset: moment().utcOffset(),
                    trunc: period.defaultGraphGrouping
                })
                .then(function (response) {
                    $scope.model.flotOptions.xaxis.timeformat = axisFormat(period.period, period.defaultGraphGrouping);
                    $scope.model.flotOptions.xaxis.minTickSize = axisMinTickSize(period.period, period.defaultGraphGrouping);
                    var times = makeTimesArray(period.period, period.defaultGraphGrouping);
                    var dataMap = {};
                    _(response.data.rows).each(function (row) {
                        var key = moment(row.time_point).valueOf();
                        dataMap[key] = dataMap[key] || {};
                        dataMap[key][policySeverityTypes.idFixed(row.severity)] = row.count;
                    });
                    var params = alertsHelper.getParams();
                    var severities = angular.copy(policySeverityTypes.asVariants()).reverse();
                    if (params && params.severity) {
                        var severityParamsArray = params.severity.split(',');
                        severities = _(severities).filter(function (severity) {
                            return _(severityParamsArray).contains(severity.id);
                        });
                    }
                    $scope.model.data = _(severities).map(function (severity) {
                        return {
                            data: _(times).map(function (time) {
                                return [time, (dataMap[time] || {})[severity.id] || 0];
                            }),
                            color: severity.dashboardColor,
                            label: severity.label
                        };
                    });
                });
        }

        var periodWatch = $scope.$watch(function () {
            return alertsHelper.scope().model.period;
        }, function (newValue) {
            loadData();
        });

        var filtersWatch =$scope.$watch(function () {
            return alertsHelper.scope().model.filters;
        }, function (newVal, oldVal) {
            if (_(newVal).isEqual(oldVal)) {
                return;
            }
            loadData();
        }, true);

        $scope.$on('alert-states-data-saved', function () {
            if (alertsHelper.getParams().state) {
                loadData();
            }
        });

        $scope.$on('alert-severities-data-saved', function () {
            loadData();
        });


        return function() {
            periodWatch();
            filtersWatch();
        };
    };
});
