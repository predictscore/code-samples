var m = angular.module('alerts');

m.factory('alertsAppPieLogic', function(alertsDao, alertsHelper) {
    return function ($scope, logic) {
        var alertsScope = alertsHelper.scope();
        logic.updateTick = alertsHelper.chartsUpdateTick();

        function loadData () {
            return alertsDao.appCounts()
                .params({
                    after_period: alertsScope.model.period
                })
                .then(function (response) {
                    $scope.model.data = _(response.data.rows).map(function (row) {
                        return {
                            data: row.count,
                            label: row.app_label + ': ' + row.count
                        };
                    });
                });
        }

        var paramsWatch = $scope.$watch(function () {
            return alertsHelper.scope().model.period;
        }, function (newValue) {
            loadData();
        });

        return function() {
            paramsWatch();
        };
    };
});
