var m = angular.module('alerts');

m.factory('alertsSeverityPieLogic', function(alertsDao, alertsHelper, policySeverityTypes) {
    return function ($scope, logic) {
        var alertsScope = alertsHelper.scope();
        $scope.model.logic.updateTick = alertsHelper.chartsUpdateTick();


        function updateGraph() {
            var counts = _(policySeverityTypes.asVariants())
                .map(function (row) {
                    return {
                        count: $scope.cachedData[row.id] || 0,
                        color: row.dashboardColor,
                        id: row.id,
                        label: row.label
                    };
                }).reverse();
            $scope.model.data = _(counts).map(function (el) {
                return {
                    data: el.count,
                    color: el.color,
                    label: el.label + ': ' + el.count
                };
            });
        }

        function loadData () {
            return alertsDao.severityCounts()
                .params({
                    after_period: alertsScope.model.period
                })
                .then(function (response) {
                    $scope.cachedData = {};
                    _(response.data.rows).each(function (row) {
                        var id = policySeverityTypes.idFixed(row.severity);
                        $scope.cachedData[id] = row.count + ($scope.cachedData[id] || 0);
                    });
                    updateGraph();
                });
        }

        function instantChange(data) {
            _(data).each(function (val, key) {
                $scope.cachedData[key] = ($scope.cachedData[key] || 0) + val;
            });
            updateGraph();
        }

        $scope.$on('alert-severities-data-changed', function (event, data) {
            instantChange(data);
        });
        $scope.$on('alert-severities-data-saved', function () {
            loadData();
        });

        var paramsWatch = $scope.$watch(function () {
            return alertsHelper.scope().model.period;
        }, function (newValue) {
            loadData();
        });

        return function() {
            paramsWatch();
        };
    };
});
