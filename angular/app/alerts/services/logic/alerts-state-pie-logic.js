var m = angular.module('alerts');

m.factory('alertsStatePieLogic', function(alertsDao, alertsHelper, alertStates) {
    return function ($scope, logic, reinit) {
        var alertsScope = alertsHelper.scope();
        logic.updateTick = alertsHelper.chartsUpdateTick();

        function updateGraph() {
            $scope.model.data = _(alertStates.asVariants()).map(function (state) {
                var count = $scope.cachedData[state.id] || 0;
                return {
                    data: count,
                    color: state.color,
                    label: state.label + ': ' + count
                };
            });
        }

        function loadData () {
            return alertsDao.stateCounts()
                .params({
                    after_period: alertsScope.model.period
                })
                .then(function (response) {
                    $scope.cachedData = {};
                    _(response.data.rows).each(function (row) {
                        $scope.cachedData[row.state] = row.count + ($scope.cachedData[row.state] || 0);
                    });
                    updateGraph();
                });
        }

        function instantChange(data) {
            _(data).each(function (val, key) {
                $scope.cachedData[key] = ($scope.cachedData[key] || 0) + val;
            });
            updateGraph();
        }

        $scope.$on('alert-states-data-changed', function (event, data) {
            instantChange(data);
        });
        $scope.$on('alert-states-data-saved', function () {
            loadData();
        });


        var paramsWatch = $scope.$watch(function () {
            return alertsHelper.scope().model.period;
        }, function (newValue) {
            loadData();
        });

        return function() {
            paramsWatch();
        };
    };
});
