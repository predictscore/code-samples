var m = angular.module('alerts');

m.factory('alertStates', function() {
    var types = [{
        label: 'New',
        id: 'new',
        color: '#f35428'
    }, {
        label: 'In progress',
        id: 'in_progress',
        color: '#f39c12'
    }, {
        label: 'Resolved',
        id: 'resolved',
        color: '#16a085'
    }, {
        label: 'Dismissed',
        id: 'dismissed',
        color: '#9bbb59'
    }, {
        label: 'Exception',
        id: 'exception',
        color: '#2980b9'
    }];
    
    return {
        asVariants: function() {
            return types;
        },
        idToLabel: function(id) {
            id = id || 'none';
            return (_(types).find(function(t) {
                return t.id == id;
            }) || {}).label || id;
        }
    };
});