var m = angular.module('advanced-data-search');

m.directive('alertsGraphs', function(path, alertsDao, policySeverityTypes, alertStates, $timeout) {
    return {
        restrict: 'A',
        templateUrl: path('alerts').directive('alerts-graphs').template(),
        scope: false,
        link: function($scope, element, attrs) {

            $scope.graphRows = {
                "pie": {
                    "rows": [
                        [{
                            "type": "flot",
                            "logic": {
                                "name": "alertsSeverityPieLogic"
                            },
                            "size": "col-lg-4 col-md-4 col-sm-12 col-xs-12",
                            "style": {
                                "height": "251px"
                            },
                            "wrapper": {
                                "type": "widget-panel",
                                "class": "avanan white-body",
                                "title": "Alerts By Severity"
                            },
                            "flotOptions": {
                                "series": {
                                    "pie": {
                                        "show": true,
                                        "radius": 0.7
                                    }
                                },
                                "legend": {
                                    "show": true,
                                    "position": "ne"
                                },
                                "shadowSize": 0
                            }
                        }, {
                            "type": "flot",
                            "logic": {
                                "name": "alertsStatePieLogic"
                            },
                            "size": "col-lg-4 col-md-4 col-sm-12 col-xs-12",
                            "style": {
                                "height": "251px"
                            },
                            "wrapper": {
                                "type": "widget-panel",
                                "class": "avanan white-body",
                                "title": "Alerts By State"
                            },
                            "flotOptions": {
                                "series": {
                                    "pie": {
                                        "show": true,
                                        "radius": 0.7
                                    }
                                },
                                "legend": {
                                    "show": true,
                                    "position": "ne"
                                },
                                "shadowSize": 0
                            }
                        }, {
                            "type": "flot",
                            "logic": {
                                "name": "alertsAppPieLogic"
                            },
                            "size": "col-lg-4 col-md-4 col-sm-12 col-xs-12",
                            "style": {
                                "height": "251px"
                            },
                            "wrapper": {
                                "type": "widget-panel",
                                "class": "avanan white-body",
                                "title": "Alerts By Saas"
                            },
                            "flotOptions": {
                                "series": {
                                    "pie": {
                                        "show": true,
                                        "radius": 0.7
                                    }
                                },
                                "legend": {
                                    "show": true,
                                    "position": "ne"
                                },
                                "shadowSize": 0
                            }
                        }]

                    ]
                },
                "history": {
                    "rows": [
                        [{
                            "type": "flot",
                            "logic": {
                                "name": "alertsHistoryGraphLogic"
                            },
                            "size": "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                            "style": {
                                "height": "251px"
                            },
                            "wrapper": {
                                "type": "widget-panel",
                                "class": "avanan white-body",
                                "title": "Alerts History"
                            },
                            "flotOptions": {
                                "series": {
                                    "stack": true,
                                    "lines": {
                                        "show": true,
                                        "fill":true
                                    },
                                    "points": {
                                        "show": true
                                    }
                                },
                                "xaxis": {
                                    "mode": "time",
                                    "timezone": "browser"
                                },
                                "grid": {
                                    "hoverable": true,
                                    "clickable": false,
                                    "borderWidth": 1,
                                    "tickColor": "#eee",
                                    "borderColor": "#eee"
                                },
                                "tooltip": true,
                                "tooltipOpts": {
                                    "defaultTheme": false,
                                    "content": "%s: %x: %y.0",
                                    "shifts": {
                                        "x": 0,
                                        "y": 20
                                    },
                                    "lines": true
                                }

                            }
                        }]
                    ]
                }
            };
        }
    };
});