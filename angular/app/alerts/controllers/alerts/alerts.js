var m = angular.module('alerts');

m.controller('AlertsController', function($scope, $q, $timeout, tablePaginationHelper, alertsDao, objectTypeDao, path, tableExportHelper, policySeverityTypes, modulesManager, policyDao, base64, $location, alertStates, alertsHelper, sideMenuManager, avananUserDao, modals, workingIndicatorHelper) {

    alertsHelper.scope($scope);
    $scope.alertsHelper = alertsHelper;

    $scope.periods = alertsHelper.periods();

    $scope.model = {
        filters: [],
        period: ''
    };
    var savedFilters = $location.search().filters;
    if(_(savedFilters).isString()) {
        try {
            $scope.model.filters = JSON.parse(base64.decode(savedFilters));
        } catch (e) {

        }
    } else {
        addFilter('state', 'new', alertStates.idToLabel('new'));
        addFilter('state', 'in_progress', alertStates.idToLabel('in_progress'));
    }
    var savedPeriod = $location.search().period;
    if (_(savedPeriod).isString()) {
        try {
            savedPeriod = base64.decode(savedPeriod);
            $scope.model.period = _($scope.periods).chain().find(function (period) {
                return period.period === savedPeriod;
            }).isUndefined().value() ? '' : savedPeriod;
        } catch (e) {
        }
    }

    $scope.resultsPanel = {
        title: 'Loading...',
        'class': 'light white-body white-header'
    };

    function addFilter(type, id, label) {
        var found = _($scope.model.filters).find(function (filter) {
            return filter.type === type && filter.id === id;
        });
        if (_(found).isUndefined()) {
            $scope.model.filters.push({
                type: type,
                id: id,
                label: label
            });
        }
    }

    function removeAllFilter(type) {
        $scope.model.filters = _($scope.model.filters).filter(function (filter) {
            return filter.type !== type;
        });
    }

    $scope.removeFilterByIndex = function (idx) {
        $scope.model.filters.splice(idx, 1);
    };

    $scope.setPeriod = function (value) {
        $scope.model.period = value;
    };

    var policyFilterButton = {
        label: 'Query',
        position: 'left',
        'class': 'btn btn-blue',
        filter: true,
        filterPlaceholder: 'Filter...',
        options: [{
            execute: function () {
                removeAllFilter('policy');
            },
            label: 'All',
            noFilter: true
        }, {
            label: 'Loading list...',
            noFilter: true
        }]
    };
    var assigneeFilterButton = {
        label: 'Assignee',
        position: 'left',
        'class': 'btn btn-blue',
        options: [{
            execute: function () {
                removeAllFilter('assignee');
            },
            label: 'All'
        }, {
            label: 'Loading list...'
        }]
    };

    var bulkActionsButton = {
        label: 'Bulk Actions',
        position: 'right',
        'class': 'btn btn-blue',
        hidden: true,
        options: [{
                label: 'Update Status',
                execute: function () {
                    $scope.bulkUpdate('state');
                }
            }, {
                label: 'Update Assignee',
                execute: function () {
                    $scope.bulkUpdate('assignee');
                }
            }]
    };

    var filterTypes = [{
        "type": "app",
        "label": "SaaS",
        "updateParam": "saas"
    }, {
        "type": "severity",
        "label": "Severity",
        "updateParam": "severity"
    }, {
        "type": "state",
        "label": "Status",
        "updateParam": "state"
    }, {
        "type": "policy",
        "label": "Query",
        "updateParam": "policy_id",
        "multiline": true
    }, {
        "type": "assignee",
        "label": "Assignee",
        "updateParam": "assignee",
        "multiline": true
    }];


    $scope.filtersPanel = {
        title: 'Loading...',
        'class': 'light white-body white-header',
        buttons: [{
            label: 'SaaS',
            position: 'left',
            'class': 'btn btn-blue',
            options: [{
                execute: function () {
                    removeAllFilter('app');
                },
                label: 'All'
            }].concat(_(modulesManager.activeSaasApps()).map(function (val) {
                return {
                    execute: function () {
                        addFilter('app', val.name, val.title);
                    },
                    label: val.title
                };
            }))
        }, {
            label: 'Severity',
            position: 'left',
            'class': 'btn btn-blue',
            options: [{
                execute: function () {
                    removeAllFilter('severity');
                },
                label: 'All'
            }].concat(_(policySeverityTypes.asVariants()).map(function (val) {
                return {
                    execute: function () {
                        addFilter('severity', val.id, val.label);
                    },
                    label: val.label
                };
            }))
        }, policyFilterButton,
            {
            label: 'Status',
            position: 'left',
            'class': 'btn btn-blue',
            options: [{
                execute: function () {
                    removeAllFilter('state');
                },
                label: 'All'
            }].concat(_(alertStates.asVariants()).map(function (val) {
                return {
                    execute: function () {
                        addFilter('state', val.id, val.label);
                    },
                    label: val.label
                };
            }))
        }, assigneeFilterButton, bulkActionsButton, {
                label: 'Export...',
                position: 'right',
                'class': 'btn btn-blue',
                options: [
                    {
                        label: 'Export as pdf',
                        execute: function () {
                            tableExportHelper('pdf', 'alerts', $scope.tableHelper);
                        }
                    }, {
                        label: 'Export as csv',
                        execute: function () {
                            tableExportHelper('csv', 'alerts', $scope.tableHelper);
                        }
                    }, {
                        label: 'Export as xlsx',
                        execute: function () {
                            tableExportHelper('xlsx', 'alerts', $scope.tableHelper);
                        }
                    }]
            }]
    };

    alertsDao.policies().then(function(response) {
        policyFilterButton.options = [
            policyFilterButton.options[0]
        ].concat(_(response.data.rows).chain().sortBy('policy_name').map(function(row) {
            return {
                execute: function () {
                    addFilter('policy', row.policy_id, row.policy_name);
                },
                label: row.policy_name
            };
        }).value());
    });

    function updateAssigneesFilter () {
        alertsDao.assignees().then(function(response) {
            assigneeFilterButton.options = [
                assigneeFilterButton.options[0]
            ].concat(_(response.data.rows).chain().sortBy('assignee').map(function(row) {
                return {
                    execute: function () {
                        addFilter('assignee', row.assignee, row.assignee);
                    },
                    label: row.assignee
                };
            }).value());
        });
    }

    updateAssigneesFilter();


    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 20,
        pagination: {
            page: 1,
            ordering: {}
        },
        saveState: true,
        showWorkingIndicator: true,
        disableTop: true
    };

    function updateAlert(id, params) {
        return alertsDao.update(id, params);
    }

    function stateChangeAutoComment(comment, oldState, newState) {
        return '- ' + moment().format('M/D/YYYY HH:mm ZZ') + ': ' + sideMenuManager.currentUser().email +
                ' changed status from "' + alertStates.idToLabel(oldState) + '" to "' + alertStates.idToLabel(newState) + '"\n\n' +
            (comment || '');
    }

    function severityChangeAutoComment(comment, oldSeverity, newSeverity) {
        return '- ' + moment().format('M/D/YYYY HH:mm ZZ') + ': ' + sideMenuManager.currentUser().email +
            ' changed severity from "' + policySeverityTypes.idToLabel(oldSeverity) + '" to "' + policySeverityTypes.idToLabel(newSeverity) + '"\n\n' +
            (comment || '');
    }

    function assigneeChangeAutoComment(comment, oldAssignee, newAssignee) {
        if (!oldAssignee) {
            return '- ' + moment().format('M/D/YYYY HH:mm ZZ') + ': ' + sideMenuManager.currentUser().email +
                ' assigned alert to  "' + newAssignee + '"\n\n' +
                (comment || '');
        }
        if (!newAssignee) {
            return '- ' + moment().format('M/D/YYYY HH:mm ZZ') + ': ' + sideMenuManager.currentUser().email +
                ' removed assignee \n\n' +
                (comment || '');

        }
        return '- ' + moment().format('M/D/YYYY HH:mm ZZ') + ': ' + sideMenuManager.currentUser().email +
            ' changed assignee from "' + oldAssignee + '" to "' + newAssignee + '"\n\n' +
            (comment || '');
    }



    function getRowById (id) {
        return _($scope.tableModel.data).find(function (row) {
            return row[$scope.tableModel.idToColumn.id.idx].originalText === id;
        });
    }

    $scope.$on('alert-comment-changed', function (event, data) {
        return updateAlert(data.attributes.id, {
            comment: data.newValue
        });
    });

    $scope.$on('alert-state-changed', function (event, data) {
        var emitData = {};
        emitData[data.oldValue] = -1;
        emitData[data.newValue] = 1;
        $scope.$broadcast('alert-states-data-changed', emitData);
        return updateAlert(data.attributes.id, {
            state: data.newValue,
            comment: stateChangeAutoComment(getRowById(data.attributes.id)[$scope.tableModel.idToColumn.comment.idx].originalText, data.oldValue, data.newValue)
        }).then(function () {
            $scope.tableHelper.reload(true, true);
            $scope.$broadcast('alert-states-data-saved');
        });
    });

    $scope.$on('alert-severity-changed', function (event, data) {
        var emitData = {};
        emitData[data.oldValue] = -1;
        emitData[data.newValue] = 1;
        $scope.$broadcast('alert-severities-data-changed', emitData);
        return updateAlert(data.attributes.id, {
            severity: data.newValue,
            comment: severityChangeAutoComment(getRowById(data.attributes.id)[$scope.tableModel.idToColumn.comment.idx].originalText, data.oldValue, data.newValue)
        }).then(function () {
            $scope.tableHelper.reload(true, true);
            $scope.$broadcast('alert-severities-data-saved');
        });
    });

    $scope.$on('alert-assignee-changed', function (event, data) {
        return updateAlert(data.attributes.id, {
            assignee: data.newValue || null,
            comment: assigneeChangeAutoComment(getRowById(data.attributes.id)[$scope.tableModel.idToColumn.comment.idx].originalText, data.oldValue, data.newValue)
        }).then(function () {
            $scope.tableHelper.reload(true, true);
        });
    });


    $q.all([
        path('alerts').ctrl('alerts').json('alerts').retrieve(),
        avananUserDao.users()
    ]).then(function(responses) {

        $scope.options = responses[0].data;

        var stateColumn = _($scope.options.columns).find(function (column) {
            return column.id === 'state';
        });
        if (stateColumn && stateColumn.converterArgs) {
            stateColumn.converterArgs.variants = alertStates.asVariants();
        }

        var severityColumn = _($scope.options.columns).find(function (column) {
            return column.id === 'severity';
        });
        if (severityColumn && severityColumn.converterArgs) {
            severityColumn.converterArgs.variants = _(policySeverityTypes.asVariants()).map(function (el) {
                return {
                    id: el.id,
                    label: '#' + JSON.stringify({
                        text: el.label,
                        display: 'text',
                        'class': 'severity-display',
                        'style': {
                            'background-color': el.dashboardColor
                        }
                    })
                };
            });

        }

        var assigneeVariants = [{
            id: null,
            label: '[no assignee]'
        }].concat(_(responses[1].data.rows).map(function (user) {
            return {
                id: user.email,
                label: user.email
            };
        }));

        var assigneeColumn = _($scope.options.columns).find(function (column) {
            return column.id === 'assignee';
        });
        if (assigneeColumn && assigneeColumn.converterArgs) {
            assigneeColumn.converterArgs.variants = assigneeVariants;
        }

        $scope.bulkUpdate = function (type) {
            var label = {
                'state': 'Status',
                'assignee': 'Assignee'
            }[type];

            var msg = '';
            var dialogParams = [];
            var updateParams = {};
            var dialogTitle;

            var selectedIds = $scope.tableHelper.getSelectedIds();
            if (selectedIds.length) {
                dialogTitle = 'Change ' + label + ' for selected alerts';
                updateParams = {
                    filter_alert_id: selectedIds
                };

                dialogParams.push({
                    type: 'message',
                    message: 'Change ' + label + ' for ' +
                    '#' + JSON.stringify({
                        display: 'text',
                        'class': 'bold-text',
                        text: selectedIds.length
                    }) + ' selected alerts.',
                    paramClass: 'full-width-param'
                });


            } else {
                dialogTitle = 'Change ' + label + ' for all matched alerts';

                var filtersByTypes = {};

                _($scope.model.filters).each(function (filter) {
                    filtersByTypes[filter.type] = filtersByTypes[filter.type] || [];
                    filtersByTypes[filter.type].push(filter);
                });

                _(filterTypes).each(function (filterType) {
                    if (!filtersByTypes[filterType.type]) {
                        return;
                    }
                    updateParams['filter_' + filterType.updateParam] = _(filtersByTypes[filterType.type]).pluck('id');
                    if (filterType.type === 'severity' && _(updateParams['filter_' + filterType.updateParam]).contains('none')) {
                        updateParams['filter_' + filterType.updateParam].push(null);
                        updateParams['filter_' + filterType.updateParam].push('');
                    }

                    var oneMsg =  '#' + JSON.stringify({
                            display: 'text',
                            'class': 'alerts-confirm-filter-title',
                            text: filterType.label + ':'
                        });

                    if (filterType.multiline) {
                        oneMsg += '#' + JSON.stringify({
                                display: 'linked-text',
                                'class': 'alerts-confirm-filter-value',
                                text: _(filtersByTypes[filterType.type]).map(function (filter) {
                                    return '#' + JSON.stringify({
                                            display: 'text',
                                            'class': 'alerts-confirm-filter-element',
                                            text: filter.label
                                        });
                                }).join('')
                            });
                    } else {
                        oneMsg += '#' + JSON.stringify({
                                display: 'text',
                                'class': 'alerts-confirm-filter-value',
                                text: _(filtersByTypes[filterType.type]).pluck('label').join(', ')
                            });
                    }
                    msg += '#' + JSON.stringify({
                            display: 'linked-text',
                            'class': 'alerts-confirm-filter-line',
                            text: oneMsg
                        });
                });

                if (!msg) {
                    dialogParams.push({
                        type: 'message',
                        message: 'Are you sure you want to change ' + label + ' for ' +
                        '#' + JSON.stringify({
                            display: 'text',
                            'class': 'bold-text',
                            text: 'ALL'
                        }) + ' alerts?',
                        paramClass: 'full-width-param'
                    });
                } else {
                    dialogParams.push({
                        type: 'message',
                        message: 'Are you sure you want to change ' + label + ' for all alerts matching filter' + msg,
                        paramClass: 'full-width-param'
                    });
                }

                dialogParams.push({
                    type: 'message',
                    message: 'Update will affect ' +
                    '#' + JSON.stringify({
                        display: 'text',
                        'class': 'bold-text',
                        text: $scope.tableHelper.getTotal()
                    }) + ' alerts.',
                    paramClass: 'full-width-param'
                });
            }


            var propertyParam = {
                type: 'selector',
                'label': label,
                id: type
            };
            if (type === 'state') {
                propertyParam.variants = alertStates.asVariants();
                propertyParam.validation = {
                    required: true
                };
            }
            if (type === 'assignee') {
                propertyParam.variants = assigneeVariants;
                propertyParam.validation = {
                    defined: true
                };
            }

            dialogParams.push(propertyParam);

            modals.params([{
                lastPage: true,
                params: dialogParams
            }], dialogTitle).then(function (params) {
                $.extend(params, updateParams);
                workingIndicatorHelper.show(true);
                return alertsDao.updateByFilter(params);
            }).then(function () {
                workingIndicatorHelper.hide();
                if (type === 'assignee') {
                    updateAssigneesFilter();
                }
                $scope.tableHelper.clearSelection();
                $scope.tableHelper.reload();
            }, function (error) {
                workingIndicatorHelper.hide();
            });
        };

        $scope.cellHoverInfo = [{
            column: 'policy_name',
            dataColumn: 'reportDataPromise',
            'class': 'alerts-details-hover'
        }, {
            column: 'description',
            dataColumn: 'reportDataPromise',
            'class': 'alerts-details-hover'
        }];

        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
            $location.search('filters', base64.encode(JSON.stringify($scope.model.filters)));
            $location.search('period', base64.encode($scope.model.period) || null);

            return alertsDao.list($scope.options.columns, '2001-01-01')
                .params(alertsHelper.getParams())
                .params({
                    after_period: $scope.model.period
                })
                .pagination(args.offset, args.limit)
                .order(args.ordering.columnName, args.ordering.order);
        }, function(tableModel) {
            $scope.tableModel = tableModel;
            $scope.filtersPanel.title = $scope.tableHelper.getTotal() + ' alerts';
            if ($scope.tableHelper.getTotal()) {
                bulkActionsButton.hidden = false;
            } else {
                bulkActionsButton.hidden = true;
            }
        });

        $scope.$watch('model.filters', function (newVal, oldVal) {
            if (_(newVal).isEqual(oldVal)) {
                return;
            }
            $scope.tableOptions.pagination.page = 1;
            $scope.tableHelper.reload();
        }, true);

        $scope.$watch('model.period', function (newVal, oldVal) {
            if (_(newVal).isEqual(oldVal)) {
                return;
            }
            $scope.tableOptions.pagination.page = 1;
            $scope.tableHelper.reload();
        }, true);

    });
    
    sideMenuManager.treeNavigationPath(['alerts']);
});
