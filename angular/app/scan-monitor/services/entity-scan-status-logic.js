var m = angular.module('scan-monitor');

m.factory('entityScanStatusLogic', function(policyDao, workingIndicatorHelper) {
    return function ($scope, logic) {
        policyDao.scanStatus().then(function(response) {
            $scope.model.data = response;
            workingIndicatorHelper.hide();
        });
    };
});