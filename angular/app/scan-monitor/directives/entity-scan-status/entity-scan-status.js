var m = angular.module('scan-monitor');

m.directive('entityScanStatus', function(path, $q, modulesManager, entityTypes, widgetLogic, objectTypeDao, troubleshooting) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('scan-monitor').directive('entity-scan-status').template(),
        scope: {
            model: '=entityScanStatus'
        },
        link: function($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);
            
            $scope.debugEnabled = troubleshooting.entityDebugEnabled();
            
            $q.all([
                objectTypeDao.entities(),
                modulesManager.init()
            ]).then(function(responses) {
                $scope.entities = responses[0].data.entities;
                $scope.$watch('model.data', function() {
                    $scope.internalModel = $scope.model.data;
                }, true);
            });
            
            
            $scope.getSaasIcon = function(entity) {
                var saas =  modulesManager.cloudApp(entityTypes.toSaas(entity.name));
                return '#' + JSON.stringify({
                    display: 'img',
                    path: saas.img,
                    tooltip: saas.title
                });
            };

            $scope.getAppName = function(secApp) {
                var app = modulesManager.app(secApp.name);
                return app ? app.label : secApp.name;
            };
            
            $scope.getAppStatusClass = function(secApp) {
                var status = secApp.status == 'n/a' ? 'hide' : secApp.status; 
                return 'status-' + status;  
            };
            
            $scope.getEntityName = function(entity) {
                return $scope.entities[entity.name].label;
            };
        }
    };
});