var m = angular.module('scan-monitor');

m.controller('ScanMonitorController', function($scope, widgetPolling, path, sizeClasses, workingIndicatorHelper) {
    widgetPolling.updateWidgets(
        path('scan-monitor').ctrl('scan-monitor').json('scan-monitor').path
    );
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
    workingIndicatorHelper.show(true);
});