var m = angular.module('settings');

m.directive('systemConfigParameter', function(path, configDao, troubleshooting, configCache) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('settings').directive('system-config-parameter').template(),
        scope: {
            parameter: '@systemConfigParameter',
            debugOnly: '=',
            label: '@',
            storage: '@?'
        },
        link: function($scope, element, attrs) {

            $scope.model = {};

            $scope.visible = function () {
                return _($scope.model.value).isBoolean() && (!$scope.debugOnly || troubleshooting.entityDebugEnabled());
            };

            $scope.changed = function () {
                return $scope.model.original !== $scope.model.value;
            };

            $scope.save = function () {
                $scope.model.original = $scope.model.value;
                if ($scope.storage === 'local') {
                    if ($scope.model.value) {
                        window.localStorage.setItem('download-entity-no-confirm', true);
                    } else {
                        window.localStorage.removeItem('download-entity-no-confirm');
                    }
                } else {
                    configDao.setParam($scope.parameter, $scope.model.value).then(function () {
                        configCache.reload($scope.parameter);
                    });
                }
            };

            if ($scope.storage === 'local') {
                var localValue = window.localStorage.getItem($scope.parameter);
                $scope.model.original = (localValue === 'true' || localValue === true || false);
                $scope.model.value = $scope.model.original;
            } else {
                configDao.param($scope.parameter).then(function (value) {
                    $scope.model.original = value;
                    $scope.model.value = value;
                });
            }

        }
    };
});
