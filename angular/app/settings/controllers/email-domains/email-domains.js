var m = angular.module('settings');

m.controller('EmailDomainsController', function($scope, $q, tablePaginationHelper, routeHelper, path, emailDomainsDao, columnsHelper, modals, sideMenuManager) {

    $scope.domainsListPanel = {
        title: "Manage email domains",
        'class': 'light white-body white-header',
        'bodyStyle': {
            'min-height': '500px'
        }
    };
    var actions = [{
        label: 'Delete',
        active: function() {
            return $scope.tableHelper.getSelectedIds().length;
        },
        display: 'button',
        execute: function() {
            var selected = $scope.tableHelper.getSelectedIds();
            return modals.confirm({
                message: 'Are you sure you want to delete domain' + (selected.length > 1 ? 's' : '') +
                '<div class="paddingLR-sm">' + _(selected).join('<br>') + '</div>'
            }).then(function () {
                return $q.all(_(selected).map(function (domain) {
                    return emailDomainsDao.remove(domain);
                }));
            }).then(function() {
                $scope.tableHelper.clearSelection();
                $scope.tableHelper.reload();
            });
        }
    }, {
        label: 'Update',
        active: function() {
            return $scope.tableHelper.getSelectedIds().length;
        },
        display: 'button',
        execute: function() {
            var selected = $scope.tableHelper.getSelectedIds();
            var plural = '';
            if (selected.length > 1) {
                plural = 's';
            }
            var typeColumnIdx = columnsHelper.getIdxById($scope.options.columns, 'type');
            var types = _($scope.tableHelper.getSelected()).chain().map(function (row) {
                return row[typeColumnIdx].originalText;
            }).unique().value();
            return modals.params([{
                lastPage: true,
                params: [{
                    type: 'message',
                    label: 'Domain' + plural,
                    message: _(selected).map(function (domain) {
                        return '#' + JSON.stringify({
                                display: 'text',
                                text: domain,
                                'class': 'display-block'
                            });
                    }).join(''),
                    paramClass: 'constant-param'
                }, {
                    id: 'type',
                    label: 'Type',
                    validation: {
                        required: true
                    },
                    data: (types.length === 1 ? types[0] : undefined),
                    type: 'list',
                    variants: _($scope.domainTypes).map(function (type) {
                        return {
                            id: type,
                            text: type
                        };
                    }),
                    settings: {
                        multiple: false,
                        allowCreate: true
                    }
                }]
            }], 'Update domain' + plural).then(function (data) {
                return $q.all(_(selected).map(function (domain) {
                    return emailDomainsDao.update(domain, data.type);
                }));
            }).then(function() {
                $scope.tableHelper.clearSelection();
                $scope.tableHelper.reload();
            });
        }
    }, {
        label: 'Create New Domain',
        display: 'button',
        execute: function() {
            return createDomain();
        }
    }];

    function createDomain(data) {
        return modals.params([{
            lastPage: true,
            params: [{
                id: 'entity_id',
                type: 'string',
                label: 'Domain',
                validation: {
                    required: true
                },
                data: data && data.entity_id
            }, {
                id: 'type',
                label: 'Type',
                validation: {
                    required: true
                },
                data: data && data.type,
                type: 'list',
                variants: _($scope.domainTypes).map(function (type) {
                    return {
                        id: type,
                        text: type
                    };
                }),
                settings: {
                    multiple: false,
                    allowCreate: true
                }
            }]
        }], 'Create new domain').then(function (data) {
            return emailDomainsDao.create(data.entity_id, data.type).then(function () {
                $scope.tableHelper.reload();
            }, function (error) {
                var msg = error && error.data && error.data.message || 'Unknown error occurred';
                return modals.alert(msg, {title: 'Error'}).then(function () {
                    return createDomain(data);
                });
            });
        });
    }

    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 50,
        pagination: {
            page: 1,
            ordering: {}
        },
//        actions: actions, // temporary disabled actions until better API is created
        "dropdownHeader": true,
        saveState: true,
        headerOptions: {
            enum: {
                multiple: true
            }
        },
        showWorkingIndicator: true
    };
    function refreshDomainTypes(types) {
        $scope.domainTypes = types;
        var filterTypes = $scope.domainTypes;
        if ($scope.tableOptions.pagination.columnsFilters && $scope.tableOptions.pagination.columnsFilters.types) {
            filterTypes = _(filterTypes.concat(_($scope.tableOptions.pagination.columnsFilters.types).pluck('data'))).unique();
        }
        setVariants('type', _(filterTypes).map(function (type) {
            return {
                id: type,
                label: type
            };
        }));
        $scope.tableOptions.headerOptions = {
            enum: {
                multiple: true
            }
        };
    }

    function setVariants(columnId, variants) {
        var column = _($scope.options.columns).find(function(column) {
            return column.id == columnId;
        });
        if (column) {
            column.property = {
                variants: variants
            };
        }
    }


    path('settings').ctrl('email-domains').json('email-domains').retrieve().then(function(response) {
        $scope.options = response.data;
        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {

            var result = emailDomainsDao.list($scope.options.columns)
                .preFetch(emailDomainsDao.types(), 'domainTypes')
                .converter(function (response, args) {
                    refreshDomainTypes(args.domainTypes);
                    return $q.when(response);
                }, _.identity)
                .pagination(args.offset, args.limit)
                .order(args.ordering.columnName, args.ordering.order);
            if (args.columnsFilters) {
                var filterParams = columnsHelper.filtersToParams(args.columnsFilters);
                result.params(filterParams);
            }

            return result;
        }, function(tableModel) {
            $scope.tableModel = tableModel;
        });
    });

    sideMenuManager.treeNavigationPath(['configuration', 'email-domains']);
});