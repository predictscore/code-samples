var m = angular.module('settings');

m.controller('SystemConfigController', function($scope, configDao, $q, modals) {
    $scope.passwordSecurityPanel = {
        title: 'Password Security Policy',
        'class': 'light white-body white-header no-margin',
        bodyStyle: {
            "min-height": '175px'
        },
        showLoading: true,
        isLoading: true
    };
    $scope.passwordSecurityParams = {
        'pwd_expire_enabled': {
            type: 'boolean'
        },
        'pwd_expire_days': {
            type: 'integer',
            errorLabel: 'Password expiration period in days'
        },
        'pwd_history_enabled': {
            type: 'boolean'
        },
        'pwd_history_length': {
            type: 'integer',
            errorLabel: 'Amount of last passwords that can not be used'
        },
        'pwd_lock_enabled': {
            type: 'boolean'
        },
        'pwd_lock_release_hours': {
            type: 'integer',
            errorLabel: 'Automatic password login un-lock interval in hours'
        },
        'pwd_lock_attempts': {
            type: 'integer',
            errorLabel: 'Amount of failed password login attempts in a row'
        },
        'pwd_lock_interval_minutes': {
            type: 'integer',
            errorLabel: 'Period in minutes, during which amount of failed password login attempts is counted'
        },
        'pwd_security_limits': {
            type: 'object',
            noSave: true
        }
    };
    $scope.passwordSecurityModel = {};
    
    function parseParamValue(paramInfo, value) {
        if (paramInfo.type === 'integer') {
            if (_(value).isNull()) {
                return paramInfo.default || 0;
            } else {
                return parseInt(value);
            }
        }
        if (paramInfo.type === 'boolean') {
            return !!value;
        }
        return value;
    }

    $q.all(_($scope.passwordSecurityParams).map(function (paramInfo, param) {
        paramInfo.error = false;
        return configDao.param(param).then(function (value) {
            $scope.passwordSecurityModel[param] = parseParamValue(paramInfo, value);
        });
    })).then(function () {
        $scope.passwordSecurityPanel.isLoading = false;
    }, function (error) {
        $scope.passwordSecurityPanel.isLoading = false;
        $scope.passwordSecurityModel.error = 'Failed to load configuration';
    });
    $scope.savePasswordSecurity = function () {

        var errors = validate();
        if (errors.length) {
            return modals.alert(errors, {title: 'Error'});
        }

        $scope.passwordSecurityPanel.isLoading = true;
        $q.all(_($scope.passwordSecurityParams).chain().pick(function (paramInfo) {
            return !paramInfo.noSave;
        }).map(function (paramInfo, param) {
            return configDao.setParam(param, $scope.passwordSecurityModel[param]);
            }).value()).then(function () {
            $scope.passwordSecurityPanel.isLoading = false;
        }, function (error) {
            $scope.passwordSecurityPanel.isLoading = false;
            return modals.alert('Failed to save password security policy configuration', {title: 'Save error'});
        });
    };

    function validationTextAndStatus(param, value) {
        if (!$scope.passwordSecurityModel || !$scope.passwordSecurityModel.pwd_security_limits ||
            !$scope.passwordSecurityModel.pwd_security_limits[param]) {
            return null;
        }
        var limits = $scope.passwordSecurityModel.pwd_security_limits[param];
        if (!_(limits.min).isUndefined() && !_(limits.max).isUndefined()) {
            return {
                text: 'should be in the range from ' + limits.min + ' to ' + limits.max,
                isValid: value >= limits.min && value <= limits.max
            };
        }
        if (!_(limits.min).isUndefined()) {
            return {
                text: 'should be equal to or greater than ' + limits.min,
                isValid: value >= limits.min
            };
        }

        if (!_(limits.max).isUndefined()) {
            return {
                text: 'should be equal to or less than ' + limits.max,
                isValid: value <= limits.max
            };
        }

        return null;
    }

    function validate() {
        var errors = [];
        _($scope.passwordSecurityParams).each(function (paramInfo, paramName) {
            paramInfo.error = false;
            var validationData = validationTextAndStatus(paramName, $scope.passwordSecurityModel[paramName]);
            if (!validationData) {
                return;
            }
            if (!validationData.isValid) {
                paramInfo.error = paramInfo.errorLabel + ' ' + validationData.text;
                errors.push(paramInfo.error);
            }
        });
        return errors;
    }

    $scope.passwordSecurityTip = function (param) {
        var validationData = validationTextAndStatus(param);
        if (!validationData || !validationData.text) {
            return null;
        }
        return 'Value ' + validationData.text;
    };
});
