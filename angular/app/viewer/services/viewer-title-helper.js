var m = angular.module('viewer');

m.factory('viewerTitleHelper', function($q, $location, $route, modulesManager) {

    function htmlTitle(text) {
        return '#' + JSON.stringify({
                display: 'html',
                text: text
            });
    }

    return {
        'moduleFoundFiles' : function () {
            $route.current.fullTitle = htmlTitle('<strong>' + ((modulesManager.app($location.search().app_name) || {}).label || '') + '</strong> found files');
        },
        'locationSearchTemplate': function (args) {
            if (!args || !args.options || !args.options.pageTitleTemplate) {
                return;
            }
            var data = $location.search();
            // following replace with regexp function replaces angular-style params: e.g. {{saas}} will be replaced by data['saas']
            $route.current.fullTitle = htmlTitle(args.options.pageTitleTemplate.replace(/\{\{([^\}]+)\}\}/g, function (found, param) {
                return data[param] || '';
            }));
        }
    };
});