var m = angular.module('viewer');

m.factory('viewerColumnsFiltersConverter', function() {
    return function (filters) {
        var params = {};
        _(filters).each(function (filter, key) {
            switch (filter.type) {
                case 'boolean':
                    if (_(filter.data).isBoolean()) {
                        params[key] = '' + filter.data;
                    }
                    break;
                case 'text':
                    if (filter.data) {
                        params[key] = filter.data;
                    }
                    break;
            }
        });
        return params;
    };
});