var m = angular.module('viewer');

m.controller('ViewerController', function($scope, $routeParams, viewerTypeDao, objectTypeDao, tablePaginationHelper, $q, addMaxApplicationsRiskConverter, routeHelper, modulesManager, $location, workingIndicatorHelper, viewerTitleHelper, tableExportHelper, configDao, modals, $timeout, columnsHelper) {
    workingIndicatorHelper.init($scope);

    var type = $routeParams.id;

    viewerTypeDao.types().then(function(response) {
        var metaInfo = response.data[type];
        var dataType = metaInfo.type || 'viewer';
        if(dataType == 'viewer') {
            initViewer();
        } else if(dataType == 'custom') {
            var params = metaInfo.params[modulesManager.currentModule()];
            if(_(params).isString()) {
                params = {
                    id: params
                };
            }
            params.module = modulesManager.currentModule();
            routeHelper.redirectTo(metaInfo.route, params, {
                replace: true
            });
        }
     });

    function initViewer() {
        $q.all([
            objectTypeDao.retrieve(),
            viewerTypeDao.listColumns(type)
        ]).then(function (responses) {
            $scope.actionTypes = responses[0].data.actionTypes;
            $scope.options = responses[1].data;

            (viewerTitleHelper[$scope.options.options.pageTitleHelper] || _.noop)($scope.options);

            $scope.viewerPanel = {
                title: 'Loading...',
                'class': 'avanan white-body single-widget bigger-custom-buttons'
            };
            if ($scope.options.panel) {
                $.extend($scope.viewerPanel, $scope.options.panel);
            }

            $scope.tableOptions = {
                pagesAround: 2,
                pageSize: 20,
                pagination: {
                    page: 1,
                    ordering: {},
                    filter: $scope.options.options.noFilter ? undefined : ''
                },
                saveState: true,
                showWorkingIndicator: true,
                disableTop: $scope.options.options.disableTop,
                dropdownHeader: $scope.options.options.dropdownHeader
            };

            if (_($scope.options.entityType).isString()) {
                $scope.tableOptions.actions = [];
                _($scope.actionTypes[$scope.options.entityType]).each(function (action) {
                    $scope.tableOptions.actions.push({
                        label: action.label,
                        execute: function () {
                        }
                    });
                });
            }

            $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function (args) {
                var qs = _($location.search()).omit('pagination');
                var result = viewerTypeDao.list($scope.options.columns, type, qs, {
                    addMaxApplicationsRiskConverter: addMaxApplicationsRiskConverter
                }).pagination(args.offset, args.limit)
                    .filter(args.filter)
                    .order(args.ordering.columnName, args.ordering.order);
                if ($scope.viewerPanel.inlineFilters) {
                    result.params(_($scope.viewerPanel.inlineFilters).chain().map(function (filter) {
                        if (filter.id) {
                            return [filter.id, filter.value];
                        }
                    }).compact().object().value());
                }
                if (args.columnsFilters) {
                    result.params(columnsHelper.filtersToParams(args.columnsFilters));
                }
                return result;
            }, function (tableModel) {
                $scope.tableModel = tableModel;
                if ($scope.options.options.panelTitleTemplate) {
                    $scope.viewerPanel.title = $scope.options.options.panelTitleTemplate.replace(/\{\{([^\}]+)\}\}/g, function (match, param) {
                        if (param === 'tableTotal') {
                            return $scope.tableHelper.getTotal();
                        }
                        if (!$scope.tableModel || !$scope.tableModel.data || !$scope.tableModel.data.length) {
                            return 0;
                        }
                        var columnIdx = columnsHelper.getIdxById($scope.options.columns, param);
                        return $scope.tableModel.data[0][columnIdx].text;
                    });
                } else {
                    $scope.viewerPanel.title = $scope.tableHelper.getTotal() + ' ' + $scope.options.options.title;
                }
            });

            var reloadTimer = null;
            function reloadOnFilter() {
                if (reloadTimer) {
                    $timeout.cancel(reloadTimer);
                    reloadTimer = null;
                }
                reloadTimer = $timeout(function () {
                    reloadTimer = null;
                    $scope.tableHelper.reload();
                }, 500);
            }

            if ($scope.viewerPanel.inlineFilters) {
                $scope.$watch('viewerPanel.inlineFilters', function (newFilters, oldFilters) {
                    if (!_(newFilters).isEqual(oldFilters)) {
                        reloadOnFilter();
                    }
                }, true);
            }




            if (_($scope.options.options.exports).isArray() && $scope.options.options.exports.length > 0) {
                $scope.viewerPanel.buttons = [{
                    'class': 'btn-icon',
                    iconClass: 'fa fa-download',
                    tooltip: 'Export as...',
                    options: _($scope.options.options.exports).map(function (type) {
                        if (type.type === 'email') {
                            return {
                                label: 'Export as ' + type.format + ' to e-mail',
                                onclick: function () {
                                    return exportToEmail(type.format);
                                }
                            };
                        }
                        return {
                            label: 'Export as ' + (type.format || type),
                            onclick: function () {
                                tableExportHelper(type, $scope.options.options.title, $scope.tableHelper);
                            }
                        };
                    })
                }];
            }

            function exportToEmail(format) {
                var p;
                if (format === 'xls') {
                    p = configDao.param('drive_analytics_report_max_lines');
                } else {
                    p = $q.when();
                }


                return p.then(function (maxXlsLines) {
                    maxXlsLines = (maxXlsLines || 0) - 1;

                    if (format === 'xls' && $scope.tableHelper.getTotal() > maxXlsLines) {
                        return modals.alert('Amount of data in report exceeds maximum amount of rows allowed for ' + format + ' file. Please use csv format instead.', {
                            title: 'Report is too big'
                        });
                    }

                    return modals.confirm({
                        message: 'Your export is about to start. An email will be sent to you once completed.',
                        title: 'Export report',
                        okText: 'Start Export'
                    }).then(function () {
                        var qs = $location.search();
                        var args = $scope.tableOptions.pagination;
                        return viewerTypeDao.emailExport(type, qs, format)
                            .filter(args.filter)
                            .order(args.ordering.columnName, args.ordering.order);
                    });
                });

            }

        });
    }
});
