var m = angular.module('viewer');

m.controller('AdvancedViewerController', function($scope, $routeParams, $route, path, advancedViewerDao, tablePaginationHelper, columnsHelper, $q, workingIndicatorHelper, viewerColumnsFiltersConverter, materializedViewsReindexHelper, sideMenuManager) {
    workingIndicatorHelper.init($scope);

    var viewerType = $routeParams.type;
    var module = $routeParams.module;
    var preFilter = $routeParams.preFilter;

    path('viewer').ctrl('advanced-viewer').json(module + '/' + viewerType).retrieve().then(function(response) {
        $scope.options = response.data;

        $scope.viewerPanel = {
            title: 'Loading...',
            'class': 'avanan white-body single-widget'
        };

        if ($scope.options.options.viewName) {
            $scope.viewRefreshHelper = materializedViewsReindexHelper($scope, $scope.options.options.viewName);
            $scope.viewerPanel.buttons = [$scope.viewRefreshHelper.iconButton()];
            delete $scope.options.options.autoUpdate;
            $scope.$watch('viewRefreshHelper.lastRefreshed', function (newVal, oldVal) {
                if (!oldVal || !newVal || newVal == oldVal) {
                    return;
                }
                $scope.tableHelper.reload(true, true);
            });
        }

        $scope.tableOptions = {
            pagesAround: 2,
            pageSize: 20,
            pagination: $.extend({
                page: 1,
                ordering: {},
                filter: ''
            }, ($scope.options.preFilters || {})[preFilter]),
            saveState: true,
            disableTop: true,
            dropdownHeader: true,
            showWorkingIndicator: true
        };

        var facetsTimeout = $q.defer();
        var lastFacets = null;
        var lastArgs;
        var lastFacetsParams = null;
        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function (args) {
            lastArgs = args;
            return advancedViewerDao.list($scope.options.columns, $scope.options.options.dataUrl)
                .params(viewerColumnsFiltersConverter(args.columnsFilters))
                .params(_(args.facets).chain().map(function(value, key) {
                    return [key, value.join(',')];
                }).object().value())
                .pagination(args.offset, args.limit)
                .filter(args.filter)
                .order(args.ordering.columnName, args.ordering.order);
        }, function (tableModel) {
            $scope.tableModel = tableModel;
            $scope.viewerPanel.title = $scope.tableHelper.getTotal() + ' ' + $scope.options.options.title;

            if(_($scope.tableModel).isObject()) {
                if (!_(lastFacets).isNull()) {
                    columnsHelper.updateFacets($scope.tableModel.columns, lastFacets);
                }

                var facetsParams = {
                    params: $.extend({}, viewerColumnsFiltersConverter(lastArgs.columnsFilters), _(lastArgs.facets).chain().map(function (value, key) {
                        return [key, value.join(',')];
                    }).object().value()),
                    filter: lastArgs.filter
                };
                if (_(facetsParams).isEqual(lastFacetsParams)) {
                    return;
                }
                lastFacetsParams = facetsParams;

                facetsTimeout.resolve();
                facetsTimeout = $q.defer();

                $q.all(_($scope.tableModel.columns).chain().map(function (column) {
                    return column.facetsName;
                }).compact().map(function (facets) {
                    return advancedViewerDao.facets($scope.options.options.facetsUrl)
                        .params({facets: facets})
                        .params(facetsParams.params)
                        .filter(facetsParams.filter)
                        .timeout(facetsTimeout.promise).run();
                }).value()).then(function (responses) {
                    lastFacets = {};
                    _(responses).each(function (response) {
                        $.extend(lastFacets, response.data);
                    });
                    columnsHelper.updateFacets($scope.tableModel.columns, lastFacets);
                });
            }
        });
    });
});