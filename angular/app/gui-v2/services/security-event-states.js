var m = angular.module('gui-v2');

m.factory('securityEventStates', function() {
    var data = [{
        id: 'new',
        label: 'New'
    }, {
        id: 'remediated',
        label: 'Remediated'
    }, {
        id: 'dismissed',
        label: 'Dismissed'
    }];

    return {
        data: data,
        get: function(id) {
            var found = _(data).find(function(res) {
                return res.id == id;
            });
            if(!found) {
                return {
                    id: id,
                    label: id.charAt(0).toUpperCase() + id.slice(1)
                };
            }
            return found;
        }
    };
});