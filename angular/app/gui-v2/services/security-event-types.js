var m = angular.module('gui-v2');

m.factory('securityEventTypes', function() {
    var data = [{
        id: 'dlp',
        label: 'DLP'
    }, {
        id: 'malware',
        label: 'Malware'
    }, {
        id: 'phishing',
        label: 'Phishing'
    }, {
        id: 'anomaly',
        label: 'Anomaly'
    }, {
        id: 'suspicious',
        label: 'Suspicious'
    }];

    return {
        data: data,
        get: function(id) {
            var found = _(data).find(function(res) {
                return res.id == id
            });
            if(!found) {
                return {
                    id: id,
                    label: id.charAt(0).toUpperCase() + id.slice(1)
                };
            }
            return found;
        }
    };
});