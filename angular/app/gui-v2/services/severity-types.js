var m = angular.module('gui-v2');

m.factory('severityTypes', function() {
    var data = [{
        id: 5,
        label: 'Critical'
    }, {
        id: 4,
        label: 'High'
    }, {
        id: 3,
        label: 'Medium'
    }, {
        id: 2,
        label: 'Low'
    }, {
        id: 1,
        label: 'None'
    }];

    return {
        data: data,
        get: function(id) {
            var found = _(data).find(function(res) {
                return res.id == id
            });
            if(!found) {
                return {
                    id: id,
                    label: id.charAt(0).toUpperCase() + id.slice(1)
                };
            }
            return found;
        }
    };
});