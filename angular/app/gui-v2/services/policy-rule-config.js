var m = angular.module('gui-v2');

m.factory('policyRuleConfig', function() {

    var saasConfigs = {
        'office365_emails': {
            'type': 'email',
            'identityTypes': {
                'users': 'office365_emails_user'
            }
        },
        'office365_onedrive': {
            'type': 'drive',
            'identityTypes': {
                'users': 'office365_emails_user'
            }
        },
        'google_mail': {
            'type': 'email',
            'identityTypes': {
                'users': 'google_user',
                'groups': 'google_group'
            }
        },
        'google_drive': {
            'type': 'drive',
            'identityTypes': {
                'users': 'google_user',
                'groups': 'google_group'
            }
        }
    };

    var ruleTypes = [{
        id: 'dlp',
        label: 'DLP',
        saasTypes: {
            drive: true,
            email: true
        },
        backendType: 'dlp',
        sec_types: ['dlp'],
        alwaysOnSecTypes: {'dlp': true}
    }, {
        id: 'av',
        label: 'Malware',
        saasTypes: {
            drive: true
        },
        backendType: 'malware',
        sec_types: ['av']
    }, {
        id: 'av_ap',
        label: 'Malware + Phishing',
        saasTypes: {
            email: true
        },
        backendType: 'malware',
        workflows: true,
        sec_types: ['av', 'ap']
    }, {
        id: 'enc',
        label: 'Encryption',
        saasTypes: {
            drive: true
        },
        disabled: true
    }, {
        id: 'anomaly',
        label: 'Anomaly Detection',
        saasTypes: {
            drive: true,
            email: true
        },
        disabled: true
    }, {
        id: 'compliance',
        label: 'End Point Compliance',
        saasTypes: {
            drive: true,
            email: true
        },
        disabled: true
    }, {
        id: 'shadow_it',
        label: 'Shadow IT Discovery',
        saasTypes: {
            email: true
        },
        disabled: true
    }];

    var identityIcon = function (item) {
        if (item.type === 'users') {
            return '#' + JSON.stringify({
                    display: 'html',
                    text: '<svg width="19" height="18" viewBox="0 0 19 18" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" fill="#A1ABAF" d="M14.627 7.146L14.627 7.146C14.8308657 6.575666 14.9357098 5.974673 14.937 5.369 14.8966639 2.394707 12.4740662 0.004741 9.4995 0.004741 6.5249338 0.004741 4.1023361 2.394707 4.062 5.369 4.0631377 5.974773 4.1683343 6.575848 4.373 7.146L4.373 7.146C2.1906699 8.228358 0.8073632 10.451018 0.8 12.887L0.8 16.11C0.8018505 16.396966 0.9176252 16.671443 1.1218536 16.873046 1.326082 17.074649 1.602034 17.186863 1.889 17.185L17.111 17.185C17.397966 17.186863 17.673918 17.074649 17.8781464 16.873046 18.0823748 16.671443 18.1981495 16.396966 18.2 16.11L18.2 12.887C18.1921691 10.451152 16.8090029 8.228718 14.627 7.146L14.627 7.146ZM9.5 2.146C11.2747699 2.168028 12.6977111 3.620825 12.6828875 5.395669 12.6680639 7.170514 11.2210553 8.599341 9.4461651 8.591722 7.6712748 8.584103 6.236586 7.142907 6.237 5.368 6.2513069 3.578797 7.7107642 2.138572 9.5 2.148L9.5 2.146ZM16.025 15.035L2.975 15.035 2.975 12.885C2.9873223 11.208493 3.9729318 9.69202 5.5 9 6.5336784 10.113342 7.9842828 10.745964 9.5035 10.745964 11.0227172 10.745964 12.4733216 10.113342 13.507 9 15.0362077 9.692777 16.0214374 11.213203 16.029 12.892L16.029 15.042 16.025 15.035Z"/></svg>',
                    class: 'item-icon rule-identity-icon-users'
                });
        }
        if (item.type === 'groups') {
            return '#' + JSON.stringify({
                    display: 'html',
                    text: '<svg width="18" height="19" viewBox="0 0 18 19" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" fill="#A1ABAF" d="M15.39 7.444L15.238 7.444C15.622 6.793 15.825 6.052 15.825 5.296 15.825 2.894 13.877 0.946 11.475 0.946 9.073 0.946 7.125 2.894 7.125 5.296 5.978 5.289 4.875 5.738 4.059 6.544 3.243 7.351 2.781 8.449 2.775 9.596 2.775 10.352 2.978 11.093 3.362 11.744L3.21 11.744C1.688 12.367 0.668 13.82 0.6 15.463L0.6 17.114C0.713 17.646 1.123 18.065 1.653 18.188L12.7 18.188C13.207 18.059 13.584 17.633 13.652 17.114L13.652 15.46C13.647 14.915 13.521 14.379 13.282 13.889L17.05 13.889C17.557 13.759 17.934 13.334 18.002 12.815L18.002 11.164C17.933 9.52 16.913 8.067 15.39 7.444ZM11.475 3.144C12.328 3.135 13.106 3.631 13.456 4.409 13.807 5.186 13.664 6.098 13.093 6.731 12.521 7.364 11.629 7.599 10.82 7.33 10.465 6.764 9.983 6.288 9.412 5.939 9.341 5.731 9.303 5.512 9.3 5.292 9.312 4.101 10.284 3.142 11.475 3.148L11.475 3.144ZM7.125 7.444C8.262 7.455 9.194 8.349 9.251 9.485 9.309 10.62 8.471 11.604 7.341 11.729 6.211 11.854 5.179 11.077 4.987 9.956 4.963 9.836 4.951 9.715 4.95 9.593 4.953 9.02 5.185 8.471 5.592 8.068 6 7.665 6.552 7.441 7.125 7.444ZM11.475 16.037L2.775 16.037 2.775 15.46C2.823 14.808 3.188 14.22 3.752 13.889L10.5 13.889C11.064 14.22 11.429 14.808 11.477 15.46L11.477 16.037 11.475 16.037ZM15.825 11.737L10.888 11.737C11.272 11.086 11.475 10.345 11.475 9.589L14.848 9.589C15.412 9.92 15.777 10.508 15.825 11.16L15.825 11.737 15.825 11.737Z"/></svg>',
                    class: 'item-icon rule-identity-icon-groups'
                });
        }
        return '';

    };

    var workflows = [{
        id: 'approve_restoration',
        label: 'Malware workflow',
        variants: [{
            id: 'user',
            label: 'User is alerted and allowed to restore the email',
            default: true
        }, {
            id: 'admin',
            label: 'User is alerted, allowed to request a restore. Admin must approve'
        }, {
            id: 'none',
            label: 'Email removed. User is not alerted. Admin can restore'
        }],
        secType: 'av'
    }, {
        id: 'phishing_mode',
        label: 'Anti-Phishing workflow',
        variants: [{
            id: 'alert',
            label: 'User receives the email with a warning email',
            default: true
        }, {
            id: 'block',
            label: 'User does not receive the email. Admin can restore'
        }],
        secType: 'ap'
    }, {
        id: 'suspicious_phishing_mode',
        label: 'Suspicious phishing workflow',
        advanced: true,
        variants: [{
            id: 'alert',
            label: 'User receives the email with a warning email',
            default: true
        }, {
            id: 'block',
            label: 'User does not receive the email. Admin can restore'
        }],
        secType: 'ap'
    }];

    var ruleTypesMap = _(ruleTypes).chain().map(function (ruleType) {
        return [ruleType.id, ruleType];
    }).object().value();

    var ruleStates = [{
        id: 'running',
        label: 'Running'
    }, {
        id: 'pause',
        label: 'Stopped'
    }];

    var ruleStatesMap = _(ruleStates).chain().map(function (item) {
        return [item.id, item];
    }).object().value();

    var policyRuleConfig = {
        getSaasType: function (saas) {
            return saasConfigs[saas] && saasConfigs[saas].type || 'drive';
        },
        getSaasRuleTypes: function (saas, securityApps) {
            var availableSecTypes = _(securityApps).reduce(function (memo, app) {
                if (app.sec_type) {
                    memo[app.sec_type] = true;
                }
                return memo;
            }, {});
            var saasType = policyRuleConfig.getSaasType(saas);
            return _(ruleTypes).chain().map(function (type) {
                if (_(type.saasTypes[saasType]).isUndefined()) {
                    return;
                }
                return {
                    id: type.id,
                    label: type.label,
                    disabled: type.disabled || !type.saasTypes[saasType] ||
                    (type.sec_types && _(type.sec_types).chain().find(function (secType) {
                        return availableSecTypes[secType];
                    }).isUndefined().value())
                };
            }).compact().value();
        },
        ruleTypeConfig: function (ruleTypeId, securityApps) {
            var availableSecTypes = _(securityApps).reduce(function (memo, app) {
                if (app.sec_type) {
                    memo[app.sec_type] = true;
                }
                return memo;
            }, {});
            var config = angular.copy(ruleTypesMap[ruleTypeId]);
            config.activeSecTypes = {};
            if (config.sec_types) {
                _(config.sec_types).each(function (secType) {
                    if (availableSecTypes[secType]) {
                        config.activeSecTypes[secType] = true;
                    }
                });
                if (_(config.activeSecTypes).isEmpty()) {
                    config.activeSecTypes = null;
                }
            }
            return config;
        },
        getSaasIdentityType: function (saas, type) {
            return saasConfigs[saas] && saasConfigs[saas].identityTypes && saasConfigs[saas].identityTypes[type] || '';
        },
        backendRuleType: function (ruleTypeId) {
            return ruleTypesMap[ruleTypeId].backendType;
        },
        localRuleType: function (saas, backendType) {
            var saasType = policyRuleConfig.getSaasType(saas);
            var ruleType = _(ruleTypes).find(function (type) {
                return type.saasTypes[saasType] && type.backendType === backendType;
            });
            return ruleType && ruleType.id;
        },
        ruleStates: function () {
            return angular.copy(ruleStates);
        },
        identitiesToList: function (rule) {
            var typesOrders = {
                'groups': 1,
                'users': 2
            };
            var list = [];
            _(rule.identity).each(function (data, type) {
                _(data.items).each(function (id, idx) {
                    var resolved = rule._resolved && rule._resolved.entities && rule._resolved.entities[type] && rule._resolved.entities[type][id];
                    if (!resolved || resolved.not_found) {
                        return;
                    }
                    list.push({
                        type: type,
                        id: id,
                        label: resolved.gui_label,
                        sub_label: resolved.gui_sub_label,
                        order: (typesOrders[type] || 3) + '-' + resolved.gui_label + resolved.gui_sub_label,
                        entity_type: data.type
                    });
                });
            });
            return _(list).sortBy('order');
        },
        formatIdentity: function (item) {
            return identityIcon(item) + item.label;
        },
        allDlpRules: function (dlpRules) {
            var selected = [];
            function processItems (items) {
                _(items).each(function (item) {
                    if (item.id) {
                        selected.push(item.id);
                    }
                    if (item.items) {
                        processItems(item.items);
                    }
                });
            }
            processItems(dlpRules);
            return selected;
        },
        workflows: function () {
            return angular.copy(workflows);
        }
    };

    return policyRuleConfig;
});