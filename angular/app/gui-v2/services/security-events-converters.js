var m = angular.module('gui-v2');

m.factory('securityEventsConverters', function(securityEventTypes, securityEventStates, linkedText, $filter, routeHelper, modulesManager, severityTypes) {
    return {
        securityEventTypeConverter: function (text) {
            return securityEventTypes.get(text).label;
        },
        doubleLineDate: function (text) {
            var time = moment.utc(text).local();
            return '#' + JSON.stringify({
                    'display': 'text',
                    'class': 'dbl-line-date-time',
                    'text': time.format('HH:mm:ss')
                }) + '#' + JSON.stringify({
                    'display': 'text',
                    'class': 'dbl-line-date-date',
                    'text': time.format('D.M.YYYY')
                });
        },
        severityConverter: function (text, columnId, columnsMap, args) {
            var attributes = _(args.attributes).chain().map(function (attribute, name) {
                return [name, columnsMap[attribute]];
            }).object().value();
            return '#' + JSON.stringify({
                    'display': 'linked-text',
                    'class': 'severity-indicator level-' + text,
                    'text': _(_.range(1,6)).map(function (idx) {
                        return '#' + JSON.stringify({
                                'display': 'linked-text',
                                'class': 'severity-mark severity-mark-' + idx,
                                text: '#' + JSON.stringify({
                                    display: 'text',
                                    'class': 'item-indicator'
                                }),
                                clickEvent: {
                                    name: args.eventName,
                                    attributes: attributes,
                                    newValue: idx,
                                    oldValue: text
                                },
                                tooltip: (idx !== text) ? 'Change to ' + severityTypes.get(idx).label : null
                            });
                    }).join('')
                });
        },
        severityConverterDashboard: function (text) {
            return '#' + JSON.stringify({
                    'display': 'text',
                    'class': 'display-block',
                    'text': text
                }) + '#' + JSON.stringify({
                    'display': 'linked-text',
                    'class': 'severity-indicator level-' + text,
                    'text': _(_.range(1,6)).map(function (idx) {
                        return '#' + JSON.stringify({
                                'display': 'text',
                                'class': 'severity-mark severity-mark-' + idx
                            });
                    }).join('')
                });

        },
        stateConverter: function (text) {
            if (!text) {
                return '';
            }
            return '#' + JSON.stringify({
                    'display': 'text',
                    'class': 'event-state state-' + text,
                    'text': securityEventStates.get(text).label
                });

        },
        eventWorkflowConverter: function (text, columnId, event) {
            if (!event._resolved) {
                return '';
            }
            var contextActions = {};
            _(event._resolved.available_actions).each(function (action, actionId) {
                if (action.base_action) {
                    action.actionId = actionId;
                    contextActions[action.base_action] = contextActions[action.base_action] || [];
                    contextActions[action.base_action].push(action);
                }
            });
            return '#' + JSON.stringify({
                    display: 'linked-text',
                    'class': 'actions-disabler',
                    text: '#' + JSON.stringify({
                        display: 'text',
                        'class':  'fa fa-spinner fa-pulse'
                    })
                }) + _(event._resolved.performed_actions).chain().map(function (action, actionId) {
                    action.actionId = actionId;
                    return action;
                }).sortBy('order').map(function (action) {
                    var actionId = action.actionId;
                    return '#' + JSON.stringify({
                            display: 'linked-text',
                            'class': 'display-block white-space-nowrap',
                            text: action.label + _(contextActions[actionId]).map(function (action) {
                                return ' #' + JSON.stringify({
                                        display: 'text',
                                        'class': 'action-link',
                                        text: action.label,
                                        clickEvent: {
                                            name: 'event-action',
                                            eventId: event.id,
                                            actionId: action.actionId,
                                            action: action
                                        }
                                    });
                            }).join('')
                        });
                }).value().join('') + _(event._resolved.available_actions).chain().map(function (action, actionId) {
                    if (action.base_action && event._resolved.performed_actions[action.base_action]) {
                        return;
                    }
                    action.actionId = actionId;
                    return action;
                }).compact().sortBy('order').map(function (action) {
                    var actionId = action.actionId;
                    return '#' + JSON.stringify({
                            display: 'text',
                            'class': 'display-block white-space-nowrap action-link',
                            text: action.label,
                            clickEvent: {
                                name: 'event-action',
                                eventId: event.id,
                                actionId: actionId,
                                action: action
                            }
                        });
                }).value().join('');
        },
        resortArrayConverter: function (text, columnId, columnsMap, args) {
            var arr = _(text).sortBy(args.field);
            if (args.reverse) {
                return _(arr).reverse();
            }
            return arr;
        },
        historyItemConverter: function (text, columnId, columnsMap) {
            return '#' + JSON.stringify({
                    display: 'text',
                    'class': 'event-history-item',
                    text: columnsMap.label,
                    tooltip: $filter('localTime')(columnsMap.create_time)
                });
        },
        descriptionConverter: function (text, columnId, columnsMap) {
            if (columnsMap._resolved && columnsMap._resolved[columnId]) {
                text = columnsMap._resolved[columnId];
            }
            var parts = linkedText.toSimpleParts(text);
            return _(parts).chain().map(function (part) {
                if (part.display === 'text') {
                    return part;
                }
                var visual;
                if (part.display === 'embedded-entity') {
                    var tooltip;
                    var tooltipData = _(part.tooltip_fields).filter(function (field) {
                        return (field.type !== 'text' || !_(field.value).isNull()) &&
                            (field.type !== 'entity' || !_(field.entity_id).isNull());
                    });
                    if (tooltipData.length) {
                        tooltip = '<table>' + tooltipData.map(function (field) {
                                var value;
                                switch (field.type) {
                                    case 'text':
                                        value = $filter('localTimeIfIsoString')(field.value);
                                        break;
                                    case 'entity':
                                        value = '<a href="' + routeHelper.getHref('profile', {
                                                entityType: field.entity_type,
                                                id: field.entity_id
                                            }) + '">' + field.label + '</a>';
                                        break;
                                    default:
                                        value = field.value;
                                }
                                return '<tr><td class="key">' + field.key + '</td><td class="value">' +
                                    '<div class="value-wrapper">' + value + '</div></td></tr>';
                            }).join('') + '</table>';
                    }
                    visual = {
                        display: 'link',
                        'class': 'tooltip-keyword',
                        type: 'profile',
                        entityType: part.entity_type,
                        id: part.entity_id,
                        text: part.label
                    };
                    if (tooltip) {
                        visual.tooltip = {
                            content: {
                                text: tooltip
                            },
                            position: {
                                my: 'top left',
                                at: 'bottom left',
                                viewport: true
                            },
                            hide: {
                                delay: 200,
                                fixed: true
                            },
                            style: {
                                classes: 'security-events-entity-tooltip',
                                tip: {
                                    corner: false
                                }
                            }
                        };

                    }
                    return visual;
                }
                if (!part.display && part.label) {
                    return {
                        display: 'text',
                        text: part.label
                    };
                }
            }).map(function (part) {
                return '#' + JSON.stringify(part);
            }).value().join('');
        },
        eventSaas: function (text) {
            if(_(text).isNull()) {
                return '';
            }
            return '#' + JSON.stringify({
                    display: 'img',
                    text: modulesManager.cloudApp(text).title,
                    path: modulesManager.cloudApp(text).img
                });
        }
    };


});