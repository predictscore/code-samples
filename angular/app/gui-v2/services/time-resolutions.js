var m = angular.module('gui-v2');

m.factory('timeResolutions', function() {
    var data = [{
        id: 'minute',
        label: 'Minute',
        pastLabel: 'Last Minute',
        seconds: 60,
        format: 'HH:mm'
    }, {
        id: 'hour',
        label: 'Hour',
        pastLabel: 'Last Hour',
        seconds: 60 * 60,
        format: 'HH:mm'
    }, {
        id: 'day',
        label: 'Today',
        pastLabel: 'Today',
        seconds: 24 * 60 * 60,
        format: 'MMM DD'
    }, {
        id: 'week',
        label: 'Week',
        pastLabel: 'Past Week',
        seconds: 7 * 24 * 60 * 60,
        format: 'MMM DD'
    }, {
        id: 'month',
        label: 'Month',
        pastLabel: 'Past Month',
        seconds: 30 * 24 * 60 * 60,
        format: 'MMM DD'
    }, {
        id: 'year',
        label: 'Year',
        pastLabel: 'Past Year',
        seconds: 365 * 24 * 60 * 60,
        format: 'YYYY'
    }];
    return {
        data: data,
        get: function(id) {
            return _(data).find(function(res) {
                return res.id == id;
            });
        }
    };
});