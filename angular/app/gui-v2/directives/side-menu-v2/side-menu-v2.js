var m = angular.module('gui-v2');

m.directive('sideMenuV2', function(path, routeHelper, $route) {
    var root = path('gui-v2').directive('side-menu-v2');
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('gui-v2').directive('side-menu-v2').template(),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.logo = root.img('logo-white.svg');
            $scope.logoHref = routeHelper.pathToHref('/');
            $scope.menuVisible = function () {
                return $route.current && !$route.current.fullscreen;
            };
        }
    };
});