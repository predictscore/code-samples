var m = angular.module('gui-v2');

m.directive('policyRulesDlpSelector', function(path, $timeout, $q) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('policy-rules/dlp-selector').template('dlp-selector'),
        scope: {
            model: '=policyRulesDlpSelector',
            dlpRules: '='
        },
        link: function($scope, element, attrs) {

            function processItems (items, processor) {
                _(items).each(function (item) {
                    processor(item);
                    if (item.items) {
                        processItems(item.items, processor);
                    }
                });
            }

            $scope.items = angular.copy($scope.dlpRules);
            processItems($scope.items, function (item) {
                if (item.items) {
                    item.collapsed = true;
                }
            });

            processItems($scope.items, function (item) {
                if (item.id && _($scope.model).find(function (itemId) {
                        return item.id === itemId;
                    })) {
                    item.checked = true;
                }
            });

            $scope.$watch('items', function () {
                var selected = [];
                processItems($scope.items, function (item) {
                    if (item.id && item.checked) {
                        selected.push(item.id);
                    }
                });
                $scope.model = selected;
            }, true);
        }
    };
});