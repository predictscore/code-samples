var m = angular.module('gui-v2');

m.directive('policyRulesDlpRulesGroup', function(path, $timeout, $q) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('policy-rules/dlp-rules-group').template('dlp-rules-group'),
        scope: {
            items: '=policyRulesDlpRulesGroup',
            levelStr: '@level'
        },
        link: function($scope, element, attrs) {
            $scope.level = parseInt($scope.levelStr);
            $scope.itemStyle = {
                'padding-left': ($scope.level * 20 + 10) + 'px'
            };
            $scope.itemClicked = function (event, item) {
                if (item.items) {
                    item.collapsed = !item.collapsed;
                }
            };
        }
    };
});