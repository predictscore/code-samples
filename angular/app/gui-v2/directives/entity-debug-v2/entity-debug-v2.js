var m = angular.module('gui-v2');

m.directive('entityDebugV2', function(path, widgetLogic) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('gui-v2').directive('entity-debug-v2').template(),
        scope: {
            model: '=entityDebugV2'
        },
        link: function($scope, element, attrs) {
            widgetLogic($scope, $scope.model.logic);

            $scope.entityDebug = {
                type: $scope.model.entityType,
                id: $scope.model.entityId
            };
        }
    };
});