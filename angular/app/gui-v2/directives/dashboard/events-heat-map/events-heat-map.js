var m = angular.module('gui-v2');

m.directive('dashboardEventsHeatMap', function (path, $q, ipDao, modulesManager) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('dashboard/events-heat-map').template('events-heat-map'),
        scope: {
            resolution: '='
        },
        link: function($scope, element, attrs) {
            $scope.widgetApi = {};
            var missingSaaS = {};

            $scope.$watch('resolution', function(newVal, oldVal) {
                if(newVal !== oldVal) {
                    $scope.widgetApi.reload();
                }
            });

            $scope.loadData = function () {
                return $q.all(_(modulesManager.activeSaasApps()).map(function (saas) {
                    if (missingSaaS[saas.name]) {
                        return;
                    }
                    return ipDao.heatMapRes(saas.name, $scope.resolution).then(function (response) {
                        $scope.model.saasData[saas.name] = response.data.rows;
                    }, function (error) {
                        if (error.status === 404) {
                            missingSaaS[saas.name] = true;
                        }
                    }).then(function () {
                        updateData();
                    });
                }));
            };

            $scope.model = $scope.model || {
                    data: [],
                    saasData: {

                    }
                };

            var mapSingleton = null;

            function getMap() {
                if(_(mapSingleton).isNull()) {
                    mapSingleton = L.map(element.find('.map-container')[0]);//.fitWorld().zoomIn();
                    // L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(mapSingleton);
                    L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
                        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
                        subdomains: 'abcd',
                        maxZoom: 19,
                        minZoom: 1
                    }).addTo(mapSingleton);
                    mapSingleton.fitWorld();
                }
                return mapSingleton;
            }

            var heatLayer = false;

            function updateData() {
                var map = getMap();
                var totalCount = 0;
                var maxCount = 0;
                var allData = {};
                _($scope.model.saasData).each(function (data) {
                    _(data).each(function (point) {
                        var key = point.geo_lat + '_' + point.geo_lon;
                        if (!allData[key]) {
                            allData[key] = {
                                geo_lat: point.geo_lat,
                                geo_lon: point.geo_lon,
                                count: 0
                            };
                        }
                        allData[key].count += point.count;
                    });
                });
                $scope.model.data = _(allData).values();
                _($scope.model.data).each(function (point) {
                    totalCount += (point.count || 0);
                    if (point.count > maxCount) {
                        maxCount = point.count;
                    }
                });
                if (heatLayer) {
                    map.removeLayer(heatLayer);
                }

                var heatParams = {radius: 12, blur: 12, minOpacity: 0.35, maxZoom: 7, max: maxCount};
                if ($scope.model.heatParams) {
                    heatParams.radius = $scope.model.heatParams.radius || 12;
                    heatParams.blur = $scope.model.heatParams.blur || 12;
                    heatParams.minOpacity = ($scope.model.heatParams.minOpacity || 35) / 100;
                    heatParams.maxZoom = $scope.model.heatParams.maxZoom || 7;
                    heatParams.max = ($scope.model.heatParams.ignoreMax ? 1 : maxCount);
                }

                heatLayer = L.heatLayer(_($scope.model.data).map(function(point) {
                    return [point.geo_lat, point.geo_lon, point.count];
                }), heatParams).addTo(map);
            }

            $scope.$on('$destroy', function() {
                if(!_(mapSingleton).isNull()) {
                    mapSingleton.remove();
                }
            });
        }
    };
});