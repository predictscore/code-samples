var m = angular.module('gui-v2');

m.directive('dashboardEventsList', function (path, $q, securityEventsDao, tablePaginationHelper, defaultListConverter, columnsHelper, modulesManager, securityEventTypes, $filter, routeHelper, securityEventsConverters) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('dashboard/events-list').template('events-list'),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.widgetApi = {};
            $scope.loadData = function (initialLoad) {
                return initialLoad ? $q.when() : $scope.tableHelper.reload();
            };

            var filterStates = ['new','remediated'];

            $scope.options = {
                "columns": [{
                    "id": "id",
                    "hidden": true,
                    "primary": true
                }, {
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "entity_type",
                    "hidden": true
                }, {
                    "id": "create_time",
                    "text": "Time",
                    "converter": "doubleLineDate",
                    "sortable": true
                }, {
                    "id": "severity",
                    "text": "Severity",
                    "dataClass": "text-align-center",
                    "converter": "severityConverterDashboard",
                    "filter": {
                        "name": "filterBy_severity",
                        "type": "enum"
                    },
                    "property": {
                        "variants": _(_.range(1,6)).map(function (item) {
                            return {
                                id: item,
                                label: item
                            };
                        })
                    }
                }, {
                    "id": "saas",
                    "text": "SaaS",
                    "converter": "eventSaas",
                    "dataClass": "saas-icon",
                    "filter": {
                        "name": "filterBy_saas",
                        "type": "enum"
                    },
                    "property": {
                        "variants": _(modulesManager.activeSaasApps()).map(function (saas) {
                            return {
                                id: saas.name,
                                label: saas.title
                            };
                        })
                    }
                }, {
                    "id": "type",
                    "text": "Type",
                    "converter": "securityEventTypeConverter",
                    "filter": {
                        "name": "filterBy_type",
                        "type": "enum"
                    },
                    "property": {
                        "variants": securityEventTypes.data
                    }
                }, {
                    "id": "description_short",
                    "text": "Event Description",
                    "class": "wide-column",
                    "dataClass": "wide-column",
                    "converter": "descriptionConverter"
                }, {
                    "id": "_resolved.history",
                    "text": "Remediation",
                    "converter": "multiConverter",
                    "converterArgs": {
                        "converters": [{
                            "converter": "resortArrayConverter",
                            "converterArgs": {
                                "field": "create_time",
                                "reverse": true
                            }
                        }, {
                            "converter": "listConverter",
                            "converterArgs": {
                                "countPostfix": "actions",
                                "collapseRest": true,
                                "moreInPopup": true,
                                "expanded": 10,
                                "itemConverter": "historyItemConverter",
                                "itemConverterScope": "element"
                            }
                        }]
                    }
                }, {
                    "id": "_resolved",
                    "hidden": true
                }],
                "options": {
                    "defaultOrdering": {
                        "columnName": "create_time",
                        "order": "desc"
                    },
                    "singleSelection": true,
                    "lineSelection": true
                }
            };
            $scope.tableOptions = {
                disableTop: true,
                disableBottom: true,
                bodyScroll: true,
                bodyScrollAutoWidth: true,
                orderDisplayMode: 'highlight-column',
                pagination: {
                    pageSize: 20,
                    ordering: {}
                },
                "dropdownHeader": true
            };

            if ($scope.options.options.defaultOrdering) {
                var defaultOrderingColumnIdx = columnsHelper.getIdxByIdStrict($scope.options.columns, $scope.options.options.defaultOrdering.columnName);
                if (defaultOrderingColumnIdx !== -1) {
                    $scope.tableOptions.pagination.ordering = $scope.options.options.defaultOrdering;
                    $scope.tableOptions.pagination.ordering.column = defaultOrderingColumnIdx;
                }
            }

            $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function (args) {
                var result = securityEventsDao.list();
                result.params({
                    resolve: true,
                    filterEnum_current_state: filterStates.join(',')
                });

                if (args.columnsFilters) {
                    var filterParams = columnsHelper.filtersToParams(args.columnsFilters);
                    result.params(filterParams);
                    $scope.prevColumnsFilters = filterParams;
                }

                return result.pagination(args.offset, args.limit)
                    .order(args.ordering.columnName, args.ordering.order)
                    .converter(defaultListConverter, {
                        columns: $scope.options.columns,
                        converters: securityEventsConverters
                    });
            }, function (tableModel) {
                if (!$scope.tableModel || !tableModel || !angular.equals($scope.tableModel.data, tableModel.data)) {
                    $scope.tableModel = tableModel;
                }
            });

            $scope.$watch(function () {
                return $scope.tableHelper.getSelected();
            }, function (selected) {
                if (selected.length !== 1) {
                    return;
                }
                routeHelper.redirectTo('security-events', {}, {
                    qs: {
                        eventId: $scope.tableHelper.getSelectedIds()[0]
                    }
                });
            }, true);
        }
    };
});