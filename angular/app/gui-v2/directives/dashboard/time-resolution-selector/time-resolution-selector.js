var m = angular.module('gui-v2');

m.directive('timeResolutionSelector', function (path, timeResolutions) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('dashboard/time-resolution-selector').template('time-resolution-selector'),
        scope: {
            resolution: '='
        },
        link: function ($scope, element, attrs) {
            $scope.timeResolutions = timeResolutions;

            $scope.onSelect = function (resolution) {
                $scope.resolution = resolution;
            };

            $scope.getLabel = function(resolution) {
                return _(timeResolutions).find(function(res) {
                    return res.value == resolution
                }).label;
            };
        }
    };
});