var m = angular.module('gui-v2');

m.directive('dashboardEventsGraph', function (path, securityEventsDao, timeResolutions, uuid, securityEventTypes) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('dashboard/events-graph').template('events-graph'),
        scope: {
            resolution: '='
        },
        link: function ($scope, element, attrs) {
            $scope.widgetApi = {};
            var internalBarWidth = 0.6;
            $scope.chartLegendId = uuid.random();

            $scope.$watch('resolution', function(newVal, oldVal) {
                if(newVal !== oldVal) {
                    $scope.widgetApi.reload();
                }
            });

            $scope.loadData = function() {
                var config = configureResolutionSettings();
                return securityEventsDao.list().params({
                    'resolutionColumn': 'create_time',
                    'resolutionStep': config.resolutionStep,
                    'resolutionFromNow': true,
                    'resolutionMax': config.resolutionMax,
                    'resolutionFields': ['type'].join(',')
                }).then(function(response) {
                    var tickMap = {};
                    var types = {};
                    var maxValue = 0;
                    _(response.data.rows).each(function(row) {
                        tickMap[row.delta] = tickMap[row.delta] || {};
                        tickMap[row.delta][row.type] = row.count;
                        types[row.type] = true;
                        maxValue = Math.max(maxValue, row.count);
                    });
                    var typesOrdered = _(securityEventTypes.data).map(function(t) {
                        return t.id;
                    });
                    $scope.model = {
                        watchFlotOptions: false,
                        flotOptions: {
                            series: {
                                bars: {
                                    show: true,
                                    fill: 1,
                                    lineWidth: 0
                                }
                            },
                            colors: [
                                '#4c87e7',
                                '#43b4e3',
                                '#f85640',
                                '#fec543'
                            ],
                            xaxis: {
                                min: -0.5,
                                max: config.resolutionMax - 0.5
                            },
                            yaxis: {
                                position: 'right',
                                ticks: [0, 1],
                                transform: function(v) {
                                    return Math.log(v + 1);
                                },
                                min: 0
                            },
                            grid: {
                                borderWidth: 0,
                                margin: 20
                            },
                            legend: {
                                noColumns: 4,
                                container: '#' + $scope.chartLegendId
                            }
                        }
                    };
                    var ytick = 1;
                    while(ytick <= maxValue) {
                        ytick *= 10;
                        $scope.model.flotOptions.yaxis.ticks.push(ytick);
                    }
                    $scope.model.flotOptions.yaxis.max = ytick;
                    var fullBarWidth = 0.5 / Math.max(typesOrdered.length, 1);
                    $scope.model.flotOptions.series.bars.barWidth = fullBarWidth * internalBarWidth;
                    var now = moment();
                    $scope.model.flotOptions.xaxis.ticks = _.chain(_.range(0, config.resolutionMax, config.chartStep)).map(function(delta) {
                        return [config.resolutionMax - delta - 1, moment(now).add(-(delta + 1) * timeResolutions.get(config.resolutionStep).seconds, 'seconds')];
                    }).sortBy(function(result) {
                        return result[1].valueOf();
                    }).map(function(result) {
                        return [result[0], result[1].format(timeResolutions.get(config.resolutionStep).format)];
                    }).value();
                    $scope.model.data = _(typesOrdered).chain().map(function(type, order) {
                        return {
                            label: type,
                            data: _(_.range(0, config.resolutionMax)).map(function(tick) {
                                var tickData = tickMap[config.resolutionMax - tick - 1] || {};
                                return [tick - 0.25 + order * fullBarWidth + fullBarWidth * (1 - internalBarWidth) / 2, tickData[type] || 0];
                            })
                        };
                    }).value();
                });
            };

            function configureResolutionSettings() {
                return {
                        hour: {
                            resolutionMax: 60,
                            chartStep: 10,
                            resolutionStep: 'minute'
                        }, day: {
                            resolutionMax: 24,
                            chartStep: 4,
                            resolutionStep: 'hour'
                        }, week: {
                            resolutionMax: 7,
                            chartStep: 1,
                            resolutionStep: 'day'
                        }, month: {
                            resolutionMax: 4,
                            chartStep: 1,
                            resolutionStep: 'week'
                        }, year: {
                            resolutionMax: 12,
                            chartStep: 2,
                            resolutionStep: 'month'
                        }
                    }[$scope.resolution] || {
                        chartStep: 1,
                        resolutionMax: 5,
                        resolutionStep: $scope.resolution
                    };
            }
        }
    };
});