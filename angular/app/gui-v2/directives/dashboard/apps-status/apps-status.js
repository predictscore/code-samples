var m = angular.module('gui-v2');

m.directive('dashboardAppsStatus', function (path, $q, modulesDao, policyDao, secAppsStatusDao, $timeout, routeHelper) {

    var reloadOnErrorTimeout = 20000;

    function queryReloader(getQuery, timeoutPromise) {
        return getQuery().timeout(timeoutPromise).run().then(function (input) {
            return input;
        }, function (error) {
            if (error.status > 0) {
                return $timeout(reloadOnErrorTimeout).then(function () {
                    return queryReloader(getQuery, timeoutPromise);
                });
            }
            return $q.reject(error);
        });
    }

    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('dashboard/apps-status').template('apps-status'),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.widgetApi = {};
            $scope.loadData = function () {
                return $q.all([loadSaasData(), loadSecurityData()]);
            };

            $scope.modeClass = function (saas) {
                var c = {};
                c[saas.mode] = true;
                return c;
            };

            $scope.appStatusClass = function (app) {
                if (app.status) {
                    return 'fa fa-check status-ok';
                } else {
                    return 'fa fa-times status-bad';
                }
            };

            var infoTags = {
                'files_all': {},
                'emails_all': {},
                'users_internal': {}
            };

            var modeLabels = {
                'detect': 'Detect Mode',
                'protected': 'Protected Mode'
            };

            function loadSaasData() {
                return $q.all([
                    modulesDao.getModules().then(function (response) {
                        return _(response.data.apps_items.modules).chain().filter(function (app) {
                            return app.is_saas && app.state === 'active';
                        }).map(function (app) {
                            var mode = 'detect';
                            var iconNameV2 = app.stack_icon_round ? ('saas-icons-round/' + app.stack_icon_round) : ('saas-icons/' + app.stack_icon);
                            return {
                                name: app.name,
                                label: app.label,
                                img: path('gui-v2').img(iconNameV2),
                                modeLabel: modeLabels[mode],
                                mode: mode,
                                active: {
                                    label: 'Active Users',
                                    count: 0
                                },
                                total: {
                                    label: 'Total objects',
                                    count: 0
                                },
                                order: app.security_stack_order
                            };
                        }).sortBy('order').value();
                    }),
                    policyDao.policyCatalog().params({
                        filterBy_tags: _(infoTags).keys().join(',')
                    }).then(function (response) {
                        return _(response.data.rows).chain().filter(function (row) {
                            return row.policy_status === 'running';
                        }).reduce(function (memo, row) {
                            _(row.tags).each(function (tag) {
                                if (!infoTags[tag]) {
                                    return;
                                }
                                var saas = row.context.saas;
                                memo[saas] = memo[saas] || {};
                                memo[saas][tag] = memo[saas][tag] || {
                                        policies: [],
                                        count: 0
                                    };
                                memo[saas][tag].count += row.counts_match_count;
                                memo[saas][tag].policies.push(row);
                            });
                            return memo;
                        }, {}).value();
                    })
                ]).then(function (responses) {
                    var saasList = responses[0];
                    var policies = responses[1];
                    _(saasList).each(function (saas) {
                        if (!policies[saas.name]) {
                            return;
                        }
                        var usersData = policies[saas.name].users_internal;
                        if (saas.name === 'google_mail' &&
                            (!policies[saas.name] || !policies[saas.name].users_internal) &&
                            policies.google_drive && policies.google_drive.users_internal
                        ) {
                            usersData = policies.google_drive.users_internal;
                        }
                        saas.active.count = usersData && usersData.count || 0;
                        // TODO: update style of policy page before uncommenting
                        saas.active.href = routeHelper.getHref('policy-edit', {
                            id: usersData.policies[0].policy_id
                        });

                        var totalData;
                        if (policies[saas.name].emails_all) {
                            totalData = policies[saas.name].emails_all;
                            totalData.label = 'Total Emails';
                        } else {
                            if (policies[saas.name].files_all) {
                                totalData = policies[saas.name].files_all;
                                totalData.label = 'Total Files';
                            }
                        }
                        if (totalData) {
                            saas.total.count = totalData.count || 0;
                            // TODO: update style of policy page before uncommenting
                            saas.total.href = routeHelper.getHref('policy-edit', {
                                id: totalData.policies[0].policy_id
                            });
                            saas.total.label = totalData.label;
                        }
                    });
                    $scope.saasList = saasList;
                });
            }

            var timeoutDeferred = $q.defer();

            function updateAppList(appsMap) {
                if ($scope.appList) {
                    $scope.appList = _($scope.appList).filter(function (app) {
                        if (!appsMap[app.name]) {
                            return false;
                        }
                        appsMap[app.name].exists = true;
                        return true;
                    });
                } else {
                    $scope.appList = [];
                }
                _(appsMap).each(function (info, appName) {
                    if (info.exists) {
                        return;
                    }
                    $scope.appList.push({
                        name: appName,
                        label: info.app_label,
                        status: true,
                        scanned: 'Loading...',
                        scannedLoading: true,
                        today: 'Loading...',
                        todayLoading: true
                    });
                });
            }
            var totalHours = 0;

            function loadSecurityData() {
                timeoutDeferred.resolve();
                timeoutDeferred = $q.defer();
                return secAppsStatusDao.license().timeout(timeoutDeferred.promise).then(function (response) {
                    updateAppList(response.data);
                    _($scope.appList).each(function (app) {
                        $q.all([
                            queryReloader(function () {
                            return secAppsStatusDao.counts(app.name, 'success_unmatch', 24);
                        }, timeoutDeferred.promise),
                            queryReloader(function () {
                                return secAppsStatusDao.counts(app.name, 'success_match', 24);
                            }, timeoutDeferred.promise)
                        ]).then(function (responses) {
                            app.todayLoading = false;
                            app.today = (responses[0].data || 0) + (responses[1].data || 0);
                        });
                        $q.all([
                            queryReloader(function () {
                                return secAppsStatusDao.counts(app.name, 'success_unmatch', totalHours);
                            }, timeoutDeferred.promise),
                            queryReloader(function () {
                                return secAppsStatusDao.counts(app.name, 'success_match', totalHours);
                            }, timeoutDeferred.promise)
                        ]).then(function (responses) {
                            app.scannedLoading = false;
                            app.scanned = (responses[0].data || 0) + (responses[1].data || 0);
                        });
                    });
                });
            }
        }
    };
});