var m = angular.module('gui-v2');

m.directive('dashboardEventsStatistic', function (path, $q, modulesDao, securityEventsDao, timeResolutions, routeHelper, base64) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('dashboard/events-statistic').template('events-statistic'),
        scope: {
            resolution: '='
        },
        link: function ($scope) {
            $scope.widgetApi = {};
            $scope.widgets = [];
            $scope.$watch('resolution', function(newVal, oldVal) {
                if(oldVal !== newVal) {
                    $scope.widgetApi.reload();
                }
            });

            var filterStates = ['new','remediated'];

            $scope.loadData = function() {
                return $q.all([
                    modulesDao.getModules(),
                    securityEventsDao.list().params({
                        'resolutionColumn': 'create_time',
                        'resolutionStep': $scope.resolution,
                        'resolutionFromNow': true,
                        'resolutionMax': 1,
                        'resolutionFields': ['type', 'saas'].join(',')
                    }).params({
                        filterEnum_current_state: filterStates.join(',')
                    }),
                    securityEventsDao.list().params({
                        'groupBy': ['type', 'saas', 'current_state'].join(',')
                    }).params({
                        filterEnum_current_state: filterStates.join(',')
                    })
                ]).then(function(responses) {
                    var modulesResponse = responses[0].data;
                    var eventsResolutionResponse = responses[1].data.rows;
                    var eventsTotalResponse = responses[2].data.rows;

                    var saass = _(modulesResponse.apps_items.modules).filter(function (app) {
                        return app.is_saas && app.state === 'active';
                    });

                    function count(events, filter) {
                        return _(events).chain().filter(function(row) {
                            return (!filter.type || row.type === filter.type) &&
                                (!filter.state || row.current_state === filter.state) &&
                                (!filter.saas || row.saas === filter.saas);
                        }).reduce(function(memo, row) {
                            return memo + (row.count || 0);
                        }, 0).value();
                    }

                    function createHref(type, resolution, states, saas) {
                        var filters = [];
                        if (type) {
                            filters.push({
                                type: 'type',
                                id: type
                            });
                        }
                        if (saas) {
                            filters.push({
                                type: 'saas',
                                id: saas
                            });
                        }

                        if (!states || states.length === 0) {
                            states = filterStates;
                        }
                        _(states).each(function (state) {
                            filters.push({
                                type: 'current_state',
                                id: state
                            });
                        });

                        var qs = {};
                        if (filters.length) {
                            qs.filters = base64.encode(JSON.stringify(filters));
                        }
                        if (resolution) {
                            qs.resolution = base64.encode(resolution);
                        }
                        return routeHelper.getHref('security-events', {}, qs);
                    }

                    function baseEvents(label, type) {
                        var result = {};
                        result.label = label;
                        result.leftCountLabel = 'Total';
                        result.allCount = count(eventsTotalResponse, {type: type});
                        result.leftCount = count(eventsTotalResponse, {type: type, state: 'new'});
                        result.leftHref = createHref(type, 'all', ['new']);
                        result.remediated = result.allCount ? Math.floor(100 * count(eventsTotalResponse, {type: type, state: 'remediated'}) / result.allCount) : 0;
                        result.remediatedCount = count(eventsTotalResponse, {type: type, state: 'remediated'});
                        result.remediatedHref = createHref(type, 'all', ['remediated']);
                        result.saass = _(saass).chain().map(function(saas) {
                            return {
                                name: saas.name,
                                label: saas.label,
                                totalCount: count(eventsTotalResponse, {type: type, saas: saas.name}),
                                totalHref: createHref(type, 'all', [], saas.name),
                                resolutionLabel: timeResolutions.get($scope.resolution).label,
                                resolutionCount: count(eventsResolutionResponse, {type: type, saas: saas.name}),
                                resolutionHref: createHref(type, $scope.resolution, [], saas.name)
                            };
                        }).filter(function (info) {
                            return info.totalCount || info.resolutionCount;
                        }).value();
                        return result;
                    }

                    function malwareEvents() {
                        return baseEvents('Malware events', 'malware');
                    }

                    function phishingEvents() {
                        var result = baseEvents('Phishing events', 'phishing');
                        result.leftCountLabel = 'Malicious';
                        result.leftCount = count(eventsTotalResponse, {type: 'phishing', state: 'new'});
                        result.leftHref = createHref('phishing', 'all', ['new']);
                        result.rightCountLabel = 'Suspicious';
                        result.rightCount = count(eventsTotalResponse, {type: 'suspicious'});
                        result.rightHref = createHref('suspicious', 'all');
                        _(result.saass).each(function(saas) {
                            saas.disabled = !_(['office365_emails', 'google_mail']).contains(saas.name);
                        });
                        return result;
                    }

                    function anomalyEvents() {
                        return baseEvents('Anomaly events', 'anomaly');
                    }

                    function dlpEvents() {
                        return baseEvents('DLP events', 'dlp');
                    }

                    $scope.widgets = [malwareEvents(), phishingEvents(), anomalyEvents(), dlpEvents()];
                });
            };


        }
    };
});