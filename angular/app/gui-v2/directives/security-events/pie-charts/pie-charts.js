var m = angular.module('gui-v2');

m.directive('securityEventsPieCharts', function (path, $q, modulesDao, securityEventsDao, severityTypes, timeResolutions, securityEventStates,
$timeout, $interval) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('security-events/pie-charts').template('pie-charts'),
        scope: {
            resolution: '=',
            widgetApi: '=?api',
            filters: '=',
            updateFilter: '&'
        },
        link: function ($scope, element, attrs) {
            $scope.widgetApi = $scope.widgetApi || {};
            $scope.severityTypes = severityTypes.data;
            $scope.securityEventsTypes = securityEventStates.data;
            $scope.saasTypes = [];
            $scope.chartVisible = true;
            $scope.loadingFirstTime = true;
            function getData (type) {
                var result = securityEventsDao.list();
                result.params(_($scope.filters).chain().reduce(function (memo, filter) {
                    var type = 'filterEnum_' + filter.type;
                    memo[type] = memo[type] || [];
                    memo[type].push(filter.id);
                    return memo;
                }, {}).mapObject(function (val) {
                    return val.join(',');
                }).value());
                if (timeResolutions.get($scope.resolution)) {
                    result.params({
                        'resolutionColumn': 'create_time',
                        'resolutionStep': $scope.resolution,
                        'resolutionFromNow': true,
                        'resolutionMax': 1,
                        'resolutionFields': type
                    });
                } else {
                    result.params({
                        groupBy: type
                    });
                }
                return result;
            }

            $scope.loadData = function() {
                return $q.all([
                    modulesDao.getModules(),
                    getData('severity'),
                    getData('current_state'),
                    getData('saas')
                ]).then(function(responses) {
                    var modulesResponse = responses[0].data;
                    $scope.saasTypes = composeLabelsMap(modulesResponse);
                    var charts = [{
                        label: 'Events by Severity',
                        field: 'severity',
                        raw: responses[1].data.rows,
                        variants: $scope.severityTypes
                    }, {
                        label: 'Events by State',
                        field: 'current_state',
                        raw: responses[2].data.rows
                    }, {
                        label: 'Events by SaaS',
                        field: 'saas',
                        raw: responses[3].data.rows,
                        variants: $scope.saasTypes
                    }];

                    var colorsByState = {
                        'new': '#e63c53',
                        'remediated': '#91ce55',
                        'dismissed': '#fec543'
                    };
                    var colorsList = [
                        '#e63c53',
                        '#fec543',
                        '#43b4e3',
                        '#91ce55',
                        '#4c87e7'
                    ];
                    var colorsListState = _(charts[1].raw).map(function (row) {
                        return colorsByState[row.current_state];
                    });

                    _(charts).each(function(chart) {
                        chart.show = chart.raw && chart.raw.length > 0;
                        chart.model = {
                            watchFlotOptions: false,
                            flotOptions: {
                                series: {
                                    pie: {
                                        show: true,
                                        highlight: {
                                            opacity: 0.2
                                        }
                                    }
                                },
                                colors: chart.field === 'current_state' ? colorsListState : colorsList,
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                }
                            }
                        };

                        $timeout(function() {
                            $(".chart").bind("plotclick", function(event, pos, obj){
                                if (!obj) {
                                    return;
                                }
                                $scope.chartClick(obj.series.label);
                            });
                        });

                        var maxLoopCount = 20;
                        var loopIndex = 1;
                        var intervalPromise = $interval(function () {
                            if (loopIndex > maxLoopCount) {
                                $interval.cancel(intervalPromise);
                            }
                            var el = $(".chart").find(".legendLabel");
                            if (el && el.length > 0 && el[0].outerText) {
                                $(".legendLabel").bind("click", function (event) {
                                    var label = $(this)[0].outerText;
                                    if (label) {
                                        $scope.chartClick(label);
                                    }
                                });
                                $interval.cancel(intervalPromise);
                            }
                            loopIndex++;
                        }, 250);

                        $scope.$on('$destroy', function () {
                            $interval.cancel(intervalPromise);
                        });

                        if(chart.variants) {
                            chart.model.data = _(chart.variants).map(function(variant) {
                                var label = variant.label;
                                var found = _(chart.raw).find(function(row) {
                                    return row[chart.field] === variant.id;
                                });
                                var count = found ? found.count : 0;
                                return {
                                    label: label.toUpperCase() + ' ' + count,
                                    data: count
                                };
                            });
                        } else {
                            chart.model.data = _(chart.raw).map(function (row) {
                                var label = row[chart.field].toUpperCase();
                                return {
                                    label: label + ' ' + row.count,
                                    data: row.count
                                };
                            });
                        }
                    });

                    if(!angular.equals(charts, $scope.charts)) {
                        $scope.charts = charts;
                    }

                    $scope.loadingFirstTime = false;
                });

                function composeLabelsMap(modulesResponse) {
                    var activeSass = _(modulesResponse.apps_items.modules).filter(function (app) {
                        return app.is_saas && app.state === 'active';
                    });
                    return activeSass.map(function (app) {
                        return {
                            id: app.module_name,
                            label: app.label
                        };
                    });
                }
            };

            $scope.chartClick = function(label) {
                var type;
                var variant = {};
                var severityFound = _($scope.severityTypes).filter(function(item) {
                    return label.toLowerCase().indexOf(item.label.toLowerCase()) === 0;
                });
                if (severityFound && severityFound.length === 1) {
                    type = 'severity';
                    variant = severityFound[0];
                    $scope.updateFilter()(type, variant, false);
                } else {
                    var securityEventFound = _($scope.securityEventsTypes).filter(function(item) {
                        return label.toLowerCase().indexOf(item.label.toLowerCase()) === 0;
                    });
                    if (securityEventFound && securityEventFound.length === 1) {
                        type = 'current_state';
                        variant = securityEventFound[0];
                        $scope.updateFilter()(type, variant, false);
                    } else {
                        var saasFound = _($scope.saasTypes).filter(function (item) {
                            return label.toLowerCase().indexOf(item.label.toLowerCase()) === 0;
                        });
                        if (saasFound && saasFound.length === 1) {
                            type = 'saas';
                            variant = saasFound[0];
                            $scope.updateFilter()(type, variant, false);
                        }
                    }
                }
            };
            $scope.toggleVisibility = function() {
                $scope.chartVisible = !$scope.chartVisible;
            };
        }
    };
});