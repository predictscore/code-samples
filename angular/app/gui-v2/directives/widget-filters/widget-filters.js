var m = angular.module('gui-v2');

m.directive('widgetFilters', function (path, widgetSettings) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('widget-filters').template(),
        scope: {
            model: '=widgetFilters'
        },
        link: function($scope, element, attrs) {
            var forceUpdate = $scope.model.forceUpdate || _.noop;

            function syncCookies(propertyName, propertyState, updateCallback) {
                if (!propertyName) {
                    return;
                }
                var savedState = widgetSettings.param($scope.model.uuid, propertyName);
                if (!_(savedState).isUndefined()) {
                    updateCallback(savedState);
                } else {
                    widgetSettings.param($scope.model.uuid, propertyName, propertyState);
                }
            }

            $scope.$watch('model.filters', function() {
                _($scope.model.filters).each(function (filter) {
                    syncCookies(filter.value, filter.state, function(fromCookies) {
                        filter.state = fromCookies;
                    });
                });
            });


            $scope.filterChanged = function (filter) {
                if(filter) {
                    if (filter.value) {
                        widgetSettings.param($scope.model.uuid, filter.value, filter.state);
                        if (!filter.local) {
                            forceUpdate();
                        }
                    }
                    if (filter.execute) {
                        filter.execute();
                    }
                } else {
                    var force = false;
                    _($scope.model.wrapper.filters).each(function(filter) {
                        if (!filter.value) {
                            return;
                        }
                        widgetSettings.param($scope.model.uuid, filter.value, filter.state);
                        if (!filter.local) {
                            force = true;
                        }
                    });
                    if(force) {
                        forceUpdate();
                    }
                }
            };
        }
    };
});