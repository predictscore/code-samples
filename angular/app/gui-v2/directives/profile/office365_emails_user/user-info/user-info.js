var m = angular.module('gui-v2');

m.directive('profileOffice365EmailsUserUserInfo', function (path) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('profile/office365_emails_user/user-info').template('user-info'),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.panel = {
                "type": "info",
                "logic": {
                    "name": "userInfoLogic",
                    "nameColumn": "display_name",
                    "emailColumn": "mail"
                },
                "size": "small",
                "class": "user-info",
                "bodyStyle": {
                    "height":"260px"
                },
                "wrapper": {
                    "type": "widget-panel",
                    "title": "User info",
                    "class": "avanan white-body"
                }
            };
        }
    };
});