var m = angular.module('gui-v2');

m.directive('profileOffice365EmailsUserUserMetaInfo', function (path) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('profile/office365_emails_user/user-meta-info').template('user-meta-info'),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.panel = {
                "type": "meta-info",
                "logic": {
                    "name": "userMetaInfoLogic",
                    "beautifyKey": true,
                    "extendedData": ["display_name"]
                },
                "uuid": "c95ef4b7-c767-4104-9c64-381d4ebddde8",
                "size": "col-lg-5 col-md-8 col-sm-12 col-xs-12",
                "bodyStyle": {
                    "height":"260px"
                },
                "wrapper": {
                    "type": "widget-panel",
                    "title": "User meta data",
                    "class": "avanan white-body"
                },
                "pair": {
                    "class": "user-meta-data key-width-190"
                }
            };
        }
    };
});