var m = angular.module('gui-v2');

m.directive('profileOffice365EmailsUserUserEmails', function (path) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('profile/office365_emails_user/user-emails').template('user-emails'),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.panel = {
                "type": "data-table",
                "logic": {
                    "name": "userEmailAddressesLogic"
                },
                "size": "col-lg-4 col-md-6 col-sm-12 col-xs-12",
                "bodyStyle": {
                    "height": "260px"
                },
                "class": "light medium-icons",
                "wrapper": {
                    "type": "widget-panel",
                    "title": "User emails",
                    "class": "avanan white-body"
                },
                "options":{
                    "checkboxes":false
                },
                "columns": [{
                    "id": "entity_id",
                    "text": "Email",
                    "primary": true
                }]
            };
        }
    };
});