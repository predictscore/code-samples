var m = angular.module('gui-v2');

m.directive('profileOffice365EmailsUserCollaborators', function (path) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('profile/office365_emails_user/collaborators').template('collaborators'),
        scope: {
            external: '=?'
        },
        link: function($scope, element, attrs) {

            $scope.panel = {
                "type": "flot",
                "logic": {
                    "name": "userTopCollaboratorsPlotLogic",
                    "lines": 20,
                    "noEmptyLines": true,
                    "showFlotNumbers": true,
                    "showExternal": $scope.external,
                    "showInternal": !$scope.external
                },
                "uuid": "7942857a-1d96-4d0e-bc6e-65ff051002c2",
                "size": "col-lg-6 col-md-6 col-sm-12 col-xm-12",
                "bodyStyle": {
                    "height": "400px"
                },
                "wrapper": {
                    "type": "widget-panel",
                    "title": 'Top ' + ($scope.external ? 'External' : 'Internal') + ' collaborators',
                    "class": "avanan white-body overflow-auto"
                },
                "flotOptions": {
                    "series": {
                        "bars": {
                            "show": true,
                            "horizontal": true,
                            "align": "center",
                            "barWidth": 0.8
                        }
                    },
                    "grid":{
                        "hoverable": true,
                        "clickable": true,
                        "borderWidth": 1,
                        "tickColor": "#eee",
                        "borderColor": "#eee"
                    },
                    "legend": {
                        "show": false,
                        "position": "nw"
                    },
                    "yaxis": {
                        "axisLabelUseCanvas": true,
                        "tickSize": 1
                    },
                    "xaxis": {
                        "minTickSize": 1
                    },
                    "shadowSize": 0,
                    "tooltip": true,
                    "tooltipOpts": {
                        "defaultTheme": false,
                        "content": "%s: %x.0",
                        "shifts": {
                            "x": 0,
                            "y": 20
                        }
                    },
                    "colors": [
                        "#2980b9",
                        "#89b041",
                        "#f39c12"
                    ]
                }
            };
        }
    };
});