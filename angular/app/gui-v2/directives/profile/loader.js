var m = angular.module('gui-v2');

m.directive('profileLoader', function (widgetPolling) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: function (element, attrs) {
            return widgetPolling.scope().profileTemplate;
        },
        scope: false,
        link: function($scope, element, attrs) {
            $scope.shared = {};
        }
    };
});