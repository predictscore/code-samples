var m = angular.module('gui-v2');

m.directive('profileJsonLoader', function (path, widgetPolling, sizeClasses) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('profile/json-loader').template('json-loader'),
        scope: false,
        link: function($scope, element, attrs) {
            $scope.widgetPolling = widgetPolling;
            $scope.getSizeClasses = sizeClasses;

            widgetPolling.updateWidgets(
                path('gui-v2').ctrl('profile-v2').json(widgetPolling.scope().widgetsPath).path,
                path('gui-v2').ctrl('profile-v2').json('common-widgets').path
            );
        }
    };
});