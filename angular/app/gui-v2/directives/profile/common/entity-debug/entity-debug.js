var m = angular.module('gui-v2');

m.directive('profileEntityDebug', function (path, troubleshooting) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('profile/common/entity-debug').template('entity-debug'),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.panel = {
                "type": "entity-debug",
                "logic": {
                    "name": "debugEntityLogic",
                    "usePollingEntityType": true
                },
                "uuid": "4f711b83-1d9d-44cb-aeda-11a9ca16a5dc",
                "size": "large",
                "class": "light",
                "wrapper": {
                    "type": "widget-panel",
                    "title": "Entity debug",
                    "class": {
                        "avanan white-body overflow-auto": true,
                        "display-none": true
                    }
                }
            };
        }
    };
});