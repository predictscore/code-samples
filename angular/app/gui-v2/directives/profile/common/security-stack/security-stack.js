var m = angular.module('gui-v2');

m.directive('profileSecurityStack', function (path, $q) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('profile/common/security-stack').template('security-stack'),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.widgetApi = {};
            $scope.loadData = function () {
                return $q.when();
            };

            $scope.panel = {
                "type": "multi-widget",
                "logic": {
                    "name": "scanDetailsLogic",
                    "updateTick": 60000
                },
                "uuid": "96d4b3f1-f171-45a3-8437-dd349c1159d3",
                "size": "medium",
                "class": "light",
                "wrapper": {
                    "type": "widget-panel",
                    "title": "Security Stack",
                    "class": "avanan white-body overflow-auto"
                }
            };
            $scope.filters = {
                uuid: $scope.panel.uuid,
                forceUpdate: function () {
                    console.log('force Update is called');
                    $scope.panel.forceUpdate();
                }
            };
            $scope.$watch('panel.wrapper.filters', function () {
                if ($scope.panel.wrapper.filters) {
                    $scope.filters.filters = $scope.panel.wrapper.filters;
                }
            });
        }
    };
});