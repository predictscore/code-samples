var m = angular.module('gui-v2');

m.directive('profileOffice365EmailsEmailAttachments', function (path) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('profile/office365_emails_email/attachments').template('attachments'),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.panel = {
                "type": "data-table",
                "logic": {
                    "name": "emailAttachmentsListLogic",
                    "entityType": "office365_emails_attachment",
                    "updateTick": 270000
                },
                "size": "col-lg-12 col-md-12 col-sm-12 col-xm-12",
                "bodyStyle": {
                    "height": "230px"
                },
                "class": "light medium-icons",
                "wrapper": {
                    "type": "widget-panel",
                    "title": "Email attachments",
                    "class": "avanan white-body overflow-auto"
                },
                "columns": [{
                    "id": "entity_id",
                    "hidden": true,
                    "primary": true
                }, {
                    "id": "ContentType",
                    "text": false,
                    "converter": "mimeIconConverter",
                    "converterArgs": {
                        "niceMimeColumn": "nice_mime_type"
                    },
                    "dataStyle": {
                        "width": "40px"
                    },
                    "style": {
                        "width": "40px"
                    }
                }, {
                    "id": "Name",
                    "text": "Name",
                    "converter": "attachmentNameConverter",
                    "converterArgs": {
                        "idColumn": "entity_id",
                        "quarantineStatusEntity": "office365_emails_attachment"
                    }
                }, {
                    "id": "scanStatus",
                    "text": " "
                }, {
                    "id": "Size",
                    "text": "Size",
                    "format": "bytes"
                }, {
                    "id": "is_orig_quarantine",
                    "hidden": true
                }, {
                    "id": "is_orig_quarantine_restored",
                    "hidden": true
                }, {
                    "id": "is_quarantined",
                    "hidden": true
                }, {
                    "id": "quarantine_is_restored_attachment",
                    "hidden": true
                }, {
                    "id": "quarantine_is_original_attachment",
                    "hidden": true
                }, {
                    "id": "quarantine_is_quarantined_attachment",
                    "hidden": true
                }, {
                    "id": "quarantine_is_replacement_attachment",
                    "hidden": true
                }, {
                    "id": "container_eml_is_quarantined_restored",
                    "hidden": true
                }]
            };
        }
    };
});