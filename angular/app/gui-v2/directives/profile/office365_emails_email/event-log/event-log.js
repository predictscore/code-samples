var m = angular.module('gui-v2');

m.directive('profileOffice365EmailsEmailEventLog', function (path) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('profile/office365_emails_email/event-log').template('event-log'),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.panel = {
                "type": "data-table",
                "logic": {
                    "name": "emailEventsListLogic",
                    "entityType": "office365_emails_email",
                    "itemsEntityType": "office365_emails_attachment",
                    "itemNameColumn": "Name",
                    "updateTick": 60000
                },
                "size": "col-lg-12 col-md-12 col-sm-12 col-xm-12",
                "bodyStyle": {
                    "height": "415px"
                },
                "class": "light",
                "wrapper": {
                    "type": "widget-panel",
                    "title": "Live event log",
                    "class": "avanan white-body overflow-auto"
                },
                "options": {
                    "pagesAround": 2,
                    "pageSize": 100000,
                    "pagination": {
                        "page": 1
                    }
                },
                "columns": [{
                    "id": "id",
                    "hidden": true,
                    "description": "Metacolumn that not exist in sql, used for dynamic rows"
                }, {
                    "id": "time",
                    "text": "Date/time",
                    "format": "time",
                    "class": "one-line-date"
                }, {
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "file_name",
                    "text": "Object",
                    "href": {
                        "type": "file",
                        "params": {
                            "id": "entity_id"
                        }

                    }
                }, {
                    "id": "name",
                    "text": "User"
                }, {
                    "id": "description",
                    "text": "Event",
                    "dataClass": "cell-div-width-100"
                }]
            };
        }
    };
});