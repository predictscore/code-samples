var m = angular.module('gui-v2');

m.directive('profileOffice365EmailsEmailConversations', function (path) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('profile/office365_emails_email/conversations').template('conversations'),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.panel = {
                "type": "data-table",
                "logic": {
                    "name": "emailConversationListLogic"
                },
                "size": "col-lg-12 col-md-12 col-sm-12 col-xm-12",
                "bodyStyle": {
                    "height": "230px"
                },
                "class": "light medium-icons",
                "wrapper": {
                    "type": "widget-panel",
                    "title": "Conversation",
                    "class": "avanan white-body overflow-auto"
                },
                "columns": [{
                    "id": "entity_id",
                    "hidden": true,
                    "primary": true
                }, {
                    "id": "ReceivedDateTime",
                    "text": "Time",
                    "format": "time"
                }, {
                    "id": "Subject",
                    "text": "Subject",
                    "href": {
                        "type": "email",
                        "params": {
                            "id": "entity_id"
                        }
                    }
                }]
            };
        }
    };
});