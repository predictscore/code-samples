var m = angular.module('gui-v2');

m.directive('profileOffice365EmailsEmailEmailInfo', function (path, $q, mainQueueTroubleshootingHelper) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('gui-v2').directive('profile/office365_emails_email/email-info').template('email-info'),
        scope: {
            shared: '=?profileOffice365EmailsEmailEmailInfo'
        },
        link: function($scope, element, attrs) {
            var titlePrefix = 'EMAIL PROFILE';

            $scope.shared.title = titlePrefix;

            $scope.widgetApi = {};
            $scope.loadData = function () {
                return $q.when();
            };

            $scope.recipientsWidget = {
                "logic": {
                    "name": "emailRecipientsListLogic"
                },
                "class": "light medium-icons meta-like",
                "wrapper": {
                    "type": "none"
                },
                "columns": [{
                    "id": "type",
                    "text": "Type",
                    "dataStyle": {
                        "text-transform": "capitalize"
                    }
                }, {
                    "id": "users",
                    "text": false,
                    "converter": "listConverter",
                    "converterArgs": {
                        "countPostfix": "recipients",
                        "collapseRest": true,
                        "expanded": 4,
                        "itemConverter": "inlineUserWithEmail",
                        "itemConverterArgs": {
                            "userIdColumn": "entity_id",
                            "emailColumn": "mail",
                            "addAllEmailsLink": true
                        },
                        "itemConverterScope": "element",
                        "itemConverterTextColumn": "displayName",
                        "class": "dropdown-auto-one-line"
                    }
                }]
            };

            $scope.infoWidget = {
                "logic": {
                    "name": "emailMetaInfoLogic",
                    "beautifyKey": true,
                    "advanced": true,
                    "subjectTitle": false,
                    "updateTick": null,
                    "entityDataMaster": true,
                    "extendEntityInfo": {
                        "userAliases": {
                            "label": "User Aliases",
                            "ticket_view_default_order": 13,
                            "ticket_view_advanced_order": 13
                        }
                    },
                    "converters": {
                        "userAliases": {
                            "converter": "limitedArrayConverter",
                            "args": {
                                "collapseRest": true,
                                "expanded": 10,
                                "countPostfix": "aliases"
                            }
                        }
                    }
                },
                "uuid": "265cd681-6b2e-4dcb-98a1-234a2b202773",
                "wrapper": {
                    "type": "none"
                },
                "pair": {
                    "class": "email-meta-data"
                }
            };

            $scope.restoreRequestWidget = {
                "logic": {
                    "name": "emailRestoreRequestLogic",
                    "entityDataSlave": true
                },
                "class": "restore-request-widget",
                "style": {
                    "display": "none"
                },
                "wrapper": {
                    "type": "none"
                }
            };

            $scope.actionsWidget = {
                "type": "actions",
                "logic": {
                    "name": "remediationActionsLogic",
                    "updateTick": null,
                    "entityType": "office365_emails_email",
                    "entityDataSlave": true
                },
                "class": "file-info-actions-widget wide-250-buttons",
                "wrapper": {
                    "type": "none"
                },
                "actions": [{
                    "actionType": "office365_emails_email_quarantine",
                    "disabledEntityAny": {
                        "is_quarantined": true
                    },
                    "hiddenEntityAny": {
                        "is_quarantined_notification": true,
                        "is_quarantined_user_notification": true,
                        "is_restored_from_quarantine": true,
                        "is_deleted_message": true,
                        "is_pre_release_message": true
                    }
                }, {
                    "actionType": "office365_emails_email_restore",
                    "hiddenEntityAny": {
                        "is_quarantined": {
                            "operator": "not_equal",
                            "value": true
                        }
                    },
                    "disabledEntityAny": {
                        "is_quarantined_restored": true
                    },
                    "feature": "emailRestore"
                }, {
                    "actionType": "office365_emails_restore_decline",
                    "hiddenEntityAny": {
                        "is_quarantined": {
                            "operator": "not_equal",
                            "value": true
                        },
                        "is_restore_declined": true,
                        "restoration_requested": false,
                        "is_quarantined_restored": true
                    },
                    "feature": "emailRestore"
                }]
            };

            $scope.filters = {
                uuid: '265cd681-6b2e-4dcb-98a1-234a2b202773',
                "filters": [{
                    "type": "flag",
                    "text": "Advanced",
                    "value": "advanced",
                    "local": true
                }]
            };

            $scope.mainQueueDebug = mainQueueTroubleshootingHelper.button();

            $scope.$watch('infoWidget.entityData', function () {
                $scope.restoreRequestWidget.entityData = $scope.infoWidget.entityData;
                $scope.actionsWidget.entityData = $scope.infoWidget.entityData;
                if ($scope.infoWidget.entityData && $scope.infoWidget.entityData.Subject) {
                    $scope.shared.title = titlePrefix + ' - ' + $scope.infoWidget.entityData.Subject;
                }
            });

        }
    };
});