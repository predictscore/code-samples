var m = angular.module('gui-v2');

m.directive('mainMenuV2', function(path, routeHelper, modulesManager, feature) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('gui-v2').directive('main-menu-v2').template(),
        scope: {},
        link: function($scope, element, attrs) {
            var menus = [{
                "title": "Dashboard",
                "name": "dashboard"
            }, {
                "title": "Policy",
                "name": "policy-rules"
            }, {
                "title": "Events",
                "name": "security-events"
            }, {
                "title": "Quarantine",
                "name": "quarantine"
            }, {
                "title": "Configuration",
                "name": "configuration",
                "nolink": true,
                "submenus": [{
                    "title": "Cloud App Store",
                    "icon": "fa-laptop",
                    "name": ["cloud-apps"],
                    "feature": "cloudAppsTab"
                }, {
                    "title": "Security App Store",
                    "icon": "fa-tag",
                    "name": ["app-store", "app-store-filter"],
                    "feature": "appStoreTab"
                }, {
                    "title": "Users Management",
                    "icon": "fa-tag",
                    "name": ["user-management", "wizard-user-management"],
                    "feature": "userManagementTab"
                }, {
                	"title": "Settings",
                    "icon": "fa-cog",
                    "name": ["settings"],
                    "feature": "appSettingsTab"
                }]
            }];

            $scope.menuItemClicked = function (e) {
                element.find('.openable').not(e.currentTarget).removeClass('open');
                var menuItem = $(e.currentTarget);
                menuItem.toggleClass('open');
                // if (menuItem.hasClass('open')) {
                //     var submenu = menuItem.find('.submenu');
                //     if (submenu.length > 0) {
                //         submenu[0].scrollIntoView();
                //     }
                // }
            };

            function setMenuHref (menu, activeModules) {
                if (!menu.name || menu.nolink) {
                    return;
                }
                if (_(menu.saas).isUndefined()) {
                    menu.href = routeHelper.getHref(menu.name, menu.params);
                } else {
                    menu.href = routeHelper.getHref(menu.name, $.extend({
                        module: _(menu.saas).intersection(activeModules)[0]
                    }, menu.params));
                }
            }

            function filterMenuItems (menus, activeModules) {
                return _(menus).chain().filter(function(menu) {
                    return _(menu.feature).isUndefined() || feature[menu.feature];
                }).filter(function(menu) {
                    return _(menu.saas).isUndefined() || _(menu.saas).intersection(activeModules).length > 0;
                }).filter(function(menu) {
                    return !menu.admin || sideMenuManager.currentUser().admin;
                }).value();
            }


            function setMenus() {
                var activeModules = _(modulesManager.cloudApps()).pluck('name');
                $scope.menus = filterMenuItems(menus, activeModules);
                _($scope.menus).each(function (menu) {
                    menu.isActive = function () {
                        return routeHelper.isActive(menu.name);
                    };
                    setMenuHref(menu, activeModules);
                    _(menu.submenus).each(function (item) {
                        setMenuHref(item, activeModules);
                    });
                });
            }
            setMenus();
        }
    };
});