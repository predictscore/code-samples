var m = angular.module('gui-v2');

m.directive('wizardItemV2', function(path, wizardHelper, appConfigurator, licenseOptions) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('gui-v2').directive('wizard-item-v2').template(),
        scope: {
            model: '=wizardItemV2'
        },
        link: function ($scope, element, attrs) {
            $scope.path = path('app-store');

            $scope.operationInProgress = function () {
                return wizardHelper.operationInProgress($scope.model.options.moduleName);
            };

            $scope.startButton = {
                enabled: wizardHelper.showActivateButton($scope.model.options.moduleName),
                execute: function () {
                    wizardHelper.activateApp($scope.model.options.moduleName);
                }
            };

            $scope.contactSupport = {
                enabled: $scope.model.options.options &&
                    $scope.model.options.options.license === 'contact_support' &&
                    $scope.model.options.moduleStatus !== 'display_only' &&
                    $scope.model.options.moduleStatus !== 'require_license',
                execute: function () {
                    licenseOptions.contactSupportDialog($scope.model.options);
                }
            };


            $scope.configure = function() {
                if (appConfigurator.configAvailable($scope.model.options, true)) {
                    return appConfigurator.configure($scope.model.options, true);
                }
            };

            $scope.$on('configure-app', function (event, args) {
                if (args.app === $scope.model.options.moduleName) {
                    $scope.configure();
                }
            });
        }
    };
});