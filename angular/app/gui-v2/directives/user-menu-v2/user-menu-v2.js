var m = angular.module('gui-v2');

m.directive('userMenuV2', function(path, sideMenuManager, popupManager) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('gui-v2').directive('user-menu-v2').template(),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.displayName = '';

            $scope.isAdmin = function () {
                return !!sideMenuManager.currentUser().admin;
            };

            $scope.logout = function (event) {
                event.preventDefault();
                event.stopPropagation();
                popupManager.current('logoutConfirm');
            };

            $scope.$watch(function () {
                return sideMenuManager.currentUser();
            }, function (currentUser) {
                var userName = _([currentUser.first_name, currentUser.last_name]).compact().join(' ');
                $scope.displayName = userName || currentUser.email;
                $scope.tooltip = {
                    content: {
                        text: (userName ? (userName + '<br />'): '') + currentUser.email
                    },
                    position: {
                        at: 'top left',
                        my: 'bottom left',
                        viewport: true
                    }
                };
            });
        }
    };
});