var m = angular.module('gui-v2');

m.directive('selectorV2', function(path, $timeout) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('gui-v2').directive('selector-v2').template(),
        scope: {
            model: '=selectorV2',
            variants: '=',
            change: '&?',
            noVariantsCaption: '@?',
            isOpen: '=?dropdownOpen'
        },
        link: function($scope, element, attrs) {
            $scope.change = $scope.change || _.noop;

            $scope.setVariant = function($event, variant) {
                $event.preventDefault();
                if (variant && variant.disabled) {
                    $event.stopPropagation();
                    return;
                }
                var prev = $scope.model;
                if(_(variant).isObject()) {
                    $scope.model = variant.id;
                } else {
                    $scope.model = variant;
                }
                if(prev != $scope.model) {
                    $timeout(function () {
                        $scope.change({
                            newValue: $scope.model,
                            oldValue: prev
                        });
                    });
                }
            };

            $scope.getLabel = function(id) {
                var variant = _($scope.variants).find(function(v) {
                    if(_(v).isObject()) {
                        return v.id === id;
                    }
                    return v === id;
                });
                if(_(variant).isUndefined()) {
                    return '';
                }
                if(_(variant).isObject()) {
                    return variant.label;
                }
                return variant;
            };

            $scope.getVariantLabel = function(variant) {
                if(_(variant).isObject()) {
                    return variant.label;
                }
                return variant;
            };
        }
    };
});