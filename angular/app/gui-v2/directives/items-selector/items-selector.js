var m = angular.module('gui-v2');

m.directive('itemsSelector', function(path, $timeout, $q) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('gui-v2').directive('items-selector').template(),
        scope: {
            api: '=itemsSelector',
            getItems: '&',
            showFilter: '@?',
            options: '=?'
        },
        link: function($scope, element, attrs) {
            $scope.api = $scope.api || {};

            $scope.internal = {
                items: [],
                filter: ''
            };
            $scope.pageSize = 6;
            var listElement = element.find('.items-list');
            var haveMore = true;
            var lastItemTop = false;

            var getItems = $scope.api.getItems || $scope.getItems;

            $scope.api.getSelected = function () {
                return _($scope.internal.items).chain().filter(function (item) {
                    return item.selected;
                }).map(function (item) {
                    return _(item).omit('display', 'selected', 'uniqueId');
                }).value();
            };

            $scope.api.reload = function (force) {
                haveMore = true;
                $scope.internal.items = [];
                lastItemTop = false;
                loadMore(true, force);
            };


            function getIcon(type) {
                if (type === 'users') {
                    return '#' + JSON.stringify({
                            display: 'text',
                            'class': 'fa fa-user'
                        });
                } else {
                    return '';
                }
            }

            var loadTimeout = $q.defer();

            function loadMore(loadDouble, force) {
                if (!haveMore) {
                    return;
                }
                if ($scope.loading && !force) {
                    return;
                }
                loadTimeout.resolve();
                loadTimeout = $q.defer();

                $scope.loading = true;
                var limit = $scope.pageSize * (loadDouble ? 2 : 1);
                getItems($scope.internal.items.length, limit, $scope.internal.filter, loadTimeout.promise).then(function (response) {
                    if (response.data.rows && response.data.rows.length) {
                        _(response.data.rows).each(function (item) {
                            $.extend(item, {
                                display: $scope.api.formatItem(item),
                                uniqueId: item.type + '_' + item.id
                            });
                            $scope.internal.items.push(item);
                        });
                    }
                    if (response.data.rows.length < limit) {
                        haveMore = false;
                    }
                    $scope.loading = false;
                    lastItemTop = false;
                    $timeout(scrollWatcher); // to handle cases where pageSize is too small
                });

            }

            $scope.toggleSelected = function (item) {
                item.selected = !item.selected;
            };

            $scope.doubleClicked = function (event, item) {
                event.preventDefault();
                event.stopPropagation();
                item.selected = true;
                if ($scope.api.doubleClicked) {
                    $scope.api.doubleClicked();
                }
            };

            function scrollWatcher() {
                if (!haveMore || $scope.loading) {
                    return;
                }
                if (lastItemTop === false) {
                    var lastItem = listElement.find('.item').not('.loading').last();
                    if (!lastItem.length) {
                        return;
                    }
                    lastItemTop = lastItem[0].offsetTop;
                }
                if (lastItemTop < listElement.scrollTop() + listElement.height()) {
                    loadMore();
                }
            }

            listElement.on('scroll', scrollWatcher);

            $scope.api.reload();

            var filterWatchTimer;
            function setFilter() {
                $timeout.cancel(filterWatchTimer);
                $scope.api.reload(true);
            }

            $scope.$watch('internal.filter', function (newValue, oldValue) {
                if (newValue == oldValue) {
                    return;
                }
                filterWatchTimer = $timeout(setFilter, 300);
            });

            $scope.$on('$destroy', function () {
                listElement.off('scroll', scrollWatcher);
            });


        }
    };
});