var m = angular.module('gui-v2');

m.controller('PolicyRulesController', function($scope, $q, objectTypeDao, tablePaginationHelper, policyRulesDao, tableBodyAdjuster, routeHelper, defaultListConverter, policyRuleConfig, modulesManager) {
    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 20,
        pagination: {
            page: 1,
            ordering: {}
        },
        saveState: true,
        disableTop: true,
        showWorkingIndicator: true,
        dropdownHeader: true,
        headerOptions: {
            enum: {
                multiple: true
            }
        },
        bodyScroll: false,
        bodyScrollAdjustEvent: 'policy-rules-adjust-done',
        orderDisplayMode: 'highlight-column'
    };

    $scope.addHref = routeHelper.getHref('policy-rule-add');

    $scope.options = {
        "options": {
            "checkboxes": false,
            "singleSelection": false,
            "autoUpdate": 30000,
            "forceServerProcessing": true,
            "defaultOrdering": {
                "columnName": "created",
                "order": "desc"
            }
        },
        "columns": [
            {
                "id": "id",
                "hidden": true,
                "primary": true
            }, {
                "id": "policy_data.disableReason",
                "hidden": true
            }, {
                "id": "state",
                "text": "Status",
                "converter": "ruleState",
                "sortable": {
                    "original": true
                },
                "filter": {
                    "name": "filterEnum_state",
                    "type": "enum"
                },
                "property": {
                    "variants": policyRuleConfig.ruleStates()
                }
            }, {
                "id": "saas",
                "text": "SaaS",
                "converter": "saasConverter",
                "sortable": {
                    "original": true
                },
                "filter": {
                    "name": "filterEnum_saas",
                    "type": "enum"
                },
                "dataClass": "rule-saas"
            }, {
                "id": "name",
                "text": "Rule Name",
                "sortable": {
                    "original": true
                },
                "filter": {
                    "name": "filterBy_policy_name",
                    "type": "text"
                },
                "href": {
                    "type": "policy-rule-edit",
                    "params": {
                        "id": "id"
                    }
                }

            }, {
                "id": "_resolved",
                "hidden": true
            }, {
                "id": "identity",
                "text": "Scope",
                "converter": "ruleIdentityConverter",
                "dataClass": "rule-scopes"
            }, {
                "id": "_resolved.security_events_count",
                "text": "Events",
                "style": {
                    "text-align": "center"
                },
                "dataStyle": {
                    "font-size": "14px",
                    "font-weight": "700",
                    "color": "#333",
                    "text-align": "center"
                }
            }, {
                "id": "actions",
                "text": "Remediation workflow",
                "converter": "workflowConverter"
            }, {
                "id": "rule",
                "hidden": true
            }
        ]
    };

    function setVariants(columnId, variants) {
        _($scope.options.columns).find(function(column) {
            return column.id == columnId;
        }).property = {
            variants: variants
        };
    }

    $q.all([
        objectTypeDao.retrieve(undefined, true)
    ]).then(function(responses) {
        $scope.sources = responses[0].data.sources;
        $scope.sourcesInfo = responses[0].data.sourcesInfo;
        $scope.saasList = responses[0].data.saasList;

        setVariants('saas', _($scope.saasList).map(function(saas, id) {
            return {
                id: id,
                label: saas.label
            };
        }));

        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
            var filterParams = _(args.columnsFilters).chain().map(function(value, name) {
                var first = _(value).first();
                if(first.type == 'text' && first.operator == 'contains') {
                    return [name, first.data];
                }
                if(first.type == 'enum' && first.operator == 'is') {
                    return [name, _(value).map(function(v) {
                        return v.data;
                    }).join(',')];
                }
                if (first.type === 'boolean' && first.operator === 'is') {
                    return [name, first.data];
                }
                return null;
            }).compact().object().value();
            if (!filterParams.filterEnum_state) {
                filterParams.filterEnum_state = 'running,pause';
            }
            return policyRulesDao.list()
                .params(filterParams)
                .pagination(args.offset, args.limit)
                .order(args.ordering.columnName, args.ordering.order)
                .preFetch(policyRulesDao.count().params(filterParams).params({'filterEnum_state': 'running'}), 'runningCount')
                .converter(function (input, preFetches) {
                    try {
                        $scope.runningCount = preFetches.runningCount.data.total_rows;
                    } catch (e) {}
                    return $q.when(input);
                }, _.identity).converter(defaultListConverter, {
                    columns: $scope.options.columns,
                    converters: {
                        ruleState: function (text, columnId, columnsMap) {
                            return '#' + JSON.stringify({
                                    "display": "text",
                                    "class": "rule-state state-" + text,
                                    "text": {
                                        "running": "Running",
                                        "pause": "Stopped",
                                        "disabled": "Disabled",
                                        "mark_for_deletion": "Marked for deletion"
                                    }[text]
                                });
                        },
                        saasConverter: function (text) {
                            var cloudApp = modulesManager.cloudApp(text);
                            if (!cloudApp) {
                                return '';
                            }
                            return '#' + JSON.stringify({
                                    display: 'img',
                                    path: cloudApp.img
                                }) + cloudApp.title;
                        },
                        actionsConverter: function (text) {
                            return '';
                        },
                        ruleIdentityConverter: function (text, columnId, columnsMap) {
                            function formatText (text) {
                                return '#' + JSON.stringify({
                                        display: 'linked-text',
                                        'class': 'rule-identity-item',
                                        text: text
                                    });
                            }
                            if (_(text).isEmpty()) {
                                return formatText(policyRuleConfig.formatIdentity({
                                    label: 'All Users',
                                    type: 'users'
                                }));
                            } else {
                                return _(policyRuleConfig.identitiesToList(columnsMap)).map(function (item) {
                                    return formatText(policyRuleConfig.formatIdentity(item));
                                }).join('');
                            }
                        },
                        workflowConverter: function (text, columnId, columnsMap) {
                            var result = '';
                            if (columnsMap.actions) {
                               _(columnsMap.actions).each(function (actions) {
                                   _(actions).each(function (action) {
                                       var actionId = action.id;
                                       if (!actionId) {
                                           return;
                                       }
                                       if (!columnsMap._resolved.actions || !columnsMap._resolved.actions[actionId] ||
                                           !columnsMap._resolved.actions[actionId].label) {
                                           return;
                                       }
                                       result += '#' + JSON.stringify({
                                               display: 'text',
                                               'class': 'display-block',
                                               text: columnsMap._resolved.actions[actionId].label
                                           });
                                   });
                               });
                            }
                            return result;
                        }
                    }
                });
        }, function(tableModel) {
            $scope.tableModel = tableModel;
            $scope.tableOptions.bottomInfoText = '#' + JSON.stringify({
                    display: 'text',
                    'class': 'total-word',
                    text: 'Total'
                }) + ' ' + $scope.tableHelper.getTotal() + ' Rules';
            if (!_($scope.runningCount).isUndefined()) {
                $scope.tableOptions.bottomInfoText += ' / ' + $scope.runningCount + ' Running';
            }
        });

    });



});