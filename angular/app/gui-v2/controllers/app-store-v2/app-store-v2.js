var m = angular.module('gui-v2');

m.controller('AppStoreV2Controller', function ($scope, $route, appsDao, path, licenseOptions, modulePropsToOptions, utilityDao, sideMenuManager, modals, appConfigurator, appOperationHelper, authorizationLinkHelper, modulesDao, troubleshooting) {

    var catsFilter, opButtonAsIcon, activeGroups, topCategories;
    if ($route.current.accessor === 'cloudApps') {
        catsFilter = { family: "saas" };
        opButtonAsIcon = false;
        activeGroups = true;
        topCategories = true;
    } else {
        catsFilter = { family: "security" };
        opButtonAsIcon = true;
        activeGroups = false;
        topCategories = true;
    }

    var imgPath = path('app-store');
    var img2Path = path('gui-v2');
    var img2Prefix = 'apps-logos/';

    var stateWeight = {
        active: 0,
        enabled: 0,
        nonactive: 1,
        disabled: 1,
        display_only: 2
    };


    function loadData () {
        return appsDao.getApps().then(function (response) {
            var categoriesMap = {};
            var topCategoriesMap = {};
            $scope.topCategories = _(response.data.topCategories).chain().map(function (cat) {
                topCategoriesMap[cat.name] = cat;
                cat.categories = [];
                return cat;
            }).sortBy('order').value();
            topCategoriesMap.noTop = {
                label: '',
                name: 'noTop',
                categories: []
            };
            $scope.topCategories.push(topCategoriesMap.noTop);

            $scope.categories = _(response.data.apps_categories.categories).chain().filter(function (cat) {
                if (catsFilter.family && cat.family !== catsFilter.family) {
                    return false;
                }
                categoriesMap[cat.categoryId] = cat;
                (topCategoriesMap[cat.topCategory || 'noTop'] || topCategoriesMap.noTop).categories.push(cat);
                cat.activeApps = [];
                cat.inactiveApps = [];
                cat.allApps = [];
                if (activeGroups) {
                    cat.groups = [{
                        title: 'Active ',
                        apps: cat.activeApps
                    }, {
                        title: 'Inactive ',
                        apps: cat.inactiveApps
                    }];
                } else {
                    cat.groups = [{
                        title: '',
                        apps: cat.allApps,
                        activeApps: cat.activeApps
                    }];
                }
                return true;
            }).sortBy(function (cat) {
                return parseInt(cat.appStoreOrder);
            }).value();

            _(response.data.apps_items.modules).chain().filter(function (app) {
                return categoriesMap[app.category_id] && !app.hidden && (!app.gui_hidden || troubleshooting.entityDebugEnabled());
            }).sortBy(function (app) {
                return parseInt(app.app_store_order);
            }).sortBy(function (app) {
                if (app.options && app.options.license === 'contact_support') {
                    return 6;
                }
                if (!app.license_info) {
                    return 5;
                }
                if (app.license_info.license_type === 'paid') {
                    if (app.isExpired) {
                        return 2;
                    }
                    return 1;
                }
                if (app.license_info.license_type === 'demo') {
                    if (app.isExpired) {
                        return 4;
                    }
                    return 3;
                }
                return 5;
            }).sortBy(function (app) {
                return stateWeight[app.state];
            }).each(function (app) {
                var cat = categoriesMap[app.category_id];
                app.category = cat;
                if (app.app_store2_logo) {
                    app.imgSrc = img2Path.img(img2Prefix + app.app_store2_logo);
                } else {
                    app.imgSrc = imgPath.img(app.app_store_icon);
                }
                app.UIoptions = modulePropsToOptions(app, { licenseButton: {} }, true);
                setAppButtons(app);
                cat.allApps.push(app);
                if (app.state === 'active') {
                    cat.activeApps.push(app);
                } else {
                    cat.inactiveApps.push(app);
                }
            });
            $scope.topCategories = _($scope.topCategories).filter(function (topCat) {
                topCat.categories = _(topCat.categories).filter(function (cat) {
                    return _(cat.groups).find(function (group) {
                        return group.apps.length;
                    });
                });
                return topCat.categories.length;
            });
        });
    }

    function setAppButtons (app) {
        app.buttons = [];
        if (app.state === 'display_only') {
            app.buttons.push({
                label: 'Notify Me When Available',
                execute: function () {
                    return notifyWhenAvailable(app);
                }
            });
            return;
        }
        if (app.license === 'contact_support') {
            app.buttons.push({
                label: 'Contact Support',
                execute: function () {
                    return licenseOptions.contactSupportDialog(app.UIoptions);
                }
            });
            return;
        }

        if (appConfigurator.configAvailable(app.UIoptions)) {
            app.buttons.push({
                label: 'Configure',
                execute: function () {
                    return configure(app);
                }
            });
        }
        if (opButtonAsIcon) {
            var licenseButton = licenseOptions.button(app.UIoptions);
            if (licenseButton.enabled) {
                app.buttons.push({
                    label: 'License',
                    execute: licenseButton.execute
                });
            }
        }

        var operation = getOperation(app);
        if (operation === 'hide') {
            return;
        }
        app.opButton = {
            inProgress: function () {
                return appOperationHelper.operationInProgress(app.name);
            },
            label: (operations[operation] || operations.default).label || operation,
            btnClass: 'op-btn op-' + operation,
            iconClass: (operations[operation] || operations.default).iconClass || '',
            asIcon: opButtonAsIcon,
            execute: function () {
                if (app.opButton.inProgress()) {
                    return;
                }
                return executeOperation(app);
            }
        };
        if (!opButtonAsIcon) {
            app.buttons.push(app.opButton);
        }
    }

    var configDialogShown = false;
    function configure (app) {
        if (configDialogShown) {
            return;
        }
        configDialogShown = true;
        return appConfigurator.configure(app.UIoptions).then(function() {
            loadData();
        }).finally(function () {
            configDialogShown = false;
        });
    }

    $scope.$on('configure-app', function (event, args) {
        var foundApp;
        _($scope.categories).find(function (cat) {
            return _(cat.groups).find(function (group) {
                return _(group.apps).find(function (app) {
                    if (app.name === args.app) {
                        foundApp = app;
                    }
                });
            });
        });
        if (foundApp) {
            configure(foundApp);
        }
    });


    function notifyWhenAvailable (app) {
        var msg = '';
        var subject = 'Request to notify when \'coming soon\' app is available';
        msg += '<p>This is automatically generated message.<br />User pressed \'Notify me when available\' button</p>';
        msg += '<table boder="0">';
        msg += '<tr><td>Application name</td><td>' + app.name + '</td></tr>';
        msg += '<tr><td>Application title</td><td>' + app.label + '</td></tr>';
        msg += '<tr><td>Domain</td><td>' + window.location.host + '</td></tr>';
        msg += '<tr><td>User</td><td>' + sideMenuManager.currentUser().email + '</td></tr>';
        msg += '<tr><td>Full page url</td><td>' + window.location.href + '</td></tr>';
        msg += '</table>';
        utilityDao.sendSupportMessage({
            subject: subject,
            body: msg
        }).then(function () {
            modals.alert('This application is coming soon! We will notify you when it becomes available.', {title: ' '});
        });
    }

    function getOperation (app) {
        if (!app.options ||
            !app.name ||
            !app.options.operation ||
            app.options.operation === 'none'
        ) {
            return 'hide';
        } else {
            return app.options.operation;
        }
    }

    function executeOperation (app) {
        if (app.is_saas &&
            !app.saas_authorized &&
            app.options.operation === 'start' &&
            appConfigurator.showConfigurationLink(app.UIoptions)
        ) {
            authorizationLinkHelper.openLink(app.UIoptions);
            return;
        }
        var pages = [{
            lastPage: true,
            okButton: app.opButton.label,
            params: [{
                type: 'message',
                message: '#' + JSON.stringify({
                    display: 'text',
                    text: 'Are you sure you want to ' + app.opButton.label +' ' + app.label + '?'
                }),
                paramClass: 'full-width-param'
            }]
        }];

        if (app.options.operation === 'start') {
            pages[0].params.push({
                id: 'start_matrix_policy',
                type: 'boolean',
                rightLabel: 'Enable matrix queries',
                data: true
            });
        }

        function refresh () {
            return loadData().then(function () {
                appOperationHelper.operationEnd(app.name);
            });
        }

        modals.params(pages, app.opButton.label + ' ' + app.label).then(function (params) {
            appOperationHelper.operationStart(app.name);
            return modulesDao.operation(app.name, app.options.operation, params).then(refresh, refresh);
        });
    }

    var operations = {
        'start': {
            iconClass: 'fa-play',
            label: 'Start'
        },
        'stop': {
            iconClass: 'fa-pause',
            label: 'Stop'
        },
        'default': {
            iconClass: 'fa-question'
        }
    };




    loadData();
});