var m = angular.module('gui-v2');

m.controller('PolicyRuleEditController', function ($scope, $q, $routeParams, $route, policyDao, modulesManager, routeHelper, appsDao, policyRuleConfig, policyRulesDao, errorStatusHolder, modals, severityTypes, modulesDao) {
    var saasApps = [];
    var securityApps = [];
    var categoriesMap = {};
    $scope.loading = true;

    $scope.cancelHref = routeHelper.getHref('policy-rules');
    $scope.ruleStates = policyRuleConfig.ruleStates();
    $scope.workflows = policyRuleConfig.workflows();
    $scope.severityTypes = severityTypes.data;
    var defaultSeverity = 3;


    $scope.model = {};
    $scope.internal = {
        selectedApps: {},
        workflows: {},
        actions: {}
    };

    function loadApps () {
        return appsDao.getApps().then(function (response) {
            categoriesMap = _(response.data.apps_categories.categories).chain().map(function (category) {
                return [category.categoryId, category];
            }).object().value();

            saasApps = _(response.data.apps_items.modules).chain().filter(function (app) {
                return app.state !== 'display_only' && app.is_saas;
            }).sortBy('security_stack_order').sortBy(function (app) {
                return app.state === 'active' ? 1 : 2;
            }).value();
            securityApps = _(response.data.apps_items.modules).chain().filter(function (app) {
                return app.state !== 'display_only' && !app.is_saas && !app.hidden_from_rules;
            }).sortBy('security_stack_order').value();

            $scope.activeAppsBySecType = {};
            _(['av', 'ap']).each(function (secType) {
                $scope.activeAppsBySecType[secType] = _(securityApps).filter(function (app) {
                    return app.sec_type === secType && app.state === 'active';
                });
            });

            $scope.saasList = _(saasApps).map(function (app) {
                return {
                    id: app.name,
                    label: app.label,
                    disabled: app.state !== 'active'
                };
            });
        });
    }

    function loadActions (saas) {
        return policyRulesDao.ruleActions(saas).then(function (response) {
            $scope.actions = response.data;
            $scope.internal.actions = {};
        });
    }
    function loadDlpRules () {
        return policyRulesDao.dlpRules().then(function (response) {
            $scope.dlpRules = _(response.data.normalized_rules).map(function (rule) {
                return {
                    id: rule,
                    label: rule
                };
            });
        });
    }

    function loadWorkflows (saas) {
        return modulesDao.retrieveConfig(saas).then(function (response) {
            var config = response.data;
            _($scope.workflows).each(function (workflow) {
                if (config[workflow.id] && config[workflow.id].variants && config[workflow.id].variants.length) {
                    workflow.variants = config[workflow.id].variants;
                    var defaultVariant = _(workflow.variants).find(function (variant) {
                        return variant.id === config[workflow.id].value;
                    }) || workflow.variants[0];
                    defaultVariant.default = true;
                }
            });
        });
    }


    if ($route.current.mode === 'create') {
        $scope.mode = 'create';
        $scope.step = 'create';
        $scope.pageTitlePrefix = 'New Policy Rule';

        loadApps().then(function () {
            $scope.loading = false;
            $scope.internal.saasSelectorOpen = true;
        });

    } else {
        $scope.mode = 'edit';
        $scope.pageTitlePrefix = 'Edit Rule';
        $scope.pageTitle = $scope.pageTitlePrefix;
        if (!$routeParams.id) {
            errorStatusHolder.errorStatus(404);
            return;
        }
        $scope.ruleId = $routeParams.id;
        $q.all([
            policyRulesDao.retrieve($scope.ruleId),
            loadApps()
        ]).then(function (responses) {
            var rule = responses[0].data;
            $scope.model.saas = rule.saas;
            $scope.model.ruleType = policyRuleConfig.localRuleType(rule.saas, rule.type);

            var saasApp = _(saasApps).find(function (app) {
                return app.name === $scope.model.saas;
            });
            $scope.ruleTypeConfig = policyRuleConfig.ruleTypeConfig($scope.model.ruleType, securityApps);
            if (!saasApp || !$scope.ruleTypeConfig) {
                errorStatusHolder.errorStatus(500);
                return;
            }
            if (!$scope.ruleTypeConfig.activeSecTypes) {
                modals.alert('No security tools required for this type of rule is running.', {
                    title: 'Error'
                }).finally(function () {
                    routeHelper.redirectTo('policy-rules');
                });
                return;
            }
            var ruleTypeName = saasApp.label + ' ' + $scope.ruleTypeConfig.label;
            $scope.pageTitle = $scope.pageTitlePrefix + ' - ' + ruleTypeName;
            var queries = [
                loadActions($scope.model.saas),
                checkScopes($scope.model.saas),
            ];
            if ($scope.ruleTypeConfig.activeSecTypes.dlp) {
                queries.push(loadDlpRules());
            }
            if ($scope.ruleTypeConfig.workflows) {
                queries.push(loadWorkflows($scope.model.saas));
            }
            return $q.all(queries).then(function () {
                $scope.loading = false;
                $scope.step = 'details';
                $scope.model.name = rule.name;
                $scope.model.state = rule.state;
                $scope.model.rule = _(rule.rule).pick('dlp_rules');
                if ($scope.scopesAvailable) {
                    var list = policyRuleConfig.identitiesToList(rule);
                    if (list && list.length) {
                        $scope.model.scopes = list;
                    } else {
                        $scope.model.all = true;
                    }
                }
                if (rule.rule.active_sectools && rule.rule.active_sectools.any) {
                    $scope.internal.selectedApps = _(rule.rule.active_sectools.any).chain().map(function (appName) {
                        return [appName, true];
                    }).object().value();
                } else {
                    $scope.internal.selectedApps = {};
                }
                _($scope.workflows).each(function (workflow) {
                    var found = _(workflow.variants).find(function (variant) {
                        return variant.id === (rule.rule.workflows && rule.rule.workflows[workflow.id]);
                    });
                    if (found) {
                        $scope.internal.workflows[workflow.id] = found.id;
                        return;
                    }
                    $scope.internal.workflows[workflow.id] = _(workflow.variants).find(function (item) {
                        return item.default;
                    }).id;
                });

                _(rule.actions).each(function (actions) {
                    _(actions).each(function (action) {
                        if (!action.id) {
                            return;
                        }
                        $scope.internal.actions[action.id] = {
                            enabled: true
                        };
                        if (action.input) {
                            $scope.internal.actions[action.id].params = {};
                            _(action.input).each(function (param, paramId) {
                                $scope.internal.actions[action.id].params[paramId] = param.value;
                            });
                        }
                    });
                });

                $scope.model.security_events = rule.security_events;
                if (!$scope.model.security_events ||
                !_($scope.severityTypes).find(function (item) {
                    return item.id === $scope.model.security_events.severity;
                })) {
                    $scope.model.security_events.severity = defaultSeverity;
                }

            });
        }, function (error) {
            errorStatusHolder.errorStatus(error.status);
        });
    }
    $scope.pageTitle = $scope.pageTitlePrefix;

    $scope.$watch('model.saas', function (newValue, oldValue) {
        if ($scope.step !== 'create') {
            return;
        }
        if (!$scope.model.saas) {
            $scope.ruleTypes = [];
        } else {
            $scope.ruleTypes = policyRuleConfig.getSaasRuleTypes($scope.model.saas, securityApps);
        }
        if (oldValue !== newValue || !newValue) {
            $scope.model.ruleType = null;
        }
    });

    $scope.$watch('internal.saasSelectorOpen', function (newValue, oldValue) {
        if (newValue === false && oldValue === true && $scope.model.saas && !$scope.model.ruleType && $scope.ruleTypes.length) {
            $scope.internal.ruleTypeSelectorOpen = true;
        }
    });

    $scope.nextDisabled = function () {
        return !$scope.model.saas || !$scope.model.ruleType;
    };

    $scope.back = function () {
        $scope.step = 'create';
    };

    $scope.next = function () {
        if ($scope.nextDisabled()) {
            return;
        }
        var saasApp = _(saasApps).find(function (app) {
            return app.name === $scope.model.saas;
        });
        $scope.ruleTypeConfig = policyRuleConfig.ruleTypeConfig($scope.model.ruleType, securityApps);
        if (!saasApp || !$scope.ruleTypeConfig) {
            return;
        }
        $scope.loading = true;
        var queries = [
            policyRulesDao.list().params({
                filterEnum_saas: $scope.model.saas,
                filterEnum_type: $scope.ruleTypeConfig.backendType
            }),
            loadActions($scope.model.saas),
            checkScopes($scope.model.saas),
        ];
        if ($scope.ruleTypeConfig.activeSecTypes.dlp) {
            queries.push(loadDlpRules());
        }
        if ($scope.ruleTypeConfig.workflows) {
            queries.push(loadWorkflows($scope.model.saas));
        }

        return $q.all(queries).then(function (responses) {
            if (responses[0].data.rows.length && !$scope.ruleTypeConfig.multiple) {
                modals.confirm({
                    title: 'Rule already exists',
                    message: 'Selected rule type already exists, do you want to edit existing rule?'
                }).then(function () {
                    routeHelper.redirectTo('policy-rule-edit', {
                        id: responses[0].data.rows[0].id
                    });
                }, function () {
                    routeHelper.redirectTo('policy-rules');
                });
                return;
            }
            $scope.loading = false;
            $scope.step = 'details';
            var ruleTypeName = saasApp.label + ' ' + $scope.ruleTypeConfig.label;
            $scope.pageTitle = $scope.pageTitlePrefix + ' - ' + ruleTypeName;
            $scope.model.name = ruleTypeName;
            $scope.model.state = 'running';
            $scope.model.all = true;
            $scope.model.security_events = {
                severity: defaultSeverity
            };
            $scope.model.rule = {};
            $scope.internal.selectedApps = {};
            if ($scope.ruleTypeConfig.activeSecTypes.dlp) {
                $scope.model.rule.dlp_rules = policyRuleConfig.allDlpRules($scope.dlpRules);
            }
            _($scope.activeAppsBySecType).each(function (apps, secType) {
                if ($scope.ruleTypeConfig.activeSecTypes[secType]) {
                    _(apps).each(function (app) {
                        $scope.internal.selectedApps[app.name] = true;
                    });
                }
            });
            _($scope.workflows).each(function (workflow) {
                $scope.internal.workflows[workflow.id] = _(workflow.variants).find(function (item) {
                    return item.default;
                }).id;
            });
        });
    };

    $scope.scopes = {
        available: {
            getItems: function (offset, limit, filter, timeoutPromise) {
                var grouped = {};
                _($scope.model.scopes).each(function (item) {
                    grouped[item.type] = grouped[item.type] || [];
                    grouped[item.type].push(item);
                });
                var params = {};
                _(grouped).each(function (items, key) {
                    params['exclude_' + key + '_ids'] = _(items).pluck('id').join(',');
                });
                return policyDao.scopes($scope.model.saas)
                    .order('order')
                    .params(params)
                    .filter(filter)
                    .timeout(timeoutPromise)
                    .pagination(offset, limit);
            },
            doubleClicked: function () {
                $scope.scopesAdd();
            },
            formatItem: policyRuleConfig.formatIdentity
        },
        selected: {
            getItems: function (offset, limit) {
                return $q.when({
                    data: {
                        rows: $scope.model.scopes && $scope.model.scopes.length && _($scope.model.scopes)
                            .chain().rest(offset).first(limit).map(function (item) {
                                return item;
                            }).value() || []
                    }
                });
            },
            doubleClicked: function () {
                $scope.scopesRemove();
            },
            formatItem: policyRuleConfig.formatIdentity
        }
    };

    $scope.scopesAdd = function () {
        var items = $scope.scopes.available.getSelected();
        if (items.length) {
            $scope.model.scopes = $scope.model.scopes || [];
            _(items).each(function (item) {
                $scope.model.scopes.push(item);
            });
            $scope.model.scopes = _($scope.model.scopes).sortBy('order');
            $scope.scopes.available.reload();
            $scope.scopes.selected.reload();
        }
    };

    $scope.scopesRemove = function () {
        var items = $scope.scopes.selected.getSelected();
        if (items.length) {
            var itemsMap = _(items).reduce(function (memo, item) {
                memo[item.type] = memo[item.type] || {};
                memo[item.type][item.id] = true;
                return memo;
            }, {});
            $scope.model.scopes = _($scope.model.scopes).chain().filter(function (item) {
                return !itemsMap[item.type] || !itemsMap[item.type][item.id];
            }).sortBy('order').value();
            $scope.scopes.available.reload();
            $scope.scopes.selected.reload();
        }
    };

    $scope.scopesRemoveAll = function () {
        $scope.model.scopes = [];
        $scope.scopes.available.reload();
        $scope.scopes.selected.reload();
    };

    function checkScopes(saas) {
        $scope.scopesAvailable = false;
        return policyDao.scopes(saas).pagination(0, 1).then(function (response) {
            if (response && response.data && response.data.rows && response.data.rows.length) {
                $scope.scopesAvailable = true;
            } else {
                $scope.scopesAvailable = false;
                $scope.model.all = true;
            }
        }, function (error) {
            $scope.scopesAvailable = false;
            $scope.model.all = true;
            return $q.when();
        }).then(_.noop, _.noop);
    }

    function saveRule(rule) {
        if ($scope.mode === 'create') {
            return policyRulesDao.create(rule);
        } else {
            return policyRulesDao.update($scope.ruleId, rule);
        }
    }

    $scope.malwareAdvancedVisible = function () {
        return !!_($scope.workflows).find(function (workflow) {
            return workflow.advanced && $scope.workflowAvailable(workflow);
        });
    };

    $scope.workflowAvailable = function (workflow) {
        return $scope.ruleTypeConfig.workflows && secTypeSelected(workflow.secType);
    };

    function secTypeSelected (secType) {
        if ($scope.ruleTypeConfig.alwaysOnSecTypes && $scope.ruleTypeConfig.alwaysOnSecTypes[secType]) {
            return true;
        }
        return !!_($scope.activeAppsBySecType[secType]).find(function (app) {
            return $scope.internal.selectedApps[app.name];
        });
    }

    $scope.actionAvailable = function (action) {
        return !!_(action.security_types).find(function (secType) {
            return secTypeSelected(secType);
        });
    };

    $scope.actionsAvailable = function () {
        return !!_($scope.actions).find(function (action) {
            return $scope.actionAvailable(action);
        });
    };

    $scope.save = function () {
        var selectedSecTypes = _($scope.ruleTypeConfig.sec_types).filter(function (secType) {
            return secTypeSelected(secType);
        });
        var rule = {
            "name": $scope.model.name,
            "state": $scope.model.state,
            "saas": $scope.model.saas,
            "type": policyRuleConfig.backendRuleType($scope.model.ruleType),
            "mode": "normal",
            "security_events": $scope.model.security_events,
            "rule": $scope.model.rule,
            "actions": _($scope.actions).chain().filter(function (action) {
                    return $scope.actionAvailable(action) && $scope.internal.actions[action.id] && $scope.internal.actions[action.id].enabled;
                }).reduce(function (memo, actionInfo) {
                    var action = angular.copy(_(actionInfo).pick('id', 'data'));
                    if (actionInfo.input) {
                        action.input = {};
                        _(actionInfo.input).each(function (param, paramId) {
                            action.input[paramId] = {
                                value: $scope.internal.actions[action.id].params &&
                                $scope.internal.actions[action.id].params[paramId] || ''
                            };
                            _(action.data.attributes).each(function (attribute, attributeId) {
                                action.data.attributes[attributeId] = attribute.replace(new RegExp('\\{' + paramId + '\\}', 'g'), action.input[paramId].value);
                            });
                        });
                    }
                    if (!actionInfo.security_types || !actionInfo.security_types.length || !_($scope.ruleTypeConfig.sec_types).difference(actionInfo.security_types).length) {
                        memo.all.push(action);
                    } else {
                        memo[_(actionInfo.security_types).intersection(selectedSecTypes)[0]].push(action);
                    }
                    return memo;
                }, $.extend(_($scope.ruleTypeConfig.sec_types).chain().map(function (secType) {
                    return [secType, []];
                }).object().value(), {
                    all: []
                })).value(),
            "tags": []
        };
        if ($scope.model.all) {
            rule.identity = {};
        } else {
            rule.identity = _($scope.model.scopes).reduce(function (memo, item) {
                memo[item.type] = memo[item.type] || {
                        type: item.entity_type,
                        items: []
                    };
                memo[item.type].items.push(item.id);
                return memo;
            }, {});
        }
        var selectedApps = [];
        _($scope.activeAppsBySecType).each(function (apps, secType) {
            if ($scope.ruleTypeConfig.activeSecTypes[secType]) {
                _(apps).each(function (app) {
                    if ($scope.internal.selectedApps[app.name]) {
                        selectedApps.push(app.name);
                    }
                });
            }
        });
        if (selectedApps.length) {
            rule.rule.active_sectools = {
                any: selectedApps
            };
        }
        var workflows = {};
        _($scope.workflows).each(function (workflow) {
            if ($scope.workflowAvailable(workflow)) {
                workflows[workflow.id] = $scope.internal.workflows[workflow.id];
            }
        });
        if (!_(workflows).isEmpty()) {
            rule.rule.workflows = workflows;
        }

        $scope.loading = true;
        saveRule(rule).then(function (response) {
            routeHelper.redirectTo('policy-rules');
        });
    };

});