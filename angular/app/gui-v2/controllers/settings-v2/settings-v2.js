var m = angular.module('gui-v2');

m.controller('SettingsControllerV2', function($scope, avananUserDao, modals, friendlyDomainsDao, localModuleDao, commonDao, sideMenuManager, configDao, objectTypeDao, fileDao, $location, troubleshooting, entityTypes, $q, fileSecurityDebugHelper, idleManager, modulesManager, feature, routeHelper, emailReportDao, modulesDao, $timeout, $route) {
    $scope.changePasswordPanel = {
        title: 'Change Password',
        'class': 'light white-body white-header no-margin',
        bodyStyle: {
            height: '175px'
        }
    };
    $route.current.title = "Settings";

    $scope.changePasswordModel = {
        oldPassword: '',
        newPassword: '',
        repeatPassword: ''
    };
    if (feature.emailDomainsManagement) {
        $scope.domainsManageLink = routeHelper.getPath('email-domains');
    }

    $scope.error = {};
    $scope.changePassword = function() {
        $scope.error = {};
        if(!$scope.changePasswordModel.oldPassword.length) {
            $scope.error.oldPassword = 'Password should not be empty';
            return;
        }
        if(!$scope.changePasswordModel.newPassword.length) {
            $scope.error.newPassword = 'Password should not be empty';
            return;
        }
        if($scope.changePasswordModel.newPassword !== $scope.changePasswordModel.repeatPassword) {
            $scope.error.repeatPassword = 'Passwords are not same';
            return;
        }
        avananUserDao.changePassword($scope.changePasswordModel.oldPassword, $scope.changePasswordModel.newPassword).then(function() {
            modals.alert('Password changes successfully');
        }, function(error) {
            $scope.error.oldPassword = error.data.message;
            $scope.error.newPassword = error.data.message;
        });
    };

    $scope.friendlyDomainsModel = {
        domains: []
    };

    friendlyDomainsDao.list().then(function(domains) {
        $scope.friendlyDomainsModel.domains = domains.data;

        $scope.friendlyDomainsPanel = {
            title: 'Friendly domains',
            'class': 'light white-body white-header no-margin',
            bodyStyle: {
                height: '175px'
            }
        };

        $scope.saveDomains = function() {
            friendlyDomainsDao.update($scope.friendlyDomainsModel.domains, true);
        };
    });

    $scope.advancedPanel = {
        title: 'Advanced',
        'class': 'light white-body white-header no-margin',
        bodyStyle: {
            height: '360px'
        }
    };

    localModuleDao.retrieve().then(function (response) {
        if (!response.data || _(response.data).isEmpty()) {
            return;
        }

        $scope.updateLocalModules = function () {
            var params = [{
                type: 'selector',
                id: 'module',
                label: 'Choose module',
                variants: _(response.data).map(function (val, key) {
                    return {
                        id: key,
                        label: key
                    };
                }),
                validation: {
                    required: {
                        message: 'Please select module'
                    }
                }
            }];
            var modulesArr = [];
            var entitiesArr = [];
            _(response.data).each(function (val, key) {
                modulesArr.push(key);
                params.push({
                    type: 'selector',
                    id: 'entity_id',
                    label: 'Choose entity',
                    variants: _(val.entities).map(function (info, entity_id) {
                        entitiesArr.push(entity_id);
                        return {
                            id: entity_id,
                            label: info.label
                        };
                    }),
                    enabled: {
                        param: 'module',
                        equal: key
                    },
                    validation: {
                        required: {
                            message: 'Please select entity'
                        }
                    }
                });
            });

            params.push({
                id: 'logfile',
                type: 'file',
                label: 'Choose a file',
                validation: {
                    required: {
                        message: 'Please select file'
                    }
                },
                enabled: [{
                    param: 'module',
                    in: modulesArr
                }, {
                    param: 'entity_id',
                    in: entitiesArr
                }]
            });

            modals.params([{
                lastPage: true,
                okButton: 'Upload',
                params: params
            }], 'Upload configuration file').then(function (data) {
                localModuleDao.uploadCsv(data.module, data.entity_id, data.logfile).then(function (response) {
                    if (response.data.success) {
                        modals.alert('Successfully uploaded', {title: 'Success'});
                    } else {
                        modals.alert('Error uploading file', {title: 'Error'});
                    }
                }, function (error) {
                    modals.alert('Error uploading file', {title: 'Error'});
                });
            });
        };
    });

    $scope.uploadCustomIcon = function () {
        var params = [{
            type: 'message',
            message: '#' + JSON.stringify({
                display: 'text',
                text: 'For best result use jpeg or png image'
            }),
            paramClass: 'full-width-param'
        }, {
            id: 'file',
            type: 'file',
            label: 'Choose a file',
            validation: {
                required: {
                    message: 'Please select file'
                }
            }
        }];

        modals.params([{
            lastPage: true,
            okButton: 'Upload',
            params: params
        }], 'Upload custom logo').then(function (data) {
            commonDao.uploadCustomIcon(data.file).then(function (response) {
                if (response.data.success) {
                    sideMenuManager.customIcon(true);
                    modals.alert('Successfully uploaded', {title: 'Success'});
                } else {
                    modals.alert('Error uploading file', {title: 'Error'});
                }
            }, function (error) {
                modals.alert('Error uploading file', {title: 'Error'});
            });
        });
    };

    $scope.removeCustomIcon = function () {
        modals.confirm({
            message:'Are you sure you want to remove custom logo?',
            title: 'Remove custom logo'
        }).then(function () {
            commonDao.removeCustomIcon().then(function (response) {
                if (!response.data.success) {
                    return removeError(response.data.message);
                }
                sideMenuManager.customIcon(true);
            }, function (error) {
                removeError(error.data && error.data.message);
            });
        });
    };

    function removeError(message) {
        return modals.alert(message || 'Unknown error removing custom logo', {title: 'Error'});
    }

    if ($location.search().debug === '1') {
        $scope.entityDebugEnabled = troubleshooting.entityDebugEnabled(true);
        sideMenuManager.updateMenus();
    } else {
        if ($location.search().debug === '0') {
            $scope.entityDebugEnabled = troubleshooting.entityDebugEnabled(false);
            sideMenuManager.updateMenus();
        }
        $scope.entityDebugEnabled = troubleshooting.entityDebugEnabled();
    }

    if (sideMenuManager.currentUser().view_private_data) {
        $scope.showDownloadConfirmationParam = true;
    }

    if ($scope.entityDebugEnabled) {
        $scope.sendEmailReport = function() {
            emailReportDao.trigger('office365-weekly').then(function() {
                modals.alert('Report will be send in few minutes');
            });
        };

        objectTypeDao.retrieve().then(function(response) {
            $scope.entityDebugPanel = {
                title: 'Entity Debug',
                'class': {
                    "light": true,
                    "white-body": true,
                    "white-header": true,
                    "no-margin": true
                },
                bodyStyle: {
                    'min-height': '500px'
                },
                logic: {
                    "name": "debugEntityLogic"
                },
                objectTypes: response
            };
            $scope.entityDebugPanel.wrapper = $scope.entityDebugPanel;
        });


        $scope.debugLinksPanel = {
            title: 'Debug links',
            'class': 'light white-body white-header no-margin',
            bodyStyle: {
                height: '360px'
            },
            links: ['#' + JSON.stringify({
                display: 'link',
                type: 'sec-app-stats',
                text: 'Security applications statistics'
            }), '#' + JSON.stringify({
                display: 'link',
                type: 'main-queue-stats',
                text: 'Main queue statistics'
            }), '#' + JSON.stringify({
                display: 'link',
                type: 'download-history',
                text: 'Download history'
            }), '#' + JSON.stringify({
                display: 'link',
                type: 'entity-trend',
                text: 'Entity trend'
            }), '#' + JSON.stringify({
                display: 'link',
                type: 'entity-per-user',
                text: 'Entity per user'
            }), '#' + JSON.stringify({
                display: 'link',
                type: 'scan-monitor',
                text: 'Scan monitor'
            }), '#' + JSON.stringify({
                display: 'link',
                type: 'licenses',
                text: 'Licenses'
            }), '#' + JSON.stringify({
                display: 'link',
                type: 'health-check',
                text: 'Health Check'
            }), '#' + JSON.stringify({
                display: 'link',
                type: 'learning-mode-state',
                text: 'Learning Mode State'
            })]
        };
        if (modulesManager.isActive('office365_emails')) {
            $scope.debugLinksPanel.links.push('#' + JSON.stringify({
                    display: 'link',
                    type: 'inline-messages',
                    module: 'office365_emails',
                    text: 'Office365 Inline Messages'
                }));
            $scope.debugLinksPanel.links.push('#' + JSON.stringify({
                    display: 'link',
                    type: 'viewer',
                    module: 'office365_emails',
                    id: 'top-email-owners',
                    text: 'Office365 Emails Top Email Owners'
                }));
            $scope.debugLinksPanel.links.push('#' + JSON.stringify({
                    display: 'link',
                    type: 'viewer',
                    module: 'office365_emails',
                    id: 'top-senders',
                    text: 'Office365 Emails Top Senders'
                }));
            $scope.debugLinksPanel.links.push('#' + JSON.stringify({
                    display: 'link',
                    type: 'viewer',
                    module: 'office365_emails',
                    id: 'suspicious-subjects',
                    qs: {
                        phishing: 'suspicious'
                    },
                    text: 'Office365 Emails Suspicious Subjects'
                }));
            $scope.debugLinksPanel.links.push('#' + JSON.stringify({
                    display: 'link',
                    type: 'viewer',
                    module: 'office365_emails',
                    id: 'suspicious-subjects',
                    qs: {
                        phishing: 'phishing'
                    },
                    text: 'Office365 Emails Phishing Subjects'
                }));

        }
        if (modulesManager.isActive('google_mail')) {
            $scope.debugLinksPanel.links.push('#' + JSON.stringify({
                    display: 'link',
                    type: 'inline-messages',
                    module: 'google_mail',
                    text: 'Gmail Inline Messages'
                }));
            $scope.debugLinksPanel.links.push('#' + JSON.stringify({
                    display: 'link',
                    type: 'viewer',
                    module: 'google_mail',
                    id: 'top-email-owners',
                    text: 'GMail Top Email Owners'
                }));
            $scope.debugLinksPanel.links.push('#' + JSON.stringify({
                    display: 'link',
                    type: 'viewer',
                    module: 'google_mail',
                    id: 'top-senders',
                    text: 'Gmail Top Senders'
                }));
            $scope.debugLinksPanel.links.push('#' + JSON.stringify({
                    display: 'link',
                    type: 'viewer',
                    module: 'google_mail',
                    id: 'suspicious-subjects',
                    qs: {
                        phishing: 'suspicious'
                    },
                    text: 'Gmail Suspicious Subjects'
                }));
            $scope.debugLinksPanel.links.push('#' + JSON.stringify({
                    display: 'link',
                    type: 'viewer',
                    module: 'google_mail',
                    id: 'suspicious-subjects',
                    qs: {
                        phishing: 'phishing'
                    },
                    text: 'Gmail Phishing Subjects'
                }));
        }

        if (feature.hardReset) {
            $scope.showHardReset = true;

            $scope.performHardReset = function () {
                return modals.confirm('Are you sure you want to reset all the data?').then(function () {
                    return commonDao.hardReset();
                }).then(function () {
                    var deferred = $q.defer();
                    modals.widget({
                        panel: {
                            "type": "info",
                            "class": "hard-reset-status-dialog",
                            "wrapper": {
                                "type": "widget-panel",
                                "title": "Resetting...",
                                "class": "avanan white-body overflow-auto"
                            },
                            lines: [{
                                text: "Performing hard reset. Please wait...",
                                "class": "text-align-center"
                            }, {
                                text: "",
                                icon: "fa-spinner fa-pulse fa-3x",
                                "class": "text-align-center",
                                "style": {"margin": "50px"}
                            }]
                        },
                        keyboard: false,
                        backdrop: 'static',
                        noDialogButtons: true
                    });
                    function checkModules() {
                        modulesDao.cloudAppsDashboard().then(function (response) {
                            var activeSaas = _(response.data.saas).find(function (saas) {
                                return saas.isSaas && saas.state === 'active';
                            });
                            if (!activeSaas) {
                                routeHelper.redirectTo('dashboard');
                            } else {
                                $timeout(checkModules, 10000);
                            }
                        });
                    }

                    checkModules();
                    return deferred.promise;
                }, function (error) {
                    modals.alert('Failed to perform hard reset', {
                        title: 'Error'
                    });
                });
            };
        }
    }

    $scope.configParams = [{
        id: 'matrix_full_scan_period_days',
        label: 'Matrix full scan period (days)',
        type: 'integer',
        min: 1,
        loaded: false,
        hidden: true
    }];
    $q.all(_($scope.configParams).chain().map(function (param) {
        if (param.hidden) {
            return false;
        }
        return configDao.param(param.id).then(function (value) {
            if (param.type === 'integer') {
                value = parseInt(value);
            }
            param.data = value;
            param.loaded = true;
        });
    }).compact().value());

    $scope.saveConfigParam = function(param) {
        return configDao.setParam(param.id, param.data).then(function () {
            if (param.onSave) {
                param.onSave();
            }
        });
    };

    $scope.params = {
        idleTimeout: idleManager.timeout()
    };

    $scope.saveIdleTimeout = function() {
        idleManager.timeout(parseInt($scope.params.idleTimeout));
    };

});