var m = angular.module('gui-v2');

m.controller('ProfileControllerV2', function($scope, $routeParams, widgetPolling, path, objectTypeDao, modulesDao, $q, defaultHttpErrorHandler, openAppStatusListener, $injector, $templateCache) {

    var widgetsScope = {
        entityType: $routeParams.entityType,
        id: $routeParams.id
    };

    var loader = 'json';
    var widgetsPath = 'entity/' + widgetsScope.entityType;
    var profileTemplate = path('gui-v2').directive('profile').template(widgetsScope.entityType + '/' + widgetsScope.entityType);
    if ($templateCache.get(profileTemplate)) {
        loader = 'template';
    }
    var commonQueries = [objectTypeDao.retrieve().then(function (response) {
        widgetsScope.objectTypes = response;
    })];
    var customJsonExists = false;
    if (loader === 'json') {
        commonQueries.push(path('gui-v2').ctrl('profile-v2').json('entity/' +widgetsScope.entityType).retrieve(true)
            .onRequestFail(function () {
                return $q.when({});
            }).then(function (response) {
                if (response.data) {
                    customJsonExists = true;
                }
            }));
    }


    $q.all(commonQueries).then(function (responses) {
        var saasByEntityType = widgetsScope.objectTypes.data &&
            widgetsScope.objectTypes.data.entities &&
            widgetsScope.objectTypes.data.entities[widgetsScope.entityType] &&
            widgetsScope.objectTypes.data.entities[widgetsScope.entityType].saas;
        if (saasByEntityType) {
            widgetsScope.module = saasByEntityType;
            $injector.get('modulesManager').currentModule(widgetsScope.module === 'office365_mgmnt' ? 'office365_emails' : widgetsScope.module); //TODO: this line is for compatibility of v2 with v1
        }
        if (!customJsonExists && !saasByEntityType) {
            return $q.reject({
                status: 404
            });
        }
        return modulesDao.entityInfoRaw(widgetsScope.module, widgetsScope.entityType)
            .then(function (rawEntityInfo) {
                widgetsScope.rawEntityInfo = rawEntityInfo;
                if (customJsonExists || loader !== 'json') {
                    return;
                }
                if (rawEntityInfo.sec_type_connected && rawEntityInfo.sec_type_connected.length) {
                    widgetsPath = 'basic-scannable-profile';
                } else {
                    widgetsPath = 'basic-profile';
                }
            });
    }).then(function () {
        if (loader === 'json') {
            widgetsScope.widgetsPath = widgetsPath;
        } else {
            widgetsScope.profileTemplate = profileTemplate;
        }
        widgetPolling.scope(widgetsScope);
        $.extend($scope, widgetsScope);
        $scope.loader = loader;

    }, defaultHttpErrorHandler);

    var destroyAppStatusListener = openAppStatusListener($scope);

    $scope.$on('$destroy', function() {
        destroyAppStatusListener();
    });
});