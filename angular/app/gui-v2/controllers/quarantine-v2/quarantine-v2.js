var m = angular.module('gui-v2');

m.controller('QuarantineControllerV2', function($scope, path, modulesManager, tablePaginationHelper, fileDao, routeHelper, objectTypeDao, $q, modals, remediationProperties, manualActionDao, sideMenuManager, entityTypes, emailDao, $route, feature, widgetSettings, columnsHelper, $routeParams) {

    $scope.saasEntities = {
        'office365_emails': [{
            type: 'email',
            label: 'Emails'
        }]
    };

    if (feature.o365emailsQuarantineAttachments) {
        $scope.saasEntities.office365_emails.push({
            type: 'file',
            label: 'Attachments'
        });
    }

    var availableSaases = ['google_drive', 'box', 'office365_emails', 'google_mail', 'sharefile', 'office365_onedrive', 'office365_sharepoint'];
    $scope.enabledSaases = _(modulesManager.activeSaasApps()).filter(function (saas) {
        return _(availableSaases).contains(saas.name);
    }).map(function (saas) {
        return {
            img: saas.img,
            name: saas.name,
            title: saas.title,
            link: routeHelper.getPath('quarantine', {
                module: saas.name
            })
        };
    });

    $scope.currentModule = $routeParams.module;
    var staticFilePostfix = '';
    if ($scope.saasEntities[$scope.currentModule]) {
        if ($routeParams.type && _($scope.saasEntities[$scope.currentModule]).find(function (entity) {
            return entity.type === $routeParams.type;
            })) {
            $scope.currentEntity = $routeParams.type;
        } else {
            $scope.currentEntity = $scope.saasEntities[$scope.currentModule][0].type;
        }
        staticFilePostfix = '.' + $scope.currentEntity;
        _($scope.saasEntities[$scope.currentModule]).each(function (entity) {
            entity.link = routeHelper.getPath('quarantine', {
                module: $scope.currentModule,
                type: entity.type
            });
        });
    }

    if ($scope.enabledSaases.length && !_($scope.enabledSaases).find(function (saas) {
            return saas.name === $scope.currentModule;
        })) {
        routeHelper.redirectTo('quarantine', {
            module: $scope.enabledSaases[0].name
        });
        return;
    }

    $scope.resultsPanel = {
        title: 'Loading...',
        'class': 'light white-body white-header',
        "bodyStyle": {
            "min-height": "500px"
        }
    };

    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 20,
        pagination: {
            page: 1,
            ordering: {}
        },
        saveState: true,
        showWorkingIndicator: true,
        disableTop: true,
        orderDisplayMode: 'highlight-column',
        dropdownHeader: true

    };

    function updateColumns(options, changeObject) {
        if ($scope.resultsPanel.uuid && $scope.resultsPanel.filters) {
            var filterParams = widgetSettings.params($scope.resultsPanel.uuid);
            var newColumns = _(options.columns).map(function (column) {
                if (column.showIf) {
                    if (filterParams[column.showIf]) {
                        delete column.hidden;
                    } else {
                        column.hidden = true;
                    }
                }
                return column;
            });
            if (changeObject) {
                options.columns = newColumns;
            }
        }
    }

    $q.all([
        objectTypeDao.retrieve(false),
        path('gui-v2').ctrl('quarantine-v2').json($scope.currentModule + '.quarantine' + staticFilePostfix).retrieve()
    ]).then(function (responses) {
        $scope.options = responses[1].data;
        $scope.options.localEntity = 'file';

        $scope.resultsPanel.filters = $scope.options.filters;
        $scope.resultsPanel.uuid = $scope.options.uuid;

        if ($scope.options.entityType) {
            $scope.options.localEntity = entityTypes.toLocal($scope.options.entityType);
            if ($scope.options.options.title) {
                $route.current.title = $scope.options.options.title;
            }
            $scope.sources = responses[0].data.sources($scope.options.entityType);
            $scope.actionTypes = responses[0].data.actionTypes[$scope.options.entityType];
            var restoreAction;
            if (_($scope.options.restoreFeature).isUndefined() || feature[$scope.options.restoreFeature]) {
                var restoreActionIds = $scope.options.restoreActionIds;
                if (!restoreActionIds && $scope.options.restoreActionId) {
                    restoreActionIds = [$scope.options.restoreActionId];
                }
                _(restoreActionIds).find(function (actionId) {
                    return (restoreAction = _($scope.actionTypes).find(function (action) {
                        return action.id === actionId;
                    }));
                });
            }
            if ($scope.options.restoreActionId && (_($scope.options.restoreFeature).isUndefined() || feature[$scope.options.restoreFeature])) {
                restoreAction = _($scope.actionTypes).find(function (action) {
                    return action.id === $scope.options.restoreActionId;
                });
            }
            if (restoreAction) {
            $scope.restoreButton = {
                    label: 'Restore...',
                    display: 'button',
                    active: function () {
                        return $scope.tableHelper.getSelectedIds().length > 0;
                    },
                    execute: function() {
                        modals.params([{
                            params: _(restoreAction.attributes).chain().filter(function(param) {
                                return _(param.label).isString();
                            }).map(function(param) {
                                return $.extend(true, {}, param);
                            }).value(),
                            lastPage: true,
                            getProperties: _.memoize(remediationProperties($scope.sources.properties))
                        }], restoreAction.label, restoreAction.size, {
                            noDataLabel: restoreAction.confirm
                        }).then(function(data) {
                            var body = {
                                name: restoreAction.id,
                                attributes: data
                            };
                            manualActionDao.create($scope.options.entityType, $scope.tableHelper.getSelectedIds(), body).then(function () {
                                $scope.restoreAutoUpdateAfter = moment().add(30,'seconds');
                                $scope.tableHelper.options.options.autoUpdate = 2000;
                                $scope.tableHelper.clearSelection();
                                $scope.tableHelper.reload();
                            });
                        });
                    }
                };
            }
            $scope.tableOptions.actions = [$scope.restoreButton];
        }
        updateColumns($scope.options);

        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
            var q = ($scope.options.localEntity === 'email' ? emailDao : fileDao).quarantine($scope.options.columns);

            if ($scope.resultsPanel.uuid && $scope.resultsPanel.filters) {
                q.params(widgetSettings.params($scope.resultsPanel.uuid));
            }
            if (args.columnsFilters) {
                q.params(columnsHelper.filtersToParams(args.columnsFilters));
            }

            return q.pagination(args.offset, args.limit)
                .order(args.ordering.columnName, args.ordering.order, 'last');
        }, function(tableModel) {
            $scope.tableModel = tableModel;
            $scope.resultsPanel.title = $scope.tableHelper.getTotal() + ' quarantined ' + $scope.options.localEntity + 's';

            if ($scope.restoreAutoUpdateAfter && $scope.restoreAutoUpdateAfter.isBefore(moment())) {
                $scope.tableHelper.options.options.autoUpdate = $scope.options.options.autoUpdate;
                $scope.restoreAutoUpdateAfter = null;
            }

        });

        $scope.resultsPanel.forceUpdate = function () {
            updateColumns($scope.tableHelper.options);
            updateColumns($scope.options);
            $scope.tableHelper.clearSelection();
            $scope.tableHelper.reload();
        };

    });

    sideMenuManager.treeNavigationPath(['configuration', 'quarantine']);
});
