var m = angular.module('gui-v2');

m.controller('SecurityEventsController', function($scope, securityEventStates, securityEventTypes, severityTypes, tablePaginationHelper, securityEventsDao, defaultListConverter, $location, base64, timeResolutions, modulesManager, $timeout, columnsHelper, securityEventsConverters, $q, modals, workingIndicatorHelper) {
    $scope.chartsWidget = {};
    $scope.model = {
        resolution: 'week',
        filters: []
    };
    updateResolution({id: $scope.model.resolution});
    $scope.workingIndicator = workingIndicatorHelper;

    $scope.totalEvents = 0;
    var eventsUpdating = {};



    var resolutionFilterData = _(timeResolutions.data).chain().filter(function (item) {
        return item.id !== 'minute';
    }).map(function (item) {
        return {
            id: item.id,
            label: item.pastLabel
        };
    }).value();


    $scope.filterButtons = [{
        label: 'Date',
        id: 'create_time',
        'class': 'filter-date',
        variants: resolutionFilterData,
        execute: function (variant) {
            updateResolution(variant);
        }
    }, {
        label: 'State',
        id: 'current_state',
        'class': 'filter-state',
        variants: angular.copy(securityEventStates.data),
        execute: function (variant) {
            $scope.updateFilter('current_state', variant);
        }
    }, {
        label: 'Type',
        id: 'type',
        'class': 'filter-type',
        variants: angular.copy(securityEventTypes.data)
    }, {
        label: 'Severity Level',
        id: 'severity',
        'class': 'filter-severity',
        variants: [{
            id: 5,
            label: 'Critical'
        }, {
            id: 4,
            label: 'High'
        }, {
            id: 3,
            label: 'Medium'
        }, {
            id: 2,
            label: 'Low'
        }, {
            removeAll: true,
            label: 'All'
        }],
        skipAll: true,
        displayVariants: angular.copy(severityTypes.data),
        execute: function (variant) {
            var button = this;
            $scope.updateFilter('severity', {
                removeAll: true
            }, true);
            if (variant.removeAll) {
                reload();
                return;
            }
            _(button.displayVariants).chain().filter(function (type) {
                return type.id >= variant.id;
            }).each(function (type) {
                $scope.updateFilter('severity', type, true);
            });
            reload();
        }
    }, {
        label: 'SaaS',
        id: 'saas',
        'class': 'filter-saas',
        variants: _(modulesManager.activeSaasApps()).map(function (saas) {
            return {
                id: saas.name,
                label: saas.title
            };
        })
    }];
    var filterButtonsMap = {};
    _($scope.filterButtons).each(function (button) {
        filterButtonsMap[button.id] = button;
        if (!button.skipAll) {
            button.variants.unshift({
                removeAll: true,
                label: 'All'
            });
        }
        if (!button.execute) {
            button.execute = function (variant) {
                $scope.updateFilter(button.id, variant);
            };
        }
    });

    var savedFilters = $location.search().filters;
    if (!_(savedFilters).isString()) {
        savedFilters = base64.encode(JSON.stringify([{"id":"new","type":"current_state"},{"id":"remediated","type":"current_state"}]));
    }

    if(_(savedFilters).isString()) {
        try {
            var filters = JSON.parse(base64.decode(savedFilters));
            $scope.model.filters = _(filters).chain().map(function (filter) {
                if (!filterButtonsMap[filter.type]) {
                    return;
                }
                var found = _(filterButtonsMap[filter.type].displayVariants || filterButtonsMap[filter.type].variants).find(function (variant) {
                    return variant.id === filter.id;
                });
                if (!found) {
                    return;
                }
                return {
                    type: filter.type,
                    id: found.id,
                    label: found.label
                };
            }).compact().value();
        } catch (e) {

        }
    }

    var savedResolution = $location.search().resolution;
    if (_(savedResolution).isString()) {
        try {
            savedResolution = base64.decode(savedResolution);
            var foundResolution = _(resolutionFilterData).find(function (item) {
                return item.id === savedResolution;
            });
            if (!foundResolution) {
                foundResolution = {
                    removeAll: true
                };
            }
            updateResolution(foundResolution);
        } catch (e) {
        }
    }
    var highlightEvent = $location.search().eventId || false;
    if (highlightEvent) {
        highlightEvent = parseInt(highlightEvent);
        if (isNaN(highlightEvent)) {
            highlightEvent = false;
        }
        $location.search(_($location.search()).omit('eventId'));
    }

    function updateResolution (variant) {
        if (!variant.removeAll) {
            var foundResolution = _(resolutionFilterData).find(function (item) {
                return item.id === variant.id;
            });
            if (foundResolution) {
                $scope.model.resolution = foundResolution.id;
                $scope.model.resolutionLabel = foundResolution.label;
                reload();
                return;
            }
        }
        $scope.model.resolution = 'all';
        $scope.model.resolutionLabel = '';
        reload();
    }

    $scope.clearResolution = function () {
        updateResolution({removeAll: true});
    };

    $scope.updateFilter = function(type, variant, noReload) {
        var prevLength = $scope.model.filters.length;
        if (variant.removeAll) {
            $scope.model.filters = _($scope.model.filters).filter(function (filter) {
                return filter.type !== type;
            });
        } else {
            var id = variant.id;
            var label = variant.label;
            var found = _($scope.model.filters).find(function (filter) {
                return filter.type === type && filter.id === id;
            });
            if (_(found).isUndefined()) {
                $scope.model.filters.push({
                    type: type,
                    id: id,
                    label: label
                });
            }
        }
        if (!noReload && $scope.model.filters.length !== prevLength) {
            reload();
        }
    };

    $scope.removeFilterByIndex = function (idx) {
        $scope.model.filters.splice(idx, 1);
        reload();
    };

    $scope.options = {
        "columns": [{
            "id": "id",
            "hidden": true,
            "primary": true
        }, {
            "id": "create_time",
            "text": "Time",
            "converter": "doubleLineDate",
            "sortable": true
        }, {
            "id": "current_state",
            "text": "State",
            "converter": "stateConverter",
            "class": "text-align-center",
            "dataClass": "text-align-center"
        }, {
            "id": "severity",
            "text": "Severity",
            "dataClass": "text-align-center",
            "converter": "severityConverter",
            "converterArgs": {
                "class": "severity-inline-editor",
                "eventName": "security-event-severity-change",
                "attributes": {
                    "eventId": "id"
                }
            }
        }, {
            "id": "saas",
            "text": "SaaS",
            "converter": "eventSaas",
            "dataClass": "saas-icon"
        }, {
            "id": "type",
            "text": "Type",
            "converter": "securityEventTypeConverter"
        }, {
            "id": "description",
            "text": "Event Description",
            "class": "wide-column",
            "dataClass": "wide-column",
            "converter": "descriptionConverter"
        }, {
            "id": "_resolved.available_actions",
            "text": "Workflow",
            "converter": "eventWorkflowConverter",
            "dataClass": "workflow-cell"
        }, {
            "id": "_resolved.history",
            "text": "History",
            "converter": "multiConverter",
            "converterArgs": {
                "converters": [{
                    "converter": "resortArrayConverter",
                    "converterArgs": {
                        "field": "create_time",
                        "reverse": true
                    }
                }, {
                    "converter": "listConverter",
                    "converterArgs": {
                        "countPostfix": "actions",
                        "collapseRest": true,
                        "moreInPopup": true,
                        "expanded": 10,
                        "itemConverter": "historyItemConverter",
                        "itemConverterScope": "element"
                    }
                }]
            }
        }, {
            "id": "_resolved",
            "hidden": true
        }],
        "options": {
            "defaultOrdering": {
                "columnName": "create_time",
                "order": "desc"
            },
            "checkboxes": true,
            "autoUpdate": 30000
        }
    };
    $scope.tableOptions = {
        disableTop: true,
        orderDisplayMode: 'highlight-column',
        showWorkingIndicator: true,
        workingIndicatorDisableGui: false,
        saveState: true,
        pagesAround: 2,
        pagination: {
            pageSize: 20,
            ordering: {}
        },
        "dropdownHeader": true
    };

    function rowMatchesFilters (row) {
        var filters = _($scope.model.filters).reduce(function (memo, filter) {
            var type = filter.type;
            memo[type] = memo[type] || [];
            memo[type].push(filter.id);
            return memo;
        }, {});
        return !_(filters).find(function (values, field) {
            return !_(values).contains(row[field]);
        });
    }

    $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function (args) {
        $location.search('filters', base64.encode(JSON.stringify(_($scope.model.filters).map(function (filter) {
            return _(filter).pick('type', 'id');
        }))));
        $location.search('resolution', base64.encode($scope.model.resolution));
        var result = securityEventsDao.list().params({resolve: true});

        if (timeResolutions.get($scope.model.resolution)) {
            result.params({
                'afterColumn': 'create_time',
                'afterStep': $scope.model.resolution,
                'afterFromNow': true,
                'afterMax': 1
            });
        }

        result.params(_($scope.model.filters).chain().reduce(function (memo, filter) {
            var type = 'filterEnum_' + filter.type;
            memo[type] = memo[type] || [];
            memo[type].push(filter.id);
            return memo;
        }, {}).mapObject(function (val) {
            return val.join(',');
        }).value());

        return result.pagination(args.offset, args.limit)
            .order(args.ordering.columnName, args.ordering.order)
            .converter(defaultListConverter, {
                columns: $scope.options.columns,
                converters: securityEventsConverters
            });
    }, function (tableModel) {
        $scope.tableModel = tableModel;
        $scope.totalEvents = $scope.tableHelper.getTotal();
        updateRowClasses();
    });

    var actionsColumnIdx = columnsHelper.getIdxByIdStrict($scope.options.columns, '_resolved.available_actions');
    $scope.groupActions = [{
        label: 'Dismiss...',
        enabled: function () {
            if (actionsColumnIdx === -1) {
                return false;
            }
            if (!$scope.tableHelper) {
                return false;
            }
            var selected = ($scope.tableHelper.getSelected());
            if (!selected.length) {
                return false;
            }
            var noDismissEvent = _(selected).find(function (row) {
                return !row[actionsColumnIdx].originalText || !row[actionsColumnIdx].originalText.dismiss;
            });
            if (noDismissEvent) {
                return false;
            }
            return true;
        },
        execute: function () {
            var btn = this;
            if (!btn.enabled()) {
                return;
            }
            var primaryColumn = columnsHelper.getPrimaryIdx($scope.options.columns);
            var selectedEventIds = _($scope.tableHelper.getSelected()).chain().filter(function (row) {
                return row[actionsColumnIdx].originalText && row[actionsColumnIdx].originalText.dismiss;
            }).map(function (row) {
                return row[primaryColumn].originalText;
            }).value();
            if (!selectedEventIds.length) {
                return;
            }
            modals.confirm({
                message: 'Are you sure you want to dismiss ' + selectedEventIds.length + ' event' +
                   (selectedEventIds.length > 1 ? 's' : '') + '?'
            }).then(function () {
                $scope.tableHelper.clearSelection();
                $q.all(_(
                    _(selectedEventIds).map(function (eventId) {
                        return performSingleEventAction(eventId, 'dismiss');
                    })
                )).then(function () {
                    var needReload = false;
                    try {
                        needReload = $scope.tableModel.pagination.total > 1 &&
                            $scope.tableModel.data.length < $scope.tableOptions.pagination.pageSize / 2;
                    } catch (e) {}
                    if (needReload) {
                        reload();
                    }
                });
            });
        }
    }];

    function updateEventInline (response, removeOnly) {
        var keepRow = rowMatchesFilters(response.data);
        return  defaultListConverter({
            data: {
                rows: [response.data]
            }
        }, {
            columns: $scope.options.columns,
            converters: securityEventsConverters
        }).then(function (data) {
            var newRow = data.data.data[0];
            var primaryColumn = columnsHelper.getPrimaryIdx($scope.options.columns);
            var foundIndex = _($scope.tableModel.data).findIndex(function (row) {
                return row[primaryColumn].originalText === newRow[primaryColumn].originalText;
            });
            if (foundIndex > -1) {
                if (keepRow) {
                    if (!removeOnly) {
                        $scope.tableModel.data.splice(foundIndex, 1, newRow);
                    }
                } else {
                    $scope.tableModel.data.splice(foundIndex, 1);
                }
            }
        });
    }

    function performSingleEventAction (eventId, actionId) {
        eventsUpdating[eventId] = true;
        updateRowClasses();
        return securityEventsDao.action(eventId, actionId).then(function (response) {
            return updateEventInline (response);
        }).finally(function () {
            eventsUpdating[eventId] = false;
            updateRowClasses();
        });
    }

    function updateVisualSeverity (eventId, newValue) {
        var primaryColumn = columnsHelper.getPrimaryIdx($scope.options.columns);
        var severityColumnIdx = columnsHelper.getIdxByIdStrict($scope.options.columns, 'severity');
        if (severityColumnIdx === -1) {
            return false;
        }
        var found = _($scope.tableModel.data).find(function (row) {
            return row[primaryColumn].originalText === eventId;
        });
        if (!found) {
            return false;
        }
        found[severityColumnIdx].originalText = newValue;
        found[severityColumnIdx].text = securityEventsConverters.severityConverter(newValue, 'severity', {
            id: eventId
        }, $scope.options.columns[severityColumnIdx].converterArgs);
        return true;
    }

    $scope.$on('security-event-severity-change', function (event, data) {
        if (data.oldValue === data.newValue) {
            return;
        }
        if (!updateVisualSeverity(data.attributes.eventId, data.newValue)) {
            return;
        }
        securityEventsDao.update(data.attributes.eventId, {
            severity: data.newValue
        }).then(function (response) {
            return updateEventInline (response, true);
        }).then(function () {
            updateVisualSeverity(data.attributes.eventId, data.newValue);
            $timeout($scope.chartsWidget.reload);
        }, function (error) {
            reload();
        });
    });

    $scope.$on('event-action', function (event, data) {
        return performSingleEventAction(data.eventId, data.actionId);
    });

    function updateRowClasses () {
        if (!$scope.tableModel || !$scope.tableModel.data) {
            return;
        }
        var primaryColumn = columnsHelper.getPrimaryIdx($scope.options.columns);
        _($scope.tableModel.data).each(function (row, idx) {
            $scope.tableModel.rowClasses[idx] = $scope.tableModel.rowClasses[idx] || {};
            if (eventsUpdating[row[primaryColumn].originalText]) {
                $scope.tableModel.rowClasses[idx].processing = true;
            } else {
                delete $scope.tableModel.rowClasses[idx].processing;
            }
            delete $scope.tableModel.rowClasses[idx]['highlighted-event'];
            if (highlightEvent && row[primaryColumn].originalText === highlightEvent) {
                highlightEvent = false;
                $scope.tableModel.rowClasses[idx]['highlighted-event'] = true;
                $timeout(function () {
                    var hRow = $('tr.highlighted-event');
                    if (hRow.length) {
                        hRow[0].scrollIntoView();
                    }
                });
            }
        });
    }

    function reload (noChartsReload) {
        if (!$scope.tableHelper) {
            return;
        }
        $scope.tableHelper.clearSelection();
        $scope.tableHelper.reload();
        if (!noChartsReload) {
            $timeout($scope.chartsWidget.reload);
        }
    }

});