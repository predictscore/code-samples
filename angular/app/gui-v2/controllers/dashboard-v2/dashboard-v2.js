var m = angular.module('gui-v2');

m.controller('DashboardControllerV2', function($scope, $q, wizardHelper, modulesManager, routeHelper) {
    $scope.model = {
        resolution: 'week'
    };

    $q.all([
        wizardHelper.reloadState(),
        modulesManager.reloadModules()
    ]).then(function(responses) {
        var saasAppsLength = modulesManager.activeSaasApps().length || 0;
        var wizardState = wizardHelper.getState().inProgress || false;
        if (saasAppsLength === 0 || wizardState) {
            routeHelper.redirectTo('wizard');
        }
    });
});