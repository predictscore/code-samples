var m = angular.module('gui-v2');

m.controller('WizardControllerV2', function($scope, wizardHelper, sizeClasses, $routeParams, $route, modulesDao, wizardDao, $q, modulesManager, routeHelper, path, sideMenuManager, commonDao) {
    $scope.loading = true;
    $scope.getSizeClasses = sizeClasses;
    $scope.wizardHelper = wizardHelper;
    $scope.userManagementUrl = '/#!' + routeHelper.getPath('wizard-user-management');
    $scope.bgImage = path('auth').ctrl('auth').img('background.png');
    $scope.imgRoot = path('gui-v2').ctrl('wizard-v2');
    $scope.stepInfo = {};
    $scope.terms = { accepted: false };

    wizardHelper.reset();
    wizardHelper.scope($scope);

    var enabledMap = {};

    $scope.allowNav = function () {
        return modulesManager.activeSaasApps().length > 0;
    };

    $scope.exit = function () {
        if (!$scope.allowNav()) {
            return;
        }
        wizardHelper.setStatus('disabled').then(function () {
            routeHelper.redirectTo('dashboard');
        });
    };

    $scope.goNext = function () {
        if ($scope.loading || (!$scope.allowNav() && $scope.currentStep > 0)) {
            return;
        }
        if ($scope.stepInfo.type === 'category-selector') {
            $scope.startingApps = true;
            return wizardDao.startApps($scope.startCategories).then(function () {
                $scope.startingApps = false;
                goToStep($scope.currentStep + 1);
            });
        }
        goToStep($scope.currentStep + 1);
    };

    $scope.goBack = function () {
        if ($scope.loading) {
            return;
        }
        goToStep($scope.currentStep - 1);
    };

    $scope.stepDone = function (step) {
        return enabledMap[step];
    };

    $scope.progressStepByIndex = function (idx) {
        return wizardHelper.steps.length - idx - 1;
    };

    $scope.hereStyle = function () {
        if ($scope.currentStep !== wizardHelper.steps.length - 2) {
            return {
                left: (($scope.currentStep - 1) * (33 + 36) + 10) + 'px'
            };
        } else {
            return {
                left: 'auto',
                right: '-15px'
            };
        }
    };

    $scope.acceptTerms = function () {
        return commonDao.acceptTerms().then(function (response) {
            $scope.terms.accepted = response.data;
        });
    };

    function getCategories(categories, params) {
        return _(categories).filter(function (category) {
            return _(params).reduce(function (memo, value, key) {
                return memo && category[key] === value;
            }, true);
        });
    }

    function setEnabledMap(categories) {
        enabledMap = _(wizardHelper.steps).map(function (info) {
            if (info.type === 'info') {
                return true;
            }
            if (info.type === 'category-selector') {
                return false;
            }
            return !_(getCategories(categories, info.categoryFilter)).chain().find(function (category) {
                return category.haveActiveItems;
            }).isUndefined().value();
        });
    }


    function goToStep(step) {
        $scope.loading = true;
        $scope.currentStep = parseInt(step);
        if (!isFinite($scope.currentStep) || (!$scope.allowNav() && $scope.currentStep > 1)) {
            $scope.currentStep = 0;
        }
        $scope.stepInfo = wizardHelper.steps[$scope.currentStep];
        if ($scope.stepInfo.titleNavAllowed) {
            $scope.title = ($scope.allowNav() ? $scope.stepInfo.titleNavAllowed : $scope.stepInfo.title);
        } else {
            $scope.title = $scope.stepInfo.title;
        }
        if ($scope.stepInfo.titleAddUser) {
            $scope.title += ', ' + sideMenuManager.currentUser().email;
        }
        if ($scope.stepInfo.linkGoTo) {
            $scope.linkUrl =  '/#!' + routeHelper.getPath($scope.stepInfo.linkGoTo, $scope.stepInfo.linkGoToParams);
        } else {
            $scope.linkUrl = null;
        }

        $scope.stepInfo.showTerms = false;
        wizardHelper.setStep($scope.currentStep);

        if (!$scope.stepInfo) {
            wizardHelper.reset();
            routeHelper.redirectTo('dashboard');
            return;
        }
        if ($scope.stepInfo.type === 'info') {
            wizardHelper.reset();
            $scope.loading = false;

            if ($scope.stepInfo.disableWizard) {
                wizardHelper.setStatus('disabled');
            }
            return;
        }

        if ($scope.stepInfo.type === 'category-selector') {
            wizardHelper.reset();
            $scope.loading = true;
            $scope.visibleCategories = {};
            $scope.startCategories = _($scope.stepInfo.categories).chain().map(function (category) {
                return [category.type, false];
            }).object().value();
            wizardDao.wizardApps().then(function (categoriesConfig) {
                $scope.visibleCategories = _($scope.stepInfo.categories).chain().map(function (category) {
                    var categoryVisible = categoriesConfig && categoriesConfig[category.type] || false;
                    if (categoryVisible && category.selected) {
                        $scope.startCategories[category.type] = true;
                    }
                    return [category.type, categoryVisible];
                }).object().value();
                $scope.loading = false;
            });
            return;
        }

        if ($scope.stepInfo.terms) {
            commonDao.getTermsAccepted().then(function (response) {
                $scope.terms.accepted = response.data;
                //$scope.stepInfo.showTerms = !$scope.terms.accepted; // don't show 'accept terms now since it's shown in the dialog
            });
        }
        var prevResponse = null;
        function responseModules(response) {
            return _(response.data.widgets).map(function(widget) {
                return _(widget.options).omit('configurationLink'); //Is it safe?
            });
        }
        wizardHelper.reload(function (params) {
            return wizardDao.getApps($scope.stepInfo).params(params).then(function (response) {
                if(!_(prevResponse).isEqual(responseModules(response))) {
                    prevResponse = responseModules(response);
                    setEnabledMap(response.data.categories);
                    wizardHelper.setWidgets(response.data.widgets);
                }
                if ($scope.stepInfo.modulesRecheck) {
                    modulesManager.reloadModules();
                }
                $scope.loading = false;
            });
        }, $scope.stepInfo.updateTick);
    }

    wizardHelper.reloadState().then(function() {
        var state = wizardHelper.getState();
        if (state.inProgress) {
            goToStep(state.savedStep);
            return;
        }
        modulesManager.reloadModules().then(function () {
            if (modulesManager.activeSaasApps().length) {
                routeHelper.redirectTo('dashboard', {
                    module: modulesManager.currentModule()
                });
                return;
            }
            $q.all([
                wizardHelper.setStatus('enabled'),
                wizardHelper.setStep(null)
            ]);
            state.savedStep = null;
            goToStep(state.savedStep);
        });
    });

    $scope.getWidgetClasses = function(widget) {
        var result = {};
        result[$scope.getSizeClasses(widget.size)] = true;
        result['module-' + widget.options.moduleName] = true;
        return result;
    };

    $scope.toggleCategoryState = function (type) {
        $scope.startCategories[type] = !$scope.startCategories[type];
    };

    $scope.categoryTooltip = function (category) {
        var text;
        if ($scope.startCategories[category.type]) {
            text = 'Click to de-select ' + category.label;
        } else {
            text = 'Click to select ' + category.label;
        }
        return {
            content: {
                text: text
            },
            position: {
                at: 'top center',
                my: 'bottom left',
                viewport: true
            }
        };
    };

    $scope.$on('$destroy', function () {
        wizardHelper.reset();
    });
});