var m = angular.module('report');

m.controller('ReportController', function($q, $scope, reportDao, objectTypeDao, $routeParams, modals, tablePaginationHelper, tableExportHelper, workingIndicatorHelper, $route, sideMenuManager, modulesManager) {
    workingIndicatorHelper.init($scope);

    var id = $routeParams.id;

    $q.all([
        reportDao.retrieve(id),
        objectTypeDao.retrieve()
    ]).then(function(responses) {
        $scope.model = responses[0].data;
        $scope.sources = responses[1].data.sources;

        if ($scope.model.data && $scope.model.data.template && $scope.model.data.template.saas) {
            modulesManager.currentModule($scope.model.data.template.saas);
        }

        function updatePageTitle() {
            $route.current.fullTitle = $route.current.title + ($scope.model.data.name ? (' - ' + $scope.model.data.name) : '');
        }

        updatePageTitle();

        $scope.reportOptionsPanel = {
            title: "Report options",
            'class': 'light white-body white-header no-margin'
        };

        $scope.resultsPanel = {
            title: 'Results',
            'class': 'light white-body white-header no-margin',
            buttons: [{
                position: 'left',
                'class': 'btn-blue',
                execute: function() {
                    setShowOptions(!$scope.showOptions);
                    $scope.splitterOptions.disabled = !$scope.showOptions;
                }
            }]
        };
        function setShowOptions(state) {
            $scope.showOptions = state;
            $scope.resultsPanel.buttons[0].label = $scope.showOptions ? 'Hide options': 'Show options';
        }
        setShowOptions(false);


        $scope.splitterOptions = {
            orientation: 'vertical',
            position: '50%',
            limit: 150,
            disabled: !$scope.showOptions
        };

        $scope.update = function() {
            if(_($scope.model.data.name).isUndefined()) {
                modals.alert('You must specify report name before you can update');
                return;
            }

            reportDao.update(id, $scope.model).then(function() {
                $scope.tableHelper.clearSelection();
                $scope.tableHelper.reload();
                updatePageTitle();
            });
        };

        $scope.tableOptions = {
            pagesAround: 2,
            pageSize: 20,
            pagination: {
                page: 1,
                ordering: {},
                filter: ''
            },
            saveState: true,
            disableTop: true,
            showWorkingIndicator: true,
            actions: [{
                label: 'Export...',
                display: 'group-button',
                submenus: [
                    {
                        label: 'Export as pdf',
                        execute: function () {
                            tableExportHelper('pdf', $scope.model.data.name, $scope.tableHelper);
                        }
                    }, {
                        label: 'Export as csv',
                        execute: function () {
                            tableExportHelper('csv', $scope.model.data.name, $scope.tableHelper);
                        }
                    }, {
                        label: 'Export as xlsx',
                        execute: function () {
                            tableExportHelper('xlsx', $scope.model.data.name, $scope.tableHelper);
                        }
                    }]
            }]
        };

        $scope.options = $scope.model.data.template;
        $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
            var params = {};
            _($scope.model.data.params).each(function(value, key) {
                if(_(value).isObject()) {
                    if(_(value).isArray()) {
                        params[key] =value.join(",");
                    }else {
                        params[key] = value[value.primaryKey];
                    }
                }else {
                    params[key] = value;
                }
            });
            return reportDao.report($scope.options.columns, $scope.options.url, params)
                .pagination(args.offset, args.limit)
                .filter(args.filter)
                .order(args.ordering.columnName, args.ordering.order, args.ordering.nulls);
        }, function(tableModel) {
            $scope.tableModel = tableModel;
        });
    });
    
    sideMenuManager.treeNavigationPath(['reports']);
});