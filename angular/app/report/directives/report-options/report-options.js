var m = angular.module('report');

m.directive('reportOptions', function(path, tablePaginationHelper, reportDao, columnsHelper, $q, folderDao, eventsDao, $injector) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('report').directive('report-options').template(),
        scope: {
            model: '=reportOptions'
        },
        link: function($scope) {
            $scope.internalModel = {
                editableTemplate: false
            };

            $scope.promiseResult = {};

            if(_($scope.model.template).isUndefined()) {
                $scope.internalModel.editableTemplate = true;
                $scope.tableOptions = {
                    pagesAround: 2,
                    pageSize: 20,
                    pagination: {
                        page: 1,
                        ordering: {},
                        filter: ''
                    },
                    disableBottom: true
                };

                path('report').directive('report-options').json('template-list-conf').retrieve().then(function(response) {
                    $scope.options = response.data;
                    $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function (args) {
                        return reportDao.templates(response.data.columns);
                    }, function (tableModel) {
                        $scope.tableModel = tableModel;
                    });

                    $scope.$watchCollection('tableHelper.getSelected()', function(selected) {
                        if (_(selected).isUndefined() || !selected.length) {
                            return;
                        }

                        var templateIdx = columnsHelper.getIdxById(response.data.columns, null);
                        $scope.model.template = $.extend(true, {}, _(selected).first()[templateIdx]).originalText;
                        $scope.model.params = _($scope.model.template.params).chain().map(function(param) {
                            return [param.id, param.value];
                        }).object().value();

                        generateQuestions();

                        $scope.tableHelper.clearSelection();
                    });
                });
            } else {
                generateQuestions();
            }

            function generateQuestions() {
                $q.all(_($scope.model.template.params).chain().filter(function(param) {
                    return !_(param.label).isUndefined();
                }).map(function(param) {
                    if (param.extendWith) {
                        var promise = $injector.get(param.extendWith);
                        if (_(promise).isUndefined()) {
                            alert('Unable to extend param with ' + param.extendWith);
                        }
                        return promise().then(function (data) {
                            return $q.when($.extend(true, param, data));
                        });
                    }
                    return $q.when(param);
                })).then(function(params) {
                    $scope.internalModel.questions = _(params).map(function(param) {
                        return $.extend(true, {}, param, {
                            type: {
                                'TEXT': 'string',
                                'LIST': 'list',
                                'DATETIME':'datetime',
                                'INT': 'integer',
                                'BOOLEAN': 'boolean',
                                'DIRECTORY': 'tree',
                                'FILE': 'tree',
                                'SELECTOR': 'selector'
                            }[param.type],
                            options: {
                                retrieveContent: function(id) {
                                    if(_(id).isUndefined()) {
                                        return $q.when({
                                            data:{
                                                rows: [{
                                                    label: '#' + JSON.stringify({
                                                        display: 'img',
                                                        path: path('common').img('mime/folder.svg'),
                                                        'class': 'icon-16px'
                                                    }) + '/',
                                                    entity_id: '',
                                                    expandable: true,
                                                    primaryKey: 'entity_id'
                                                }]
                                            }
                                        });
                                    }
                                    return folderDao.content(id, param.type == 'FILE');
                                },
                                primaryField: 'entity_id',
                                labelField: 'label',
                                expandableField: 'expandable'
                            },
                            popup: {
                                title: param.label
                            }
                        });
                    });
                });
            }

            $scope.reset = function() {
                if($scope.internalModel.editableTemplate) {
                    delete $scope.model.template;
                    delete $scope.model.params;
                }
            };
        }
    };
});