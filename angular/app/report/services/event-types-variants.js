var m = angular.module('report');

m.factory('eventTypesVariants', function ($q, eventsDao) {
    return function () {
        return eventsDao.retrieveTypes({
            drive: true,
            login: true
        }).then(function(response) {
            return $q.when({
                variants: _(response.data).map(function(eventType) {
                    return {
                        id: eventType,
                        label: eventType
                    };
                })
            });
        });
    };
});