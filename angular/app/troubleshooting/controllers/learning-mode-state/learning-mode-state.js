var m = angular.module('troubleshooting');

m.controller('LearningModeStateController', function($scope, widgetPolling, sizeClasses, path) {
    widgetPolling.updateWidgets(
        path('troubleshooting').ctrl('learning-mode-state').json('learning-mode-state').path
    );
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
});