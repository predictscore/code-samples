var m = angular.module('troubleshooting');

m.controller('MainQueueStatsController', function($scope, widgetPolling, sizeClasses, path) {
    widgetPolling.updateWidgets(
        path('troubleshooting').ctrl('main-queue-stats').json('main-queue-stats').path
    );
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
});