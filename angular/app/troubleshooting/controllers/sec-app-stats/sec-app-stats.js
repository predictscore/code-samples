var m = angular.module('troubleshooting');

m.controller('SecAppStatsController', function($scope, widgetPolling, sizeClasses, path) {
    widgetPolling.updateWidgets(
        path('troubleshooting').ctrl('sec-app-stats').json('sec-app-stats').path
    );
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
});