var m = angular.module('troubleshooting');

m.controller('HealthCheckController', function($scope, widgetPolling, sizeClasses, path) {

    widgetPolling.updateWidgets(
        path('troubleshooting').ctrl('health-check').json('health-check').path
    );
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
});