var m = angular.module('troubleshooting');

m.controller('EntityPerUserController', function($scope, widgetPolling, sizeClasses, path, objectTypeDao, modulesManager, $q) {
    $q.all([
        objectTypeDao.retrieve(),
        modulesManager.init()
    ]).then(function(responses) {
        widgetPolling.scope({
            objectTypes: responses[0]
        });
        widgetPolling.updateWidgets(
            path('troubleshooting').ctrl('entity-per-user').json('entity-per-user').path
        );
        $scope.widgetPolling = widgetPolling;
        $scope.getSizeClasses = sizeClasses;
    });
});