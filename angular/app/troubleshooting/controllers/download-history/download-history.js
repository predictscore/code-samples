var m = angular.module('troubleshooting');

m.controller('DownloadHistoryController', function($scope, widgetPolling, sizeClasses, path) {
    widgetPolling.updateWidgets(
        path('troubleshooting').ctrl('download-history').json('download-history').path
    );
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
});