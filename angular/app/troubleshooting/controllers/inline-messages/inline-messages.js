var m = angular.module('troubleshooting');

m.controller('InlineMessagesController', function($scope, path, $routeParams, tablePaginationHelper, entityTypes, routeHelper, columnsHelper, troubleshootDao, widgetSettings, $q) {

    $scope.currentModule = $routeParams.module;

    var refreshPeriods = [{
        id: 1,
        label: '1 minute'
    }, {
        id: 5,
        label: '5 minutes'
    }, {
        id: 10,
        label: '10 minutes'
    }, {
        id: 15,
        label: '15 minutes'
    }];


    function rebuildButtons(reloadTable) {
        var params = widgetSettings.params($scope.resultsPanel.uuid);
        var selected = params.refreshPeriod;
        if (!_(refreshPeriods).find(function (period) {
                return period.id === params.refreshPeriod;
            })) {
            selected = 1;
        }
        if($scope.lastSelected === selected) {
            return;
        }
        $scope.options.options.autoUpdate = 60000 * selected;
        if (reloadTable) {
            $scope.tableHelper.reload();
        }
        $scope.lastSelected = selected;
        $scope.resultsPanel.buttons = [{
            'class': 'btn-icon refresh-selector',
            iconClass: 'fa fa-clock-o',
            tooltip: "Autmoatic reload interval",
            options: _(refreshPeriods).map(function(period) {
                return {
                    label: '#' + JSON.stringify({
                        display: 'text',
                        'class': {
                            'fa': true,
                            'fa-circle-o': selected != period.id,
                            'fa-check-circle-o': selected == period.id
                        }
                    }) + ' ' + period.label,
                    execute: function() {
                        var params = widgetSettings.params($scope.resultsPanel.uuid);
                        params.refreshPeriod = period.id;
                        widgetSettings.params($scope.resultsPanel.uuid, params);
                        rebuildButtons(true);
                    }
                };
            })
        }];
    }

    $scope.resultsPanel = {
        title: 'Loading...',
        'class': 'light white-body white-header',
        "bodyStyle": {
            "min-height": "500px"
        }
    };

    $scope.tableOptions = {
        pagesAround: 2,
        pageSize: 20,
        pagination: {
            page: 1,
            ordering: {},
            "columnsFilters": {
                "received_time_filter": [{
                    "type": "date",
                    "operator": ">=",
                    "operatorLabel": ">=",
                    "not": false,
                    "options": {},
                    "data": moment().subtract(7, 'days').format('YYYY-MM-DD HH:mm:ss')
                }]
            }
        },
        saveState: true,
        showWorkingIndicator: true,
        disableTop: true,
        headerOptions: {
            enum: {
                multiple: true
            }
        }
    };

    $scope.localEntity = 'email';
    $scope.entityType = entityTypes.toBackend($scope.currentModule, $scope.localEntity);
    if (!$scope.entityType) {
        routeHelper.redirectTo('dashboard');
        return;
    }


    path('troubleshooting').ctrl('inline-messages').json($scope.currentModule + '.inline-messages').retrieve()
        .then(function (response) {
            $scope.options = response.data;

            var actionColumnIdx = columnsHelper.getIdxById($scope.options.columns, 'action');
            var scanTimeColumnIdx = columnsHelper.getIdxById($scope.options.columns, 'scan_time');

            $scope.resultsPanel.filters = $scope.options.filters;
            $scope.resultsPanel.uuid = $scope.options.uuid;

            if ($scope.resultsPanel.uuid) {
                rebuildButtons();
            }

            $scope.tableOptions.dropdownHeader = $scope.options.options.dropdownHeader;

            $scope.tableHelper = tablePaginationHelper($scope, $scope.tableOptions, $scope.options, function(args) {
                var q = troubleshootDao.inlineMessages($scope.currentModule, $scope.options.columns);

                if (args.columnsFilters) {
                    q.params(columnsHelper.filtersToParams(args.columnsFilters));
                }

                return q.pagination(args.offset, args.limit)
                    .order(args.ordering.columnName, args.ordering.order, 'last');
            }, function(tableModel) {
                $scope.tableModel = tableModel;
                $scope.resultsPanel.title = $scope.tableHelper.getTotal() + ' messages';
                if (!_($scope.tableOptions.pagination.columnsFilters).isEmpty()) {
                    $scope.resultsPanel.title += ' (Filtered. Click columns to change filters)';
                }
                if ($scope.tableModel) {
                    _($scope.tableModel.data).each(function (row, rowIdx) {
                        var scanTimeMinutes = row[scanTimeColumnIdx] && row[scanTimeColumnIdx].text && moment.duration(row[scanTimeColumnIdx].text).asMinutes() || 0;
                        if (row[actionColumnIdx].text && (row[actionColumnIdx].text !== 'unknown' || scanTimeMinutes >= 10)) {
                            $scope.tableModel.rowClasses[rowIdx] = $scope.tableModel.rowClasses[rowIdx] || {};
                            $scope.tableModel.rowClasses[rowIdx]['row-action-' + row[actionColumnIdx].text] = true;
                        }
                    });
                }
            });
        });

});
