var m = angular.module('troubleshooting');

m.controller('LicensesController', function($scope, widgetPolling, sizeClasses, path, avananUserDao, sideMenuManager, troubleshooting, $q) {
    var mode = 'licenses';
    (troubleshooting.entityDebugEnabled() ?
        avananUserDao.users().then(function (response) {
            var currentUser = _(response.data.rows).find(function (row) {
                return row.id === sideMenuManager.currentUser().id;
            });
            if (!currentUser) {
                mode = 'licenses-dep';
            }
        }) : $q.when()).then(function () {
        widgetPolling.updateWidgets(
            path('troubleshooting').ctrl('licenses').json(mode).path
        );
    });
    $scope.widgetPolling = widgetPolling;
    $scope.getSizeClasses = sizeClasses;
});