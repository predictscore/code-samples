var m = angular.module('troubleshooting');

m.factory('securityHealthLogicV2', function($q, simpleSortableLogicCore, troubleshootDao, widgetPolling) {
    return function($scope, logic) {
        $scope.selectedSaas = widgetPolling.scope().healthActiveSaas;

        var timeoutDeferred = $q.defer();

        var simpleFree = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                timeoutDeferred.resolve();
                timeoutDeferred = $q.defer();
                return troubleshootDao.secAppsStatusParallel($scope.model.columns, $scope.selectedSaas, timeoutDeferred.promise);
            }
        });


        var locationListener = $scope.$on('$locationChangeStart', function () {
            timeoutDeferred.resolve();
        });
        var saasSelectorWatcher = $scope.$watch(function () {
            return widgetPolling.scope().healthActiveSaas;
        }, function (newValue, oldValue) {
            if (newValue !== oldValue) {
                $scope.selectedSaas = newValue;
                $scope.tableHelper.reload();
            }
        });

        return function() {
            simpleFree();
            timeoutDeferred.resolve();
            locationListener();
            saasSelectorWatcher();
        };
    };
});