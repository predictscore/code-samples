var m = angular.module('troubleshooting');

m.factory('entityTrendLogic', function(troubleshootDao, $q, entityTypes, widgetPolling, $timeout) {
    return function($scope, logic) {
        function fillGaps(dateCountMap, after) {
            return _(dateCountMap).chain().map(function(value, date) {
                return [parseInt(moment(date).format('x'), 10), value];
            }).sortBy(function(o) {
                return o[0];
            }).value();
        }
        
        var objectTypes = widgetPolling.scope().objectTypes;
        
        $scope.model.wrapper.filters = [{
            type: 'select-all',
            local: true
        }, {
            type: 'filter',
            local: true
        }];
        
        var filters = [];
        _(objectTypes.data.entities).each(function(entity, entityId) {
            var modifyDateFound = _(entity.properties).find(function(property) {
                return property.parent_property == 'modify_date';
            });
            var label = '';
            if(entity.saas && objectTypes.data.saasList[entity.saas]) {
                label += objectTypes.data.saasList[entity.saas].label + ' - ';
            }
            label += entity.label;
            filters.push({
                "type": "flag",
                "state": false,
                "text": label,
                "value": entityId,
                "local": true,
                "resolveModifyDate": modifyDateFound
            });
        });
        _(filters).chain().sortBy('text').each(function(filter) {
            $scope.model.wrapper.filters.push(filter);
        });
        
        function rebuild(force) {
            var filters = $scope.model.wrapper.filters;
            if(force) {
                _(filters).each(function(filter) {
                    filter.update_time = undefined;
                    filter.modify_date = undefined;
                });
            }
            $q.all(_(filters).chain().filter(function(filter) {
                return filter.type == 'flag' && filter.state && (!filter.update_time || filter.resolveModifyDate && !filter.modify_date);
            }).map(function(filter) {
                function retrieveMode(mode) {
                    var resolution = $scope.model.wrapper.resolution;
                    return troubleshootDao.entityCount(filter.value, resolution.interval, resolution.count, resolution.value, mode).then(function(response) {
                        var dateCountMap = _(response.data.rows).chain().map(function(row) {
                            return [row.date, row.count];
                        }).object().value();
                        filter[mode] = fillGaps(dateCountMap, logic.days);
                        return filter[mode];
                    });
                }
                var result = [];
                if(!filter.update_time) {
                    result.push(retrieveMode('update_time'));
                }
                if(filter.resolveModifyDate && !filter.modify_date) {
                    result.push(retrieveMode('modify_date'));
                }
                return $q.all(result);
            }).value()).then(function() {
                $scope.model.data = _(filters).chain().filter(function(filter) {
                    return filter.type == 'flag' && filter.state && (filter.update_time || filter.modify_date);
                }).map(function(filter) {
                    var result = [];
                    if(filter.update_time && filter.update_time.length) {
                        result.push({
                            data: filter.update_time,
                            label: filter.text + ' (update time)'
                        });
                    }
                    if(filter.modify_date && filter.modify_date.length) {
                        result.push({
                            data: filter.modify_date,
                            label: filter.text + ' (modify date)'
                        });
                    }
                    return result;
                }).flatten().value();
                
                $scope.model.flotOptions.xaxis.barWidth = 0.25;
            });
        }
        
        var rebuildTimeout = null;
        var rebuildDefer = function() {
            if(rebuildTimeout) {
                $timeout.cancel(rebuildTimeout);
            }
            rebuildTimeout = $timeout(function() {
                rebuildTimeout = null;
                rebuild(true);
            }, 500);
        };
        
        var filtersWatch = $scope.$watch('model.wrapper.filters', function(newValue, oldValue) {
            if(oldValue == newValue) {
                return;
            }
            rebuild();
        }, true);
        
        var resolutionWatch = $scope.$watch('model.wrapper.resolution', function(newValue, oldValue) {
            if(oldValue == newValue) {
                return;
            }
            rebuildDefer();
        }, true);
        rebuild();
        
        return function() {
            if(rebuildTimeout) {
                $timeout.cancel(rebuildTimeout);
            }
            filtersWatch();
            resolutionWatch();
        };
    };
});