var m = angular.module('troubleshooting');

m.factory('securityHealthLogic', function($q, simpleSortableLogicCore, troubleshootDao) {
    return function($scope, logic) {

        var simpleFree = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return troubleshootDao.secAppsStatus($scope.model.columns);
            }
        });

        return function() {
            simpleFree();
        };
    };
});