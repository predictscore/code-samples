var m = angular.module('troubleshooting');

m.factory('downloadHistoryLogic', function(objectTypeDao, troubleshootDao, $q, modulesManager, entityTypes) {
    return function($scope, logic) {
        function fillGaps(dateCountMap, after) {
            var result = [];
            var current = moment().hours(12).minutes(0).seconds(0).milliseconds(0).add(-after + 1, 'days');
            for (var i = 0; i < after; i++) {
                var date = current.format('YYYY-MM-DD');
                result.push([parseInt(current.format('x'), 10), dateCountMap[date] || 0]);
                current.add(1, 'days');
            }
            return result;
        }
        
        $q.all([
            objectTypeDao.entities(),
            troubleshootDao.downloadHistory(logic.days),
            modulesManager.init()
        ]).then(function(responses) {
            var dateCountMaps = {};
            _(responses[1].data.rows).each(function(row) {
                dateCountMaps[row.file_entity_type] = dateCountMaps[row.file_entity_type] || {};
                dateCountMaps[row.file_entity_type][row.last_download] = row.sum;
            });
            $scope.model.allData = _(dateCountMaps).chain().map(function(dateCountMap, type) {
                var saas = modulesManager.cloudApp(entityTypes.toSaas(type));
                return {
                    data: fillGaps(dateCountMap, logic.days),
                    label: responses[0].data.entities[type] ? (saas.title + ' ' + responses[0].data.entities[type].label) : type
                };
            }).filter(function(d) {
                return !_(d).isNull();
            }).value();

            $scope.model.wrapper.filters = [{
                type: 'select-all',
                local: true
            }, {
                type: 'filter',
                local: true
            }];
            
            _($scope.model.allData).each(function(data) {
                $scope.model.wrapper.filters.push({
                    "type": "flag",
                    "state": true,
                    "text": data.label,
                    "value": data.label,
                    "local": true
                });
            });
        });
        
        var filtersWatch = $scope.$watch('model.wrapper.filters', function(filters) {
            if(!$scope.model.allData) {
                return;
            }
            $scope.model.data = _($scope.model.allData).filter(function(data) {
                var found = _(filters).find(function(filter) {
                    return filter.text == data.label;
                });
                return found.state;
            });
        }, true);
        
        return function() {
            filtersWatch();
        };
    };
});