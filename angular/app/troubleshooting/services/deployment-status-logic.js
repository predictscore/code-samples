var m = angular.module('troubleshooting');

m.factory('deploymentStatusLogic', function($q, configDao, modals, sideMenuManager) {

    function formatExpires (value) {
        if (!_(value).isNull()) {
            if (value < 0) {
                return 'Expired (' + (-value) + ' days ago)';
            } else {
                return value + ' days';
            }
        } else {
            return 'never';
        }
    }

    function parseOptionalInt (value) {
        var result = parseInt(value);
        if (!isFinite(result)) {
            result = null;
        }
        return result;
    }

    return function($scope, logic, reinit) {

        configDao.getDeploymentStatus(true).then(function (data) {

            $scope.model.data = [{
                key: 'Deployment Mode',
                value: data.deployment_mode
            }, {
                key: 'Association Date',
                value: data.association_date
            }, {
                key: 'POC duration in days',
                value: parseOptionalInt(data.poc_duration_in_days)
            }, {
                key: 'POC Expires In (calculated)',
                value: formatExpires(data.pocExpiresInDays)
            }, {
                key: 'POC Expiration Date (calculated)',
                value: data.pocExpirationDate
            }, {
                key: 'Paid Started',
                value: data.paid_start
            }, {
                key: 'Paid duration in days',
                value: parseOptionalInt(data.paid_duration_in_days)
            }, {
                key: 'Paid Expires In (calculated)',
                value: formatExpires(data.paidExpiresInDays)
            }, {
                key: 'Paid Expiration Date (calculated)',
                value: data.paidExpirationDate
            }, {
                button: {
                    label: 'Change',
                    'class': 'btn btn-blue',
                    onClick: function () {
                        return modals.params([{
                            lastPage: true,
                            params: [{
                                id: 'deployment_mode',
                                type: 'selector',
                                variants: ['poc', 'paid'],
                                data: data.deployment_mode,
                                label: 'Deployment Mode'
                            }, {
                                id: 'poc_duration_in_days',
                                type: 'integer',
                                data: parseOptionalInt(data.poc_duration_in_days),
                                label: 'POC duration in days',
                                enabled: {
                                    param: 'deployment_mode',
                                    equal: 'poc'
                                }
                            }, {
                                id: 'paid_duration_in_days',
                                type: 'integer',
                                data: parseOptionalInt(data.paid_duration_in_days),
                                label: 'Paid duration in days',
                                enabled: {
                                    param: 'deployment_mode',
                                    equal: 'paid'
                                }
                            }]
                        }], 'Change Deployment Configuration').then(function (newData) {
                            if (newData.deployment_mode === 'paid' && !data.paid_start) {
                                newData.paid_start = moment().toJSON();
                            }
                            return $q.all(_(newData).chain().map(function (value, param) {
                                if (data[param] === value) {
                                    return;
                                }
                                return configDao.setParam(param, value);
                            }).compact().value()).then(function () {
                                sideMenuManager.updateMenus();
                                reinit();
                            });
                        });
                    }
                }
            }];
        });

        return function() {
        };
    };
});