var m = angular.module('troubleshooting');

m.factory('saasSelectorLogic', function (path, modulesManager, widgetPolling, widgetSettings) {
    return function ($scope, logic, reinitLogic) {

        var icons = _(modulesManager.cloudApps()).chain().filter(function (saasApp) {
            return saasApp.isSaas;
        }).map(function (saasApp) {
            return _(saasApp).pick('name', 'title', 'img');
        }).value();
        icons.unshift({
            img: path('security-stack').img('applications/super-dashboard.png'),
            title: 'All',
            name: 'all'
        });

        function isValidApp(appName) {
            return !!_(icons).find(function (icon) {
                return icon.name === appName;
            });
        }

        function setActiveApp(appName) {
            var app = _(icons).find(function (icon) {
                return icon.name === appName;
            });
            if (!app) {
                return;
            }
            widgetPolling.scope()[logic.idVariableName] = appName;
            widgetPolling.scope()[logic.objVariableName] = app;
            widgetSettings.param($scope.model.uuid, 'selectedSaas', appName);
        }

        function rebuildIcons() {
            $scope.model.lines = [{
                display: 'linked-text',
                text: _(icons).map(function (icon) {
                    var result = {
                        tooltip: icon.title,
                        'class': {},
                        clickEvent: {
                            name: logic.changeEventName,
                            app: icon.name
                        }
                    };
                    if (icon.img) {
                        result.display ='img';
                        result.path = icon.img;
                    } else {
                        result.display = 'text';
                        result.text = icon.text || '';
                    }
                    if (icon.class) {
                        if (_(icon.class).isString()) {
                            result['class'][icon.class] = true;
                        } else {
                            result['class'] = angular.copy(icon.class);
                        }
                    }
                    if (icon.name === widgetPolling.scope()[logic.idVariableName]) {
                        result['class'].active = true;
                    }
                    return '#' + JSON.stringify(result);
                }).join('')
            }];
        }
        if (!isValidApp(widgetPolling.scope()[logic.idVariableName])) {
            setActiveApp(icons[0].name);
        }

        if (!_($scope.model.lines).isArray()) {
            rebuildIcons();
        }
        var saasChangeStopListen = $scope.$on(logic.changeEventName, function (event, data) {
            setActiveApp(data.app);
            rebuildIcons();
        });

        return function () {
            saasChangeStopListen();
        };
    };
});
