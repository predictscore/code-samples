var m = angular.module('troubleshooting');

m.factory('saasHealthLogic', function($q, simpleSortableLogicCore, troubleshootDao) {
    return function($scope, logic) {

        var simpleFree = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return troubleshootDao.saasSummary($scope.model.columns);
            }
        });

        return function() {
            simpleFree();
        };
    };
});