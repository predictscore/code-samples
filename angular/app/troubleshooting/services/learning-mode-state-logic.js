var m = angular.module('troubleshooting');

m.factory('learningModeStateLogic', function($q, modulesManager, simpleSortableLogicCore, defaultListConverter, modulesDao, modals) {
    return function($scope, logic, reinit) {

        var simpleFree = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return modulesDao.getModules().converter(function (input) {
                    var rows = _(input.data.apps_items.modules).filter(function (module) {
                        return module.learning_mode_enable;
                    });

                    return $q.when({
                        data: {
                            rows: rows,
                            total_rows: rows.length
                        }
                    });
                }).converter(defaultListConverter, {
                    columns: $scope.model.columns,
                    converters: {
                        stateSwitchConverter: function (text, columnId, columnsMap, args) {
                            if (columnsMap.state !== 'active') {
                                return '#' + JSON.stringify({
                                        display: 'text',
                                        text: '',
                                        'class': 'fa fa-ban fa-2x',
                                        tooltip: 'App is disabled. Learning mode can\'t be managed'
                                    });
                            }
                            if (text) {
                                return '#' + JSON.stringify({
                                        display: 'text',
                                        'class': 'switch-on cursor-pointer',
                                        text: 'On',
                                        clickEvent: {
                                            name: args.eventName,
                                            currentState: text,
                                            app: columnsMap.name,
                                            appLabel: columnsMap.label
                                        }
                                    });
                            }
                            return '#' + JSON.stringify({
                                    display: 'text',
                                    'class': 'switch-off cursor-pointer',
                                    text: 'Off',
                                    clickEvent: {
                                        name: args.eventName,
                                        currentState: text,
                                        app: columnsMap.name,
                                        appLabel: columnsMap.label
                                    }
                                });

                        }
                    }
                });
            }
        });
        var learnClickListener = $scope.$on('change-app-learning-mode', function (event, data) {
            modals.confirm('Are you sure you want to <strong>turn ' + (data.currentState ? 'off' : 'on') + '</strong> learning mode for ' + data.appLabel + '?')
                .then(function () {
                    return modulesDao.learningModeEnable(data.app, !data.currentState);
                }).then(function () {
                $scope.tableHelper.reload();
            });
        });


        return function() {
            simpleFree();
            learnClickListener();
        };
    };
});