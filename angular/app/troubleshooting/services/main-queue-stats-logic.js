var m = angular.module('troubleshooting');

m.factory('mainQueueStatsLogic', function(modulesManager, troubleshootDao, $q) {
    return function($scope, logic) {
        $scope.model.flotOptions.tooltipOpts.content = function(label, xval, yval, flotItem) {
            var trend = null;
            if(flotItem.dataIndex > 0) {
                trend = yval / (flotItem.series.data[flotItem.dataIndex -1][1] / 100) - 100;
            }
            var result = label + ': ' + yval; 
            if(_(trend).isFinite()) {
                trend = trend.toFixed(2);
                if(trend > 0) {
                    trend = '+' + trend;
                }
                result += ' (' + trend + '%)';
            }
            return result;
        };
        troubleshootDao.mainQueueStats(logic.days, logic.group).then(function(response) {
            var dataMap = {};
            _(response.data.rows).each(function(row) {
                var key = row.entity_type;
                if(!logic.group) {
                    key += ' + ' + row.src_entity_type;
                }
                dataMap[key] = dataMap[key] || [];
                dataMap[key].push([parseInt(moment(row.update_time).format('x'), 10), row.count]);
            });
            $scope.model.allData = _(dataMap).map(function(value, key) {
                return {
                    data: _(value).sortBy(function(v) {
                        return v[0];
                    }),
                    label: key
                };
            });
            
            $scope.model.wrapper.filters = [{
                type: 'select-all',
                local: true
            }, {
                type: 'filter',
                local: true
            }];
            
            _($scope.model.allData).each(function(data) {
                $scope.model.wrapper.filters.push({
                    "type": "flag",
                    "state": true,
                    "text": data.label,
                    "value": data.label,
                    "local": true
                });
            });
        });
        
        var filtersWatch = $scope.$watch('model.wrapper.filters', function(filters) {
            if(!$scope.model.allData) {
                return;
            }
            $scope.model.data = _($scope.model.allData).filter(function(data) {
                var found = _(filters).find(function(filter) {
                    return filter.text == data.label;
                });
                return found.state;
            });
        }, true);
        
        return function() {
            filtersWatch();
        };
    };
});