var m = angular.module('troubleshooting');

m.factory('licensesLogic', function($q, modulesManager, simpleSortableLogicCore, defaultListConverter, modulesDao, modals) {
    return function($scope, logic) {

        var simpleFree = simpleSortableLogicCore($scope, logic, {
            retrieve: function() {
                return modulesDao.getModules().converter(function (input) {
                    var rows = _(input.data.apps_items.modules).chain().filter(function (module) {
                        return module.license_info;
                    }).sortBy('label').sortBy('state').value();

                    return $q.when({
                        data: {
                            rows: rows,
                            total_rows: rows.length
                        }
                    });
                }).converter(defaultListConverter, {
                    columns: $scope.model.columns
                });
            }
        });

        var licenseUpdateUnbind = $scope.$on('update-license', function (event, data) {
            var appName = data.attributes.app;
            modulesDao.getModules().then(function (response) {
                var app = _(response.data.apps_items.modules).find(function (module) {
                    return module.name === appName;
                });
                return modals.params([{
                    lastPage: true,
                    params: [{
                        id: "license_type",
                        label: "License Type",
                        type: "selector",
                        variants: [{
                            id: 'demo',
                            label: 'demo'
                        }, {
                            id: 'paid',
                            label: 'paid'
                        }],
                        data: app.license_info.license_type
                    }, {
                        id: "expire_at",
                        label: "Expire At",
                        type: "datetime",
                        data: moment.utc(app.license_info.expire_at).toDate(),
                        options: {
                            dateOnly: true
                        }
                    }, {
                        id: "license_key",
                        label: "Set new paid license key (optional)",
                        type: "string",
                        enabled: {
                            param: 'license_type',
                            equal: 'paid'
                        }
                    }]
                }], "Change license options").then(function (data) {
                    var saveData = {
                        license_type: data.license_type,
                        expire_at: moment(data.expire_at).toJSON()
                    };
                    if (data.license_type === 'paid' && data.license_key) {
                        saveData.license_key = data.license_key;
                    }
                    return modulesDao.adminLicense(appName, saveData).then(function () {
                        $scope.tableHelper.reload();
                    });
                });

            });
        });

        return function() {
            simpleFree();
            licenseUpdateUnbind();
        };
    };
});