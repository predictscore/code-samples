var m = angular.module('troubleshooting');

m.factory('secAppStatsLogic', function(modulesManager, troubleshootDao, $q) {
    return function($scope, logic) {
        function fillGaps(dateCountMap, after) {
            var result = [];
            var current = moment().hours(12).minutes(0).seconds(0).milliseconds(0).add(-after + 1, 'days');
            for (var i = 0; i < after; i++) {
                var date = current.format('YYYY-MM-DD');
                result.push([parseInt(current.format('x'), 10), dateCountMap[date] || 0]);
                current.add(1, 'days');
            }
            return result;
        }
        
        modulesManager.init().then(function() {
            var activeSecApps = modulesManager.activeSecApps(true);
            $q.all(_(activeSecApps).map(function(module) {
                return troubleshootDao.secAppStats(module, logic.days).onRequestFail(function() {
                    return $q.when(null);
                });
            })).then(function(responses) {
                $scope.model.allData = _(responses).chain().map(function(response, idx) {
                    if (_(response).isNull()) {
                        return null;
                    }
                    var dateCountMap = _(response.data.rows).chain().map(function(row) {
                        return [row.update_date, row.count];
                    }).object().value();
                    return {
                        data: fillGaps(dateCountMap, logic.days),
                        label: modulesManager.app(activeSecApps[idx]).label
                    };
                }).filter(function(d) {
                    return !_(d).isNull();
                }).value();

                $scope.model.wrapper.filters = [{
                    type: 'select-all',
                    local: true
                }, {
                    type: 'filter',
                    local: true
                }];
                
                _($scope.model.allData).each(function(data) {
                    $scope.model.wrapper.filters.push({
                        "type": "flag",
                        "state": true,
                        "text": data.label,
                        "value": data.label,
                        "local": true
                    });
                });
            });
        });
        
        var filtersWatch = $scope.$watch('model.wrapper.filters', function(filters) {
            if(!$scope.model.allData) {
                return;
            }
            $scope.model.data = _($scope.model.allData).filter(function(data) {
                var found = _(filters).find(function(filter) {
                    return filter.text == data.label;
                });
                return found.state;
            });
        }, true);
        
        return function() {
            filtersWatch();
        };
    };
});