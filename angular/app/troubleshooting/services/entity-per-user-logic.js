var m = angular.module('troubleshooting');

m.factory('entityPerUserLogic', function(troubleshootDao, $q, entityTypes, widgetPolling, $timeout) {
    return function($scope, logic) {
        var types = {
            'office365_emails': {
                'web_notifications_per_user': {
                    label: 'Web notifications'
                },
                'emails_per_user': {
                    label: 'Emails',
                    modes: ['ReceivedDateTime', 'CreatedDateTime', 'LastModifiedDateTime', 'update_time', 'SentDateTime']
                }
            }
        };
        
        $scope.model.flotOptions.tooltipOpts.content = function(label, x, y) {
            return label + ': ' + y + ' user(s) have ' + $scope.ticksReverse[x] + ' entities';
        };
        
        var prevPow = null;
        function rebuildTicks(basePow, maxYTick) {
            $scope.ticks = {};
            _(_.range(0, 11)).each(function(x) {
                $scope.ticks[Math.pow(basePow, x) + '-' + (Math.pow(basePow, x + 1) - 1)] = x;
            });
            $scope.ticks['> ' + Math.pow(basePow, 11)] = 11;
            $scope.model.flotOptions.xaxis.ticks = _($scope.ticks).map(function(tick, label) {
                return [tick, label];
            });
            $scope.ticksReverse = _($scope.ticks).invert();
            prevPow = basePow;
            
            var yTicks = [0];
            var pow = 0;
            while(true) {
                var tick = Math.pow(10, pow++);
                yTicks.push(tick);
                if(tick > maxYTick) {
                    break;
                }
            }
            $scope.model.flotOptions.yaxis = {
                ticks: yTicks,
                transform: function(v) {
                    // v = v + 1;
                    return Math.log(v + 1);
                },
            };
        }
        
        var now = moment();
        $scope.model.wrapper.dateInterval.to = now.format('YYYY-MM-DD HH:mm:ss');
        $scope.model.wrapper.dateInterval.from = now.add(-1, 'month').format('YYYY-MM-DD HH:mm:ss');
        
        var objectTypes = widgetPolling.scope().objectTypes;
        
        var filters = [];
        _(objectTypes.data.saasList).each(function(saasData, saasId) {
            if(!types[saasId]) {
                return;
            }
            _(types[saasId]).each(function(typeData, typeId) {
                var modes = typeData.modes || [undefined];
                _(modes).each(function(mode) {
                    var label = saasData.label + ' - ' + typeData.label;
                    if(mode) {
                        label += ' (' + mode + ')';
                    }
                    filters.push({
                        "type": "flag",
                        "state": false,
                        "text": label,
                        "value": {
                            saas: saasId,
                            type: typeId,
                            mode: mode
                        },
                        "local": true
                    });
                });
            });
        });
        
        $scope.ticksFilter = {
            type: 'number',
            state: 5,
            text: 'Pow base',
            min: 2,
            max: 10,
            local: true
        };
        $scope.model.wrapper.filters.push($scope.ticksFilter);
        _(filters).chain().sortBy('text').each(function(filter) {
            $scope.model.wrapper.filters.push(filter);
        });
        
        function rebuild(force) {
            var filters = $scope.model.wrapper.filters;
            if(force) {
                _(filters).each(function(filter) {
                    filter.data = undefined;
                });
            }
            
            $q.all(_(filters).chain().filter(function(filter) {
                return filter.type == 'flag' && filter.state && !filter.data;
            }).map(function(filter) {
                var dateInterval = $scope.model.wrapper.dateInterval;
                return troubleshootDao.entityPerUser(filter.value.saas, filter.value.type, filter.value.mode, dateInterval.from, dateInterval.to, $scope.ticksFilter.state).then(function(response) {
                    var maxYTick = _(response.data.rows).reduce(function(memo, row) {
                        return Math.max(memo, row.users_count);
                    }, 10);
                    rebuildTicks($scope.ticksFilter.state, maxYTick);
                    filter.data = _(response.data.rows).map(function(row) {
                        if(_(row.r_max).isNull()) {
                            return [$scope.ticks['> ' + row.r_min], row.users_count];
                        } else {
                            return [$scope.ticks[row.r_min + '-' + row.r_max], row.users_count];
                        }
                    });
                });
            }).value()).then(function() {
                var filtered = _(filters).filter(function(filter) {
                    return filter.type == 'flag' && filter.state && filter.data;
                });
                $scope.model.data = _(filtered).map(function(filter, idx) {
                    return {
                        data: filter.data,
                        label: filter.text,
                        bars: {
                            order: idx + 1,
                            barWidth: 0.5 / filtered.length
                        }
                    };
                });
            });
        }
        
        var filtersWatch = $scope.$watch('model.wrapper.filters', function(newValue, oldValue) {
            if(oldValue == newValue) {
                return;
            }
            rebuild(prevPow != $scope.ticksFilter.state);
        }, true);
        
        var dateIntervalWatch = $scope.$watch('model.wrapper.dateInterval', function(newValue, oldValue) {
            if(oldValue == newValue) {
                return;
            }
            rebuild(true);
        }, true);
        
        rebuild();
        
        return function() {
            filtersWatch();  
            dateIntervalWatch();
        };
    };
});