var m = angular.module('app', ['ngRoute', 'ngCookies', 'utils', 'ui.slimscroll', 'auth', 'dashboard', 'profiles', 'app-store', 'policy', 'monospaced.mousewheel', 'dao', 'user-management', 'ngSanitize', 'viewer', 'security-stack', 'angular-data.DSCacheFactory', 'report', 'settings', 'advanced-data-search', 'wizard', 'templates', 'alerts', 'configuration', 'quarantine', 'troubleshooting', 'scan-monitor', 'devices', 'email-report', 'gui-v2'], function($rootScopeProvider) {
    $rootScopeProvider.digestTtl(20); 
});

m.config(function ($routeProvider, $locationProvider, $httpProvider, $compileProvider) {
    function templatePath(moduleName, controllerName) {
        return '/assets/' + moduleName + '/controllers/' + controllerName + '/' + controllerName + '.html';
    }

    var authRequired = {
        currentUser: ['$q', 'avananUserDao', 'sideMenuManager', 'idleManager', function($q, avananUserDao, sideMenuManager, idleManager) {
            var deferred = $q.defer();
            avananUserDao.current().then(function(response) {
                sideMenuManager.currentUser(response.data);
                idleManager.init().then(function() {
                    deferred.resolve(response);
                });
            }, deferred.reject);
            return deferred.promise;
        }],
        featureResolver: ['feature', function(feature) {
            return feature.init();
        }],
        modulesManagerResolver: ['modulesManager', function(modulesManager) {
            return modulesManager.init();
        }],
        wizardHelperResolver: ['wizardHelper', function(wizardHelper) {
            return wizardHelper.init();
        }]
    };

    var noAuthRequired = {
        userNotExist: ['$q', 'avananUserDao', function($q, avananUserDao) {
            var deferred = $q.defer();
            avananUserDao.current().then(function() {
                deferred.reject();
            }, function() {
                deferred.resolve();
            });
            return deferred.promise;
        }]
    };

    function title(text) {
        return htmlTitle('<strong>' + text + '</strong>');
    }

    function htmlTitle(text) {
        return '#' + JSON.stringify({
                display: 'html',
                text: text
            });
    }

    if(window.guiVersion === 1) {
        $routeProvider
            .when('/dashboard', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'DashboardController',
                title: title('Dashboard'),
                name: 'dashboard',
                resolve: authRequired
            })
            .when('/dashboard/:module/', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'DashboardController',
                title: title('Dashboard'),
                name: 'dashboard',
                resolve: authRequired
            })
            .when('/application/:module/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'ApplicationProfileController',
                title: title('Application profile'),
                name: 'application',
                resolve: authRequired
            })
            .when('/user/:module/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'UserProfileController',
                title: title('User profile'),
                name: 'user',
                resolve: authRequired
            })
            .when('/folder/:module/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'FolderProfileController',
                title: title('Folder profile'),
                name: 'folder',
                resolve: authRequired
            })
            .when('/file/:module/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'FileProfileController',
                title: title('File profile'),
                name: 'file',
                resolve: authRequired
            })
            .when('/email/:module/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'EmailProfileController',
                title: title('Email profile'),
                name: 'email',
                resolve: authRequired
            })
            .when('/organization/:module/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'OrganizationProfileController',
                title: title('Organization profile'),
                name: 'organization',
                resolve: authRequired
            })
            .when('/group/:module/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'GroupProfileController',
                title: title('Group profile'),
                name: 'group',
                resolve: authRequired
            })
            .when('/site/:module/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'SiteProfileController',
                title: title('Site profile'),
                name: 'site',
                resolve: authRequired
            })
            .when('/ip/:module/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'IpProfileController',
                title: title('IP profile'),
                name: 'ip',
                resolve: authRequired
            })
            .when('/bucket/:module/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'BucketProfileController',
                title: title('Bucket profile'),
                name: 'bucket',
                resolve: authRequired
            })
            .when('/project/:module/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'ProjectProfileController',
                title: title('Project profile'),
                name: 'project',
                resolve: authRequired
            })
            .when('/iam-user/:module/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'UserProfileController',
                title: title('IAM User profile'),
                name: 'iam-user',
                resolve: authRequired
            })
            .when('/iam-group/:module/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'GroupProfileController',
                title: title('IAM Group profile'),
                name: 'iam-group',
                resolve: authRequired
            })
            .when('/instance/:module/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'InstanceProfileController',
                title: title('Instance profile'),
                name: 'instance',
                resolve: authRequired
            })
            .when('/anomaly/:module/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'AnomalyProfileController',
                title: title('Anomaly details'),
                name: 'anomaly',
                resolve: authRequired
            })
            .when('/viewer/:module/:id', {
                templateUrl: templatePath('viewer', 'viewer'),
                controller: 'ViewerController',
                title: title('Viewer'),
                name: 'viewer',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/advanced-viewer/:module/:type/:preFilter', {
                templateUrl: templatePath('viewer', 'advanced-viewer'),
                controller: 'AdvancedViewerController',
                title: title('Viewer'),
                name: 'advanced-viewer',
                resolve: authRequired,
                reloadOnSearch: false
            })

            .when('/auth/:page', {
                templateUrl: templatePath('auth', 'auth'),
                controller: 'AuthController',
                title: title('Login'),
                name: 'auth',
                fullscreen: true,
                resolve: noAuthRequired
            })
            .when('/login', {
                redirectTo: '/auth/login'
            })
            .when('/password-reset', {
                redirectTo: '/auth/password-reset'
            })
            .when('/app-store/:filter', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'AppStoreController',
                title: 'Applications Store',
                fullTitle: htmlTitle('<strong>Extend the avanan platform</strong> with these 3rd party services'),
                name: 'app-store-filter',
                accessor: 'appStore',
                resolve: authRequired
            })
            .when('/app-store', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'AppStoreController',
                title: 'Applications Store',
                fullTitle: htmlTitle('<strong>Extend the avanan platform</strong> with these 3rd party services'),
                name: 'app-store',
                accessor: 'appStore',
                resolve: authRequired
            })
            .when('/cloud-apps', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'AppStoreController',
                title: title('Cloud applications store'),
                name: 'cloud-apps',
                accessor: 'cloudApps',
                resolve: authRequired
            })
            .when('/user-management', {
                templateUrl: templatePath('user-management', 'user-list'),
                controller: 'UserListController',
                title: title('Users management'),
                name: 'user-management',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/wizard-user-management', {
                templateUrl: templatePath('user-management', 'user-list'),
                controller: 'UserListController',
                title: title('Users management'),
                name: 'wizard-user-management',
                resolve: authRequired,
                reloadOnSearch: false,
                fullscreen: true
            })
            .when('/configuration/dlp', {
                templateUrl: templatePath('configuration', 'dlp-configuration-list'),
                controller: 'DlpConfigurationListController',
                title: title('Smart Search'),
                name: 'dlp-configuration',
                resolve: authRequired,
                reloadOnSearch: false
            })

            .when('/configuration/anomaly-detection', {
                templateUrl: templatePath('configuration', 'anomaly-detection-config'),
                controller: 'AnomalyDetectionConfigController',
                title: title('Anomaly Detection'),
                name: 'anomaly-detection',
                resolve: authRequired,
                reloadOnSearch: false
            })

            .when('/report/create', {
                templateUrl: templatePath('report', 'report-create'),
                controller: 'ReportCreateController',
                title: title('Report create'),
                name: 'report-create',
                resolve: authRequired
            })
            .when('/report/edit/:id', {
                templateUrl: templatePath('report', 'report'),
                controller: 'ReportController',
                title: title('Report'),
                name: 'report-edit',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/report', {
                templateUrl: templatePath('report', 'report-list'),
                controller: 'ReportListController',
                title: title('Reports'),
                name: 'report-list',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/policy/create', {
                templateUrl: templatePath('policy', 'policy-create'),
                controller: 'PolicyCreateController',
                title: title('Query create'),
                name: 'policy-create',
                resolve: authRequired,
                internalModuleChange: false,
                reloadOnSearch: false,
                hideSaasSwitch: true
            })
            .when('/policy/edit/:id', {
                templateUrl: templatePath('policy', 'policy'),
                controller: 'PolicyController',
                title: title('Query'),
                name: 'policy-edit',
                resolve: authRequired,
                reloadOnSearch: false,
                hideSaasSwitch: true,
                hiddenOverflow: true,
                noPaddings: true
            })
            .when('/policy/dashboard', {
                templateUrl: templatePath('policy', 'policy-dashboard'),
                controller: 'PolicyDashboardController',
                title: title('Queries dashboard'),
                name: 'policy-dashboard',
                resolve: authRequired,
                reloadOnSearch: false,
                hideSaasSwitch: true
            })
            .when('/policy', {
                templateUrl: templatePath('policy', 'policy-list'),
                controller: 'PolicyListController',
                title: title('Analytics'),
                name: 'policy-list',
                resolve: authRequired,
                reloadOnSearch: false,
                hideSaasSwitch: true
            })
            .when('/policy/root-cause-view/:module/:id', {
                templateUrl: templatePath('policy', 'policy-root-cause-view'),
                controller: 'PolicyRootCauseViewController',
                title: title('Query'),
                name: 'policy-root-cause-view',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/policy/allows/:id', {
                templateUrl: templatePath('policy', 'policy-allows'),
                controller: 'PolicyAllowsController',
                title: title('Query allows'),
                name: 'policy-allows',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/drive-explorer/:module', {
                templateUrl: templatePath('advanced-data-search', 'advanced-data-search'),
                controller: 'AdvancedDataSearchController',
                title: title('Drive explorer'),
                name: 'drive-explorer',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/mail-explorer/:module', {
                templateUrl: templatePath('advanced-data-search', 'advanced-data-search'),
                controller: 'AdvancedDataSearchController',
                title: title('Mail Explorer'),
                name: 'mail-explorer',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/security-stack', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'SecurityStackController',
                title: title('Security stack'),
                name: 'security-stack',
                resolve: authRequired
            })
            .when('/settings', {
                templateUrl: templatePath('settings', 'settings'),
                controller: 'SettingsController',
                title: title('Settings'),
                name: 'settings',
                resolve: authRequired
            })
            .when('/wizard', {
                templateUrl: templatePath('wizard', 'wizard'),
                controller: 'WizardController',
                title: title('Wizard'),
                name: 'wizard',
                fullscreen: true,
                resolve: authRequired
            })
            .when('/alerts', {
                templateUrl: templatePath('alerts', 'alerts'),
                controller: 'AlertsController',
                title: title('Alerts'),
                name: 'alerts',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/quarantine/:module/:type?', {
                templateUrl: templatePath('quarantine', 'quarantine'),
                controller: 'QuarantineController',
                title: title('Quarantined Files'),
                name: 'quarantine',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/authorize-success', {
                templateUrl: templatePath('auth', 'authorize-error'),
                controller: 'AuthorizeErrorController',
                title: title('Successful Service Authorization'),
                name: 'authorize-error',
                fullscreen: true
            })
            .when('/authorize-error', {
                templateUrl: templatePath('auth', 'authorize-error'),
                controller: 'AuthorizeErrorController',
                title: title('Service Authorization Error'),
                name: 'authorize-error',
                fullscreen: true
            })
            .when('/:module/inline-policy/:type?', {
                templateUrl: templatePath('policy', 'inline-policy'),
                controller: 'InlinePolicyController',
                title: title('Inline Query'),
                name: 'inline-policy',
                resolve: authRequired,
                internalModuleChange: false,
                reloadOnSearch: false,
                hideSaasSwitch: true
            })
            .when('/sec-app-stats', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'SecAppStatsController',
                title: title('Security applications statistics'),
                name: 'sec-app-stats',
                resolve: authRequired
            })
            .when('/main-queue-stats', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'MainQueueStatsController',
                title: title('Main queue statistics'),
                name: 'main-queue-stats',
                resolve: authRequired
            })
            .when('/download-history', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'DownloadHistoryController',
                title: title('Download History'),
                name: 'download-history',
                resolve: authRequired
            })
            .when('/scan-monitor', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'ScanMonitorController',
                title: title('Scanning status'),
                name: 'scan-monitor',
                resolve: authRequired
            })
            .when('/licenses', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'LicensesController',
                title: title('Licenses status'),
                name: 'licenses',
                resolve: authRequired
            })
            .when('/system-log', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'SystemLogController',
                title: title('System Log'),
                name: 'system-log',
                resolve: authRequired
            })
            .when('/entity-trend', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'EntityTrendController',
                title: title('Entity trend'),
                name: 'entity-trend',
                resolve: authRequired
            })
            .when('/entity-per-user', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'EntityPerUserController',
                title: title('Entity per user'),
                name: 'entity-per-user',
                resolve: authRequired
            })
            .when('/health-check', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'HealthCheckController',
                title: title('Health Status'),
                name: 'health-check',
                resolve: authRequired
            })
            .when('/devices', {
                templateUrl: templatePath('devices', 'devices-list'),
                controller: 'DevicesListController',
                title: title('Devices'),
                name: 'devices-list',
                resolve: authRequired
            })
            .when('/email-restore/:module/:msg', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'EmailRestoreController',
                title: title('Email restore'),
                name: 'email-restore',
                resolve: authRequired
            })
            .when('/email-domains', {
                templateUrl: templatePath('settings', 'email-domains'),
                controller: 'EmailDomainsController',
                title: title('Email Domains Management'),
                name: 'email-domains',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/learning-mode-state', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'LearningModeStateController',
                title: title('Learning mode state'),
                name: 'learning-mode-state',
                resolve: authRequired
            })
            .when('/sec-tool-exceptions/:module/:type?', {
                templateUrl: templatePath('configuration', 'sec-tool-exceptions'),
                controller: 'SecToolExceptionsController',
                title: title('Security Tools Exceptions'),
                name: 'sec-tool-exceptions',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/configuration/ap-exceptions/:type?', {
                templateUrl: templatePath('configuration', 'ap-avanan-exceptions'),
                controller: 'ApAvananExceptionsController',
                title: title('Smart-Phish Exceptions'),
                name: 'ap-avanan-exceptions',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/inline-messages/:module/', {
                templateUrl: templatePath('troubleshooting', 'inline-messages'),
                controller: 'InlineMessagesController',
                title: title('Inline messages'),
                name: 'inline-messages',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/email-report/:reportId', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'EmailReportController',
                title: title('Email Report'),
                fullscreen: true,
                hideMessenger: true,
                name: 'email-report',
                resolve: authRequired
            })
            .when('/system-config', {
                templateUrl: templatePath('settings', 'system-config'),
                controller: 'SystemConfigController',
                title: title('System Configuration'),
                name: 'system-config',
                resolve: authRequired
            })
            .when('/password-change', {
                templateUrl: templatePath('auth', 'password-change'),
                controller: 'PasswordChangeController',
                title: title('Change Password'),
                name: 'password-change',
                fullscreen: true
            })
            .when('/configuration/solgate-exceptions', {
                templateUrl: templatePath('configuration', 'solgate-exceptions'),
                controller: 'SolgateExceptionsController',
                title: title('Solgate Exceptions'),
                name: 'solgate-exceptions',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/profile/:entityType/:id*', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'ProfileController',
                title: title('Profile'),
                name: 'profile',
                resolve: authRequired
            })
            .otherwise({
                redirectTo: '/dashboard'
            });
    } else if(window.guiVersion === 2) {
        $routeProvider
            .when('/', {
                templateUrl: templatePath('gui-v2', 'dashboard-v2'),
                controller: 'DashboardControllerV2',
                title: 'Dashboard',
                name: 'dashboard',
                resolve: authRequired
            })
            .when('/policy-rules', {
                templateUrl: templatePath('gui-v2', 'policy-rules'),
                controller: 'PolicyRulesController',
                title: 'Policy',
                name: 'policy-rules',
                reloadOnSearch: false,
                resolve: authRequired
            })
            .when('/policy/add-rule', {
                templateUrl: templatePath('gui-v2', 'policy-rule-edit'),
                controller: 'PolicyRuleEditController',
                title: 'Policy',
                name: 'policy-rule-add',
                mode: 'create',
                resolve: authRequired
            })
            .when('/policy/edit-rule/:id', {
                templateUrl: templatePath('gui-v2', 'policy-rule-edit'),
                controller: 'PolicyRuleEditController',
                title: 'Policy',
                name: 'policy-rule-edit',
                resolve: authRequired
            })
            .when('/app-store', {
                templateUrl: templatePath('gui-v2', 'app-store-v2'),
                controller: 'AppStoreV2Controller',
                title: 'Applications Store',
                name: 'app-store',
                accessor: 'appStore',
                resolve: authRequired
            })
            .when('/cloud-apps', {
                templateUrl: templatePath('gui-v2', 'app-store-v2'),
                controller: 'AppStoreV2Controller',
                title: 'Cloud Store',
                name: 'cloud-apps',
                accessor: 'cloudApps',
                resolve: authRequired
            })
            .when('/security-events', {
                templateUrl: templatePath('gui-v2', 'security-events'),
                controller: 'SecurityEventsController',
                title: 'Events',
                name: 'security-events',
                reloadOnSearch: false,
                resolve: authRequired
            })
            .when('/quarantine/:module/:type?', {
                templateUrl: templatePath('gui-v2', 'quarantine-v2'),
                controller: 'QuarantineControllerV2',
                title: 'Quarantine',
                name: 'quarantine',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/configuration', {
                templateUrl: templatePath('example', 'example'),
                controller: 'ExampleController',
                title: 'Configuration',
                name: 'configuration',
                resolve: authRequired
            })
            .when('/example', {
                templateUrl: templatePath('example', 'example'),
                controller: 'ExampleController',
                title: 'Example',
                name: 'example'
            })
            .when('/settings', {
                templateUrl: templatePath('gui-v2', 'settings-v2'),
                controller: 'SettingsControllerV2',
                title: title('Settings'),
                name: 'settings',
                resolve: authRequired
            })
            .when('/wizard', {
                templateUrl: templatePath('gui-v2', 'wizard-v2'),
                controller: 'WizardControllerV2',
                title: 'Wizard',
                name: 'wizard',
                fullscreen: true
            })
            .when('/email-domains', {
                templateUrl: templatePath('settings', 'email-domains'),
                controller: 'EmailDomainsController',
                title: title('Email Domains Management'),
                name: 'email-domains',
                resolve: authRequired,
                scrollOnPage: true
            })
            .when('/user-management', {
                templateUrl: templatePath('gui-v2', 'user-list-v2'),
                controller: 'UserListControllerV2',
                title: title('Users management'),
                name: 'user-management',
                resolve: authRequired,
                reloadOnSearch: false
            })
            .when('/wizard-user-management', {
                templateUrl: templatePath('gui-v2', 'user-list-v2'),
                controller: 'UserListControllerV2',
                title: title('Users management'),
                name: 'wizard-user-management',
                resolve: authRequired,
                reloadOnSearch: false,
                fullscreen: true
            })
            .when('/sec-app-stats', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'SecAppStatsController',
                title: title('Security applications statistics'),
                name: 'sec-app-stats',
                resolve: authRequired,
                scrollOnPage: true
            })
            .when('/main-queue-stats', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'MainQueueStatsController',
                title: title('Main queue statistics'),
                name: 'main-queue-stats',
                resolve: authRequired,
                scrollOnPage: true
            })
            .when('/download-history', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'DownloadHistoryController',
                title: title('Download History'),
                name: 'download-history',
                resolve: authRequired,
                scrollOnPage: true
            })
            .when('/entity-trend', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'EntityTrendController',
                title: title('Entity trend'),
                name: 'entity-trend',
                resolve: authRequired,
                scrollOnPage: true
            })
            .when('/entity-per-user', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'EntityPerUserController',
                title: title('Entity per user'),
                name: 'entity-per-user',
                resolve: authRequired,
                scrollOnPage: true
            })
            .when('/health-check', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'HealthCheckController',
                title: title('Health Status'),
                name: 'health-check',
                resolve: authRequired,
                scrollOnPage: true
            })
            .when('/scan-monitor', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'ScanMonitorController',
                title: title('Scanning status'),
                name: 'scan-monitor',
                resolve: authRequired,
                scrollOnPage: true
            })
            .when('/licenses', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'LicensesController',
                title: title('Licenses status'),
                name: 'licenses',
                resolve: authRequired,
                scrollOnPage: true
            })
            .when('/learning-mode-state', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'LearningModeStateController',
                title: title('Learning mode state'),
                name: 'learning-mode-state',
                resolve: authRequired,
                scrollOnPage: true
            })
            .when('/inline-messages/:module/', {
                templateUrl: templatePath('troubleshooting', 'inline-messages'),
                controller: 'InlineMessagesController',
                title: title('Inline messages'),
                name: 'inline-messages',
                resolve: authRequired,
                reloadOnSearch: false,
                scrollOnPage: true
            })
            .when('/viewer/:module/:id', {
                templateUrl: templatePath('viewer', 'viewer'),
                controller: 'ViewerController',
                title: title('Viewer'),
                name: 'viewer',
                resolve: authRequired,
                reloadOnSearch: false,
                scrollOnPage: true
            })
            .when('/system-config', {
                templateUrl: templatePath('settings', 'system-config'),
                controller: 'SystemConfigController',
                title: title('System Configuration'),
                name: 'system-config',
                resolve: authRequired,
                scrollOnPage: true
            })
            .when('/password-change', {
                templateUrl: templatePath('gui-v2', 'password-change-v2'),
                controller: 'PasswordChangeControllerV2',
                title: title('Change Password'),
                name: 'password-change',
                fullscreen: true
            })
            .when('/auth/:page', {
                templateUrl: templatePath('gui-v2', 'auth-v2'),
                controller: 'AuthControllerV2',
                title: title('Login'),
                name: 'auth',
                fullscreen: true,
                resolve: noAuthRequired
            })
            .when('/login', {
                redirectTo: '/auth/login'
            })
            .when('/password-reset', {
                redirectTo: '/auth/password-reset'
            })
            .when('/profile/:entityType/:id*', {
                templateUrl: templatePath('gui-v2', 'profile-v2'),
                controller: 'ProfileControllerV2',
                title: 'Profile',
                name: 'profile',
                resolve: authRequired
            })
            .when('/email-restore/:module/:msg', {
                templateUrl: templatePath('dashboard', 'dashboard'),
                controller: 'EmailRestoreController',
                title: title('Email restore'),
                name: 'email-restore',
                resolve: authRequired
            })
            .when('/policy/edit/:id', {
                templateUrl: templatePath('policy', 'policy'),
                controller: 'PolicyController',
                title: title('Query'),
                name: 'policy-edit',
                resolve: authRequired,
                reloadOnSearch: false,
                hideSaasSwitch: true,
                hiddenOverflow: true,
                noPaddings: true,
                addStandardPage: true
            })
            .otherwise({
                redirectTo: '/'
            });
    }

    $locationProvider.html5Mode(false);
    $locationProvider.hashPrefix('!');

    //initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';

    //TODO should be true on dev and false on production
    $compileProvider.debugInfoEnabled(true);
});

m.run(function ($http, DSCacheFactory, $location, $route, $rootScope, underscoreMixins, docTitleHelper) {
    var original = $location.path;
    $location.path = function (path, reload) {
        if(_(reload).isUndefined()) {
            reload = true;
        }
        if (reload === false) {
            var lastRoute = $route.current;
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                $route.current = lastRoute;
                un();
            });
        }
        return original.apply($location, [path]);
    };

    DSCacheFactory('defaultCache', {
        maxAge: 1000, // Items added to this cache expire after 15 minutes.
        deleteOnExpire: 'aggressive' // Items will be deleted from this cache right when they expire.
    });

    $http.defaults.cache = DSCacheFactory.get('defaultCache');

    underscoreMixins.addMixins();
});
