var m = angular.module('app');

m.directive('errorStatus', function(errorStatusHolder, path, modals, utilityDao, $route) {
    return {
        restrict: 'A',
        templateUrl: path('main').directive('error-status').template(),
        scope: {}, 
        link: function ($scope, element, attrs) {
            $scope.errorStatusHolder = errorStatusHolder;
            
            $scope.contact = function () {
                var data = {
                    title: 'Send a message to support',
        
                    pages: [{
                        lastPage: true,
                        params: [{
                            id: 'subject',
                            type: 'string',
                            data: 'Internal error occurred',
                            label: 'Subject',
                            validation: {
                                required: true,
                                minLength: {
                                    message: 'Please enter subject of your message',
                                    value: 1
                                }
                            }
                        }, {
                            label: 'Message',
                            id: 'body',
                            type: 'text',
                            paramClass: 'text-param-average',
                            validation: {
                                required: {
                                    message: 'Please enter message text',
                                    value: true
                                },
                                minLength: {
                                    message: 'Please enter message text',
                                    value: 1
                                }
                            }
                        }]
                    }],
                    args:  {
                        okButton: 'Send'
                    }
                };
        
                modals.params(data.pages, data.title, data.size, data.args)
                    .then(function(params) {
                        utilityDao.sendSupportMessage(params);
                    });
            };
            
            $scope.reload = function() {
                errorStatusHolder.errorStatus(null);
                $route.reload();
            };
        }
    };
});