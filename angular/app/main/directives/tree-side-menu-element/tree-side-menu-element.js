var m = angular.module('app');

m.directive('treeSideMenuElement', function(path, sideMenuManager, $q) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('main').directive('tree-side-menu-element').template(),
        transclude: {
            'subElement': '?subElement'
        },
        scope: {
            model: '=treeSideMenuElement',
            path: '=',
        },
        link: function($scope, element, attrs) {
            $scope.sideMenuManager = sideMenuManager;
            if(!$scope.model.path) {
                $scope.model.path = [$scope.model.id];
            }
            
            function expand() {
                $scope.model.state = 'loading';
                $q.when($scope.model.getSub()).then(function(sub) {
                    _(sub).each(function(s) {
                        s.path = $scope.model.path.slice();
                        s.path.push(s.id);
                    });
                    $scope.model.sub = sub;
                    $scope.model.state = 'expanded';
                    
                    if($scope.model.active) {
                        var navigationPath = sideMenuManager.treeNavigationPath();
                        var found = _($scope.model.sub).each(function(s) {
                            var diffFound = _(s.path).find(function(id, idx) {
                                return id != navigationPath[idx];
                            });
                            return !diffFound;
                        });
                        if(found) {
                            found.active = true;
                            $scope.model.active = false;
                        }
                    }
                });
            }
            
            function collaps() {
                var activeFound = _($scope.model.sub).find(function(s) {
                    return s.active;
                });
                if(activeFound) {
                    $scope.model.active = true;
                }
                $scope.model.state = 'collapsed';
                $scope.model.sub = null;
            }
            
            var states = {
                'collapsed': {
                    'class': 'fa-chevron-down',
                    onClick: expand
                },
                'expanded': {
                    'class': 'fa-chevron-up',
                    onClick: collaps
                },
                'loading': {
                    'class': 'fa-spinner fa-rotate'
                },
                'none': {
                    'class': 'no-state'
                }
            };
            $scope.$watch('model.state', function() {
                $scope.statusIcon = states[$scope.model.state].class;
            });
            
            $scope.statusClick = function(event) {
                (states[$scope.model.state].onClick || _.noop)();
                event.stopPropagation();
            };
            
            $scope.onClick = function() {
                sideMenuManager.treeNavigationPath($scope.model.path);
                ($scope.model.onClick || _.noop)();
            };
            
            $scope.$watch('sideMenuManager.treeNavigationPath()', function(navigationPath) {
                var diffFound = _($scope.model.path).find(function(id, idx) {
                    return id != navigationPath[idx];
                });
                $scope.model.active = false;
                if(!diffFound) {
                    if(navigationPath.length == $scope.model.path.length) {
                        $scope.model.active = true;
                    } else {
                        if($scope.model.state == 'collapsed') {
                            expand();
                        }
                    }
                }
            });
        }
    };
});