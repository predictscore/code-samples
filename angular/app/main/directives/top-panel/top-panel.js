var m = angular.module('app');

m.directive('topPanel', function(path) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('main').directive('top-panel').template(),
        transclude: {
            right: '?right'
        },
        scope: {
            title: '@topPanel'
        },
        link: function($scope, element, attrs) {

        }
    };
});