var m = angular.module('app');

m.directive('workingIndicator', function(path, workingIndicatorHelper) {
    return {
        restrict: 'A',
        templateUrl: path('main').directive('working-indicator').template(),
        scope: {},
        link: function ($scope, element, attrs) {
            $scope.helper = workingIndicatorHelper;
            
            $scope.unlock = function() {
                $scope.helper.hide();  
            };
        }
    };
});