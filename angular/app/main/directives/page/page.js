var m = angular.module('app');

m.directive('page', function(path, $route) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('main').directive('page').template(),
        transclude: {
            top: '?top',
            content: 'content'
        },
        link: function($scope, element, attrs) {
            $scope.title = $route.current.title;
        }
    };
});