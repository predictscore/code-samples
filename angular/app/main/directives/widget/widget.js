var m = angular.module('app');

m.directive('widget', function(path, $timeout, slowdownHelper) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('main').directive('widget').template(),
        transclude: true,
        scope: {
            reload: '&widget',
            tick: '=?',
            visible: '=?',
            api: '=?'
        },
        link: function($scope, element, attrs) {
            $scope.api = $scope.api || {};
            $scope.api.reload = function() {
                if($scope.reloading) {
                    $scope.reloadAfter = true;
                    return;
                }
                $timeout.cancel($scope.timeoutHandle);
                $scope.reloading = true;
                $scope.reload().then(onLoad, onError);
            };
            $scope.api.loaded = false;

            $scope.visible = _($scope.visible).isUndefined() ? true : $scope.visible;

            $scope.api.reload(true);

            function onLoad() {
                $scope.api.loaded = true;
                $scope.reloading = false;

                if($scope.tick > 0 || $scope.reloadAfter) {
                    var timeout = $scope.reloadAfter ? 0 : slowdownHelper.adjustInterval($scope.tick);
                    $scope.reloadAfter = false;
                    $scope.timeoutHandle = $timeout(function () {
                        $scope.api.reload();
                    }, timeout);
                }
            }

            function onError() {
                $scope.reloading = false;
                $scope.reloadAfter = false;
                $scope.timeoutHandle = $timeout(function () {
                    $scope.api.reload();
                }, 5000);
            }

            $scope.$on('$destroy', function() {
                $timeout.cancel($scope.timeoutHandle);
            });
        }
    };
});