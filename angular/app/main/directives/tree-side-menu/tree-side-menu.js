var m = angular.module('app');

m.directive('treeSideMenu', function(path, sideMenuManager, modulesManager, $q, policyDao, reportDao, routeHelper, policyGroups, feature) {
    var root = path('main').directive('side-menu');
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('main').directive('tree-side-menu').template(),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.sideMenuManager = sideMenuManager;
            $scope.root = root;
            
            function configurationMenus() {
                var result = [];
                
                if(feature.cloudAppsTab) {
                    result.push({
                        title: 'Cloud App Store',
                        icon: 'fa-laptop',
                        name: 'cloud-apps'
                    });
                }
                if(feature.appStoreTab) {
                    result.push({
                        title: 'Security App Store',
                        icon: 'fa-tag',
                        name: 'app-store'
                    });
                }
                if(feature.userManagementTab) {
                    result.push({
                        title: 'User management',
                        icon: 'fa-user',
                        name: 'user-management',
                        admin: true
                    });
                }
                if(feature.dlpConfigurationTab) {
                    result.push({
                        title: 'Smart Search',
                        icon: 'fa-cog',
                        name: 'dlp-configuration'
                    });
                }
                
                var quarantineAppFound = _(modulesManager.cloudApps()).find(function(app) {
                    return _(['google_drive', 'box', 'office365_emails']).contains(app.name);
                });
                if(quarantineAppFound) {
                    result.push({
                        title: 'Quarantine',
                        icon: 'fa-lock',
                        name: 'quarantine'
                    });
                }
                return result;
            }
            
            $scope.menus = {
                dashboard: {
                    id: 'dashboard',
                    icon: 'fa-cloud',
                    label: 'Dashboard',
                    getSub: function() {
                        return _(modulesManager.cloudApps()).map(function(app) {
                            return {
                                id: app.name,
                                image: app.img,
                                label: app.title,
                                state: 'none',
                                onClick: function() {
                                    routeHelper.redirectTo('dashboard', {
                                        module: app.name
                                    });
                                }
                            };
                        });
                    },
                    onClick: function() {
                        sideMenuManager.treeNavigationPath(['dashboard', 'super-dashboard']);
                        routeHelper.redirectTo('dashboard', {
                            module: 'super-dashboard'
                        });
                    }
                },
                analytics: {
                    id: 'analytics',
                    icon: 'fa-shield',
                    label: 'Analytics',
                    getSub: function() {
                        var result = [];
                        var existApps = _(modulesManager.cloudApps()).map(function(app) {
                            return app.name;
                        });
                        if(feature.driveAnaliticsTab && _(existApps).contains('google_drive')) {
                            result.push({
                                id: 'drive-explorer/google_drive',
                                icon: 'fa-eye',
                                label: 'Google Drive Explorer',
                                state: 'none',
                                onClick: function() {
                                    routeHelper.redirectTo('drive-explorer', {
                                        module: 'google_drive'
                                    });
                                }
                            });
                        }
                        if(feature.driveAnaliticsTab && _(existApps).contains('box')) {
                            result.push({
                                id: 'drive-explorer/box',
                                icon: 'fa-eye',
                                label: 'Box Drive Explorer',
                                state: 'none',
                                onClick: function() {
                                    routeHelper.redirectTo('drive-explorer', {
                                        module: 'box'
                                    });
                                }
                            });
                        }
                        if(feature.driveAnaliticsTab && _(existApps).contains('office365_emails')) {
                            result.push({
                                id: 'mail-explorer/office365_emails',
                                icon: 'fa-envelope',
                                label: 'Mail Explorer',
                                state: 'none',
                                onClick: function() {
                                    routeHelper.redirectTo('mail-explorer', {
                                        module: 'office365_emails'
                                    });
                                }
                            });
                        }
                        _(policyGroups).each(function(group) {
                            result.push({
                                id: _(group.tags).first() || 'user',
                                type: 'group',
                                icon: 'fa-tags',
                                label: group.label,
                                state: 'none',
                                onClick: function() {
                                    routeHelper.redirectTo('policy-list', {}, {
                                        qs: {
                                            tags: _(group.tags).first(),
                                            not_tags: group.notTags
                                        },
                                        reloadRoute: true
                                    });
                                }
                            });
                        });
                        return result;
                    },
                    onClick: function() {
                        routeHelper.redirectTo('policy-list');
                    }
                },
                alerts: {
                    id: 'alerts',
                    icon: 'fa-exclamation-triangle',
                    label: 'Alerts',
                    state: 'none',
                    onClick: function() {
                        routeHelper.redirectTo('alerts');
                    }
                },
                reports: {
                    id: 'reports',
                    icon: 'fa-list',
                    label: 'Reports',
                    state: 'none',
                    onClick: function() {
                        routeHelper.redirectTo('report-list');
                    }
                },
                securityStack: {
                    id: 'security-stack',
                    icon: 'fa-table',
                    label: 'Security Stack',
                    state: 'none',
                    onClick: function() {
                        routeHelper.redirectTo('security-stack');
                    }
                },
                configuration: {
                    id: 'configuration',
                    icon: 'fa-cog',
                    label: 'Configuration',
                    getSub: function() {
                        return _(configurationMenus()).map(function(menu) {
                            return {
                                id: menu.name,
                                icon: menu.icon,
                                label: menu.title,
                                state: 'none',
                                onClick: function() {
                                    routeHelper.redirectTo(menu.name);
                                }
                            };
                        });
                    }
                }
            };
            
            _($scope.menus).each(function(menu) {
                menu.state = menu.state || 'collapsed';
            });
        }
    };
});