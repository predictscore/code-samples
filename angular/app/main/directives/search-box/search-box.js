var m = angular.module('app');

m.directive('searchBox', function(path, $timeout, globalSearchDao, ref, modulesManager, $q) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('main').directive('search-box').template(),
        scope: {},
        link: function ($scope, element, attrs) {
            var defaultQueryPriority = 100;
            var saasQueries = {
                "office365_emails": [
                    {"query": "user", "priority": 10},
                    {"query": "policy", "priority": 20},
                    {"query": "file", "priority": 30},
                    {"query": "folder", "priority": 40},
                    {"query": "email", "priority": 200}
                ]
            };
            var columns = {
                "policy": [{
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "saas",
                    "hidden": true
                }, {
                    "id": "saasIcon",
                    "converter": "saasIcon",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "image",
                    "converter": "policyImageConverter",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "name",
                    "href": {
                        "type": "policy-edit",
                        "params": {
                            "id": "entity_id"
                        }
                    }
                }],
                "user": [{
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "saas",
                    "hidden": true
                }, {
                    "id": "saasIcon",
                    "converter": "saasIcon",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "image",
                    "converter": "userImageConverter",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "name",
                    "href": {
                        "type": "user",
                        "params": {
                            "id": "entity_id",
                            "module": "saas"
                        }
                    }
                }],
                "group": [{
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "saas",
                    "hidden": true
                }, {
                    "id": "saasIcon",
                    "converter": "saasIcon",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "image",
                    "converter": "groupImageConverter",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "name",
                    "href": {
                        "type": "group",
                        "params": {
                            "id": "entity_id",
                            "module": "saas"
                        }
                    }
                }],
                "file": [{
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "saas",
                    "hidden": true
                }, {
                    "id": "saasIcon",
                    "converter": "saasIcon",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "mime",
                    "converter": "mimeIconConverter",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "name",
                    "href": {
                        "type": "file",
                        "params": {
                            "id": "entity_id",
                            "module": "saas"
                        }
                    }
                }],
                "folder": [{
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "saas",
                    "hidden": true
                }, {
                    "id": "saasIcon",
                    "converter": "saasIcon",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "mime",
                    "converter": "mimeIconConverter",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "name",
                    "href": {
                        "type": "folder",
                        "params": {
                            "id": "entity_id",
                            "module": "saas"
                        }
                    }
                }],
                "bucket": [{
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "saas",
                    "hidden": true
                }, {
                    "id": "saasIcon",
                    "converter": "saasIcon",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "mime",
                    "converter": "mimeIconConverter",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "name",
                    "href": {
                        "type": "bucket",
                        "params": {
                            "id": "entity_id",
                            "module": "saas"
                        }
                    }
                }],
                "site": [{
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "saas",
                    "hidden": true
                }, {
                    "id": "saasIcon",
                    "converter": "saasIcon",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "mime",
                    "converter": "mimeIconConverter",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "name",
                    "href": {
                        "type": "site",
                        "params": {
                            "id": "entity_id",
                            "module": "saas"
                        }
                    }
                }],
                "app": [{
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "saas",
                    "hidden": true
                }, {
                    "id": "saasIcon",
                    "converter": "saasIcon",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "image",
                    "converter": "applicationImageConverter",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "name",
                    "href": {
                        "type": "application",
                        "params": {
                            "id": "entity_id",
                            "module": "saas"
                        }
                    }
                }],
                "email": [{
                    "id": "entity_id",
                    "hidden": true
                }, {
                    "id": "saas",
                    "hidden": true
                }, {
                    "id": "saasIcon",
                    "converter": "saasIcon",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "mime",
                    "converter": "mimeIconConverter",
                    "dataStyle": {
                        "width": "30px"
                    }
                }, {
                    "id": "name",
                    "href": {
                        "type": "email",
                        "params": {
                            "id": "entity_id",
                            "module": "saas"
                        }
                    }
                }]
            };

            $scope.showResults = ref(false);

            $scope.tableOptions = {};
            $scope.tableModels = {};

            $scope.haveResults = function() {
                return !_($scope.tableModels).isEqual({});
            };

            var searchTimeout = null;
            var requestTimeout = $q.defer();
            function search(tableModels) {
                searchTimeout = null;
                requestTimeout.resolve();
                requestTimeout = $q.defer();
                var activeSaass = _(modulesManager.activeSaasApps()).pluck('name');
                var categoryLimit = (activeSaass.length > 1) ? 8 : 10;
                var saasData = {};
                var queriesList = [];

                _(activeSaass).each(function (saas) {
                    saasData[saas] = {};
                    if (saasQueries[saas]) {
                        _(saasQueries[saas]).each(function (queryInfo) {
                            queriesList.push({
                                priority: queryInfo.priority || defaultQueryPriority,
                                query: queryInfo.query,
                                saas: saas
                            });
                        });
                    } else {
                        queriesList.push({
                            priority: defaultQueryPriority,
                            saas: saas
                        });
                    }
                });
                $scope.searchQueries = queriesList;

                _(queriesList).chain().sortBy('priority').each(function (queryInfo, queryIdx) {
                    queryInfo.searching = true;
                    var saas = queryInfo.saas;
                    globalSearchDao.search(columns, $scope.keyword, categoryLimit, saas, queryInfo.query)
                        .timeout(requestTimeout.promise)
                        .then(function (response) {
                            queryInfo.searching = false;
                            if ($scope.searchQueries === queriesList) {
                                $scope.searching = !_(queriesList).chain().find(function (item) {
                                    return item.searching;
                                }).isUndefined().value();
                            }
                            _(response).each(function(promise, key) {
                                promise.then(function(tableModel) {
                                    saasData[saas][key] = tableModel.data;
                                    tableModels[key] = {
                                        data: _(activeSaass).reduce(function (memo, saas) {
                                            if (saasData[saas][key]) {
                                                return memo.concat(saasData[saas][key].data);
                                            }
                                            return memo;
                                        }, []),
                                        columns: columns[key],
                                        options: {}
                                    };
                                });
                            });
                        }, function (error) {
                            if ($scope.searchQueries === queriesList) {
                                queryInfo.searching = false;
                                $scope.searching = !_(queriesList).chain().find(function (item) {
                                    return item.searching;
                                }).isUndefined().value();
                            }
                        });
                });
            }

            $scope.$watch('keyword', function(keyword) {
                if(!_(searchTimeout).isNull()) {
                    $timeout.cancel(searchTimeout);
                    searchTimeout = null;
                }
                if(_(keyword).isString() && keyword.length) {
                    $scope.searching = true;
                    $scope.searchQueries = false;
                    searchTimeout = $timeout(function() {
                        $scope.tableModels = {};
                        search($scope.tableModels);
                    }, 300);
                } else {
                    $scope.tableModels = {};
                }
            });
        }
    };
});