var m = angular.module('app');

m.directive('topNavigation', function(path, popupManager, sideMenuManager, modulesDao, modulesManager, $routeParams, $route, routeHelper, modals, systemDao, $timeout, notificationCenter, feature) {
    return {
        restrict: 'A',
        replace: false,
        templateUrl: path('main').directive('top-navigation').template(),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.route = $route;
            $scope.sideMenuManager = sideMenuManager;
            $scope.modulesManager = modulesManager;
            $scope.feature = feature;
            
            $scope.showLogoutConfirm = function(event) {
                event.preventDefault();
                popupManager.current('logoutConfirm');
            };

            $scope.switchCloudApp = function(event, app) {
                if($route.current.internalModuleChange) {
                    event.preventDefault();
                    routeHelper.redirectTo($route.current.name, $.extend({}, $route.current.params, {
                        module: app.name
                    }), {
                        saveQueryString: true,
                        reload: false
                    });
                }
                modulesManager.currentModule(app.name);
            };
            
            $scope.switchCloudLink = function(app) {
                if($route.current.internalModuleChange) {
                    return routeHelper.getPath($route.current.name, $.extend({}, $route.current.params, {
                        module: app.name
                    }));
                } else {
                    return app.link;
                }
            };

            $scope.isAppActive = function(app) {
                return app.name == modulesManager.currentModule() || _($routeParams.module).isUndefined();
            };

            $scope.showSettings = function(event) {
                event.preventDefault();
                routeHelper.redirectTo('settings');
            };

            $scope.showPaloAlto = function () {
                return modulesManager.isActive('pan_shadow_it');
            };
            
            $scope.showVersion = function(event) {
                event.preventDefault();
                systemDao.version().then(function(response) {
                    var messages = [
                        'Version: ' + response.data.version_info.version, 
                        'Build: ' + response.data.version_info.build
                    ];
                    var options = {
                        title: 'Version'
                    };
                    modals.alert(messages, options);
                });
            };


            function calculateTopNav() {
                $timeout(function () {
                    var leftPart = element.find('.top-nav-cloud-apps');
                    var rightPart = element.find('.nav-notification');
                    var pageTitle = $('#top-nav-title');
                    var container = $('#top-nav');
                    if (pageTitle.width() && leftPart.width() && rightPart.width()) {
                        container.css('min-width', (leftPart.width() + rightPart.width() + 10) + 'px');
                        var availableWidth = container.width() -  leftPart.width() - rightPart.width();
                        if (availableWidth < pageTitle.width()) {
                            $('#wrapper').addClass('two-line-top');
                        } else {
                            $('#wrapper').removeClass('two-line-top');
                        }
                    }
                });

            }

            $scope.$watch('sideMenuManager.hidden()', function () {
                $timeout(calculateTopNav, 500);
            });
            $(window).on('resize.topNav', calculateTopNav);

            $scope.$watchCollection('route.current', calculateTopNav);

            $scope.$on('$destroy', function () {
                $(window).off('.topNav');
            });
            
            $scope.notification = {
                getClass: function() {
                    if(notificationCenter.getNotificationsCount()) {
                        return 'red-text';
                    }
                    return 'gray-text';
                },
                getTooltip: function() {
                    var count = notificationCenter.getNotificationsCount();
                    var tooltip = 'You don\'t have new alerts';
                    if(count) {
                        tooltip =  'You have ' + count + ' new alerts';
                    }
                    return {
                        content: tooltip,
                        position: {
                            my: 'top right',
                            at: 'bottom center'
                        }
                    };
                },
                onClick: function() {
                    notificationCenter.clearNotifications();
                    routeHelper.redirectTo('dashboard', {
                        module: 'super-dashboard'
                    });
                }
            };
        }
    };
});