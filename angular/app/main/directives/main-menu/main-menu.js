var m = angular.module('app');

m.directive('mainMenu', function(path, sideMenuManager, routeHelper, feature, modulesManager, $route, troubleshooting) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('main').directive('main-menu').template(),
        scope: {},
        link: function ($scope, element, attrs) {
            $scope.sideMenuManager = sideMenuManager;
            $scope.modulesManager = modulesManager;
            $scope.routeHelper = routeHelper;

            $scope.menuItemClicked = function (e) {
                element.find('.openable').not(e.currentTarget).removeClass('open');
                var menuItem = $(e.currentTarget);
                menuItem.toggleClass('open');
                if (menuItem.hasClass('open')) {
                    var submenu = menuItem.find('.submenu');
                    if (submenu.length > 0) {
                        submenu[0].scrollIntoView();
                    }
                }
            };

            function closeMenus() {
                element.find('.openable').removeClass('open');
                element.find('.submenu').removeAttr('style');
            }
            $scope.$watch('sideMenuManager.hidden()', closeMenus);
            $scope.$watch('sideMenuManager.mini()', closeMenus);
            $scope.$watch('sideMenuManager.display()', closeMenus);
            $scope.$watch(function () {
                return $route.current;
            }, closeMenus);

            function filterMenuItems (menus, activeModule, activeModules, activeSecApps) {
                return _(menus).chain().filter(function(menu) {
                    return _(menu.feature).isUndefined() || feature[menu.feature];
                }).filter(function(menu) {
                    return _(menu.saas).isUndefined() || _(menu.saas).contains(activeModule) || _(menu.saas).intersection(activeModules).length > 0;
                }).filter(function(menu) {
                    return !menu.admin || sideMenuManager.currentUser().admin;
                }).filter(function (menu) {
                    return !menu.activeSecApps || !!_(menu.activeSecApps).find(function (app) {
                            return _(activeSecApps).find(function (secApp) {
                                return secApp.name === app && (!secApp.gui_hidden || troubleshooting.entityDebugEnabled());
                            });
                        });
                }).value();
            }
            function menuPathFunction (menu, activeModule, activeModules) {
                return function() {
                    if (menu.nolink || !menu.name) {
                        return;
                    }

                    if (menu.preferredSaas && _(activeModules).contains(menu.preferredSaas)) {
                        return '/#!' + routeHelper.getPath(menu.name, $.extend({
                                module: menu.preferredSaas
                            }, menu.params));
                    }

                    if (_(menu.saas).isUndefined() || _(menu.saas).contains(activeModule)) {
                        return '/#!' + routeHelper.getPath(menu.name, menu.params);
                    } else {
                        return '/#!' + routeHelper.getPath(menu.name, $.extend({
                                module: _(menu.saas).intersection(activeModules)[0]
                            }, menu.params));
                    }
                };
            }

            function setMenus(menus) {
                var activeModule = modulesManager.currentModule();
                var activeModules = _(modulesManager.cloudApps()).pluck('name');
                var activeSecApps = modulesManager.activeSecAppsFull();
                $scope.menus = filterMenuItems(menus, activeModule, activeModules, activeSecApps);
                _($scope.menus).each(function(menu) {
                    menu.isActive = function() {
                        return routeHelper.isActive(menu.name);
                    };
                    menu.path = menuPathFunction(menu, activeModule, activeModules);
                    if (menu.submenu) {
                        menu.submenus = filterMenuItems(menu.submenu, activeModule, activeModules, activeSecApps);
                        _(menu.submenus).each(function (menu) {
                            menu.path = menuPathFunction(menu, activeModule, activeModules);
                        });
                    }
                });
            }

            feature.init().then(function() {
                $scope.$watch('sideMenuManager.getMenus()', setMenus);
                $scope.$watch('modulesManager.currentModule()', function () {
                    setMenus(sideMenuManager.getMenus());
                });
                $scope.$watch('modulesManager.allCloudApps()', function () {
                    setMenus(sideMenuManager.getMenus());
                });
            });
        }
    };
});