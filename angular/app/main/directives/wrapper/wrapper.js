var m = angular.module('app');

m.directive('wrapper', function(path, sideMenuManager, errorStatusHolder, feature, $route) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('main').directive('wrapper').template(),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.guiVersion = window.guiVersion;
            $scope.sideMenuManager = sideMenuManager;
            $scope.errorStatusHolder = errorStatusHolder;
            $scope.$route = $route;
            $scope.feature = feature;
            
            $scope.getContainerClass = function() {
                var result = {
                    'fullscreen': sideMenuManager.fullscreen(), 
                    'hidden-overflow': sideMenuManager.hiddenOverflow(),
                    'no-paddings': sideMenuManager.noPaddings()
                };
                result[sideMenuManager.routeClass()] = true;
                return result;
            };

            $scope.$watch('sideMenuManager.hidden()', function(state) {
                if(state) {
                    element.addClass('sidebar-hide');
                } else {
                    element.removeClass('sidebar-hide');
                }
            });
            $scope.$watch('sideMenuManager.mini()', function(state) {
                element.off("resize");
                if(state) {
                    element.addClass('sidebar-mini');
                } else {
                    element.removeClass('sidebar-mini');
                }
            });
            $scope.$watch('sideMenuManager.display()', function(state) {
                if(state) {
                    element.addClass('sidebar-display');
                } else {
                    element.removeClass('sidebar-display');
                }
            });

            if(!element.hasClass('sidebar-mini')) {
                if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 868px)')) {
                    $('#wrapper').addClass('sidebar-mini');
                }
            }
        }
    };
});