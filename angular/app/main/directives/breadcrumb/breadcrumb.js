var m = angular.module('app');

m.directive('breadcrumb', function(path, sideMenuManager) {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: path('main').directive('breadcrumb').template(),
        scope: {},
        link: function($scope, element, attrs) {
            $scope.sideMenuManager = sideMenuManager;
            $scope.home = '#{"text":"Home", "type":"dashboard"}';
        }
    };
});