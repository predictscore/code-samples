var m = angular.module('app');

m.controller('MainController', function($scope, widgetPolling, modulesManager, routeHelper, $route, sideMenuManager, $timeout, avananUserDao, modals, wizardHelper, analytics, linkedText, errorStatusHolder, policiesRefreshHelper, configDao, trialExpiredModal) {
    function htmlToPlaintext(text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    }
    $scope.$on('$routeChangeSuccess', function(event, current) {
        var url = $route.current.originalPath;
        _($route.current.params).each(function(val, key) {
            if(key != 'id') {
                url = url.split(':' + key).join(val);
            }
        });
        analytics.pageView(url, htmlToPlaintext(linkedText.toText($route.current.title)));
        sideMenuManager.fullscreen($route.current.fullscreen || false);
        sideMenuManager.routeClass('route-' + $route.current.name);
        policiesRefreshHelper.clearPolicies();
        errorStatusHolder.errorStatus(null);
        
        var body = $('body');
        if($route.current.hideMessenger) {
            body.addClass('hide-messenger');
        } else {
            body.removeClass('hide-messenger');
        }
        if (window.guiVersion === 2) {
            body.addClass('guiV2');
            var hasScroll = $route.current.scrollOnPage || false;
            if (hasScroll) {
                body.addClass('guiV2-scroll');
            } else {
                body.removeClass('guiV2-scroll');
            }
        }

        $('html, body').animate({ scrollTop: 0 }, 'fast');
        widgetPolling.stopUpdating();
        if(!_(current.$$route).isUndefined() &&
            current.$$route.name != 'auth' &&
            current.$$route.name != 'authorize-error' &&
            current.$$route.name != 'password-change') {
            $('body').css('background', 'none');
            if (_(current.pathParams.module).isString()) {
                modulesManager.currentModule(current.pathParams.module);
            }
            modulesManager.reloadModules();
        } else {
            if (window.guiVersion === 2) {
                $('body').css('background', "url('/assets/gui-v2/controllers/auth-v2/images/background.png')");
            } else {
                $('body').css('background', "url('/assets/auth/controllers/auth/images/background.png')");
            }
        }
        $timeout(function() {
            if ($route.current.name === 'wizard') {
                $('#ng-view').addClass('in-wizard');
            } else {
                $('#ng-view').removeClass('in-wizard');
            }
            // combined 'background' property doesn't work in Firefox for unknown reason
            var srcEl = $('#ng-view > div');
            var mainEl = $('#main-container');
            _(['image', 'position', 'size', 'repeat', 'origin', 'clip', 'attachment', 'color']).each(function (prop) {
                mainEl.css('background-' + prop, srcEl.css('background-' + prop));
            });
        });
    });

    $scope.$on('$routeChangeError', function(event, current, previous) {
        if(current.$$route.name == 'auth') {
            routeHelper.redirectTo('dashboard');
        } else {
            routeHelper.saveRoute();
            routeHelper.redirectTo('auth', {
                page: 'login'
            });
        }
    });

    var maxErrorCount = 10;
    var errorCount = maxErrorCount;
    window.successLogin = function() {
        function checkCurrent() {
            avananUserDao.current().then(function (response) {
                if (response.data.hidden) {
                    return routeHelper.restoreRoute();
                }
                return configDao.getTrialExpirationDays().then(function (trialExpirationDays) {
                    if (trialExpirationDays < 0) {
                        avananUserDao.logout();
                        return trialExpiredModal.show().then(function () {
                            routeHelper.redirectTo('auth', {
                                page: 'login'
                            });
                        });
                    }
                    return routeHelper.restoreRoute();
                });
            }, function () {
                if(errorCount < maxErrorCount) {
                    errorCount++;
                    $timeout(checkCurrent, 500);
                }
            });
        }
        if(errorCount < maxErrorCount) {
            errorCount = 0;
        } else {
            checkCurrent();
        }
    };

    window.failedLogin = function(queryString) {
        var qs = {};
        if (queryString) {
            qs = _(queryString.slice(1).split('&')).chain().map(function (element) {
                var param = element.split('=');
                return [param[0], decodeURIComponent(param[1] || '')];
            }).object().value();
        }
        var msg = qs.email ? ('The user ' + qs.email + ' is not authorized to use this service') : 'You are not authorized to access this service';
        modals.alert(msg, {
            title: 'Login error'
        });
    };

    $(window).resize(function() {
        if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 868px)')) {
            $('#wrapper').addClass('sidebar-mini').addClass('window-resize');
            $('.main-menu').find('.openable').removeClass('open');
            $('.main-menu').find('.submenu').removeAttr('style');
        }
        else if (Modernizr.mq('(min-width: 869px)')) {
            if($('#wrapper').hasClass('window-resize'))	{
                $('#wrapper').removeClass('sidebar-mini window-resize');
                $('.main-menu').find('.openable').removeClass('open');
                $('.main-menu').find('.submenu').removeAttr('style');
            }
        }
        else {
            $('#wrapper').removeClass('sidebar-mini window-resize');
            $('.main-menu').find('.openable').removeClass('open');
            $('.main-menu').find('.submenu').removeAttr('style');
        }
    });
});