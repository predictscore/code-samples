var m = angular.module('app');

m.service('modulesManager', function($timeout, $cookies, $injector, $q, avananModules, appIconHelper) {
    var currentModule = $cookies.get('currentModule') || 'super-dashboard';
    $cookies.put('currentModule', currentModule);

    var cloudApps = [];
    var apps = {};
    var modules = {};
    var labelsToIcons = {};

    function reloadModules() {
        var modulesDao = $injector.get('modulesDao');
        return modulesDao.cloudAppsDashboard().then(function (response) {
            if(!_(angular.copy(cloudApps)).isEqual(response.data.saas)) {
                cloudApps = response.data.saas;
            }
            apps = response.data.apps;
            modules = response.data.modules;

            labelsToIcons = _(apps).chain().map(function (app) {
                return [app.label, appIconHelper.appIconByApp(app, true)];
            }).object().value();
            _(avananModules).each(function (module) {
                labelsToIcons[module.backendLabel || module.label] =  appIconHelper.appIconByApp(module, true);
            });

            if (!modulesManager.activeSaasApps().length) {
                modulesManager.currentModule('please-enable-saas');
            } else {
                var app = modulesManager.cloudApp(modulesManager.currentModule());
                if(_(app).isUndefined() || app.state != 'active') {
                    var first = _(cloudApps).find(function(app) {
                        return app.state == 'active';
                    });
                    if(_(first).isObject()) {
                        modulesManager.currentModule(first.name);
                    } else {
                        modulesManager.currentModule('please-enable-saas');
                    }
                }
            }
            return $q.when({});
        });
    }

    var initialized = false;

    var modulesManager = {
        init: function() {
            if(initialized) {
                return $q.when();
            }
            initialized = true;
            return reloadModules();
        },
        currentModule: function(newModule) {
            if(_(newModule).isString()) {
                $cookies.put('currentModule', newModule);
                currentModule = newModule;
            }
            return currentModule;
        },
        currentModuleCatalog: function() {
            var app = modulesManager.cloudApp(currentModule);
            if (!app.super) {
                return currentModule;
            } else {
                return _(cloudApps).chain().filter(function(app) {
                    return app.state == 'active';
                }).pluck('name').value().join(',');
            }
        },
        reloadModules: reloadModules,
        cloudApps: function() {
            return _(cloudApps).filter(function(app) {
                return app.state == 'active';
            });
        },
        activeSaasApps: function() {
            return _(cloudApps).filter(function(app) {
                return app.state == 'active' && app.isSaas;
            });
        },
        cloudApp: function(name) {
            return _(cloudApps).find(function(app) {
                return app.name == name;
            });
        },
        isActive: function(name) {
            return apps[name] && apps[name].state == 'active';
        },
        activeSecApps: function(onlyWithSecEntity) {
            return _(modules).chain().map(function(apps) {
                return apps;
            }).flatten().filter(function(appName) {
                return !apps[appName].is_saas && modulesManager.isActive(appName) &&
                    (!onlyWithSecEntity || apps[appName].sec_entity);
            }).value();
        },
        isModuleActive: function(name) {
            return _(modules[name]).some(function(appName) {
                return modulesManager.isActive(appName);
            });
        },
        app: function (name) {
            return apps[name];
        },
        allCloudApps: function () {
            return cloudApps;
        },
        labelToIcon: function (label) {
            return labelsToIcons[label];
        },
        activeSecAppsFull: function (onlyWithSecEntity) {
            return _(apps).filter(function (app) {
                return !app.is_saas && app.state === 'active' && (!onlyWithSecEntity || app.sec_entity);
            });
        }
    };
    return modulesManager;
});