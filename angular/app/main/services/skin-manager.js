var m = angular.module('app');

m.service('skinManager', function($cookies) {
    return {
        current: function(newSkin) {
            if(_(newSkin).isString()) {
                $cookies.put('skin_color', newSkin);
            }
            return 'skin-light';
        }
    };
});