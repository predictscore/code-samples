var m = angular.module('app');

m.service('sideMenuManager', function($q, $timeout, path, avananUserDao, $route, feature, idleManager, configDao, trialExpiredModal) {
    var data = {
        menus: [],
        updateTick: 1000,
        updateUrl: 'side-menu'
    };
    var dataUpdateHandle = null;
    function updateMenus() {
        $q.all([
            path('main').directive('main-menu').json('main-menu').retrieve(),
            configDao.getTrialExpirationDays().run().then(function (response) {
                return response;
            }, function () {
                return $q.when(null);
            })
        ]).then(function(responses) {
            data = $.extend(data, responses[0].data);
            var trialExpirationDays = responses[1];
            if (!_(trialExpirationDays).isNull()) {
                if (trialExpirationDays < 0) {
                    if (currentUser && !currentUser.hidden) {
                        avananUserDao.logout();
                        trialExpiredModal.show();
                    }
                    trialMessage = 'Your evaluation license has expired';
                } else {
                    trialMessage = 'Trial License - Expires in ' + trialExpirationDays + ' day' + (trialExpirationDays !== 1 ? 's': '');
                }
            } else {
                trialMessage = '';
            }
        }, function() {
            dataUpdateHandle = $timeout(updateMenus, 1000);
        });
    }
    dataUpdateHandle = $timeout(updateMenus);

    var versionCheck = {
        timer: null,
        updateTick: 15000,
        cancelPromise: null,
        inProgress: false,
        requestStart: null,
        requestTimeout: 60000
    };
    function versionRecheck() {
        if (inAuthedArea) {
            if (!versionCheck.inProgress || (((new Date()) - versionCheck.requestStart) > versionCheck.requestTimeout)) {
                if(versionCheck.cancelPromise) {
                    versionCheck.cancelPromise.resolve();
                }
                versionCheck.requestStart = new Date();
                versionCheck.inProgress = true;
                versionCheck.cancelPromise = $q.defer();
                avananUserDao.current().timeout(versionCheck.cancelPromise).then(function () {
                    versionCheck.cancelPromise = null;
                    versionCheck.inProgress = false;
                }, function () {
                    versionCheck.cancelPromise = null;
                    versionCheck.inProgress = false;
                });
            }
        }
        versionCheck.timer = $timeout(versionRecheck, versionCheck.updateTick);
    }
    versionRecheck();

    function inAuthedArea() {
        return $route.current && $route.current.name != 'auth' && $route.current.name != 'authorize-error';
    }

    var lastActivityTime = new Date();
    $(window.document).on('mousemove', function () {
        lastActivityTime = new Date();
    });

    function inactivityCheck() {
        var logoutAfterMins = Math.max(idleManager.timeout(), 1);
        if (logoutAfterMins && ((new Date()) - lastActivityTime > logoutAfterMins * 60 * 1000) && inAuthedArea()) {
            if (feature.disableAutoLogout) {
                lastActivityTime = new Date();
            } else {
                console.log('User is inactive for ' + logoutAfterMins + ' mins');
                avananUserDao.logout().then(function () {
                    lastActivityTime = new Date();
                });
            }
        }
        $timeout(inactivityCheck, 30000);
    }
    inactivityCheck();

    var hidden = false;
    var mini = false;
    var display = false;
    var fixed = true;
    var fullscreen = true;
    var currentUser = {};
    var customIconRandom;
    var logoIsCustom = true;
    var navigationPath = [];
    var trialMessage = '';
    var routeClass = '';

    return {
        toggleHide: function() {
            hidden = !hidden;
        },
        hidden: function(newState) {
            if(_(newState).isBoolean()) {
                hidden = newState;
            }
            return hidden;
        },
        hiddenOverflow: function() {
            return !!($route.current && $route.current.hiddenOverflow);
        },
        noPaddings: function() {
            return !!($route.current && $route.current.noPaddings);
        },
        toggleMini: function() {
            mini = !mini;
        },
        mini: function(newState) {
            if(_(newState).isBoolean()) {
                mini = newState;
            }
            return mini;
        },
        toggleDisplay: function() {
            display = !display;
        },
        display: function(newState) {
            if(_(newState).isBoolean()) {
                display = newState;
            }
            return display;
        },
        toggleFixed: function() {
            fixed = !fixed;
        },
        fixed: function(newState) {
            if(_(newState).isBoolean()) {
                fixed = newState;
            }
            return fixed;
        },
        getMenus: function() {
            return data.menus;
        },
        updateMenus: function() {
            data.updateTick = 1000;
            $timeout.cancel(dataUpdateHandle);
            dataUpdateHandle = $timeout(updateMenus);
        },
        fullscreen: function(newState) {
            if(_(newState).isBoolean()) {
                fullscreen = newState;
            }
            return fullscreen;
        },
        routeClass: function(newClass) {
            if(_(newClass).isString()) {
                routeClass = newClass;
            }
            return routeClass;
        },
        currentUser: function(newUser) {
            if(_(newUser).isObject()) {
                if (!currentUser || currentUser.email !== newUser.email) {
                    updateMenus();
                }
                currentUser = newUser;
            }
            return currentUser;
        },
        customIcon: function (refresh) {
            if (refresh || !customIconRandom) {
                logoIsCustom = true;
                customIconRandom = '' + Math.random();
            }
            return '/api/v1/custom_icon#'+customIconRandom;
        },
        logoIsCustom: function (newValue) {
            if (!_(newValue).isUndefined()) {
                logoIsCustom = newValue;
            }
            return logoIsCustom;
        },
        treeNavigationPath: function(newPath) {
            if(!_(newPath).isUndefined()) {
                navigationPath = newPath;
            }
            return navigationPath;
        },
        trialMessage: function () {
            return trialMessage;
        },
        sideMenuHeight: function () {
            var baseTopSize = 198;
            var trialSize = 50;
            return 'calc(100% - ' + (baseTopSize + (trialMessage ? trialSize : 0)) + 'px)';
        }
    };
});