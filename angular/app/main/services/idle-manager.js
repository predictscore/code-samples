var m = angular.module('app');

m.factory('idleManager', function(configDao, avananUserDao) {
    var paramName = 'property_idle_timeout';
    var idleTimeout = null;
    function init() {
        
        return avananUserDao.currentProperties().then(function(result) {
            idleTimeout = result.data[paramName];
        });
    }
    return {
        timeout: function(newValue) {
            if(newValue) {
                idleTimeout = newValue;
                var params = {};
                params[paramName] = newValue;
                return avananUserDao.updateCurrentProperties(params);
            }
            return idleTimeout;
        },
        init: init
    };
});