var m = angular.module('app');

m.service('workingIndicatorHelper', function($timeout) {
    var timeoutHandle = null;
    function cancelTimeout() {
        if(timeoutHandle) {
            $timeout.cancel(timeoutHandle);
        }
        timeoutHandle = null;
    }
    var helper = {
        init: function ($scope) {
            helper.show(true);
            $scope.$on('$destroy', function () {
                helper.hide();
            });
        },
        show: function (disableScreen) {
            helper.disableScreen = disableScreen;
            helper.isShown = true;
            cancelTimeout();
            helper.showUnlockButton = false;
            timeoutHandle = $timeout(function() {
                helper.showUnlockButton = true;
            }, 15000);
        },
        hide: function () {
            helper.isShown = false;
            cancelTimeout();
        },
        hideIfDisabled: function () {
            if (helper.disableScreen) {
                helper.isShown = false;
            }
        },
        isShown : false,
        showUnlockButton: false,
        class: ''
    };
    return helper;
});