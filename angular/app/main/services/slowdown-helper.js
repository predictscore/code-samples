var m = angular.module('app');

m.service('slowdownHelper', function($q, $timeout, $window, $injector, $route, routeHelper) {

    var updateInterval = 15000;
    var updateTimer = null;
    var currentVersion;
    var slowdown = 1;
    var defaultShutdownMessage = 'A new version of this user interface is available. Please reload the page to continue.';
    var shutdownMessage = null;

    var avananUserDao;

    function scheduleCurrentUserRefresh() {
        if (!avananUserDao) {
            avananUserDao = $injector.get('avananUserDao');
        }
        updateTimer = $timeout(function () {
            avananUserDao.current().run();
        }, updateInterval);
    }

    function reset() {
        currentVersion = undefined;
        slowdown = 1;
        shutdownMessage = null;
    }

    function checkVersionChange(newVersion) {
        if (_(newVersion).isUndefined()) {
            return false;
        }
        if (newVersion === currentVersion) {
            return false;
        }
        if (_(currentVersion).isUndefined()) {
            currentVersion = newVersion;
            return false;
        }
        return true;
    }

    return {
        slowdownConverter: function (input) {
            if (updateTimer) {
                $timeout.cancel(updateTimer);
                updateTimer = null;
            }

            slowdown = input.data.slowdown;

            var disableMsg;
            if (checkVersionChange(input.data.current_version)) {
                disableMsg = 'A newer version of this UI is available. Please click Reload to refresh your browser and obtain the latest version.';
                slowdown = 0;
            } else if (_(slowdown).isFinite() && slowdown <= 0) {
                disableMsg = input.data.shutdown_message || defaultShutdownMessage;
            }
            if (disableMsg) {
                $injector.get('modals').alert(disableMsg, {
                    title: 'Alert',
                    closeText: 'Reload',
                    disableCloseX: true,
                    backdrop: 'static',
                    keyboard: false
                }).then(function () {
                    $window.location.reload(true);
                });
            }
            if (input.data.password_expired && $route.current.name !== 'password-change') {
                routeHelper.redirectTo('password-change');
            }
            return $q.when(input);
        },
        adjustInterval: function (interval) {
            if (!_(slowdown).isFinite()) {
                return interval;
            }
            if (!_(interval).isFinite()) {
                return interval;
            }
            if (slowdown <= 0) {
                return;
            }
            return Math.round(interval / slowdown);
        },
        disableRequests: function () {
            return _(slowdown).isFinite() && slowdown <= 0;
        }

    };

});
