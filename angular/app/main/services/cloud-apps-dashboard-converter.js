var m = angular.module('app');

m.factory('cloudAppsDashboardConverter', function ($q, path, routeHelper, $parse, feature) {
    return function (input, args) {
        var deferred = $q.defer();

        var appStore = input.data;
        var saasGroup = _(_(appStore.groups).filter(function (group) {
            return group.family == 'saas';
        })).first();

        var data = {
            saas: _(saasGroup.modules).map(function (module) {
                var iconNameV2 = module.stack_icon_round ? ('saas-icons-round/' + module.stack_icon_round) : ('saas-icons/' + module.stack_icon);
                return {
                    state: module.state,
                    img: window.guiVersion === 2 ? path('gui-v2').img(iconNameV2)
                        : path('security-stack').img('applications/' + module.stack_icon),
                    title: module.label,
                    link: routeHelper.getPath('dashboard', {
                        module: module.name
                    }),
                    name: module.name,
                    isSaas: module.is_saas
                };
            }),
            apps: _(appStore.groups).chain().map(function(group) {
                return _(group.modules).map(function(module) {
                    return [module.name, module];
                });
            }).flatten(true).object().value(),
            modules: _(appStore.groups).chain().map(function(group) {
                return _(group.modules).map(function(module) {
                    return [module.module_name, module.name];
                });
            }).flatten(true).reduce(function(memo, data) {
                var moduleName = _(data).first();
                memo[moduleName] = memo[moduleName] || [];
                memo[moduleName].push(_(data).last());
                return memo;
            }, {}).value(),
            shadowItApps: _(appStore.groups).chain().filter(function (group) {
                return group.family === 'security' && group.type === 'shadow_it';
            }).map(function (group) {
                return _(group.modules).map(function(module) {
                    return [module.name, module];
                });
            }).flatten(true).object().value()
        };

        if(_(data.shadowItApps).find(function (app) {
                return app.state === 'active';
            })) {
            data.saas.push({
                state: 'active',
                img: path('security-stack').img('applications/shadow-it.png'),
                title: 'Shadow IT',
                link: routeHelper.getPath('dashboard', {
                    module: 'shadow-it'
                }),
                name: 'shadow-it'
            });
        }
        if (feature.superDashboard) {

            if (_(data.saas).filter(function (module) {
                    return module.state === 'active';
                }).length > 0) {
                data.saas.unshift({
                    state: 'active',
                    img: path('security-stack').img('applications/super-dashboard.png'),
                    title: 'Super Dashboard',
                    dashboardTitle: 'Main Dashboard',
                    link: routeHelper.getPath('dashboard', {
                        module: 'super-dashboard'
                    }),
                    name: 'super-dashboard',
                    super: true
                });
                data.saas.push({
                    state: 'active',
                    img: path('security-stack').img('applications/summary-report.svg'),
                    title: 'Summary Report',
                    dashboardTitle: 'Summary Report',
                    link: routeHelper.getPath('dashboard', {
                        module: 'summary-report'
                    }),
                    name: 'summary-report',
                    super: true
                });
            }
        }

        deferred.resolve($.extend({}, input, {
            data: data
        }));
        return deferred.promise;
    };
});