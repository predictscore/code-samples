'use strict';

const restify         = require('restify'),
      restifyErrors   = require('restify-errors'),
      config          = require('config'),
      fs              = require('fs');

const routes          = require('./routes'),
      common          = require('fm-common'),
      logger          = common.logger,
      UserAdapter     = common.user.UserAdapter,
      mw              = require('./auth'),
      packageJson     = require('./package.json');

// read out all of the secrets required
const serverCertificate   = fs.readFileSync('./secrets/server.cer'),
      serverKey           = fs.readFileSync('./secrets/server.key');

// create the api server
let server = restify.createServer({

  name:         packageJson.name,
  version:      packageJson.version,
  log:          logger,

  certificate:  serverCertificate,
  key:          serverKey

});

server.use(restify.acceptParser(server.acceptable));
server.use(restify.authorizationParser());

server.use(restify.CORS({
  origins: ['*'],
  credentials: true,
  headers: ['accept', 'accept-version', 'content-type', 'api-version', 'origin', 'x-requested-with', 'authorization']
}));

server.use(restify.dateParser());
server.use(restify.queryParser());
server.use(restify.jsonp());
server.use(restify.gzipResponse());
server.use(restify.bodyParser({
	mapFiles: true
}));

server.use(restify.requestExpiry({
  header: 'x-request-expiry-time'
}));

server.use(restify.throttle({
  burst:    0,
  rate:     0,
  ip:       true,

  overrides: {
    '172.17.0.1': {
      rate:       0,
      burst:      0
    }
  }
}));

server.use(restify.conditionalRequest());

server.on('MethodNotAllowed', (req, res) => {

  if (req.method.toLowerCase() === 'options') {
    let allowHeaders = ['Accept', 'Accept-Version', 'Content-Type', 'Api-Version', 'Authorization'];

    if (res.methods.indexOf('OPTIONS') === -1) {
      res.methods.push('OPTIONS');
    }

    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Headers', allowHeaders.join(', '));
    res.header('Access-Control-Allow-Methods', req.header("Access-Control-Request-Method"));
    res.header('Access-Control-Allow-Origin', req.headers.origin);

    return res.send(204);
  }

  return res.send(new restifyErrors.MethodNotAllowedError());

});

// setup the audit logger after each request
server.on('after', restify.auditLogger({ log: logger, body:true }));

// setup the wicket keeper for the unhandled stuff
server.on('uncaughtException', (req, res, route, error) => {
  res.send(error);
  restify.auditLogger({ log: logger, body:true })(req, res, route, error);
});

/* Generalised party endpoint ***********************************************************/

server.get("/party/search",           mw.auth, routes.party.list);

server.get("/party/:id",              mw.auth, mw.assertPerm("read:entity"), routes.party.retrieve);
server.get("/party/:id/addresses",    mw.auth, mw.assertPerm("read:entity"), routes.party.address.list);
server.get("/party/:id/phones",       mw.auth, mw.assertPerm("read:entity"), routes.party.phone.list);
server.get("/party/:id/emails",       mw.auth, mw.assertPerm("read:entity"), routes.party.email.list);
server.get("/party/:id/socials",      mw.auth, mw.assertPerm("read:entity"), routes.party.social.list);
server.get("/party/:id/party-roles",  mw.auth, mw.assertPerm("read:entity"), routes.party.roles.list);
server.get("/party/:id/logins",       mw.auth, mw.assertPerm("read:entity"), routes.party.login.list);
server.get("/party/:id/accreditations", mw.auth, mw.assertPerm("read:entity"), routes.party.accreditation.list);
server.get("/party/:id/compliances", mw.auth, mw.assertPerm("read:entity"), routes.party.compliance.list);
server.get("/party/:id/bankaccounts", mw.auth, mw.assertPerm("read:entity"), routes.party.bankAccount.list);

server.get("/party/address/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.address.retrieve);
server.del("/party/address/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.address.delete);
server.put("/party/address/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.address.save);
server.post("/party/address",         mw.auth, mw.assertPerm("write:entity"), routes.party.address.save);

server.get("/party/phone/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.phone.retrieve);
server.del("/party/phone/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.phone.delete);
server.put("/party/phone/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.phone.save);
server.post("/party/phone",         mw.auth, mw.assertPerm("write:entity"), routes.party.phone.save);

server.get("/party/email/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.email.retrieve);
server.del("/party/email/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.email.delete);
server.put("/party/email/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.email.save);
server.post("/party/email",         mw.auth, mw.assertPerm("write:entity"), routes.party.email.save);

server.get("/party/party-role/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.roles.retrieve);
server.put("/party/party-role/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.roles.save);
server.post("/party/party-role",         mw.auth, mw.assertPerm("write:entity"), routes.party.roles.save);

server.post("/party/social",         mw.auth, mw.assertPerm("write:entity"), routes.party.social.save);

server.get("/party/accreditation/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.accreditation.retrieve);
server.del("/party/accreditation/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.accreditation.delete);
server.post("/party/accreditation",         mw.auth, mw.assertPerm("write:entity"), routes.party.accreditation.save);
server.post("/party/accreditation/:id",     mw.auth, mw.assertPerm("write:entity"), routes.party.accreditation.save);

server.get("/party/compliance/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.compliance.retrieve);
server.del("/party/compliance/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.compliance.delete);
server.post("/party/compliance",         mw.auth, mw.assertPerm("write:entity"), routes.party.compliance.save);
server.post("/party/compliance/:id",     mw.auth, mw.assertPerm("write:entity"), routes.party.compliance.save);

server.get("/party/bankaccount/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.bankAccount.retrieve);
server.del("/party/bankaccount/:id",      mw.auth, mw.assertPerm("write:entity"), routes.party.bankAccount.delete);
server.post("/party/bankaccount",         mw.auth, mw.assertPerm("write:entity"), routes.party.bankAccount.save);
server.post("/party/bankaccount/:id",     mw.auth, mw.assertPerm("write:entity"), routes.party.bankAccount.save);

server.post("/party/:id/party-role",      mw.auth, mw.assertPerm("write:entity"), routes.party.partyRole.save);

server.post("/party",     mw.auth, mw.assertPerm('write:entity'),  routes.party.create);
server.put("/party/:id",  mw.auth, mw.assertPerm('write:entity'),  routes.party.update);

server.get("/party/:id/history-notes", mw.auth, mw.assertPerm("read:history"), routes.history.list);
server.post("/party/:id/history-note", mw.auth, mw.assertPerm("write:history"), routes.history.create);
server.put("/party/history-note/:id", mw.auth, mw.assertPerm("write:history"), routes.history.update);

server.get("/party/:id/email-history", mw.auth, mw.assertPerm("read:history"), routes.history.listEmails);
server.get("/party/email-history/:id", mw.auth, mw.assertPerm("read:history"), routes.history.retrieveEmail);

server.get("/party/:id/insuredparties", mw.auth, mw.assertPerm("read:entity"), routes.abs.retrieveInsuranceDetails);

server.post("/party/:id/upload-logo", routes.party.uploadLogo);
server.get(/\/party\/logos\/?.*/, restify.serveStatic({
	directory: './uploads',
	default: 'logo-1'
}));

/* PartyRole endpoints *****************************************************************************/
server.get("/partyrole/:id/children", mw.auth, mw.assertPerm("read:entity"), routes.partyRole.retrieveChildren);
server.get("/partyrole/:id/children/:relationshipTypeId", mw.auth, mw.assertPerm("read:entity"), routes.partyRole.retrieveChildrenWithRelationshipTypeId);
server.get("/partyrole/:id/parents", mw.auth, mw.assertPerm("read:entity"), routes.partyRole.retrieveParents);
server.get("/partyrole/:id/parents/:relationshipTypeId", mw.auth, mw.assertPerm("read:entity"), routes.partyRole.retrieveParentsWithRelationshipTypeId);
server.post("/partyrole/relate", mw.auth, mw.assertPerm("write:entity"), routes.partyRole.relate);
server.post("/partyrole/unrelate", mw.auth, mw.assertPerm("write:entity"), routes.partyRole.unrelate);
server.post("/partyrole/:id/activation/status", mw.auth, mw.assertPerm("write:entity"), routes.partyRole.setPartyRoleActivationStatus);
server.post("/partyrole/:id/share", mw.auth, mw.assertPerm("write:entity"), routes.partyRole.requestShare);
server.get("/partyrole/share/requestee", mw.auth, mw.assertPerm("read:entity"), routes.partyRole.requesteeShareRequests);
server.get("/partyrole/share/requestor", mw.auth, mw.assertPerm("read:entity"), routes.partyRole.requestorShareRequests);
server.post("/partyrole/share/:id/respond", mw.auth, mw.assertPerm("write:entity"), routes.partyRole.respondToShareRequest);
server.post("/partyrole/:id/changeowner", mw.auth, mw.assertPerm("write:entity"), routes.partyRole.changeOwner);
server.del("/partyrole/:id", mw.auth, mw.assertPerm("write:entity"), routes.partyRole.delete);

server.get("/partyrole/:id/insuredparties/children", mw.auth, mw.assertPerm("read:entity"), routes.abs.listChildren);
server.get("/partyrole/:id/insuredparties/parentsandsiblings", mw.auth, mw.assertPerm("read:entity"), routes.abs.listParentAndSiblings);
server.get("/partyrole/:id/insuredparties", mw.auth, mw.assertPerm("read:entity"), routes.abs.listRelationships);
server.get("/partyrole/insuredparties/search", mw.auth, mw.assertPerm("read:entity"), routes.abs.listMatchedInsurers);
server.post("/partyrole/insuredparties", mw.auth, mw.assertPerm("write:entity"), routes.abs.saveRelationships);

/* Searches *****************************************************************************/

server.get("/address/search",         routes.address.searchForAddress);
server.get("/suburb/search",          routes.address.searchForSuburb);
server.get("/address",                routes.address.retrieveDetailed);

/* Specific role-based endpoints ********************************************************/

server.get("/broker/:id/program",mw.auth, routes.broker.retrieveCurrentProgram);
server.get("/broker/:id/programs",mw.auth, routes.broker.retrievePrograms);
server.post("/broker/:id/program",mw.auth, routes.broker.createProgram);

/* Utility endpoints ********************************************************************/

server.get("/me",         mw.auth, routes.user.me);

/* User endpoints ********************************************************************/
server.post("/user/validate", routes.user.validateCredentials);
server.get("/user/claims", mw.auth, mw.assertPerm("read:user"), routes.user.retrieveClaims);
server.post("/user/claims", mw.auth, mw.assertPerm("write:user"), routes.user.createUpdateClaims);
server.get("/user/access", mw.auth, mw.assertPerm("read:user"), routes.user.retrieveAccessInformation);
server.get("/user/roles", mw.auth, mw.assertPerm("read:user"), routes.user.retrieveRolesForUsername);
server.get("/user/:id/permissions", mw.auth, mw.assertPerm("read:user"), routes.user.retrievePermissionSet);
server.get("/user/:id/parties", mw.auth, mw.assertPerm("read:user"), routes.user.retrievePartiesForLogin);
server.post("/user/applicationRoles", mw.auth, mw.assertPerm("write:user"), routes.user.saveApplicationRoles);

server.get("/user", mw.auth, mw.assertPerm("read:user"), routes.admin.user.retrieveSummary);
server.post("/user/reset", mw.auth, mw.assertPerm("write:user"), routes.admin.user.resetPassword);
server.post("/user", mw.auth, mw.assertPerm("write:user"), routes.admin.user.createLogin);
server.get("/user/:id", mw.auth, mw.assertPerm("read:user"), routes.admin.user.retrieveDetailedLogin);
server.post("/user/:id/demote", mw.auth, mw.assertPerm("write:user"), routes.admin.user.demoteLoginStatus);
server.get("/user/:id/statistics", mw.auth, mw.assertPerm("read:user"), routes.admin.user.retrieveLoginStatistics);
server.put("/user/:id", mw.auth, mw.assertPerm("write:user"), routes.admin.user.updateUser);
server.get("/users", mw.auth, mw.assertPerm("read:user"), routes.admin.user.list);

/* template endpoints ********************************************************************/
server.get("/template/email", mw.auth, mw.assertPerm("read:template"), routes.template.listEmail);
server.get("/template/email/tags", mw.auth, mw.assertPerm("read:template"), routes.template.retrieveEmailTags);
server.get("/template/email/linklocation", mw.auth, mw.assertPerm("read:template"), routes.template.retrieveTemplateLinkLocation);
server.get("/template/email/:id", mw.auth, mw.assertPerm("read:template"), routes.template.retrieveEmail);
server.post("/template/email/preview", mw.auth, mw.assertPerm("read:template"), routes.template.previewTemplateEmail);
server.post("/template/email", mw.auth, mw.assertPerm("write:template"), routes.template.saveEmail);
server.del("/template/email/:id", mw.auth, mw.assertPerm("write:template"), routes.template.deleteEmail);

server.get("/template/sms", mw.auth, mw.assertPerm("read:template"), routes.template.listSms);
server.get("/template/sms/tags", mw.auth, mw.assertPerm("read:template"), routes.template.retrieveSmsTags);
server.get("/template/sms/:id", mw.auth, mw.assertPerm("read:template"), routes.template.retrieveSms);
server.post("/template/sms", mw.auth, mw.assertPerm("write:template"), routes.template.saveSms);
server.del("/template/sms/:id", mw.auth, mw.assertPerm("write:template"), routes.template.deleteSms);

server.get("/template/flyer", mw.auth, mw.assertPerm("read:template"), routes.template.listFlyer);
server.get("/template/flyer/tags", mw.auth, mw.assertPerm("read:template"), routes.template.retrieveFlyerTags);
server.get("/template/flyer/:id", mw.auth, mw.assertPerm("read:template"), routes.template.retrieveFlyer);
server.post("/template/flyer", mw.auth, mw.assertPerm("write:template"), routes.template.saveFlyer);
server.del("/template/flyer/:id", mw.auth, mw.assertPerm("write:template"), routes.template.deleteFlyer);

/* secondary authentication endpoints ********************************************************************/
server.post("/2fa/auth/", routes.secondAuth.authenticate);
server.post("/2fa", routes.secondAuth.create);
server.get("/2fa/:id", routes.secondAuth.retrieve);

/* news endpoints ********************************************************************/
server.get("/news", mw.auth, routes.newsFeed.list);
server.get("/news/:id", mw.auth, routes.newsFeed.retrieve);
server.post("/news", mw.auth, mw.assertPerm("write:news"), routes.newsFeed.save)
server.del("/news/:id", mw.auth, mw.assertPerm("write:news"), routes.newsFeed.delete);

/* message endpoints ********************************************************************/
server.get("/comm/message", mw.auth, mw.assertPerm("read:message"), routes.message.list);
server.get("/comm/message/:id", mw.auth, mw.assertPerm("read:message"), routes.message.retrieve);
server.post("/comm/message", mw.auth, mw.assertPerm("write:message"), routes.message.send);
server.post("/comm/message/:id/read", mw.auth, mw.assertPerm("write:message"), routes.message.read);
server.post("/comm/message/:id/identifier", mw.auth, mw.assertPerm("write:message"), routes.message.update);
server.del("/comm/message/:id", mw.auth, mw.assertPerm("write:message"), routes.message.delete);

/* email/sms endpoints ********************************************************************/
server.post("/comm/sms", mw.auth, mw.assertPerm("write:sms"), routes.comms.sendSms);
server.post("/comm/email", mw.auth, mw.assertPerm("write:email"), routes.comms.sendEmail);

/* link endpoints ********************************************************************/
server.post("/link/", mw.auth, mw.assertPerm("write:link"), routes.link.create);
server.get("/link/:id", mw.auth, mw.assertPerm("read:link"), routes.link.retrieve);
server.post("/link/:id/consume", mw.auth, mw.assertPerm("write:link"), routes.link.registerAccess);

/* branding endpoints ********************************************************************/
server.post("/brandstore/image", mw.auth, mw.assertPerm("write:brand-store"), routes.brandStore.uploadImage);
server.post("/brandstore", mw.auth, mw.assertPerm("write:brand-store"), routes.brandStore.save);
server.get("/brandstore/:id", mw.auth, mw.assertPerm("read:brand-store"), routes.brandStore.retrieve);

/* bank account endpoints ********************************************************************/
server.post("/bankaccount", mw.auth, mw.assertPerm("write:bank-account"), routes.bankAccount.save);
server.get("/bankaccount", mw.auth, mw.assertPerm("read:bank-account"), routes.bankAccount.list);

/* org id endpoints ********************************************************************/
server.post("/ext/orgid/party/:id", mw.auth, mw.assertPerm("write:entity"), routes.ext.orgId.save);
server.get("/ext/orgid/search", mw.auth, mw.assertPerm("read:entity"), routes.ext.orgId.identify);

/* lists endpoints ******************************************************************/
server.get("/lists", mw.auth, mw.assertPerm("read:lists"), routes.lists.getLists);
server.post("/lists", mw.auth, mw.assertPerm("write:lists"), routes.lists.saveList);
server.post("/lists/runList", mw.auth, mw.assertPerm("write:lists"), routes.lists.runList);
server.del("/lists/:id", mw.auth, mw.assertPerm("write:lists"), routes.lists.deleteList);
server.post("/lists/:id/favourite", mw.auth, mw.assertPerm("write:lists"), routes.lists.changeFavouriteStatus);
server.get("/lists/lookFor", mw.auth, mw.assertPerm("read:lists"), routes.lists.lookFor);
server.get("/lists/:id", mw.auth, mw.assertPerm("read:lists"), routes.lists.getList);


/* campaigns endpoints **************************************************************/
server.get("/campaigns", mw.auth, mw.assertPerm("read:campaigns"), routes.campaigns.getCampaignsList);
server.get("/campaigns/:id", mw.auth, mw.assertPerm("read:campaigns"), routes.campaigns.getCampaign);
server.post("/campaigns", mw.auth, mw.assertPerm("write:campaigns"), routes.campaigns.saveCampaign);

/* orphans endpoints *************************************************************/
server.get("/orphans", mw.auth, mw.assertPerm("read:orphans"), routes.orphans.getOrphansList);
/* settings endpoints *************************************************************/
server.get("/party-import/history", mw.auth, routes.settings.retrieveHistory);
/* settings endpoints *************************************************************/
server.post("/contact-import", mw.auth, mw.assertPerm("write:contact-import-attempt"), routes.settings.contactImport);
server.get("/settings/download/:spreadsheetType", mw.auth, routes.settings.downloadTemplate);
server.post("/settings/upload/:spreadsheetType", mw.auth, routes.settings.uploadSpreadsheet);

/* state endpoints *************************************************************/
server.get("/states", routes.state.list);

/* type lists endpoints *************************************************************/
server.get("/types/:type", mw.auth, mw.assertPerm("read:types"), routes.typeLists.getList);

server.get('/ver', (req, res, next) => {

  let pkg = require("./package.json");

  res.send({
    name: pkg.name,
    version: pkg.version,
    description: pkg.description
  });

  return next();

});


module.exports = server;
