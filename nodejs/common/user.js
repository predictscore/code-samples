'use strict';

/**
 * User adapter class
 * @module user.js
 *
 * Acts as a lense into more complex user objects to easily present
 * information important to Firstmac processing
 */

const Q       = require('q'),
      config  = require('config'),
      _       = require('lodash'),
      util    = require('util'),
      fs      = require('fs'),
      jwt     = require('restify-jwt'),
      restifyErrors = require('restify-errors');

const logger  = require('./logger');

let dao = require('../dao');
function _getDao() {
    if(!dao.crumbs) {dao = require('../dao');}
    return dao;
}
const signingCertificate = fs.readFileSync('./secrets/signing.cer');

/*
var PermissionNotAssertedError = function (message, perms) {
  Error.captureStackTrace(this, this.constructor);

  this.name = 'PermissionNotAssertedError';
  this.message = message;
  this.perms = perms;
};

var ObjectAccessNotAssertedError = function (message, objs) {
  Error.captureStackTrace(this, this.constructor);

  this.name = 'ObjectAccessNotAssertedError';
  this.message = message;
  this.objs = objs;
};

util.inherits(PermissionNotAssertedError, restifyErrors.NotAuthorizedError)
util.inherits(ObjectAccessNotAssertedError, restifyErrors.NotAuthorizedError)
*/


const ATTRS = {
  primaryGroup:       "http://schemas.firstmac.com.au/ws/2015/03/identity/claims/primarygroup",
  siteTypeId:         "http://schemas.firstmac.com.au/ws/2009/12/identity/claims/sitetypeid",
  commonName:         "common_name",
  crumbsLoginId:      "crumbs_login_id",
  crumbsPartyRoleId:  "crumbs_party_role_id",
  v8PartyRoleId:      "v8_party_role_id",
  familyName:         "family_name",
  givenName:          "given_name",
  userName:           "name"
};

var anonymousUserData = {
  "http://schemas.firstmac.com.au/ws/2015/03/identity/claims/primarygroup": "nogroup",
  "http://schemas.firstmac.com.au/ws/2009/12/identity/claims/sitetypeid": null,
  "common_name": "nobody",
  "crumbs_login_id": null,
  "crumbs_party_role_id": null,
  "v8_party_role_id": null,
  "family_name": "Anonymous",
  "given_name": "Anonymous",
  "name": null
};

let UserAdapter = function (userData) {
  let self = this;

  self.userData = userData || anonymousUserData;

  /**
   * Determines if the user is anonymous
   * @func isAnonymous
   * @returns Boolean
   */
  self.isAuthenticated = () => self.userData != null && self.userData != anonymousUserData;

  /**
   * Retrieves the user's loginId
   * @func getLoginId
   * @returns Number
   */
  self.getLoginId = () => parseInt(self.userData[ATTRS.crumbsLoginId]);

  /**
   * Retrieves the user's full name
   * @func getFullName
   * @returns String
   */
  self.getFullName = () => this.userData[ATTRS.commonName];

  /**
   * Retrieves the user's name
   * @func getUsername
   */
  self.getUsername = () => this.userData[ATTRS.userName];

  /**
   * Retrieves the user's primary group
   * @func getPrimaryGroup
   */
  self.getPrimaryGroup = () => this.userData[ATTRS.primaryGroup];

  /**
   * Retrieves the user's site type id
   * @func getSiteTypeId
   */
  self.getSiteTypeId = () => parseInt(this.userData[ATTRS.siteTypeId]);

  /**
   * Retrieves the user's party role id
   * @func getPartyRoleId
   */
  self.getPartyRoleId = () => parseInt(this.userData[ATTRS.crumbsPartyRoleId]);

  /**
   * Retrieves a list of role strings from the database
   * @func getRoles
   * @returns {Promise} Returning an array of roles
   */
  self.getRoles = () => {

    let applicationType = config.const.applicationTypeId;

    return _getDao().crumbs.ApplicationRole.findAll({
      where: {
        $and: [
          { loginID: self.getLoginId() },
          { applicationTypeID: applicationType }
        ]
      },
      include: [
        { model: _getDao().crumbs.RoleType, as: 'RoleType' }
      ]
    }).then(ars => {
      return _.map(ars, x => x.RoleType.name);
    });

  };

  /**
   * Retrieves a list of assertable permissions for the user
   * @func getPerms
   * @returns {Promise} Returning an array of roles
   */
  self.getPerms = () => {

    /*let applicationType = config.get("const").applicationTypeId;
    let userLoginId = self.getLoginId();
    if (_.isNaN(userLoginId)) {
      return [];
    }

    return _getDao().crumbs.ApplicationRole.findAll({
      where: {
        $and: [
          { loginID: userLoginId },
          { applicationTypeID: applicationType }
        ]
      },
      include: [{
        model: _getDao().crumbs.RoleType,
        as: 'RoleType',
        include: [
          { model: _getDao().crumbs.PermissionType, as: 'RoleTypePermissionTypePermissionTypes' }
        ]
      }]
    }).then(ars => {
      return _.flatMap(ars, x => {
          return  _.map(x.RoleType.RoleTypePermissionTypePermissionTypes, r => r.name)
      })
    });*/

    return Q([
      "read:entity",
      "write:entity",
      "read:history",
      "write:history",
      "read:user",
      "write:user",
      "read:lists",
      "write:lists",
      "read:campaigns",
      "write:campaigns",
      "read:orphans",
      "write:template",
      "read:template",
      "write:types",
      "read:types",
      "write:address",
      "write:link",
      "read:link",
      "write:message",
      "read:message",
      "write:news"
    ]);
  };

  /**
   * Asserts a single or array of permissions
   * @func assertPermission
   * @param perms {String|Array}
   */
  self.assertPermission = function (perms) {

    return Q.Promise((resolve, reject, progress) => {

      if (typeof perms == 'string') {
        perms = [perms];
      } else if (!Array.isArray(perms)) {
        return reject(new Error('No permissions were specified to assert'));
      }

      return self.getPerms()
        .then(actual => {

          // establish the differential in requested verses actual permissions
          let diff = _.difference(perms, actual)

          if (diff.length > 0) {
            return reject(
              new restifyErrors.NotAuthorizedError({
                message: 'User did not assert the required permissions',
                context: {
                  perms: perms
                }
              })
            );
          }

          return resolve(true);
        });

    });

  };

  /**
   * Asserts a dictionary of object access requests for this user
   * @func assertObjectAccess
   * @param config {Object} The access to assert
   * @returns Promise[bool]
   */
  self.assertObjectAccess = function (config) {

    return Q.Promise((resolve, reject, progress) => {

      if (config == null) {
        return reject(new Error('No objects were specified to assert'));
      }

      // we'll get the assertion to raise an error
      if (false) {
        // TODO: format the name of the failing permission into the string
        return reject(new ObjectAccessNotAssertedError('User does not have security scope over the requested objects', config));
      }

      return resolve(true);

    });

  };

};

/** Sets up the user adapter to operate on incoming user data
  * @func _setupAdapter
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next function in middleware chain */
function _setupAdapter(req, res, next) {
  req.userAdapter = new UserAdapter(req.user);
  return next();
}

/** Asserts application defined permissions against user assinged ones
  * @func _assertPerm
  * @param perms {String|Array} A string or array of permission names to test
  * @returns {Promise} Permission assertion */
function _assertPerm(perms) {
  return (req, res, next) => {

    return req.userAdapter.assertPermission(perms)
      .then(ok => {
        return next();
      })
      .catch(next)
      .done();

  };
}

let _auth = [ jwt({ secret: signingCertificate }), _setupAdapter ];
let _anon = [ _setupAdapter ];


module.exports = {
  UserAdapter: UserAdapter,

  mw: {
    auth:       _auth,
    anon:       _anon,
    assertPerm: _assertPerm
  }
};
