'use strict';

/**
 * Bunyan logger implementation
 * @module logger
 */

const config          = require('config'),
      bunyan          = require('bunyan'),
      restify         = require('restify'),
      restifyErrors   = require('restify-errors'),
      packageJson     = require('../package.json'),
      KinesisWritable = require('aws-kinesis-writable');

// create the base logger configuration
let loggerOpts = {
  name: packageJson.name,

  serializers: {
    req: bunyan.stdSerializers.req,
    res: bunyan.stdSerializers.res,
    err: restifyErrors.bunyanSerializer
  },

  streams: [{
    level: "trace",
    stream: process.stdout
  }]
};

let loggerConf = config.get("logger");

if (loggerConf) {

  // only add the bunyan logstash stream, if there was one supplied
  // in the configuration
  if (loggerConf.kinesis) {

    loggerOpts.streams.push({
      stream: new KinesisWritable(loggerConf.kinesis)
    });

  }

}

module.exports = bunyan.createLogger(loggerOpts);
