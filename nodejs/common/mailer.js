"use strict";

/** Mailer module 
 *  @module common/mailer */

const config = require('config'),
      nodemailer = require('nodemailer'),
      Q = require('q'),
      logger = require('fm-common').logger;


/** Invokes the mailer module to perform its communication task
 *  @func _sendMail
 *  @param opts {Object} The email parameters
 *  @returns A promise resolving the send mail result */ 
function _sendMail(opts) {

  let mailerConfig = config.get('mailer');

  /* only in scenarios where the mailer configuration has been explicitly specified
   * should we even attempt to send email */
  if (mailerConfig != null) {

    let transport = nodemailer.createTransport(mailerConfig);

    return Q.post(transport, "sendMail", opts)
      .then(info => {
        logger.info(info);
        return info;
      });

  } else { 
    /* otherwise, we should just log the invocation out to the logger */
    logger.info(opts, 'Non-network sendmail invocation');
    return Q({});
  }

}

/** Send mail function frontent to transition function arity to callers */
module.exports.sendMail = function (from, to, cc, bcc, subject, text, html, attachments) {
  return _sendMail({ from, to, cc, bcc, subject, text, html, attachments });
};

