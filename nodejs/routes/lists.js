'use strict';

/** Lists api routes
 * @module routes/lists */

const dao = require('../dao');
const restifyErrors   = require("restify-errors"),
      _               = require("lodash"),
      utils           = require('./utils');

function getLists(req, res, next) {
  let searchTerm;
  if (req.query.type === 'contacts') {
    searchTerm = 1;
  } else if (req.query.type === 'accounts') {
    searchTerm = 2;
  } else {
    searchTerm = [1, 2];
  }

  dao.crumbs.List.findAll({
    where: {
      partyTypeFilter: searchTerm,
      active: {$not: false},
    },
    include: {
      model: dao.crumbs.PartyRole, as: 'CreatedBy',
      include: {
        model: dao.crumbs.Party, as: 'Party',
        include: [{
          model: dao.crumbs.Person, as: 'PersonPartyFks'
        }, {
          model: dao.crumbs.Company, as: 'CompanyPartyFks'
        }, {
          model: dao.crumbs.Trust, as: 'TrustPartyFks'
        }]
      }
    }
  })
    .then(result => {

      if (!result || (result && !result.length)) {
        return next(new restifyErrors.NotFoundError());
      }

      let response = result.map(l=>{
        if(l.dataValues){
          l = l.get({plain:true});
        }
        if(l.CreatedBy && l.CreatedBy.Party){
          l.partyName = dao.utils.partyNameByParty(l.CreatedBy.Party);
        }
        return _.pickBy(l, (value, key)=> {
          return key != 'CreatedBy';
        })
      });

      res.send(response);
      return next();
    })
    .catch(next)
    .done();
}

function runList(req, res, next) {
  let filters = {
    loginId: req.userAdapter.getLoginId(),
    partyTypeFilter: req.body.partyTypeFilter || null,
    stateFilter: req.body.stateFilter || null,
    partyRoleTypeFilter: req.body.partyRoleTypeFilter || null,
    ownerFilter: req.body.ownerFilter || null,
    statusFilter: req.body.statusFilter || null,
    nameFilter: req.body.nameFilter || null,
    hasLoginFilter: req.body.hasLoginFilter || null,
    postcodeFilter: req.body.postcodeFilter || null,
    lastActivityDateFilter: req.body.lastActivityDateFilter || null,
    lastAppDateBeforeFilter: req.body.lastAppDateBeforeFilter || null,
    excludedPartyFilter: req.body.excludedPartyFilter || null,
    page: req.body.page || null,
    pageSize: req.body.pageSize || null,
  };
  let stateFilter = (!filters.stateFilter || filters.stateFilter.length == 0) ? 'NULL' : "'{:stateFilter}'";
  let partyRoleTypeFilter = (!filters.partyRoleTypeFilter || filters.partyRoleTypeFilter.length == 0) ? 'NULL' : "'{:partyRoleTypeFilter}'";
  let ownerFilter = (!filters.ownerFilter || filters.ownerFilter.length == 0) ? 'NULL' : "'{:ownerFilter}'";
  let statusFilter = (!filters.statusFilter || filters.statusFilter.length == 0) ? 'NULL' : "'{:statusFilter}'";
  let excludedPartyFilter = (!filters.excludedPartyFilter || filters.excludedPartyFilter.length == 0) ? 'NULL' : "'{:excludedPartyFilter}'";

  let query = 'SELECT * FROM "List_Retrieve"(:loginId, :partyTypeFilter, '+stateFilter+', '+partyRoleTypeFilter+', '+ownerFilter+', ' +
    statusFilter+', :nameFilter, :hasLoginFilter, :postcodeFilter, :lastActivityDateFilter, :lastAppDateBeforeFilter, ' +
    excludedPartyFilter+', :page, :pageSize)';
  return dao.sequelize.query(query, {
        replacements: filters,
        type: dao.sequelize.QueryTypes.SELECT,
      }
    )
    .then(list => {
      res.send(list);
      return next();
    })
    .catch(next)
    .done();
}

function saveList(req, res, next) {
  delete req.body.created;
  delete req.body.createdByPartyRoleID;
  if (!req.body.iD) {
    req.body.created = new Date();
    req.body.createdByPartyRoleID = req.userAdapter.getPartyRoleId();
  }

  req.body.lastUpdated = new Date();
  req.body.lastUpdatedByPartyRoleID = req.userAdapter.getPartyRoleId();
  dao.sequelize.transaction(tran => {
      return dao.crumbs.List.findOne({
          where: { iD: req.body.iD },
          transaction: tran,
        })
        .then(result => {
          let promise;
          if (!result) {
            if (req.body.iD) {
              return next(new restifyErrors.NotFoundError());
            }
            promise = dao.crumbs.List.create(req.body);
          } else {
            promise = dao.crumbs.List.update(req.body, {
              where: { iD: req.body.iD },
            });
          }

          return promise
            .then((l) => {
              let id = req.body.iD || l.iD;
              if (!result) {
                res.send(201, id);
              } else {
                res.send(200, id);
              }

              return next();
            })
            .catch(next);
        })
        .catch(next);
    })
    .then()
    .catch(next)
    .done();
}

function deleteList(req, res, next) {
  dao.crumbs.List.update({
      active: false,
    }, {
      where: {
        iD: req.params.id,
        active: { $not: false },
      },
    })
    .then((r) => {
      if (r[0] === 0) {
        return next(new restifyErrors.NotFoundError());
      }

      res.send(204);
      return next();
    })
    .catch(next)
    .done();
}

function changeFavouriteStatus(req, res, next) {
  if (req.query.favourite !== 'true' && req.query.favourite !== 'false') {
    if (!req.query.favourite) {
      req.query.favourite = 'true';
    } else {
      return next(new restifyErrors.BadRequestError('favourite'));
    }
  }

  dao.crumbs.List.update({
      favourite: req.query.favourite,
    }, {
      where: { iD: req.params.id },
    })
    .then((r) => {
      if (r[0] === 0) {
        return next(new restifyErrors.NotFoundError());
      } else {
        res.end();
      }

      return next();
    })
    .catch(next)
    .done();
}

function getList(req, res, next) {

  let id = req.params.id;

  if (!utils.ci(id)) {
    return next(new restifyErrors.BadRequestError('id'));
  }

  dao.crumbs.List.findOne({
    where: {iD: id},
  })
    .then(result => {
      if (!result) {
        return next(new restifyErrors.NotFoundError());
      }

      let promises =[];
      let owners = [];

      if (result.ownerFilter && result.ownerFilter.length > 0) {
      result.ownerFilter.forEach(partyRoleId =>{
        promises.push(dao.crumbs.PartyRole.findOne({
            where: {iD: partyRoleId},
            include: {
              model: dao.crumbs.Party,
              as: "Party",
              include:[{
                model: dao.crumbs.Person,
                as: "PersonPartyFks"
              },
                {
                  model: dao.crumbs.Company,
                  as: "CompanyPartyFks"
                },
                {
                  model: dao.crumbs.Trust,
                  as: "TrustPartyFks"
                }]
            }
        }).then(pr =>{

          let partyName;

          if (pr && pr.Party) {
            partyName = dao.utils.partyNameByParty(pr.Party);
          }

          owners.push({
            partyRoleId,
            partyName
          })
        }));
      });
      }

      Promise.all(promises).then(r =>{
        result.ownerFilter = owners;
        res.send(result);
        return next();
      });
    })
    .catch(next)
    .done();
}

function lookFor(req, res, next) {
  res.send([
    {
      id: 1,
      name: 'All contacts',
    }, {
      id: 2,
      name: 'My contacts',
    }, {
      id: 3,
      name: 'My owned contacts',
    }, {
      id: 4,
      name: 'Al accounts',
    }, {
      id: 5,
      name: 'My accounts',
    }, {
      id: 6,
      name: 'My owned accounts',
    },
  ]);
  return next();
}

module.exports = {
  getLists,
  saveList,
  runList,
  deleteList,
  changeFavouriteStatus,
  getList,
  lookFor
};
