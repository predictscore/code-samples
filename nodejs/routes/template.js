"use strict";

const dao = require('../dao'),
  errors = require('restify-errors'),
  mongo = require('../dao/mongo'),
  ObjectId = require('mongoose').Types.ObjectId,
  emailTemplate = mongo.models.emailTemplate,
  flyerTemplate = mongo.models.flyerTemplate,
  smsTemplate = mongo.models.smsTemplate;

/** Template routes
    @module routes/template

 TODO: attachment uploading to go through the files service */
/** List out templates
 * @func retrieveListByTags
 * @param req {Object} Incoming request object
 * @param res {Object} Outgoing response object
 * @param func {Function} Next function in the middleware chain
 * @param template {Object} Current schema
 *
 */
function retrieveListByTags(req, res, next, template) {
    const tag = req.query.t;
    let result;
    if (tag) {
        result = template.find({tags: tag});
    } else {
        result = template.find();
    }
    return result.then(function (list) {
        res.send(200, list);
    })
        .catch(next)
        .done();
}

/** Saves a template
 * @func saveDocument
 * @param req {Object} Incoming request object
 * @param res {Object} Outgoing response object
 * @param func {Function} Next function in the middleware chain
 * @param template {Object} Current schema
 *
 */
 function saveDocument (req, res, next, template) {
   if (req.body._id && !ObjectId.isValid(req.body._id)) {
     return next(new errors.BadRequestError());
   }
   let userDate = {
     date: new Date(),
     user:{
       id: req.userAdapter.getLoginId(),
       name: req.userAdapter.getUsername()
     }
   };
   req.body.updated = userDate;
   delete req.body.created;
   let upsert = false;
   if (!req.body._id) {
     upsert = true;
     req.body.created = userDate;
   }
   return template.findOneAndUpdate ({_id: new ObjectId(req.body._id)}, req.body, {upsert: upsert, new: true, strict: false})
     .then(function (result) {
       if (!result) {
         return next(new errors.NotFoundError());
       }

       res.send((req.body._id) ? 200 : 201, result.toObject());
     })
     .catch(err => {
       return next(new errors.NotFoundError());
     })
     .done();
 }

     /** Deletes a template
      * @func deleteDocument
      * @param req {Object} Incoming request object
      * @param res {Object} Outgoing response object
      * @param func {Function} Next function in the middleware chain
      * @param template {Object} Current schema
      *
      */
     function deleteDocument (req, res, next, template){
       if (!ObjectId.isValid(req.params.id)) {
         console.log(req.params.id)
         return next(new errors.BadRequestError());
       }
       return template.remove({_id: new ObjectId(req.params.id)})
         .then((result) => {
           if(result.result.n == 0){
             return next(new errors.NotFoundError());
           } else {
             res.send(204);
           }
         })
         .catch(next)
         .done();
     }

     /** Retrieve array of tags for template
      * @func retrieveTags
      * @param req {Object} Incoming request object
      * @param res {Object} Outgoing response object
      * @param func {Function} Next function in the middleware chain
      * @param template {Object} Current schema
      *
      */
     function retrieveTags(req, res, next, template){
       return template.distinct('tags')
         .then(function (list) {
           res.send(200, list);
         })
         .catch(next)
         .done();
     }

   /** Retrieves a template
    * @func retrieveDocument
 * @param req {Object} Incoming request object
 * @param res {Object} Outgoing response object
 * @param func {Function} Next function in the middleware chain
 * @param template {Object} Current schema
 *
 */
function retrieveDocument(req, res, next, template){
    if (!ObjectId.isValid(req.params.id)) {
        return next(new errors.BadRequestError());
    }
    return template.findOne({_id: new ObjectId(req.params.id)})
        .then(function (document) {
            if (document) {
                res.send(200, document);
            } else {
                return next(new errors.NotFoundError());
            }
        })
        .catch(next)
        .done();
}

/** List out email templates
  * @func _listEmail
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next function in the middleware chain
  *
  */
function _listEmail(req, res, next) {
    retrieveListByTags(req, res, next, emailTemplate);
}

/** List out sms templates
  * @func _listSms
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next function in the middleware chain
  *
  */
function _listSms(req, res, next) {
  retrieveListByTags(req, res, next, smsTemplate);
}

/** List out flyer templates
 * @func _listFlyer
 * @param req {Object} Incoming request object
 * @param res {Object} Outgoing response object
 * @param func {Function} Next function in the middleware chain
 *
 */
function _listFlyer(req, res, next) {
    retrieveListByTags(req, res, next, flyerTemplate);
}

/** Retrieve email tags
  * @func _retrieveEmailTags
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next function in the middleware chain
  *
  */
function _retrieveEmailTags(req, res, next) {
    retrieveTags(req, res, next, emailTemplate);
}

/** Retrieve a unique list of tags associated to sms templates
  * @func _retrieveSmsTags
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next function in the middleware chain
  *
  */
function _retrieveSmsTags(req, res, next) {
    retrieveTags(req, res, next, smsTemplate);
}

/** Retrieve a unique list of tags associated to flyer templates
 * @func _retrieveFlyerTags
 * @param req {Object} Incoming request object
 * @param res {Object} Outgoing response object
 * @param func {Function} Next function in the middleware chain
 *
 */
function _retrieveFlyerTags(req, res, next) {
  retrieveTags(req, res, next, flyerTemplate);
}

/** Retrieves an email template
  * @func _retrieveEmail
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next function in the middleware chain
  *
  */
function _retrieveEmail(req, res, next) {
    retrieveDocument(req, res, next, emailTemplate);
}

/** Retrieves an sms template
  * @func _retrieveSms
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next function in the middleware chain
  *
  */
function _retrieveSms(req, res, next) {
    retrieveDocument(req, res, next, smsTemplate);
}

/** Retrieves an flyer template
 * @func _retrieveFlyer
 * @param req {Object} Incoming request object
 * @param res {Object} Outgoing response object
 * @param func {Function} Next function in the middleware chain
 *
 */
function _retrieveFlyer(req, res, next) {
    retrieveDocument(req, res, next, flyerTemplate);
}

/** Saves an email template
  * @func _saveEmail
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next function in the middleware chain
  *
  */
function _saveEmail(req, res, next) {
    saveDocument (req, res, next, emailTemplate);
}

/** Save an sms template
  * @func _saveSms
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next function in the middleware chain
  *
  */
function _saveSms(req, res, next) {
    saveDocument (req, res, next, smsTemplate);
}

/** Save an flyer template
 * @func _saveFlyer
 * @param req {Object} Incoming request object
 * @param res {Object} Outgoing response object
 * @param func {Function} Next function in the middleware chain
 *
 */
function _saveFlyer(req, res, next) {
  saveDocument (req, res, next, flyerTemplate);
}

/** Deletes an email template
  * @func _deleteEmail
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next function in the middleware chain
  *
  */
function _deleteEmail(req, res, next) {
    deleteDocument (req, res, next, emailTemplate);
}

/** Deletes an sms templte
  * @func _deleteSms
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next function in the middleware chain
  *
  */
function _deleteSms(req, res, next) {
    deleteDocument (req, res, next, smsTemplate);
}

/** Deletes an flyer template
 * @func _deleteFlyer
 * @param req {Object} Incoming request object
 * @param res {Object} Outgoing response object
 * @param func {Function} Next function in the middleware chain
 *
 */
function _deleteFlyer(req, res, next) {
    deleteDocument (req, res, next, flyerTemplate);
}

/** Fabricates a template email
    @func _previewTemplateEmail
    @param req {Object} Incoming request object
    @param res {Object} Outgoing response object
    @param next {Function} Next middleware function in the chain

    CommsService.PreviewTemplateEmail
    CommsService.PreviewTemplateEmailByName
*/
function _previewTemplateEmail(req, res, next) {

}

/** Retrieves the template link location
    @func _retrieveTemplateLinkLocation
    @param req {Object} Incoming request object
    @param res {Object} Outgoing response object
    @param next {Function} Next middleware function in the chain

    CommsService.RetrieveTemplateLinkLocation
*/
function _retrieveTemplateLinkLocation(req, res, next) {
  return dao.crumbs.Login.findById(req.userAdapter.getLoginId(), {
      include: [{
        model: dao.crumbs.SiteType,
        as: 'SiteType',
        include: [{
          model: dao.crumbs.SiteTypeTemplateLinkLocation,
          as: 'TemplateLinkLocationSitetypeFks',
          include: [{
            model: dao.crumbs.LinkLocation,
            as: 'LinkLocation',
          }],
        }],
      }],
    })
    .then(ll => {
      if (!ll.SiteType.TemplateLinkLocationSitetypeFks[0]) {
        return next(new errors.NotFoundError());
      }
      let linkLocation = ll.SiteType.TemplateLinkLocationSitetypeFks[0].LinkLocation.dataValues;
      let siteTypeTemplateLinkLocation = ll.SiteType.TemplateLinkLocationSitetypeFks[0];
      res.send({
        location: {
          iD: linkLocation.iD,
          name: linkLocation.name,
          href: linkLocation.href,
          expireOnAccess: linkLocation.expireOnA,
          lifespan: linkLocation.lifespan,
        },
        siteTypeTemplateLinkLocation: {
          siteTypeID: siteTypeTemplateLinkLocation.siteTypeID,
          templateName: siteTypeTemplateLinkLocation.templateName,
          linkLocationID: siteTypeTemplateLinkLocation.linkLocationID,
        }
      });
      return next();
    })
    .catch(next);
}

module.exports = {
  listEmail:          _listEmail,
  listSms:            _listSms,
  listFlyer:          _listFlyer,
  retrieveEmailTags:  _retrieveEmailTags,
  retrieveSmsTags:    _retrieveSmsTags,
  retrieveFlyerTags:  _retrieveFlyerTags,
  retrieveEmail:      _retrieveEmail,
  retrieveSms:        _retrieveSms,
  retrieveFlyer:      _retrieveFlyer,
  saveEmail:          _saveEmail,
  saveSms:            _saveSms,
  saveFlyer:          _saveFlyer,
  deleteEmail:        _deleteEmail,
  deleteSms:          _deleteSms,
  deleteFlyer:        _deleteFlyer,
  previewTemplateEmail:           _previewTemplateEmail,
  retrieveTemplateLinkLocation:   _retrieveTemplateLinkLocation
};
