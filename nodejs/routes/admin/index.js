"use strict";

/** Admin routes module
  * @module routes/admin */

module.exports = {
	user: 	require("./user")
};