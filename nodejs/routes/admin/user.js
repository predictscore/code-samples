"use strict";

/** User administration module
  * @module routes/admin/user */
const dao     = require("../../dao"),
      common  = require('fm-common'),
      _       = require('lodash'),
      errors  = require('restify-errors'),
      crypto  = require('crypto'),
	    utils   = require('../utils');

/** Retrieves an account security and login summary for a username and sitetypeid pair
  * @func _retrieveSummary
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next middleware function
  *
  * UserAdminService.RetrieveSummaryForUsername
  */
function _retrieveSummary(req, res, next) {
    let filters = {
        username: req.query.un || req.userAdapter.getUsername(),
        siteTypeId: req.query.stid || req.userAdapter.getSiteTypeId()
    };
    if (!Number.isInteger(+req.query.stid)){
        return next(new errors.BadRequestError('siteTypeId'));
    }
    return dao.crumbs.Login.findOne({
      where: {
        username: { $iLike: filters.username },
        siteTypeID: filters.siteTypeId,
        active: true,
      },
      include: [{
        model: dao.crumbs.ClaimValue,
        as: 'ClaimValueLoginFks'
      }, {
        model: dao.crumbs.LoginAttempt,
        as: 'AttemptLoginFks',
        attributes: ['loginID', 'when', 'successful']
      }, {
        model: dao.crumbs.ApplicationRole,
        as: 'ApplicationRoleLoginFks'
      }]
    })
      .then((result) => {
        if (!result) {
          return next(new errors.NotFoundError());
        }
        result = result.dataValues;
        result.password = undefined;
        result.SiteTypeID = undefined;
        result.claims = result.ClaimValueLoginFks;
        result.ClaimValueLoginFks = undefined;
        result.attempts = result.AttemptLoginFks;
        result.AttemptLoginFks = undefined;
        result.roles = result.ApplicationRoleLoginFks;
        result.ApplicationRoleLoginFks = undefined;
        result.objectSID = undefined;
        result.decommissioned = undefined;
        res.send(result);
        return next();
      })
      .catch(next)
      .done();
}

/** Retrieves a set of login statistics for any given username
  * @func _retrieveLoginStatistics
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function
  *
  * UserAdminService.RetrieveExcessivePotentialsForLogin
  */
function _retrieveLoginStatistics(req, res, next) {

}

/** Deactivates or decomissions a user account
  * @func _demoteUserStatus
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function
  *
  * UserAdminService.DeactivateLogin
  * UserAdminService.DecomissionLogin
  */
function _demoteLoginStatus(req, res, next) {
  let Login = dao.crumbs.Login,
    SiteType = dao.crumbs.SiteType,
    LoginAttempt = dao.crumbs.LoginAttempt;

  Login.belongsTo(SiteType);
  SiteType.hasMany(Login);
  LoginAttempt.belongsTo(Login);
  Login.hasMany(LoginAttempt);

  return Login.findOne({
    where: { ID: req.params.id },
    include: [{
      model: SiteType,
    }, {
      model: dao.crumbs.LoginAttempt,
    },],
  })
    .then(login=> {
        if (!login) {
          return next(new errors.NotFoundError());
        }

        let param;
        switch (req.body.action) {
          case 1:
            param = { decommissioned: true, };
            break;
          case 2:
            param = { locked: true, expired: new Date(), };
            break;
          default:
            return next(new errors.BadRequestError());
            break;
        }
        login.updateAttributes(param);

        return login.save()
          .then(() => {
            let result = {
              attemptCount: login.LoginAttempts.length,
              siteTypeName: login.SiteType.name,
              expired: login.expired,
              loginId: login.iD,
              username: login.username,
              locked: login.locked,
            };
            res.send(result);
            return next();
          });
      }
    );
}

/** Retrieves a detailed login block
  * @func _retrieveDetailedLogin
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function
  *
  * UserAdminService.RetreiveDetailedLogin
  * UserAdminService.RetrieveDetailedLoginBySubjectId
  * UserAdminService.RetrieveDetailedLoginForEmail
  */
function _retrieveDetailedLogin(req, res, next) {
  let loginId = req.params.id;

  return dao.crumbs.Login.findOne({
    where: {ID: loginId},
    include: [{
      model: dao.crumbs.ClaimValue, as: 'ClaimValueLoginFks'
    }, {
      model: dao.crumbs.ApplicationRole, as: 'ApplicationRoleLoginFks'
    }, {
      model: dao.crumbs.PermissionType, as: 'LoginPermissionTypePermissionTypes'
    }]
  }).then(r => {
    if (!r || !r.iD) {
      return next(new errors.NotFoundError());
    }
    let result = {
      claims: r.ClaimValueLoginFks,
      displayName: null,
      expired: r.expired,
      locked: r.locked,
      id: r.iD,
      partyRoleId: null,
      active: r.active,
      roles: r.ApplicationRoleLoginFks,
      permissions: r.LoginPermissionTypePermissionTypes,
      username: r.username
    };
    r.ClaimValueLoginFks.find(cv => {
      if (cv.claimTypeID == 14) {
        result.partyRoleId = +cv.value;
      }
    });
    if (result.partyRoleId) {
      return dao.crumbs.PartyRole.findOne({
        where: {iD: result.partyRoleId},
        include: {
          model: dao.crumbs.Party,
          as: 'Party',
          include: [{
            model: dao.crumbs.PartyRole,
            as: 'RolePartyFks'
          }, {
            model: dao.crumbs.Person,
            as: 'PersonPartyFks'
          }, {
            model: dao.crumbs.Company,
            as: 'CompanyPartyFks'
          }, {
            model: dao.crumbs.Trust,
            as: 'TrustPartyFks'
          }]
        }
      })
        .then(pr => {
          if (pr.Party) {
            result.displayName = dao.utils.partyNameByParty(pr.Party);
          }
          if (pr.Party && pr.Party.RolePartyFks && pr.Party.RolePartyFks.length) {
            result.partyRoles = _.uniqBy(pr.Party.RolePartyFks
              .filter(p => (p.partyRoleTypeID == 134 || p.partyRoleTypeID == 135 || p.partyRoleTypeID == 136))
              .map(p => {
                return {partyRoleTypeID: p.partyRoleTypeID}
              }), 'partyRoleTypeID');
          }
          res.send(result);
          return next();
        })
    }
    res.send(result);
    return next();
  }).catch(next)
    .done();
}

/** Deletes a login
  * @func _deleteLogin
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function
  *
  * UserAdminService.DeleteLogin
  */
function _deleteLogin(req, res, next) {

}

/** Resets a login's password
  * @func _resetPassword
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function
  *
  * UserAdminService.ResetPassword
  */
function _resetPassword(req, res, next) {
  let expired;
  if (req.body.expirePassword == true) {
    expired = new Date();
  }
  let data = {
    password: crypto.createHash('sha256').update(req.body.password).digest('base64'),
    locked: false,
    expired: expired
  };
  return dao.crumbs.Login.update(data, {where: {ID: req.body.loginId}})
    .then(result => {
      if (!result || !result[0]) {
        return next(new errors.NotFoundError());
      }
      res.send(true);
      return next();
    })
    .catch(err => {
      res.send(err);
    })
    .done();
}

/** Creates a login
  * @func _createLogin
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function
  *
  * UserAdminService.CreateLogin
  */
function _createLogin(req, res, next) {
let now = new Date(),
    user = {
      username: req.body.username,
      siteTypeID: req.body.siteTypeID,
      active: true,
      locked: false,
      expired: now
  };
  if(req.body.password){
    user.password = crypto.createHash('sha256').update(req.body.password).digest('base64')
  }
  return dao.sequelize.transaction(tran => {
    return dao.crumbs.Login.create(user, {transaction: tran})
      .then(createdUser => {
        let claims = [],
          arr = [],
          who = req.user.crumbs_party_role_id;
        let party = {
          created: now,
          createdByPartyRoleID: who,
          lastUpdated: now,
          lastUpdatedByPartyRoleID: who,
          partyTypeID: 1,
          active: true
        };
        arr.push(dao.crumbs.Party.create(party, {transaction: tran}));
        if(req.body.claims){
          claims = req.body.claims.map(claim => {
            claim.loginID = createdUser.iD;
            return claim;
          });
          arr.push(dao.crumbs.ClaimValue.bulkCreate(claims, {transaction: tran}));
        }
        return Promise.all(arr)
          .then(result => {
            let partyRoles = [{partyRoleTypeID: 2}];
            if (req.body.partyRoles && req.body.partyRoles.length) {
              partyRoles = partyRoles.concat(req.body.partyRoles);
            }
            partyRoles = partyRoles.map(role => {
              role.partyID = result[0].iD;
              role.justicePartyRoleID = null;
              role.tontoID = null;
              role.created = now;
              role.createdByPartyRoleID = who;
              role.justicePartyRoleID = null;
              return role;
            });
            return dao.crumbs.PartyRole.bulkCreate(partyRoles, {transaction: tran, returning: true})
              .then(pr => {
                return dao.crumbs.ClaimValue.create({
                  claimTypeID: 14,
                  value: pr[0].iD,
                  loginID: createdUser.iD
                }, {transaction: tran})
                  .then(r => {
                    return res.send(201, createdUser.iD);
                  });
              })
              .catch(next);
          });
      })
      .catch(next);
    }).then()
    .catch(next)
    .done();
}

/** Performs a user search across the database
 @func _list
 @param req {Object} Incoming request object
 @param res {Object} Outgoing response object
 @param next {Function} Next middleware function in the chain
 */
function _list(req, res, next) {

	let siteTypeId = req.query.stid;

	if (!utils.ci(siteTypeId)) {
		return next(new errors.BadRequestError('stid'));
	}

	let limit = utils.si(req.query.ps) || 20;
	let offset = limit * (req.query.p - 1) || 0;
	let where = {
		siteTypeID: req.query.stid
	};
	if (req.query.un) {
		where.username = {$iLike: `%${req.query.un}%`};
	}
	return dao.crumbs.Login.findAll({
		where,
		limit,
		offset,
	})
		.then(users => {

			if (!users || (users && !users.length)) {
				return next(new errors.NotFoundError());
			}

			users.forEach(user => {
				delete user.password;
			});
			res.send(users);
			return next();
		})
		.catch(next)
		.done();
}

/** Updates a login
 * @func _updateUser
 * @param req {Object} Incoming request object
 * @param res {Object} Outgoing response object
 * @param next {Function} Next middleware function
 *
 */
function _updateUser(req, res, next) {
    return dao.sequelize.transaction(tran => {
        return dao.crumbs.Login.findOne({
            where: {ID: req.params.id},
            include: {
                model: dao.crumbs.ClaimValue,
                as: 'ClaimValueLoginFks'
            },
        }).then(user => {
            let arr = [],
                when = new Date(),
                who = req.user.crumbs_party_role_id;
            if (user && user.iD) {
                let partyRoleId = user.ClaimValueLoginFks.find(claimValue=>{
                    if(claimValue.claimTypeID == 14){
                        arr.push(dao.crumbs.PartyRole.findOne({where: {ID: claimValue.value}}));
                        return claimValue.value;
                    }
                });
                arr.push(dao.crumbs.Login.update(req.body, {transaction: tran, where: {ID: req.params.id}}));
                if (req.body.claims) {
                    let claims = req.body.claims.map(claim => {
                        claim.loginID = req.params.id;
                        return claim;
                    });
                    claims.forEach(claim => {
                        arr.push(dao.crumbs.ClaimValue.upsert(claim, {
                            where: {
                                loginID: claim.loginID,
                                claimTypeID: claim.claimTypeID
                            }, transaction: tran
                        }))
                    });
                }
                return Promise.all(arr)
                    .then(result => {
                        if (partyRoleId && result[0] && req.body.partyRoles && req.body.partyRoles.length) {
                            let partyRoles = req.body.partyRoles.map(role => {
                                role.partyID = result[0].partyID;
                                role.justicePartyRoleID = null;
                                role.tontoID = null;
                                role.created = when;
                                role.createdByPartyRoleID = who;
                                role.justicePartyRoleID = null;
                                return role;
                            });
                            return dao.crumbs.PartyRole.bulkCreate(partyRoles, {transaction: tran, returning: true})
                                .then(pr => {
                                    return res.send(200, true);
                                })
                                .catch(next);
                        }
                        return res.send(200, true);
                    })
                    .catch(next);
            } else {
                return next(new errors.NotFoundError());
            }
        });
    }).then()
        .catch(next)
        .done();

}

module.exports = {
    createLogin:                _createLogin,
    resetPassword:              _resetPassword,
    deleteLogin:                _deleteLogin,
    retrieveDetailedLogin:      _retrieveDetailedLogin,
    demoteLoginStatus:          _demoteLoginStatus,
    retrieveLoginStatistics:    _retrieveLoginStatistics,
    retrieveSummary:            _retrieveSummary,
    updateUser:                 _updateUser,
    list:                       _list
};
