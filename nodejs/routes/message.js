"use strict";

/** Message api routes
  * @module routes/message */
const dao     = require("../dao"),
      _ 	  = require("lodash"),
      common  = require("fm-common"),
      mw      = common.user.mw,
      errors = require('restify-errors');

const CRUMBS_LOGIN_ID = "Crumbs Login ID";

/** Sends a message
  * @func _send
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next function in the middleware chain
  *
  * MessageService.Send
  * MessageService.SendWithIdentifiers */
function _send(req, res, next) {
  let filters = {
    messageTypeId: req.body.message.messageTypeId || req.body.message.messageTypeID,
    messagePriority: req.body.message.messagePriorityTypeId || req.body.message.messagePriorityTypeID,
    subject: req.body.message.subject,
    body: req.body.message.body,
    contentType: req.body.message.contentType,
    sentFromSiteTypeId: req.body.message.sentFromSiteTypeId || req.body.message.sentFromSiteTypeID,
    parentId: req.body.message.parentId || req.body.message.parentID || null,
    loginId: req.userAdapter.getLoginId(),
    name: CRUMBS_LOGIN_ID,
  };
  return dao.sequelize.transaction(tran => {
    return dao.crumbs.Message.findOne({
      where: {iD: filters.parentId},
      include: {
        model: dao.crumbs.MessageIdentifier,
        as: 'IdentifierMessageFks',
        include: {
          model: dao.crumbs.IdentifierType,
          as: 'IdentifierType',
          where: {name: filters.name}
        }
      }
    })
      .then(resValue => {
        if (filters.parentId && !resValue) {
          return next(new errors.BadRequestError('parentId'));
        }
        if (resValue && (!/^[48]$/.test(filters.sentFromSiteTypeId) && +resValue.IdentifierMessageFks[0].value != filters.loginId)) {
          return next(new errors.BadRequestError(`Parent message with ${filters.parentId} does not belong to user with login ${filters.loginId}: the user is not allowed to reply to this message.`));
        }
        else {
          return dao.crumbs.Message.create({
            messageTypeID: filters.messageTypeId,
            messagePriorityTypeID: filters.messagePriority,
            subject: filters.subject,
            body: filters.body,
            sent: new Date(),
            sentFromSiteTypeID: filters.sentFromSiteTypeId,
            parentID: filters.parentId,
            contentType: filters.contentType
          }, {transaction: tran})
            .then(newMessageId => {
              if (req.body.identifiers.length > 0) {
                let identifiers = req.body.identifiers.map(identifier => {
                  return {
                    messageID: newMessageId.iD,
                    identifierTypeID: identifier.identifierTypeId,
                    IdentifierTypeID: identifier.identifierTypeId,
                    value: identifier.value
                  }
                });
                return dao.crumbs.MessageIdentifier.bulkCreate(identifiers, {
                  transaction: tran,
                })
                  .then(r => {
                    res.send(201, newMessageId.iD);
                    return next();
                  })
                  .catch(next);
              }
            })
            .catch(next);
        }
      })
  })
    .then()
    .catch(next);
}

/** Reads a message
  * @func _read
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next function in the middleware chain
  *
  * MessageService.ReadMessage */
function _read(req, res, next) {
  return dao.crumbs.Message.findOne({
    where: { iD: req.params.id },
    include: [{
      model: dao.crumbs.MessageIdentifier,
      as: 'IdentifierMessageFks',
      include: [{
        model: dao.crumbs.IdentifierType,
        as: 'IdentifierType',
        where: { name: CRUMBS_LOGIN_ID },
      }],
    }],
  })
    .then(message => {
      if (!message) {
        return next(new errors.NotFoundError());
      }
      if (message.IdentifierMessageFks[0].value != req.userAdapter.getLoginId()) {
        return next(new errors.NotAuthorizedError());
      }
      if (!message.read) {
        message.updateAttributes({
          read: new Date(),
          readFromSiteTypeID: req.body.readFromSiteTypeId,
        });
      }
      message.save()
        .then(() => {
          return dao.crumbs.Message.findOne({
            where: { iD: req.params.id },
            include: [{
              model: dao.crumbs.MessageType,
              as: 'MessageType',
            }, {
              model: dao.crumbs.MessagePriorityType,
              as: 'MessagePriorityType'
            }, {
              model: dao.crumbs.MessageIdentifier,
              as: 'IdentifierMessageFks'
            }]
          })
            .then(message => {
              message = message.dataValues;
              let result = {};
              result.type = message.MessageType;
              message.MessageType = undefined;
              result.priority = message.MessagePriorityType;
              message.MessagePriorityType = undefined;
              result.identifiers = message.IdentifierMessageFks;
              message.IdentifierMessageFks = undefined;
              result.message = message;
              res.send(result);
              return next();
            })
            .catch(next);
        })
        .catch(next);
    })
    .catch();
}

/** Deletes a message
  * @func _delete
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next function in the middleware chain
  *
  * MessageService.Delete */
function _delete(req, res, next) {
    return dao.sequelize.transaction(tran => {
        return dao.crumbs.Message.findOne({
            where : {iD: req.params.id},
            transaction: tran
        })
        .then(msg => {
            if (!msg){
                return next(new errors.NotFoundError());
            }
            msg.update({deleted: new Date()})
            .then(md => {
                res.send(204);
                return next();
            })
            .catch(next)
        })
        .catch(next)
    })
    .catch(next)
    .done();
}

/** List messages
  * @func _list
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next function in the middleware chain
  *
  * MessageService.ListMessagesForIdentifier
  * MessageService.ListDetailedMessagesForIdentifier
  * MessageService.ListDetailedMessagesForLoginAndIdentifier
  * MessageService.ListMessagesForLogin
  * MessageService.ListDetailedMessagesForLogin */
function _list(req, res, next) {
  let identifierTypeID = req.query.itid,
    identifierValue = req.query.iv,
    includeRead = req.query.r == 'true' || false,
    includeDeleted = req.query.d == 'true' || false,
    loginID = req.query.lid || req.userAdapter.getLoginId(),
    messageCondition = {};
  if (!includeRead) {
    messageCondition = {read: null}
  }
  if (!includeDeleted) {
    _.merge(messageCondition, {deleted: null})
  }
  return dao.crumbs.ClaimValue.findAll({
    where: {loginID: loginID, claimTypeID: {$in: [1, 3, 4]}},
    include: {
      model: dao.crumbs.ClaimType, as: 'ClaimType',
      include: {
        model: dao.crumbs.IdentifierType, as: 'FKs'
      }
    }
  })
    .then(result => {
      let arr = [],
        messages = [];
      result.forEach(cv => {
        let messageIdentifierCondition = {identifierTypeID: cv.ClaimType.FKs[0].iD, value: cv.value};
        if (identifierTypeID && identifierValue) {
          messageIdentifierCondition = {
            $or: [{identifierTypeID: identifierTypeID,value: identifierValue}, {identifierTypeID: cv.ClaimType.FKs[0].iD, value: cv.value}]
          }
        }
        arr.push(dao.crumbs.Message.findAll({
          where: messageCondition,
          include: [{
            model: dao.crumbs.MessageIdentifier,
            as: 'IdentifierMessageFks',
            where: messageIdentifierCondition
          }, {
            model: dao.crumbs.MessagePriorityType,
            as: 'MessagePriorityType'
          }, {
            model: dao.crumbs.MessageType,
            as: 'MessageType'
          }],
        }).then(msg => {
          messages = _.uniqBy(messages.concat(msg), 'iD');
        }))
      });
      Promise.all(arr)
        .then(result => {
          messages = messages.map(msg => {
            let message = {};
            message.message = {
              iD: msg.iD,
              messageTypeID: msg.messageTypeID,
              messagePriorityTypeID: msg.messagePriorityTypeID,
              subject: msg.subject,
              body: msg.body,
              sent: msg.sent,
              sentFromSiteTypeID: msg.sentFromSiteTypeID,
              read: msg.read,
              readFromSiteTypeID: msg.readFromSiteTypeID,
              parentID: msg.parentID,
              deleted: msg.deleted,
              contentType: msg.contentType,
              retailMessageID: msg.retailMessageID
            };
            message.priority = msg.MessagePriorityType;
            message.type = msg.MessageType;
            message.identifiers = msg.IdentifierMessageFks;
            return message;
          });
          res.send(messages);
          return next();
        })
        .catch(next);
    })
    .catch(next)
    .done();
}

/** Retrieves a message
  * @func _retrieve
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next function in the middleware chain
  *
  * MessageService.RetrieveDetailedMessage */
function _retrieve(req, res, next) {
  if (req.query.lid) {
    mw.assertPerm("admin:message");
  }
  return dao.crumbs.Message.findOne({
    where: {iD: req.params.id},
    include: [{
      model: dao.crumbs.MessageType,
      as: 'MessageType',
    }, {
      model: dao.crumbs.MessagePriorityType,
      as: 'MessagePriorityType'
    }, {
      model: dao.crumbs.MessageIdentifier,
      as: 'IdentifierMessageFks',
    }]
  })
    .then(message => {
      if (!message) {
        return next(new errors.NotFoundError());
      }
      let ownership = message.IdentifierMessageFks.filter(elem => elem.identifierTypeID == 3)[0] || {};
      if (ownership.value != (req.query.lid ||  req.userAdapter.getLoginId())) {
        return next(new errors.NotAuthorizedError());
      }
      message = message.dataValues;
      let result = {};
      result.type = message.MessageType;
      message.MessageType = undefined;
      result.priority = message.MessagePriorityType;
      message.MessagePriorityType = undefined;
      result.identifiers = message.IdentifierMessageFks;
      message.IdentifierMessageFks = undefined;
      result.message = message;
      res.send(result);
      return next();
    })
    .catch(next);
}

/** Updates the attributes of a message
  * @func _update
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next function in the middleware chain
  *
  * MessageService.ApplyIdentifier */
function _update(req, res, next) {
  let promises = [];
  return dao.crumbs.Message.findOne({
    where: { iD: req.context.id }
  })
    .then(msg => {
      if (!msg) {
        return next(new errors.NotFoundError());
      }
      for (let identifier of req.body) {
        promises.push(dao.crumbs.MessageIdentifier.create({
          messageID: req.context.id,
          identifierTypeID: identifier.identifierTypeID,
          value: identifier.value
        }));
      }
      return Promise.all(promises)
        .then(() => {
          res.end();
          return next();
        })
        .catch(next);
    })
    .catch(next);
}

module.exports = {
  send:         _send,
  read:         _read,
  delete:       _delete,
  list:         _list,
  retrieve:     _retrieve,
  update:       _update
};
