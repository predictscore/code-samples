"use strict";

/** Link routes
  * @module routes/link */
const dao = require("../dao"),
      errors = require('restify-errors');
const crypto = require('crypto');
const uuid = require('uuid/v4');

/** Creates a new disposable link
  * @func _create
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function
  *
  * LinkService.Create */
function _create(req, res, next) {
  let stepOne = crypto.createHash('md5').update(uuid()).digest('base64');
  let stepTwo = crypto.createHash('sha256').update(stepOne).digest('base64');
  let stepThree = crypto.createHash('sha256').update(stepTwo + Math.random()).digest('hex');
  let slug = stepThree.replace(/\+/g, 'z').replace(/\//g, 'y').replace(/=/g, 'x');
  return dao.sequelize.transaction(tran => {
    return dao.crumbs.Link.create({
      linkLocationID: req.body.linkLocationId,
      slug: slug,
      created: new Date(),
      accessCount: 0,
      lifespan: req.body.lifespanMillis,
    })
      .then(link => {
        if (req.body.parameters) {
          if (req.body.parameters.length > 0) {
            let paramPromises = [];
            for (let parameter of req.body.parameters) {
              paramPromises.push(dao.crumbs.LinkParameter.create({
                linkID: link.iD,
                name: parameter.key,
                value: parameter.value,
                transaction: tran,
              }));
            }
            Promise.all(paramPromises)
              .then(() => {
                return getLinkInfo(link.iD, res, next);
              });
          } else {
            return getLinkInfo(link.iD, res, next);
          }
        } else {
          return getLinkInfo(link.iD, res, next);
        }
      })
      .catch(next);
  })
    .catch(next);
}

function getLinkInfo(linkId, res, next) {
  return dao.crumbs.Link.findOne({
    where: { iD: linkId },
    include: [{
      model: dao.crumbs.LinkLocation,
      as: 'LinkLocation',
    }, {
      model: dao.crumbs.LinkParameter,
      as: 'ParameterLinkFks',
    }]
  })
    .then(link => {
      link = link.dataValues;
      let result = {};
      result.linkLocation = link.LinkLocation;
      link.LinkLocation = undefined;
      result.parameters = link.ParameterLinkFks;
      link.ParameterLinkFks = undefined;
      link.LinkLocationID = undefined;
      result.link = link;
      res.send(result);
      return next();
    })
}

/** Retrives the details of a link
  * @func _retrieve
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function
  *
  * LinkService.Retrieve */
function _retrieve(req, res, next) {
	return dao.crumbs.Link.findOne({
		where: {
			slug: req.params.id,
		},
		include: [{
			model: dao.crumbs.LinkLocation,
			as: 'LinkLocation',
		}, {
			model: dao.crumbs.LinkParameter,
			as: 'ParameterLinkFks'
		}]
	})
		.then(link => {
			if (!link) {
				return next(new errors.NotFoundError());
			}
			link = link.dataValues;
			let result = {
				linkLocation: link.LinkLocation,
				parameters: link.ParameterLinkFks,
			};
			link.LinkLocation = undefined;
			link.ParameterLinkFks = undefined;
			result.link = link;
			res.send(result);
			return next();
		})
		.catch(next);
}

/** Registers access of a link
  * @func _registerAccess
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function
  *
  * LinkService.RegisterAccess */
function _registerAccess(req, res, next) {
  return dao.crumbs.Link.findOne({
    where: { slug: req.params.id },
    include: [{
      model: dao.crumbs.LinkLocation,
      as: 'LinkLocation',
    }],
  })
    .then(link => {
      if (!link) {
        return next(new errors.NotFoundError());
      }
      let mustExpire = false;
      let accessed = true;
      let expireOnAccess = link.LinkLocation.expireOnAccess;
      let lifespan = link.lifespan || link.LinkLocation.lifespan;
      let creationDate = link.created;
      let expired = link.expired;
      let result = false;
      let attributes = {};
      if (expired) {
        return next(new errors.ConflictError("The contents of this link has already expired"));
      } else {
        if (expireOnAccess) {
          mustExpire = true;
        }
        if (lifespan) {
          if (creationDate.getTime() + lifespan < Date.now()) {
            mustExpire = true;
            accessed = false;
          }
        }
        if (accessed) {
          attributes.accessCount = ++link.accessCount;
          result = true;
        }
        if (mustExpire && !expired) {
          attributes.expired = new Date();
        }
      }
      link.updateAttributes(attributes);
      return link.save()
        .then(() => {
          res.send(204, result);
          return next();
        })
        .catch(next);
    })
    .catch(next);
}

module.exports = {
	create: 		_create,
	retrieve: 		_retrieve,
	registerAccess: _registerAccess
};
