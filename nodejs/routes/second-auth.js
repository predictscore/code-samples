"use strict";

/** Secondary authentication routes
    @module routes/second-auth */
const dao     = require("../dao"),
      errors  = require('restify-errors'),
      crypto  = require('crypto'),
      uuidV4 = require('uuid/v4');

/** Creates a new secondary authority request protecting a dictionary of parameters
  * @func _create
  * @param req {Object} Request object
  * @param res {Object} Response object
  * @param func {Function} Next function in the middleware chain
  *
  * SecondaryAuthentication.Create */
function _create(req, res, next) {
  let token = crypto.createHash('sha256').update(uuidV4()).digest('base64').replace(/\+/g, 'z').replace(/\//g, 'y').replace(/\=/g, 'x'),
    parameters = req.body.parameters,
    code = req.body.code,
    lifespan = req.body.lifespanMillis;
  if (!code || typeof code !== 'string' || !Number.isInteger(lifespan)) {
    return next(new errors.BadRequestError());
  }
  return dao.sequelize.transaction(tran => {
    return dao.crumbs.SecondaryAuthority.create({
      code: code,
      token: token,
      created: new Date(),
      expired: null,
      attempts: 0,
      lifespan: lifespan
    }, {transaction: tran})
      .then(sa => {
        if (!parameters) {
          res.send(200, true);
          return next();
        }
        let params = [];
        for (let i in parameters) {
          params.push({secondaryAuthorityID: sa.iD, name: i, value: parameters[i]})
        }
        dao.crumbs.SecondaryAuthorityParameter.removeAttribute('id');
        return dao.crumbs.SecondaryAuthorityParameter.bulkCreate(params, {transaction: tran})
          .then(sap => {
            res.send(200, true);
            return next();
          })
          .catch(next);
      })
      .catch(next);
  })
    .then()
    .catch(next);
}

/** Authenticates a secondary authority request
  * @func _authenticate
  * @param req {Object} Request object
  * @param res {Object} Response object
  * @param func {Function} Next function in the middleware chain
  *
  * SecondaryAuthentication.Authenticate */
function _authenticate(req, res, next) {
  return dao.crumbs.SecondaryAuthority.findOne({
    where: {
      code: req.body.code,
      token: req.body.token,
      expired: null,
    },
    attributes: ['created', 'expired', 'attempts', 'lifespan'],
  })
    .then(sa => {
      if (!sa) {
        res.send(false);
        return next();
      }
      let authenticated = false;
      let options = {
        attempts: sa.attempts + 1,
      };
      if (sa.created.getTime() + sa.lifespan > Date.now() || !sa.lifespan) {
        options.expired = Date.now();
      }
      sa.updateAttributes(options);
      return sa.save()
        .then(result => {
          if (result && options.expired) {
            authenticated = true;
          }
          res.send(authenticated);
          return next();
        })
        .catch(next);
    })
    .catch(next);
}

/** Retrieves a secondary authentication request
  * @func _authenticate
  * @param req {Object} Request object
  * @param res {Object} Response object
  * @param func {Function} Next function in the middleware chain
  *
  * SecondaryAuthentication.Retreive */
function _retrieve(req, res, next) {
  return dao.crumbs.SecondaryAuthority.findById(req.params.id, {
    include: [{
      model: dao.crumbs.SecondaryAuthorityParameter,
      as: 'ParameterSecondaryauthorityFks'
    }],
    attributes: ['attempts', 'code', 'created', 'expired', 'token'],
  })
    .then(sa => {
      if (!sa) {
        return next(new errors.NotFoundError());
      }
      sa = sa.dataValues;
      sa.parameters = {};
      sa.ParameterSecondaryauthorityFks.forEach(parameter => {
        if (parameter.name) {
          sa.parameters[parameter.name] = parameter.value;
        }
      });
      sa.ParameterSecondaryauthorityFks = undefined;
      res.send(sa);
      return next();
    })
    .catch(next);
}

module.exports = {
  create:       _create,
  authenticate: _authenticate,
  retrieve:     _retrieve
};
