"use strict";

/** News feed routes
  * @module routes/news-feed 
  *
  * TODO: uploadFile needs to be implemented by the files service
  * TODO: deleteAttachment needs to come under the _save function
  */
const dao     = require("../dao"),
      errors = require('restify-errors'),
      _ = require('lodash');

/** List all of the news feed items
  * @func _list
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function
  *
  * NewsFeed.List */
function _list(req, res, next) {
  return dao.crumbs.NewsFeed.findAll({
    offset: req.query.s,
    limit: req.query.l,
    where: {active: true},
    include: [{
      model: dao.crumbs.PartyRole,
      as: 'CreatedByPartyRole',
      include: [{
        model: dao.crumbs.Party,
        as: 'Party',
        include: [{
          model: dao.crumbs.Person,
          as: 'PersonPartyFks'
        }]
      }]
    }, {
      model: dao.crumbs.PartyRole,
      as: 'LastUpdatedByPartyRole',
      include: [{
        model: dao.crumbs.Party,
        as: 'Party',
        include: [{
          model: dao.crumbs.Person,
          as: 'PersonPartyFks'
        }]
      }]
    }],
  })
    .then(newsItems => {
      newsItems = newsItems.map((newsItem) => {
        newsItem = newsItem.dataValues;
        let result = {
          createdBy: _.get(newsItem, 'CreatedByPartyRole.Party.PersonPartyFks', null),
          lastUpdatedBy: _.get(newsItem, 'LastUpdatedByPartyRole.Party.PersonPartyFks', null),
        };
        newsItem.CreatedByPartyRole = undefined;
        newsItem.LastUpdatedByPartyRole = undefined;
        result.newsItem = newsItem;
        return result;
      });
      res.send(newsItems);
      return next();
    })
    .catch(next);
}

/** Retreives a news feed item
  * @func _retrieve
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function
  *
  * NewsFeed.Retrieve */
function _retrieve(req, res, next) {
  return dao.crumbs.NewsFeed.findOne({
    where: { ID: req.params.id },
    include: [{
      model: dao.crumbs.PartyRole,
      as: 'CreatedByPartyRole',
      include: [{
        model: dao.crumbs.Party,
        as: 'Party',
        include: [{
          model: dao.crumbs.Person,
          as: 'PersonPartyFks'
        }]
      }]
    }, {
      model: dao.crumbs.PartyRole,
      as: 'LastUpdatedByPartyRole',
      include: [{
        model: dao.crumbs.Party,
        as: 'Party',
        include: [{
          model: dao.crumbs.Person,
          as: 'PersonPartyFks'
        }]
      }]
    }]
  })
    .then(newsItem => {
      newsItem = newsItem.dataValues;
      if (newsItem) {
        let result = {
          createdBy: _.get(newsItem, 'CreatedByPartyRole.Party.PersonPartyFks', null),
          lastUpdatedBy: _.get(newsItem, 'LastUpdatedByPartyRole.Party.PersonPartyFks', null),
        };
        newsItem.CreatedByPartyRole = undefined;
        newsItem.LastUpdatedByPartyRole = undefined;
        result.newsItem = newsItem;
        res.send(result);
      } else {
        return next(new errors.NotFoundError());
      }
      return next();
    })
    .catch(next);
}

/** Saves a news feed item
  * @func _create
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function
  *
  * NewsFeed.Create 
  * NewsFeed.Save */
function _save(req, res, next) {
  let title = req.body.title,
    body = req.body.body,
    partyRoleID = req.userAdapter.getPartyRoleId(),
    id = req.body.ID || req.body.iD || null,
    status = id == null ? 201 : 200;

  if(!id){
    return dao.crumbs.NewsFeed.create({
      title: title,
      body: body,
      createdByPartyRoleID: partyRoleID,
      created: new Date(),
      active: true
    })
      .then(result => {
        res.send(status);
        return next();
      })
      .catch(next)
      .done();
  } else {
    return dao.crumbs.NewsFeed.update({
      title: title,
      body: body,
      createdByPartyRoleID: partyRoleID,
      lastUpdated: new Date(),
    }, {
      where: { iD: id }
    })
      .then(result => {
        if (result[0] === 0) {
          return next(new errors.NotFoundError());
        }
        res.send(status);
        return next();
      })
      .catch(next);
  }
}

/** Deletes a news feed item
  * @func _delete
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function
  *
  * NewsFeed.Delete */
function _delete(req, res, next) {
  return dao.crumbs.NewsFeed.update({
      active: false,
      lastUpdated: new Date(),
      lastUpdatedByPartyRoleID: req.userAdapter.getPartyRoleId(),
    }, {
      where: { iD: req.params.id },
    })
    .then(r=> {
      if (r[0] === 0){
        return next(new errors.NotFoundError());
      }

      res.send(204);
      return next();
    })
    .catch(next)
    .done();
}

module.exports = {
  list:     _list,
  retrieve: _retrieve,
  save:     _save,
  delete:   _delete
};

