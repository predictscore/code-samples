'use strict';

/** Converts or nullifies an integer
 Uses Number not parseInt to ensure it does not try and grab numbers should a guid or other start with numbers
 @func _si
 @param val {Object} Incoming object to cast

 */
function _si(val) {
  let i = Number(val);
  if (isNaN(i)) { return null; }
  return i;
}

/** Checks if an integer is valid
 @func _si
 @param val {Object} Incoming object to cast

 */
function _ci(val) {
  if(!val)
    return false;
  else{
    let i = _si(val);
    if (!i || isNaN(i)) { return false; }
  }
  return true;
}

module.exports = {
  si: _si,
  ci: _ci,
};