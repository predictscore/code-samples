"use strict";

/** State routes
 @module routes/state */
const dao         = require("../dao"),
  restifyErrors   = require("restify-errors"),
  utils           = require('./utils');

/** Performs a state search across the database
 @func _list
 @param req {Object} Incoming request object
 @param res {Object} Outgoing response object
 @param next {Function} Next middleware function in the chain
 */
function _list(req, res, next) {
  let countryId = utils.si(req.query.cid);
  let limit = utils.si(req.query.l) || 20;
  let offset = limit * (req.query.p - 1) || 0;
  let where = { active: true };
  if(countryId){
    where.countryID = countryId;
  }
  return dao.crumbs.State.findAll({
    limit,
    offset,
    where
  })
    .then(states => {

      if(!states || (states && !states.length)){
        return next(new restifyErrors.NotFoundError());
      }

      states = states.map(state => {
        return { Id: state.iD, Name: state.name, countryID: state.countryID }
      });
      res.send(states);
      return next();
    })
    .catch(next)
}

module.exports = {
  list: _list
};