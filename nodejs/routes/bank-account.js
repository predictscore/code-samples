"use strict";

/** Bank account route implementation
	@module routes/bank-account */
const dao = require('../dao');
const _ = require('lodash');
const errors = require('restify-errors');

/** List bank accounts
	@func _list
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	BankAccountService.List
*/
function _list(req, res, next) {
	return dao.crumbs.PartyBankAccount.findAll({
		include: [{
			model: dao.crumbs.BankBSB,
			as: 'BankBSB',
			where: {
				bSB: { $iLike: `%${req.query.st}%` },
				active: true,
			}
		}, {
			model: dao.crumbs.BankAccount,
			as: 'BankAccount',
		}, {
			model: dao.crumbs.PartyBankAccountType,
			as: 'PartyBankAccountType',
		}, {
			model: dao.crumbs.PartyRole,
			as: 'TreasuryApprovedByPartyRole',
			include: [{
				model: dao.crumbs.Party,
				as: 'Party',
				include: [{
					model: dao.crumbs.Person,
					as: 'PersonPartyFks'
				}, {
					model: dao.crumbs.Company,
					as: 'CompanyPartyFks'
				}, {
					model: dao.crumbs.Trust,
					as: 'TrustPartyFks'
				}],
			}],
		}]
	})
		.then(pba => {
			let result = pba.map(account => {
				return {
					accountName: account.BankAccount.accountName,
					accountNumber: account.BankAccount.accountNumber,
					bankAccountID: account.bankAccountID,
					bankBSBId: account.bankBSBId,
					bsb: account.BankBSB.bSB,
					partyBankAccountId: account.iD,
					partyID: account.partyID,
					type: {
						id: account.PartyBankAccountType.iD,
						name: account.PartyBankAccountType.name,
					},
					approved: account.treasuryApproved && account.TreasuryApprovedByPartyRole ? {
						when: account.treasuryApproved,
						who: account.TreasuryApprovedByPartyRole && account.TreasuryApprovedByPartyRole.Party ? {
							id: account.TreasuryApprovedByPartyRole.Party.iD,
							name: dao.utils.partyNameByParty(account.TreasuryApprovedByPartyRole.Party),
						} : null,
					} : null,
				}
			});
			res.send(result);
			return next();
		})
		.catch(next);
}

/** Save a bank account
	@func _save
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	BankAccountService.Save
*/
function _save(req, res, next) {
	return dao.sequelize.transaction(tran => {
		req.tran = tran;
		return validateBSB(req, res, next)
			.then(bankBsb => {
				req.body.bsb = bankBsb.bSB;
				req.body.bankBSBId = bankBsb.iD;
				return createUpdatePartyBankAccount(req, res, next)
					.then(pba => {
						return retrievePartyBankAccountById(req, res, next, pba.id)
							.then(result => {
								res.send(pba.status, result);
								return next();
							})
							.catch(next);
					})
					.catch(next);
			})
			.catch(next);
	})
		.catch(next);
}

function validateBSB(req, res, next) {
	if (!req.body.bsb && !req.body.bankBSBId) {
		return next(new errors.BadRequestError('bsb, bankBSBId'));
	}
	let promise;
	if (!req.body.bsb && req.body.bankBSBId) {
		promise = dao.crumbs.BankBSB.findOne({
			where: {
				iD: req.body.bankBSBId,
			},
			transaction: req.tran,
		});
	} else if (req.body.bsb && !req.body.bankBSBId) {
		promise = dao.crumbs.BankBSB.findOne({
			where: {
				bSB: req.body.bsb,
			},
			transaction: req.tran,
		});
	}
	return promise
		.then(bankBsb => {
			return bankBsb;
		})
		.catch(next);
}

function createUpdateBankAccount(req, res, next) {
	return dao.crumbs.BankAccount.findOne({
		where: {
			accountNumber: req.body.accountNumber,
			accountName: req.body.accountName,
			bankBSBID: req.body.bankBSBId,
		},
		transaction: req.tran,
	})
		.then(ba => {
			let bankAccount = {
				bankBSBID: req.body.bankBSBId,
				accountName: req.body.accountName,
				accountNumber: req.body.accountNumber,
				lastUpdated: new Date(),
				lastUpdatedByPartyRoleID: req.userAdapter.getPartyRoleId(),
				active: true,
			};
			if (ba && req.body.bankAccountId) {
				return dao.crumbs.BankAccount.update(bankAccount, {
					where: {
						iD: ba.iD,
					},
					returning: true,
					transaction: req.tran,
				})
					.then(r => {
						return r[1][0].iD;
					})
					.catch(next);
			} else if (!ba || !req.body.bankAccountId) {
				bankAccount.created = bankAccount.lastUpdated;
				bankAccount.createdByPartyRoleID = bankAccount.lastUpdatedByPartyRoleID;
				return dao.crumbs.BankAccount.create(bankAccount, {
					returning: true,
					transaction: req.tran,
				})
					.then(r => {
						return r.iD;
					})
					.catch(next);
			}
		})
		.catch(next);
}

function createUpdatePartyBankAccount(req, res, next) {
	return createUpdateBankAccount(req, res, next)
		.then(bankAccountID => {
			let partyBankAccount = {
				bankAccountID: bankAccountID,
				partyID: req.body.partyId,
				bankBSBID: req.body.bankBSBId,
				partyBankAccountTypeID: req.body.typeId,
				lastUpdated: new Date(),
				lastUpdatedByPartyRoleID: req.userAdapter.getPartyRoleId(),
			};
			if (!req.body.partyBankAccountId) {
				return dao.crumbs.PartyBankAccount.findOne({
					where: {
						bankBSBID: req.body.bankBSBId,
						partyID: req.body.partyId,
						partyBankAccountTypeID: req.body.typeId,
						bankAccountID: bankAccountID,
					},
					transaction: req.tran,
				})
					.then(pba => {
						if (!pba) {
							partyBankAccount.created = partyBankAccount.lastUpdated;
							partyBankAccount.createdByPartyRoleID = partyBankAccount.lastUpdatedByPartyRoleID;
							return dao.crumbs.PartyBankAccount.create(partyBankAccount, {
								returning: true,
								transaction: req.tran,
							})
								.then(r => {
									return { id: r.iD, status: 201 };
								})
								.catch(next);
						}
						res.end();
						return next();
					})
					.catch(next);
			} else {
				return dao.crumbs.PartyBankAccount.findById(req.body.partyBankAccountId, {
					include: [{
						model: dao.crumbs.BankAccount,
						as: 'BankAccount',
					}],
					transaction: req.tran,
				})
					.then(pba => {
						if (pba) {
							if (pba.BankAccount.bSB != req.body.bsb ||
								pba.BankAccount.accountNumber != req.body.accountNumber ||
								pba.BankAccount.accountName != req.body.accountName ||
								pba.partyBankAccountTypeID != partyBankAccount.partyBankAccountTypeID) {
								partyBankAccount.treasuryApproved = null;
								partyBankAccount.treasuryApprovedByPartyRoleID = null;
							}
							pba.updateAttributes(partyBankAccount);
							return pba.save({ returning: true, transaction: req.tran })
								.then(r => {
									return {id: r.iD, status: 200 };
								})
								.catch(next);
						}
						return next(new errors.BadRequestError());
					})
					.catch(next);
			}
		})
		.catch(next);
}

function retrievePartyBankAccountById(req, res, next, id) {
	return dao.crumbs.PartyBankAccount.findById(id, {
			include: [{
				model: dao.crumbs.BankBSB,
				as: 'BankBSB',
			}, {
				model: dao.crumbs.BankAccount,
				as: 'BankAccount',
			}, {
				model: dao.crumbs.PartyBankAccountType,
				as: 'PartyBankAccountType',
			}, {
				model: dao.crumbs.PartyRole,
				as: 'TreasuryApprovedByPartyRole',
				include: [{
					model: dao.crumbs.Party,
					as: 'Party',
					include: [{
						model: dao.crumbs.Person,
						as: 'PersonPartyFks'
					}, {
						model: dao.crumbs.Company,
						as: 'CompanyPartyFks'
					}, {
						model: dao.crumbs.Trust,
						as: 'TrustPartyFks'
					}],
				}],
			}],
			transaction: req.tran,
		})
		.then(pba => {
			return {
				accountName: pba.BankAccount.accountName,
				accountNumber: pba.BankAccount.accountNumber,
				bankAccountId: pba.bankAccountID,
				bankBSBId: pba.bankBSBID,
				bsb: pba.BankBSB.bSB,
				partyBankAccountId: pba.iD,
				partyId: pba.partyID,
				type: {
					id: pba.PartyBankAccountType.iD,
					name: pba.PartyBankAccountType.name,
				},
				approved: pba.treasuryApproved && pba.TreasuryApprovedByPartyRole ? {
					when: pba.treasuryApproved,
					who: pba.TreasuryApprovedByPartyRole && pba.TreasuryApprovedByPartyRole.Party ? {
						id: pba.TreasuryApprovedByPartyRole.Party.iD,
						name: dao.utils.partyNameByParty(pba.TreasuryApprovedByPartyRole.Party),
					} : null,
				} : null,
			}
		})
		.catch(next);
}

module.exports = {
	list: _list,
	save: _save
};
