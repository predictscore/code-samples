"use strict";

/** History implementation routes
	@module routes/history */

const Q 			= require("q"),
	  _ 			= require("lodash"),
	  config 		= require("config"),
	  restifyErrors = require("restify-errors"),
      ObjectId = require('mongoose').Types.ObjectId;

const dao = require("../dao"),
	  mongo = require('../dao/mongo'),
	  emails = mongo.models.emails;

/** Lists any emails currently listed by the history registry
	@func _listEmails
    @param req {Object} Incoming request object
    @param res {Object} Outgoing response object
    @param next {Function} Next middleware function in the chain 
	
	EmailHistoryService.List
*/	
function _listEmails(req, res, next) {
	let where = {partyId: +req.params.id, $or: [ {"to.address" : req.user.email }, {"from.address" : req.user.email } ]};
	if(req.query.m == 'false'){
		where = {partyId: +req.params.id};
	}
    return emails.find(where)
        .then(function(list){
            res.send(200, list);
        })
        .catch(next)
        .done();
}
/** Retrieves a single email history
	@func _retrieveEmail
    @param req {Object} Incoming request object
    @param res {Object} Outgoing response object
    @param next {Function} Next middleware function in the chain 
	
	EmailHistoryService.Retrieve
*/
function _retrieveEmail(req, res, next) {
    if (!ObjectId.isValid(req.params.id)) {
        return next(new restifyErrors.BadRequestError());
    }
    return emails.findOne({_id: new ObjectId(req.params.id)})
        .then(function(email){
        	if(email){
                res.send(200, email);
			} else {
                return next(new restifyErrors.NotFoundError());
			}
        })
        .catch(next)
        .done();
}

/** Lists the history notes 
	@func _list
    @param req {Object} Incoming request object
    @param res {Object} Outgoing response object
    @param next {Function} Next middleware function in the chain 

    HistoryService.List
*/	
function _list(req, res, next) {

	let id = req.params.id;

	// collect arbitrary query string filters
	let filters = {
		partyID: id
	};

	if (req.query.hnt) {
		let hnt = parseInt(req.query.hnt);
		if (!isNaN(hnt)) { filters["historyNoteTypeID"] = hnt; }
	}

	if (req.query.hnst) {
		let hnt = parseInt(req.query.hnst);
		if (!isNaN(hnt)) { filters["historyNoteStatusTypeID"] = hnst; }
	}

	// collect paging parameters
	let page 		= parseInt(req.query.p || 0),
		pageSize 	= parseInt(req.query.ps || config.get("defaults").pageSize);

	return dao.crumbs.HistoryNote.findAll({
		where: filters,
		offset: page * pageSize,
		limit: pageSize
	})
	.then(hs => {
		res.send(hs);
		return next();
	})
	.catch(next)
	.done();

}

/** Write a history note
	@func _save
    @param req {Object} Incoming request object
    @param res {Object} Outgoing response object
    @param next {Function} Next middleware function in the chain 

	HistoryService.Write
*/	
function _create(req, res, next) {

	// this is the id of the party we'll be creating a record against
	let partyId = req.params.id; 

	// make the required, static adjustments for a create
	delete req.body.iD;

	req.body["partyID"] = partyId;
	req.body["created"] = new Date();
	req.body["createdByPartyRoleID"] = req.userAdapter.getPartyRoleId();

	// build the record to save
	return dao.crumbs.HistoryNote.build(req.body)
		.save()
		.then(note => {
			res.send(201, note.iD);
			return next();
		})
		.catch(next)
		.done();

}

function _update(req, res, next) {

	let id = req.params.id;

	// make the required, static adjustments for a create
	delete req.body.partyId;
	delete req.body.created;
	delete req.body.createdByPartyRoleID;
	delete req.body.iD;

	return dao.crumbs.HistoryNote.find({ 
			where: { iD: id }
		})
		.then(note => {

			// 404 where the note doesn't exist
			if (!note) {
				return next(new restifyErrors.NotFoundError(`History note does not exist`));
			}

			// update the values within the retrieved note
			note.updateAttributes(req.body);

			// save it
			return note.save();

		})
		.then(note => {
			res.send({});
			return next();
		})
		.catch(next)
		.done();

}

module.exports = {
	listEmails: 	_listEmails,
	retrieveEmail: 	_retrieveEmail,
	list: 			_list,
	create: 		_create,
	update: 		_update
};

