'use strict';

/** Party api routes
  * @module routes/party */

const Q 						= require("q"),
	  	_ 						= require("lodash"),
	  	config 				= require("config"),
	  	restifyErrors = require("restify-errors"),
			fs						= require('fs');

const dao 					= require("../dao"),
	  	addressDao		= dao.address,
	  	telephoneDao	= dao.telephone,
	  	emailDao 			= dao.email,
	  	common 				= require("fm-common"),
      partyRoleDao    = dao.partyRole,
	    socialMediaAccountDao = dao.socialMediaAccount,
	  	logger 	 			= common.logger,
			utils      = require('./utils');

/** Retrieve an existing party from the database
	@func _retrieve
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	PartyService.RetrieveParty
	IdentityService.RetrieveIdentityDetails
*/
function _retrieve(req, res, next) {

	let id = req.params.id,
		byId = { where: { iD: id } },
		byPartyId = { where: { partyID: id } };

	let dataPromises = [
		dao.crumbs.Party.find(byId),
		dao.crumbs.Company.find(byPartyId),
		dao.crumbs.Person.find(byPartyId),
		dao.crumbs.Trust.find(byPartyId)
	];

	return Q.spread(dataPromises,
		(party, company, person, trust) => {

			if (!party) {
				return next(new restifyErrors.NotFoundError(`Party ${id} does not exist`));
			}

          	let bodyFields = _.filter([
          	  person,
          	  company,
          	  trust,
          	], x => x);

          	if (bodyFields.length > 1) {
          	  	return next(new restifyErrors.InternalServerError('Sorry, requested Party is invalid'));
          	}

			res.send({
				party: party,
				company: company,
				person: person,
				trust: trust
			});

			return next();
		})
		.catch(next)
		.done();

}

/** Lists all of the addresses for a given party
  * @func _listAddresses
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function */
function _listAddresses(req, res, next) {

	let id = req.params.id;

	return dao.crumbs.PartyAddress.findAll({
		where: { partyID: id },
		include: [
			{ model: dao.crumbs.Address, as: 'Address' }
		]
	})
	.then(partyAddresses => {
		res.send(partyAddresses);
		return next();
	})
	.catch(next)
	.done();

}

/** Retrieves an address for its id
  * @func _retrieveAddress
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function */
function _retrieveAddress(req, res, next) {

	let id = req.params.id;

	return dao.crumbs.PartyAddress.findAll({
		where: { iD: id },
		include: [
			{ model: dao.crumbs.Address, as: 'Address' }
		]
	})
	.then(partyAddress => {

		if (!partyAddress) {
			return next(new restifyErrors.NotFoundError());
		}

		res.send(partyAddress);
		return next();
	})
	.catch(next)
	.done();

}

/** Saves an address object for a party
  * @func _saveAddress
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function */
function _saveAddress(req, res, next) {
	let when = new Date(),
		who = req.userAdapter.getPartyRoleId();

	let address = req.body.address;

	let addressPromise = addressDao.save(address, req.userAdapter),
		promise = null;

	if (req.params.id) {
		req.body.iD = req.params.id;

		delete req.body.created;
		delete req.body.createdByPartyRoleID;
		/* always set mutational attributes during writes */
		req.body.lastUpdated = when;
		req.body.lastUpdatedByPartyRoleID = who;

		promise = addressPromise
			.then(addressId => {

				return dao.crumbs.PartyAddress.find({
					where: { iD: req.params.id }
				}).then(pa => {

					// the record needs to be there
					if (!pa) {
						return next(new restifyErrors.NotFoundError("Party address does not exist"));
					}

					// update the ad-hoc attributes
					pa.updateAttributes(req.body);

					// save it off
					return pa.save()
						.then(() => {
							res.send(200);
							return next();
						});

				});


			});

	} else  {
		/* clear out all of the attributes that we don't want sent in during
		 a create for the party address */
		delete req.body.iD;

		req.body.created = when;
		req.body.createdByPartyRoleID = who;
		/* always set mutational attributes during writes */
		req.body.lastUpdated = when;
		req.body.lastUpdatedByPartyRoleID = who;

		// save the raw address details first
		promise = addressPromise
			.then(addressId => {
				/* after writing the address to the database, we'll use its ID
				 to fill out the remainder of the PartyAddress record */
				req.body.addressID = addressId;

				return dao.crumbs.PartyAddress.build(req.body)
					.save()
					.then(partyAddress => {
						res.send(201, partyAddress.iD);
						return next();
					});

			});

	}

	return promise.catch(next).done();

}

/** Deletes an address object from a party
  * @func _deleteAddress
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function */
function _deleteAddress(req, res, next) {

	let when = new Date(),
		who = req.userAdapter.getPartyRoleId(),
		id = req.params.id;

	// delete the record
	return dao.crumbs.PartyAddress.destroy({
		where: { iD: parseInt(id) }
	})
	.then(n => {

		// no record effected, really is not found
		if (n == 0) {
			return next(new restifyErrors.NotFoundError());
		}

		res.send(200);
		return next();
	})
	.catch(next)
	.done();

}

/** Retrieves all of the telephones for a party
  * @func _listPhones
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function */
function _listPhones(req, res, next) {

	let id = req.params.id;

	return dao.crumbs.PartyTelephone.findAll({
		where: { partyID: id },
		include: [
			{ model: dao.crumbs.TelephoneNumber, as: 'TelephoneNumber' }
		]
	})
	.then(partyTelephones => {
		res.send(partyTelephones);
		return next();
	})
	.catch(next)
	.done();

}

/** Retrieves a single phone
	* @func _retrievePhone
	* @param req {Object} The incoming request
	* @param res {Object} The outgoing response
	* @param next {Function} Next middle ware function */
function _retrievePhone(req, res, next) {

	let id = req.params.id;

	return dao.crumbs.PartyTelephone.findAll({
		where: { iD: id },
		include: [
			{ model: dao.crumbs.TelephoneNumber, as: 'TelephoneNumber' }
		]
	})
	.then(partyTelephone => {

		if (!partyTelephone) {
			return next(new restifyErrors.NotFoundError());
		}

		res.send(partyTelephone);
		return next();
	})
	.catch(next)
	.done();

}

/** Saves a phone number
	* @func _savePhone
	* @param req {Object} The incoming request
	* @param res {Object} The outgoing response
	* @param next {Function} Next middle ware function */
function _savePhone(req, res, next) {
	let when = new Date(),
		who = req.userAdapter.getPartyRoleId(),
		phone = req.body.TelephoneNumber,
		telephonePromise = telephoneDao.save(phone, req.userAdapter),
		promise = null;

	if (req.params.id) {
		req.body.iD = req.params.id;

		delete req.body.created;
		delete req.body.createdByPartyRoleID;
		/* always set mutational attributes during writes */
		req.body.lastUpdated = when;
		req.body.lastUpdatedByPartyRoleID = who;

		promise = telephonePromise
			.then(telephoneId => {

				return dao.crumbs.PartyTelephone.find({
					where: { iD: req.params.id }
				}).then(pt => {

					// the record needs to be there
					if (!pt) {
						return next(new restifyErrors.NotFoundError("Party telephone does not exist"));
					}
					// update the ad-hoc attributes
					req.body.TelephoneNumberID = telephoneId;
					req.body.telephoneNumberID = telephoneId;
					pt.updateAttributes(req.body);

					// save it off
					return pt.save()
						.then(() => {
							res.send(200);
							return next();
						});

				});


			});

	} else  {
		/* clear out all of the attributes that we don't want sent in during
		 a create for the party address */
		delete req.body.iD;

		req.body.created = when;
		req.body.createdByPartyRoleID = who;
		/* always set mutational attributes during writes */
		req.body.lastUpdated = when;
		req.body.lastUpdatedByPartyRoleID = who;

		// save the raw address details first
		promise = telephonePromise
			.then(telephoneId => {

				/* after writing the address to the database, we'll use its ID
				 to fill out the remainder of the PartyAddress record */
				req.body.telephoneNumberID = telephoneId;

				return dao.crumbs.PartyTelephone.build(req.body)
					.save()
					.then(partyTelephone => {
						res.send(201, partyTelephone.iD);
						return next();
					});

			});

	}

	return promise.catch(next).done();
}

/** Deletes a phone number
	* @func _deletePhone
	* @param req {Object} The incoming request
	* @param res {Object} The outgoing response
	* @param next {Function} Next middle ware function */
function _deletePhone(req, res, next) {

	let when 	= new Date(),
			who 	= req.userAdapter.getPartyRoleId(),
			id 		= req.params.id;

	// delete the record
	return dao.crumbs.PartyTelephone.destroy({
		where: { iD: parseInt(id) }
	})
	.then(n => {

		// no record effected, really is not found
		if (n == 0) {
			return next(new restifyErrors.NotFoundError());
		}

		res.send(200);
		return next();
	})
	.catch(next)
	.done();

}

/** Retrieves all of the email addresses for a party
  * @func _listEmails
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function */
function _listEmails(req, res, next) {

	let id = req.params.id;

	return dao.crumbs.PartyEmail.findAll({
		where: { partyID: id },
		include: [
			{ model: dao.crumbs.EmailAddress, as: 'EmailAddress' }
		]
	})
	.then(partyEmails => {
		res.send(partyEmails);
		return next();
	})
	.catch(next)
	.done();

}

/** Retrieves a single email
	* @func _retrieveEmail
	* @param req {Object} The incoming request
	* @param res {Object} The outgoing response
	* @param next {Function} Next middle ware function */
function _retrieveEmail(req, res, next) {

	let id = req.params.id;

	return dao.crumbs.PartyEmail.findAll({
		where: { iD: id },
		include: [
			{ model: dao.crumbs.EmailAddress, as: 'EmailAddress' }
		]
	})
	.then(partyEmail => {

		if (!partyEmail) {
			return next(new restifyErrors.NotFoundError());
		}

		res.send(partyEmail);
		return next();
	})
	.catch(next)
	.done();


}

/** Saves an email address
	* @func _saveEmail
	* @param req {Object} The incoming request
	* @param res {Object} The outgoing response
	* @param next {Function} Next middle ware function */
function _saveEmail(req, res, next) {

	let when = new Date(),
		who = req.userAdapter.getPartyRoleId(),
		email = req.body.EmailAddress;

	let emailPromise = emailDao.save(email, req.userAdapter),
		promise = null;

	if (req.params.id) {
		req.body.iD = req.params.id;

		delete req.body.created;
		delete req.body.createdByPartyRoleID;
		/* always set mutational attributes during writes */
		req.body.lastUpdated = when;
		req.body.lastUpdatedByPartyRoleID = who;

		promise = emailPromise
			.then(emailId => {

				return dao.crumbs.PartyEmail.find({
					where: { iD: req.params.id }
				}).then(pe => {

					// the record needs to be there
					if (!pe) {
						return next(new restifyErrors.NotFoundError("Party email does not exist"));
					}

					// update the ad-hoc attributes
					req.body.EmailAddressID = emailId;
					req.body.emailAddressID = emailId;
					pe.updateAttributes(req.body);

					// save it off
					return pe.save()
						.then(() => {
							res.send(200);
							return next();
						});

				});


			});

	} else  {
		/* clear out all of the attributes that we don't want sent in during
		 a create for the party address */
		delete req.body.iD;

		req.body.created = when;
		req.body.createdByPartyRoleID = who;
		/* always set mutational attributes during writes */
		req.body.lastUpdated = when;
		req.body.lastUpdatedByPartyRoleID = who;

		// save the raw address details first
		promise = emailPromise
			.then(emailId => {

				/* after writing the address to the database, we'll use its ID
				 to fill out the remainder of the PartyAddress record */
				req.body.emailAddressID = emailId;

				return dao.crumbs.PartyEmail.build(req.body)
					.save()
					.then(partyEmail => {
						res.send(201, partyEmail.iD);
						return next();
					});

			});

	}

	return promise.catch(next).done();

}

/** Deletes a phone number
	* @func _deletePhone
	* @param req {Object} The incoming request
	* @param res {Object} The outgoing response
	* @param next {Function} Next middle ware function */
function _deleteEmail(req, res, next) {

	let when 	= new Date(),
			who 	= req.userAdapter.getPartyRoleId(),
			id 		= req.params.id;

	// delete the record
	return dao.crumbs.PartyEmail.destroy({
		where: { iD: parseInt(id) }
	})
	.then(n => {

		// no record effected, really is not found
		if (n == 0) {
			return next(new restifyErrors.NotFoundError());
		}

		res.send(200);
		return next();
	})
	.catch(next)
	.done();

}

/** Retrieves all of the socials for a party
 * @func _listSocials
 * @param req {Object} Incoming request object
 * @param res {Object} Outgoing response object
 * @param next {Function} Next middleware function */
function _listSocials(req, res, next) {

  let id = req.params.id;

  return dao.crumbs.PartySocialMedia.findAll({
    where: { partyID: id },
    include: [
      { model: dao.crumbs.SocialMediaAccount, as: 'SocialMediaAccount' }
    ]
  })
    .then(partySocials => {
      res.send(partySocials);
      return next();
    })
    .catch(next)
    .done();
}

/** Saves a social media
 * @func _saveSocial
 * @param req {Object} The incoming request
 * @param res {Object} The outgoing response
 * @param next {Function} Next middle ware function */
function _saveSocial(req, res, next) {

	let when = new Date(),
		who = req.userAdapter.getPartyRoleId();

	let partyId = req.body.partyID,
		account = req.body.SocialMediaAccount,
		newRecord = false,
		saveOptions = { };

	let accountPromise = socialMediaAccountDao.save(account, req.userAdapter),
		promise = null;

	if (req.params.id) {
		req.body.iD = req.params.id;

		delete req.body.created;
		delete req.body.createdByPartyRoleID;
		/* always set mutational attributes during writes */
		req.body.lastUpdated = when;
		req.body.lastUpdatedByPartyRoleID = who;

		promise = accountPromise
			.then(accountId => {

				return dao.crumbs.PartySocialMedia.find({
					where: { iD: req.params.id }
				}).then(ps => {

					// the record needs to be there
					if (!ps) {
						return next(new restifyErrors.NotFoundError("Party email does not exist"));
					}

					// update the ad-hoc attributes
					req.body.SocialMediaAccountID = accountId;
					req.body.socialMediaAccountID = accountId;
					ps.updateAttributes(req.body);

					// save it off
					return ps.save()
						.then(() => {
							res.send(200);
							return next();
						});

				});


			});

	} else  {
		/* clear out all of the attributes that we don't want sent in during
		 a create for the party address */
		delete req.body.iD;

		req.body.created = when;
		req.body.createdByPartyRoleID = who;
		/* always set mutational attributes during writes */
		req.body.lastUpdated = when;
		req.body.lastUpdatedByPartyRoleID = who;

		// save the raw address details first
		promise = accountPromise
			.then(accountId => {

				/* after writing the address to the database, we'll use its ID
				 to fill out the remainder of the PartyAddress record */
				req.body.socialMediaAccountID = accountId;

				return dao.crumbs.PartySocialMedia.build(req.body)
					.save()
					.then(PartySocialMedia => {
						res.send(201, PartySocialMedia.iD);
						return next();
					});

			});

	}

	return promise.catch(next).done();

}

/** Retrieves all of the party roles for a party
  * @func _listPartyRoles
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function */
function _listPartyRoles(req, res, next) {
	let id = req.params.id;

	return dao.crumbs.PartyRole.findAll({
    where: {partyID: id},
    include: [
      {model: dao.crumbs.PartyRoleType, as: 'PartyRoleType'}
    ]
  })
  .then(partyRoles => {
    res.send(partyRoles);
    return next();
  })
  .catch(next)
  .done();
}

/** Retrieves a single party role for a party
 * @func _retrievePartyRole
 * @param req {Object} Incoming request object
 * @param res {Object} Outgoing response object
 * @param next {Function} Next middleware function */
function _retrievePartyRole(req, res, next) {
  let id = req.params.id;

  return dao.crumbs.PartyRole.findOne({
    where: { iD: id },
    include: [
      { model: dao.crumbs.PartyRoleType, as: 'PartyRoleType' }
    ]
  })
    .then(partyRole => {
      if (!partyRole) {
        return next(new restifyErrors.NotFoundError());
      }
      res.send(partyRole);
      return next();
    })
    .catch(next)
    .done();
}

/** Saves a party role
 * @func _savePartyRole
 * @param req {Object} Incoming request object
 * @param res {Object} Outgoing response object
 * @param next {Function} Next middleware function */
function _savePartyRole(req, res, next) {
  let when = new Date(),
    who = req.userAdapter.getPartyRoleId();

  let partyRole = req.body;

  let partyRolePromise = () => partyRoleDao.save(partyRole, req.userAdapter),
    promise = null;

  if (req.params.id) {

    req.body.iD = req.params.id;
    delete req.body.created;
    delete req.body.createdByPartyRoleID;

    promise = partyRolePromise()
      .then(prt => {

        return dao.crumbs.PartyRole.find({
          where: {iD: req.params.id}
        }).then(pr => {
          // the record needs to be there
          if (!pr) {
            return next(new restifyErrors.NotFoundError("Party role does not exist"));
          }
          // update the ad-hoc attributes
          pr.updateAttributes(req.body);
          // save it off
          return pr.save()
            .then(() => {
              res.send(200);
              return next();
            });
        });
      });
  } else {
    /* clear out all of the attributes that we don't want sent in during
     a create for the party role */
    delete req.body.iD;
    return dao.crumbs.PartyRole.find({
      where: {
        PartyID: partyRole.PartyID,
        PartyRoleTypeID: partyRole.PartyRoleTypeID
      }
    })
      .then(duplicate => {

        if (duplicate) {
          return next(new restifyErrors.ConflictError('Record already exists'));
        }

        req.body.created = when;
        req.body.createdByPartyRoleID = who;

        // save the party role
        promise = partyRolePromise()
          .then(pr => {
            res.send(201, pr);
            return next();
          });
      }).catch(next);

  }

  return promise.catch(next);
}

/** Creates a new party into the database
	@func _save
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	PartyService.SaveParty
	PartyService.SetUltracsClientId
	IdentityService.SavePartyIdentityDetails
*/
function _create(req, res, next) {

  	let bodyFields = _.filter([
  	  req.body.person,
      req.body.company,
      req.body.trust,
  	], x => x);

  	if (bodyFields.length > 1) {
  	  return next(new restifyErrors.BadRequestError());
  	}

	let when = new Date(),
		who = req.userAdapter.getPartyRoleId();

	// TODO: prepare the party object here for creation
	delete req.body.party.iD;
	req.body.party.created = when;
	req.body.party.createdByPartyRoleID = who;
	req.body.party.lastUpdated = when;
	req.body.party.lastUpdatedByPartyRoleID = who;

	// duplicate check?

	return dao.sequelize.transaction(tran => {

		// first data-task is to save the party object
		return dao.crumbs.Party.create(req.body.party, {transaction: tran})
			.then(p => {

				let out = {
					party: p
				};

				let params = {
					partyID: p.iD,
					lastUpdated: when,
					lastUpdatedByPartyRoleID: who
				};

				let arr = [];

				if (req.body.person) {
					delete req.body.person.partyID;
					arr.push(dao.crumbs.Person.create(_.merge(req.body.person, params), {transaction: tran})
                    .then(x => {out.person = x}))
				}
				if (req.body.company) {
                  delete req.body.company.partyID;
					arr.push(dao.crumbs.Company.create(_.merge(req.body.company, params), {transaction: tran})
						.then(x => {out.company = x}));
				}
				if (req.body.trust) {
                  delete req.body.trust.partyID;
					arr.push(dao.crumbs.Trust.create(_.merge(req.body.trust, params), {transaction: tran})
						.then(x => { out.trust = x}))
				}

				return Promise.all(arr)
					.then(r => { return out; })
					.catch(err => { return err; })
			});

		})
		.then(party => {
			if (!party) {
				res.send(new restifyErrors.InternalServerError(`No party was created`));
			}else{
				res.send(201, party);
				return next();
			}
		})
		.catch(next)
		.done();

}

/** Saves a party into the database
	@func _save
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	PartyService.SaveParty
	PartyService.SetUltracsClientId
	IdentityService.SavePartyIdentityDetails
*/
function _update(req, res, next) {

  	let bodyFields = _.filter([
  	  req.body.person,
  	  req.body.company,
  	  req.body.trust,
  	], x => x);

  	if (bodyFields.length > 1) {
  	  return next(new restifyErrors.BadRequestError());
  	}

	let id 		= req.params.id,
		when 	= new Date(),
		who 	= req.userAdapter.getPartyRoleId();

	// prepare the party object here for update
	req.body.party.iD = id;
	req.body.party.lastUpdated = when;
	req.body.party.lastUpdatedByPartyRoleID = who;

	// duplicate check?

	return dao.sequelize.transaction(tran => {

		return dao.crumbs.Party.find({
			where: { iD: id },
			include: [{
				model: dao.crumbs.Person,
				as: "PersonPartyFks"
			}, {
				model: dao.crumbs.Company,
				as: "CompanyPartyFks"
			}, {
				model: dao.crumbs.Trust,
				as: "TrustPartyFks"
			}]
		})
		.then(p => {
            if (!p) {
                return next(new restifyErrors.NotFoundError(`Party does not exist`));
            }
            let params = {
                lastUpdated: when,
                lastUpdatedByPartyRoleID: who
            };
            // update the data object now
            p.updateAttributes(req.body.party);

            let arr = [];

            // update the detailed object as well
            if(req.body.person){
                req.body.person.partyID = id;
                req.body.person.PartyID = id;
                if (p.PersonPartyFks) {
                    p.PersonPartyFks.updateAttributes(_.merge(req.body.person, params));
                } else if((!p.CompanyPartyFks || (p.CompanyPartyFks && !p.CompanyPartyFks.length)) && (!p.TrustPartyFks || (p.TrustPartyFks && !p.TrustPartyFks.length))){
                    arr.push(dao.crumbs.Person.create(_.merge(req.body.person, params), {transaction: tran})
                        .then(r=>{
                          p.PersonPartyFks = r;
                        }))
                }
            }
            if(req.body.company){
                req.body.company.partyID = id;
                req.body.company.PartyID = id;
                if (p.CompanyPartyFks && p.CompanyPartyFks[0]) {
                    p.CompanyPartyFks[0].updateAttributes(_.merge(req.body.company, params));
                } else if((!p.PersonPartyFks || (p.PersonPartyFks && !p.PersonPartyFks.length)) && (!p.TrustPartyFks || (p.TrustPartyFks && !p.TrustPartyFks.length))){
                    arr.push(dao.crumbs.Company.create(_.merge(req.body.company, params), {transaction: tran})
                        .then(r=>{
                          p.CompanyPartyFks[0] = r;
						}))
                }
            }
            if(req.body.trust){
                req.body.trust.partyID = id;
                req.body.trust.PartyID = id;
                if (p.TrustPartyFks && p.TrustPartyFks[0]) {
                    p.TrustPartyFks[0].updateAttributes(_.merge(req.body.trust, params));
                } else if((!p.PersonPartyFks || (p.PersonPartyFks && !p.PersonPartyFks.length)) && (!p.CompanyPartyFks || (p.CompanyPartyFks && !p.CompanyPartyFks.length))){
                    arr.push(dao.crumbs.Trust.create(_.merge(req.body.trust, params), {transaction: tran})
                        .then(r=>{
                          p.TrustPartyFks[0] = r;
                        }))
                }
            }

            if (arr.length > 0) {
                return Promise.all(arr)
                    .then(r => {
                        return p.update(p, { transaction: tran })
                            .then(r =>{
                                return r;
                            });
                    })
                    .catch(err => {
                        return err;
                    })
            } else {
                return p.update(p, { transaction: tran })
                    .then(r =>{
                        return r;
                    });
            }
        });

    })
    .then(partyOut => {
        res.send(partyOut);
        return next();
    })
    .catch(next)
    .done();

}

/** Performs a party search across the database
	@func _list
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	PartyService.ListParty
	PartyService.ListPartyRelated
	PartyRoleService.ListOwnersForPartyType
	CompanyService.SearchCompanies
	CompanyService.SearchCompaniesByABN
	PersonService.FindMatchedPeople
*/
function _list(req, res, next) {
	const pa = (p) => {
		p = p || '';
		if (Array.isArray(p)) {
			return p;
		} else {
			return p.split(',');
		}
	};
	const si = (s) => {
		let i;
		if (Array.isArray(s)) {
			i = [];
			s.forEach(e => {
				let n = parseInt(e);
				if (!isNaN(n)) { i.push(n); }
			});
		} else {
			i = parseInt(s);
			if (isNaN(i)) { return null; }
		}
		return i;
	};

	// paging parameters allow us to control how much data is being
	// retrieved from the database as well as being sent back to the
	// client on search requests
	let page = req.query.p || 1,
		pageSize = req.query.ps || config.get("defaults").pageSize;

	let filters = {
		partyTypeId:             si(pa(req.query.pt)),
		partyRoleTypeId:         si(pa(req.query.prt)),
		partyRoleCategoryId:     si(pa(req.query.prc)),
		partyRoleGroupId:         si(pa(req.query.prg)),
		partyRoleSuiteId:         si(pa(req.query.prs)),
		name:                     req.query.name || null,
		//ownerId:                 si(pa(req.query.oid)),
		relationshipTypeId:			si(req.query.rt),
		relationshipParticipationType: si(req.query.r),
		relationshipPartyRoleId: si(req.query.rpr),
		loginId:                 si(req.userAdapter.getLoginId()),
		page:                     si(page),
		pageSize:                 si(pageSize)
	};

	return dao.party.list(filters, next)
		.then(ps => {
			res.send(ps);
			return next();
		})
		.catch(next);

}

/** Retrieve a login summary for a party
	@func _retrieveLoginSummary
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	PartyService.RetrieveLoginSummary
*/
function _retrieveLoginSummary(req, res, next) {
  let partyId = req.params.id,
    result = {};
  return dao.crumbs.PartyRole.findAll({
    where: {partyID: partyId}
  })
    .then(pr => {
      if(!pr || !pr.length){
        return next(new restifyErrors.NotFoundError());
      }
      result.roles = pr;
      let partyRolesId = pr.map(pr => {
        return pr.iD.toString();
      });
      return dao.crumbs.Login.findAll({
        include: [{
          model: dao.crumbs.ClaimValue,
          as: 'ClaimValueLoginFks',
          where: {claimTypeID: 14, value: {$in: partyRolesId}}
        }, {
          model: dao.crumbs.SiteType,
          as: 'SiteType'
        }, {
          model: dao.crumbs.ApplicationRole,
          as: 'ApplicationRoleLoginFks',
          include: [{
            model: dao.crumbs.ApplicationType,
            as: 'ApplicationType'
          }, {
            model: dao.crumbs.RoleType,
            as: 'RoleType'
          }]
        }],
      })
        .then(logins => {
          result.applicationRoles = [];
          result.login = logins.map(login => {
            let appRoles = _.groupBy(login.ApplicationRoleLoginFks, 'applicationTypeID');
            for (let i in appRoles) {
              let appRole = {
                applicationId: appRoles[i][0].applicationTypeID,
                loginId: appRoles[i][0].loginID,
                partyRoleId: +login.ClaimValueLoginFks[0].value,
                roleIds: []
              };
              appRole.roleIds = appRoles[i].map(r => {
                return r.RoleType.iD
              });
              result.applicationRoles.push(appRole);
            }
            return {
              iD: login.iD,
              username: login.username,
              active: login.active,
              locked: login.locked,
              expired: login.expired,
              siteTypeID: login.siteTypeID,
              objectSID: login.objectSID,
              decommissioned: login.decommissioned
            }
          });
          res.send(result);
          return next();
        })
        .catch(next);
    })
    .catch(next);
}

function retrieveDetailed(partyId, next){
	if (!partyId) {
		return Promise.resolve(null);
	}
	return dao.crumbs.Party.findById(partyId, {
		include: [{
			model: dao.crumbs.Person,
			as: 'PersonPartyFks',
		}, {
			model: dao.crumbs.Company,
			as: 'CompanyPartyFks',
		}, {
			model: dao.crumbs.Trust,
			as: 'TrustPartyFks',
		}, {
			model: dao.crumbs.PartyEmail,
			as: 'EmailPartyFks',
		}, {
			model: dao.crumbs.PartyTelephone,
			as: 'TelephonePartyFks',
			include: [{
				model: dao.crumbs.TelephoneNumber,
				as: 'TelephoneNumber'
			}]
		}],
	})
		.then(r => {
			let party = r.dataValues;
			party.person = party.PersonPartyFks[0] || null;
			party.company = party.CompanyPartyFks[0] || null;
			party.trust = party.TrustPartyFks[0] || null;
			party.emails = party.EmailPartyFks;
			party.telephones = party.TelephonePartyFks;
			party.PersonPartyFks = undefined;
			party.CompanyPartyFks = undefined;
			party.TrustPartyFks = undefined;
			party.EmailPartyFks = undefined;
			party.TelephonePartyFks = undefined;
			return party;
		})
		.catch(next);
}

function retrieveDetailed(partyId, next){
	if (!partyId) {
		return Promise.resolve(null);
	}
	return dao.crumbs.Party.findById(partyId, {
		include: [{
			model: dao.crumbs.Person,
			as: 'PersonPartyFks',
		}, {
			model: dao.crumbs.Company,
			as: 'CompanyPartyFks',
		}, {
			model: dao.crumbs.Trust,
			as: 'TrustPartyFks',
		}, {
			model: dao.crumbs.PartyEmail,
			as: 'EmailPartyFks',
			include: [{
				model: dao.crumbs.EmailAddress,
				as: 'EmailAddress',
			}]
		}, {
			model: dao.crumbs.PartyTelephone,
			as: 'TelephonePartyFks',
			include: [{
				model: dao.crumbs.TelephoneNumber,
				as: 'TelephoneNumber'
			}]
		}],
	})
		.then(r => {
			let party = r.dataValues;
			party.person = party.PersonPartyFks[0] || null;
			party.company = party.CompanyPartyFks[0] || null;
			party.trust = party.TrustPartyFks[0] || null;
			party.emails = party.EmailPartyFks;
			party.telephones = party.TelephonePartyFks;
			party.PersonPartyFks = undefined;
			party.CompanyPartyFks = undefined;
			party.TrustPartyFks = undefined;
			party.EmailPartyFks = undefined;
			party.TelephonePartyFks = undefined;
			return party;
		})
		.catch(next);
}

function _uploadLogo(req, res, next) {
	if(!utils.ci(req.params.id)){
		return next(new restifyErrors.BadRequestError());
	}

	fs.writeFile('uploads/party/logos/logo-'+req.params.id, req.params.logo, function (err, result) {
		if (err){
			res.send(500, err);
			return next();
		}
		res.send(201);
		return next();
	});
}

/** Retrieves accreditation for a party
 * @func _listAccreditation
 * @param req {Object} Incoming request object
 * @param res {Object} Outgoing response object
 * @param next {Function} Next middleware function */
function _listAccreditation(req, res, next) {
  if (!utils.ci(req.params.id)) {
    return next(new restifyErrors.BadRequestError('id'));
  }
  let id = utils.si(req.params.id);

  return dao.crumbs.PartyAccreditation.findAll({
      where: { partyID: id },
    })
    .then(partyAccreditations => {
      res.send(partyAccreditations);
      return next();
    })
    .catch(next)
    .done();
}

/** Retrieves a single accreditation
 * @func _retrieveAccreditation
 * @param req {Object} The incoming request
 * @param res {Object} The outgoing response
 * @param next {Function} Next middle ware function */
function _retrieveAccreditation(req, res, next) {
  if (!utils.ci(req.params.id)) {
    return next(new restifyErrors.BadRequestError('id'));
  }
  let id = utils.si(req.params.id);

  return dao.crumbs.PartyAccreditation.findOne({
    where: { iD: id },
  })
    .then(partyAccreditation => {
      if (!partyAccreditation) {
        return next(new restifyErrors.NotFoundError());
      }

      res.send(partyAccreditation);
      return next();
    })
    .catch(next)
    .done();
}

/** Saves an accreditation
 * @func _saveAccreditation
 * @param req {Object} The incoming request
 * @param res {Object} The outgoing response
 * @param next {Function} Next middle ware function */
function _saveAccreditation(req, res, next) {
  let when = new Date(),
    who = req.userAdapter.getV8PartyRoleId();   //TODO: use getPartyRoleId when it's available?
  let promise;

  if (req.params.id) {
    if (!utils.ci(req.params.id)) {
      return next(new restifyErrors.BadRequestError('id'));
    }
    let id = utils.si(req.params.id);

    req.body.iD = id;

    delete req.body.created;
    delete req.body.createdByPartyRoleID;
    /* always set mutational attributes during writes */
    req.body.lastUpdated = when;
    req.body.lastUpdatedByPartyRoleID = who;

    promise = dao.crumbs.PartyAccreditation.update(
      req.body,
      {
        where: { iD: id },
      }
    )
      .then(pa => {
        if (pa[0] === 0) {
          return next(new restifyErrors.NotFoundError("Party accreditation does not exist"))
        }

        res.send(200);
        return next();
      });
  } else  {
    /* clear out all of the attributes that we don't want sent in during
     a create for the party address */
    delete req.body.iD;

    req.body.created = when;
    req.body.createdByPartyRoleID = who;
    /* always set mutational attributes during writes */
    req.body.lastUpdated = when;
    req.body.lastUpdatedByPartyRoleID = who;

    // save the raw address details first
    promise = dao.crumbs.PartyAccreditation.create(req.body)
      .then(pa => {
        res.send(201, pa.iD);
        return next();
      });
  }

  return promise.catch(next).done();
}

/** Deletes an accreditation
 * @func _deletePhone
 * @param req {Object} The incoming request
 * @param res {Object} The outgoing response
 * @param next {Function} Next middle ware function */
function _deleteAccreditation(req, res, next) {
  if (!utils.ci(req.params.id)) {
    return next(new restifyErrors.BadRequestError('id'));
  }
  let id = utils.si(req.params.id);

  return dao.crumbs.PartyAccreditation.destroy({
      where: { iD: id }
    })
    .then(n => {

      // no record effected, really is not found
      if (n == 0) {
        return next(new restifyErrors.NotFoundError());
      }

      res.send(200);
      return next();
    })
    .catch(next)
    .done();
}

/** Retrieves compliance for a party
 * @func _listCompliance
 * @param req {Object} Incoming request object
 * @param res {Object} Outgoing response object
 * @param next {Function} Next middleware function */
function _listCompliance(req, res, next) {
  if (!utils.ci(req.params.id)) {
    return next(new restifyErrors.BadRequestError('id'));
  }
  let id = utils.si(req.params.id);

  return dao.crumbs.PartyCompliance.findAll({
      where: { partyID: id },
    })
    .then(partyCompliances => {
      res.send(partyCompliances);
      return next();
    })
    .catch(next)
    .done();
}

/** Retrieves a single compliance
 * @func _retrieveCompliance
 * @param req {Object} The incoming request
 * @param res {Object} The outgoing response
 * @param next {Function} Next middle ware function */
function _retrieveCompliance(req, res, next) {
  if (!utils.ci(req.params.id)) {
    return next(new restifyErrors.BadRequestError('id'));
  }
  let id = utils.si(req.params.id);

  return dao.crumbs.PartyCompliance.findOne({
    where: { iD: id },
  })
    .then(partyCompliance => {
      if (!partyCompliance) {
        return next(new restifyErrors.NotFoundError());
      }

      res.send(partyCompliance);
      return next();
    })
    .catch(next)
    .done();
}

/** Saves an compliance
 * @func _saveCompliance
 * @param req {Object} The incoming request
 * @param res {Object} The outgoing response
 * @param next {Function} Next middle ware function */
function _saveCompliance(req, res, next) {
  let when = new Date(),
    who = req.userAdapter.getV8PartyRoleId();   //TODO: use getPartyRoleId when it's available?
  let promise;

  if (req.params.id) {
    if (!utils.ci(req.params.id)) {
      return next(new restifyErrors.BadRequestError('id'));
    }
    let id = utils.si(req.params.id);

    req.body.iD = id;

    delete req.body.created;
    delete req.body.createdByPartyRoleID;
    /* always set mutational attributes during writes */
    req.body.lastUpdated = when;
    req.body.lastUpdatedByPartyRoleID = who;

    promise = dao.crumbs.PartyCompliance.update(
      req.body,
      {
        where: { iD: id },
      }
    )
      .then(pa => {
        if (pa[0] === 0) {
          return next(new restifyErrors.NotFoundError("Party compliance does not exist"))
        }

        res.send(200);
        return next();
      });
  } else  {
    /* clear out all of the attributes that we don't want sent in during
     a create for the party address */
    delete req.body.iD;

    req.body.created = when;
    req.body.createdByPartyRoleID = who;
    /* always set mutational attributes during writes */
    req.body.lastUpdated = when;
    req.body.lastUpdatedByPartyRoleID = who;

    // save the raw address details first
    promise = dao.crumbs.PartyCompliance.create(req.body)
      .then(pa => {
        res.send(201, pa.iD);
        return next();
      });
  }

  return promise.catch(next).done();
}

/** Deletes an compliance
 * @func _deletePhone
 * @param req {Object} The incoming request
 * @param res {Object} The outgoing response
 * @param next {Function} Next middle ware function */
function _deleteCompliance(req, res, next) {
  if (!utils.ci(req.params.id)) {
    return next(new restifyErrors.BadRequestError('id'));
  }
  let id = utils.si(req.params.id);

  return dao.crumbs.PartyCompliance.destroy({
      where: { iD: id }
    })
    .then(n => {

      // no record effected, really is not found
      if (n == 0) {
        return next(new restifyErrors.NotFoundError());
      }

      res.send(200);
      return next();
    })
    .catch(next)
    .done();
}

/** Retrieves bankAccounts for a party
 * @func _listBankAccounts
 * @param req {Object} Incoming request object
 * @param res {Object} Outgoing response object
 * @param next {Function} Next middleware function */
function _listBankAccounts(req, res, next) {
  if (!utils.ci(req.params.id)) {
    return next(new restifyErrors.BadRequestError('id'));
  }
  let id = utils.si(req.params.id);

  return dao.crumbs.PartyBankAccount.findAll({
      where: { partyID: id },
    })
    .then(partyBankAccounts => {
      res.send(partyBankAccounts);
      return next();
    })
    .catch(next)
    .done();
}

/** Retrieves a single bankAccount
 * @func _retrieveBankAccount
 * @param req {Object} The incoming request
 * @param res {Object} The outgoing response
 * @param next {Function} Next middle ware function */
function _retrieveBankAccount(req, res, next) {
  if (!utils.ci(req.params.id)) {
    return next(new restifyErrors.BadRequestError('id'));
  }
  let id = utils.si(req.params.id);

  return dao.crumbs.PartyBankAccount.findOne({
    where: { iD: id },
  })
    .then(partyBankAccount => {
      if (!partyBankAccount) {
        return next(new restifyErrors.NotFoundError());
      }

      res.send(partyBankAccount);
      return next();
    })
    .catch(next)
    .done();
}

/** Saves an bankAccount
 * @func _saveBankAccount
 * @param req {Object} The incoming request
 * @param res {Object} The outgoing response
 * @param next {Function} Next middle ware function */
function _saveBankAccount(req, res, next) {
  let when = new Date(),
    who = req.userAdapter.getV8PartyRoleId();   //TODO: use getPartyRoleId when it's available?
  let promise;

  if (req.params.id) {
    if (!utils.ci(req.params.id)) {
      return next(new restifyErrors.BadRequestError('id'));
    }
    let id = utils.si(req.params.id);

    req.body.iD = id;

    delete req.body.created;
    delete req.body.createdByPartyRoleID;
    /* always set mutational attributes during writes */
    req.body.lastUpdated = when;
    req.body.lastUpdatedByPartyRoleID = who;

    promise = dao.crumbs.PartyBankAccount.update(
      req.body,
      {
        where: { iD: id },
      }
    )
      .then(pa => {
        if (pa[0] === 0) {
          return next(new restifyErrors.NotFoundError("Party bankAccount does not exist"))
        }

        res.send(200);
        return next();
      });
  } else  {
    /* clear out all of the attributes that we don't want sent in during
     a create for the party address */
    delete req.body.iD;

    req.body.created = when;
    req.body.createdByPartyRoleID = who;
    /* always set mutational attributes during writes */
    req.body.lastUpdated = when;
    req.body.lastUpdatedByPartyRoleID = who;

    // save the raw address details first
    promise = dao.crumbs.PartyBankAccount.create(req.body)
      .then(pa => {
        res.send(201, pa.iD);
        return next();
      });
  }

  return promise.catch(next).done();
}

/** Deletes an bankAccount
 * @func _deletePhone
 * @param req {Object} The incoming request
 * @param res {Object} The outgoing response
 * @param next {Function} Next middle ware function */
function _deleteBankAccount(req, res, next) {
  if (!utils.ci(req.params.id)) {
    return next(new restifyErrors.BadRequestError('id'));
  }
  let id = utils.si(req.params.id);

  return dao.crumbs.PartyBankAccount.destroy({
      where: { iD: id }
    })
    .then(n => {

      // no record effected, really is not found
      if (n == 0) {
        return next(new restifyErrors.NotFoundError());
      }

      res.send(200);
      return next();
    })
    .catch(next)
    .done();
}

/** Adds a party role for a party
 * @func _savePartyRoleForParty
 * @param req {Object} The incoming request
 * @param res {Object} The outgoing response
 * @param next {Function} Next middle ware function */
function _savePartyRoleForParty(req, res, next) {
	if (!utils.ci(req.params.id)) {
		return next(new restifyErrors.BadRequestError('id'));
	}

	let id = utils.si(req.params.id);
	req.body.partyID = id;
	delete req.body.iD;
	delete req.body.Created;
	delete req.body.CreatedByPartyRoleID;
	req.body.created = new Date();
	req.body.createdByPartyRoleID = utils.si(req.userAdapter.getPartyRoleId());
	delete req.body.v8ID;
	req.body.active = true;

	return dao.crumbs.Party.findById(id)
		.then(party => {
			if (!party) {
				return next(new restifyErrors.NotFoundError());
			}

			return dao.crumbs.PartyRole.create(req.body)
				.then(result => {
					res.send(201, result);
					return next();
				})
				.catch(next)
				.done();
		})
		.catch(next)
		.done();
}

module.exports = {
	retrieveDetailed,
	retrieve: 			_retrieve,
	create: 			_create,
	update: 			_update,
	list: 				_list,
	uploadLogo: 	_uploadLogo,

	address: {
		list: 				_listAddresses,
		retrieve: 		_retrieveAddress,
		save: 				_saveAddress,
		delete: 			_deleteAddress
	},

	phone: {
		list: 				_listPhones,
		retrieve: 		_retrievePhone,
		save: 				_savePhone,
		delete: 			_deletePhone
	},

	email: {
		list: 				_listEmails,
		retrieve: 		_retrieveEmail,
		save: 				_saveEmail,
		delete: 			_deleteEmail
	},

  social: {
    list:                 _listSocials,
		save:                 _saveSocial
  },

	login: {
		list:					_retrieveLoginSummary
	},

	roles: {
		list: 				_listPartyRoles,
    retrieve:      _retrievePartyRole,
    save:          _savePartyRole
	},

  accreditation: {
    list: 				_listAccreditation,
    retrieve: 		_retrieveAccreditation,
    save: 				_saveAccreditation,
    delete: 			_deleteAccreditation
  },

  compliance: {
    list: 				_listCompliance,
    retrieve: 		_retrieveCompliance,
    save: 				_saveCompliance,
    delete: 			_deleteCompliance
  },

  bankAccount: {
    list: 				_listBankAccounts,
    retrieve: 		_retrieveBankAccount,
    save: 				_saveBankAccount,
    delete: 			_deleteBankAccount
  },

	partyRole: {
		//list: _listPartyRolesForParty,
		//retrieve: _retrievePartyRoleForParty,
		save: _savePartyRoleForParty,
		//delete: _deletePartyRoleForParty,
	},
};
