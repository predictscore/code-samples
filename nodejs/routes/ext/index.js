"use strict";

/** External data routes module
  * @module routes/ext */

module.exports = {
	orgId: require("./org-id")
};