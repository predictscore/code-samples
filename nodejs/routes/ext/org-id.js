"use strict";

/** OrgID routes
	@module routes/ext/org-id */

/** Identifies a company
    @func _identify

	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	CompanyService.IdntifyOrganisation
*/
function _identify(req, res, next) {
	// TODO: move this into a "biz-talk" style middle-layer
}

/** Identifies a company to be saved
    @func _save

	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	CompanyService.SaveWithOrgId
*/
function _save(req, res, next) {

}

module.exports = {
	identify: _identify,
	save: 	  _save
}
