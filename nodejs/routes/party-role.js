"use strict";

/** Party role route implementations
    @module routes/party-role */
const dao     = require("../dao"),
      errors = require('restify-errors'),
      _ = require('lodash');

/** Retrieves all of the children for a party role
 @func _retrieveChildren
 @param req {Object} Incoming request object
 @param res {Object} Outgoing response object
 @param next {Function} Next middleware function in the chain

 PartyRole.RetrieveChildren
 */
function _retrieveChildren(req, res, next) {
	let searchTerm = {
		parentPartyRoleID: req.params.id,
	};
	return retrieveChildren(searchTerm, req, res, next);
}

/** Retrieves all of the children for a party role with a given RelationshipTypeId
 @func _retrieveChildren
 @param req {Object} Incoming request object
 @param res {Object} Outgoing response object
 @param next {Function} Next middleware function in the chain

 PartyRole.RetrieveChildren
 */
function _retrieveChildrenWithRelationshipTypeId(req, res, next) {
  let searchTerm = {
    parentPartyRoleID: req.params.id,
    relationshipTypeID: req.params.relationshipTypeId,
  };
  return retrieveChildren(searchTerm, req, res, next);
}

function retrieveChildren(searchTerm, req, res, next){
  return dao.crumbs.PartyRole.findOne({
      where: { iD: req.params.id }
    })
    .then(pr => {
      if (!pr) {
        return next(new errors.NotFoundError());
      }
      return dao.crumbs.PartyRoleRelationship.findAll({
          where: searchTerm,
          include: [{
            model: dao.crumbs.RelationshipType,
            as: 'RelationshipType',
          }, {
            model: dao.crumbs.PartyRole,
            as: 'ChildPartyRole',
            include: [{
              model: dao.crumbs.Party,
              as: 'Party',
              include: [{
                model: dao.crumbs.Person,
                as: 'PersonPartyFks',
              },{
                model: dao.crumbs.Company,
                as: 'CompanyPartyFks',
              },{
                model: dao.crumbs.Trust,
                as: 'TrustPartyFks',
              }]
            }, {
              model: dao.crumbs.PartyRoleType,
              as: 'PartyRoleType',
            }],
          }],
        })
        .then(pr => {
          let children = pr.map((child)=> {
            if(child.dataValues){
              child = child.get({plain:true});
            }
            return {
              party: _.omit(child.ChildPartyRole.Party, ['PersonPartyFks', 'CompanyPartyFks', 'TrustPartyFks']),
              partyName: dao.utils.partyNameByParty(child.ChildPartyRole.Party),
              partyRole: _.omit(child.ChildPartyRole, ['Party', 'PartyRoleType']),
              partyRoleType: child.ChildPartyRole.PartyRoleType,
              type: child.RelationshipType
            }
          });
          res.send(children);
          return next();
        })
    })
    .catch(next)
    .done();
}

/** Retrieves all of the parents for a party role
	@func _retrieveParents
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	PartyRole.RetrieveParents
*/
function _retrieveParents(req, res, next) {
  let searchTerm = {
    childPartyRoleID: req.params.id,
  };
  return retrieveParents(searchTerm, req, res, next);
}

/** Retrieves all of the parents for a party role with a given RelationshipTypeId
 @func retrieveParents
 @param req {Object} Incoming request object
 @param res {Object} Outgoing response object
 @param next {Function} Next middleware function in the chain

 PartyRole.RetrieveParents
 */
function _retrieveParentsWithRelationshipTypeId(req, res, next) {
  let searchTerm = {
    childPartyRoleID: req.params.id,
    relationshipTypeID: req.params.relationshipTypeId,
  };
  return retrieveParents(searchTerm, req, res, next);
}

function retrieveParents(searchTerm, req, res, next){
  return dao.crumbs.PartyRole.findOne({
    where: { iD: req.params.id }
  })
    .then(pr => {
      if (!pr) {
        return next(new errors.NotFoundError());
      }
      return dao.crumbs.PartyRoleRelationship.findAll({
        where: searchTerm,
        include: [{
          model: dao.crumbs.RelationshipType,
          as: 'RelationshipType',
        }, {
          model: dao.crumbs.PartyRole,
          as: 'ParentPartyRole',
          include: [{
            model: dao.crumbs.Party,
            as: 'Party',
            include: [{
              model: dao.crumbs.Person,
              as: 'PersonPartyFks',
            },{
              model: dao.crumbs.Company,
              as: 'CompanyPartyFks',
            },{
              model: dao.crumbs.Trust,
              as: 'TrustPartyFks',
            }]
          }, {
            model: dao.crumbs.PartyRoleType,
            as: 'PartyRoleType',
          }],
        }],
      })
        .then(pr => {
          let parents = pr.map((parent)=> {
            if(parent.dataValues){
              parent = parent.get({plain:true});
            }
            return {
              party: _.omit(parent.ParentPartyRole.Party, ['PersonPartyFks', 'CompanyPartyFks', 'TrustPartyFks']),
              partyName: dao.utils.partyNameByParty(parent.ParentPartyRole.Party),
              partyRole: _.omit(parent.ParentPartyRole, ['Party', 'PartyRoleType']),
              partyRoleType: parent.ParentPartyRole.PartyRoleType,
              type: parent.RelationshipType,
            }
          });
          res.send(parents);
          return next();
        })
    })
    .catch(next)
    .done();
}

/** Relates two party roles together
	@func _relate
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	PartyRole.Relate
*/
function _relate(req, res, next) {
	return dao.sequelize.transaction(tran => {
		return Promise.all([
			dao.crumbs.PartyRole.findOne({
        where: {iD: req.body.parentPartyRoleId},
        transaction: tran,
        include: [{ model: dao.crumbs.Party, as: 'Party' }],
      }).then(parent => { return parent; }),
			dao.crumbs.PartyRole.findOne({
        where: {iD: req.body.childPartyRoleId},
        transaction: tran,
        include: [{ model: dao.crumbs.Party, as: 'Party' }],
      }).then(child => { return child; })
		])
		.then((pr) => {
      let parent = pr[0];
      let child = pr[1];
			if (!parent || !child){
        return next(new errors.NotFoundError());
			}

      let promises = [];
      let result = true;
      let wasRelationshipCreated = (instance, created) => {
        if (!created) {
          result = false;
        }
      };

      if (parent.Party.partyTypeID == 2 && child.Party.partyTypeID == 1) {
        promises.push(dao.crumbs.RelationshipType.findById(req.body.relationshipTypeId)
          .then(rt => {
            if (rt.checkForPrimaryContact == true) {
              return dao.crumbs.PartyRoleRelationship.findOrCreate({
                where: {
                  childPartyRoleID: req.body.parentPartyRoleId,
                  relationshipTypeID: 18,
                },
                defaults: {
                  childPartyRoleID: req.body.parentPartyRoleId,
                  parentPartyRoleID: req.body.childPartyRoleId,
                  relationshipTypeID: 18,
                },
                transaction: tran,
              }).catch(next);
            }
          }).catch(next));
      }

      if (req.body.relationshipTypeId != 18) {
        promises.push(dao.crumbs.PartyRoleRelationship.findOrCreate({
          where: {
            childPartyRoleID: req.body.childPartyRoleId,
            parentPartyRoleID: req.body.parentPartyRoleId,
            relationshipTypeID: req.body.relationshipTypeId,
          },
          defaults: {
            childPartyRoleID: req.body.childPartyRoleId,
            parentPartyRoleID: req.body.parentPartyRoleId,
            relationshipTypeID: req.body.relationshipTypeId,
          },
          transaction: tran,
        }).spread(wasRelationshipCreated).catch(next));
      } else {
        let destroyOptions;
        let createOptions;
        let findOrCreateOptions;
        if (parent.Party.partyTypeID == 2 && child.Party.partyTypeID == 1) {
          destroyOptions = {
            childPartyRoleID: req.body.parentPartyRoleId,
            relationshipTypeID: 18,
          };
          createOptions = {
            childPartyRoleID: req.body.parentPartyRoleId,
            parentPartyRoleID: req.body.childPartyRoleId,
            relationshipTypeID: 18,
          };
          findOrCreateOptions = {
            childPartyRoleID: req.body.childPartyRoleId,
            parentPartyRoleID: req.body.parentPartyRoleId,
            relationshipTypeID: 17,
          };
        } else {
          destroyOptions = {
            childPartyRoleID: req.body.childPartyRoleId,
            relationshipTypeID: 18,
          };
          createOptions = {
            childPartyRoleID: req.body.childPartyRoleId,
            parentPartyRoleID: req.body.parentPartyRoleId,
            relationshipTypeID: 18,
          };
          findOrCreateOptions = {
            childPartyRoleID: req.body.parentPartyRoleId,
            parentPartyRoleID: req.body.childPartyRoleId,
            relationshipTypeID: 17,
          };
        }

        promises.push(dao.crumbs.PartyRoleRelationship.destroy({
            where: destroyOptions,
            transaction: tran,
          })
          .then(() => {
            return dao.crumbs.PartyRoleRelationship.create(createOptions, {
                transaction: tran,
            })
              .then(() => {
                return dao.crumbs.PartyRoleRelationship.findOrCreate({
                  where: findOrCreateOptions,
                  defaults: findOrCreateOptions,
                  transaction: tran,
                }).spread(wasRelationshipCreated).catch(next);
              }).catch(next);
          }).catch(next));
      }

      return Promise.all(promises)
        .then(() => {
          res.send(result);
          return next();
        })
        .catch(next);
		})
		.catch(next);
	})
	.then(()=>{})
	.catch(next)
	.done();
}

/** Breaks the relationship between two party roles
	@func _unrelate
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	PartyRole.Unrelate
*/
function _unrelate(req, res, next) {
    return dao.crumbs.PartyRoleRelationship.destroy({
        where: {
            relationshipTypeID: req.body.relationshipTypeId,
            parentPartyRoleID: req.body.parentPartyRoleId,
            childPartyRoleID: req.body.childPartyRoleId,
        }
    })
        .then(result => {
            res.send(true);
            return next();
        })
        .catch(next)
        .done();
}

/** Sets the party role activation status
	@func _setPartyRoleActivationStatus
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	PartyRole.SetPartyRoleActivationStatus
*/
function _setPartyRoleActivationStatus(req, res, next) {
  if (typeof req.body.active !== 'boolean') {
    return next(new errors.BadRequestError('active'));
  }

  return dao.crumbs.PartyRole.update({
    active: req.body.active,
  }, {
    where: { iD: req.params.id },
  })
    .then((r) => {
      if (r[0] === 0) {
        return next(new errors.NotFoundError());
      } else {
        res.end();
      }

      return next();
    })
    .catch(next)
    .done();
}

/** Requests a share on a party role
	@func _requestShare
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	PartyRole.RequestShare
*/
function _requestShare(req, res, next) {
  return dao.crumbs.PartyRole.findOne({
      where: { iD: req.params.id }
    })
    .then(pr => {
      if (!pr) {
        return next(new errors.NotFoundError());
      }

      return dao.crumbs.RelationshipRequest.create({
          relationshipTypeID: 14,
          requestorPartyRoleID: req.userAdapter.getPartyRoleId(),
          requesteePartyRoleID: Number(req.params.id),
          created: new Date(),
          createdByPartyRoleID: req.userAdapter.getPartyRoleId(),
        })
        .then(result => {
          res.send(202);
          return next();
        })
        .catch(next);
    })
    .catch(next)
    .done();
}

/** Retrieves all of the shares requested of this user
	@func _requesteeShareRequests
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	PartyRole.RetrieveSharesRequestedOfMe
*/
function _requesteeShareRequests(req, res, next) {
  req.query.u = req.query.u === 'true' || req.query.u === 'false' ? req.query.u : 'true';
  let searchTerm;
  if (req.query.u === 'true') {
    searchTerm = {
      approved: { $eq: null },
      declined: { $eq: null },
    }
  }
  return dao.crumbs.RelationshipRequest.findAll({
    where: searchTerm,
    include: [{
      model: dao.crumbs.PartyRole,  //createdBy
      as: 'CreatedByPartyRole',
      include: [{
        model: dao.crumbs.Party,
        as: 'Party',
        include: [{
          model: dao.crumbs.Person,
          as: 'PersonPartyFks'
        }]
      }]
    }, {
      model: dao.crumbs.PartyRole,
      as: 'DeclinedByPartyRole',
      include: [{
        model: dao.crumbs.Party,
        as: 'Party',
        include: [{
          model: dao.crumbs.Person,
          as: 'PersonPartyFks'
        }]
      }]
    }, {
      model: dao.crumbs.PartyRole,
      as: 'ApprovedByPartyRole',
      include: [{
        model: dao.crumbs.Party,
        as: 'Party',
        include: [{
          model: dao.crumbs.Person,
          as: 'PersonPartyFks'
        }]
      }]
    }, {
      model: dao.crumbs.PartyRole,
      as: 'RequesteePartyRole',
      include: [{
        model: dao.crumbs.Party,
        as: 'Party',
        include: [{
          model: dao.crumbs.Person,
          as: 'PersonPartyFks'
        }]
      }]
    }, {
      model: dao.crumbs.PartyRole,
      as: 'RequestorPartyRole',
      include: [{
        model: dao.crumbs.Party,
        as: 'Party',
        include: [{
          model: dao.crumbs.Person,
          as: 'PersonPartyFks'
        }]
      }]
    }]
  })
    .then(requests => {
      requests = requests.map(request => {
        return request.dataValues;
      });
      requests = requests.map(request => {
        let result = {};
        result.createdBy = _.get(request, 'CreatedByPartyRole.Party.PersonPartyFks', null);
        request.CreatedByPartyRole = undefined;
        result.declinedBy = _.get(request, 'DeclinedByPartyRole.Party.PersonPartyFks', null);
        request.DeclinedByPartyRole = undefined;
        result.approvedBy = _.get(request, 'ApprovedByPartyRole.Party.PersonPartyFks', null);
        request.ApprovedByPartyRole = undefined;
        result.requestee = _.get(request, 'RequesteePartyRole.Party.PersonPartyFks', null);
        request.RequesteePartyRole = undefined;
        result.requestor = _.get(request, 'RequestorPartyRole.Party.PersonPartyFks', null);
        request.RequestorPartyRole = undefined;
        result.relationshipRequest = request;
        return result;
      });
      res.send(requests);
      return next();
    })
    .catch(next)
    .done();
}

/** Retrieves all of the shares requested by this user
	@func _requestorShareRequests
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	PartyRole.RetrieveMyShareRequests
*/
function _requestorShareRequests(req, res, next) {
  req.query.u = req.query.u === 'true' || req.query.u === 'false' ? req.query.u : 'true';
  let searchTerm;
  if (req.query.u === 'true') {
    searchTerm = {
      approved: {$eq: null},
      declined: {$eq: null},
    }
  }
  return dao.crumbs.RelationshipRequest.findAll({
    where: searchTerm,
    include: [{
      model: dao.crumbs.PartyRole,  //createdBy
      as: 'CreatedByPartyRole',
      include: [{
        model: dao.crumbs.Party,
        as: 'Party',
        include: [{
          model: dao.crumbs.Person,
          as: 'PersonPartyFks'
        }]
      }]
    }, {
      model: dao.crumbs.PartyRole,
      as: 'DeclinedByPartyRole',
      include: [{
        model: dao.crumbs.Party,
        as: 'Party',
        include: [{
          model: dao.crumbs.Person,
          as: 'PersonPartyFks'
        }]
      }]
    }, {
      model: dao.crumbs.PartyRole,
      as: 'ApprovedByPartyRole',
      include: [{
        model: dao.crumbs.Party,
        as: 'Party',
        include: [{
          model: dao.crumbs.Person,
          as: 'PersonPartyFks'
        }]
      }]
    }, {
      model: dao.crumbs.PartyRole,
      as: 'RequesteePartyRole',
      include: [{
        model: dao.crumbs.Party,
        as: 'Party',
        include: [{
          model: dao.crumbs.Person,
          as: 'PersonPartyFks'
        }]
      }]
    }, {
      model: dao.crumbs.PartyRole,
      as: 'RequestorPartyRole',
      include: [{
        model: dao.crumbs.Party,
        as: 'Party',
        include: [{
          model: dao.crumbs.Person,
          as: 'PersonPartyFks'
        }]
      }]
    }]
  })
    .then(requests => {
      requests = requests.map(request => {
        let result = {};
        result.createdBy = _.get(request, 'CreatedByPartyRole.Party.PersonPartyFks[0]', null);
        request.CreatedByPartyRole = undefined;
        result.declinedBy = _.get(request, 'DeclinedByPartyRole.Party.PersonPartyFks[0]', null);
        request.DeclinedByPartyRole = undefined;
        result.approvedBy = _.get(request, 'ApprovedByPartyRole.Party.PersonPartyFks[0]', null);
        request.ApprovedByPartyRole = undefined;
        result.requestee = _.get(request, 'RequesteePartyRole.Party.PersonPartyFks[0]', null);
        request.RequesteePartyRole = undefined;
        result.requestor = _.get(request, 'RequestorPartyRole.Party.PersonPartyFks[0]', null);
        request.RequestorPartyRole = undefined;
        result.relationshipRequest = request;
        return result;
      });
      res.send(requests);
      return next();
    })
    .catch(next)
    .done();
}

/** Responds to outstanding share requests
	@func _respond
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	PartyRole.RetrieveMyShareRequests
*/
function _respondToShareRequest(req, res, next) {
  let promise;
	if (req.body.decision != 1 && req.body.decision != 2){
    return next(new errors.BadRequestError('decision'));
	}
	return dao.sequelize.transaction(tran => {
		return dao.crumbs.RelationshipRequest.findOne({
			where: {
				iD: req.params.id
			},
			transaction: tran
		})
		.then(rr => {
			if (!rr){
        return next(new errors.NotFoundError());
			}
			if (req.body.decision == 1){
        promise = dao.crumbs.RelationshipRequest.findOne({
          where: { iD: req.params.id },
          transaction: tran,
        })
          .then(rr => {
            return dao.crumbs.PartyRoleRelationship.create({
              parentPartyRoleID: rr.requestorPartyRoleID,
              childPartyRoleID: rr.requesteePartyRoleID,
              relationshipTypeID: rr.relationshipTypeID,
            }, {
              transaction: tran,
            })
              .then(r => {
                return dao.crumbs.RelationshipRequest.update({
                  approved: new Date(),
                  approvedByPartyRoleID: req.userAdapter.getPartyRoleId(),
                }, {
                  where: { iD: req.params.id },
                  transaction: tran,
                });
              });
          });
			} else if (req.body.decision == 2){
				promise = dao.crumbs.RelationshipRequest.update({
          declined: new Date(),
          declinedByPartyRoleID: req.userAdapter.getPartyRoleId(),
        }, {
          where: { iD: req.params.id },
          transaction: tran,
        });
			}
			return promise
				.then(() => {
					res.end();
					return next();
				})
				.catch(next)
		})
		.catch(next);
	})
	.then(() => {})
	.catch(next)
	.done();
}

/** Changes the owner on a party role
	@func _changeOwner
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	PartyRole.ChangeOwner
*/
function _changeOwner(req, res, next) {
	return dao.sequelize.transaction(tran => {
		return dao.crumbs.PartyRole.findById(req.params.id, { transaction: tran })
			.then(r => {
				if (!r) {
					return next(new errors.NotFoundError());
				}
				return dao.crumbs.PartyRoleRelationship.destroy({
						where: {
							relationshipTypeID: 13,
							childPartyRoleID: req.params.id,
						},
						transaction: tran,
					})
					.then(() => {
						return dao.crumbs.PartyRoleRelationship.create({
								relationshipTypeID: 13,
								parentPartyRoleID: req.body.partyRoleId,
								childPartyRoleID: req.params.id,
							}, {
								transaction: tran,
							})
							.then(() => {
								res.end();
								return next();
							})
							.catch(next);
					})
					.catch(next);
			})
			.catch(next);
	})
		.catch(next);
}

/** Deletes a party role
	@func _delete
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	PartyRole.Delete
*/
function _delete(req, res, next) {
  return dao.crumbs.PartyRole.findOne({
    where: {iD: req.params.id}
  })
    .then(pr => {
      if (!pr) {
        return next(new errors.NotFoundError());
      }
      pr.updateAttributes({active: false});
      return pr.save()
        .then(r => {
          res.send(204);
          return next();
        })
        .catch(next);
    })
    .catch(next);
}

module.exports = {
	retrieveChildrenWithRelationshipTypeId: 	  _retrieveChildrenWithRelationshipTypeId,
	retrieveChildren: 				_retrieveChildren,
  retrieveParentsWithRelationshipTypeId:		  _retrieveParentsWithRelationshipTypeId,
	retrieveParents: 				_retrieveParents,
	relate: 						_relate,
	unrelate: 						_unrelate,
	setPartyRoleActivationStatus: 	_setPartyRoleActivationStatus,
	requestShare: 					_requestShare,
	requesteeShareRequests: 		_requesteeShareRequests,
	requestorShareRequests: 		_requestorShareRequests,
	respondToShareRequest: 			_respondToShareRequest,
	changeOwner: 					_changeOwner,
	delete: 						_delete
};
