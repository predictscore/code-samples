"use strict";

/** Program routes for product data
  * @module routes/broker */

const errors      = require("restify-errors");

const dao         = require("../dao"),
      common      = require("fm-common"),
      logger      = common.logger,
      utils      = require('./utils');

/** Retrieves the active program record for a party role
  * @func retrieveByPartyRoleId
  * @param partyRoleId {Integer} The party role id to retrieve for
  * @returns {Object} The program record */
function _retrieveCurrentProgram(req, res, next) {

  let partyRoleId = req.params.id;

  return dao.crumbs.BrokerProgramAgreement.findOne({
    where: { partyRoleID: partyRoleId },
    order: '"Effective" DESC'
  }).then(function (bpa) {

    // no broker agreement could be found!
    if (!bpa) {
      return next(new errors.NotFoundError());
    }

    res.send(bpa);
    return next();

  })
  .catch(next)
  .done();

}

/** Retrieves list of an active program records for a party role
 * @func _retrievePrograms
 * @param id {Integer} The party role id to retrieve for
 * @returns {Object} The program record */
function _retrievePrograms(req, res, next) {

  let partyRoleId = req.params.id;

  if (!utils.ci(partyRoleId)) {
    return next(new errors.BadRequestError('id'));
  }

  return dao.crumbs.BrokerProgramAgreement.findAll({
    where: { partyRoleID: partyRoleId },
    order: '"Effective" DESC'
  }).then(function (bpa) {

    if (!bpa || (bpa && !bpa.length)) {
      return next(new errors.NotFoundError());
    }

    res.send(bpa);
    return next();

  })
    .catch(next);

}

/** Creates a new program record
 * @func _createProgram
 * @returns {Object} The program record */
function _createProgram(req, res, next) {

  let partyRoleId = req.params.id;

  if (!utils.ci(partyRoleId)) {
    return next(new errors.BadRequestError('id'));
  }

  delete req.body.iD;
  req.body.partyRoleID = partyRoleId;
  req.body.PartyRoleID = partyRoleId;
  req.body.created = new Date();
  req.body.createdByPartyRoleID = req.userAdapter.getPartyRoleId();

  return dao.crumbs.BrokerProgramAgreement.create(req.body)
    .then(function (bpa) {

      if (!bpa) {
        return next(new errors.InternalServerError());
      }

      res.send(201, bpa);
      return next();

    })
    .catch(next);

}

module.exports = {
  retrieveCurrentProgram: _retrieveCurrentProgram,
  retrievePrograms:       _retrievePrograms,
  createProgram:          _createProgram
};