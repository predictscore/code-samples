"use strict";

/** User route implementation
    @module routes/user */

const Q       = require('q'),
      _       = require('lodash');
const errors = require('restify-errors');
const dao = require('../dao');
const utils = require('./utils')

      const common  = require("fm-common"),
      logger  = common.logger,
      crypto  = require('crypto'),
      util = require ('../dao/utils');

const ClaimTypes = {
    Email: 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress'
};

/** Validates a user's credential
  * @func _validateCredentials
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next middleware function in the chain */
function _validateCredentials(req, res, next) {
  let lockTolerance = 3;
  let success = false;
  return dao.sequelize.transaction(tran => {
    return dao.crumbs.Login.findOne({
      where: {
        username: {$iLike: req.body.username},
        siteTypeID: req.body.siteTypeId,
      },
      attributes: ['iD'],
      transaction: tran,
    })
      .then(attemptedLogin => {
        if (!attemptedLogin) {
          res.send(success);
          logger.info(`username ${req.body.username} not found`);
          return next();
        }
        return dao.crumbs.Login.findOne({
            where: {
              iD: attemptedLogin.iD,
              password: crypto.createHash('sha256').update(req.body.password).digest('base64'),
              siteTypeID: req.body.siteTypeId,
              active: true,
              locked: false,
            },
            transaction: tran,
          })
          .then(login => {
            if (login) {
              success = true;
            }
            return dao.crumbs.LoginAttempt.create({
                loginID: attemptedLogin.iD,
                when: new Date(),
                successful: success,
                transaction: tran,
              })
              .then(() => {
                if (!success) {
                  return dao.crumbs.LoginAttempt.findAll({
                      where: {
                        loginID: attemptedLogin.iD,
                      },
                      order: [['when', 'DESC']],
                      limit: lockTolerance,
                      transaction: tran,
                    })
                    .then(lastLoginAttempts => {
                      lastLoginAttempts = lastLoginAttempts.filter(la => la.successful == false);
                      if (!login && lastLoginAttempts.length >= lockTolerance) {
                        attemptedLogin.updateAttributes({
                          locked: true,
                          transaction: tran,
                        });
                        attemptedLogin.save()
                          .then(() => {
                            res.send(success);
                            return next();
                          })
                          .catch(next);
                      } else {
                        res.send(success);
                        return next();
                      }
                    })
                    .catch(next);
                }
                res.send(success);
                return next();
              })
              .catch(next);
          })
          .catch(next);
      })
      .catch(next);
  })
    .catch();
}

/** Retrieves a user's claims
  * @func _retrieveClaims
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param next {Function} Next middleware function in the chain */
function _retrieveClaims(req, res, next) {
  let filters = {
    username: req.query.un,
    siteTypeId: req.query.stid
  };
  return dao.crumbs.Login.find({
    where: {username: filters.username, siteTypeID: filters.siteTypeId},
    include: {
      model: dao.crumbs.ClaimValue,
      as: "ClaimValueLoginFks",
      include: {model: dao.crumbs.ClaimType, as: "ClaimType"}
    }
  })
    .then(uc => {
      if (!uc || !uc.ClaimValueLoginFks) {
        return next(new errors.NotFoundError());
      }
      let claims = uc.ClaimValueLoginFks.map(claim => {
        return {typeName: claim.ClaimType.name, value: claim.value}
      });
      if (!claims.find(elem => {
          return elem.typeName == ClaimTypes.Email
        })) {
        let partyRoleId;
        uc.ClaimValueLoginFks.find(claim => {
          if (claim.claimTypeID == 14) {
            partyRoleId = claim.value;
          }
        });
        if (partyRoleId) {
          dao.crumbs.Party.findOne({
            include: [
              {
                model: dao.crumbs.PartyRole, as: 'RolePartyFks',
                where: {iD: partyRoleId}
              },
              {
                model: dao.crumbs.PartyEmail, as: 'EmailPartyFks',
                include: {model: dao.crumbs.EmailAddress, as: 'EmailAddress'}
              }
            ]
          })
            .then(prt => {
              if (prt) {
                if (prt.EmailPartyFks.length) {
                  let emails = _.orderBy(prt.EmailPartyFks, ['preferredEmail', 'lastUpdated'], ['desc', 'desc']);
                  claims.push({typeName: ClaimTypes.Email, value: emails[0].EmailAddress.emailAddress})
                }
              }
              res.send(claims);
              return next();
            })
            .catch(next)
            .done();
        }
      } else {
        res.send(claims);
        return next();
      }
    })
    .catch(next)
    .done();
}

/** Retrieves access information
  * @func _retrieveAccessInformation
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next middleware function in the chain */
function _retrieveAccessInformation(req, res, next) {
  let username = req.query.un,
    cvid = req.query.cvid,
    siteTypeId = req.query.stid,
    promise;
  if (username) {
    promise = dao.crumbs.Login.findOne({
      where: {username: {$iLike: username}, siteTypeID: siteTypeId, active: true},
      attributes: ['decommissioned', 'expired', 'locked']
    })
  } else if (cvid) {
    promise = dao.crumbs.Login.findOne({
      where: {siteTypeID: siteTypeId, active: true},
      attributes: ['decommissioned', 'expired', 'locked'],
      include: {
        model: dao.crumbs.ClaimValue,
        as: "ClaimValueLoginFks",
        where: {value: cvid}
      }
    })
  } else {
    return next(new errors.BadRequestError());
  }
  return promise
    .then(user => {
      if(!user){
        return next(new errors.NotFoundError());
      }
      if(user && user.dataValues && user.dataValues.ClaimValueLoginFks){
        delete user.dataValues.ClaimValueLoginFks;
      }
      res.send(user);
      return next();
    })
    .catch(next);
}

/** Retrieve roles for username
  * @func _retrieveRolesForUsername
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next middleware function in the chain */
function _retrieveRolesForUsername(req, res, next) {
  let username = req.query.un,
    siteTypeId = req.query.stid,
    applicationName = req.query.an;
  if (!username || !siteTypeId || !applicationName) {
    return next(new errors.BadRequestError());
  }
  return dao.crumbs.RoleType.findAll({
    include: [
      {
        model: dao.crumbs.Login, as: 'ApplicationRoleLogins',
        where: {username: {$iLike: username}, siteTypeID: siteTypeId, active: true}
      },
      {
        model: dao.crumbs.ApplicationRole, as: 'ApplicationRoleRoletypeFks'
      },
      {
        model: dao.crumbs.ApplicationType, as: 'ApplicationRoleApplicationTypes',
        where: {name: {$iLike: applicationName}}
      }
    ]
  })
    .then(data => {
      if (data && data.length) {
        res.send(data.map((role) => {
          return role.name;
        }));
        return next();
      }
      return next(new errors.NotFoundError());
    })
    .catch(next)
    .done();
}

/** Resets a user's password
  * @func _resetPassword
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next middleware function in the chain */
function _resetPassword(req, res, next) {

}

/** Creates a login
  * @func _createLogin
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next middleware function in the chain */
function _createLogin(req, res, next) {

}

/** Retrieves a user's permission set
  * @func _retrievePermissionSet
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next middleware function in the chain */
function _retrievePermissionSet(req, res, next) {
  let loginId = req.params.id;
  return dao.crumbs.ApplicationRole.findAll({
    where: {loginID: loginId},
    include: [
      {
        model: dao.crumbs.RoleType,
        as: 'RoleType',
        include: {
          model: dao.crumbs.PermissionType,
          as: 'RoleTypePermissionTypePermissionTypes'
        },
      },
      {
        model: dao.crumbs.ApplicationType,
        as: 'ApplicationType'
      }
    ]
  })
    .then(result => {
      if (result.length) {
        let perms = result.map(ar => {
          return {
            applicationType: ar.ApplicationType,
            permissionTypes: ar.RoleType.RoleTypePermissionTypePermissionTypes
          }
        });
        res.send(perms);
      } else {
        return next(new errors.NotFoundError());
      }
      return next();
    }).catch(next)
    .done();
}

/** Retrieves all of the parties at login
  * @func _retrievePartiesForLogin
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next middleware function in the chain */
function _retrievePartiesForLogin(req, res, next) {
    let filters = {
        loginId: req.params.id
    };
    let parties = {
        identity: {},
        authorised: []
    };
    return dao.sequelize.query(
        'SELECT * FROM "OriginatorAndBroker_RetrieveByLoginID"(:loginId);',
        {
            replacements: filters,
            type: dao.sequelize.QueryTypes.SELECT
        }
    )
        .then(OriginatorAndBroker => {
          if(!OriginatorAndBroker || !OriginatorAndBroker[0].ContactPartyRoleID){
            return next(new errors.NotFoundError());
          }
            let identity = OriginatorAndBroker[0];
            parties = {
                identity: {
                    contact:{
                        id: identity.ContactPartyRoleID,
                        name: identity.ContactName
                    },
                    broker: {
                        id: identity.BrokerPartyRoleID,
                        name: identity.BrokerName
                    },
                    originator: {
                        id: identity.OriginatorPartyRoleID,
                        name: identity.OriginatorName
                    }
                },
                authorised: []
            };
            dao.sequelize.query(
                'SELECT * FROM "OriginatorAndBrokerParties_RetrieveForEmployerByLoginID"(:loginId);',
                {
                    replacements: filters,
                    type: dao.sequelize.QueryTypes.SELECT
                }
            )
              .then(OriginatorAndBrokerParties => {
                if (OriginatorAndBrokerParties.length) {
                  _.forOwn(_.groupBy(OriginatorAndBrokerParties, 'OriginatorPartyRoleID'), or => {
                    let brokers = [];
                    _.forOwn(_.groupBy(or, 'BrokerPartyRoleID'), br => {
                      brokers.push({
                        brokerId: br[0].BrokerPartyRoleID,
                        brokerName: br[0].BrokerName,
                        contacts: br.map(cn => {
                          return {
                            contactId: cn.ContactPartyRoleID,
                            contactName: cn.ContactName
                          }
                        })
                      })
                    });
                    parties.authorised.push({
                      originatorId: or[0].OriginatorPartyRoleID,
                      originatorName: or[0].OriginatorName,
                      brokers: brokers
                    })
                  });
                }
                res.send(parties);
                return next();
              })
            .catch(next)
            .done();
        })
        .catch(next)
        .done();
}

/** Login exists for a given username check
  * @func _loginExistsForUsername
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next middleware function in the chain */
function _retrieveLoginForUsername(req, res, next) {

}

/** Retrieve all of my details
  * @func _me
  * @param req {Object} Incoming request object
  * @param res {Object} Outgoing response object
  * @param func {Function} Next middleware function in the chain */
function _me(req, res, next) {

  let loginId = req.userAdapter.getLoginId();

  return dao.crumbs.Login.findOne({
    where: {ID: loginId},
    include: [{
      model: dao.crumbs.ClaimValue, as: 'ClaimValueLoginFks'
    }, {
      model: dao.crumbs.RoleType, as: 'ApplicationRoleRoleTypes'
    }, {
      model: dao.crumbs.PermissionType, as: 'LoginPermissionTypePermissionTypes'
    }]
  }).then(r => {
    if (!r || !r.iD) {
      return next(new errors.NotFoundError());
    }
    let claims = r.ClaimValueLoginFks.map(c=>{
      return {
        claimTypeId: c.claimTypeID,
        value: c.value
      }
    });
    let perms = r.LoginPermissionTypePermissionTypes.map(p=>{
      return p.name;
    });
    let roles = r.ApplicationRoleRoleTypes.map(r=>{
      return r.name;
    });
    let result = {
      user: {
        displayName: null,
        id: r.iD,
        crumbs_login_id: req.user.crumbs_login_id,
        crumbs_party_role_id: req.user.crumbs_party_role_id,
        claims: claims
      },
      roles: roles,
      perms: perms
    };
    let partyRoleId;
    r.ClaimValueLoginFks.forEach(cv => {
      if (cv.claimTypeID == 14) {
        partyRoleId = cv.value;
      }
    });
    if (partyRoleId) {
      return dao.crumbs.PartyRole.findOne({
        where: {iD: partyRoleId},
        include: {
          model: dao.crumbs.Party,
          as: 'Party',
          include: [{
            model: dao.crumbs.PartyRole,
            as: 'RolePartyFks'
          }, {
            model: dao.crumbs.Person,
            as: 'PersonPartyFks'
          }, {
            model: dao.crumbs.Company,
            as: 'CompanyPartyFks'
          }, {
            model: dao.crumbs.Trust,
            as: 'TrustPartyFks'
          }]
        }
      })
        .then(pr => {
          if (pr.Party) {
            result.user.displayName = dao.utils.partyNameByParty(pr.Party);
          }
          res.send(result);
          return next();
        })
    }
    res.send(result);
    return next();
  }).catch(next);
}

function _createUpdateClaims(req, res, next) {
  let errorFields = _.filter([
                              !req.body.username && 'username',
                              !utils.ci(req.body.siteTypeID) && 'siteTypeID',
                              !utils.ci(req.body.claimTypeID) && 'claimTypeID',
                             ], x => x);

  if (errorFields.length > 0) {
    return next(new errors.BadRequestError(errorFields.join()));
  }

  return dao.crumbs.Login.findOne({
    where: {
      username: req.body.username,
      siteTypeID: req.body.siteTypeID,
    },
  })
    .then(login => {
      if (!login) {
        return next(new errors.NotFoundError());
      }
      return dao.crumbs.ClaimValue.upsert({
        loginID: login.iD,
        claimTypeID: req.body.claimTypeID,
        value: req.body.value,
      })
        .then(result => {
          res.send(result ? 201 : 200);
          return next();
        })
        .catch(next)
        .done();
    })
    .catch(next)
    .done();
}

function _saveApplicationRoles(req, res, next) {

  let errorFields = [];

  if (req.body && req.body.length) {
    _.forEach(req.body, ar => {
      errorFields = _.concat(errorFields, _.filter([
        !utils.ci(ar.applicationTypeID) && 'applicationTypeID',
        !utils.ci(ar.loginID) && 'loginID',
        !utils.ci(ar.roleTypeID) && 'roleTypeID',
      ], x => x));
    })
  }
  if (errorFields.length > 0) {
    return next(new errors.BadRequestError(errorFields.join()));
  }

  return dao.crumbs.ApplicationRole.bulkCreate(req.body)
    .then(result => {
      if (!result || (result && !result.length)) {
        return next(new errors.InternalServerError());
      }

      res.send(201, result);
    })
    .catch(next)
    .done();
}

module.exports = {
  validateCredentials:        _validateCredentials,
  retrieveClaims:             _retrieveClaims,
  retrieveAccessInformation:  _retrieveAccessInformation,
  retrieveRolesForUsername:   _retrieveRolesForUsername,
  retrievePermissionSet:      _retrievePermissionSet,
  retrievePartiesForLogin:    _retrievePartiesForLogin,
  me:                         _me,
  createUpdateClaims:         _createUpdateClaims,
  saveApplicationRoles:       _saveApplicationRoles,
};
