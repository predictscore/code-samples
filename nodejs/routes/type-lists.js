'use strict';

const dao = require('../dao'),
  errors = require('restify-errors');


const TABLES = {
  'ct': dao.crumbs.CommunicationType,
  'tt': dao.crumbs.TelephoneType,
  'lt': dao.crumbs.LocationType,
  'et': dao.crumbs.EmailType,
  'smt': dao.crumbs.SocialMediaType,
  'at': dao.crumbs.AddressType,
  'st': dao.crumbs.SalutationType,
  'prt': dao.crumbs.PartyRoleType,
  'rel': dao.crumbs.RelationshipType,
  'apt': dao.crumbs.ApplicationType,
  'prs': dao.crumbs.PartyRoleSuite,
  'prg': dao.crumbs.PartyRoleGroup,
  'prc': dao.crumbs.PartyRoleCategory,
  'sit': dao.crumbs.SiteType,
  'rt': dao.crumbs.RoleType,
  'perm': dao.crumbs.PermissionType,
  'pt': dao.crumbs.PartyType,
  'ast': dao.crumbs.AccreditationStatusType,
  'cdt': dao.crumbs.CampaignDeliveryType,
  'prtatrt': dao.crumbs.PartyRoleTypeApplicationTypeRoleType,
  'rtpt': dao.crumbs.RoleTypePermissionType,
  'prtrt': dao.crumbs.PartyRoleTypeRoleType,
  'ptprt': dao.crumbs.PartyTypePartyRoleType,
  'hnt': dao.crumbs.HistoryNoteType,
};

function getList(req, res, next) {

  let table = TABLES[req.params.type];

  if (!table) {
    return next(new errors.BadRequestError(`Type '${req.params.type}' not found`));
  }

  return table.findAll()
    .then(tl => {

      if (!tl || (tl && !tl.length)) {
        return next(new errors.NotFoundError());
      }

      res.send(tl);
      return next();
    })
    .catch(next)
    .done();
}

module.exports = {
  getList
};
