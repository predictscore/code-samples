"use strict"

/** Brand store route implementation
	@module routes/brand-store */
const dao = require("../dao"),
      errors = require('restify-errors');

/** Upload an image to a brand
	@func _uploadImage
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain 

	BrandStoreService.UploadImage
*/	
function _uploadImage(req, res, next) {

}

/** Save a brand in the store
	@func _save
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain 

	BrandStoreService.Save
*/
function _save(req, res, next) {
  let brand = req.body.brand;
  const preDocumentBrandTexts = getDocumentBrandTexts(brand, req.body, 3);
  const postDocumentBrandTexts = getDocumentBrandTexts(brand, req.body, 4);
  const onlineBrandTexts = getOnlineBrandTexts(brand, req.body);
  const brandTexts = preDocumentBrandTexts.concat(postDocumentBrandTexts, onlineBrandTexts);
  return dao.sequelize.transaction(tran => {
    return dao.crumbs.Brand.findOne({
      where: {partyRoleID: brand.partyRoleID},
      transaction: tran,
    })
      .then(br => {
        let promise;
        let status = 201;
        let options = {
          sPVName: brand.sPVName,
          sPVACN: brand.sPVACN,
          ultracsBranchID: brand.ultracsBranchID,
          vedaSubscriberID: brand.vedaSubscriberID,
          vedaOperatorID: brand.vedaOperatorID,
          documentBrandLevelTypeID: brand.documentBrandLevelTypeID,
          onlineBrandLevelTypeID: brand.onlineBrandLevelTypeID,
          active: brand.active || true,
          lastUpdated: new Date(),
          lastUpdatedByPartyRoleID: req.userAdapter.getPartyRoleId(),
        };
        if (!br) {
          options.partyRoleID = brand.partyRoleID;
          options.created = new Date();
          options.createdByPartyRoleID = req.userAdapter.getPartyRoleId();
          promise = dao.crumbs.Brand.create(options, {
            transaction: tran,
          });
        } else {
          status = 200;
          promise = dao.crumbs.Brand.update(options, {
            where: {partyRoleID: req.body.brand.partyRoleID},
            transaction: tran,
          });
        }
        return promise
          .then(() => {
            if (brandTexts.length > 0) {
              return saveBrandText(tran, brand, brandTexts, status, res, next);
            }
            res.send(status);
            return next();
          })
          .catch(next);
      })
      .catch(next);
  })
    .catch(next);
}

function saveBrandText(tran, brand, brandTexts, status, res, next) {
  const createUpdateBrandTexts = brandTexts.map(brandText => {
    return dao.crumbs.BrandText.findOne({
      where: {
        partyRoleID: brand.partyRoleID,
        brandTypeID: brandText.brandTypeID,
        currencyTypeID: brandText.currencyTypeID || null,
        documentLocationTypeID: brandText.documentLocationTypeID,
      },
      transaction: tran,
    })
      .then(bt => {
        let promise;
        if (!bt) {
          promise = dao.crumbs.BrandText.create({
            partyRoleID: brand.partyRoleID,
            brandTypeID: brandText.brandTypeID,
            currencyTypeID: brandText.currencyTypeID,
            documentLocationTypeID: brandText.documentLocationTypeID,
            alignmentTypeID: brandText.alignmentTypeID,
            text: brandText.text,
          },{
            transaction: tran,
          });
        } else {
          promise = dao.crumbs.BrandText.update({
            alignmentTypeID: brandText.alignmentTypeID,
            text: brandText.text,
          }, {
            where: {
              partyRoleID: brand.partyRoleID,
              brandTypeID: brandText.brandTypeID,
              currencyTypeID: brandText.currencyTypeID || null,
              documentLocationTypeID: brandText.documentLocationTypeID,
            },
            transaction: tran,
          });
        }
        return promise;
      })
      .catch(next);
  });
  return Promise.all(createUpdateBrandTexts)
    .then(() => {
      res.send(status);
      return next();
    })
    .catch(next);
}

function getDocumentBrandTexts(brand, body, currencyTypeId) {
  const settlement = currencyTypeId == 3 ? body.documentsBranding.preSettlement : body.documentsBranding.postSettlement;
  let list = [];

  if (settlement) {
    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 1, currencyTypeID: currencyTypeId, documentLocationTypeID: 4, alignmentTypeID: body.documentsBranding.headerAlignmentTypeId, text: body.documentsBranding.headerLogoId });
    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 1, currencyTypeID: currencyTypeId, documentLocationTypeID: 5, text: settlement.signatureImageId });
    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 1, currencyTypeID: currencyTypeId, documentLocationTypeID: 6, text: settlement.signatureLineText1 });
    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 1, currencyTypeID: currencyTypeId, documentLocationTypeID: 7, text: settlement.signatureLineText2 });

    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 1, currencyTypeID: currencyTypeId, documentLocationTypeID: 8, alignmentTypeID: settlement.footerAlignmentTypeId});
    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 1, currencyTypeID: currencyTypeId, documentLocationTypeID: 9, text: settlement.footerLine1 });
    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 1, currencyTypeID: currencyTypeId, documentLocationTypeID: 10, text: settlement.footerLine2 });
    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 1, currencyTypeID: currencyTypeId, documentLocationTypeID: 11, text: settlement.footerLine3 });
    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 1, currencyTypeID: currencyTypeId, documentLocationTypeID: 12, text: settlement.footerLine4 });
    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 1, currencyTypeID: currencyTypeId, documentLocationTypeID: 13, text: settlement.footerLine5 });

    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 1, currencyTypeID: currencyTypeId, documentLocationTypeID: 15, text: settlement.footerLine1Column2 });
    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 1, currencyTypeID: currencyTypeId, documentLocationTypeID: 16, text: settlement.footerLine2Column2 });
    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 1, currencyTypeID: currencyTypeId, documentLocationTypeID: 17, text: settlement.footerLine3Column2 });
    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 1, currencyTypeID: currencyTypeId, documentLocationTypeID: 18, text: settlement.footerLine4Column2 });
    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 1, currencyTypeID: currencyTypeId, documentLocationTypeID: 19, text: settlement.footerLine5Column2 });
  }

  return list;
}

function getOnlineBrandTexts(brand, body) {
  let list = [];

  if (brand.onlineBrandLevelTypeID == 3) {
    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 2, documentLocationTypeID: 4, text: body.onlineBranding.onlineHeaderLogoId });
    list.push({ partyRoleID: brand.partyRoleID, brandTypeID: 2, documentLocationTypeID: 14, text: body.onlineBranding.onlineServicesUrl });
  }

  return list;
}

/** Retrieve a brand from the store
	@func _retrieve
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain 
	
	BrandStore.Retrieve
*/	
function _retrieve(req, res, next) {
  let partyRoleId = req.params.id;
  return dao.sequelize.transaction(tran => {
    dao.crumbs.VwBrandtextPivot.removeAttribute('id');
    return dao.crumbs.Brand.findOne({
      where: {partyRoleID: partyRoleId},
      transaction: tran,
      include: {model: dao.crumbs.BrandLevelType, as: 'OnlineBrandLevelType'}
    })
      .then(br => {
        if (!br) {
          return next(new errors.NotFoundError());
        }
        let result = {
            brand: br
          },
          arr = [];
        arr.push(dao.crumbs.VwBrandtextPivot.findAll({
          where: {partyRoleID: partyRoleId},
          transaction: tran
        })
          .then(btp => {
            let pre = btp.find(bt => {
                if (bt.currencyTypeID == 3) {
                  return bt;
                }
              }),
              post = btp.find(bt => {
                if (bt.currencyTypeID == 4) {
                  return bt;
                }
              }),
              online = btp.find(bt => {
                if (bt.currencyTypeID == null) {
                  return bt;
                }
              });
            if (br.documentBrandLevelTypeID == 3) {
              result.documentsBranding = {
                headerAlignmentTypeId: pre.headerAlignmentTypeID,
                headerAlignmentTypeName: pre.headerAlignmentTypeName,
                headerLogoId: pre.headerLogo,
                preSettlement: pre,
                postSettlement: post
              };
            }
            if (br.onlineBrandLevelTypeID == 3) {
              result.onlineBranding = {
                brandLevelTypeId: br.onlineBrandLevelTypeID,
                brandLevelTypeName: br.OnlineBrandLevelType.name,
                onlineHeaderLogoId: online.headerLogo,
                onlineServicesUrl: online.servicesUrl,
                partyRoleId: br.partyRoleID
              };
            }
          }));
        return Promise.all(arr)
          .then(r => {
            res.send(result);
            return next();
          })
          .catch(next);
      })
      .catch(next);
  })
    .then()
    .catch(next)
    .done();
}

module.exports = {
	uploadImage: _uploadImage,
	save: 		 _save,
	retrieve: 	 _retrieve
};
