'use strict';

/** Campaigns api routes
 * @module routes/campaigns */

const dao = require('../dao'),
  errors = require('restify-errors'),
  utils      = require('./utils');

function getCampaignsList(req, res, next) {
  let searchTerm;
  if (req.query.type === 'contacts') {
    searchTerm = 1;
  } else if (req.query.type === 'accounts') {
    searchTerm = 2;
  } else {
    searchTerm = [1, 2];
  }

  dao.crumbs.Campaign.findAll({
      include: [{
        model: dao.crumbs.List,
        as: 'List',
        where: { partyTypeFilter: searchTerm },
      }]
    })
    .then(result => {
      result.forEach(function (campaign) {
        campaign.dataValues.List = undefined;
      });

      res.send(result);
      return next();
    })
    .catch(next)
    .done();
}

function saveCampaign(req, res, next) {
  delete req.body.created;
  delete req.body.createdByPartyRoleID;
  if (!req.body.iD) {
    req.body.created = new Date();
    req.body.createdByPartyRoleID = req.userAdapter.getPartyRoleId();
  }

  req.body.lastUpdated = new Date();
  req.body.lastUpdatedByPartyRoleID = req.userAdapter.getPartyRoleId();
  dao.sequelize.transaction(tran => {
      return dao.crumbs.Campaign.findOne({
          where: { iD: req.body.iD },
          transaction: tran,
        })
        .then(result => {
          let promise;
          if (!result) {
            if (req.body.iD) {
              return next(new errors.NotFoundError());
            }
            promise = dao.crumbs.Campaign.create(req.body);
          } else {
            promise = dao.crumbs.Campaign.update(req.body, {
              where: { iD: req.body.iD },
            });
          }

          return promise
            .then((l) => {
              let id = req.body.iD || l.iD;
              if (!result) {
                res.send(201, id);
              } else {
                res.send(200, id);
              }

              return next();
            })
            .catch(next);
        })
        .catch(next);
    })
    .then()

    .catch(next)
    .done();
}

function getCampaign(req, res, next) {

  let id = utils.si(req.params.id);

  if(!id){
    return next(new errors.BadRequestError());
  }

  dao.crumbs.Campaign.findOne({
    where: {iD: id}
  })
    .then(result => {

      if(!result){
        return next(new errors.NotFoundError());
      }

      res.send(result);
      return next();
    })
    .catch(next)
    .done();
}

module.exports = {
  getCampaignsList,
  saveCampaign,
  getCampaign
};
