'use strict';

/**
 * Routing module
 * @module routes
 */

module.exports = {
	admin: 						require("./admin"),
	ext: 							require("./ext"),

	abs: 							require("./abs"),
	address: 					require("./address"),
	bankAccount: 			require("./bank-account"),
	brandStore: 			require("./brand-store"),
	broker: 					require("./broker"),
	comms: 						require("./comms"),
	history: 					require("./history"),
	link: 						require("./link"),
	message: 					require("./message"),
	newsFeed: 				require("./news-feed"),
	partyRole: 				require("./party-role"),
	party: 						require("./party"),
	secondAuth: 			require("./second-auth"),
	template: 				require("./template"),
	user: 						require("./user"),
	lists:            require("./lists"),
	campaigns: 				require('./campaigns'),
	orphans:          require('./orphans'),
	settings:         require("./settings"),
	utils: 						require("./utils"),
	state: 						require("./state"),
  	typeLists: 					require('./type-lists'),
};
