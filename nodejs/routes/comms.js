"use strict";

/** Communications route implementation
    @module routes/comms */

const dao = require('../dao');
const mailer = require('../common/mailer');
const errors = require('restify-errors');
const Handlebars = require('handlebars');
const request = require('request');
const partyService = require('./party');
const _ = require('lodash');
const mongo = require('../dao/mongo');
const emailTemplate = mongo.models.emailTemplate;
const smsTemplate = mongo.models.smsTemplate;
const ObjectId = require('mongoose').Types.ObjectId;

const TELEPHONE_TYPE_MOBILE = 3;
const SENDER = 'systems@firstmac.com.au';
const SMS_RECIPIENT_DOMAIN = 'sms.corp.firstmac.com';

/** Sends an SMS message
    @func _sendSms
    @param req {Object} Incoming request object
    @param res {Object} Outgoing response object
    @param next {Function} Next middleware function in the chain

    CommsService.SendSms
    CommsService.SendSmsBatch
    CommsService.SendTemplateSms
    CommsService.SendTemplateSmsByName
*/
function _sendSms(req, res, next) {
  let promises = req.body.map(sms => {
    if (sms.sms) {
      return sendSms(sms.sms, res, next);
    } else if (sms.template) {
      return sendSmsTemplate(sms.template, res, next);
    }
  });
  Promise.all(promises)
    .then(r => {
      res.end();
      return next();
    })
    .catch(next);
}

function sendSms(sms, res, next) {
  return smsAddressForRecipient(sms.recipient, null, next)
    .then(telephoneNumbers => {
      telephoneNumbers = telephoneNumbers.map(number => `${number}@${SMS_RECIPIENT_DOMAIN}`);
      return mailer.sendMail(SENDER, telephoneNumbers, null, null, null, sms.message, null, null, next);
    })
    .catch(next);
}

function smsAddressForRecipient(phones, model, next) {
  let promises = phones.map(phone => {
    if (phone.literal) {
      return Promise.resolve(phone.literal.mobileNumber);
    } else if (phone.crumbs) {
      return partyService.retrieveDetailed(phone.crumbs.id, next)
        .then(party => {
          if (!party || !party.telephones.length) {
            return next(new errors.BadRequestError());
          }
          return smsAddressForParty(party, phone.crumbs.locationTypeId, phone.crumbs.communicationTypeId);
        })
        .catch(next);
    } else if (model && phone.model) {
      return Promise.resolve(_.get(model, phone.model.numberProperty, ''));
    }
  });
  return Promise.all(promises);
}

function smsAddressForParty(party, locationTypeId, communicationTypeId) {
  let phones = party.telephones.filter(phone => phone.telephoneTypeID == TELEPHONE_TYPE_MOBILE);
  if (locationTypeId) {
    phones = phones.filter(phone => phone.locationTypeID == locationTypeId);
  }
  if (communicationTypeId) {
    phones = phones.filter(phone => phone.communicationTypeID == communicationTypeId);
  }
  let phone = phones[0];
  if (!phone) {
    return null;
  }
  return Promise.resolve(phone.TelephoneNumber.telephoneNumber);
}

function sendSmsTemplate(body, res, next) {
  if (!body.id && !body.name) {
    return next(new errors.BadRequestError('id, name'));
  }
  return prepareTemplate(smsTemplate, body.id, body.name, body.override, next)
    .then(template => {
      if (!template || Object.keys(template).length === 0) {
        return next(new errors.BadRequestError());
      }
      return partyService.retrieveDetailed(body.partyId, next)
        .then(party => {
          let model = body.jsonModel;
          model.party = party;
          let message = Handlebars.compile(template.message)(model);
          return smsAddressForRecipient(template.to, model, next)
            .then(telephoneNumbers => {
              telephoneNumbers = telephoneNumbers.map(number => `${number}@${SMS_RECIPIENT_DOMAIN}`);
              return mailer.sendMail(SENDER, telephoneNumbers, null, null, null, message, null, null, next);
            })
            .catch(next);
        });
    })
    .catch(next);
}
/** Sends an email message
    @func _sendEmail
    @param req {Object} Incoming request object
    @param res {Object} Outgoing response object
    @param next {Function} Next middleware function in the chain

    CommsService.SendEmail
    CommsService.SendEmailBatch
    CommsService.SendTemplateEmail
    CommsService.SendTemplateEmailByName
*/
function _sendEmail(req, res, next) {
  let promises = req.body.map(email => {
    if (email.email) {
      return sendEmail(email.email, res, next);
    } else if (email.template) {
      return sendEmailTemplate(email.template, res, next);
    }
  });
  Promise.all(promises)
    .then(r => {
      res.end();
      return next();
    })
    .catch(next);
}

function sendEmail(email, res, next) {
  email.attachments = email.attachments || [];
  return prepareEmailRecipients(email, null, next)
    .then(recipients => {
      let mail = {
        subject: email.subject,
        attachments: email.attachments.map(attachment => {
          return {
            filename: attachment.name,
            path: attachment.path,
            contentType: attachment.mediaType,
          }
        }),
        from: `${recipients.from.displayName} <${recipients.from.email}>`,
        to: recipients.to.map(e => e.email),
        cc: recipients.cc.map(e => e.email),
        bcc: recipients.bcc.map(e => e.email),
        text: email.isHtml ? null : email.body,
        html: email.isHtml ? email.body : null,
      };
      return mailer.sendMail(mail.from, mail.to, mail.cc, mail.bcc, mail.subject, mail.text, mail.html, mail.attachments, next);
    })
    .catch(next);
}

function prepareEmailRecipients(email, model, next) {
  email.cc = email.cc || [];
  email.bcc = email.bcc || [];
  return mailAddressForRecipient(email.from, model, next)
    .then(from => {
      let toPromises = email.to.map(to => mailAddressForRecipient(to, model, next));
      return Promise.all(toPromises)
        .then(to => {
          let ccPromises = email.cc.map(cc => mailAddressForRecipient(cc, model, next));
          return Promise.all(ccPromises)
            .then(cc => {
              let bccPromises = email.bcc.map(bcc => mailAddressForRecipient(bcc, model, next));
              return Promise.all(bccPromises)
                .then(bcc => {
                  return {
                    from,
                    to,
                    cc,
                    bcc,
                  };
                })
                .catch(next);
            })
            .catch(next);
        })
        .catch(next);
    })
    .catch(next);
}

function mailAddressForRecipient(email, model, next) {
  if (email.literal) {
    return Promise.resolve({
      email: email.literal.address || email.literal.emailAddress,
      displayName: email.literal.name || email.literal.displayName,
    });
  } else if (email.crumbs) {
    return partyService.retrieveDetailed(email.crumbs.id, next)
      .then(party => {
        if (!party || !party.emails || !party.emails.length) {
          return next(new errors.BadRequestError());
        }
        return mailAddressForParty(party, email.crumbs.emailTypeId, email.crumbs.communicationTypeId, next);
      })
      .catch(next);
  } else if (model && email.model) {
    return Promise.resolve({
      email: _.get(model, email.model.emailProperty, null),
      displayName: _.get(model, email.model.displayNameProperty, null),
    });
  } else {
    return Promise.reject(new errors.BadRequestError('Invalid email format'));
  }
}

function mailAddressForParty(party, emailTypeId, communicationTypeId, next) {
  let emails = party.emails;
  if (emailTypeId) {
    emails = emails.filter(email => email.emailTypeID == emailTypeId);
  }
  if (communicationTypeId) {
    emails = emails.filter(email => email.communicationTypeID == communicationTypeId);
  }
  let emailAddress = emails[0];
  if (!emailAddress || !emailAddress.EmailAddress) {
    return next(new errors.BadRequestError());
  }
  let name;
  if (party.person) {
    name = `${party.person.firstName} ${party.person.lastName}`;
  } else if (party.company) {
    name = party.company.companyName;
  }
  return Promise.resolve({
    email: emailAddress.EmailAddress.emailAddress,
    displayName: name,
  });
}

function sendEmailTemplate(body, res, next) {
  if (!body.id && !body.name) {
    return next(new errors.BadRequestError('id, name'));
  }
  return prepareTemplate(emailTemplate, body.id, body.name, body.override, next)
    .then(template => {
      if (!template || Object.keys(template).length === 0) {
        return next(new errors.BadRequestError());
      }
      return partyService.retrieveDetailed(body.partyId, next)
        .then(party => {
          let model = body.jsonModel;
          model.party = party;
          let emailBody = Handlebars.compile(template.body)(model);
          let subject = Handlebars.compile(template.subject)(model);
          return prepareEmailRecipients(template, model, next)
            .then(recipients => {
              return prepareEmailAttachments(template.attachments)
                .then(attachments => {
                  let mail = {
                    attachments: attachments.map(attachment => {
                      return {
                        filename: attachment.filename,
                        content: attachment.data,
                      }
                    }),
                    from: `${recipients.from.displayName} <${recipients.from.email}>`,
                    to: recipients.to.map(e => e.email),
                    cc: recipients.cc.map(e => e.email),
                    bcc: recipients.bcc.map(e => e.email),
                    subject: subject,
                    text: template.isHtml ? null : emailBody,
                    html: template.isHtml ? emailBody : null,
                  };
                  return mailer.sendMail(mail.from, mail.to, mail.cc, mail.bcc, mail.subject, mail.text, mail.html, mail.attachments, next);
                })
                .catch(next);
            })
            .catch(next);
        });
    });
}

function prepareTemplate(templateSchema, id, name, override, next) {
  let searchTerm;
  if (!id){
    searchTerm = { name };
  } else {
    searchTerm = { _id: new ObjectId(id) };
  }
  return templateSchema.findOne(searchTerm)
    .then(result => {
      if(!result){
        return next(new errors.BadRequestError());
      }
      let template = result.toObject();
      if (!template) {
        return next(new errors.BadRequestError());
      } else {
        override = _.pickBy(override, property => {
          return property !== null;
        });
        Object.assign(template, override);
      }
      return template;
    })
    .catch(next);
}

function prepareEmailAttachments(attachments) {
  return new Promise((resolve, reject) => {
    let result = [];
    if (!attachments || attachments.length === 0) {
      resolve(result);
    }
    let iterationCount = 0;
    let complete = function (attachment) {
      result.push(attachment);
      if (++iterationCount >= attachments.length) {
        resolve(result);
      }
    };
    attachments.forEach(attachment => {
      if (!attachment) {
        complete(null);
      }
      if (attachment.local) {       //GridFS
        dao.mongo.dbs.grid.findOne({ _id: attachment.local.fileId }, function(error, file){
          if (error) {
            reject(error);
          }
          let stream = dao.mongo.dbs.grid.createReadStream({ _id: file._id });
          let chunks = [];
          stream.on('data', (chunk) => {
            chunks.push(chunk);
          });
          stream.on('end', () => {
            complete({
              filename: file.filename,
              data: Buffer.concat(chunks),
            });
          });
        });
      } else if (attachment.remote) {
        request.head(attachment.remote.url, function(error, response, body){
          if (error) {
            reject(error);
          }
          let stream = request(attachment.remote.url);
          let chunks = [];
          stream.on('data', (chunk) => {
            chunks.push(chunk);
          });
          stream.on('end', () => {
            let filename = 'file.dat';
            if (response && response.headers['content-disposition']) {
              filename = response.headers['content-disposition'].substr(response.headers['content-disposition'].indexOf("filename=") + 10).replace("\"", "");
            } else {
              filename = attachment.remote.url.substr(attachment.remote.url.lastIndexOf('/') + 1);
            }
            complete({
              filename,
              data: Buffer.concat(chunks),
            });
          });
        });
      }
    })
  });
}

module.exports = {
    sendSms:                        _sendSms,
    sendEmail:                      _sendEmail,
};
