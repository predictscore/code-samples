'use strict'

const dao = require('../dao'),
  _ = require('lodash'),
  errors = require('restify-errors'),
  fs = require('fs');

function _retrieveHistory(req, res, next) {
  return dao.crumbs.ContactImportAttempt.findAll()
    .then(hst => {
      if (!hst || !hst.length) {
        return next(new errors.NotFoundError('There are no imports'));
      }
      res.send(hst);
      return next();
    })
    .catch(next);
}

function contactImport(req, res, next) {
  return dao.crumbs.ContactImportAttempt.create({
      fileID: req.body.fileID,
      success: req.body.success,
      numContactsAdded: req.body.numContactsAdded,
      createdByPartyRoleID: req.userAdapter.getPartyRoleId(),
    })
    .then(r => {
      res.send(201);
      return next();
    })
    .catch(next);
}

function _downloadTemplate(req, res, next) {
  let path;
  if (req.params.spreadsheetType == 'contacts'){
    path = 'contacts.csv'
  } else if (req.params.spreadsheetType == 'accounts'){
    path = 'accounts.csv'
  } else {
    return next(new errors.BadRequestError());
  }
  fs.readFile(path, function(err, file) {
    if (err) {
      res.send(err);
      return next();
    }
    res.header('Content-Type', 'text/csv');
    res.header('Content-Disposition', 'attachment; filename=' + path);
    res.write(file);
    res.end();
    return next();
  });
}

function _uploadSpreadsheet(req, res, next) {
  if((req.params.spreadsheetType == 'contacts' || req.params.spreadsheetType == 'accounts') && req.params.import){
    let path = `uploads/import/spreadsheet-${req.params.spreadsheetType}-${Date.now()}`;
    fs.writeFile(path, req.params.import, function (err, result) {
      if (err){
        res.send(err);
        return next();
      } else {
        res.send(200);
        return next();
      }
    });
  } else {
    return next(new errors.BadRequestError());
  }

}

module.exports = {
  contactImport,
  retrieveHistory: _retrieveHistory,
  downloadTemplate: _downloadTemplate,
  uploadSpreadsheet: _uploadSpreadsheet
};
