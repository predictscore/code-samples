"use strict";

/** ABS route implementations
    @module routes/abs */
const dao     = require("../dao"),
      _       = require("lodash"),
      errors = require('restify-errors');

const PROFESSIONAL_INDEMNITY_INSURANCE_ID = 19;

/** Creates an ABS company and contact
	@func _createAbs
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	AccreditationService.CreateAbs
*/
function _createAbs(req, res, next) {

}

/** List related insured parties
    @func _listChildren
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	AccreditationService.ListChildren
*/
function _listChildren(req, res, next) {
  return dao.crumbs.PartyRole.findById(req.params.id)
    .then(pr => {
      if (!pr) {
        return next(new errors.NotFoundError());
      }
      return dao.crumbs.PartyRoleRelationship.findAll(relationshipSearchOptions(req.params.id, 'ChildPartyRole', 'childPartyRoleID'))
        .then(children => {
          res.send(children.map(child => {
            return {
              ChildPartyRoleName: dao.utils.partyNameByParty(child.ChildPartyRole.Party),
              ChildPartyRoleID: child.childPartyRoleID,
            }
          }));
          return next();
        })
        .catch(next);
    })
    .catch(next);
}

/** List the parent and siblings
	@func _listParentAndSiblings
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	Accreditation.GetParentAndSiblings
*/
function _listParentAndSiblings(req, res, next) {
  return dao.crumbs.PartyRole.findById(req.params.id)
    .then(pr => {
      if (!pr) {
        return next(new errors.NotFoundError());
      }
      return dao.crumbs.PartyRoleRelationship.findOne(relationshipSearchOptions(req.params.id, 'ParentPartyRole', 'parentPartyRoleID'))
        .then(parent => {
          if (!parent) {
            return next(new errors.NotFoundError());
          }
          return dao.crumbs.PartyRoleRelationship.findAll(relationshipSearchOptions(parent.parentPartyRoleID, 'ChildPartyRole', 'childPartyRoleID'))
            .then(children => {
              let result = [{
                ChildPartyRoleName: dao.utils.partyNameByParty(parent.ParentPartyRole.Party),
                ChildPartyRoleID: parent.parentPartyRoleID,
              }];
              res.send(result.concat(children.map(child => {
                return {
                  ChildPartyRoleName: dao.utils.partyNameByParty(child.ChildPartyRole.Party),
                  ChildPartyRoleID: child.childPartyRoleID,
                }
              })));
              return next();
            })
            .catch(next);
        })
        .catch(next);
    })
    .catch(next);
}

/** List the insurance relationships
	@func _listRelationships
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	AccreditationService.GetRelationships
*/
function _listRelationships(req, res, next) {
  return dao.crumbs.PartyRoleRelationship.findOne(relationshipSearchOptions(req.params.id, 'ParentPartyRole', 'parentPartyRoleID'))
    .then(parent => {
      if (!parent) {
        return next(new errors.NotFoundError());
      }
      return dao.crumbs.PartyRoleRelationship.findAll(relationshipSearchOptions(req.params.id, 'ChildPartyRole', 'childPartyRoleID'))
        .then(children => {
          let result = {
            isChild: req.params.id != parent.parentPartyRoleID,
            isParent: children.length > 0 && req.params.id == parent.parentPartyRoleID,
            parentPartyId: parent.ParentPartyRole.partyID,
            parentPartyName: dao.utils.partyNameByParty(parent.ParentPartyRole.Party),
            parentPartyRoleId: parent.parentPartyRoleID,
            relatedParties: [],
          };
          if (result.isParent) {
            result.relatedParties = children.map(child => {
              return {
                ChildPartyRoleName: dao.utils.partyNameByParty(child.ChildPartyRole.Party),
                ChildPartyRoleID: child.childPartyRoleID,
              }
            });
          } else if (result.isChild) {
            result.relatedParties.push({ ChildPartyRoleName: result.parentPartyName, ChildPartyRoleID: result.parentPartyRoleId });
            result.relatedParties = result.relatedParties.concat(children.map(child => {
              if (child.childPartyRoleID != result.parentPartyRoleId) {
                return {
                  ChildPartyRoleName: dao.utils.partyNameByParty(child.ChildPartyRole.Party),
                  ChildPartyRoleID: child.childPartyRoleID,
                }
              }
            }));
          }
          res.send(result);
          return next();
        })
        .catch(next);
    })
    .catch(next);
}

function relationshipSearchOptions(id, association, attribute) {
  return {
    where: {
      relationshipTypeID: 19,
      $or: {
        parentPartyRoleID: id,
        childPartyRoleID: id,
      },
    },
    include: [{
      model: dao.crumbs.PartyRole,
      as: association,
      include: [{
        model: dao.crumbs.Party,
        as: 'Party',
        include: [{
          model: dao.crumbs.Person,
          as: 'PersonPartyFks'
        }, {
          model: dao.crumbs.Company,
          as: 'CompanyPartyFks'
        }, {
          model: dao.crumbs.Trust,
          as: 'TrustPartyFks'
        }],
      }],
    }],
    attributes: [attribute],
  };
}

/** Saves insurance relationships
	@func _saveRelationships
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	AccreditationService.SaveRelationships
*/
function _saveRelationships(req, res, next) {
  var parentPartyRoleId = req.body.parentPartyRoleId;

  return dao.crumbs.PartyRoleRelationship.findOne({
    where: {
      relationshipTypeID: PROFESSIONAL_INDEMNITY_INSURANCE_ID,
      childPartyRoleID: parentPartyRoleId /* 364788  => { "ParentPartyRoleID": 342283, "ChildPartyRoleID": 364788 }*/
    }
  }).then(p => {
    if (p) {
      return Promise.reject("Party has existing insurance with other related party, " +
        "please remove the old PI relationship before adding");
    }

    return dao.crumbs.PartyRoleRelationship.destroy({
      where: {
        relationshipTypeID: PROFESSIONAL_INDEMNITY_INSURANCE_ID,
        parentPartyRoleID: parentPartyRoleId
      }
    });
  }).then(r => {
    return Promise.all(
      _.map(req.body.insuredParties, p => checkAndCreateParty(p, parentPartyRoleId))
    )
  }).then(r => {

    res.send(201, r.length);
    return next();
  }).catch(r => {

    res.send(409);
    return next();
  }).done();
}

/**
 *
 * @param p {Object}
 * @param p.partyRoleId {Object} not used but specified in documentation
 * @param p.partyName {Object} not used but specified in documentation
 * @param p.parentPartyRoleId {Object} not used but specified in documentation
 *
 * @param parentPartyRoleID
 *
 */
function checkAndCreateParty(p, parentPartyRoleId) {
  let child_party_role_id = p.parentPartyRoleId;

  return dao.crumbs.PartyRoleRelationship.findOne({
    where: {
      $or: {
        childPartyRoleID: child_party_role_id,
        parentPartyRoleID: child_party_role_id,
      },
      relationshipTypeID: PROFESSIONAL_INDEMNITY_INSURANCE_ID,
      parentPartyRoleID: {$ne: parentPartyRoleId}
    }
  }).then(p => {
    if (p) {
      return Promise.reject("Party has existing insurance with other related party," +
        " please remove the old PI relationship before adding");
    }

    return dao.crumbs.PartyRoleRelationship.findOrCreate({
      where: {
        parentPartyRoleID: parentPartyRoleId,
        childPartyRoleID: child_party_role_id,
        relationshipTypeID: PROFESSIONAL_INDEMNITY_INSURANCE_ID
      }
    })
  });
}

/** Lists a set of matched insurers
	@func _listMatchedInsurers
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	AccreditationService.ListMatchedInsurers
*/
function _listMatchedInsurers(req, res, next) {
	if (!req.query.frag) {
		return next(new errors.BadRequestError('frag'));
	}
	let filters = {
		name: req.query.frag,
		loginId: req.userAdapter.getLoginId(),
		partyRoleTypeId: 56,
		page: req.query.p || null,
		pageSize: req.query.ps || null,
	};
	return dao.party.list(filters, next)
		.then(il => {
			if (il.length == 0) {
				return next(new errors.NotFoundError());
			}
      var partyColumns = _.map(il, item => _.pickBy(item, (value, key) => key == "PartyRoleID" || key == "PartyName" ));
			res.send(partyColumns);
			return next();
		})
		.catch(next)
		.done();
}

/** Searches for insurers
	@func _searchCompanies
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	AccreditationService.SearchCompanies
*/
function _searchCompanies(req, res, next) {

}

/** Retrieves a set of insurance details
	@func _retrieveInsuranceDetails
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain

	AccreditationService.RetrieveInsuranceDetails
*/
function _retrieveInsuranceDetails(req, res, next) {
  let partyId = req.params.id;
  return Promise.all([dao.crumbs.PartyAccreditation.findOne({
    where: {partyID: partyId, active: true},
    include: {
      model: dao.crumbs.Party,
      as: 'Party',
      include: [{
        model: dao.crumbs.Person,
        as: 'PersonPartyFks'
      }, {
        model: dao.crumbs.Company,
        as: 'CompanyPartyFks'
      }, {
        model: dao.crumbs.Trust,
        as: 'TrustPartyFks'
      }]
    }
  }),
    dao.crumbs.PartyCompliance.findAll({
      where: {partyID: partyId, active: true}
    })])
    .then(r => {
      if (!r || ((!r[0]) && (!r[1] || !r[1].length))) {
        res.send(200, null);
        return next();
      }
      let result = {},
        insurance = r[0],
        compliance = r[1];
      if (insurance) {
        result.insurance = {
          ID: insurance.iD,
          InsurerName: null,
          InsurerPartyRoleID: insurance.insurerPartyRoleID,
          PolicyNumber: insurance.policyNumber,
          CoverAmount: insurance.coverAmount,
          PolicyDate: insurance.policyDate,
          CertificateOfCurrencySuppliedDate: insurance.certificateOfCurrencySuppliedDate,
          ExpiryEmailSentDate: insurance.expiryEmailSentDate,
          ExpiryDate: insurance.expiryDate,
          ConfirmRequestedDate: insurance.confirmRequestedDate,
          ConfirmReceivedDate: insurance.confirmReceivedDate
        };
        if (insurance.Party) {
          result.insurance.InsurerName = dao.utils.partyNameByParty(insurance.Party);
        }
      }
      if (compliance) {
        let cmp = _.orderBy(compliance, ['created'], ['desc']);
        result.compliance = {
          ID: cmp[0].iD,
          RegistrationTypeID: cmp[0].registrationTypeID,
          RegistrationNumber: cmp[0].registrationNumber,
          ExpiryDate: cmp[0].expiryDate
        }
      }
      res.send(result);
      return next();
    });
}

module.exports = {
	createAbs: 					_createAbs,
	listChildren: 				_listChildren,
	listParentAndSiblings: 		_listParentAndSiblings,
	listRelationships: 			_listRelationships,
	saveRelationships: 			_saveRelationships,
	listMatchedInsurers: 		_listMatchedInsurers,
	retrieveInsuranceDetails: 	_retrieveInsuranceDetails
};
