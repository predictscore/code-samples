"use strict";

/** Address route implementations
  * @module routes/address */
const Q 			= require("q"),
	  _ 			= require("lodash"),
	  config 		= require("config"),
	  errors = require("restify-errors");

const dao = require("../dao");

/** Retrieve a detailed address
	@func _retrieveDetailed
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain 

	AddressService.RetrieveDetailed
	AddressService.RetrieveDetailedByJusticeId
	AddressService.RetrieveDetailedWithAcillaryDetails
	AddressService.RetrieveDetailedRP
*/
function _retrieveDetailed(req, res, next) {
	const addressId = req.query.aid;
	const justiceAddressId = req.query.jid;
	const rpAddressId = req.query.rpid;
  let searchTerm;

	if(!(addressId || justiceAddressId || rpAddressId)) {
    return next(new errors.BadRequestError());
	}
	if(addressId) {
		searchTerm = { iD: addressId };
	}
	if(justiceAddressId) {
    searchTerm = { justiceID: justiceAddressId };
	}
  if (rpAddressId) {
    searchTerm = { rPDataPropertyID: rpAddressId };
  }
  return _getAddress(searchTerm, res, next);
}

function _getAddress(searchTerm, res, next) {
	return dao.crumbs.Address.findOne({
		where: searchTerm,
    include: [{
      model: dao.crumbs.Country,
      as: 'Country',
    }, {
      model: dao.crumbs.State,
      as: 'State',
    }]
	})
		.then(result => {
			if (!result) {
				return next(new errors.NotFoundError());
			}
			res.send({
        addressText: result.addressText,
        line: result.addressText,
        propertyName: result.propertyName,
        rp: result.rPDataPropertyID,
        stateId: result.stateID,
        stateName: result.State.name,
        streetNumber: result.streetNumber,
        streetName: result.streetName,
        streetTypeId: result.streetTypeID,
        stateText: result.stateText,
        streetTypeText: result.streetTypeText,
        suburb: result.suburb,
        unitNumber: result.unitNumber,
        country: result.Country.name,
        postCode: result.postCode,
        countryId: result.countryID,
        id: result.iD,
      });
			return next();
		})
		.catch(next);
}

/** Search for an address
    @func _search
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain 

	AddressService.SearchForAddress
	AddressService.SearchForAddressWithRp
*/
function _searchForAddress(req, res, next) {
	// paging parameters allow us to control how much data is being
	// retrieved from the database as well as being sent back to the
	// client on search requests
	let page = req.query.p || 1,
		pageSize = req.query.ps || config.get("defaults").pageSize;

	let filters = {
		frag: 					req.query.frag || null,
		page: 					page,
		pageSize: 				pageSize
	};

	return dao.sequelize.query(
		'SELECT * FROM "Address_RetrieveWithRPDataByAddressString_Paged"(:frag, :page, :pageSize);', 
		{ 
			replacements: filters,
			type: dao.sequelize.QueryTypes.SELECT
		}
	)
	.then(ps => {
		res.send(ps);
		return next();
	})
	.catch(next);
}

/** Search for a suburb
    @func _searchForSuburb
	@param req {Object} Incoming request object
	@param res {Object} Outgoing response object
	@param next {Function} Next middleware function in the chain 

	AddressService.SearchForSuburb
	AddressService.SearchForSuburbPostcode
*/ 	
function _searchForSuburb(req, res, next) {

	let si = (s) => {
		let i = parseInt(s);
		if (isNaN(i)) { return null; }
		return i;
	}

	// paging parameters allow us to control how much data is being
	// retrieved from the database as well as being sent back to the
	// client on search requests
	let page = req.query.p || 0,
		pageSize = req.query.ps || config.get("defaults").pageSize;

	let filters = {
		frag: 					req.query.frag || null,
		page: 					page,
		pageSize: 				pageSize
	};

	return dao.sequelize.query(
		'SELECT * FROM "Suburb_RetrieveBySuburbNameOrPostcode_Paged"(:frag, :page, :pageSize);', 
		{ 
			replacements: filters,
			type: dao.sequelize.QueryTypes.SELECT
		}
	)
	.then(ps => {
		res.send(ps);
		return next();
	})
	.catch(next)
	.done();
}

module.exports = {
	searchForAddress: _searchForAddress,
	retrieveDetailed: _retrieveDetailed,
	searchForSuburb: _searchForSuburb
};