'use strict';

const dao = require('../dao');

function getOrphansList(req, res, next) {
  let filters = {
    partyRoleId: req.userAdapter.getPartyRoleId(),
    limit: Number(req.query.l) || null,
    offset: Number(req.query.l) * Number(req.query.o) || 0,
    wi: req.query.wi || 'true',
  };
  let orphans = [];
  let withInternal = (filters.wi === 'true');
  let companies = dao.crumbs.Company.findAll({
      where: { partyID: { $notIn: dao.sequelize.literal('(SELECT "Party"."ID" FROM "Party" ' +
        'INNER JOIN "PartyRole" ON "PartyRole"."PartyID" = "Party"."ID" ' +
        'INNER JOIN "PartyRoleRelationship" ON "PartyRoleRelationship"."ChildPartyRoleID" = "PartyRole"."ID" ' +
        (withInternal ? '' : 'INNER JOIN "PartyRoleType" ON "PartyRoleType"."ID" = "PartyRole"."PartyRoleTypeID" ') +
        'WHERE "Party"."PartyTypeID" = 2 AND "PartyRoleRelationship"."RelationshipTypeID" = 18 ' + 
        (withInternal ? '' : 'AND "PartyRoleType"."PartyRoleGroupID" <> 10') + ')') } },
      include: [{
        model: dao.crumbs.PartyRole,
        as: 'RolePartyFks',
        required: true,
      }],
      order: [['partyID','ASC']],
      attributes: ['companyName', 'partyID'],
      limit: filters.limit,
      offset: filters.offset,
    })
    .then(r => {
      let result = [];
      r.map(elem => {
        elem.RolePartyFks.forEach(pr => {
          result.push({
            companyName: elem.companyName,
            partyId: elem.partyID,
            partyRoleId: pr.iD,
          })
        });
      });
      orphans = orphans.concat(result);
    })
    .catch(next);
  let contacts = dao.crumbs.Person.findAll({
      where: { partyID: { $notIn: dao.sequelize.literal('(SELECT "Party"."ID" FROM "Party" ' +
        'INNER JOIN "PartyRole" ON "PartyRole"."PartyID" = "Party"."ID" ' +
        'INNER JOIN "PartyRoleRelationship" ON "PartyRoleRelationship"."ChildPartyRoleID" = "PartyRole"."ID" ' +
        (withInternal ? '' : 'INNER JOIN "PartyRoleType" ON "PartyRoleType"."ID" = "PartyRole"."PartyRoleTypeID" ') +
        'WHERE "Party"."PartyTypeID" = 1 AND "PartyRoleRelationship"."RelationshipTypeID" = 17 ' + 
        (withInternal ? '' : 'AND "PartyRoleType"."PartyRoleGroupID" <> 10') + ')') } },
      include: [{
        model: dao.crumbs.PartyRole,
        as: 'RolePartyFks',
        required: true,
      }],
      order: [['partyID','ASC']],
      attributes: ['firstName', 'lastName', 'partyID'],
      limit: filters.limit,
      offset: filters.offset,
    })
    .then(r => {
      let result = [];
      r.map(elem => {
        elem.RolePartyFks.forEach(pr => {
          result.push({
            firstName: elem.firstName,
            lastName: elem.lastName,
            partyId: elem.partyID,
            partyRoleId: pr.iD,
          })
        });
      });
      orphans = orphans.concat(result);
    })
    .catch(next);
  let requesteeShares = dao.crumbs.RelationshipRequest.findAll({
      where: { requestorPartyRoleID: filters.partyRoleId },
      include: [{
        model: dao.crumbs.PartyRole,
        as: 'RequesteePartyRole',
        required: true,
        include: [{
          model: dao.crumbs.Person,
          as: 'Person',
          required: true,
        }]
      }],
      limit: filters.limit,
      offset: filters.offset,
    })
    .then(r => {
      r = r.map(elem => {
        return {
          firstName: elem.RequesteePartyRole.Person.firstName,
          lastName: elem.RequesteePartyRole.Person.lastName,
          partyId: elem.RequesteePartyRole.Person.partyID,
          partyRoleId: elem.RequesteePartyRole.iD,
        }
      });
      orphans = orphans.concat(r);
    })
    .catch(next);
  let requestorShares = dao.crumbs.RelationshipRequest.findAll({
      where: { requesteePartyRoleID: filters.partyRoleId },
      include: [{
        model: dao.crumbs.PartyRole,
        as: 'RequestorPartyRole',
        required: true,
        include: [{
          model: dao.crumbs.Person,
          as: 'Person',
          required: true,
        }]
      }],
      limit: filters.limit,
      offset: filters.offset,
    })
    .then(r => {
      r = r.map(elem => {
        return {
          firstName: elem.RequestorPartyRole.Person.firstName,
          lastName: elem.RequestorPartyRole.Person.lastName,
          partyId: elem.RequestorPartyRole.Person.partyID,
          partyRoleId: elem.RequestorPartyRole.iD,
        }
      });
      orphans = orphans.concat(r);
    })
    .catch(next);
  return Promise.all([
      companies,
      contacts,
      requesteeShares,
      requestorShares,
    ])
    .then(() => {
      res.send(orphans);
      return next();
    })
    .catch(next);
}

module.exports = {
  getOrphansList,
};
