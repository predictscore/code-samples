"use strict";

const dao = require('./conn');

module.exports.list = function (filters, next) {
  let partyTypeId = (!filters.partyTypeId || filters.partyTypeId.length == 0) ? 'NULL' : "'{:partyTypeId}'";
  let partyRoleTypeId = (!filters.partyRoleTypeId || filters.partyRoleTypeId.length == 0) ? 'NULL' : "'{:partyRoleTypeId}'";
  let partyRoleCategoryId = (!filters.partyRoleCategoryId || filters.partyRoleCategoryId.length == 0) ? 'NULL' : "'{:partyRoleCategoryId}'";
  let partyRoleGroupId = (!filters.partyRoleGroupId || filters.partyRoleGroupId.length == 0) ? 'NULL' : "'{:partyRoleGroupId}'";
  let partyRoleSuiteId = (!filters.partyRoleSuiteId || filters.partyRoleSuiteId.length == 0) ? 'NULL' : "'{:partyRoleSuiteId}'";
  //let ownerId = (!filters.ownerId || filters.ownerId.length == 0) ? 'NULL' : "'{:ownerId}'";
  let relationship = 'NULL';
  if (filters.relationshipTypeId || filters.relationshipParticipationType || filters.relationshipPartyRoleId) {
    filters.relationship = JSON.stringify([{
      RelationshipTypeID: filters.relationshipTypeId,
      RelationshipParticipationType: filters.relationshipParticipationType,
      PartyRoleID: filters.relationshipPartyRoleId
    }]);
    relationship = ':relationship';
  }

  return dao.sequelize.query(
    `SELECT * FROM "Party_List"(:loginId, ${partyTypeId}, ${partyRoleTypeId}, ${partyRoleCategoryId}, ${partyRoleGroupId}, ${partyRoleSuiteId}, :name, ${relationship}::json, :page, :pageSize);`,
    {
      replacements: filters,
      type: dao.sequelize.QueryTypes.SELECT
    }
  )
    .then(result => {
      return result;
    })
    .catch(next);
};
