"use strict";

/** Party role data access
 * @module dao/party-role */

const Q = require("q");

const dao = require("./conn"),
    common = require("fm-common"),
    logger = common.logger;

/** Saves a party role to the database
 * @func save
 * @param partyRole {Object} The data object to save
 * @param userAdapter {Object} The user invoking this function */
module.exports.save = function (partyRole, userAdapter) {

    let when = new Date(),
        who = userAdapter.getPartyRoleId();

    return dao.crumbs.PartyRole.findOne({
        where: {
            ID: partyRole.iD
        },
        attributes: [ 'ID' ]
    }).then(existingPartyRole => {

        // if we've already found one, then we're not updating; we'll just use it
        if (existingPartyRole &&
            existingPartyRole.dataValues &&
            existingPartyRole.dataValues.ID) {
            return existingPartyRole.dataValues.ID;
        }

        // set the creational and mutation attributes
        partyRole.created = when;
        partyRole.createdByPartyRoleID = who;

        // no existing record was found, so we're going to build the write request
        return dao.crumbs.PartyRole.build(partyRole)
            .save()
            .then(createdPartyRole => {
                return createdPartyRole.iD;
            });


    });
};