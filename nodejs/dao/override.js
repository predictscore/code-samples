const Sequelize = require('sequelize');

module.exports = function (model) {

  model.SecondaryAuthorityParameter.removeAttribute('id'); // removing the default SecondaryAuthorityParameter PK

  if (!model.PartyBankAccount.rawAttributes.treasuryApproved) {
    model.PartyBankAccount.rawAttributes.treasuryApproved = {
      type: Sequelize.DATE,
      field: 'TreasuryApproved',
      allowNull: true,
      Model: model.PartyBankAccount,
      fieldName: 'treasuryApproved',
      _modelAttribute: true
    };
  }

  if (!model.PartyBankAccount.rawAttributes.treasuryApprovedByPartyRoleID) {
    model.PartyBankAccount.rawAttributes.treasuryApprovedByPartyRoleID = {
      type: Sequelize.INTEGER,
      field: 'TreasuryApprovedByPartyRoleID',
      allowNull: true,
      references: { model: 'PartyRole', key: 'ID' },
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION',
      Model: model.PartyBankAccount,
      fieldName: 'treasuryApprovedByPartyRoleID',
      _modelAttribute: true
    };
  }

  model.PartyBankAccount.refreshAttributes();

  model.PartyBankAccount.belongsTo(model.PartyRole, {
    as: 'TreasuryApprovedByPartyRole',
    foreignKey: 'TreasuryApprovedByPartyRoleID',
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

  return model;
};
