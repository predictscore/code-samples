"use strict";

/** Telephone number data access
  * @module dao/address */

const Q = require("q");

const dao = require("./conn"),
    common = require("fm-common"),
    logger = common.logger;

/** Saves a telephone number to the database
  * @func save
  * @param telephoneNumber {Object} The data object to save
  * @param userAdapter {Object} The user invoking this function */
module.exports.save = function (telephoneNumber, userAdapter) {

  let when = new Date(),
      who = userAdapter.getPartyRoleId();

  /* check if only the id has been sent through; because this will force us to 
     not perform any data work */
  if (!telephoneNumber.telephoneNumber) {
    return Q(telephoneNumber.iD);
  }

  return dao.crumbs.TelephoneNumber.findOne({
    where: { telephoneNumber: telephoneNumber.telephoneNumber },
    attributes: [ 'iD' ]
  }).then(existingTelephoneNumber => {

    // if we've already found one, then we're not updating; we'll just use it
    if (existingTelephoneNumber && 
        existingTelephoneNumber.dataValues && 
        existingTelephoneNumber.dataValues.iD) {
      return existingTelephoneNumber.dataValues.iD;
    }

    // set the creational and mutation attributes
    delete telephoneNumber.iD;
    telephoneNumber.created = when;
    telephoneNumber.createdByPartyRoleID = who;
    telephoneNumber.lastUpdated = when;
    telephoneNumber.lastUpdatedByPartyRoleID = who;

    // no existing record was found, so we're going to build the write request
    return dao.crumbs.TelephoneNumber.build(telephoneNumber)
      .save()
      .then(createdTelephoneNumber => {
        return createdTelephoneNumber.iD;
      });


  });


};