'use strict';

/**
 * Data access object module
 * @module dao
 */

const conn 		= require("./conn");

module.exports = {
  sequelize: 	conn.sequelize,
  mongo:      require('./mongo'),
  crumbs: 		conn.crumbs,

  party:      require("./party"),
  utils:      require("./utils"),

  address: 		require("./address"),
  email: 			require("./email"),
  telephone: 	require("./telephone"),
  socialMediaAccount: require("./social-media"),
  partyRole: require("./party-role")
};
