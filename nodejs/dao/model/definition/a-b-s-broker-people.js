'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ABSBrokerPerson', {
        salutationTypeID: {
            type: DataTypes.INTEGER,
            field: 'SalutationTypeID',
            allowNull: true
        },
        firstName: {
            type: DataTypes.STRING(150),
            field: 'FirstName',
            allowNull: false
        },
        middleName: {
            type: DataTypes.STRING(150),
            field: 'MiddleName',
            allowNull: true
        },
        lastName: {
            type: DataTypes.STRING(150),
            field: 'LastName',
            allowNull: false
        },
        personPartyID: {
            type: DataTypes.INTEGER,
            field: 'PersonPartyID',
            allowNull: true
        },
        personPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'PersonPartyRoleID',
            allowNull: true
        },
        companyName: {
            type: DataTypes.STRING(150),
            field: 'CompanyName',
            allowNull: false
        },
        companyPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CompanyPartyRoleID',
            allowNull: true
        },
        personTypeID: {
            type: DataTypes.INTEGER,
            field: 'PersonTypeID',
            allowNull: false
        },
        emailAddress: {
            type: DataTypes.STRING(250),
            field: 'EmailAddress',
            allowNull: true
        },
        workPhone: {
            type: DataTypes.STRING(250),
            field: 'WorkPhone',
            allowNull: true
        },
        mobilePhone: {
            type: DataTypes.STRING(250),
            field: 'MobilePhone',
            allowNull: true
        },
        propertyName: {
            type: DataTypes.STRING(255),
            field: 'PropertyName',
            allowNull: true
        },
        unitNumber: {
            type: DataTypes.STRING(255),
            field: 'UnitNumber',
            allowNull: true
        },
        streetNumber: {
            type: DataTypes.STRING(255),
            field: 'StreetNumber',
            allowNull: true
        },
        pOBox: {
            type: DataTypes.STRING(255),
            field: 'POBox',
            allowNull: true
        },
        streetName: {
            type: DataTypes.STRING(255),
            field: 'StreetName',
            allowNull: true
        },
        streetType: {
            type: DataTypes.STRING(255),
            field: 'StreetType',
            allowNull: true
        },
        suburb: {
            type: DataTypes.STRING(255),
            field: 'Suburb',
            allowNull: true
        },
        state: {
            type: DataTypes.STRING(255),
            field: 'State',
            allowNull: true
        },
        postCode: {
            type: DataTypes.STRING(255),
            field: 'PostCode',
            allowNull: true
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'ABSBrokerPeople',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var ABSBrokerPerson = model.ABSBrokerPerson;

};
