'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentifierType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(512),
            field: 'Name',
            allowNull: false
        },
        claimTypeID: {
            type: DataTypes.INTEGER,
            field: 'ClaimTypeID',
            allowNull: true,
            references: {
                model: 'ClaimType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'IdentifierType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentifierType = model.IdentifierType;
    var MessageIdentifier = model.MessageIdentifier;
    var ClaimType = model.ClaimType;
    var Message = model.Message;

    IdentifierType.hasMany(MessageIdentifier, {
        as: 'MessageIdentifierIdentifiertypeFks',
        foreignKey: 'IdentifierTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentifierType.belongsTo(ClaimType, {
        as: 'ClaimType',
        foreignKey: 'ClaimTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentifierType.belongsToMany(Message, {
        as: 'MessageIdentifierMessages',
        through: MessageIdentifier,
        foreignKey: 'IdentifierTypeID',
        otherKey: 'MessageID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
