'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('DocumentLocationType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        contentTypeID: {
            type: DataTypes.INTEGER,
            field: 'ContentTypeID',
            allowNull: true,
            references: {
                model: 'ContentType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'DocumentLocationType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var DocumentLocationType = model.DocumentLocationType;
    var BrandText = model.BrandText;
    var ContentType = model.ContentType;
    var AlignmentType = model.AlignmentType;
    var BrandType = model.BrandType;
    var CurrencyType = model.CurrencyType;
    var PartyRole = model.PartyRole;

    DocumentLocationType.hasMany(BrandText, {
        as: 'BrandTextDocumentlocationtypeFks',
        foreignKey: 'DocumentLocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    DocumentLocationType.belongsTo(ContentType, {
        as: 'ContentType',
        foreignKey: 'ContentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    DocumentLocationType.belongsToMany(AlignmentType, {
        as: 'BrandTextAlignmentTypes',
        through: BrandText,
        foreignKey: 'DocumentLocationTypeID',
        otherKey: 'AlignmentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    DocumentLocationType.belongsToMany(BrandType, {
        as: 'BrandTextBrandTypes',
        through: BrandText,
        foreignKey: 'DocumentLocationTypeID',
        otherKey: 'BrandTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    DocumentLocationType.belongsToMany(CurrencyType, {
        as: 'BrandTextCurrencyTypes',
        through: BrandText,
        foreignKey: 'DocumentLocationTypeID',
        otherKey: 'CurrencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    DocumentLocationType.belongsToMany(PartyRole, {
        as: 'BrandTextPartyRoles',
        through: BrandText,
        foreignKey: 'DocumentLocationTypeID',
        otherKey: 'PartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
