'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('RetailCustomerTelephone', {
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: true
        },
        partyTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyTypeID',
            allowNull: true
        },
        partyRoleID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleID',
            allowNull: true
        },
        partyTelephoneID: {
            type: DataTypes.INTEGER,
            field: 'PartyTelephoneID',
            allowNull: true
        },
        telephoneID: {
            type: DataTypes.INTEGER,
            field: 'TelephoneID',
            allowNull: true
        },
        telephoneNumber: {
            type: DataTypes.STRING(20),
            field: 'TelephoneNumber',
            allowNull: true
        },
        optOut: {
            type: DataTypes.BOOLEAN,
            field: 'OptOut',
            allowNull: true
        },
        communicationTypeID: {
            type: DataTypes.INTEGER,
            field: 'CommunicationTypeID',
            allowNull: true
        },
        locationTypeID: {
            type: DataTypes.INTEGER,
            field: 'LocationTypeID',
            allowNull: true
        },
        telephoneTypeID: {
            type: DataTypes.INTEGER,
            field: 'TelephoneTypeID',
            allowNull: true
        },
        priority: {
            type: DataTypes.INTEGER,
            field: 'Priority',
            allowNull: true
        },
        locationTypeName: {
            type: DataTypes.STRING(50),
            field: 'LocationTypeName',
            allowNull: true
        },
        communicationTypeName: {
            type: DataTypes.STRING(200),
            field: 'CommunicationTypeName',
            allowNull: true
        },
        telephoneTypeName: {
            type: DataTypes.STRING(50),
            field: 'TelephoneTypeName',
            allowNull: true
        },
        preferredContact: {
            type: DataTypes.BOOLEAN,
            field: 'PreferredContact',
            allowNull: true
        },
        claimLeadID: {
            type: DataTypes.INTEGER,
            field: 'ClaimLeadID',
            allowNull: true
        },
        claimCustomerID: {
            type: DataTypes.INTEGER,
            field: 'ClaimCustomerID',
            allowNull: true
        },
        claimPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'ClaimPartyRoleID',
            allowNull: true
        },
        loginID: {
            type: DataTypes.INTEGER,
            field: 'LoginID',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'RetailCustomerTelephones',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var RetailCustomerTelephone = model.RetailCustomerTelephone;

};
