'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyRole', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: false,
            references: {
                model: 'Party',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        partyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleTypeID',
            allowNull: false,
            references: {
                model: 'PartyRoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        justicePartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'JusticePartyRoleID',
            allowNull: true
        },
        tontoID: {
            type: DataTypes.INTEGER,
            field: 'TontoID',
            allowNull: true
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: true
        },
        v8ID: {
            type: DataTypes.INTEGER,
            field: 'V8ID',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'PartyRole',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyRole = model.PartyRole;
    var Address = model.Address;
    var BankAccount = model.BankAccount;
    var BankBSB = model.BankBSB;
    var Brand = model.Brand;
    var BrandText = model.BrandText;
    var BrokerProgramAgreement = model.BrokerProgramAgreement;
    var Company = model.Company;
    var Campaign = model.Campaign;
    var EmailAddress = model.EmailAddress;
    var HistoryNote = model.HistoryNote;
    var IdentityVerificationReport = model.IdentityVerificationReport;
    var NewsFeed = model.NewsFeed;
    var Party = model.Party;
    var PartyAccreditation = model.PartyAccreditation;
    var PartyAddress = model.PartyAddress;
    var PartyBankAccount = model.PartyBankAccount;
    var PartyCompliance = model.PartyCompliance;
    var PartyEmail = model.PartyEmail;
    var PartyRoleRelationship = model.PartyRoleRelationship;
    var PartySearchAvailability = model.PartySearchAvailability;
    var PartyTelephone = model.PartyTelephone;
    var PartyTrustee = model.PartyTrustee;
    var Person = model.Person;
    var RelationshipRequest = model.RelationshipRequest;
    var TelephoneNumber = model.TelephoneNumber;
    var Trust = model.Trust;
    var PartyRoleType = model.PartyRoleType;
    var Country = model.Country;
    var State = model.State;
    var StreetType = model.StreetType;
    var BrandLevelType = model.BrandLevelType;
    var AlignmentType = model.AlignmentType;
    var BrandType = model.BrandType;
    var CurrencyType = model.CurrencyType;
    var DocumentLocationType = model.DocumentLocationType;
    var AccountRankingType = model.AccountRankingType;
    var AccreditationStatusType = model.AccreditationStatusType;
    var CompanyType = model.CompanyType;
    var IndustryType = model.IndustryType;
    var MainBusinessActivityType = model.MainBusinessActivityType;
    var HistoryNoteStatusType = model.HistoryNoteStatusType;
    var HistoryNoteType = model.HistoryNoteType;
    var IdentityVerificationReportType = model.IdentityVerificationReportType;
    var PartyType = model.PartyType;
    var AddressType = model.AddressType;
    var CommunicationType = model.CommunicationType;
    var LocationType = model.LocationType;
    var ResidencyType = model.ResidencyType;
    var PartyBankAccountType = model.PartyBankAccountType;
    var RegistrationType = model.RegistrationType;
    var EmailType = model.EmailType;
    var RelationshipType = model.RelationshipType;
    var SearchType = model.SearchType;
    var TelephoneType = model.TelephoneType;
    var GenderType = model.GenderType;
    var MaritalStatusType = model.MaritalStatusType;
    var ObjectColourType = model.ObjectColourType;
    var NameChangeReasonType = model.NameChangeReasonType;
    var SalutationType = model.SalutationType;
    var DocumentationType = model.DocumentationType;
    var TrustPurposeType = model.TrustPurposeType;
    var TrustStructureType = model.TrustStructureType;
    var List = model.List;

    PartyRole.hasMany(Address, {
        as: 'AddressCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(Address, {
        as: 'AddressLastupdatedbypartyroleFks',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(BankAccount, {
        as: 'BankAccountCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(BankAccount, {
        as: 'BankAccountLastupdatedbypartyroleFks',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(BankBSB, {
        as: 'BankBSBCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(BankBSB, {
        as: 'BankBSBLastupdatedbypartyroleFks',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(Brand, {
        as: 'BrandCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(Brand, {
        as: 'BrandLastupdatedbypartyroleFks',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(Brand, {
        as: 'BrandPartyroleFks',
        foreignKey: 'PartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(BrandText, {
        as: 'BrandTextPartyroleFks',
        foreignKey: 'PartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(BrokerProgramAgreement, {
        as: 'BrokerProgramAgreementCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(BrokerProgramAgreement, {
        as: 'BrokerProgramAgreementPartyroleFks',
        foreignKey: 'PartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(Company, {
        as: 'CompanyAccountantpartyroleFks',
        foreignKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(Company, {
        as: 'CompanyLastupdatedbypartyroleFks',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(EmailAddress, {
        as: 'EmailAddressCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(HistoryNote, {
        as: 'HistoryNoteCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(HistoryNote, {
        as: 'HistoryNoteFollowupbypartyroleFks',
        foreignKey: 'FollowUpByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(IdentityVerificationReport, {
        as: 'FKIdentityverificationreportCreatedbypartyroles',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(IdentityVerificationReport, {
        as: 'FKIdentityverificationreportLastupdatedbypartyroles',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(NewsFeed, {
        as: 'NewsFeedCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(NewsFeed, {
        as: 'NewsFeedUpdatedbypartyroleFks',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(Party, {
        as: 'PartyCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(Party, {
        as: 'PartyLastupdatedbypartyroleFks',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyAccreditation, {
        as: 'PartyAccreditationCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyAccreditation, {
        as: 'PartyAccreditationInsurerpartyroleFks',
        foreignKey: 'InsurerPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyAccreditation, {
        as: 'PartyAccreditationLastupdatedbypartyroleFks',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyAddress, {
        as: 'PartyAddressCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyAddress, {
        as: 'PartyAddressPartyroleFks',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyBankAccount, {
        as: 'PartyBankAccountCreatebypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyBankAccount, {
        as: 'PartyBankAccountLastupdatedbypartyroleFks',
        foreignKey: 'LastUpdateByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyCompliance, {
        as: 'PartyComplianceCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyCompliance, {
        as: 'PartyComplianceLastupdatedbypartyroleFks',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyEmail, {
        as: 'PartyEmailCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyEmail, {
        as: 'PartyEmailLastupdatedbypartyroleFks',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyRole, {
        as: 'CreatedByPartyRoleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyRoleRelationship, {
        as: 'RelationshipChildpartyroleFks',
        foreignKey: 'ChildPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyRoleRelationship, {
        as: 'RelationshipParentpartyroleFks',
        foreignKey: 'ParentPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartySearchAvailability, {
        as: 'PartySearchAvailabilityCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartySearchAvailability, {
        as: 'PartySearchAvailabilityLastupdatedbypartyroleFks',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyTelephone, {
        as: 'PartyTelephoneCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyTelephone, {
        as: 'PartyTelephoneLastupdatedbypartyroleFks',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyTrustee, {
        as: 'PartyTrusteeCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(PartyTrustee, {
        as: 'PartyTrusteePartyroleFks',
        foreignKey: 'TrustPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(Person, {
        as: 'PersonAccountantpartyroleFks',
        foreignKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(Person, {
        as: 'PersonLastupdatedbypartyroleFks',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(RelationshipRequest, {
        as: 'RelationshipRequestApprovedbypartyroleFks',
        foreignKey: 'ApprovedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(RelationshipRequest, {
        as: 'RelationshipRequestCreatedbypartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(RelationshipRequest, {
        as: 'RelationshipRequestDeclinedbypartyroleFks',
        foreignKey: 'DeclinedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(RelationshipRequest, {
        as: 'RelationshipRequestRequesteepartyroleFks',
        foreignKey: 'RequesteePartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(RelationshipRequest, {
        as: 'RelationshipRequestRequestorpartyroleFks',
        foreignKey: 'RequestorPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(TelephoneNumber, {
        as: 'TelephoneNumberPartyroleFks',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(Trust, {
        as: 'TrustAccountantpartyroleFks',
        foreignKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(Trust, {
        as: 'TrustLastupdatedbypartyroleFks',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(List, {
        as: 'ListCreatedbypartyroleFks',
        foreignKey: 'createdByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(List, {
        as: 'ListLastupdatedbypartyroleFks',
        foreignKey: 'lastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(Campaign, {
        as: 'CampaignCreatedbypartyroleFks',
        foreignKey: 'createdByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.hasMany(Campaign, {
        as: 'CampaignLastupdatedbypartyroleFks',
        foreignKey: 'lastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsTo(PartyRoleType, {
        as: 'PartyRoleType',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsTo(Party, {
        as: 'Party',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsTo(Person, {
        as: 'Person',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Country, {
        as: 'AddressCountries',
        through: Address,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'CountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'AddressLastUpdatedByPartyRoles',
        through: Address,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(State, {
        as: 'AddressStates',
        through: Address,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(StreetType, {
        as: 'AddressStreetTypes',
        through: Address,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'StreetTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Country, {
        as: 'AddressCountries',
        through: Address,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'AddressCreatedByPartyRoles',
        through: Address,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(State, {
        as: 'AddressStates',
        through: Address,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(StreetType, {
        as: 'AddressStreetTypes',
        through: Address,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'StreetTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(BankBSB, {
        as: 'BankAccountBankBSBs',
        through: BankAccount,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'BankBSBID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'BankAccountLastUpdatedByPartyRoles',
        through: BankAccount,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(BankBSB, {
        as: 'BankAccountBankBSBs',
        through: BankAccount,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'BankBSBID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'BankAccountCreatedByPartyRoles',
        through: BankAccount,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Address, {
        as: 'BankBSBAddresses',
        through: BankBSB,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'AddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'BankBSBLastUpdatedByPartyRoles',
        through: BankBSB,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Address, {
        as: 'BankBSBAddresses',
        through: BankBSB,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'AddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'BankBSBCreatedByPartyRoles',
        through: BankBSB,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(BrandLevelType, {
        as: 'BrandDocumentBrandLevelTypes',
        through: Brand,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'DocumentBrandLevelTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'BrandLastUpdatedByPartyRoles',
        through: Brand,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(BrandLevelType, {
        as: 'BrandOnlineBrandLevelTypes',
        through: Brand,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'OnlineBrandLevelTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'BrandPartyRoles',
        through: Brand,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'PartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'BrandCreatedByPartyRoles',
        through: Brand,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(BrandLevelType, {
        as: 'BrandDocumentBrandLevelTypes',
        through: Brand,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'DocumentBrandLevelTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(BrandLevelType, {
        as: 'BrandOnlineBrandLevelTypes',
        through: Brand,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'OnlineBrandLevelTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'BrandPartyRoles',
        through: Brand,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'PartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'BrandCreatedByPartyRoles',
        through: Brand,
        foreignKey: 'PartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(BrandLevelType, {
        as: 'BrandDocumentBrandLevelTypes',
        through: Brand,
        foreignKey: 'PartyRoleID',
        otherKey: 'DocumentBrandLevelTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'BrandLastUpdatedByPartyRoles',
        through: Brand,
        foreignKey: 'PartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(BrandLevelType, {
        as: 'BrandOnlineBrandLevelTypes',
        through: Brand,
        foreignKey: 'PartyRoleID',
        otherKey: 'OnlineBrandLevelTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(AlignmentType, {
        as: 'BrandTextAlignmentTypes',
        through: BrandText,
        foreignKey: 'PartyRoleID',
        otherKey: 'AlignmentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(BrandType, {
        as: 'BrandTextBrandTypes',
        through: BrandText,
        foreignKey: 'PartyRoleID',
        otherKey: 'BrandTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(CurrencyType, {
        as: 'BrandTextCurrencyTypes',
        through: BrandText,
        foreignKey: 'PartyRoleID',
        otherKey: 'CurrencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(DocumentLocationType, {
        as: 'BrandTextDocumentLocationTypes',
        through: BrandText,
        foreignKey: 'PartyRoleID',
        otherKey: 'DocumentLocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'BrokerProgramAgreementPartyRoles',
        through: BrokerProgramAgreement,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'PartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'BrokerProgramAgreementCreatedByPartyRoles',
        through: BrokerProgramAgreement,
        foreignKey: 'PartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(AccountRankingType, {
        as: 'CompanyAccountRankingTypes',
        through: Company,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'AccountRankingTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(AccreditationStatusType, {
        as: 'CompanyAccreditationStatusTypes',
        through: Company,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'AccreditationStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(CompanyType, {
        as: 'CompanyCompanyTypes',
        through: Company,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'CompanyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(IndustryType, {
        as: 'CompanyIndustryTypes',
        through: Company,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'IndustryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'CompanyLastUpdatedByPartyRoles',
        through: Company,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(MainBusinessActivityType, {
        as: 'CompanyMainBusinessActivities',
        through: Company,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'MainBusinessActivityID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'CompanyParties',
        through: Company,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(State, {
        as: 'CompanyRegisteredInStates',
        through: Company,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'RegisteredInStateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(AccountRankingType, {
        as: 'CompanyAccountRankingTypes',
        through: Company,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'AccountRankingTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'CompanyAccountantPartyRoles',
        through: Company,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(AccreditationStatusType, {
        as: 'CompanyAccreditationStatusTypes',
        through: Company,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'AccreditationStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(CompanyType, {
        as: 'CompanyCompanyTypes',
        through: Company,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CompanyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(IndustryType, {
        as: 'CompanyIndustryTypes',
        through: Company,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'IndustryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(MainBusinessActivityType, {
        as: 'CompanyMainBusinessActivities',
        through: Company,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'MainBusinessActivityID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'CompanyParties',
        through: Company,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(State, {
        as: 'CompanyRegisteredInStates',
        through: Company,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'RegisteredInStateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'HistoryNoteFollowUpByPartyRoles',
        through: HistoryNote,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'FollowUpByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(HistoryNoteStatusType, {
        as: 'HistoryNoteHistoryNoteStatusTypes',
        through: HistoryNote,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'HistoryNoteStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(HistoryNoteType, {
        as: 'HistoryNoteHistoryNoteTypes',
        through: HistoryNote,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'HistoryNoteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'HistoryNoteParties',
        through: HistoryNote,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'HistoryNoteCreatedByPartyRoles',
        through: HistoryNote,
        foreignKey: 'FollowUpByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(HistoryNoteStatusType, {
        as: 'HistoryNoteHistoryNoteStatusTypes',
        through: HistoryNote,
        foreignKey: 'FollowUpByPartyRoleID',
        otherKey: 'HistoryNoteStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(HistoryNoteType, {
        as: 'HistoryNoteHistoryNoteTypes',
        through: HistoryNote,
        foreignKey: 'FollowUpByPartyRoleID',
        otherKey: 'HistoryNoteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'HistoryNoteParties',
        through: HistoryNote,
        foreignKey: 'FollowUpByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(IdentityVerificationReportType, {
        as: 'IdentityVerificationReportIdentityVerificationReportTypes',
        through: IdentityVerificationReport,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'IdentityVerificationReportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'IdentityVerificationReportLastUpdatedByPartyRoles',
        through: IdentityVerificationReport,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'IdentityVerificationReportParties',
        through: IdentityVerificationReport,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'IdentityVerificationReportCreatedByPartyRoles',
        through: IdentityVerificationReport,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(IdentityVerificationReportType, {
        as: 'IdentityVerificationReportIdentityVerificationReportTypes',
        through: IdentityVerificationReport,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'IdentityVerificationReportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'IdentityVerificationReportParties',
        through: IdentityVerificationReport,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'NewsFeedLastUpdatedByPartyRoles',
        through: NewsFeed,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'NewsFeedCreatedByPartyRoles',
        through: NewsFeed,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyLastUpdatedByPartyRoles',
        through: Party,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyType, {
        as: 'PartyPartyTypes',
        through: Party,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'PartyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyCreatedByPartyRoles',
        through: Party,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyType, {
        as: 'PartyPartyTypes',
        through: Party,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'PartyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyAccreditationInsurerPartyRoles',
        through: PartyAccreditation,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'InsurerPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyAccreditationLastUpdatedByPartyRoles',
        through: PartyAccreditation,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyAccreditationParties',
        through: PartyAccreditation,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyAccreditationCreatedByPartyRoles',
        through: PartyAccreditation,
        foreignKey: 'InsurerPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyAccreditationLastUpdatedByPartyRoles',
        through: PartyAccreditation,
        foreignKey: 'InsurerPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyAccreditationParties',
        through: PartyAccreditation,
        foreignKey: 'InsurerPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyAccreditationCreatedByPartyRoles',
        through: PartyAccreditation,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyAccreditationInsurerPartyRoles',
        through: PartyAccreditation,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'InsurerPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyAccreditationParties',
        through: PartyAccreditation,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(AddressType, {
        as: 'PartyAddressAddressTypes',
        through: PartyAddress,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'AddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Address, {
        as: 'PartyAddressAddresses',
        through: PartyAddress,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'AddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(CommunicationType, {
        as: 'PartyAddressCommunicationTypes',
        through: PartyAddress,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(CurrencyType, {
        as: 'PartyAddressCurrencyTypes',
        through: PartyAddress,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'CurrencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(LocationType, {
        as: 'PartyAddressLocationTypes',
        through: PartyAddress,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyAddressLastUpdatedByPartyRoles',
        through: PartyAddress,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyAddressParties',
        through: PartyAddress,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(ResidencyType, {
        as: 'PartyAddressResidencyTypes',
        through: PartyAddress,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'ResidencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(AddressType, {
        as: 'PartyAddressAddressTypes',
        through: PartyAddress,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'AddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Address, {
        as: 'PartyAddressAddresses',
        through: PartyAddress,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'AddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(CommunicationType, {
        as: 'PartyAddressCommunicationTypes',
        through: PartyAddress,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyAddressCreatedByPartyRoles',
        through: PartyAddress,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(CurrencyType, {
        as: 'PartyAddressCurrencyTypes',
        through: PartyAddress,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CurrencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(LocationType, {
        as: 'PartyAddressLocationTypes',
        through: PartyAddress,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyAddressParties',
        through: PartyAddress,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(ResidencyType, {
        as: 'PartyAddressResidencyTypes',
        through: PartyAddress,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'ResidencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(BankAccount, {
        as: 'PartyBankAccountBankAccounts',
        through: PartyBankAccount,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'BankAccountID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(BankBSB, {
        as: 'PartyBankAccountBankBSBs',
        through: PartyBankAccount,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'BankBSBID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyBankAccountLastUpdateByPartyRoles',
        through: PartyBankAccount,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LastUpdateByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyBankAccountType, {
        as: 'PartyBankAccountPartyBankAccountTypes',
        through: PartyBankAccount,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'PartyBankAccountTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyBankAccountParties',
        through: PartyBankAccount,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(BankAccount, {
        as: 'PartyBankAccountBankAccounts',
        through: PartyBankAccount,
        foreignKey: 'LastUpdateByPartyRoleID',
        otherKey: 'BankAccountID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(BankBSB, {
        as: 'PartyBankAccountBankBSBs',
        through: PartyBankAccount,
        foreignKey: 'LastUpdateByPartyRoleID',
        otherKey: 'BankBSBID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyBankAccountCreatedByPartyRoles',
        through: PartyBankAccount,
        foreignKey: 'LastUpdateByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyBankAccountType, {
        as: 'PartyBankAccountPartyBankAccountTypes',
        through: PartyBankAccount,
        foreignKey: 'LastUpdateByPartyRoleID',
        otherKey: 'PartyBankAccountTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyBankAccountParties',
        through: PartyBankAccount,
        foreignKey: 'LastUpdateByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyComplianceLastUpdatedByPartyRoles',
        through: PartyCompliance,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyComplianceParties',
        through: PartyCompliance,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(RegistrationType, {
        as: 'PartyComplianceRegistrationTypes',
        through: PartyCompliance,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'RegistrationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyComplianceCreatedByPartyRoles',
        through: PartyCompliance,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyComplianceParties',
        through: PartyCompliance,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(RegistrationType, {
        as: 'PartyComplianceRegistrationTypes',
        through: PartyCompliance,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'RegistrationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(CommunicationType, {
        as: 'PartyEmailCommunicationTypes',
        through: PartyEmail,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(EmailAddress, {
        as: 'PartyEmailEmailAddresses',
        through: PartyEmail,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'EmailAddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(EmailType, {
        as: 'PartyEmailEmailTypes',
        through: PartyEmail,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'EmailTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyEmailLastUpdatedByPartyRoles',
        through: PartyEmail,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(LocationType, {
        as: 'PartyEmailLocationTypes',
        through: PartyEmail,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyEmailParties',
        through: PartyEmail,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(CommunicationType, {
        as: 'PartyEmailCommunicationTypes',
        through: PartyEmail,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyEmailCreatedByPartyRoles',
        through: PartyEmail,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(EmailAddress, {
        as: 'PartyEmailEmailAddresses',
        through: PartyEmail,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'EmailAddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(EmailType, {
        as: 'PartyEmailEmailTypes',
        through: PartyEmail,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'EmailTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(LocationType, {
        as: 'PartyEmailLocationTypes',
        through: PartyEmail,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyEmailParties',
        through: PartyEmail,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRoleType, {
        as: 'PartyRolePartyRoleTypes',
        through: PartyRole,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyRoleParties',
        through: PartyRole,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyRoleRelationshipParentPartyRoles',
        through: PartyRoleRelationship,
        foreignKey: 'ChildPartyRoleID',
        otherKey: 'ParentPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(RelationshipType, {
        as: 'PartyRoleRelationshipRelationshipTypes',
        through: PartyRoleRelationship,
        foreignKey: 'ChildPartyRoleID',
        otherKey: 'RelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyRoleRelationshipChildPartyRoles',
        through: PartyRoleRelationship,
        foreignKey: 'ParentPartyRoleID',
        otherKey: 'ChildPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(RelationshipType, {
        as: 'PartyRoleRelationshipRelationshipTypes',
        through: PartyRoleRelationship,
        foreignKey: 'ParentPartyRoleID',
        otherKey: 'RelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartySearchAvailabilityLastUpdatedByPartyRoles',
        through: PartySearchAvailability,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartySearchAvailabilityParties',
        through: PartySearchAvailability,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(SearchType, {
        as: 'PartySearchAvailabilitySearchTypes',
        through: PartySearchAvailability,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'SearchTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartySearchAvailabilityCreatedByPartyRoles',
        through: PartySearchAvailability,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartySearchAvailabilityParties',
        through: PartySearchAvailability,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(SearchType, {
        as: 'PartySearchAvailabilitySearchTypes',
        through: PartySearchAvailability,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'SearchTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(CommunicationType, {
        as: 'PartyTelephoneCommunicationTypes',
        through: PartyTelephone,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyTelephoneLastUpdatedByPartyRoles',
        through: PartyTelephone,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(LocationType, {
        as: 'PartyTelephoneLocationTypes',
        through: PartyTelephone,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyTelephoneParties',
        through: PartyTelephone,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(TelephoneNumber, {
        as: 'PartyTelephoneTelephoneNumbers',
        through: PartyTelephone,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'TelephoneNumberID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(TelephoneType, {
        as: 'PartyTelephoneTelephoneTypes',
        through: PartyTelephone,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'TelephoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(CommunicationType, {
        as: 'PartyTelephoneCommunicationTypes',
        through: PartyTelephone,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyTelephoneCreatedByPartyRoles',
        through: PartyTelephone,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(LocationType, {
        as: 'PartyTelephoneLocationTypes',
        through: PartyTelephone,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyTelephoneParties',
        through: PartyTelephone,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(TelephoneNumber, {
        as: 'PartyTelephoneTelephoneNumbers',
        through: PartyTelephone,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'TelephoneNumberID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(TelephoneType, {
        as: 'PartyTelephoneTelephoneTypes',
        through: PartyTelephone,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'TelephoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyTrusteeTrustPartyRoles',
        through: PartyTrustee,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'TrustPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyTrusteeTrusteeParties',
        through: PartyTrustee,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'TrusteePartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PartyTrusteeCreatedByPartyRoles',
        through: PartyTrustee,
        foreignKey: 'TrustPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PartyTrusteeTrusteeParties',
        through: PartyTrustee,
        foreignKey: 'TrustPartyRoleID',
        otherKey: 'TrusteePartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(GenderType, {
        as: 'PersonGenderTypes',
        through: Person,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'GenderTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PersonLastUpdatedByPartyRoles',
        through: Person,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(MaritalStatusType, {
        as: 'PersonMaritalStatusTypes',
        through: Person,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'MaritalStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(ObjectColourType, {
        as: 'PersonMedicareCardColourTypes',
        through: Person,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'MedicareCardColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(NameChangeReasonType, {
        as: 'PersonNameChangeReasonTypes',
        through: Person,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'NameChangeReasonTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PersonParties',
        through: Person,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(ObjectColourType, {
        as: 'PersonPassportColourTypes',
        through: Person,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'PassportColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Country, {
        as: 'PersonPassportCountries',
        through: Person,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'PassportCountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(SalutationType, {
        as: 'PersonSalutationTypes',
        through: Person,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'SalutationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(State, {
        as: 'PersonStates',
        through: Person,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'PersonAccountantPartyRoles',
        through: Person,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(GenderType, {
        as: 'PersonGenderTypes',
        through: Person,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'GenderTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(MaritalStatusType, {
        as: 'PersonMaritalStatusTypes',
        through: Person,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'MaritalStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(ObjectColourType, {
        as: 'PersonMedicareCardColourTypes',
        through: Person,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'MedicareCardColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(NameChangeReasonType, {
        as: 'PersonNameChangeReasonTypes',
        through: Person,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'NameChangeReasonTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'PersonParties',
        through: Person,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(ObjectColourType, {
        as: 'PersonPassportColourTypes',
        through: Person,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'PassportColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Country, {
        as: 'PersonPassportCountries',
        through: Person,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'PassportCountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(SalutationType, {
        as: 'PersonSalutationTypes',
        through: Person,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'SalutationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(State, {
        as: 'PersonStates',
        through: Person,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestCreatedByPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'ApprovedByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestDeclinedByPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'ApprovedByPartyRoleID',
        otherKey: 'DeclinedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(RelationshipType, {
        as: 'RelationshipRequestRelationshipTypes',
        through: RelationshipRequest,
        foreignKey: 'ApprovedByPartyRoleID',
        otherKey: 'RelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestRequesteePartyRoles',
        through: RelationshipRequest,
        foreignKey: 'ApprovedByPartyRoleID',
        otherKey: 'RequesteePartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestRequestorPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'ApprovedByPartyRoleID',
        otherKey: 'RequestorPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestApprovedByPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'ApprovedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestDeclinedByPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'DeclinedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(RelationshipType, {
        as: 'RelationshipRequestRelationshipTypes',
        through: RelationshipRequest,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'RelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestRequesteePartyRoles',
        through: RelationshipRequest,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'RequesteePartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestRequestorPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'CreatedByPartyRoleID',
        otherKey: 'RequestorPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestApprovedByPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'DeclinedByPartyRoleID',
        otherKey: 'ApprovedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestCreatedByPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'DeclinedByPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(RelationshipType, {
        as: 'RelationshipRequestRelationshipTypes',
        through: RelationshipRequest,
        foreignKey: 'DeclinedByPartyRoleID',
        otherKey: 'RelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestRequesteePartyRoles',
        through: RelationshipRequest,
        foreignKey: 'DeclinedByPartyRoleID',
        otherKey: 'RequesteePartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestRequestorPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'DeclinedByPartyRoleID',
        otherKey: 'RequestorPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestApprovedByPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'RequesteePartyRoleID',
        otherKey: 'ApprovedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestCreatedByPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'RequesteePartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestDeclinedByPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'RequesteePartyRoleID',
        otherKey: 'DeclinedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(RelationshipType, {
        as: 'RelationshipRequestRelationshipTypes',
        through: RelationshipRequest,
        foreignKey: 'RequesteePartyRoleID',
        otherKey: 'RelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestRequestorPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'RequesteePartyRoleID',
        otherKey: 'RequestorPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestApprovedByPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'RequestorPartyRoleID',
        otherKey: 'ApprovedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestCreatedByPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'RequestorPartyRoleID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestDeclinedByPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'RequestorPartyRoleID',
        otherKey: 'DeclinedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(RelationshipType, {
        as: 'RelationshipRequestRelationshipTypes',
        through: RelationshipRequest,
        foreignKey: 'RequestorPartyRoleID',
        otherKey: 'RelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'RelationshipRequestRequesteePartyRoles',
        through: RelationshipRequest,
        foreignKey: 'RequestorPartyRoleID',
        otherKey: 'RequesteePartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Country, {
        as: 'TrustCountryEstablisheds',
        through: Trust,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'CountryEstablishedID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(DocumentationType, {
        as: 'TrustDocumentationTypes',
        through: Trust,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'DocumentationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'TrustLastUpdatedByPartyRoles',
        through: Trust,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'TrustParties',
        through: Trust,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(TrustPurposeType, {
        as: 'TrustTrustPurposeTypes',
        through: Trust,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'TrustPurposeTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(TrustStructureType, {
        as: 'TrustTrustStructureTypes',
        through: Trust,
        foreignKey: 'AccountantPartyRoleID',
        otherKey: 'TrustStructureTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(PartyRole, {
        as: 'TrustAccountantPartyRoles',
        through: Trust,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Country, {
        as: 'TrustCountryEstablisheds',
        through: Trust,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'CountryEstablishedID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(DocumentationType, {
        as: 'TrustDocumentationTypes',
        through: Trust,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'DocumentationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(Party, {
        as: 'TrustParties',
        through: Trust,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(TrustPurposeType, {
        as: 'TrustTrustPurposeTypes',
        through: Trust,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'TrustPurposeTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRole.belongsToMany(TrustStructureType, {
        as: 'TrustTrustStructureTypes',
        through: Trust,
        foreignKey: 'LastUpdatedByPartyRoleID',
        otherKey: 'TrustStructureTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
