'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'PartyType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyType = model.PartyType;
    var Party = model.Party;
    var PartyRoleType = model.PartyRoleType;
    var PartyTypePartyRoleType = model.PartyTypePartyRoleType;
    var PartyRole = model.PartyRole;
    var PartyRoleCategory = model.PartyRoleCategory;
    var PartyRoleGroup = model.PartyRoleGroup;
    var PartyRoleSuite = model.PartyRoleSuite;

    PartyType.hasMany(Party, {
        as: 'PartyPartytypeFks',
        foreignKey: 'PartyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyType.hasMany(PartyRoleType, {
        as: 'PartyRoleTypePartytypeFks',
        foreignKey: 'PartyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyType.hasMany(PartyTypePartyRoleType, {
        as: 'PartyRoleTypePartytypeFks',
        foreignKey: 'PartyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyType.belongsToMany(PartyRole, {
        as: 'PartyCreatedByPartyRoles',
        through: Party,
        foreignKey: 'PartyTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyType.belongsToMany(PartyRole, {
        as: 'PartyLastUpdatedByPartyRoles',
        through: Party,
        foreignKey: 'PartyTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyType.belongsToMany(PartyRoleCategory, {
        as: 'PartyRoleTypePartyRoleCategories',
        through: PartyRoleType,
        foreignKey: 'PartyTypeID',
        otherKey: 'PartyRoleCategoryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyType.belongsToMany(PartyRoleGroup, {
        as: 'PartyRoleTypePartyRoleGroups',
        through: PartyRoleType,
        foreignKey: 'PartyTypeID',
        otherKey: 'PartyRoleGroupID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyType.belongsToMany(PartyRoleSuite, {
        as: 'PartyRoleTypePartyRoleSuites',
        through: PartyRoleType,
        foreignKey: 'PartyTypeID',
        otherKey: 'PartyRoleSuiteID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyType.belongsToMany(PartyRoleType, {
        as: 'PartyTypePartyRoleTypePartyRoleTypes',
        through: PartyTypePartyRoleType,
        foreignKey: 'PartyTypeID',
        otherKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
