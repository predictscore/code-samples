'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('LocationType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'LocationType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var LocationType = model.LocationType;
    var PartyAddress = model.PartyAddress;
    var PartyEmail = model.PartyEmail;
    var PartyTelephone = model.PartyTelephone;
    var AddressType = model.AddressType;
    var Address = model.Address;
    var CommunicationType = model.CommunicationType;
    var PartyRole = model.PartyRole;
    var CurrencyType = model.CurrencyType;
    var Party = model.Party;
    var ResidencyType = model.ResidencyType;
    var EmailAddress = model.EmailAddress;
    var EmailType = model.EmailType;
    var TelephoneNumber = model.TelephoneNumber;
    var TelephoneType = model.TelephoneType;

    LocationType.hasMany(PartyAddress, {
        as: 'PartyAddressLocationtypeFks',
        foreignKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.hasMany(PartyEmail, {
        as: 'PartyEmailLocationtypeFks',
        foreignKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.hasMany(PartyTelephone, {
        as: 'PartyTelephoneLocationtypeFks',
        foreignKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(AddressType, {
        as: 'PartyAddressAddressTypes',
        through: PartyAddress,
        foreignKey: 'LocationTypeID',
        otherKey: 'AddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(Address, {
        as: 'PartyAddressAddresses',
        through: PartyAddress,
        foreignKey: 'LocationTypeID',
        otherKey: 'AddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(CommunicationType, {
        as: 'PartyAddressCommunicationTypes',
        through: PartyAddress,
        foreignKey: 'LocationTypeID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(PartyRole, {
        as: 'PartyAddressCreatedByPartyRoles',
        through: PartyAddress,
        foreignKey: 'LocationTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(CurrencyType, {
        as: 'PartyAddressCurrencyTypes',
        through: PartyAddress,
        foreignKey: 'LocationTypeID',
        otherKey: 'CurrencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(PartyRole, {
        as: 'PartyAddressLastUpdatedByPartyRoles',
        through: PartyAddress,
        foreignKey: 'LocationTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(Party, {
        as: 'PartyAddressParties',
        through: PartyAddress,
        foreignKey: 'LocationTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(ResidencyType, {
        as: 'PartyAddressResidencyTypes',
        through: PartyAddress,
        foreignKey: 'LocationTypeID',
        otherKey: 'ResidencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(CommunicationType, {
        as: 'PartyEmailCommunicationTypes',
        through: PartyEmail,
        foreignKey: 'LocationTypeID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(PartyRole, {
        as: 'PartyEmailCreatedByPartyRoles',
        through: PartyEmail,
        foreignKey: 'LocationTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(EmailAddress, {
        as: 'PartyEmailEmailAddresses',
        through: PartyEmail,
        foreignKey: 'LocationTypeID',
        otherKey: 'EmailAddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(EmailType, {
        as: 'PartyEmailEmailTypes',
        through: PartyEmail,
        foreignKey: 'LocationTypeID',
        otherKey: 'EmailTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(PartyRole, {
        as: 'PartyEmailLastUpdatedByPartyRoles',
        through: PartyEmail,
        foreignKey: 'LocationTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(Party, {
        as: 'PartyEmailParties',
        through: PartyEmail,
        foreignKey: 'LocationTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(CommunicationType, {
        as: 'PartyTelephoneCommunicationTypes',
        through: PartyTelephone,
        foreignKey: 'LocationTypeID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(PartyRole, {
        as: 'PartyTelephoneCreatedByPartyRoles',
        through: PartyTelephone,
        foreignKey: 'LocationTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(PartyRole, {
        as: 'PartyTelephoneLastUpdatedByPartyRoles',
        through: PartyTelephone,
        foreignKey: 'LocationTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(Party, {
        as: 'PartyTelephoneParties',
        through: PartyTelephone,
        foreignKey: 'LocationTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(TelephoneNumber, {
        as: 'PartyTelephoneTelephoneNumbers',
        through: PartyTelephone,
        foreignKey: 'LocationTypeID',
        otherKey: 'TelephoneNumberID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LocationType.belongsToMany(TelephoneType, {
        as: 'PartyTelephoneTelephoneTypes',
        through: PartyTelephone,
        foreignKey: 'LocationTypeID',
        otherKey: 'TelephoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
