'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyRoleSuite', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'PartyRoleSuite',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyRoleSuite = model.PartyRoleSuite;
    var PartyRoleType = model.PartyRoleType;
    var PartyRoleCategory = model.PartyRoleCategory;
    var PartyRoleGroup = model.PartyRoleGroup;
    var PartyType = model.PartyType;

    PartyRoleSuite.hasMany(PartyRoleType, {
        as: 'PartyRoleTypePartyrolesuiteFks',
        foreignKey: 'PartyRoleSuiteID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleSuite.belongsToMany(PartyRoleCategory, {
        as: 'PartyRoleTypePartyRoleCategories',
        through: PartyRoleType,
        foreignKey: 'PartyRoleSuiteID',
        otherKey: 'PartyRoleCategoryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleSuite.belongsToMany(PartyRoleGroup, {
        as: 'PartyRoleTypePartyRoleGroups',
        through: PartyRoleType,
        foreignKey: 'PartyRoleSuiteID',
        otherKey: 'PartyRoleGroupID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleSuite.belongsToMany(PartyType, {
        as: 'PartyRoleTypePartyTypes',
        through: PartyRoleType,
        foreignKey: 'PartyRoleSuiteID',
        otherKey: 'PartyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
