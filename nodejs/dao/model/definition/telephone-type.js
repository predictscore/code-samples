'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('TelephoneType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: true
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'TelephoneType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var TelephoneType = model.TelephoneType;
    var PartyTelephone = model.PartyTelephone;
    var CommunicationType = model.CommunicationType;
    var PartyRole = model.PartyRole;
    var LocationType = model.LocationType;
    var Party = model.Party;
    var TelephoneNumber = model.TelephoneNumber;

    TelephoneType.hasMany(PartyTelephone, {
        as: 'PartyTelephoneTelephonetypeFks',
        foreignKey: 'TelephoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TelephoneType.belongsToMany(CommunicationType, {
        as: 'PartyTelephoneCommunicationTypes',
        through: PartyTelephone,
        foreignKey: 'TelephoneTypeID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TelephoneType.belongsToMany(PartyRole, {
        as: 'PartyTelephoneCreatedByPartyRoles',
        through: PartyTelephone,
        foreignKey: 'TelephoneTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TelephoneType.belongsToMany(PartyRole, {
        as: 'PartyTelephoneLastUpdatedByPartyRoles',
        through: PartyTelephone,
        foreignKey: 'TelephoneTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TelephoneType.belongsToMany(LocationType, {
        as: 'PartyTelephoneLocationTypes',
        through: PartyTelephone,
        foreignKey: 'TelephoneTypeID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TelephoneType.belongsToMany(Party, {
        as: 'PartyTelephoneParties',
        through: PartyTelephone,
        foreignKey: 'TelephoneTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TelephoneType.belongsToMany(TelephoneNumber, {
        as: 'PartyTelephoneTelephoneNumbers',
        through: PartyTelephone,
        foreignKey: 'TelephoneTypeID',
        otherKey: 'TelephoneNumberID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
