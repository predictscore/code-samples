'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('V8Creditreportrequired', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false
        },
        applicationPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'ApplicationPartyRoleID',
            allowNull: false
        },
        vedaBureauReference: {
            type: DataTypes.STRING(50),
            field: 'VedaBureauReference',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'v8_CreditReportRequired',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var V8Creditreportrequired = model.V8Creditreportrequired;

};
