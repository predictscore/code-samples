'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportProfileType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(150),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportProfileType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportProfileType = model.IdentityVerificationReportProfileType;
    var IdentityVerificationReportMain = model.IdentityVerificationReportMain;
    var IdentityVerificationReport = model.IdentityVerificationReport;
    var IdentityVerificationReportOutcomeType = model.IdentityVerificationReportOutcomeType;

    IdentityVerificationReportProfileType.hasMany(IdentityVerificationReportMain, {
        as: 'FKIdentityverificationreportmainIdentityverificationreportpros',
        foreignKey: 'ProfileTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportProfileType.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportMainIdentityVerificationReports',
        through: IdentityVerificationReportMain,
        foreignKey: 'ProfileTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportProfileType.belongsToMany(IdentityVerificationReportOutcomeType, {
        as: 'IdentityVerificationReportMainOverallOutcomes',
        through: IdentityVerificationReportMain,
        foreignKey: 'ProfileTypeID',
        otherKey: 'OverallOutcomeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
