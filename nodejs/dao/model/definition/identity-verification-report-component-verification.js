'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportComponentVerification', {
        identityVerificationReportID: {
            type: DataTypes.INTEGER,
            field: 'IdentityVerificationReportID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'IdentityVerificationReport',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        indicatorTypeID: {
            type: DataTypes.INTEGER,
            field: 'IndicatorTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportOutcomeType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        totalPoints: {
            type: DataTypes.DECIMAL(18, 4),
            field: 'TotalPoints',
            allowNull: true
        },
        selfVerificationUri: {
            type: DataTypes.STRING(500),
            field: 'SelfVerificationUri',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportComponentVerification',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportComponentVerification = model.IdentityVerificationReportComponentVerification;
    var IdentityVerificationReportOutcomeType = model.IdentityVerificationReportOutcomeType;
    var IdentityVerificationReport = model.IdentityVerificationReport;

    IdentityVerificationReportComponentVerification.belongsTo(IdentityVerificationReportOutcomeType, {
        as: 'IndicatorType',
        foreignKey: 'IndicatorTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentVerification.belongsTo(IdentityVerificationReport, {
        as: 'IdentityVerificationReport',
        foreignKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
