'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('CRMBrokerContact', {
        parentCustomerID: {
            type: DataTypes.UUID,
            field: 'ParentCustomerID',
            allowNull: false
        },
        contactID: {
            type: DataTypes.UUID,
            field: 'ContactID',
            allowNull: false
        },
        eMailAddress: {
            type: DataTypes.STRING(500),
            field: 'EMailAddress',
            allowNull: true
        },
        firstName: {
            type: DataTypes.STRING(500),
            field: 'FirstName',
            allowNull: true
        },
        lastName: {
            type: DataTypes.STRING(500),
            field: 'LastName',
            allowNull: true
        },
        ownerID: {
            type: DataTypes.UUID,
            field: 'OwnerID',
            allowNull: true
        },
        mobilePhone: {
            type: DataTypes.STRING(500),
            field: 'MobilePhone',
            allowNull: true
        },
        telephone: {
            type: DataTypes.STRING(500),
            field: 'Telephone',
            allowNull: true
        },
        fax: {
            type: DataTypes.STRING(500),
            field: 'Fax',
            allowNull: true
        },
        jobTitle: {
            type: DataTypes.STRING(500),
            field: 'JobTitle',
            allowNull: true
        },
        importStatus: {
            type: DataTypes.INTEGER,
            field: 'ImportStatus',
            allowNull: true
        },
        treatAsContactID: {
            type: DataTypes.UUID,
            field: 'TreatAsContactID',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'CRMBrokerContacts',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var CRMBrokerContact = model.CRMBrokerContact;

};
