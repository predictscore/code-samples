'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ABSBrokerCompany', {
        companyName: {
            type: DataTypes.STRING(150),
            field: 'CompanyName',
            allowNull: false
        },
        companyPartyID: {
            type: DataTypes.INTEGER,
            field: 'CompanyPartyID',
            allowNull: true
        },
        companyPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CompanyPartyRoleID',
            allowNull: true
        },
        companyPartyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'CompanyPartyRoleTypeID',
            allowNull: true
        },
        aCN: {
            type: DataTypes.STRING(15),
            field: 'ACN',
            allowNull: true
        },
        aBN: {
            type: DataTypes.STRING(15),
            field: 'ABN',
            allowNull: true
        },
        primaryContactName: {
            type: DataTypes.STRING(150),
            field: 'PrimaryContactName',
            allowNull: false
        },
        primaryContactPartyID: {
            type: DataTypes.INTEGER,
            field: 'PrimaryContactPartyID',
            allowNull: true
        },
        accountRankingTypeID: {
            type: DataTypes.INTEGER,
            field: 'AccountRankingTypeID',
            allowNull: true
        },
        accreditationStatusTypeID: {
            type: DataTypes.INTEGER,
            field: 'AccreditationStatusTypeID',
            allowNull: true
        },
        prospectSource: {
            type: DataTypes.STRING(50),
            field: 'ProspectSource',
            allowNull: true
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: true
        },
        isPrimaryContact: {
            type: DataTypes.BOOLEAN,
            field: 'IsPrimaryContact',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'ABSBrokerCompany',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var ABSBrokerCompany = model.ABSBrokerCompany;

};
