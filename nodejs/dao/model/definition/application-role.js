'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ApplicationRole', {
        applicationTypeID: {
            type: DataTypes.INTEGER,
            field: 'ApplicationTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'ApplicationType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        loginID: {
            type: DataTypes.INTEGER,
            field: 'LoginID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'Login',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        roleTypeID: {
            type: DataTypes.INTEGER,
            field: 'RoleTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'RoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'ApplicationRole',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var ApplicationRole = model.ApplicationRole;
    var ApplicationType = model.ApplicationType;
    var Login = model.Login;
    var RoleType = model.RoleType;

    ApplicationRole.belongsTo(ApplicationType, {
        as: 'ApplicationType',
        foreignKey: 'ApplicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ApplicationRole.belongsTo(Login, {
        as: 'Login',
        foreignKey: 'LoginID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ApplicationRole.belongsTo(RoleType, {
        as: 'RoleType',
        foreignKey: 'RoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
