'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('RetailCustomerAddress', {
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: true
        },
        partyTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyTypeID',
            allowNull: true
        },
        partyRoleID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleID',
            allowNull: true
        },
        optOut: {
            type: DataTypes.BOOLEAN,
            field: 'OptOut',
            allowNull: true
        },
        partyAddressID: {
            type: DataTypes.INTEGER,
            field: 'PartyAddressID',
            allowNull: true
        },
        addressID: {
            type: DataTypes.INTEGER,
            field: 'AddressID',
            allowNull: true
        },
        addressTypeID: {
            type: DataTypes.INTEGER,
            field: 'AddressTypeID',
            allowNull: true
        },
        addressText: {
            type: DataTypes.STRING(500),
            field: 'AddressText',
            allowNull: true
        },
        countryID: {
            type: DataTypes.INTEGER,
            field: 'CountryID',
            allowNull: true
        },
        pOBox: {
            type: DataTypes.STRING(30),
            field: 'POBox',
            allowNull: true
        },
        postCode: {
            type: DataTypes.STRING(10),
            field: 'PostCode',
            allowNull: true
        },
        priority: {
            type: DataTypes.INTEGER,
            field: 'Priority',
            allowNull: true
        },
        propertyName: {
            type: DataTypes.STRING(100),
            field: 'PropertyName',
            allowNull: true
        },
        stateID: {
            type: DataTypes.INTEGER,
            field: 'StateID',
            allowNull: true
        },
        streetName: {
            type: DataTypes.STRING(100),
            field: 'StreetName',
            allowNull: true
        },
        streetNumber: {
            type: DataTypes.STRING(100),
            field: 'StreetNumber',
            allowNull: true
        },
        streetTypeID: {
            type: DataTypes.INTEGER,
            field: 'StreetTypeID',
            allowNull: true
        },
        suburb: {
            type: DataTypes.STRING(100),
            field: 'Suburb',
            allowNull: true
        },
        unitNumber: {
            type: DataTypes.STRING(100),
            field: 'UnitNumber',
            allowNull: true
        },
        rPDataPropertyID: {
            type: DataTypes.INTEGER,
            field: 'RPDataPropertyID',
            allowNull: true
        },
        retailID: {
            type: DataTypes.INTEGER,
            field: 'RetailID',
            allowNull: true
        },
        iSOCountryCode: {
            type: DataTypes.STRING(50),
            field: 'ISOCountryCode',
            allowNull: true
        },
        countryName: {
            type: DataTypes.STRING(150),
            field: 'CountryName',
            allowNull: true
        },
        streetTypeName: {
            type: DataTypes.STRING(50),
            field: 'StreetTypeName',
            allowNull: true
        },
        stateName: {
            type: DataTypes.STRING(150),
            field: 'StateName',
            allowNull: true
        },
        periodOfOccupancyMonths: {
            type: DataTypes.INTEGER,
            field: 'PeriodOfOccupancyMonths',
            allowNull: true
        },
        periodOfOccupancyYears: {
            type: DataTypes.INTEGER,
            field: 'PeriodOfOccupancyYears',
            allowNull: true
        },
        communicationTypeID: {
            type: DataTypes.INTEGER,
            field: 'CommunicationTypeID',
            allowNull: true
        },
        communicationTypeName: {
            type: DataTypes.STRING(200),
            field: 'CommunicationTypeName',
            allowNull: true
        },
        locationTypeName: {
            type: DataTypes.STRING(50),
            field: 'LocationTypeName',
            allowNull: true
        },
        addressTypeName: {
            type: DataTypes.STRING(50),
            field: 'AddressTypeName',
            allowNull: true
        },
        currencyTypeID: {
            type: DataTypes.INTEGER,
            field: 'CurrencyTypeID',
            allowNull: true
        },
        currencyTypeName: {
            type: DataTypes.STRING(50),
            field: 'CurrencyTypeName',
            allowNull: true
        },
        claimLeadID: {
            type: DataTypes.INTEGER,
            field: 'ClaimLeadID',
            allowNull: true
        },
        claimCustomerID: {
            type: DataTypes.INTEGER,
            field: 'ClaimCustomerID',
            allowNull: true
        },
        claimPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'ClaimPartyRoleID',
            allowNull: true
        },
        loginID: {
            type: DataTypes.INTEGER,
            field: 'LoginID',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'RetailCustomerAddresses',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var RetailCustomerAddress = model.RetailCustomerAddress;

};
