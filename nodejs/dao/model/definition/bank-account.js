'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('BankAccount', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        bankBSBID: {
            type: DataTypes.INTEGER,
            field: 'BankBSBID',
            allowNull: false,
            references: {
                model: 'BankBSB',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        accountName: {
            type: DataTypes.STRING(250),
            field: 'AccountName',
            allowNull: true
        },
        accountNumber: {
            type: DataTypes.STRING(50),
            field: 'AccountNumber',
            allowNull: false
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        lastUpdated: {
            type: DataTypes.DATE,
            field: 'LastUpdated',
            allowNull: false
        },
        lastUpdatedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'LastUpdatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        tontoID: {
            type: DataTypes.INTEGER,
            field: 'TontoID',
            allowNull: true
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'BankAccount',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var BankAccount = model.BankAccount;
    var PartyBankAccount = model.PartyBankAccount;
    var BankBSB = model.BankBSB;
    var PartyRole = model.PartyRole;
    var PartyBankAccountType = model.PartyBankAccountType;
    var Party = model.Party;

    BankAccount.hasMany(PartyBankAccount, {
        as: 'PartyBankAccountBankaccountFks',
        foreignKey: 'BankAccountID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankAccount.belongsTo(BankBSB, {
        as: 'BankBSB',
        foreignKey: 'BankBSBID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankAccount.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankAccount.belongsTo(PartyRole, {
        as: 'LastUpdatedByPartyRole',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankAccount.belongsToMany(BankBSB, {
        as: 'PartyBankAccountBankBSBs',
        through: PartyBankAccount,
        foreignKey: 'BankAccountID',
        otherKey: 'BankBSBID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankAccount.belongsToMany(PartyRole, {
        as: 'PartyBankAccountCreatedByPartyRoles',
        through: PartyBankAccount,
        foreignKey: 'BankAccountID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankAccount.belongsToMany(PartyRole, {
        as: 'PartyBankAccountLastUpdateByPartyRoles',
        through: PartyBankAccount,
        foreignKey: 'BankAccountID',
        otherKey: 'LastUpdateByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankAccount.belongsToMany(PartyBankAccountType, {
        as: 'PartyBankAccountPartyBankAccountTypes',
        through: PartyBankAccount,
        foreignKey: 'BankAccountID',
        otherKey: 'PartyBankAccountTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankAccount.belongsToMany(Party, {
        as: 'PartyBankAccountParties',
        through: PartyBankAccount,
        foreignKey: 'BankAccountID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
