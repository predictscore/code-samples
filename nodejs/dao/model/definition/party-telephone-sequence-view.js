'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyTelephoneSequenceView', {
        a: {
            type: DataTypes.BIGINT,
            field: 'a',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'PartyTelephoneSequenceView',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyTelephoneSequenceView = model.PartyTelephoneSequenceView;

};
