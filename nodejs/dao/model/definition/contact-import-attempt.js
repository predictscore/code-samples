'use strict';

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ContactImportAttempt', {
    iD: {
      type: DataTypes.INTEGER,
      field: 'ID',
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    fileID: {
      type: DataTypes.INTEGER,
      field: 'FileID',
      allowNull: false,
    },
    success: {
      type: DataTypes.BOOLEAN,
      field: 'Success',
      allowNull: true
    },
    numContactsAdded: {
      type: DataTypes.INTEGER,
      field: 'NumContactsAdded',
      allowNull: false,
    },
    createdByPartyRoleID: {
      type: DataTypes.INTEGER,
      field: 'CreatedByPartyRoleID',
      allowNull: false,
      references: {
        model: 'PartyRole',
        key: 'ID'
      },
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION'
    }
  }, {
    schema: 'public',
    tableName: 'ContactImportAttempt',
    timestamps: false
  });
};

module.exports.initRelations = function() {
  delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
  var model = require('../index');
  var ContactImportAttempt = model.ContactImportAttempt;
  var PartyRole = model.PartyRole;

  ContactImportAttempt.belongsTo(PartyRole, {
    as: 'CreatedByPartyRole',
    foreignKey: 'CreatedByPartyRoleID',
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

};
