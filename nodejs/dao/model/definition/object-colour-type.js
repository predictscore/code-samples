'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ObjectColourType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(150),
            field: 'Name',
            allowNull: false
        },
        description: {
            type: DataTypes.STRING(500),
            field: 'Description',
            allowNull: false
        },
        objectTypeID: {
            type: DataTypes.INTEGER,
            field: 'ObjectTypeID',
            allowNull: false,
            references: {
                model: 'ObjectType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: true
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        },
        vedaCode: {
            type: DataTypes.STRING(10),
            field: 'VedaCode',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'ObjectColourType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var ObjectColourType = model.ObjectColourType;
    var Person = model.Person;
    var ObjectType = model.ObjectType;
    var PartyRole = model.PartyRole;
    var GenderType = model.GenderType;
    var MaritalStatusType = model.MaritalStatusType;
    var NameChangeReasonType = model.NameChangeReasonType;
    var Party = model.Party;
    var Country = model.Country;
    var SalutationType = model.SalutationType;
    var State = model.State;

    ObjectColourType.hasMany(Person, {
        as: 'PersonMedicarecardcolourtypeFks',
        foreignKey: 'MedicareCardColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.hasMany(Person, {
        as: 'PersonPassportcolourtypeFks',
        foreignKey: 'PassportColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsTo(ObjectType, {
        as: 'ObjectType',
        foreignKey: 'ObjectTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(PartyRole, {
        as: 'PersonAccountantPartyRoles',
        through: Person,
        foreignKey: 'MedicareCardColourTypeID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(GenderType, {
        as: 'PersonGenderTypes',
        through: Person,
        foreignKey: 'MedicareCardColourTypeID',
        otherKey: 'GenderTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(PartyRole, {
        as: 'PersonLastUpdatedByPartyRoles',
        through: Person,
        foreignKey: 'MedicareCardColourTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(MaritalStatusType, {
        as: 'PersonMaritalStatusTypes',
        through: Person,
        foreignKey: 'MedicareCardColourTypeID',
        otherKey: 'MaritalStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(NameChangeReasonType, {
        as: 'PersonNameChangeReasonTypes',
        through: Person,
        foreignKey: 'MedicareCardColourTypeID',
        otherKey: 'NameChangeReasonTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(Party, {
        as: 'PersonParties',
        through: Person,
        foreignKey: 'MedicareCardColourTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(ObjectColourType, {
        as: 'PersonPassportColourTypes',
        through: Person,
        foreignKey: 'MedicareCardColourTypeID',
        otherKey: 'PassportColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(Country, {
        as: 'PersonPassportCountries',
        through: Person,
        foreignKey: 'MedicareCardColourTypeID',
        otherKey: 'PassportCountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(SalutationType, {
        as: 'PersonSalutationTypes',
        through: Person,
        foreignKey: 'MedicareCardColourTypeID',
        otherKey: 'SalutationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(State, {
        as: 'PersonStates',
        through: Person,
        foreignKey: 'MedicareCardColourTypeID',
        otherKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(PartyRole, {
        as: 'PersonAccountantPartyRoles',
        through: Person,
        foreignKey: 'PassportColourTypeID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(GenderType, {
        as: 'PersonGenderTypes',
        through: Person,
        foreignKey: 'PassportColourTypeID',
        otherKey: 'GenderTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(PartyRole, {
        as: 'PersonLastUpdatedByPartyRoles',
        through: Person,
        foreignKey: 'PassportColourTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(MaritalStatusType, {
        as: 'PersonMaritalStatusTypes',
        through: Person,
        foreignKey: 'PassportColourTypeID',
        otherKey: 'MaritalStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(ObjectColourType, {
        as: 'PersonMedicareCardColourTypes',
        through: Person,
        foreignKey: 'PassportColourTypeID',
        otherKey: 'MedicareCardColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(NameChangeReasonType, {
        as: 'PersonNameChangeReasonTypes',
        through: Person,
        foreignKey: 'PassportColourTypeID',
        otherKey: 'NameChangeReasonTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(Party, {
        as: 'PersonParties',
        through: Person,
        foreignKey: 'PassportColourTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(Country, {
        as: 'PersonPassportCountries',
        through: Person,
        foreignKey: 'PassportColourTypeID',
        otherKey: 'PassportCountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(SalutationType, {
        as: 'PersonSalutationTypes',
        through: Person,
        foreignKey: 'PassportColourTypeID',
        otherKey: 'SalutationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ObjectColourType.belongsToMany(State, {
        as: 'PersonStates',
        through: Person,
        foreignKey: 'PassportColourTypeID',
        otherKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
