'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('TelephoneNumber', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        telephoneNumber: {
            type: DataTypes.STRING(20),
            field: 'TelephoneNumber',
            allowNull: false
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'TelephoneNumber',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var TelephoneNumber = model.TelephoneNumber;
    var PartyTelephone = model.PartyTelephone;
    var PartyRole = model.PartyRole;
    var CommunicationType = model.CommunicationType;
    var LocationType = model.LocationType;
    var Party = model.Party;
    var TelephoneType = model.TelephoneType;

    TelephoneNumber.hasMany(PartyTelephone, {
        as: 'PartyTelephoneTelephonenumberFks',
        foreignKey: 'TelephoneNumberID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TelephoneNumber.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TelephoneNumber.belongsToMany(CommunicationType, {
        as: 'PartyTelephoneCommunicationTypes',
        through: PartyTelephone,
        foreignKey: 'TelephoneNumberID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TelephoneNumber.belongsToMany(PartyRole, {
        as: 'PartyTelephoneCreatedByPartyRoles',
        through: PartyTelephone,
        foreignKey: 'TelephoneNumberID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TelephoneNumber.belongsToMany(PartyRole, {
        as: 'PartyTelephoneLastUpdatedByPartyRoles',
        through: PartyTelephone,
        foreignKey: 'TelephoneNumberID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TelephoneNumber.belongsToMany(LocationType, {
        as: 'PartyTelephoneLocationTypes',
        through: PartyTelephone,
        foreignKey: 'TelephoneNumberID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TelephoneNumber.belongsToMany(Party, {
        as: 'PartyTelephoneParties',
        through: PartyTelephone,
        foreignKey: 'TelephoneNumberID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TelephoneNumber.belongsToMany(TelephoneType, {
        as: 'PartyTelephoneTelephoneTypes',
        through: PartyTelephone,
        foreignKey: 'TelephoneNumberID',
        otherKey: 'TelephoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
