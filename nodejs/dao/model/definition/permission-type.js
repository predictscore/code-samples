'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PermissionType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'PermissionType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PermissionType = model.PermissionType;
    var LoginPermissionType = model.LoginPermissionType;
    var RoleTypePermissionType = model.RoleTypePermissionType;
    var ApplicationType = model.ApplicationType;
    var Login = model.Login;
    var RoleType = model.RoleType;

    PermissionType.hasMany(LoginPermissionType, {
        as: 'LoginPermissionTypePermissiontypeFks',
        foreignKey: 'PermissionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PermissionType.hasMany(RoleTypePermissionType, {
        as: 'RoleTypePermissionTypePermissiontypeFks',
        foreignKey: 'PermissionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PermissionType.belongsToMany(ApplicationType, {
        as: 'LoginPermissionTypeApplicationTypes',
        through: LoginPermissionType,
        foreignKey: 'PermissionTypeID',
        otherKey: 'ApplicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PermissionType.belongsToMany(Login, {
        as: 'LoginPermissionTypeLogins',
        through: LoginPermissionType,
        foreignKey: 'PermissionTypeID',
        otherKey: 'LoginID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PermissionType.belongsToMany(RoleType, {
        as: 'RoleTypePermissionTypeRoleTypes',
        through: RoleTypePermissionType,
        foreignKey: 'PermissionTypeID',
        otherKey: 'RoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
