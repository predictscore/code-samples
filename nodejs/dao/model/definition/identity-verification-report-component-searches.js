'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportComponentSearch', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        identityVerificationReportID: {
            type: DataTypes.INTEGER,
            field: 'IdentityVerificationReportID',
            allowNull: false,
            references: {
                model: 'IdentityVerificationReport',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        nameTypeID: {
            type: DataTypes.INTEGER,
            field: 'NameTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentSearchNameType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        searchTypeID: {
            type: DataTypes.INTEGER,
            field: 'SearchTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentSearchType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        resultCodeTypeID: {
            type: DataTypes.INTEGER,
            field: 'ResultCodeTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentSearchResultCodeType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        detail: {
            type: DataTypes.STRING(500),
            field: 'Detail',
            allowNull: true
        },
        score: {
            type: DataTypes.STRING(500),
            field: 'Score',
            allowNull: true
        },
        matchIndicatorTypeID: {
            type: DataTypes.INTEGER,
            field: 'MatchIndicatorTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentSearchMatchIndicatorType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportComponentSearches',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportComponentSearch = model.IdentityVerificationReportComponentSearch;
    var IdentityVerificationReportComponentSearchValue = model.IdentityVerificationReportComponentSearchValue;
    var IdentityVerificationReportComponentSearchMatchIndicatorType = model.IdentityVerificationReportComponentSearchMatchIndicatorType;
    var IdentityVerificationReportComponentSearchNameType = model.IdentityVerificationReportComponentSearchNameType;
    var IdentityVerificationReportComponentSearchResultCodeType = model.IdentityVerificationReportComponentSearchResultCodeType;
    var IdentityVerificationReportComponentSearchType = model.IdentityVerificationReportComponentSearchType;
    var IdentityVerificationReport = model.IdentityVerificationReport;
    var IdentityVerificationReportComponentSearchSubCategoryType = model.IdentityVerificationReportComponentSearchSubCategoryType;
    var IdentityVerificationReportComponentSearchCategoryType = model.IdentityVerificationReportComponentSearchCategoryType;

    IdentityVerificationReportComponentSearch.hasMany(IdentityVerificationReportComponentSearchValue, {
        as: 'FKIdentityverificationreportcomponentsearchvaluesIdentityver1s',
        foreignKey: 'SearchID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearch.belongsTo(IdentityVerificationReportComponentSearchMatchIndicatorType, {
        as: 'MatchIndicatorType',
        foreignKey: 'MatchIndicatorTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearch.belongsTo(IdentityVerificationReportComponentSearchNameType, {
        as: 'NameType',
        foreignKey: 'NameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearch.belongsTo(IdentityVerificationReportComponentSearchResultCodeType, {
        as: 'ResultCodeType',
        foreignKey: 'ResultCodeTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearch.belongsTo(IdentityVerificationReportComponentSearchType, {
        as: 'SearchType',
        foreignKey: 'SearchTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearch.belongsTo(IdentityVerificationReport, {
        as: 'IdentityVerificationReport',
        foreignKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearch.belongsToMany(IdentityVerificationReportComponentSearchMatchIndicatorType, {
        as: 'IdentityVerificationReportComponentSearchValueMatchIndicatorTypes',
        through: IdentityVerificationReportComponentSearchValue,
        foreignKey: 'SearchID',
        otherKey: 'MatchIndicatorTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearch.belongsToMany(IdentityVerificationReportComponentSearchSubCategoryType, {
        as: 'IdentityVerificationReportComponentSearchValueSubCategoryTypes',
        through: IdentityVerificationReportComponentSearchValue,
        foreignKey: 'SearchID',
        otherKey: 'SubCategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearch.belongsToMany(IdentityVerificationReportComponentSearchCategoryType, {
        as: 'IdentityVerificationReportComponentSearchValueCategoryTypes',
        through: IdentityVerificationReportComponentSearchValue,
        foreignKey: 'SearchID',
        otherKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
