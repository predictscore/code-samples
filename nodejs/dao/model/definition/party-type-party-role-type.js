'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyTypePartyRoleType', {
        partyTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'PartyType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        partyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'PartyRoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'PartyTypePartyRoleType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyTypePartyRoleType = model.PartyTypePartyRoleType;
    var PartyRoleType = model.PartyRoleType;
    var PartyType = model.PartyType;

    PartyTypePartyRoleType.belongsTo(PartyRoleType, {
        as: 'PartyRoleType',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyTypePartyRoleType.belongsTo(PartyType, {
        as: 'PartyType',
        foreignKey: 'PartyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
