'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('SupplierPartyRoleTypeRelationship', {
        partyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'PartyRoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'SupplierPartyRoleTypeRelationship',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var SupplierPartyRoleTypeRelationship = model.SupplierPartyRoleTypeRelationship;
    var PartyRoleType = model.PartyRoleType;

    SupplierPartyRoleTypeRelationship.belongsTo(PartyRoleType, {
        as: 'PartyRoleType',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
