'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PresenceType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(512),
            field: 'Name',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'PresenceType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PresenceType = model.PresenceType;
    var PartyPresence = model.PartyPresence;
    var Party = model.Party;

    PresenceType.hasMany(PartyPresence, {
        as: 'PartyPresencePresencetypeFks',
        foreignKey: 'PresenceTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PresenceType.belongsToMany(Party, {
        as: 'PartyPresenceParties',
        through: PartyPresence,
        foreignKey: 'PresenceTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
