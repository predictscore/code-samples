'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportMain', {
        identityVerificationReportID: {
            type: DataTypes.INTEGER,
            field: 'IdentityVerificationReportID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'IdentityVerificationReport',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        overallOutcomeID: {
            type: DataTypes.INTEGER,
            field: 'OverallOutcomeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportOutcomeType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        clientReference: {
            type: DataTypes.STRING(100),
            field: 'ClientReference',
            allowNull: true
        },
        enquiryID: {
            type: DataTypes.STRING(50),
            field: 'EnquiryID',
            allowNull: true
        },
        profileTypeID: {
            type: DataTypes.INTEGER,
            field: 'ProfileTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportProfileType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        profileVersion: {
            type: DataTypes.INTEGER,
            field: 'ProfileVersion',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportMain',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportMain = model.IdentityVerificationReportMain;
    var IdentityVerificationReport = model.IdentityVerificationReport;
    var IdentityVerificationReportOutcomeType = model.IdentityVerificationReportOutcomeType;
    var IdentityVerificationReportProfileType = model.IdentityVerificationReportProfileType;

    IdentityVerificationReportMain.belongsTo(IdentityVerificationReport, {
        as: 'IdentityVerificationReport',
        foreignKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportMain.belongsTo(IdentityVerificationReportOutcomeType, {
        as: 'OverallOutcome',
        foreignKey: 'OverallOutcomeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportMain.belongsTo(IdentityVerificationReportProfileType, {
        as: 'ProfileType',
        foreignKey: 'ProfileTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
