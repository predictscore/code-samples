'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('State', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        countryID: {
            type: DataTypes.INTEGER,
            field: 'CountryID',
            allowNull: false,
            references: {
                model: 'Country',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        iSOStateCode: {
            type: DataTypes.STRING(15),
            field: 'ISOStateCode',
            allowNull: false
        },
        stateCode: {
            type: DataTypes.STRING(50),
            field: 'StateCode',
            allowNull: false
        },
        name: {
            type: DataTypes.STRING(150),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: true
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        },
        retailID: {
            type: DataTypes.INTEGER,
            field: 'RetailID',
            allowNull: true
        },
        v8ID: {
            type: DataTypes.INTEGER,
            field: 'V8ID',
            allowNull: true
        },
        vedaCode: {
            type: DataTypes.STRING(3),
            field: 'VedaCode',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'State',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var State = model.State;
    var Address = model.Address;
    var Company = model.Company;
    var Person = model.Person;
    var Suburb = model.Suburb;
    var Country = model.Country;
    var PartyRole = model.PartyRole;
    var StreetType = model.StreetType;
    var AccountRankingType = model.AccountRankingType;
    var AccreditationStatusType = model.AccreditationStatusType;
    var CompanyType = model.CompanyType;
    var IndustryType = model.IndustryType;
    var MainBusinessActivityType = model.MainBusinessActivityType;
    var Party = model.Party;
    var GenderType = model.GenderType;
    var MaritalStatusType = model.MaritalStatusType;
    var ObjectColourType = model.ObjectColourType;
    var NameChangeReasonType = model.NameChangeReasonType;
    var SalutationType = model.SalutationType;

    State.hasMany(Address, {
        as: 'AddressStateFks',
        foreignKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.hasMany(Company, {
        as: 'CompanyRegisteredinstateFks',
        foreignKey: 'RegisteredInStateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.hasMany(Person, {
        as: 'PersonStateFks',
        foreignKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.hasMany(Suburb, {
        as: 'SuburbFks',
        foreignKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsTo(Country, {
        as: 'Country',
        foreignKey: 'CountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(Country, {
        as: 'AddressCountries',
        through: Address,
        foreignKey: 'StateID',
        otherKey: 'CountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(PartyRole, {
        as: 'AddressCreatedByPartyRoles',
        through: Address,
        foreignKey: 'StateID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(PartyRole, {
        as: 'AddressLastUpdatedByPartyRoles',
        through: Address,
        foreignKey: 'StateID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(StreetType, {
        as: 'AddressStreetTypes',
        through: Address,
        foreignKey: 'StateID',
        otherKey: 'StreetTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(AccountRankingType, {
        as: 'CompanyAccountRankingTypes',
        through: Company,
        foreignKey: 'RegisteredInStateID',
        otherKey: 'AccountRankingTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(PartyRole, {
        as: 'CompanyAccountantPartyRoles',
        through: Company,
        foreignKey: 'RegisteredInStateID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(AccreditationStatusType, {
        as: 'CompanyAccreditationStatusTypes',
        through: Company,
        foreignKey: 'RegisteredInStateID',
        otherKey: 'AccreditationStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(CompanyType, {
        as: 'CompanyCompanyTypes',
        through: Company,
        foreignKey: 'RegisteredInStateID',
        otherKey: 'CompanyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(IndustryType, {
        as: 'CompanyIndustryTypes',
        through: Company,
        foreignKey: 'RegisteredInStateID',
        otherKey: 'IndustryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(PartyRole, {
        as: 'CompanyLastUpdatedByPartyRoles',
        through: Company,
        foreignKey: 'RegisteredInStateID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(MainBusinessActivityType, {
        as: 'CompanyMainBusinessActivities',
        through: Company,
        foreignKey: 'RegisteredInStateID',
        otherKey: 'MainBusinessActivityID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(Party, {
        as: 'CompanyParties',
        through: Company,
        foreignKey: 'RegisteredInStateID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(PartyRole, {
        as: 'PersonAccountantPartyRoles',
        through: Person,
        foreignKey: 'StateID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(GenderType, {
        as: 'PersonGenderTypes',
        through: Person,
        foreignKey: 'StateID',
        otherKey: 'GenderTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(PartyRole, {
        as: 'PersonLastUpdatedByPartyRoles',
        through: Person,
        foreignKey: 'StateID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(MaritalStatusType, {
        as: 'PersonMaritalStatusTypes',
        through: Person,
        foreignKey: 'StateID',
        otherKey: 'MaritalStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(ObjectColourType, {
        as: 'PersonMedicareCardColourTypes',
        through: Person,
        foreignKey: 'StateID',
        otherKey: 'MedicareCardColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(NameChangeReasonType, {
        as: 'PersonNameChangeReasonTypes',
        through: Person,
        foreignKey: 'StateID',
        otherKey: 'NameChangeReasonTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(Party, {
        as: 'PersonParties',
        through: Person,
        foreignKey: 'StateID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(ObjectColourType, {
        as: 'PersonPassportColourTypes',
        through: Person,
        foreignKey: 'StateID',
        otherKey: 'PassportColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(Country, {
        as: 'PersonPassportCountries',
        through: Person,
        foreignKey: 'StateID',
        otherKey: 'PassportCountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    State.belongsToMany(SalutationType, {
        as: 'PersonSalutationTypes',
        through: Person,
        foreignKey: 'StateID',
        otherKey: 'SalutationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
