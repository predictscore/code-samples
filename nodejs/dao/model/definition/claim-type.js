'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ClaimType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(512),
            field: 'Name',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'ClaimType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var ClaimType = model.ClaimType;
    var ClaimValue = model.ClaimValue;
    var IdentifierType = model.IdentifierType;
    var Login = model.Login;

    ClaimType.hasMany(ClaimValue, {
        as: 'ClaimValueClaimtypeFks',
        foreignKey: 'ClaimTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ClaimType.hasMany(IdentifierType, {
        as: 'FKs',
        foreignKey: 'ClaimTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ClaimType.belongsToMany(Login, {
        as: 'ClaimValueLogins',
        through: ClaimValue,
        foreignKey: 'ClaimTypeID',
        otherKey: 'LoginID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
