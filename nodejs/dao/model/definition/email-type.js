'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('EmailType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        v8ID: {
            type: DataTypes.INTEGER,
            field: 'V8ID',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'EmailType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var EmailType = model.EmailType;
    var PartyEmail = model.PartyEmail;
    var CommunicationType = model.CommunicationType;
    var PartyRole = model.PartyRole;
    var EmailAddress = model.EmailAddress;
    var LocationType = model.LocationType;
    var Party = model.Party;

    EmailType.hasMany(PartyEmail, {
        as: 'PartyEmailEmailtypeFks',
        foreignKey: 'EmailTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    EmailType.belongsToMany(CommunicationType, {
        as: 'PartyEmailCommunicationTypes',
        through: PartyEmail,
        foreignKey: 'EmailTypeID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    EmailType.belongsToMany(PartyRole, {
        as: 'PartyEmailCreatedByPartyRoles',
        through: PartyEmail,
        foreignKey: 'EmailTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    EmailType.belongsToMany(EmailAddress, {
        as: 'PartyEmailEmailAddresses',
        through: PartyEmail,
        foreignKey: 'EmailTypeID',
        otherKey: 'EmailAddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    EmailType.belongsToMany(PartyRole, {
        as: 'PartyEmailLastUpdatedByPartyRoles',
        through: PartyEmail,
        foreignKey: 'EmailTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    EmailType.belongsToMany(LocationType, {
        as: 'PartyEmailLocationTypes',
        through: PartyEmail,
        foreignKey: 'EmailTypeID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    EmailType.belongsToMany(Party, {
        as: 'PartyEmailParties',
        through: PartyEmail,
        foreignKey: 'EmailTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
