'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('LoginPermissionType', {
        loginID: {
            type: DataTypes.INTEGER,
            field: 'LoginID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'Login',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        permissionTypeID: {
            type: DataTypes.INTEGER,
            field: 'PermissionTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'PermissionType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        applicationTypeID: {
            type: DataTypes.INTEGER,
            field: 'ApplicationTypeID',
            allowNull: true,
            references: {
                model: 'ApplicationType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'LoginPermissionType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var LoginPermissionType = model.LoginPermissionType;
    var ApplicationType = model.ApplicationType;
    var Login = model.Login;
    var PermissionType = model.PermissionType;

    LoginPermissionType.belongsTo(ApplicationType, {
        as: 'ApplicationType',
        foreignKey: 'ApplicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LoginPermissionType.belongsTo(Login, {
        as: 'Login',
        foreignKey: 'LoginID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LoginPermissionType.belongsTo(PermissionType, {
        as: 'PermissionType',
        foreignKey: 'PermissionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
