'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ClaimValue', {
        loginID: {
            type: DataTypes.INTEGER,
            field: 'LoginID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'Login',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        claimTypeID: {
            type: DataTypes.INTEGER,
            field: 'ClaimTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'ClaimType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        value: {
            type: DataTypes.STRING(512),
            field: 'Value',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'ClaimValue',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var ClaimValue = model.ClaimValue;
    var ClaimType = model.ClaimType;
    var Login = model.Login;

    ClaimValue.belongsTo(ClaimType, {
        as: 'ClaimType',
        foreignKey: 'ClaimTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ClaimValue.belongsTo(Login, {
        as: 'Login',
        foreignKey: 'LoginID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
