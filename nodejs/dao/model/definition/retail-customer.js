'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('RetailCustomer', {
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: true
        },
        partyTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyTypeID',
            allowNull: true
        },
        partyRoleID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleID',
            allowNull: true
        },
        partyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleTypeID',
            allowNull: true
        },
        partyRoleTypeName: {
            type: DataTypes.STRING(100),
            field: 'PartyRoleTypeName',
            allowNull: true
        },
        nameSurnameFirst: {
            type: DataTypes.STRING,
            field: 'NameSurnameFirst',
            allowNull: true
        },
        name: {
            type: DataTypes.STRING,
            field: 'Name',
            allowNull: true
        },
        companyName: {
            type: DataTypes.STRING(150),
            field: 'CompanyName',
            allowNull: true
        },
        firstName: {
            type: DataTypes.STRING(150),
            field: 'FirstName',
            allowNull: true
        },
        lastName: {
            type: DataTypes.STRING(150),
            field: 'LastName',
            allowNull: true
        },
        salutationTypeID: {
            type: DataTypes.INTEGER,
            field: 'SalutationTypeID',
            allowNull: true
        },
        salutationTypeName: {
            type: DataTypes.STRING(50),
            field: 'SalutationTypeName',
            allowNull: true
        },
        homeEmailPartyEmailID: {
            type: DataTypes.INTEGER,
            field: 'HomeEmailPartyEmailID',
            allowNull: true
        },
        homeEmailID: {
            type: DataTypes.INTEGER,
            field: 'HomeEmailID',
            allowNull: true
        },
        homeEmail: {
            type: DataTypes.STRING(250),
            field: 'HomeEmail',
            allowNull: true
        },
        homeEmailOptOut: {
            type: DataTypes.BOOLEAN,
            field: 'HomeEmailOptOut',
            allowNull: true
        },
        workEmailPartyEmailID: {
            type: DataTypes.INTEGER,
            field: 'WorkEmailPartyEmailID',
            allowNull: true
        },
        workEmailID: {
            type: DataTypes.INTEGER,
            field: 'WorkEmailID',
            allowNull: true
        },
        workEmail: {
            type: DataTypes.STRING(250),
            field: 'WorkEmail',
            allowNull: true
        },
        workEmailOptOut: {
            type: DataTypes.BOOLEAN,
            field: 'WorkEmailOptOut',
            allowNull: true
        },
        homeMobilePartyTelephoneID: {
            type: DataTypes.INTEGER,
            field: 'HomeMobilePartyTelephoneID',
            allowNull: true
        },
        homeTelephoneID: {
            type: DataTypes.INTEGER,
            field: 'HomeTelephoneID',
            allowNull: true
        },
        homeMobile: {
            type: DataTypes.STRING(20),
            field: 'HomeMobile',
            allowNull: true
        },
        homeMobileOptOut: {
            type: DataTypes.BOOLEAN,
            field: 'HomeMobileOptOut',
            allowNull: true
        },
        workMobilePartyTelephoneID: {
            type: DataTypes.INTEGER,
            field: 'WorkMobilePartyTelephoneID',
            allowNull: true
        },
        workMobileTelephoneID: {
            type: DataTypes.INTEGER,
            field: 'WorkMobileTelephoneID',
            allowNull: true
        },
        workMobile: {
            type: DataTypes.STRING(20),
            field: 'WorkMobile',
            allowNull: true
        },
        workMobileOptOut: {
            type: DataTypes.BOOLEAN,
            field: 'WorkMobileOptOut',
            allowNull: true
        },
        homePhonePartyTelephoneID: {
            type: DataTypes.INTEGER,
            field: 'HomePhonePartyTelephoneID',
            allowNull: true
        },
        homePhoneTelephoneID: {
            type: DataTypes.INTEGER,
            field: 'HomePhoneTelephoneID',
            allowNull: true
        },
        homePhone: {
            type: DataTypes.STRING(20),
            field: 'HomePhone',
            allowNull: true
        },
        homePhoneOptOut: {
            type: DataTypes.BOOLEAN,
            field: 'HomePhoneOptOut',
            allowNull: true
        },
        workPhonePartyTelephoneID: {
            type: DataTypes.INTEGER,
            field: 'WorkPhonePartyTelephoneID',
            allowNull: true
        },
        workPhoneTelephoneID: {
            type: DataTypes.INTEGER,
            field: 'WorkPhoneTelephoneID',
            allowNull: true
        },
        workPhone: {
            type: DataTypes.STRING(20),
            field: 'WorkPhone',
            allowNull: true
        },
        workPhoneOptOut: {
            type: DataTypes.BOOLEAN,
            field: 'WorkPhoneOptOut',
            allowNull: true
        },
        dateOfBirth: {
            type: DataTypes.DATEONLY,
            field: 'DateOfBirth',
            allowNull: true
        },
        physicalAddressOptOut: {
            type: DataTypes.BOOLEAN,
            field: 'PhysicalAddressOptOut',
            allowNull: true
        },
        physicalAddressID: {
            type: DataTypes.INTEGER,
            field: 'PhysicalAddressID',
            allowNull: true
        },
        physicalAddressString: {
            type: DataTypes.STRING(500),
            field: 'PhysicalAddressString',
            allowNull: true
        },
        postalAddressID: {
            type: DataTypes.INTEGER,
            field: 'PostalAddressID',
            allowNull: true
        },
        postalAddressString: {
            type: DataTypes.STRING(500),
            field: 'PostalAddressString',
            allowNull: true
        },
        postalAddressOptOut: {
            type: DataTypes.BOOLEAN,
            field: 'PostalAddressOptOut',
            allowNull: true
        },
        physicalAddressCountryID: {
            type: DataTypes.INTEGER,
            field: 'PhysicalAddressCountryID',
            allowNull: true
        },
        physicalAddressPOBox: {
            type: DataTypes.STRING(30),
            field: 'PhysicalAddressPOBox',
            allowNull: true
        },
        physicalAddressPostCode: {
            type: DataTypes.STRING(10),
            field: 'PhysicalAddressPostCode',
            allowNull: true
        },
        physicalAddressPriority: {
            type: DataTypes.INTEGER,
            field: 'PhysicalAddressPriority',
            allowNull: true
        },
        physicalAddressPropertyName: {
            type: DataTypes.STRING(100),
            field: 'PhysicalAddressPropertyName',
            allowNull: true
        },
        physicalAddressStateID: {
            type: DataTypes.INTEGER,
            field: 'PhysicalAddressStateID',
            allowNull: true
        },
        physicalAddressStreetName: {
            type: DataTypes.STRING(100),
            field: 'PhysicalAddressStreetName',
            allowNull: true
        },
        physicalAddressStreetNumber: {
            type: DataTypes.STRING(100),
            field: 'PhysicalAddressStreetNumber',
            allowNull: true
        },
        physicalAddressStreetTypeID: {
            type: DataTypes.INTEGER,
            field: 'PhysicalAddressStreetTypeID',
            allowNull: true
        },
        physicalAddressSuburb: {
            type: DataTypes.STRING(100),
            field: 'PhysicalAddressSuburb',
            allowNull: true
        },
        physicalAddressUnitNumber: {
            type: DataTypes.STRING(100),
            field: 'PhysicalAddressUnitNumber',
            allowNull: true
        },
        physicalAddressRPDataPropertyID: {
            type: DataTypes.INTEGER,
            field: 'PhysicalAddressRPDataPropertyID',
            allowNull: true
        },
        physicalAddressAddressText: {
            type: DataTypes.STRING(500),
            field: 'PhysicalAddressAddressText',
            allowNull: true
        },
        physicalAddressRetailID: {
            type: DataTypes.INTEGER,
            field: 'PhysicalAddressRetailID',
            allowNull: true
        },
        physicalAddressISOCountryCode: {
            type: DataTypes.STRING(50),
            field: 'PhysicalAddressISOCountryCode',
            allowNull: true
        },
        physicalAddressCountryName: {
            type: DataTypes.STRING(150),
            field: 'PhysicalAddressCountryName',
            allowNull: true
        },
        physicalAddressStreetTypeName: {
            type: DataTypes.STRING(50),
            field: 'PhysicalAddressStreetTypeName',
            allowNull: true
        },
        physicalAddressStateName: {
            type: DataTypes.STRING(150),
            field: 'PhysicalAddressStateName',
            allowNull: true
        },
        physicalAddressPeriodOfOccupancyMonths: {
            type: DataTypes.INTEGER,
            field: 'PhysicalAddressPeriodOfOccupancyMonths',
            allowNull: true
        },
        physicalAddressPeriodOfOccupancyYears: {
            type: DataTypes.INTEGER,
            field: 'PhysicalAddressPeriodOfOccupancyYears',
            allowNull: true
        },
        postalAddressCountryID: {
            type: DataTypes.INTEGER,
            field: 'PostalAddressCountryID',
            allowNull: true
        },
        postalAddressPOBox: {
            type: DataTypes.STRING(30),
            field: 'PostalAddressPOBox',
            allowNull: true
        },
        postalAddressPostCode: {
            type: DataTypes.STRING(10),
            field: 'PostalAddressPostCode',
            allowNull: true
        },
        postalAddressPriority: {
            type: DataTypes.INTEGER,
            field: 'PostalAddressPriority',
            allowNull: true
        },
        postalAddressPropertyName: {
            type: DataTypes.STRING(100),
            field: 'PostalAddressPropertyName',
            allowNull: true
        },
        postalAddressStateID: {
            type: DataTypes.INTEGER,
            field: 'PostalAddressStateID',
            allowNull: true
        },
        postalAddressStreetName: {
            type: DataTypes.STRING(100),
            field: 'PostalAddressStreetName',
            allowNull: true
        },
        postalAddressStreetNumber: {
            type: DataTypes.STRING(100),
            field: 'PostalAddressStreetNumber',
            allowNull: true
        },
        postalAddressStreetTypeID: {
            type: DataTypes.INTEGER,
            field: 'PostalAddressStreetTypeID',
            allowNull: true
        },
        postalAddressSuburb: {
            type: DataTypes.STRING(100),
            field: 'PostalAddressSuburb',
            allowNull: true
        },
        postalAddressUnitNumber: {
            type: DataTypes.STRING(100),
            field: 'PostalAddressUnitNumber',
            allowNull: true
        },
        postalAddressRPDataPropertyID: {
            type: DataTypes.INTEGER,
            field: 'PostalAddressRPDataPropertyID',
            allowNull: true
        },
        postalAddressAddressText: {
            type: DataTypes.STRING(500),
            field: 'PostalAddressAddressText',
            allowNull: true
        },
        postalAddressRetailID: {
            type: DataTypes.INTEGER,
            field: 'PostalAddressRetailID',
            allowNull: true
        },
        postalAddressISOCountryCode: {
            type: DataTypes.STRING(50),
            field: 'PostalAddressISOCountryCode',
            allowNull: true
        },
        postalAddressCountryName: {
            type: DataTypes.STRING(150),
            field: 'PostalAddressCountryName',
            allowNull: true
        },
        postalAddressStreetTypeName: {
            type: DataTypes.STRING(50),
            field: 'PostalAddressStreetTypeName',
            allowNull: true
        },
        postalAddressStateName: {
            type: DataTypes.STRING(150),
            field: 'PostalAddressStateName',
            allowNull: true
        },
        postalAddressPeriodOfOccupancyMonths: {
            type: DataTypes.INTEGER,
            field: 'PostalAddressPeriodOfOccupancyMonths',
            allowNull: true
        },
        postalAddressPeriodOfOccupancyYears: {
            type: DataTypes.INTEGER,
            field: 'PostalAddressPeriodOfOccupancyYears',
            allowNull: true
        },
        claimLeadID: {
            type: DataTypes.INTEGER,
            field: 'ClaimLeadID',
            allowNull: true
        },
        claimCustomerID: {
            type: DataTypes.INTEGER,
            field: 'ClaimCustomerID',
            allowNull: true
        },
        claimPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'ClaimPartyRoleID',
            allowNull: true
        },
        loginID: {
            type: DataTypes.INTEGER,
            field: 'LoginID',
            allowNull: true
        },
        loginEmail: {
            type: DataTypes.STRING(100),
            field: 'LoginEmail',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'RetailCustomer',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var RetailCustomer = model.RetailCustomer;

};
