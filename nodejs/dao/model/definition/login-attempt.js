'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('LoginAttempt', {
        loginID: {
            type: DataTypes.INTEGER,
            field: 'LoginID',
            allowNull: false,
            references: {
                model: 'Login',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        when: {
            type: DataTypes.DATE,
            field: 'When',
            allowNull: false
        },
        successful: {
            type: DataTypes.BOOLEAN,
            field: 'Successful',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'LoginAttempt',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var LoginAttempt = model.LoginAttempt;
    var Login = model.Login;

    LoginAttempt.removeAttribute('id'); // removing the default PK

    LoginAttempt.belongsTo(Login, {
        as: 'Login',
        foreignKey: 'LoginID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
