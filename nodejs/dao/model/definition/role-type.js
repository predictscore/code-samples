'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('RoleType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'RoleType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var RoleType = model.RoleType;
    var ApplicationRole = model.ApplicationRole;
    var PartyRoleTypeApplicationTypeRoleType = model.PartyRoleTypeApplicationTypeRoleType;
    var PartyRoleTypeRoleType = model.PartyRoleTypeRoleType;
    var RoleTypePermissionType = model.RoleTypePermissionType;
    var ApplicationType = model.ApplicationType;
    var Login = model.Login;
    var PartyRoleType = model.PartyRoleType;
    var PermissionType = model.PermissionType;

    RoleType.hasMany(ApplicationRole, {
        as: 'ApplicationRoleRoletypeFks',
        foreignKey: 'RoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RoleType.hasMany(PartyRoleTypeApplicationTypeRoleType, {
        as: 'PartyRoleTypeApplicationTypeRoleTypeRoletypeFks',
        foreignKey: 'RoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RoleType.hasMany(PartyRoleTypeRoleType, {
        as: 'PartyRoleTypeRoleTypeRoletypeFks',
        foreignKey: 'RoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RoleType.hasMany(RoleTypePermissionType, {
        as: 'PermissionTypeRoletypeFks',
        foreignKey: 'RoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RoleType.belongsToMany(ApplicationType, {
        as: 'ApplicationRoleApplicationTypes',
        through: ApplicationRole,
        foreignKey: 'RoleTypeID',
        otherKey: 'ApplicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RoleType.belongsToMany(Login, {
        as: 'ApplicationRoleLogins',
        through: ApplicationRole,
        foreignKey: 'RoleTypeID',
        otherKey: 'LoginID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RoleType.belongsToMany(ApplicationType, {
        as: 'PartyRoleTypeApplicationTypeRoleTypeApplicationTypes',
        through: PartyRoleTypeApplicationTypeRoleType,
        foreignKey: 'RoleTypeID',
        otherKey: 'ApplicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RoleType.belongsToMany(PartyRoleType, {
        as: 'PartyRoleTypeApplicationTypeRoleTypePartyRoleTypes',
        through: PartyRoleTypeApplicationTypeRoleType,
        foreignKey: 'RoleTypeID',
        otherKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RoleType.belongsToMany(PartyRoleType, {
        as: 'PartyRoleTypeRoleTypePartyRoleTypes',
        through: PartyRoleTypeRoleType,
        foreignKey: 'RoleTypeID',
        otherKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RoleType.belongsToMany(PermissionType, {
        as: 'RoleTypePermissionTypePermissionTypes',
        through: RoleTypePermissionType,
        foreignKey: 'RoleTypeID',
        otherKey: 'PermissionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
