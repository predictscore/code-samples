'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Party', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        partyTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyTypeID',
            allowNull: false,
            references: {
                model: 'PartyType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        justicePartyID: {
            type: DataTypes.INTEGER,
            field: 'JusticePartyID',
            allowNull: true
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        lastUpdated: {
            type: DataTypes.DATE,
            field: 'LastUpdated',
            allowNull: false
        },
        lastUpdatedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'LastUpdatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        },
        v8ID: {
            type: DataTypes.INTEGER,
            field: 'V8ID',
            allowNull: true
        },
        isInternal: {
            type: DataTypes.BOOLEAN,
            field: 'IsInternal',
            allowNull: true,
            defaultValue: false
        },
        cRMGuid: {
            type: DataTypes.UUID,
            field: 'CRMGuid',
            allowNull: true
        },
        vedaReference: {
            type: DataTypes.STRING(20),
            field: 'VedaReference',
            allowNull: true
        },
        documentGUID: {
            type: DataTypes.UUID,
            field: 'DocumentGUID',
            allowNull: true
        },
        ultracsClientID: {
            type: DataTypes.INTEGER,
            field: 'UltracsClientID',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'Party',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var Party = model.Party;
    var Company = model.Company;
    var HistoryNote = model.HistoryNote;
    var IdentityVerificationReport = model.IdentityVerificationReport;
    var PartyAccreditation = model.PartyAccreditation;
    var PartyAddress = model.PartyAddress;
    var PartyBankAccount = model.PartyBankAccount;
    var PartyCompliance = model.PartyCompliance;
    var PartyEmail = model.PartyEmail;
    var PartyPresence = model.PartyPresence;
    var PartyRole = model.PartyRole;
    var PartyScoreHistory = model.PartyScoreHistory;
    var PartySearchAvailability = model.PartySearchAvailability;
    var PartyTelephone = model.PartyTelephone;
    var PartyTrustee = model.PartyTrustee;
    var Person = model.Person;
    var Trust = model.Trust;
    var PartyType = model.PartyType;
    var AccountRankingType = model.AccountRankingType;
    var AccreditationStatusType = model.AccreditationStatusType;
    var CompanyType = model.CompanyType;
    var IndustryType = model.IndustryType;
    var MainBusinessActivityType = model.MainBusinessActivityType;
    var State = model.State;
    var HistoryNoteStatusType = model.HistoryNoteStatusType;
    var HistoryNoteType = model.HistoryNoteType;
    var IdentityVerificationReportType = model.IdentityVerificationReportType;
    var AddressType = model.AddressType;
    var Address = model.Address;
    var CommunicationType = model.CommunicationType;
    var CurrencyType = model.CurrencyType;
    var LocationType = model.LocationType;
    var ResidencyType = model.ResidencyType;
    var BankAccount = model.BankAccount;
    var BankBSB = model.BankBSB;
    var PartyBankAccountType = model.PartyBankAccountType;
    var RegistrationType = model.RegistrationType;
    var EmailAddress = model.EmailAddress;
    var EmailType = model.EmailType;
    var PresenceType = model.PresenceType;
    var PartyRoleType = model.PartyRoleType;
    var SearchType = model.SearchType;
    var TelephoneNumber = model.TelephoneNumber;
    var TelephoneType = model.TelephoneType;
    var GenderType = model.GenderType;
    var MaritalStatusType = model.MaritalStatusType;
    var ObjectColourType = model.ObjectColourType;
    var NameChangeReasonType = model.NameChangeReasonType;
    var Country = model.Country;
    var SalutationType = model.SalutationType;
    var DocumentationType = model.DocumentationType;
    var TrustPurposeType = model.TrustPurposeType;
    var TrustStructureType = model.TrustStructureType;

    Party.hasMany(Company, {
        as: 'CompanyPartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.hasMany(HistoryNote, {
        as: 'HistoryNotePartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.hasMany(IdentityVerificationReport, {
        as: 'FKIdentityverificationreportParties',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.hasMany(PartyAccreditation, {
        as: 'AccreditationPartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.hasMany(PartyAddress, {
        as: 'AddressPartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.hasMany(PartyBankAccount, {
        as: 'BankAccountPartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.hasMany(PartyCompliance, {
        as: 'CompliancePartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.hasMany(PartyEmail, {
        as: 'EmailPartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.hasMany(PartyPresence, {
        as: 'PresencePartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.hasMany(PartyRole, {
        as: 'RolePartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.hasMany(PartyScoreHistory, {
        as: 'ScoreHistoryPartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.hasMany(PartySearchAvailability, {
        as: 'SearchAvailabilityPartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.hasMany(PartyTelephone, {
        as: 'TelephonePartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.hasMany(PartyTrustee, {
        as: 'TrusteePartyFks',
        foreignKey: 'TrusteePartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.hasOne(Person, {
        as: 'PersonPartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.hasMany(Trust, {
        as: 'TrustPartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsTo(PartyRole, {
        as: 'LastUpdatedByPartyRole',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsTo(PartyType, {
        as: 'PartyType',
        foreignKey: 'PartyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(AccountRankingType, {
        as: 'CompanyAccountRankingTypes',
        through: Company,
        foreignKey: 'PartyID',
        otherKey: 'AccountRankingTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'CompanyAccountantPartyRoles',
        through: Company,
        foreignKey: 'PartyID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(AccreditationStatusType, {
        as: 'CompanyAccreditationStatusTypes',
        through: Company,
        foreignKey: 'PartyID',
        otherKey: 'AccreditationStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(CompanyType, {
        as: 'CompanyCompanyTypes',
        through: Company,
        foreignKey: 'PartyID',
        otherKey: 'CompanyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(IndustryType, {
        as: 'CompanyIndustryTypes',
        through: Company,
        foreignKey: 'PartyID',
        otherKey: 'IndustryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'CompanyLastUpdatedByPartyRoles',
        through: Company,
        foreignKey: 'PartyID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(MainBusinessActivityType, {
        as: 'CompanyMainBusinessActivities',
        through: Company,
        foreignKey: 'PartyID',
        otherKey: 'MainBusinessActivityID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(State, {
        as: 'CompanyRegisteredInStates',
        through: Company,
        foreignKey: 'PartyID',
        otherKey: 'RegisteredInStateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'HistoryNoteCreatedByPartyRoles',
        through: HistoryNote,
        foreignKey: 'PartyID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'HistoryNoteFollowUpByPartyRoles',
        through: HistoryNote,
        foreignKey: 'PartyID',
        otherKey: 'FollowUpByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(HistoryNoteStatusType, {
        as: 'HistoryNoteHistoryNoteStatusTypes',
        through: HistoryNote,
        foreignKey: 'PartyID',
        otherKey: 'HistoryNoteStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(HistoryNoteType, {
        as: 'HistoryNoteHistoryNoteTypes',
        through: HistoryNote,
        foreignKey: 'PartyID',
        otherKey: 'HistoryNoteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'IdentityVerificationReportCreatedByPartyRoles',
        through: IdentityVerificationReport,
        foreignKey: 'PartyID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(IdentityVerificationReportType, {
        as: 'IdentityVerificationReportIdentityVerificationReportTypes',
        through: IdentityVerificationReport,
        foreignKey: 'PartyID',
        otherKey: 'IdentityVerificationReportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'IdentityVerificationReportLastUpdatedByPartyRoles',
        through: IdentityVerificationReport,
        foreignKey: 'PartyID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyAccreditationCreatedByPartyRoles',
        through: PartyAccreditation,
        foreignKey: 'PartyID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyAccreditationInsurerPartyRoles',
        through: PartyAccreditation,
        foreignKey: 'PartyID',
        otherKey: 'InsurerPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyAccreditationLastUpdatedByPartyRoles',
        through: PartyAccreditation,
        foreignKey: 'PartyID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(AddressType, {
        as: 'PartyAddressAddressTypes',
        through: PartyAddress,
        foreignKey: 'PartyID',
        otherKey: 'AddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(Address, {
        as: 'PartyAddressAddresses',
        through: PartyAddress,
        foreignKey: 'PartyID',
        otherKey: 'AddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(CommunicationType, {
        as: 'PartyAddressCommunicationTypes',
        through: PartyAddress,
        foreignKey: 'PartyID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyAddressCreatedByPartyRoles',
        through: PartyAddress,
        foreignKey: 'PartyID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(CurrencyType, {
        as: 'PartyAddressCurrencyTypes',
        through: PartyAddress,
        foreignKey: 'PartyID',
        otherKey: 'CurrencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(LocationType, {
        as: 'PartyAddressLocationTypes',
        through: PartyAddress,
        foreignKey: 'PartyID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyAddressLastUpdatedByPartyRoles',
        through: PartyAddress,
        foreignKey: 'PartyID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(ResidencyType, {
        as: 'PartyAddressResidencyTypes',
        through: PartyAddress,
        foreignKey: 'PartyID',
        otherKey: 'ResidencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(BankAccount, {
        as: 'PartyBankAccountBankAccounts',
        through: PartyBankAccount,
        foreignKey: 'PartyID',
        otherKey: 'BankAccountID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(BankBSB, {
        as: 'PartyBankAccountBankBSBs',
        through: PartyBankAccount,
        foreignKey: 'PartyID',
        otherKey: 'BankBSBID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyBankAccountCreatedByPartyRoles',
        through: PartyBankAccount,
        foreignKey: 'PartyID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyBankAccountLastUpdateByPartyRoles',
        through: PartyBankAccount,
        foreignKey: 'PartyID',
        otherKey: 'LastUpdateByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyBankAccountType, {
        as: 'PartyBankAccountPartyBankAccountTypes',
        through: PartyBankAccount,
        foreignKey: 'PartyID',
        otherKey: 'PartyBankAccountTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyComplianceCreatedByPartyRoles',
        through: PartyCompliance,
        foreignKey: 'PartyID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyComplianceLastUpdatedByPartyRoles',
        through: PartyCompliance,
        foreignKey: 'PartyID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(RegistrationType, {
        as: 'PartyComplianceRegistrationTypes',
        through: PartyCompliance,
        foreignKey: 'PartyID',
        otherKey: 'RegistrationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(CommunicationType, {
        as: 'PartyEmailCommunicationTypes',
        through: PartyEmail,
        foreignKey: 'PartyID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyEmailCreatedByPartyRoles',
        through: PartyEmail,
        foreignKey: 'PartyID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(EmailAddress, {
        as: 'PartyEmailEmailAddresses',
        through: PartyEmail,
        foreignKey: 'PartyID',
        otherKey: 'EmailAddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(EmailType, {
        as: 'PartyEmailEmailTypes',
        through: PartyEmail,
        foreignKey: 'PartyID',
        otherKey: 'EmailTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyEmailLastUpdatedByPartyRoles',
        through: PartyEmail,
        foreignKey: 'PartyID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(LocationType, {
        as: 'PartyEmailLocationTypes',
        through: PartyEmail,
        foreignKey: 'PartyID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PresenceType, {
        as: 'PartyPresencePresenceTypes',
        through: PartyPresence,
        foreignKey: 'PartyID',
        otherKey: 'PresenceTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyRoleCreatedByPartyRoles',
        through: PartyRole,
        foreignKey: 'PartyID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRoleType, {
        as: 'PartyRolePartyRoleTypes',
        through: PartyRole,
        foreignKey: 'PartyID',
        otherKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartySearchAvailabilityCreatedByPartyRoles',
        through: PartySearchAvailability,
        foreignKey: 'PartyID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartySearchAvailabilityLastUpdatedByPartyRoles',
        through: PartySearchAvailability,
        foreignKey: 'PartyID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(SearchType, {
        as: 'PartySearchAvailabilitySearchTypes',
        through: PartySearchAvailability,
        foreignKey: 'PartyID',
        otherKey: 'SearchTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(CommunicationType, {
        as: 'PartyTelephoneCommunicationTypes',
        through: PartyTelephone,
        foreignKey: 'PartyID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyTelephoneCreatedByPartyRoles',
        through: PartyTelephone,
        foreignKey: 'PartyID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyTelephoneLastUpdatedByPartyRoles',
        through: PartyTelephone,
        foreignKey: 'PartyID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(LocationType, {
        as: 'PartyTelephoneLocationTypes',
        through: PartyTelephone,
        foreignKey: 'PartyID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(TelephoneNumber, {
        as: 'PartyTelephoneTelephoneNumbers',
        through: PartyTelephone,
        foreignKey: 'PartyID',
        otherKey: 'TelephoneNumberID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(TelephoneType, {
        as: 'PartyTelephoneTelephoneTypes',
        through: PartyTelephone,
        foreignKey: 'PartyID',
        otherKey: 'TelephoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyTrusteeCreatedByPartyRoles',
        through: PartyTrustee,
        foreignKey: 'TrusteePartyID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PartyTrusteeTrustPartyRoles',
        through: PartyTrustee,
        foreignKey: 'TrusteePartyID',
        otherKey: 'TrustPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PersonAccountantPartyRoles',
        through: Person,
        foreignKey: 'PartyID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(GenderType, {
        as: 'PersonGenderTypes',
        through: Person,
        foreignKey: 'PartyID',
        otherKey: 'GenderTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'PersonLastUpdatedByPartyRoles',
        through: Person,
        foreignKey: 'PartyID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(MaritalStatusType, {
        as: 'PersonMaritalStatusTypes',
        through: Person,
        foreignKey: 'PartyID',
        otherKey: 'MaritalStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(ObjectColourType, {
        as: 'PersonMedicareCardColourTypes',
        through: Person,
        foreignKey: 'PartyID',
        otherKey: 'MedicareCardColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(NameChangeReasonType, {
        as: 'PersonNameChangeReasonTypes',
        through: Person,
        foreignKey: 'PartyID',
        otherKey: 'NameChangeReasonTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(ObjectColourType, {
        as: 'PersonPassportColourTypes',
        through: Person,
        foreignKey: 'PartyID',
        otherKey: 'PassportColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(Country, {
        as: 'PersonPassportCountries',
        through: Person,
        foreignKey: 'PartyID',
        otherKey: 'PassportCountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(SalutationType, {
        as: 'PersonSalutationTypes',
        through: Person,
        foreignKey: 'PartyID',
        otherKey: 'SalutationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(State, {
        as: 'PersonStates',
        through: Person,
        foreignKey: 'PartyID',
        otherKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'TrustAccountantPartyRoles',
        through: Trust,
        foreignKey: 'PartyID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(Country, {
        as: 'TrustCountryEstablisheds',
        through: Trust,
        foreignKey: 'PartyID',
        otherKey: 'CountryEstablishedID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(DocumentationType, {
        as: 'TrustDocumentationTypes',
        through: Trust,
        foreignKey: 'PartyID',
        otherKey: 'DocumentationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(PartyRole, {
        as: 'TrustLastUpdatedByPartyRoles',
        through: Trust,
        foreignKey: 'PartyID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(TrustPurposeType, {
        as: 'TrustTrustPurposeTypes',
        through: Trust,
        foreignKey: 'PartyID',
        otherKey: 'TrustPurposeTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Party.belongsToMany(TrustStructureType, {
        as: 'TrustTrustStructureTypes',
        through: Trust,
        foreignKey: 'PartyID',
        otherKey: 'TrustStructureTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
