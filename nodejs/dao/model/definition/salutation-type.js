'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('SalutationType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        ultracsCode: {
            type: DataTypes.STRING(10),
            field: 'UltracsCode',
            allowNull: true
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        },
        retailID: {
            type: DataTypes.INTEGER,
            field: 'RetailID',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        },
        justiceGenderID: {
            type: DataTypes.INTEGER,
            field: 'JusticeGenderID',
            allowNull: true
        },
        showOnRetailWizard: {
            type: DataTypes.BOOLEAN,
            field: 'ShowOnRetailWizard',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'SalutationType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var SalutationType = model.SalutationType;
    var Person = model.Person;
    var PartyRole = model.PartyRole;
    var GenderType = model.GenderType;
    var MaritalStatusType = model.MaritalStatusType;
    var ObjectColourType = model.ObjectColourType;
    var NameChangeReasonType = model.NameChangeReasonType;
    var Party = model.Party;
    var Country = model.Country;
    var State = model.State;

    SalutationType.hasMany(Person, {
        as: 'PersonSalutationtypeFks',
        foreignKey: 'SalutationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SalutationType.belongsToMany(PartyRole, {
        as: 'PersonAccountantPartyRoles',
        through: Person,
        foreignKey: 'SalutationTypeID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SalutationType.belongsToMany(GenderType, {
        as: 'PersonGenderTypes',
        through: Person,
        foreignKey: 'SalutationTypeID',
        otherKey: 'GenderTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SalutationType.belongsToMany(PartyRole, {
        as: 'PersonLastUpdatedByPartyRoles',
        through: Person,
        foreignKey: 'SalutationTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SalutationType.belongsToMany(MaritalStatusType, {
        as: 'PersonMaritalStatusTypes',
        through: Person,
        foreignKey: 'SalutationTypeID',
        otherKey: 'MaritalStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SalutationType.belongsToMany(ObjectColourType, {
        as: 'PersonMedicareCardColourTypes',
        through: Person,
        foreignKey: 'SalutationTypeID',
        otherKey: 'MedicareCardColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SalutationType.belongsToMany(NameChangeReasonType, {
        as: 'PersonNameChangeReasonTypes',
        through: Person,
        foreignKey: 'SalutationTypeID',
        otherKey: 'NameChangeReasonTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SalutationType.belongsToMany(Party, {
        as: 'PersonParties',
        through: Person,
        foreignKey: 'SalutationTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SalutationType.belongsToMany(ObjectColourType, {
        as: 'PersonPassportColourTypes',
        through: Person,
        foreignKey: 'SalutationTypeID',
        otherKey: 'PassportColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SalutationType.belongsToMany(Country, {
        as: 'PersonPassportCountries',
        through: Person,
        foreignKey: 'SalutationTypeID',
        otherKey: 'PassportCountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SalutationType.belongsToMany(State, {
        as: 'PersonStates',
        through: Person,
        foreignKey: 'SalutationTypeID',
        otherKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
