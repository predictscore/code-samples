'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('RoleTypePermissionType', {
        roleTypeID: {
            type: DataTypes.INTEGER,
            field: 'RoleTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'RoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        permissionTypeID: {
            type: DataTypes.INTEGER,
            field: 'PermissionTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'PermissionType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'RoleTypePermissionType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var RoleTypePermissionType = model.RoleTypePermissionType;
    var PermissionType = model.PermissionType;
    var RoleType = model.RoleType;

    RoleTypePermissionType.belongsTo(PermissionType, {
        as: 'PermissionType',
        foreignKey: 'PermissionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RoleTypePermissionType.belongsTo(RoleType, {
        as: 'RoleType',
        foreignKey: 'RoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
