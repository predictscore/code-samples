'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('HistoryNoteType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'HistoryNoteType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var HistoryNoteType = model.HistoryNoteType;
    var HistoryNote = model.HistoryNote;
    var PartyRole = model.PartyRole;
    var HistoryNoteStatusType = model.HistoryNoteStatusType;
    var Party = model.Party;

    HistoryNoteType.hasMany(HistoryNote, {
        as: 'HistoryNoteHistorynotetypeFks',
        foreignKey: 'HistoryNoteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    HistoryNoteType.belongsToMany(PartyRole, {
        as: 'HistoryNoteCreatedByPartyRoles',
        through: HistoryNote,
        foreignKey: 'HistoryNoteTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    HistoryNoteType.belongsToMany(PartyRole, {
        as: 'HistoryNoteFollowUpByPartyRoles',
        through: HistoryNote,
        foreignKey: 'HistoryNoteTypeID',
        otherKey: 'FollowUpByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    HistoryNoteType.belongsToMany(HistoryNoteStatusType, {
        as: 'HistoryNoteHistoryNoteStatusTypes',
        through: HistoryNote,
        foreignKey: 'HistoryNoteTypeID',
        otherKey: 'HistoryNoteStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    HistoryNoteType.belongsToMany(Party, {
        as: 'HistoryNoteParties',
        through: HistoryNote,
        foreignKey: 'HistoryNoteTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
