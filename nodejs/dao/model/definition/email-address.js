'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('EmailAddress', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        emailAddress: {
            type: DataTypes.STRING(250),
            field: 'EmailAddress',
            allowNull: false
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'EmailAddress',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var EmailAddress = model.EmailAddress;
    var PartyEmail = model.PartyEmail;
    var PartyRole = model.PartyRole;
    var CommunicationType = model.CommunicationType;
    var EmailType = model.EmailType;
    var LocationType = model.LocationType;
    var Party = model.Party;

    EmailAddress.hasMany(PartyEmail, {
        as: 'PartyEmailEmailaddressFks',
        foreignKey: 'EmailAddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    EmailAddress.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    EmailAddress.belongsToMany(CommunicationType, {
        as: 'PartyEmailCommunicationTypes',
        through: PartyEmail,
        foreignKey: 'EmailAddressID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    EmailAddress.belongsToMany(PartyRole, {
        as: 'PartyEmailCreatedByPartyRoles',
        through: PartyEmail,
        foreignKey: 'EmailAddressID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    EmailAddress.belongsToMany(EmailType, {
        as: 'PartyEmailEmailTypes',
        through: PartyEmail,
        foreignKey: 'EmailAddressID',
        otherKey: 'EmailTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    EmailAddress.belongsToMany(PartyRole, {
        as: 'PartyEmailLastUpdatedByPartyRoles',
        through: PartyEmail,
        foreignKey: 'EmailAddressID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    EmailAddress.belongsToMany(LocationType, {
        as: 'PartyEmailLocationTypes',
        through: PartyEmail,
        foreignKey: 'EmailAddressID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    EmailAddress.belongsToMany(Party, {
        as: 'PartyEmailParties',
        through: PartyEmail,
        foreignKey: 'EmailAddressID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
