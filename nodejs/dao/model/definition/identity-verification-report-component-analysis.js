'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportComponentAnalysis', {
        identityVerificationReportID: {
            type: DataTypes.INTEGER,
            field: 'IdentityVerificationReportID',
            allowNull: false,
            references: {
                model: 'IdentityVerificationReport',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        categoryTypeID: {
            type: DataTypes.INTEGER,
            field: 'CategoryTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentAnalysisCategoryType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        searchNameTypeID: {
            type: DataTypes.INTEGER,
            field: 'SearchNameTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentSearchNameType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        rawScore: {
            type: DataTypes.INTEGER,
            field: 'RawScore',
            allowNull: true
        },
        minimumValue: {
            type: DataTypes.INTEGER,
            field: 'MinimumValue',
            allowNull: true
        },
        filteredScore: {
            type: DataTypes.INTEGER,
            field: 'FilteredScore',
            allowNull: true
        },
        weight: {
            type: DataTypes.DECIMAL(18, 4),
            field: 'Weight',
            allowNull: true
        },
        points: {
            type: DataTypes.INTEGER,
            field: 'Points',
            allowNull: true
        },
        contributeNameTypeID: {
            type: DataTypes.INTEGER,
            field: 'ContributeNameTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentAnalysisContributingValueTyp',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        contributeAddressTypeID: {
            type: DataTypes.INTEGER,
            field: 'ContributeAddressTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentAnalysisContributingValueTyp',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        contributeDateOfBirthTypeID: {
            type: DataTypes.INTEGER,
            field: 'ContributeDateOfBirthTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentAnalysisContributingValueTyp',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        contributePhoneTypeID: {
            type: DataTypes.INTEGER,
            field: 'ContributePhoneTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentAnalysisContributingValueTyp',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        contributeDriversTypeID: {
            type: DataTypes.INTEGER,
            field: 'ContributeDriversTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentAnalysisContributingValueTyp',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        contributePassportTypeID: {
            type: DataTypes.INTEGER,
            field: 'ContributePassportTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentAnalysisContributingValueTyp',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        contributeDocumentTypeID: {
            type: DataTypes.INTEGER,
            field: 'ContributeDocumentTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentAnalysisContributingValueTyp',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        contributeTransactionTypeID: {
            type: DataTypes.INTEGER,
            field: 'ContributeTransactionTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentAnalysisContributingValueTyp',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        contributeEntitlementTypeID: {
            type: DataTypes.INTEGER,
            field: 'ContributeEntitlementTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentAnalysisContributingValueTyp',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        contributeMultipleTypeID: {
            type: DataTypes.INTEGER,
            field: 'ContributeMultipleTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentAnalysisContributingValueTyp',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportComponentAnalysis',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportComponentAnalysis = model.IdentityVerificationReportComponentAnalysis;
    var IdentityVerificationReportComponentAnalysisContributingValueTyp = model.IdentityVerificationReportComponentAnalysisContributingValueTyp;
    var IdentityVerificationReportComponentSearchNameType = model.IdentityVerificationReportComponentSearchNameType;
    var IdentityVerificationReportComponentAnalysisCategoryType = model.IdentityVerificationReportComponentAnalysisCategoryType;
    var IdentityVerificationReport = model.IdentityVerificationReport;

    IdentityVerificationReportComponentAnalysis.belongsTo(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'ContributePhoneType',
        foreignKey: 'ContributePhoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysis.belongsTo(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'ContributeTransactionType',
        foreignKey: 'ContributeTransactionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysis.belongsTo(IdentityVerificationReportComponentSearchNameType, {
        as: 'SearchNameType',
        foreignKey: 'SearchNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysis.belongsTo(IdentityVerificationReportComponentAnalysisCategoryType, {
        as: 'CategoryType',
        foreignKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysis.belongsTo(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'ContributeAddressType',
        foreignKey: 'ContributeAddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysis.belongsTo(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'ContributeDateOfBirthType',
        foreignKey: 'ContributeDateOfBirthTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysis.belongsTo(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'ContributeDocumentType',
        foreignKey: 'ContributeDocumentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysis.belongsTo(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'ContributeDriversType',
        foreignKey: 'ContributeDriversTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysis.belongsTo(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'ContributeEntitlementType',
        foreignKey: 'ContributeEntitlementTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysis.belongsTo(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'ContributeMultipleType',
        foreignKey: 'ContributeMultipleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysis.belongsTo(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'ContributeNameType',
        foreignKey: 'ContributeNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysis.belongsTo(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'ContributePassportType',
        foreignKey: 'ContributePassportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysis.belongsTo(IdentityVerificationReport, {
        as: 'IdentityVerificationReport',
        foreignKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
