'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('StreetType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        code: {
            type: DataTypes.STRING(10),
            field: 'Code',
            allowNull: false
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: true
        },
        v8ID: {
            type: DataTypes.INTEGER,
            field: 'V8ID',
            allowNull: true
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        },
        tontoID: {
            type: DataTypes.INTEGER,
            field: 'TontoID',
            allowNull: true
        },
        retailID: {
            type: DataTypes.INTEGER,
            field: 'RetailID',
            allowNull: true
        },
        vedaCode: {
            type: DataTypes.STRING(10),
            field: 'VedaCode',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'StreetType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var StreetType = model.StreetType;
    var Address = model.Address;
    var Country = model.Country;
    var PartyRole = model.PartyRole;
    var State = model.State;

    StreetType.hasMany(Address, {
        as: 'AddressStreettypeFks',
        foreignKey: 'StreetTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    StreetType.belongsToMany(Country, {
        as: 'AddressCountries',
        through: Address,
        foreignKey: 'StreetTypeID',
        otherKey: 'CountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    StreetType.belongsToMany(PartyRole, {
        as: 'AddressCreatedByPartyRoles',
        through: Address,
        foreignKey: 'StreetTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    StreetType.belongsToMany(PartyRole, {
        as: 'AddressLastUpdatedByPartyRoles',
        through: Address,
        foreignKey: 'StreetTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    StreetType.belongsToMany(State, {
        as: 'AddressStates',
        through: Address,
        foreignKey: 'StreetTypeID',
        otherKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
