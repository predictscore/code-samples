'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Login', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        username: {
            type: DataTypes.STRING(100),
            field: 'Username',
            allowNull: false
        },
        password: {
            type: DataTypes.BLOB,
            field: 'Password',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        },
        locked: {
            type: DataTypes.BOOLEAN,
            field: 'Locked',
            allowNull: false
        },
        expired: {
            type: DataTypes.DATE,
            field: 'Expired',
            allowNull: true
        },
        siteTypeID: {
            type: DataTypes.INTEGER,
            field: 'SiteTypeID',
            allowNull: true,
            references: {
                model: 'SiteType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        objectSID: {
            type: DataTypes.STRING(100),
            field: 'ObjectSID',
            allowNull: true
        },
        decommissioned: {
            type: DataTypes.BOOLEAN,
            field: 'Decommissioned',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'Login',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var Login = model.Login;
    var ApplicationRole = model.ApplicationRole;
    var ClaimValue = model.ClaimValue;
    var LoginAttempt = model.LoginAttempt;
    var LoginPermissionType = model.LoginPermissionType;
    var SiteType = model.SiteType;
    var ApplicationType = model.ApplicationType;
    var RoleType = model.RoleType;
    var ClaimType = model.ClaimType;
    var PermissionType = model.PermissionType;

    Login.hasMany(ApplicationRole, {
        as: 'ApplicationRoleLoginFks',
        foreignKey: 'LoginID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Login.hasMany(ClaimValue, {
        as: 'ClaimValueLoginFks',
        foreignKey: 'LoginID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Login.hasMany(LoginAttempt, {
        as: 'AttemptLoginFks',
        foreignKey: 'LoginID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Login.hasMany(LoginPermissionType, {
        as: 'PermissionTypeLoginFks',
        foreignKey: 'LoginID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Login.belongsTo(SiteType, {
        as: 'SiteType',
        foreignKey: 'SiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Login.belongsToMany(ApplicationType, {
        as: 'ApplicationRoleApplicationTypes',
        through: ApplicationRole,
        foreignKey: 'LoginID',
        otherKey: 'ApplicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Login.belongsToMany(RoleType, {
        as: 'ApplicationRoleRoleTypes',
        through: ApplicationRole,
        foreignKey: 'LoginID',
        otherKey: 'RoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Login.belongsToMany(ClaimType, {
        as: 'ClaimValueClaimTypes',
        through: ClaimValue,
        foreignKey: 'LoginID',
        otherKey: 'ClaimTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Login.belongsToMany(ApplicationType, {
        as: 'LoginPermissionTypeApplicationTypes',
        through: LoginPermissionType,
        foreignKey: 'LoginID',
        otherKey: 'ApplicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Login.belongsToMany(PermissionType, {
        as: 'LoginPermissionTypePermissionTypes',
        through: LoginPermissionType,
        foreignKey: 'LoginID',
        otherKey: 'PermissionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
