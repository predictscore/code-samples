'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportComponentAnalysisContributingValueTyp', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(150),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportComponentAnalysisContributingValueTyp',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportComponentAnalysisContributingValueTyp = model.IdentityVerificationReportComponentAnalysisContributingValueTyp;
    var IdentityVerificationReportComponentAnalysis = model.IdentityVerificationReportComponentAnalysis;
    var IdentityVerificationReportComponentSearchNameType = model.IdentityVerificationReportComponentSearchNameType;
    var IdentityVerificationReportComponentAnalysisCategoryType = model.IdentityVerificationReportComponentAnalysisCategoryType;
    var IdentityVerificationReport = model.IdentityVerificationReport;

    IdentityVerificationReportComponentAnalysisContributingValueTyp.hasMany(IdentityVerificationReportComponentAnalysis, {
        as: 'FKIdentityverificationreportcomponentanalysisIdentityverifi10s',
        foreignKey: 'ContributePhoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.hasMany(IdentityVerificationReportComponentAnalysis, {
        as: 'FKIdentityverificationreportcomponentanalysisIdentityverifi11s',
        foreignKey: 'ContributeTransactionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.hasMany(IdentityVerificationReportComponentAnalysis, {
        as: 'FKIdentityverificationreportcomponentanalysisIdentityverific2s',
        foreignKey: 'ContributeAddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.hasMany(IdentityVerificationReportComponentAnalysis, {
        as: 'FKIdentityverificationreportcomponentanalysisIdentityverific3s',
        foreignKey: 'ContributeDateOfBirthTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.hasMany(IdentityVerificationReportComponentAnalysis, {
        as: 'FKIdentityverificationreportcomponentanalysisIdentityverific4s',
        foreignKey: 'ContributeDocumentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.hasMany(IdentityVerificationReportComponentAnalysis, {
        as: 'FKIdentityverificationreportcomponentanalysisIdentityverific5s',
        foreignKey: 'ContributeDriversTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.hasMany(IdentityVerificationReportComponentAnalysis, {
        as: 'FKIdentityverificationreportcomponentanalysisIdentityverific6s',
        foreignKey: 'ContributeEntitlementTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.hasMany(IdentityVerificationReportComponentAnalysis, {
        as: 'FKIdentityverificationreportcomponentanalysisIdentityverific7s',
        foreignKey: 'ContributeMultipleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.hasMany(IdentityVerificationReportComponentAnalysis, {
        as: 'FKIdentityverificationreportcomponentanalysisIdentityverific8s',
        foreignKey: 'ContributeNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.hasMany(IdentityVerificationReportComponentAnalysis, {
        as: 'FKIdentityverificationreportcomponentanalysisIdentityverific9s',
        foreignKey: 'ContributePassportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeTransactionTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePhoneTypeID',
        otherKey: 'ContributeTransactionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentAnalysisSearchNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePhoneTypeID',
        otherKey: 'SearchNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisCategoryType, {
        as: 'IdentityVerificationReportComponentAnalysisCategoryTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePhoneTypeID',
        otherKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeAddressTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePhoneTypeID',
        otherKey: 'ContributeAddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDateOfBirthTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePhoneTypeID',
        otherKey: 'ContributeDateOfBirthTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDocumentTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePhoneTypeID',
        otherKey: 'ContributeDocumentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDriversTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePhoneTypeID',
        otherKey: 'ContributeDriversTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeEntitlementTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePhoneTypeID',
        otherKey: 'ContributeEntitlementTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeMultipleTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePhoneTypeID',
        otherKey: 'ContributeMultipleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePhoneTypeID',
        otherKey: 'ContributeNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePassportTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePhoneTypeID',
        otherKey: 'ContributePassportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentAnalysisIdentityVerificationReports',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePhoneTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePhoneTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeTransactionTypeID',
        otherKey: 'ContributePhoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentAnalysisSearchNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeTransactionTypeID',
        otherKey: 'SearchNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisCategoryType, {
        as: 'IdentityVerificationReportComponentAnalysisCategoryTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeTransactionTypeID',
        otherKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeAddressTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeTransactionTypeID',
        otherKey: 'ContributeAddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDateOfBirthTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeTransactionTypeID',
        otherKey: 'ContributeDateOfBirthTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDocumentTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeTransactionTypeID',
        otherKey: 'ContributeDocumentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDriversTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeTransactionTypeID',
        otherKey: 'ContributeDriversTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeEntitlementTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeTransactionTypeID',
        otherKey: 'ContributeEntitlementTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeMultipleTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeTransactionTypeID',
        otherKey: 'ContributeMultipleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeTransactionTypeID',
        otherKey: 'ContributeNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePassportTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeTransactionTypeID',
        otherKey: 'ContributePassportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentAnalysisIdentityVerificationReports',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeTransactionTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePhoneTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeAddressTypeID',
        otherKey: 'ContributePhoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeTransactionTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeAddressTypeID',
        otherKey: 'ContributeTransactionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentAnalysisSearchNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeAddressTypeID',
        otherKey: 'SearchNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisCategoryType, {
        as: 'IdentityVerificationReportComponentAnalysisCategoryTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeAddressTypeID',
        otherKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDateOfBirthTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeAddressTypeID',
        otherKey: 'ContributeDateOfBirthTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDocumentTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeAddressTypeID',
        otherKey: 'ContributeDocumentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDriversTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeAddressTypeID',
        otherKey: 'ContributeDriversTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeEntitlementTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeAddressTypeID',
        otherKey: 'ContributeEntitlementTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeMultipleTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeAddressTypeID',
        otherKey: 'ContributeMultipleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeAddressTypeID',
        otherKey: 'ContributeNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePassportTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeAddressTypeID',
        otherKey: 'ContributePassportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentAnalysisIdentityVerificationReports',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeAddressTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePhoneTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDateOfBirthTypeID',
        otherKey: 'ContributePhoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeTransactionTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDateOfBirthTypeID',
        otherKey: 'ContributeTransactionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentAnalysisSearchNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDateOfBirthTypeID',
        otherKey: 'SearchNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisCategoryType, {
        as: 'IdentityVerificationReportComponentAnalysisCategoryTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDateOfBirthTypeID',
        otherKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeAddressTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDateOfBirthTypeID',
        otherKey: 'ContributeAddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDocumentTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDateOfBirthTypeID',
        otherKey: 'ContributeDocumentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDriversTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDateOfBirthTypeID',
        otherKey: 'ContributeDriversTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeEntitlementTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDateOfBirthTypeID',
        otherKey: 'ContributeEntitlementTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeMultipleTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDateOfBirthTypeID',
        otherKey: 'ContributeMultipleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDateOfBirthTypeID',
        otherKey: 'ContributeNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePassportTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDateOfBirthTypeID',
        otherKey: 'ContributePassportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentAnalysisIdentityVerificationReports',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDateOfBirthTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePhoneTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDocumentTypeID',
        otherKey: 'ContributePhoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeTransactionTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDocumentTypeID',
        otherKey: 'ContributeTransactionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentAnalysisSearchNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDocumentTypeID',
        otherKey: 'SearchNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisCategoryType, {
        as: 'IdentityVerificationReportComponentAnalysisCategoryTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDocumentTypeID',
        otherKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeAddressTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDocumentTypeID',
        otherKey: 'ContributeAddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDateOfBirthTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDocumentTypeID',
        otherKey: 'ContributeDateOfBirthTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDriversTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDocumentTypeID',
        otherKey: 'ContributeDriversTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeEntitlementTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDocumentTypeID',
        otherKey: 'ContributeEntitlementTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeMultipleTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDocumentTypeID',
        otherKey: 'ContributeMultipleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDocumentTypeID',
        otherKey: 'ContributeNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePassportTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDocumentTypeID',
        otherKey: 'ContributePassportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentAnalysisIdentityVerificationReports',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDocumentTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePhoneTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDriversTypeID',
        otherKey: 'ContributePhoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeTransactionTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDriversTypeID',
        otherKey: 'ContributeTransactionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentAnalysisSearchNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDriversTypeID',
        otherKey: 'SearchNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisCategoryType, {
        as: 'IdentityVerificationReportComponentAnalysisCategoryTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDriversTypeID',
        otherKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeAddressTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDriversTypeID',
        otherKey: 'ContributeAddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDateOfBirthTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDriversTypeID',
        otherKey: 'ContributeDateOfBirthTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDocumentTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDriversTypeID',
        otherKey: 'ContributeDocumentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeEntitlementTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDriversTypeID',
        otherKey: 'ContributeEntitlementTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeMultipleTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDriversTypeID',
        otherKey: 'ContributeMultipleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDriversTypeID',
        otherKey: 'ContributeNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePassportTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDriversTypeID',
        otherKey: 'ContributePassportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentAnalysisIdentityVerificationReports',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeDriversTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePhoneTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeEntitlementTypeID',
        otherKey: 'ContributePhoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeTransactionTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeEntitlementTypeID',
        otherKey: 'ContributeTransactionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentAnalysisSearchNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeEntitlementTypeID',
        otherKey: 'SearchNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisCategoryType, {
        as: 'IdentityVerificationReportComponentAnalysisCategoryTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeEntitlementTypeID',
        otherKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeAddressTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeEntitlementTypeID',
        otherKey: 'ContributeAddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDateOfBirthTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeEntitlementTypeID',
        otherKey: 'ContributeDateOfBirthTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDocumentTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeEntitlementTypeID',
        otherKey: 'ContributeDocumentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDriversTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeEntitlementTypeID',
        otherKey: 'ContributeDriversTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeMultipleTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeEntitlementTypeID',
        otherKey: 'ContributeMultipleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeEntitlementTypeID',
        otherKey: 'ContributeNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePassportTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeEntitlementTypeID',
        otherKey: 'ContributePassportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentAnalysisIdentityVerificationReports',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeEntitlementTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePhoneTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeMultipleTypeID',
        otherKey: 'ContributePhoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeTransactionTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeMultipleTypeID',
        otherKey: 'ContributeTransactionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentAnalysisSearchNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeMultipleTypeID',
        otherKey: 'SearchNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisCategoryType, {
        as: 'IdentityVerificationReportComponentAnalysisCategoryTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeMultipleTypeID',
        otherKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeAddressTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeMultipleTypeID',
        otherKey: 'ContributeAddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDateOfBirthTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeMultipleTypeID',
        otherKey: 'ContributeDateOfBirthTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDocumentTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeMultipleTypeID',
        otherKey: 'ContributeDocumentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDriversTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeMultipleTypeID',
        otherKey: 'ContributeDriversTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeEntitlementTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeMultipleTypeID',
        otherKey: 'ContributeEntitlementTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeMultipleTypeID',
        otherKey: 'ContributeNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePassportTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeMultipleTypeID',
        otherKey: 'ContributePassportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentAnalysisIdentityVerificationReports',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeMultipleTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePhoneTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeNameTypeID',
        otherKey: 'ContributePhoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeTransactionTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeNameTypeID',
        otherKey: 'ContributeTransactionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentAnalysisSearchNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeNameTypeID',
        otherKey: 'SearchNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisCategoryType, {
        as: 'IdentityVerificationReportComponentAnalysisCategoryTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeNameTypeID',
        otherKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeAddressTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeNameTypeID',
        otherKey: 'ContributeAddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDateOfBirthTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeNameTypeID',
        otherKey: 'ContributeDateOfBirthTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDocumentTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeNameTypeID',
        otherKey: 'ContributeDocumentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDriversTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeNameTypeID',
        otherKey: 'ContributeDriversTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeEntitlementTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeNameTypeID',
        otherKey: 'ContributeEntitlementTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeMultipleTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeNameTypeID',
        otherKey: 'ContributeMultipleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePassportTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeNameTypeID',
        otherKey: 'ContributePassportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentAnalysisIdentityVerificationReports',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributeNameTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePhoneTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePassportTypeID',
        otherKey: 'ContributePhoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeTransactionTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePassportTypeID',
        otherKey: 'ContributeTransactionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentAnalysisSearchNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePassportTypeID',
        otherKey: 'SearchNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisCategoryType, {
        as: 'IdentityVerificationReportComponentAnalysisCategoryTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePassportTypeID',
        otherKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeAddressTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePassportTypeID',
        otherKey: 'ContributeAddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDateOfBirthTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePassportTypeID',
        otherKey: 'ContributeDateOfBirthTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDocumentTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePassportTypeID',
        otherKey: 'ContributeDocumentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDriversTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePassportTypeID',
        otherKey: 'ContributeDriversTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeEntitlementTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePassportTypeID',
        otherKey: 'ContributeEntitlementTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeMultipleTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePassportTypeID',
        otherKey: 'ContributeMultipleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePassportTypeID',
        otherKey: 'ContributeNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisContributingValueTyp.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentAnalysisIdentityVerificationReports',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'ContributePassportTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
