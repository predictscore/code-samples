'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('BrokerProgramAgreement', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            autoIncrement: true
        },
        partyRoleID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        pMSProgramID: {
            type: DataTypes.INTEGER,
            field: 'PMSProgramID',
            allowNull: true
        },
        effective: {
            type: DataTypes.DATEONLY,
            field: 'Effective',
            allowNull: false
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'BrokerProgramAgreement',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var BrokerProgramAgreement = model.BrokerProgramAgreement;
    var PartyRole = model.PartyRole;

    BrokerProgramAgreement.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrokerProgramAgreement.belongsTo(PartyRole, {
        as: 'PartyRole',
        foreignKey: 'PartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
