'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('SecondaryAuthority', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        code: {
            type: DataTypes.STRING(512),
            field: 'Code',
            allowNull: false
        },
        token: {
            type: DataTypes.STRING(512),
            field: 'Token',
            allowNull: true
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        expired: {
            type: DataTypes.DATE,
            field: 'Expired',
            allowNull: true
        },
        attempts: {
            type: DataTypes.INTEGER,
            field: 'Attempts',
            allowNull: false
        },
        lifespan: {
            type: DataTypes.BIGINT,
            field: 'Lifespan',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'SecondaryAuthority',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var SecondaryAuthority = model.SecondaryAuthority;
    var SecondaryAuthorityParameter = model.SecondaryAuthorityParameter;

    SecondaryAuthority.hasMany(SecondaryAuthorityParameter, {
        as: 'ParameterSecondaryauthorityFks',
        foreignKey: 'SecondaryAuthorityID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
