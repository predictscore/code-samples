'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyAccreditation', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: false,
            references: {
                model: 'Party',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        insurerPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'InsurerPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        policyNumber: {
            type: DataTypes.STRING(100),
            field: 'PolicyNumber',
            allowNull: false
        },
        coverAmount: {
            type: DataTypes.DECIMAL(18, 4),
            field: 'CoverAmount',
            allowNull: false
        },
        policyDate: {
            type: DataTypes.DATEONLY,
            field: 'PolicyDate',
            allowNull: false
        },
        expiryDate: {
            type: DataTypes.DATEONLY,
            field: 'ExpiryDate',
            allowNull: false
        },
        certificateOfCurrencySuppliedDate: {
            type: DataTypes.DATEONLY,
            field: 'CertificateOfCurrencySuppliedDate',
            allowNull: true
        },
        expiryEmailSentDate: {
            type: DataTypes.DATEONLY,
            field: 'ExpiryEmailSentDate',
            allowNull: true
        },
        confirmRequestedDate: {
            type: DataTypes.DATEONLY,
            field: 'ConfirmRequestedDate',
            allowNull: true
        },
        confirmReceivedDate: {
            type: DataTypes.DATEONLY,
            field: 'ConfirmReceivedDate',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false,
            defaultValue: true
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        lastUpdated: {
            type: DataTypes.DATE,
            field: 'LastUpdated',
            allowNull: false
        },
        lastUpdatedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'LastUpdatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'PartyAccreditation',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyAccreditation = model.PartyAccreditation;
    var PartyRole = model.PartyRole;
    var Party = model.Party;

    PartyAccreditation.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyAccreditation.belongsTo(PartyRole, {
        as: 'InsurerPartyRole',
        foreignKey: 'InsurerPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyAccreditation.belongsTo(PartyRole, {
        as: 'LastUpdatedByPartyRole',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyAccreditation.belongsTo(Party, {
        as: 'Party',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
