'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportComponentSearchNameType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(150),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportComponentSearchNameType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportComponentSearchNameType = model.IdentityVerificationReportComponentSearchNameType;
    var IdentityVerificationReportComponentAnalysis = model.IdentityVerificationReportComponentAnalysis;
    var IdentityVerificationReportComponentSearch = model.IdentityVerificationReportComponentSearch;
    var IdentityVerificationReportComponentAnalysisContributingValueTyp = model.IdentityVerificationReportComponentAnalysisContributingValueTyp;
    var IdentityVerificationReportComponentAnalysisCategoryType = model.IdentityVerificationReportComponentAnalysisCategoryType;
    var IdentityVerificationReport = model.IdentityVerificationReport;
    var IdentityVerificationReportComponentSearchMatchIndicatorType = model.IdentityVerificationReportComponentSearchMatchIndicatorType;
    var IdentityVerificationReportComponentSearchResultCodeType = model.IdentityVerificationReportComponentSearchResultCodeType;
    var IdentityVerificationReportComponentSearchType = model.IdentityVerificationReportComponentSearchType;

    IdentityVerificationReportComponentSearchNameType.hasMany(IdentityVerificationReportComponentAnalysis, {
        as: 'FKIdentityverificationreportcomponentanalysisIdentityverifi12s',
        foreignKey: 'SearchNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.hasMany(IdentityVerificationReportComponentSearch, {
        as: 'FKIdentityverificationreportcomponentsearchesIdentityverific2s',
        foreignKey: 'NameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePhoneTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'SearchNameTypeID',
        otherKey: 'ContributePhoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeTransactionTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'SearchNameTypeID',
        otherKey: 'ContributeTransactionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReportComponentAnalysisCategoryType, {
        as: 'IdentityVerificationReportComponentAnalysisCategoryTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'SearchNameTypeID',
        otherKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeAddressTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'SearchNameTypeID',
        otherKey: 'ContributeAddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDateOfBirthTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'SearchNameTypeID',
        otherKey: 'ContributeDateOfBirthTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDocumentTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'SearchNameTypeID',
        otherKey: 'ContributeDocumentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDriversTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'SearchNameTypeID',
        otherKey: 'ContributeDriversTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeEntitlementTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'SearchNameTypeID',
        otherKey: 'ContributeEntitlementTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeMultipleTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'SearchNameTypeID',
        otherKey: 'ContributeMultipleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'SearchNameTypeID',
        otherKey: 'ContributeNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePassportTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'SearchNameTypeID',
        otherKey: 'ContributePassportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentAnalysisIdentityVerificationReports',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'SearchNameTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReportComponentSearchMatchIndicatorType, {
        as: 'IdentityVerificationReportComponentSearchMatchIndicatorTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'NameTypeID',
        otherKey: 'MatchIndicatorTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReportComponentSearchResultCodeType, {
        as: 'IdentityVerificationReportComponentSearchResultCodeTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'NameTypeID',
        otherKey: 'ResultCodeTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReportComponentSearchType, {
        as: 'IdentityVerificationReportComponentSearchSearchTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'NameTypeID',
        otherKey: 'SearchTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchNameType.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentSearchIdentityVerificationReports',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'NameTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
