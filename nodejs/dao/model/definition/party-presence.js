'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyPresence', {
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'Party',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        presenceTypeID: {
            type: DataTypes.INTEGER,
            field: 'PresenceTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'PresenceType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        value: {
            type: DataTypes.STRING(512),
            field: 'Value',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'PartyPresence',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyPresence = model.PartyPresence;
    var Party = model.Party;
    var PresenceType = model.PresenceType;

    PartyPresence.belongsTo(Party, {
        as: 'Party',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyPresence.belongsTo(PresenceType, {
        as: 'PresenceType',
        foreignKey: 'PresenceTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
