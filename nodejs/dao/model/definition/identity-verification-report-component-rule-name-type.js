'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportComponentRuleNameType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(500),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportComponentRuleNameType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportComponentRuleNameType = model.IdentityVerificationReportComponentRuleNameType;
    var IdentityVerificationReportComponentRule = model.IdentityVerificationReportComponentRule;
    var IdentityVerificationReportComponentRuleReasonType = model.IdentityVerificationReportComponentRuleReasonType;
    var IdentityVerificationReport = model.IdentityVerificationReport;

    IdentityVerificationReportComponentRuleNameType.hasMany(IdentityVerificationReportComponentRule, {
        as: 'FKIdentityverificationreportcomponentrulesIdentityverificati1s',
        foreignKey: 'NameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentRuleNameType.belongsToMany(IdentityVerificationReportComponentRuleReasonType, {
        as: 'IdentityVerificationReportComponentRuleReasonTypes',
        through: IdentityVerificationReportComponentRule,
        foreignKey: 'NameTypeID',
        otherKey: 'ReasonTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentRuleNameType.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentRuleIdentityVerificationReports',
        through: IdentityVerificationReportComponentRule,
        foreignKey: 'NameTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
