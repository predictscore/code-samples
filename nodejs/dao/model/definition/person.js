'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Person', {
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'Party',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        salutationTypeID: {
            type: DataTypes.INTEGER,
            field: 'SalutationTypeID',
            allowNull: true,
            references: {
                model: 'SalutationType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        dateOfBirth: {
            type: DataTypes.DATEONLY,
            field: 'DateOfBirth',
            allowNull: true
        },
        firstName: {
            type: DataTypes.STRING(150),
            field: 'FirstName',
            allowNull: false
        },
        middleName: {
            type: DataTypes.STRING(150),
            field: 'MiddleName',
            allowNull: true
        },
        lastName: {
            type: DataTypes.STRING(150),
            field: 'LastName',
            allowNull: false
        },
        preferredName: {
            type: DataTypes.STRING(150),
            field: 'PreferredName',
            allowNull: true
        },
        title: {
            type: DataTypes.STRING(150),
            field: 'Title',
            allowNull: true
        },
        genderTypeID: {
            type: DataTypes.INTEGER,
            field: 'GenderTypeID',
            allowNull: true,
            references: {
                model: 'GenderType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        maritalStatusTypeID: {
            type: DataTypes.INTEGER,
            field: 'MaritalStatusTypeID',
            allowNull: true,
            references: {
                model: 'MaritalStatusType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        maritalStatusChangeDate: {
            type: DataTypes.DATEONLY,
            field: 'MaritalStatusChangeDate',
            allowNull: true
        },
        dependantsUnder11: {
            type: DataTypes.INTEGER,
            field: 'DependantsUnder11',
            allowNull: true
        },
        dependantsOver11: {
            type: DataTypes.INTEGER,
            field: 'DependantsOver11',
            allowNull: true
        },
        driversLicenseNumber: {
            type: DataTypes.STRING(15),
            field: 'DriversLicenseNumber',
            allowNull: true
        },
        accountantPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'AccountantPartyRoleID',
            allowNull: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        sendMarketingMaterial: {
            type: DataTypes.BOOLEAN,
            field: 'SendMarketingMaterial',
            allowNull: true,
            defaultValue: true
        },
        declarationConfirmed: {
            type: DataTypes.BOOLEAN,
            field: 'DeclarationConfirmed',
            allowNull: true
        },
        numberOfCars: {
            type: DataTypes.INTEGER,
            field: 'NumberOfCars',
            allowNull: true
        },
        numberOfJobs: {
            type: DataTypes.INTEGER,
            field: 'NumberOfJobs',
            allowNull: true
        },
        companyCar: {
            type: DataTypes.BOOLEAN,
            field: 'CompanyCar',
            allowNull: true
        },
        stateID: {
            type: DataTypes.INTEGER,
            field: 'StateID',
            allowNull: true,
            references: {
                model: 'State',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        cardNumber: {
            type: DataTypes.STRING(50),
            field: 'CardNumber',
            allowNull: true
        },
        expiryDate: {
            type: DataTypes.DATEONLY,
            field: 'ExpiryDate',
            allowNull: true
        },
        medicareNumber: {
            type: DataTypes.STRING(50),
            field: 'MedicareNumber',
            allowNull: true
        },
        referenceNumber: {
            type: DataTypes.INTEGER,
            field: 'ReferenceNumber',
            allowNull: true
        },
        passportNumber: {
            type: DataTypes.STRING(50),
            field: 'PassportNumber',
            allowNull: true
        },
        verifyIdentityByCRA: {
            type: DataTypes.BOOLEAN,
            field: 'VerifyIdentityByCRA',
            allowNull: true
        },
        nameChangeReasonTypeID: {
            type: DataTypes.INTEGER,
            field: 'NameChangeReasonTypeID',
            allowNull: true,
            references: {
                model: 'NameChangeReasonType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        nameChangeDate: {
            type: DataTypes.DATEONLY,
            field: 'NameChangeDate',
            allowNull: true
        },
        lastUpdated: {
            type: DataTypes.DATE,
            field: 'LastUpdated',
            allowNull: false
        },
        lastUpdatedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'LastUpdatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        v8ID: {
            type: DataTypes.INTEGER,
            field: 'V8ID',
            allowNull: true
        },
        medicareCardColourTypeID: {
            type: DataTypes.INTEGER,
            field: 'MedicareCardColourTypeID',
            allowNull: true,
            references: {
                model: 'ObjectColourType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        passportColourTypeID: {
            type: DataTypes.INTEGER,
            field: 'PassportColourTypeID',
            allowNull: true,
            references: {
                model: 'ObjectColourType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        passportCountryID: {
            type: DataTypes.INTEGER,
            field: 'PassportCountryID',
            allowNull: true,
            references: {
                model: 'Country',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        medicareNameOnCard: {
            type: DataTypes.STRING(150),
            field: 'MedicareNameOnCard',
            allowNull: true
        },
        medicareValidToDate: {
            type: DataTypes.DATEONLY,
            field: 'MedicareValidToDate',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'Person',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var Person = model.Person;
    var PartyRole = model.PartyRole;
    var GenderType = model.GenderType;
    var MaritalStatusType = model.MaritalStatusType;
    var ObjectColourType = model.ObjectColourType;
    var NameChangeReasonType = model.NameChangeReasonType;
    var Party = model.Party;
    var Country = model.Country;
    var SalutationType = model.SalutationType;
    var State = model.State;

    Person.hasMany(PartyRole, {
        as: 'RolePartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Person.belongsTo(PartyRole, {
        as: 'AccountantPartyRole',
        foreignKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Person.belongsTo(GenderType, {
        as: 'GenderType',
        foreignKey: 'GenderTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Person.belongsTo(PartyRole, {
        as: 'LastUpdatedByPartyRole',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Person.belongsTo(MaritalStatusType, {
        as: 'MaritalStatusType',
        foreignKey: 'MaritalStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Person.belongsTo(ObjectColourType, {
        as: 'MedicareCardColourType',
        foreignKey: 'MedicareCardColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Person.belongsTo(NameChangeReasonType, {
        as: 'NameChangeReasonType',
        foreignKey: 'NameChangeReasonTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Person.belongsTo(Party, {
        as: 'Party',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Person.belongsTo(ObjectColourType, {
        as: 'PassportColourType',
        foreignKey: 'PassportColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Person.belongsTo(Country, {
        as: 'PassportCountry',
        foreignKey: 'PassportCountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Person.belongsTo(SalutationType, {
        as: 'SalutationType',
        foreignKey: 'SalutationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Person.belongsTo(State, {
        as: 'State',
        foreignKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
