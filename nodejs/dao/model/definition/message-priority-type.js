'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('MessagePriorityType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(512),
            field: 'Name',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'MessagePriorityType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var MessagePriorityType = model.MessagePriorityType;
    var Message = model.Message;
    var MessageType = model.MessageType;
    var SiteType = model.SiteType;

    MessagePriorityType.hasMany(Message, {
        as: 'MessageMessageprioritytypeFks',
        foreignKey: 'MessagePriorityTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    MessagePriorityType.belongsToMany(MessageType, {
        as: 'MessageMessageTypes',
        through: Message,
        foreignKey: 'MessagePriorityTypeID',
        otherKey: 'MessageTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    MessagePriorityType.belongsToMany(Message, {
        as: 'MessageParents',
        through: Message,
        foreignKey: 'MessagePriorityTypeID',
        otherKey: 'ParentID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    MessagePriorityType.belongsToMany(SiteType, {
        as: 'MessageReadFromSiteTypes',
        through: Message,
        foreignKey: 'MessagePriorityTypeID',
        otherKey: 'ReadFromSiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    MessagePriorityType.belongsToMany(SiteType, {
        as: 'MessageSentFromSiteTypes',
        through: Message,
        foreignKey: 'MessagePriorityTypeID',
        otherKey: 'SentFromSiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
