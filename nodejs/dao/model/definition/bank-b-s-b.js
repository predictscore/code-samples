'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('BankBSB', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        bSB: {
            type: DataTypes.CHAR(6),
            field: 'BSB',
            allowNull: false
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        branch: {
            type: DataTypes.STRING(200),
            field: 'Branch',
            allowNull: true
        },
        addressID: {
            type: DataTypes.INTEGER,
            field: 'AddressID',
            allowNull: true,
            references: {
                model: 'Address',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        lastUpdated: {
            type: DataTypes.DATE,
            field: 'LastUpdated',
            allowNull: false
        },
        lastUpdatedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'LastUpdatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        tontoID: {
            type: DataTypes.INTEGER,
            field: 'TontoID',
            allowNull: true
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'BankBSB',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var BankBSB = model.BankBSB;
    var BankAccount = model.BankAccount;
    var PartyBankAccount = model.PartyBankAccount;
    var Address = model.Address;
    var PartyRole = model.PartyRole;
    var PartyBankAccountType = model.PartyBankAccountType;
    var Party = model.Party;

    BankBSB.hasMany(BankAccount, {
        as: 'BankAccountBankbsbFks',
        foreignKey: 'BankBSBID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankBSB.hasMany(PartyBankAccount, {
        as: 'PartyBankAccountBankbsbFks',
        foreignKey: 'BankBSBID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankBSB.belongsTo(Address, {
        as: 'Address',
        foreignKey: 'AddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankBSB.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankBSB.belongsTo(PartyRole, {
        as: 'LastUpdatedByPartyRole',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankBSB.belongsToMany(PartyRole, {
        as: 'BankAccountCreatedByPartyRoles',
        through: BankAccount,
        foreignKey: 'BankBSBID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankBSB.belongsToMany(PartyRole, {
        as: 'BankAccountLastUpdatedByPartyRoles',
        through: BankAccount,
        foreignKey: 'BankBSBID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankBSB.belongsToMany(BankAccount, {
        as: 'PartyBankAccountBankAccounts',
        through: PartyBankAccount,
        foreignKey: 'BankBSBID',
        otherKey: 'BankAccountID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankBSB.belongsToMany(PartyRole, {
        as: 'PartyBankAccountCreatedByPartyRoles',
        through: PartyBankAccount,
        foreignKey: 'BankBSBID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankBSB.belongsToMany(PartyRole, {
        as: 'PartyBankAccountLastUpdateByPartyRoles',
        through: PartyBankAccount,
        foreignKey: 'BankBSBID',
        otherKey: 'LastUpdateByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankBSB.belongsToMany(PartyBankAccountType, {
        as: 'PartyBankAccountPartyBankAccountTypes',
        through: PartyBankAccount,
        foreignKey: 'BankBSBID',
        otherKey: 'PartyBankAccountTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BankBSB.belongsToMany(Party, {
        as: 'PartyBankAccountParties',
        through: PartyBankAccount,
        foreignKey: 'BankBSBID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
