'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyRoleRelationship', {
        relationshipTypeID: {
            type: DataTypes.INTEGER,
            field: 'RelationshipTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'RelationshipType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        parentPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'ParentPartyRoleID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        childPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'ChildPartyRoleID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'PartyRoleRelationship',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyRoleRelationship = model.PartyRoleRelationship;
    var PartyRole = model.PartyRole;
    var RelationshipType = model.RelationshipType;

    PartyRoleRelationship.belongsTo(PartyRole, {
        as: 'ChildPartyRole',
        foreignKey: 'ChildPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleRelationship.belongsTo(PartyRole, {
        as: 'ParentPartyRole',
        foreignKey: 'ParentPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleRelationship.belongsTo(RelationshipType, {
        as: 'RelationshipType',
        foreignKey: 'RelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
