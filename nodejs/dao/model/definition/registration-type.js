'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('RegistrationType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'RegistrationType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var RegistrationType = model.RegistrationType;
    var PartyCompliance = model.PartyCompliance;
    var PartyRole = model.PartyRole;
    var Party = model.Party;

    RegistrationType.hasMany(PartyCompliance, {
        as: 'PartyComplianceRegistrationtypeFks',
        foreignKey: 'RegistrationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RegistrationType.belongsToMany(PartyRole, {
        as: 'PartyComplianceCreatedByPartyRoles',
        through: PartyCompliance,
        foreignKey: 'RegistrationTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RegistrationType.belongsToMany(PartyRole, {
        as: 'PartyComplianceLastUpdatedByPartyRoles',
        through: PartyCompliance,
        foreignKey: 'RegistrationTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RegistrationType.belongsToMany(Party, {
        as: 'PartyComplianceParties',
        through: PartyCompliance,
        foreignKey: 'RegistrationTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
