'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyBankAccount', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: false,
            references: {
                model: 'Party',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        bankAccountID: {
            type: DataTypes.INTEGER,
            field: 'BankAccountID',
            allowNull: false,
            references: {
                model: 'BankAccount',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        partyBankAccountTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyBankAccountTypeID',
            allowNull: false,
            references: {
                model: 'PartyBankAccountType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        bankBSBID: {
            type: DataTypes.INTEGER,
            field: 'BankBSBID',
            allowNull: false,
            references: {
                model: 'BankBSB',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: true
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        lastUpdated: {
            type: DataTypes.DATE,
            field: 'LastUpdated',
            allowNull: true
        },
        lastUpdateByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'LastUpdateByPartyRoleID',
            allowNull: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        treasuryApproved: {
            type: DataTypes.DATE,
            field: 'TreasuryApproved',
            allowNull: true
        },
        treasuryApprovedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'TreasuryApprovedByPartyRoleID',
            allowNull: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'PartyBankAccount',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyBankAccount = model.PartyBankAccount;
    var BankAccount = model.BankAccount;
    var BankBSB = model.BankBSB;
    var PartyRole = model.PartyRole;
    var PartyBankAccountType = model.PartyBankAccountType;
    var Party = model.Party;

    PartyBankAccount.belongsTo(BankAccount, {
        as: 'BankAccount',
        foreignKey: 'BankAccountID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyBankAccount.belongsTo(BankBSB, {
        as: 'BankBSB',
        foreignKey: 'BankBSBID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyBankAccount.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyBankAccount.belongsTo(PartyRole, {
        as: 'LastUpdateByPartyRole',
        foreignKey: 'LastUpdateByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyBankAccount.belongsTo(PartyBankAccountType, {
        as: 'PartyBankAccountType',
        foreignKey: 'PartyBankAccountTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyBankAccount.belongsTo(Party, {
        as: 'Party',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyBankAccount.belongsTo(PartyRole, {
        as: 'TreasuryApprovedByPartyRole',
        foreignKey: 'TreasuryApprovedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
