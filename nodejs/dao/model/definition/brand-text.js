'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('BrandText', {
        partyRoleID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        brandTypeID: {
            type: DataTypes.INTEGER,
            field: 'BrandTypeID',
            allowNull: true,
            references: {
                model: 'BrandType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        currencyTypeID: {
            type: DataTypes.INTEGER,
            field: 'CurrencyTypeID',
            allowNull: true,
            references: {
                model: 'CurrencyType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        documentLocationTypeID: {
            type: DataTypes.INTEGER,
            field: 'DocumentLocationTypeID',
            allowNull: true,
            references: {
                model: 'DocumentLocationType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        alignmentTypeID: {
            type: DataTypes.INTEGER,
            field: 'AlignmentTypeID',
            allowNull: true,
            references: {
                model: 'AlignmentType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        text: {
            type: DataTypes.TEXT,
            field: 'Text',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'BrandText',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var BrandText = model.BrandText;
    var AlignmentType = model.AlignmentType;
    var BrandType = model.BrandType;
    var CurrencyType = model.CurrencyType;
    var DocumentLocationType = model.DocumentLocationType;
    var PartyRole = model.PartyRole;

    BrandText.belongsTo(AlignmentType, {
        as: 'AlignmentType',
        foreignKey: 'AlignmentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandText.belongsTo(BrandType, {
        as: 'BrandType',
        foreignKey: 'BrandTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandText.belongsTo(CurrencyType, {
        as: 'CurrencyType',
        foreignKey: 'CurrencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandText.belongsTo(DocumentLocationType, {
        as: 'DocumentLocationType',
        foreignKey: 'DocumentLocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandText.belongsTo(PartyRole, {
        as: 'PartyRole',
        foreignKey: 'PartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
