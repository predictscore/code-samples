'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('RelationshipTypeSecondaryRelationshipType', {
        rootRelationshipTypeID: {
            type: DataTypes.INTEGER,
            field: 'RootRelationshipTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'RelationshipType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        secondaryRelationshipTypeID: {
            type: DataTypes.INTEGER,
            field: 'SecondaryRelationshipTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'RelationshipType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'RelationshipTypeSecondaryRelationshipType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var RelationshipTypeSecondaryRelationshipType = model.RelationshipTypeSecondaryRelationshipType;
    var RelationshipType = model.RelationshipType;

    RelationshipTypeSecondaryRelationshipType.belongsTo(RelationshipType, {
        as: 'RootRelationshipType',
        foreignKey: 'RootRelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipTypeSecondaryRelationshipType.belongsTo(RelationshipType, {
        as: 'SecondaryRelationshipType',
        foreignKey: 'SecondaryRelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
