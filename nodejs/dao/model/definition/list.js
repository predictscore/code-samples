'use strict';

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('List', {
    iD: {
      type: DataTypes.INTEGER,
      field: 'ID',
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(512),
      field: 'Name',
      allowNull: false
    },
    favourite: {
      type: DataTypes.BOOLEAN,
      field: 'Favourite',
      allowNull: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      field: 'Active',
      allowNull: true
    },
    created: {
      type: DataTypes.DATE,
      field: 'Created',
      allowNull: false
    },
    partyTypeFilter: {
      type: DataTypes.INTEGER,
      field: 'PartyTypeFilter',
      allowNull: true
    },
    stateFilter: {
      type: [DataTypes.INTEGER],
      field: 'StateFilter',
      allowNull: true
    },
    partyRoleTypeFilter: {
      type: [DataTypes.INTEGER],
      field: 'PartyRoleTypeFilter',
      allowNull: true
    },
    ownerFilter: {
      type: [DataTypes.INTEGER],
      field: 'OwnerFilter',
      allowNull: true
    },
    statusFilter: {
      type: [DataTypes.INTEGER],
      field: 'StatusFilter',
      allowNull: true
    },
    nameFilter: {
      type: DataTypes.STRING(512),
      field: 'NameFilter',
      allowNull: true
    },
    hasLoginFilter: {
      type: DataTypes.BOOLEAN,
      field: 'HasLoginFilter',
      allowNull: true
    },
    postcodeFilter: {
      type: DataTypes.STRING(512),
      field: 'PostcodeFilter',
      allowNull: true
    },
    lastActivityDateFilter: {
      type: DataTypes.DATE,
      field: 'LastActivityDateFilter',
      allowNull: true
    },
    lastAppDateBefore: {
      type: DataTypes.DATE,
      field: 'Last App Date Before',
      allowNull: true
    },
    createdByPartyRoleID: {
      type: DataTypes.INTEGER,
      field: 'CreatedByPartyRoleID',
      allowNull: false,
      references: {
        model: 'PartyRole',
        key: 'ID'
      },
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION'
    },
    lastUpdated: {
      type: DataTypes.DATE,
      field: 'LastUpdated',
      allowNull: false
    },
    lastUpdatedByPartyRoleID: {
      type: DataTypes.INTEGER,
      field: 'LastUpdatedByPartyRoleID',
      allowNull: false,
      references: {
        model: 'PartyRole',
        key: 'ID'
      },
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION'
    }
  }, {
    schema: 'public',
    tableName: 'List',
    timestamps: false
  });
};

module.exports.initRelations = function() {
  delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
  var model = require('../index');
  var List = model.List;
  var Campaign = model.Campaign;
  var ListExclusion = model.ListExclusion;
  var CreatedBy = model.PartyRole;
  var LastUpdatedBy = model.PartyRole;

  List.belongsTo(CreatedBy, {
    as: 'CreatedBy',
    foreignKey: 'createdByPartyRoleID',
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

  List.belongsTo(LastUpdatedBy, {
    as: 'LastUpdatedBy',
    foreignKey: 'lastUpdatedByPartyRoleID',
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

  List.hasMany(ListExclusion, {
    as: 'ListexclusionFks',
    foreignKey: 'listID',
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

  List.hasMany(Campaign, {
    as: 'CampaignFks',
    foreignKey: 'listID',
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

};
