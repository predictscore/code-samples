'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('LinkParameter', {
        linkID: {
            type: DataTypes.INTEGER,
            field: 'LinkID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'Link',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        name: {
            type: DataTypes.STRING(512),
            field: 'Name',
            allowNull: false,
            primaryKey: true
        },
        value: {
            type: DataTypes.STRING(512),
            field: 'Value',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'LinkParameter',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var LinkParameter = model.LinkParameter;
    var Link = model.Link;

    LinkParameter.belongsTo(Link, {
        as: 'Link',
        foreignKey: 'LinkID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
