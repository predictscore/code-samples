'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('V8Applicationsuppliercrumbspartyrole', {
        applicationContainerID: {
            type: DataTypes.INTEGER,
            field: 'ApplicationContainerID',
            allowNull: false
        },
        loanID: {
            type: DataTypes.INTEGER,
            field: 'LoanID',
            allowNull: true
        },
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: true
        },
        crumbsSupplierPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CrumbsSupplierPartyRoleID',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'v8_ApplicationSupplierCrumbsPartyRole',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var V8Applicationsuppliercrumbspartyrole = model.V8Applicationsuppliercrumbspartyrole;

};
