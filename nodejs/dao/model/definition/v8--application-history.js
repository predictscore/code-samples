'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('V8Applicationhistory', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false
        },
        applicationHistoryTypeID: {
            type: DataTypes.INTEGER,
            field: 'ApplicationHistoryTypeID',
            allowNull: false
        },
        applicationContainerID: {
            type: DataTypes.INTEGER,
            field: 'ApplicationContainerID',
            allowNull: false
        },
        applicationHistoryPublicationTypeID: {
            type: DataTypes.INTEGER,
            field: 'ApplicationHistoryPublicationTypeID',
            allowNull: false
        },
        title: {
            type: DataTypes.STRING(100),
            field: 'Title',
            allowNull: false
        },
        comments: {
            type: DataTypes.STRING(8000),
            field: 'Comments',
            allowNull: true
        },
        followUpDate: {
            type: DataTypes.DATE,
            field: 'FollowUpDate',
            allowNull: true
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: true
        },
        lastUpdated: {
            type: DataTypes.DATE,
            field: 'LastUpdated',
            allowNull: false
        },
        lastUpdatedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'LastUpdatedByPartyRoleID',
            allowNull: true
        },
        active: {
            type: DataTypes.INTEGER,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'v8_ApplicationHistory',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var V8Applicationhistory = model.V8Applicationhistory;

};
