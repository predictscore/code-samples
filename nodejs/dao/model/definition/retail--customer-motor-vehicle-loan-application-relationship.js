'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('RetailCustomermotorvehicleloanapplicationrelationship', {
        customerMotorVehicleLoanApplicationRelationshipID: {
            type: DataTypes.INTEGER,
            field: 'CustomerMotorVehicleLoanApplicationRelationshipID',
            allowNull: false
        },
        motorVehicleLoanApplicationID: {
            type: DataTypes.INTEGER,
            field: 'MotorVehicleLoanApplicationID',
            allowNull: false
        },
        customerID: {
            type: DataTypes.INTEGER,
            field: 'CustomerID',
            allowNull: false
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        rowStatusTypeID: {
            type: DataTypes.INTEGER,
            field: 'RowStatusTypeID',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'retail_CustomerMotorVehicleLoanApplicationRelationship',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var RetailCustomermotorvehicleloanapplicationrelationship = model.RetailCustomermotorvehicleloanapplicationrelationship;

};
