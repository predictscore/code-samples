'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportOutcomeType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(150),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportOutcomeType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportOutcomeType = model.IdentityVerificationReportOutcomeType;
    var IdentityVerificationReportComponentVerification = model.IdentityVerificationReportComponentVerification;
    var IdentityVerificationReportMain = model.IdentityVerificationReportMain;
    var IdentityVerificationReport = model.IdentityVerificationReport;
    var IdentityVerificationReportProfileType = model.IdentityVerificationReportProfileType;

    IdentityVerificationReportOutcomeType.hasMany(IdentityVerificationReportComponentVerification, {
        as: 'FKIdentityverificationreportcomponentverificationIdentityver1s',
        foreignKey: 'IndicatorTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportOutcomeType.hasMany(IdentityVerificationReportMain, {
        as: 'FKIdentityverificationreportmainIdentityverificationreportouts',
        foreignKey: 'OverallOutcomeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportOutcomeType.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentVerificationIdentityVerificationReports',
        through: IdentityVerificationReportComponentVerification,
        foreignKey: 'IndicatorTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportOutcomeType.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportMainIdentityVerificationReports',
        through: IdentityVerificationReportMain,
        foreignKey: 'OverallOutcomeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportOutcomeType.belongsToMany(IdentityVerificationReportProfileType, {
        as: 'IdentityVerificationReportMainProfileTypes',
        through: IdentityVerificationReportMain,
        foreignKey: 'OverallOutcomeID',
        otherKey: 'ProfileTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
