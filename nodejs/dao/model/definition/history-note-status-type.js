'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('HistoryNoteStatusType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'HistoryNoteStatusType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var HistoryNoteStatusType = model.HistoryNoteStatusType;
    var HistoryNote = model.HistoryNote;
    var PartyRole = model.PartyRole;
    var HistoryNoteType = model.HistoryNoteType;
    var Party = model.Party;

    HistoryNoteStatusType.hasMany(HistoryNote, {
        as: 'HistoryNoteHistorynotestatustypeFks',
        foreignKey: 'HistoryNoteStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    HistoryNoteStatusType.belongsToMany(PartyRole, {
        as: 'HistoryNoteCreatedByPartyRoles',
        through: HistoryNote,
        foreignKey: 'HistoryNoteStatusTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    HistoryNoteStatusType.belongsToMany(PartyRole, {
        as: 'HistoryNoteFollowUpByPartyRoles',
        through: HistoryNote,
        foreignKey: 'HistoryNoteStatusTypeID',
        otherKey: 'FollowUpByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    HistoryNoteStatusType.belongsToMany(HistoryNoteType, {
        as: 'HistoryNoteHistoryNoteTypes',
        through: HistoryNote,
        foreignKey: 'HistoryNoteStatusTypeID',
        otherKey: 'HistoryNoteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    HistoryNoteStatusType.belongsToMany(Party, {
        as: 'HistoryNoteParties',
        through: HistoryNote,
        foreignKey: 'HistoryNoteStatusTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
