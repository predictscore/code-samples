'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ValidationCode', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        token: {
            type: DataTypes.STRING(512),
            field: 'Token',
            allowNull: false
        },
        code: {
            type: DataTypes.STRING(50),
            field: 'Code',
            allowNull: false
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        expiry: {
            type: DataTypes.DATE,
            field: 'Expiry',
            allowNull: false
        },
        validated: {
            type: DataTypes.DATE,
            field: 'Validated',
            allowNull: true
        },
        attempts: {
            type: DataTypes.INTEGER,
            field: 'Attempts',
            allowNull: false
        },
        loginID: {
            type: DataTypes.INTEGER,
            field: 'LoginID',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'ValidationCode',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var ValidationCode = model.ValidationCode;

};
