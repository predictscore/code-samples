'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('V8Applicationpartyroleidentity', {
        applicationPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'ApplicationPartyRoleID',
            allowNull: false
        },
        applicationContainerID: {
            type: DataTypes.INTEGER,
            field: 'ApplicationContainerID',
            allowNull: false
        },
        partyRoleID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleID',
            allowNull: false
        },
        applicationPartyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'ApplicationPartyRoleTypeID',
            allowNull: true
        },
        nominatedBorrower: {
            type: DataTypes.BOOLEAN,
            field: 'NominatedBorrower',
            allowNull: true
        },
        crumbsPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CrumbsPartyRoleID',
            allowNull: true
        },
        applicationPartyRoleTypeName: {
            type: DataTypes.STRING(50),
            field: 'ApplicationPartyRoleTypeName',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'v8_ApplicationPartyRoleIdentity',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var V8Applicationpartyroleidentity = model.V8Applicationpartyroleidentity;

};
