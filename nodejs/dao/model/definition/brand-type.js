'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('BrandType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'BrandType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var BrandType = model.BrandType;
    var BrandText = model.BrandText;
    var AlignmentType = model.AlignmentType;
    var CurrencyType = model.CurrencyType;
    var DocumentLocationType = model.DocumentLocationType;
    var PartyRole = model.PartyRole;

    BrandType.hasMany(BrandText, {
        as: 'BrandTextBrandtypeFks',
        foreignKey: 'BrandTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandType.belongsToMany(AlignmentType, {
        as: 'BrandTextAlignmentTypes',
        through: BrandText,
        foreignKey: 'BrandTypeID',
        otherKey: 'AlignmentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandType.belongsToMany(CurrencyType, {
        as: 'BrandTextCurrencyTypes',
        through: BrandText,
        foreignKey: 'BrandTypeID',
        otherKey: 'CurrencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandType.belongsToMany(DocumentLocationType, {
        as: 'BrandTextDocumentLocationTypes',
        through: BrandText,
        foreignKey: 'BrandTypeID',
        otherKey: 'DocumentLocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandType.belongsToMany(PartyRole, {
        as: 'BrandTextPartyRoles',
        through: BrandText,
        foreignKey: 'BrandTypeID',
        otherKey: 'PartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
