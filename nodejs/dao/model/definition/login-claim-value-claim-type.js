'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('LoginClaimValueClaimType', {
        loginID: {
            type: DataTypes.INTEGER,
            field: 'LoginID',
            allowNull: true
        },
        username: {
            type: DataTypes.STRING(100),
            field: 'Username',
            allowNull: true
        },
        value: {
            type: DataTypes.STRING(512),
            field: 'Value',
            allowNull: true
        },
        claimTypeID: {
            type: DataTypes.INTEGER,
            field: 'ClaimTypeID',
            allowNull: true
        },
        claimTypeName: {
            type: DataTypes.STRING(512),
            field: 'ClaimTypeName',
            allowNull: true
        },
        loginActive: {
            type: DataTypes.BOOLEAN,
            field: 'LoginActive',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'LoginClaimValueClaimType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var LoginClaimValueClaimType = model.LoginClaimValueClaimType;

};
