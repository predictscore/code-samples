'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyRoleTypeCRMAccountCategory', {
        partyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleTypeID',
            allowNull: false,
            references: {
                model: 'PartyRoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        cRMAccountCategoryCode: {
            type: DataTypes.INTEGER,
            field: 'CRMAccountCategoryCode',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'PartyRoleTypeCRMAccountCategory',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyRoleTypeCRMAccountCategory = model.PartyRoleTypeCRMAccountCategory;
    var PartyRoleType = model.PartyRoleType;

    PartyRoleTypeCRMAccountCategory.belongsTo(PartyRoleType, {
        as: 'PartyRoleType',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
