'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('V8Applicationequipmentsecurityaddresslink', {
        applicationContainerID: {
            type: DataTypes.INTEGER,
            field: 'ApplicationContainerID',
            allowNull: false
        },
        securityID: {
            type: DataTypes.INTEGER,
            field: 'SecurityID',
            allowNull: true
        },
        crumbsAddressID: {
            type: DataTypes.INTEGER,
            field: 'CrumbsAddressID',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'v8_ApplicationEquipmentSecurityAddressLink',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var V8Applicationequipmentsecurityaddresslink = model.V8Applicationequipmentsecurityaddresslink;

};
