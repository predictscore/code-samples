'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('BrandLevelType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'BrandLevelType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var BrandLevelType = model.BrandLevelType;
    var Brand = model.Brand;
    var PartyRole = model.PartyRole;

    BrandLevelType.hasMany(Brand, {
        as: 'BrandDocumentbrandleveltypeFks',
        foreignKey: 'DocumentBrandLevelTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandLevelType.hasMany(Brand, {
        as: 'BrandOnlinebrandleveltypeFks',
        foreignKey: 'OnlineBrandLevelTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandLevelType.belongsToMany(PartyRole, {
        as: 'BrandCreatedByPartyRoles',
        through: Brand,
        foreignKey: 'DocumentBrandLevelTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandLevelType.belongsToMany(PartyRole, {
        as: 'BrandLastUpdatedByPartyRoles',
        through: Brand,
        foreignKey: 'DocumentBrandLevelTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandLevelType.belongsToMany(BrandLevelType, {
        as: 'BrandOnlineBrandLevelTypes',
        through: Brand,
        foreignKey: 'DocumentBrandLevelTypeID',
        otherKey: 'OnlineBrandLevelTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandLevelType.belongsToMany(PartyRole, {
        as: 'BrandPartyRoles',
        through: Brand,
        foreignKey: 'DocumentBrandLevelTypeID',
        otherKey: 'PartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandLevelType.belongsToMany(PartyRole, {
        as: 'BrandCreatedByPartyRoles',
        through: Brand,
        foreignKey: 'OnlineBrandLevelTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandLevelType.belongsToMany(BrandLevelType, {
        as: 'BrandDocumentBrandLevelTypes',
        through: Brand,
        foreignKey: 'OnlineBrandLevelTypeID',
        otherKey: 'DocumentBrandLevelTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandLevelType.belongsToMany(PartyRole, {
        as: 'BrandLastUpdatedByPartyRoles',
        through: Brand,
        foreignKey: 'OnlineBrandLevelTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    BrandLevelType.belongsToMany(PartyRole, {
        as: 'BrandPartyRoles',
        through: Brand,
        foreignKey: 'OnlineBrandLevelTypeID',
        otherKey: 'PartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
