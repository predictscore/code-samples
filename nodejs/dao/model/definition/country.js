'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Country', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        iSOCountryCode: {
            type: DataTypes.STRING(50),
            field: 'ISOCountryCode',
            allowNull: false
        },
        name: {
            type: DataTypes.STRING(150),
            field: 'Name',
            allowNull: false
        },
        internationalDiallingCode: {
            type: DataTypes.STRING(4),
            field: 'InternationalDiallingCode',
            allowNull: true
        },
        rPDataCode: {
            type: DataTypes.STRING(10),
            field: 'RPDataCode',
            allowNull: true
        },
        v8ID: {
            type: DataTypes.INTEGER,
            field: 'V8ID',
            allowNull: true
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        },
        tontoID: {
            type: DataTypes.INTEGER,
            field: 'TontoID',
            allowNull: true
        },
        retailID: {
            type: DataTypes.INTEGER,
            field: 'RetailID',
            allowNull: true
        },
        vedaCode: {
            type: DataTypes.STRING(3),
            field: 'VedaCode',
            allowNull: true
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'Country',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var Country = model.Country;
    var Address = model.Address;
    var Person = model.Person;
    var State = model.State;
    var Trust = model.Trust;
    var PartyRole = model.PartyRole;
    var StreetType = model.StreetType;
    var GenderType = model.GenderType;
    var MaritalStatusType = model.MaritalStatusType;
    var ObjectColourType = model.ObjectColourType;
    var NameChangeReasonType = model.NameChangeReasonType;
    var Party = model.Party;
    var SalutationType = model.SalutationType;
    var DocumentationType = model.DocumentationType;
    var TrustPurposeType = model.TrustPurposeType;
    var TrustStructureType = model.TrustStructureType;

    Country.hasMany(Address, {
        as: 'AddressCountryFks',
        foreignKey: 'CountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.hasMany(Person, {
        as: 'PersonPassportcountryFks',
        foreignKey: 'PassportCountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.hasMany(State, {
        as: 'StateCountryFks',
        foreignKey: 'CountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.hasMany(Trust, {
        as: 'TrustCountryestablishedFks',
        foreignKey: 'CountryEstablishedID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(PartyRole, {
        as: 'AddressCreatedByPartyRoles',
        through: Address,
        foreignKey: 'CountryID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(PartyRole, {
        as: 'AddressLastUpdatedByPartyRoles',
        through: Address,
        foreignKey: 'CountryID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(State, {
        as: 'AddressStates',
        through: Address,
        foreignKey: 'CountryID',
        otherKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(StreetType, {
        as: 'AddressStreetTypes',
        through: Address,
        foreignKey: 'CountryID',
        otherKey: 'StreetTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(PartyRole, {
        as: 'PersonAccountantPartyRoles',
        through: Person,
        foreignKey: 'PassportCountryID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(GenderType, {
        as: 'PersonGenderTypes',
        through: Person,
        foreignKey: 'PassportCountryID',
        otherKey: 'GenderTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(PartyRole, {
        as: 'PersonLastUpdatedByPartyRoles',
        through: Person,
        foreignKey: 'PassportCountryID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(MaritalStatusType, {
        as: 'PersonMaritalStatusTypes',
        through: Person,
        foreignKey: 'PassportCountryID',
        otherKey: 'MaritalStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(ObjectColourType, {
        as: 'PersonMedicareCardColourTypes',
        through: Person,
        foreignKey: 'PassportCountryID',
        otherKey: 'MedicareCardColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(NameChangeReasonType, {
        as: 'PersonNameChangeReasonTypes',
        through: Person,
        foreignKey: 'PassportCountryID',
        otherKey: 'NameChangeReasonTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(Party, {
        as: 'PersonParties',
        through: Person,
        foreignKey: 'PassportCountryID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(ObjectColourType, {
        as: 'PersonPassportColourTypes',
        through: Person,
        foreignKey: 'PassportCountryID',
        otherKey: 'PassportColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(SalutationType, {
        as: 'PersonSalutationTypes',
        through: Person,
        foreignKey: 'PassportCountryID',
        otherKey: 'SalutationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(State, {
        as: 'PersonStates',
        through: Person,
        foreignKey: 'PassportCountryID',
        otherKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(PartyRole, {
        as: 'TrustAccountantPartyRoles',
        through: Trust,
        foreignKey: 'CountryEstablishedID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(DocumentationType, {
        as: 'TrustDocumentationTypes',
        through: Trust,
        foreignKey: 'CountryEstablishedID',
        otherKey: 'DocumentationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(PartyRole, {
        as: 'TrustLastUpdatedByPartyRoles',
        through: Trust,
        foreignKey: 'CountryEstablishedID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(Party, {
        as: 'TrustParties',
        through: Trust,
        foreignKey: 'CountryEstablishedID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(TrustPurposeType, {
        as: 'TrustTrustPurposeTypes',
        through: Trust,
        foreignKey: 'CountryEstablishedID',
        otherKey: 'TrustPurposeTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Country.belongsToMany(TrustStructureType, {
        as: 'TrustTrustStructureTypes',
        through: Trust,
        foreignKey: 'CountryEstablishedID',
        otherKey: 'TrustStructureTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
