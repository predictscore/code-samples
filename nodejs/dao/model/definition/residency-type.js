'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ResidencyType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        },
        tontoID: {
            type: DataTypes.INTEGER,
            field: 'TontoID',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'ResidencyType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var ResidencyType = model.ResidencyType;
    var PartyAddress = model.PartyAddress;
    var AddressType = model.AddressType;
    var Address = model.Address;
    var CommunicationType = model.CommunicationType;
    var PartyRole = model.PartyRole;
    var CurrencyType = model.CurrencyType;
    var LocationType = model.LocationType;
    var Party = model.Party;

    ResidencyType.hasMany(PartyAddress, {
        as: 'PartyAddressResidencytypeFks',
        foreignKey: 'ResidencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ResidencyType.belongsToMany(AddressType, {
        as: 'PartyAddressAddressTypes',
        through: PartyAddress,
        foreignKey: 'ResidencyTypeID',
        otherKey: 'AddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ResidencyType.belongsToMany(Address, {
        as: 'PartyAddressAddresses',
        through: PartyAddress,
        foreignKey: 'ResidencyTypeID',
        otherKey: 'AddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ResidencyType.belongsToMany(CommunicationType, {
        as: 'PartyAddressCommunicationTypes',
        through: PartyAddress,
        foreignKey: 'ResidencyTypeID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ResidencyType.belongsToMany(PartyRole, {
        as: 'PartyAddressCreatedByPartyRoles',
        through: PartyAddress,
        foreignKey: 'ResidencyTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ResidencyType.belongsToMany(CurrencyType, {
        as: 'PartyAddressCurrencyTypes',
        through: PartyAddress,
        foreignKey: 'ResidencyTypeID',
        otherKey: 'CurrencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ResidencyType.belongsToMany(LocationType, {
        as: 'PartyAddressLocationTypes',
        through: PartyAddress,
        foreignKey: 'ResidencyTypeID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ResidencyType.belongsToMany(PartyRole, {
        as: 'PartyAddressLastUpdatedByPartyRoles',
        through: PartyAddress,
        foreignKey: 'ResidencyTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ResidencyType.belongsToMany(Party, {
        as: 'PartyAddressParties',
        through: PartyAddress,
        foreignKey: 'ResidencyTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
