'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportComponentAnalysisCategoryType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(150),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportComponentAnalysisCategoryType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportComponentAnalysisCategoryType = model.IdentityVerificationReportComponentAnalysisCategoryType;
    var IdentityVerificationReportComponentAnalysis = model.IdentityVerificationReportComponentAnalysis;
    var IdentityVerificationReportComponentAnalysisContributingValueTyp = model.IdentityVerificationReportComponentAnalysisContributingValueTyp;
    var IdentityVerificationReportComponentSearchNameType = model.IdentityVerificationReportComponentSearchNameType;
    var IdentityVerificationReport = model.IdentityVerificationReport;

    IdentityVerificationReportComponentAnalysisCategoryType.hasMany(IdentityVerificationReportComponentAnalysis, {
        as: 'FKIdentityverificationreportcomponentanalysisIdentityverific1s',
        foreignKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisCategoryType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePhoneTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'CategoryTypeID',
        otherKey: 'ContributePhoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisCategoryType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeTransactionTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'CategoryTypeID',
        otherKey: 'ContributeTransactionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisCategoryType.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentAnalysisSearchNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'CategoryTypeID',
        otherKey: 'SearchNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisCategoryType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeAddressTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'CategoryTypeID',
        otherKey: 'ContributeAddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisCategoryType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDateOfBirthTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'CategoryTypeID',
        otherKey: 'ContributeDateOfBirthTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisCategoryType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDocumentTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'CategoryTypeID',
        otherKey: 'ContributeDocumentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisCategoryType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDriversTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'CategoryTypeID',
        otherKey: 'ContributeDriversTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisCategoryType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeEntitlementTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'CategoryTypeID',
        otherKey: 'ContributeEntitlementTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisCategoryType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeMultipleTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'CategoryTypeID',
        otherKey: 'ContributeMultipleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisCategoryType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'CategoryTypeID',
        otherKey: 'ContributeNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisCategoryType.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePassportTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'CategoryTypeID',
        otherKey: 'ContributePassportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentAnalysisCategoryType.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentAnalysisIdentityVerificationReports',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'CategoryTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
