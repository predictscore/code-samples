'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('RelationshipType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        description: {
            type: DataTypes.STRING(500),
            field: 'Description',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        },
        checkForPrimaryContact: {
            type: DataTypes.BOOLEAN,
            field: 'CheckForPrimaryContact',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'RelationshipType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var RelationshipType = model.RelationshipType;
    var PartyRoleRelationship = model.PartyRoleRelationship;
    var PartyRoleTypeRelationshipType = model.PartyRoleTypeRelationshipType;
    var RelationshipRequest = model.RelationshipRequest;
    var RelationshipTypeSecondaryRelationshipType = model.RelationshipTypeSecondaryRelationshipType;
    var PartyRole = model.PartyRole;
    var PartyRoleType = model.PartyRoleType;

    RelationshipType.hasMany(PartyRoleRelationship, {
        as: 'PartyRoleRelationshipRelationshiptypeFks',
        foreignKey: 'RelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipType.hasMany(PartyRoleTypeRelationshipType, {
        as: 'PartyRoleTypeRelationshipTypeRelationshiptypeFks',
        foreignKey: 'RelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipType.hasMany(RelationshipRequest, {
        as: 'RelationshipRequestRelationshiptypeFks',
        foreignKey: 'RelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipType.hasMany(RelationshipTypeSecondaryRelationshipType, {
        as: 'SecondaryRelationshipTypeRootrelationshiptypes',
        foreignKey: 'RootRelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipType.hasMany(RelationshipTypeSecondaryRelationshipType, {
        as: 'SecondaryRelationshipTypeSecondaryrelationships',
        foreignKey: 'SecondaryRelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipType.belongsToMany(PartyRole, {
        as: 'PartyRoleRelationshipChildPartyRoles',
        through: PartyRoleRelationship,
        foreignKey: 'RelationshipTypeID',
        otherKey: 'ChildPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipType.belongsToMany(PartyRole, {
        as: 'PartyRoleRelationshipParentPartyRoles',
        through: PartyRoleRelationship,
        foreignKey: 'RelationshipTypeID',
        otherKey: 'ParentPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipType.belongsToMany(PartyRoleType, {
        as: 'PartyRoleTypeRelationshipTypeChildPartyRoleTypes',
        through: PartyRoleTypeRelationshipType,
        foreignKey: 'RelationshipTypeID',
        otherKey: 'ChildPartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipType.belongsToMany(PartyRoleType, {
        as: 'PartyRoleTypeRelationshipTypeParentPartyRoleTypes',
        through: PartyRoleTypeRelationshipType,
        foreignKey: 'RelationshipTypeID',
        otherKey: 'ParentPartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipType.belongsToMany(PartyRole, {
        as: 'RelationshipRequestApprovedByPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'RelationshipTypeID',
        otherKey: 'ApprovedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipType.belongsToMany(PartyRole, {
        as: 'RelationshipRequestCreatedByPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'RelationshipTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipType.belongsToMany(PartyRole, {
        as: 'RelationshipRequestDeclinedByPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'RelationshipTypeID',
        otherKey: 'DeclinedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipType.belongsToMany(PartyRole, {
        as: 'RelationshipRequestRequesteePartyRoles',
        through: RelationshipRequest,
        foreignKey: 'RelationshipTypeID',
        otherKey: 'RequesteePartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipType.belongsToMany(PartyRole, {
        as: 'RelationshipRequestRequestorPartyRoles',
        through: RelationshipRequest,
        foreignKey: 'RelationshipTypeID',
        otherKey: 'RequestorPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipType.belongsToMany(RelationshipType, {
        as: 'RelationshipTypeSecondaryRelationshipTypeSecondaryRelationshipTypes',
        through: RelationshipTypeSecondaryRelationshipType,
        foreignKey: 'RootRelationshipTypeID',
        otherKey: 'SecondaryRelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipType.belongsToMany(RelationshipType, {
        as: 'RelationshipTypeSecondaryRelationshipTypeRootRelationshipTypes',
        through: RelationshipTypeSecondaryRelationshipType,
        foreignKey: 'SecondaryRelationshipTypeID',
        otherKey: 'RootRelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
