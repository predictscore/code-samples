'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyNameByPartyRoleID', {
        partyRoleID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleID',
            allowNull: true
        },
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: true
        },
        partyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleTypeID',
            allowNull: true
        },
        name: {
            type: DataTypes.STRING,
            field: 'Name',
            allowNull: true
        },
        nameSurnameFirst: {
            type: DataTypes.STRING,
            field: 'NameSurnameFirst',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'PartyNameByPartyRoleID',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyNameByPartyRoleID = model.PartyNameByPartyRoleID;

};
