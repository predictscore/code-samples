'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('NewsFeedAttachment', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        newsFeedID: {
            type: DataTypes.INTEGER,
            field: 'NewsFeedID',
            allowNull: false,
            references: {
                model: 'NewsFeed',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        fileID: {
            type: DataTypes.STRING(30),
            field: 'FileID',
            allowNull: false
        },
        fileName: {
            type: DataTypes.STRING(100),
            field: 'FileName',
            allowNull: true
        },
        contentType: {
            type: DataTypes.STRING(100),
            field: 'ContentType',
            allowNull: true
        },
        uploadedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'UploadedByPartyRoleID',
            allowNull: false
        },
        uploaded: {
            type: DataTypes.DATE,
            field: 'Uploaded',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'NewsFeedAttachment',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var NewsFeedAttachment = model.NewsFeedAttachment;
    var NewsFeed = model.NewsFeed;

    NewsFeedAttachment.belongsTo(NewsFeed, {
        as: 'NewsFeed',
        foreignKey: 'NewsFeedID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
