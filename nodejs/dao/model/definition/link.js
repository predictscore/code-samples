'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Link', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        linkLocationID: {
            type: DataTypes.INTEGER,
            field: 'LinkLocationID',
            allowNull: false,
            references: {
                model: 'LinkLocation',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        slug: {
            type: DataTypes.STRING(512),
            field: 'Slug',
            allowNull: false
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        expired: {
            type: DataTypes.DATE,
            field: 'Expired',
            allowNull: true
        },
        accessCount: {
            type: DataTypes.INTEGER,
            field: 'AccessCount',
            allowNull: false
        },
        lifespan: {
            type: DataTypes.BIGINT,
            field: 'Lifespan',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'Link',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var Link = model.Link;
    var LinkParameter = model.LinkParameter;
    var LinkLocation = model.LinkLocation;

    Link.hasMany(LinkParameter, {
        as: 'ParameterLinkFks',
        foreignKey: 'LinkID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Link.belongsTo(LinkLocation, {
        as: 'LinkLocation',
        foreignKey: 'LinkLocationID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
