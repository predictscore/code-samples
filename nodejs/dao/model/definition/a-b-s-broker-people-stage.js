'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ABSBrokerPeopleStage', {
        salutation Type: {
            type: DataTypes.STRING(255),
            field: 'Salutation Type',
            allowNull: true
        },
        first Name: {
            type: DataTypes.STRING(255),
            field: 'First Name',
            allowNull: true
        },
        middle Name: {
            type: DataTypes.STRING(255),
            field: 'Middle Name',
            allowNull: true
        },
        last Name: {
            type: DataTypes.STRING(255),
            field: 'Last Name',
            allowNull: true
        },
        company Name: {
            type: DataTypes.STRING(255),
            field: 'Company Name',
            allowNull: true
        },
        person Type: {
            type: DataTypes.STRING(255),
            field: 'Person Type',
            allowNull: true
        },
        email Address: {
            type: DataTypes.STRING(255),
            field: 'Email Address',
            allowNull: true
        },
        work Phone: {
            type: DataTypes.STRING(255),
            field: 'Work Phone',
            allowNull: true
        },
        mobile Phone: {
            type: DataTypes.STRING(255),
            field: 'Mobile Phone',
            allowNull: true
        },
        propertyName: {
            type: DataTypes.STRING(255),
            field: 'PropertyName',
            allowNull: true
        },
        unitNumber: {
            type: DataTypes.STRING(255),
            field: 'UnitNumber',
            allowNull: true
        },
        streetNumber: {
            type: DataTypes.STRING(255),
            field: 'StreetNumber',
            allowNull: true
        },
        pOBox: {
            type: DataTypes.STRING(255),
            field: 'POBox',
            allowNull: true
        },
        streetName: {
            type: DataTypes.STRING(255),
            field: 'StreetName',
            allowNull: true
        },
        streetType: {
            type: DataTypes.STRING(255),
            field: 'StreetType',
            allowNull: true
        },
        suburb: {
            type: DataTypes.STRING(255),
            field: 'Suburb',
            allowNull: true
        },
        state: {
            type: DataTypes.STRING(255),
            field: 'State',
            allowNull: true
        },
        postCode: {
            type: DataTypes.STRING(255),
            field: 'PostCode',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'ABSBrokerPeopleStage',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var ABSBrokerPeopleStage = model.ABSBrokerPeopleStage;

};
