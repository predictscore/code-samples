'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('AAACRMAddress', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        cRMCustomerType: {
            type: DataTypes.STRING(50),
            field: 'CRMCustomerType',
            allowNull: true
        },
        crumbsPartyID: {
            type: DataTypes.INTEGER,
            field: 'CrumbsPartyID',
            allowNull: true
        },
        crumbsPartyAddressID: {
            type: DataTypes.INTEGER,
            field: 'CrumbsPartyAddressID',
            allowNull: true
        },
        crumbsAddressID: {
            type: DataTypes.INTEGER,
            field: 'CrumbsAddressID',
            allowNull: true
        },
        cRMParentGuid: {
            type: DataTypes.UUID,
            field: 'CRMParentGuid',
            allowNull: false
        },
        cRMParentName: {
            type: DataTypes.STRING(500),
            field: 'CRMParentName',
            allowNull: true
        },
        cRMLine1: {
            type: DataTypes.STRING(200),
            field: 'CRMLine1',
            allowNull: true
        },
        cRMLine2: {
            type: DataTypes.STRING(200),
            field: 'CRMLine2',
            allowNull: true
        },
        cRMLine3: {
            type: DataTypes.STRING(200),
            field: 'CRMLine3',
            allowNull: true
        },
        cRMFullStreetAddress: {
            type: DataTypes.STRING(500),
            field: 'CRMFullStreetAddress',
            allowNull: true
        },
        cRMSuburb: {
            type: DataTypes.STRING(200),
            field: 'CRMSuburb',
            allowNull: true
        },
        cRMState: {
            type: DataTypes.STRING(200),
            field: 'CRMState',
            allowNull: true
        },
        cRMPostcode: {
            type: DataTypes.STRING(100),
            field: 'CRMPostcode',
            allowNull: true
        },
        cRMCountryID: {
            type: DataTypes.STRING(100),
            field: 'CRMCountryID',
            allowNull: true
        },
        cRMAddressTypeCode: {
            type: DataTypes.INTEGER,
            field: 'CRMAddressTypeCode',
            allowNull: true
        },
        crumbsAddressTypeID: {
            type: DataTypes.INTEGER,
            field: 'CrumbsAddressTypeID',
            allowNull: true
        },
        propertyName: {
            type: DataTypes.STRING(100),
            field: 'PropertyName',
            allowNull: true
        },
        unitNumber: {
            type: DataTypes.STRING(100),
            field: 'UnitNumber',
            allowNull: true
        },
        streetNumber: {
            type: DataTypes.STRING(100),
            field: 'StreetNumber',
            allowNull: true
        },
        pOBox: {
            type: DataTypes.STRING(30),
            field: 'POBox',
            allowNull: true
        },
        streetName: {
            type: DataTypes.STRING(100),
            field: 'StreetName',
            allowNull: true
        },
        streetTypeID: {
            type: DataTypes.INTEGER,
            field: 'StreetTypeID',
            allowNull: true
        },
        suburb: {
            type: DataTypes.STRING(100),
            field: 'Suburb',
            allowNull: true
        },
        stateID: {
            type: DataTypes.INTEGER,
            field: 'StateID',
            allowNull: true
        },
        postCode: {
            type: DataTypes.STRING(10),
            field: 'PostCode',
            allowNull: true
        },
        countryID: {
            type: DataTypes.INTEGER,
            field: 'CountryID',
            allowNull: true
        },
        addressText: {
            type: DataTypes.STRING(500),
            field: 'AddressText',
            allowNull: true
        },
        processed: {
            type: DataTypes.INTEGER,
            field: 'Processed',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'AAACRMAddresses',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var AAACRMAddress = model.AAACRMAddress;

};
