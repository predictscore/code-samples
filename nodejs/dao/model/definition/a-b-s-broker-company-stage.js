'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ABSBrokerCompanyStage', {
        company Name: {
            type: DataTypes.STRING(255),
            field: 'Company Name',
            allowNull: true
        },
        company Type: {
            type: DataTypes.STRING(255),
            field: 'Company Type',
            allowNull: true
        },
        aCN: {
            type: DataTypes.STRING(255),
            field: 'ACN',
            allowNull: true
        },
        aBN: {
            type: DataTypes.STRING(255),
            field: 'ABN',
            allowNull: true
        },
        primary Contact Name: {
            type: DataTypes.STRING(255),
            field: 'Primary Contact Name ',
            allowNull: true
        },
        account Ranking: {
            type: DataTypes.STRING(255),
            field: 'Account Ranking',
            allowNull: true
        },
        accreditation Status: {
            type: DataTypes.STRING(255),
            field: 'Accreditation Status',
            allowNull: true
        },
        source: {
            type: DataTypes.STRING(255),
            field: 'Source',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'ABSBrokerCompanyStage',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var ABSBrokerCompanyStage = model.ABSBrokerCompanyStage;

};
