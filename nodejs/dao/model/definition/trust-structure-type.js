'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('TrustStructureType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'TrustStructureType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var TrustStructureType = model.TrustStructureType;
    var Trust = model.Trust;
    var PartyRole = model.PartyRole;
    var Country = model.Country;
    var DocumentationType = model.DocumentationType;
    var Party = model.Party;
    var TrustPurposeType = model.TrustPurposeType;

    TrustStructureType.hasMany(Trust, {
        as: 'TrustTruststructuretypeFks',
        foreignKey: 'TrustStructureTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TrustStructureType.belongsToMany(PartyRole, {
        as: 'TrustAccountantPartyRoles',
        through: Trust,
        foreignKey: 'TrustStructureTypeID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TrustStructureType.belongsToMany(Country, {
        as: 'TrustCountryEstablisheds',
        through: Trust,
        foreignKey: 'TrustStructureTypeID',
        otherKey: 'CountryEstablishedID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TrustStructureType.belongsToMany(DocumentationType, {
        as: 'TrustDocumentationTypes',
        through: Trust,
        foreignKey: 'TrustStructureTypeID',
        otherKey: 'DocumentationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TrustStructureType.belongsToMany(PartyRole, {
        as: 'TrustLastUpdatedByPartyRoles',
        through: Trust,
        foreignKey: 'TrustStructureTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TrustStructureType.belongsToMany(Party, {
        as: 'TrustParties',
        through: Trust,
        foreignKey: 'TrustStructureTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TrustStructureType.belongsToMany(TrustPurposeType, {
        as: 'TrustTrustPurposeTypes',
        through: Trust,
        foreignKey: 'TrustStructureTypeID',
        otherKey: 'TrustPurposeTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
