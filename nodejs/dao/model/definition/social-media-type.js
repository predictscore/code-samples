'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('SocialMediaType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'SocialMediaType',
        timestamps: false
    });
};
