'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('AddressTextWord', {
        word: {
            type: DataTypes.TEXT,
            field: 'word',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'AddressTextWords',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var AddressTextWord = model.AddressTextWord;

};
