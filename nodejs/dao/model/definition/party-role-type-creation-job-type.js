'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyRoleTypeCreationJobType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        partyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleTypeID',
            allowNull: false,
            references: {
                model: 'PartyRoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        jobTypeID: {
            type: DataTypes.INTEGER,
            field: 'JobTypeID',
            allowNull: false
        },
        title: {
            type: DataTypes.STRING(100),
            field: 'Title',
            allowNull: true
        },
        description: {
            type: DataTypes.STRING(512),
            field: 'Description',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'PartyRoleTypeCreationJobType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyRoleTypeCreationJobType = model.PartyRoleTypeCreationJobType;
    var PartyRoleType = model.PartyRoleType;

    PartyRoleTypeCreationJobType.belongsTo(PartyRoleType, {
        as: 'PartyRoleType',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
