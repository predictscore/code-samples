'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyNameByPartyID', {
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: true
        },
        partyTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyTypeID',
            allowNull: true
        },
        name: {
            type: DataTypes.STRING,
            field: 'Name',
            allowNull: true
        },
        nameSurnameFirst: {
            type: DataTypes.STRING,
            field: 'NameSurnameFirst',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'PartyNameByPartyID',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyNameByPartyID = model.PartyNameByPartyID;

};
