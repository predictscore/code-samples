'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportComponentSearchResultCodeType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(150),
            field: 'Name',
            allowNull: false
        },
        vedaCode: {
            type: DataTypes.STRING(50),
            field: 'VedaCode',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportComponentSearchResultCodeType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportComponentSearchResultCodeType = model.IdentityVerificationReportComponentSearchResultCodeType;
    var IdentityVerificationReportComponentSearch = model.IdentityVerificationReportComponentSearch;
    var IdentityVerificationReportComponentSearchMatchIndicatorType = model.IdentityVerificationReportComponentSearchMatchIndicatorType;
    var IdentityVerificationReportComponentSearchNameType = model.IdentityVerificationReportComponentSearchNameType;
    var IdentityVerificationReportComponentSearchType = model.IdentityVerificationReportComponentSearchType;
    var IdentityVerificationReport = model.IdentityVerificationReport;

    IdentityVerificationReportComponentSearchResultCodeType.hasMany(IdentityVerificationReportComponentSearch, {
        as: 'FKIdentityverificationreportcomponentsearchesIdentityverific3s',
        foreignKey: 'ResultCodeTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchResultCodeType.belongsToMany(IdentityVerificationReportComponentSearchMatchIndicatorType, {
        as: 'IdentityVerificationReportComponentSearchMatchIndicatorTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'ResultCodeTypeID',
        otherKey: 'MatchIndicatorTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchResultCodeType.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentSearchNameTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'ResultCodeTypeID',
        otherKey: 'NameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchResultCodeType.belongsToMany(IdentityVerificationReportComponentSearchType, {
        as: 'IdentityVerificationReportComponentSearchSearchTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'ResultCodeTypeID',
        otherKey: 'SearchTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchResultCodeType.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentSearchIdentityVerificationReports',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'ResultCodeTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
