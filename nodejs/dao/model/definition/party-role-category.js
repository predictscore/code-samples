'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyRoleCategory', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(100),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'PartyRoleCategory',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyRoleCategory = model.PartyRoleCategory;
    var PartyRoleType = model.PartyRoleType;
    var PartyRoleGroup = model.PartyRoleGroup;
    var PartyRoleSuite = model.PartyRoleSuite;
    var PartyType = model.PartyType;

    PartyRoleCategory.hasMany(PartyRoleType, {
        as: 'PartyRoleTypePartyrolecategoryFks',
        foreignKey: 'PartyRoleCategoryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleCategory.belongsToMany(PartyRoleGroup, {
        as: 'PartyRoleTypePartyRoleGroups',
        through: PartyRoleType,
        foreignKey: 'PartyRoleCategoryID',
        otherKey: 'PartyRoleGroupID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleCategory.belongsToMany(PartyRoleSuite, {
        as: 'PartyRoleTypePartyRoleSuites',
        through: PartyRoleType,
        foreignKey: 'PartyRoleCategoryID',
        otherKey: 'PartyRoleSuiteID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleCategory.belongsToMany(PartyType, {
        as: 'PartyRoleTypePartyTypes',
        through: PartyRoleType,
        foreignKey: 'PartyRoleCategoryID',
        otherKey: 'PartyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
