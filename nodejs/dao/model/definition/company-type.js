'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('CompanyType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'CompanyType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var CompanyType = model.CompanyType;
    var Company = model.Company;
    var AccountRankingType = model.AccountRankingType;
    var PartyRole = model.PartyRole;
    var AccreditationStatusType = model.AccreditationStatusType;
    var IndustryType = model.IndustryType;
    var MainBusinessActivityType = model.MainBusinessActivityType;
    var Party = model.Party;
    var State = model.State;

    CompanyType.hasMany(Company, {
        as: 'CompanyCompanytypeFks',
        foreignKey: 'CompanyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CompanyType.belongsToMany(AccountRankingType, {
        as: 'CompanyAccountRankingTypes',
        through: Company,
        foreignKey: 'CompanyTypeID',
        otherKey: 'AccountRankingTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CompanyType.belongsToMany(PartyRole, {
        as: 'CompanyAccountantPartyRoles',
        through: Company,
        foreignKey: 'CompanyTypeID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CompanyType.belongsToMany(AccreditationStatusType, {
        as: 'CompanyAccreditationStatusTypes',
        through: Company,
        foreignKey: 'CompanyTypeID',
        otherKey: 'AccreditationStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CompanyType.belongsToMany(IndustryType, {
        as: 'CompanyIndustryTypes',
        through: Company,
        foreignKey: 'CompanyTypeID',
        otherKey: 'IndustryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CompanyType.belongsToMany(PartyRole, {
        as: 'CompanyLastUpdatedByPartyRoles',
        through: Company,
        foreignKey: 'CompanyTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CompanyType.belongsToMany(MainBusinessActivityType, {
        as: 'CompanyMainBusinessActivities',
        through: Company,
        foreignKey: 'CompanyTypeID',
        otherKey: 'MainBusinessActivityID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CompanyType.belongsToMany(Party, {
        as: 'CompanyParties',
        through: Company,
        foreignKey: 'CompanyTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CompanyType.belongsToMany(State, {
        as: 'CompanyRegisteredInStates',
        through: Company,
        foreignKey: 'CompanyTypeID',
        otherKey: 'RegisteredInStateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
