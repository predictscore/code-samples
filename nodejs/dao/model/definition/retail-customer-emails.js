'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('RetailCustomerEmail', {
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: true
        },
        partyTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyTypeID',
            allowNull: true
        },
        partyRoleID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleID',
            allowNull: true
        },
        partyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleTypeID',
            allowNull: true
        },
        partyEmailID: {
            type: DataTypes.INTEGER,
            field: 'PartyEmailID',
            allowNull: true
        },
        emailID: {
            type: DataTypes.INTEGER,
            field: 'EmailID',
            allowNull: true
        },
        emailAddress: {
            type: DataTypes.STRING(250),
            field: 'EmailAddress',
            allowNull: true
        },
        optOut: {
            type: DataTypes.BOOLEAN,
            field: 'OptOut',
            allowNull: true
        },
        priority: {
            type: DataTypes.INTEGER,
            field: 'Priority',
            allowNull: true
        },
        communicationTypeID: {
            type: DataTypes.INTEGER,
            field: 'CommunicationTypeID',
            allowNull: true
        },
        emailTypeID: {
            type: DataTypes.INTEGER,
            field: 'EmailTypeID',
            allowNull: true
        },
        locationTypeID: {
            type: DataTypes.INTEGER,
            field: 'LocationTypeID',
            allowNull: true
        },
        communicationTypeName: {
            type: DataTypes.STRING(200),
            field: 'CommunicationTypeName',
            allowNull: true
        },
        locationTypeName: {
            type: DataTypes.STRING(50),
            field: 'LocationTypeName',
            allowNull: true
        },
        emailTypeName: {
            type: DataTypes.STRING(50),
            field: 'EmailTypeName',
            allowNull: true
        },
        preferredEmail: {
            type: DataTypes.BOOLEAN,
            field: 'PreferredEmail',
            allowNull: true
        },
        claimLeadID: {
            type: DataTypes.INTEGER,
            field: 'ClaimLeadID',
            allowNull: true
        },
        claimCustomerID: {
            type: DataTypes.INTEGER,
            field: 'ClaimCustomerID',
            allowNull: true
        },
        claimPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'ClaimPartyRoleID',
            allowNull: true
        },
        loginID: {
            type: DataTypes.INTEGER,
            field: 'LoginID',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'RetailCustomerEmails',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var RetailCustomerEmail = model.RetailCustomerEmail;

};
