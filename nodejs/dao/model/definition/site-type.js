'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('SiteType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        retailChannelID: {
            type: DataTypes.INTEGER,
            field: 'RetailChannelID',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'SiteType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var SiteType = model.SiteType;
    var Login = model.Login;
    var Message = model.Message;
    var PartyRoleTypeSiteType = model.PartyRoleTypeSiteType;
    var SiteTypeTemplateLinkLocation = model.SiteTypeTemplateLinkLocation;
    var MessagePriorityType = model.MessagePriorityType;
    var MessageType = model.MessageType;
    var PartyRoleType = model.PartyRoleType;
    var LinkLocation = model.LinkLocation;

    SiteType.hasMany(Login, {
        as: 'LoginSitetypeFks',
        foreignKey: 'SiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SiteType.hasMany(Message, {
        as: 'MessageReadfromsitetypeFks',
        foreignKey: 'ReadFromSiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SiteType.hasMany(Message, {
        as: 'MessageSentfromsitetypeFks',
        foreignKey: 'SentFromSiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SiteType.hasMany(PartyRoleTypeSiteType, {
        as: 'PartyRoleTypeSiteTypeSitetypeFks',
        foreignKey: 'SiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SiteType.hasMany(SiteTypeTemplateLinkLocation, {
        as: 'TemplateLinkLocationSitetypeFks',
        foreignKey: 'SiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SiteType.belongsToMany(MessagePriorityType, {
        as: 'MessageMessagePriorityTypes',
        through: Message,
        foreignKey: 'ReadFromSiteTypeID',
        otherKey: 'MessagePriorityTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SiteType.belongsToMany(MessageType, {
        as: 'MessageMessageTypes',
        through: Message,
        foreignKey: 'ReadFromSiteTypeID',
        otherKey: 'MessageTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SiteType.belongsToMany(Message, {
        as: 'MessageParents',
        through: Message,
        foreignKey: 'ReadFromSiteTypeID',
        otherKey: 'ParentID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SiteType.belongsToMany(SiteType, {
        as: 'MessageSentFromSiteTypes',
        through: Message,
        foreignKey: 'ReadFromSiteTypeID',
        otherKey: 'SentFromSiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SiteType.belongsToMany(MessagePriorityType, {
        as: 'MessageMessagePriorityTypes',
        through: Message,
        foreignKey: 'SentFromSiteTypeID',
        otherKey: 'MessagePriorityTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SiteType.belongsToMany(MessageType, {
        as: 'MessageMessageTypes',
        through: Message,
        foreignKey: 'SentFromSiteTypeID',
        otherKey: 'MessageTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SiteType.belongsToMany(Message, {
        as: 'MessageParents',
        through: Message,
        foreignKey: 'SentFromSiteTypeID',
        otherKey: 'ParentID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SiteType.belongsToMany(SiteType, {
        as: 'MessageReadFromSiteTypes',
        through: Message,
        foreignKey: 'SentFromSiteTypeID',
        otherKey: 'ReadFromSiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SiteType.belongsToMany(PartyRoleType, {
        as: 'PartyRoleTypeSiteTypePartyRoleTypes',
        through: PartyRoleTypeSiteType,
        foreignKey: 'SiteTypeID',
        otherKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SiteType.belongsToMany(LinkLocation, {
        as: 'SiteTypeTemplateLinkLocationLinkLocations',
        through: SiteTypeTemplateLinkLocation,
        foreignKey: 'SiteTypeID',
        otherKey: 'LinkLocationID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
