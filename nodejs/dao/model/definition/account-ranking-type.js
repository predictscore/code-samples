'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('AccountRankingType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.CHAR(1),
            field: 'Name',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'AccountRankingType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var AccountRankingType = model.AccountRankingType;
    var Company = model.Company;
    var PartyRole = model.PartyRole;
    var AccreditationStatusType = model.AccreditationStatusType;
    var CompanyType = model.CompanyType;
    var IndustryType = model.IndustryType;
    var MainBusinessActivityType = model.MainBusinessActivityType;
    var Party = model.Party;
    var State = model.State;

    AccountRankingType.hasMany(Company, {
        as: 'CompanyAccountrankingtypeFks',
        foreignKey: 'AccountRankingTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    AccountRankingType.belongsToMany(PartyRole, {
        as: 'CompanyAccountantPartyRoles',
        through: Company,
        foreignKey: 'AccountRankingTypeID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    AccountRankingType.belongsToMany(AccreditationStatusType, {
        as: 'CompanyAccreditationStatusTypes',
        through: Company,
        foreignKey: 'AccountRankingTypeID',
        otherKey: 'AccreditationStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    AccountRankingType.belongsToMany(CompanyType, {
        as: 'CompanyCompanyTypes',
        through: Company,
        foreignKey: 'AccountRankingTypeID',
        otherKey: 'CompanyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    AccountRankingType.belongsToMany(IndustryType, {
        as: 'CompanyIndustryTypes',
        through: Company,
        foreignKey: 'AccountRankingTypeID',
        otherKey: 'IndustryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    AccountRankingType.belongsToMany(PartyRole, {
        as: 'CompanyLastUpdatedByPartyRoles',
        through: Company,
        foreignKey: 'AccountRankingTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    AccountRankingType.belongsToMany(MainBusinessActivityType, {
        as: 'CompanyMainBusinessActivities',
        through: Company,
        foreignKey: 'AccountRankingTypeID',
        otherKey: 'MainBusinessActivityID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    AccountRankingType.belongsToMany(Party, {
        as: 'CompanyParties',
        through: Company,
        foreignKey: 'AccountRankingTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    AccountRankingType.belongsToMany(State, {
        as: 'CompanyRegisteredInStates',
        through: Company,
        foreignKey: 'AccountRankingTypeID',
        otherKey: 'RegisteredInStateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
