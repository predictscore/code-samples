'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyRoleType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(100),
            field: 'Name',
            allowNull: false
        },
        partyRoleSuiteID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleSuiteID',
            allowNull: true,
            references: {
                model: 'PartyRoleSuite',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        partyRoleCategoryID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleCategoryID',
            allowNull: true,
            references: {
                model: 'PartyRoleCategory',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        partyRoleGroupID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleGroupID',
            allowNull: true,
            references: {
                model: 'PartyRoleGroup',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        partyTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyTypeID',
            allowNull: true,
            references: {
                model: 'PartyType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        },
        applySecurity: {
            type: DataTypes.BOOLEAN,
            field: 'ApplySecurity',
            allowNull: true
        },
        usedForOwnership: {
            type: DataTypes.BOOLEAN,
            field: 'UsedForOwnership',
            allowNull: true
        },
        requiresV8PartyRole: {
            type: DataTypes.BOOLEAN,
            field: 'RequiresV8PartyRole',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'PartyRoleType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyRoleType = model.PartyRoleType;
    var PartyRole = model.PartyRole;
    var PartyRoleTypeApplicationTypeRoleType = model.PartyRoleTypeApplicationTypeRoleType;
    var PartyRoleTypeConstrained = model.PartyRoleTypeConstrained;
    var PartyRoleTypeCreationJobType = model.PartyRoleTypeCreationJobType;
    var PartyRoleTypeCRMAccountCategory = model.PartyRoleTypeCRMAccountCategory;
    var PartyRoleTypeRelationshipType = model.PartyRoleTypeRelationshipType;
    var PartyRoleTypeRoleType = model.PartyRoleTypeRoleType;
    var PartyRoleTypeSiteType = model.PartyRoleTypeSiteType;
    var PartyTypePartyRoleType = model.PartyTypePartyRoleType;
    var SupplierPartyRoleTypeRelationship = model.SupplierPartyRoleTypeRelationship;
    var PartyRoleCategory = model.PartyRoleCategory;
    var PartyRoleGroup = model.PartyRoleGroup;
    var PartyRoleSuite = model.PartyRoleSuite;
    var PartyType = model.PartyType;
    var Party = model.Party;
    var ApplicationType = model.ApplicationType;
    var RoleType = model.RoleType;
    var RelationshipType = model.RelationshipType;
    var SiteType = model.SiteType;

    PartyRoleType.hasMany(PartyRole, {
        as: 'PartyRolePartyroletypeFks',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.hasMany(PartyRoleTypeApplicationTypeRoleType, {
        as: 'ApplicationTypeRoleTypePartyroletypeFks',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.hasMany(PartyRoleTypeConstrained, {
        as: 'ConstrainedConstrainedpartyroletypeFks',
        foreignKey: 'ConstrainedPartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.hasMany(PartyRoleTypeConstrained, {
        as: 'ConstrainedParentpartyroletypeFks',
        foreignKey: 'ParentPartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.hasMany(PartyRoleTypeCreationJobType, {
        as: 'CreationJobTypePartyroletypeFks',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.hasMany(PartyRoleTypeCRMAccountCategory, {
        as: 'CRMAccountCategoryPartyroletypeFks',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.hasMany(PartyRoleTypeRelationshipType, {
        as: 'RelationshipTypeChildpartyroletypeFks',
        foreignKey: 'ChildPartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.hasMany(PartyRoleTypeRelationshipType, {
        as: 'RelationshipTypeParentpartyroletypeFks',
        foreignKey: 'ParentPartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.hasMany(PartyRoleTypeRoleType, {
        as: 'RoleTypePartyroletypeFks',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.hasMany(PartyRoleTypeSiteType, {
        as: 'SiteTypePartyroletypeFks',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.hasMany(PartyTypePartyRoleType, {
        as: 'PartyTypePartyRoleTypePartyroletypeFks',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.hasMany(SupplierPartyRoleTypeRelationship, {
        as: 'SupplierPartyRoleTypeRelationshipPartyroletypeFks',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsTo(PartyRoleCategory, {
        as: 'PartyRoleCategory',
        foreignKey: 'PartyRoleCategoryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsTo(PartyRoleGroup, {
        as: 'PartyRoleGroup',
        foreignKey: 'PartyRoleGroupID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsTo(PartyRoleSuite, {
        as: 'PartyRoleSuite',
        foreignKey: 'PartyRoleSuiteID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsTo(PartyType, {
        as: 'PartyType',
        foreignKey: 'PartyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsToMany(PartyRole, {
        as: 'PartyRoleCreatedByPartyRoles',
        through: PartyRole,
        foreignKey: 'PartyRoleTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsToMany(Party, {
        as: 'PartyRoleParties',
        through: PartyRole,
        foreignKey: 'PartyRoleTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsToMany(ApplicationType, {
        as: 'PartyRoleTypeApplicationTypeRoleTypeApplicationTypes',
        through: PartyRoleTypeApplicationTypeRoleType,
        foreignKey: 'PartyRoleTypeID',
        otherKey: 'ApplicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsToMany(RoleType, {
        as: 'PartyRoleTypeApplicationTypeRoleTypeRoleTypes',
        through: PartyRoleTypeApplicationTypeRoleType,
        foreignKey: 'PartyRoleTypeID',
        otherKey: 'RoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsToMany(PartyRoleType, {
        as: 'PartyRoleTypeConstrainedParentPartyRoleTypes',
        through: PartyRoleTypeConstrained,
        foreignKey: 'ConstrainedPartyRoleTypeID',
        otherKey: 'ParentPartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsToMany(PartyRoleType, {
        as: 'PartyRoleTypeConstrainedConstrainedPartyRoleTypes',
        through: PartyRoleTypeConstrained,
        foreignKey: 'ParentPartyRoleTypeID',
        otherKey: 'ConstrainedPartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsToMany(PartyRoleType, {
        as: 'PartyRoleTypeRelationshipTypeParentPartyRoleTypes',
        through: PartyRoleTypeRelationshipType,
        foreignKey: 'ChildPartyRoleTypeID',
        otherKey: 'ParentPartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsToMany(RelationshipType, {
        as: 'PartyRoleTypeRelationshipTypeRelationshipTypes',
        through: PartyRoleTypeRelationshipType,
        foreignKey: 'ChildPartyRoleTypeID',
        otherKey: 'RelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsToMany(PartyRoleType, {
        as: 'PartyRoleTypeRelationshipTypeChildPartyRoleTypes',
        through: PartyRoleTypeRelationshipType,
        foreignKey: 'ParentPartyRoleTypeID',
        otherKey: 'ChildPartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsToMany(RelationshipType, {
        as: 'PartyRoleTypeRelationshipTypeRelationshipTypes',
        through: PartyRoleTypeRelationshipType,
        foreignKey: 'ParentPartyRoleTypeID',
        otherKey: 'RelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsToMany(RoleType, {
        as: 'PartyRoleTypeRoleTypeRoleTypes',
        through: PartyRoleTypeRoleType,
        foreignKey: 'PartyRoleTypeID',
        otherKey: 'RoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsToMany(SiteType, {
        as: 'PartyRoleTypeSiteTypeSiteTypes',
        through: PartyRoleTypeSiteType,
        foreignKey: 'PartyRoleTypeID',
        otherKey: 'SiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleType.belongsToMany(PartyType, {
        as: 'PartyTypePartyRoleTypePartyTypes',
        through: PartyTypePartyRoleType,
        foreignKey: 'PartyRoleTypeID',
        otherKey: 'PartyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
