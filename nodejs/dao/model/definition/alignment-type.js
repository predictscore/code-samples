'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('AlignmentType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'AlignmentType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var AlignmentType = model.AlignmentType;
    var BrandText = model.BrandText;
    var BrandType = model.BrandType;
    var CurrencyType = model.CurrencyType;
    var DocumentLocationType = model.DocumentLocationType;
    var PartyRole = model.PartyRole;

    AlignmentType.hasMany(BrandText, {
        as: 'BrandTextAlignmenttypeFks',
        foreignKey: 'AlignmentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    AlignmentType.belongsToMany(BrandType, {
        as: 'BrandTextBrandTypes',
        through: BrandText,
        foreignKey: 'AlignmentTypeID',
        otherKey: 'BrandTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    AlignmentType.belongsToMany(CurrencyType, {
        as: 'BrandTextCurrencyTypes',
        through: BrandText,
        foreignKey: 'AlignmentTypeID',
        otherKey: 'CurrencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    AlignmentType.belongsToMany(DocumentLocationType, {
        as: 'BrandTextDocumentLocationTypes',
        through: BrandText,
        foreignKey: 'AlignmentTypeID',
        otherKey: 'DocumentLocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    AlignmentType.belongsToMany(PartyRole, {
        as: 'BrandTextPartyRoles',
        through: BrandText,
        foreignKey: 'AlignmentTypeID',
        otherKey: 'PartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
