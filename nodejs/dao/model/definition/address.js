'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Address', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        propertyName: {
            type: DataTypes.STRING(100),
            field: 'PropertyName',
            allowNull: true
        },
        unitNumber: {
            type: DataTypes.STRING(100),
            field: 'UnitNumber',
            allowNull: true
        },
        streetNumber: {
            type: DataTypes.STRING(100),
            field: 'StreetNumber',
            allowNull: true
        },
        pOBox: {
            type: DataTypes.STRING(30),
            field: 'POBox',
            allowNull: true
        },
        streetName: {
            type: DataTypes.STRING(100),
            field: 'StreetName',
            allowNull: true
        },
        streetTypeID: {
            type: DataTypes.INTEGER,
            field: 'StreetTypeID',
            allowNull: true,
            references: {
                model: 'StreetType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        suburb: {
            type: DataTypes.STRING(100),
            field: 'Suburb',
            allowNull: false
        },
        stateID: {
            type: DataTypes.INTEGER,
            field: 'StateID',
            allowNull: true,
            references: {
                model: 'State',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        countryID: {
            type: DataTypes.INTEGER,
            field: 'CountryID',
            allowNull: false,
            references: {
                model: 'Country',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        postCode: {
            type: DataTypes.STRING(10),
            field: 'PostCode',
            allowNull: true
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        lastUpdated: {
            type: DataTypes.DATE,
            field: 'LastUpdated',
            allowNull: false
        },
        lastUpdatedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'LastUpdatedByPartyRoleID',
            allowNull: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        streetTypeText: {
            type: DataTypes.STRING(50),
            field: 'StreetTypeText',
            allowNull: true
        },
        stateText: {
            type: DataTypes.STRING(50),
            field: 'StateText',
            allowNull: true
        },
        addressText: {
            type: DataTypes.STRING(500),
            field: 'AddressText',
            allowNull: true
        },
        rPDataPropertyID: {
            type: DataTypes.INTEGER,
            field: 'RPDataPropertyID',
            allowNull: true
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        },
        retailID: {
            type: DataTypes.INTEGER,
            field: 'RetailID',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'Address',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var Address = model.Address;
    var BankBSB = model.BankBSB;
    var PartyAddress = model.PartyAddress;
    var Country = model.Country;
    var PartyRole = model.PartyRole;
    var State = model.State;
    var StreetType = model.StreetType;
    var AddressType = model.AddressType;
    var CommunicationType = model.CommunicationType;
    var CurrencyType = model.CurrencyType;
    var LocationType = model.LocationType;
    var Party = model.Party;
    var ResidencyType = model.ResidencyType;

    Address.hasMany(BankBSB, {
        as: 'BSBAddressidFks',
        foreignKey: 'AddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.hasMany(PartyAddress, {
        as: 'PartyAddressAddressFks',
        foreignKey: 'AddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.belongsTo(Country, {
        as: 'Country',
        foreignKey: 'CountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.belongsTo(PartyRole, {
        as: 'LastUpdatedByPartyRole',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.belongsTo(State, {
        as: 'State',
        foreignKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.belongsTo(StreetType, {
        as: 'StreetType',
        foreignKey: 'StreetTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.belongsToMany(PartyRole, {
        as: 'BankBSBCreatedByPartyRoles',
        through: BankBSB,
        foreignKey: 'AddressID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.belongsToMany(PartyRole, {
        as: 'BankBSBLastUpdatedByPartyRoles',
        through: BankBSB,
        foreignKey: 'AddressID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.belongsToMany(AddressType, {
        as: 'PartyAddressAddressTypes',
        through: PartyAddress,
        foreignKey: 'AddressID',
        otherKey: 'AddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.belongsToMany(CommunicationType, {
        as: 'PartyAddressCommunicationTypes',
        through: PartyAddress,
        foreignKey: 'AddressID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.belongsToMany(PartyRole, {
        as: 'PartyAddressCreatedByPartyRoles',
        through: PartyAddress,
        foreignKey: 'AddressID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.belongsToMany(CurrencyType, {
        as: 'PartyAddressCurrencyTypes',
        through: PartyAddress,
        foreignKey: 'AddressID',
        otherKey: 'CurrencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.belongsToMany(LocationType, {
        as: 'PartyAddressLocationTypes',
        through: PartyAddress,
        foreignKey: 'AddressID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.belongsToMany(PartyRole, {
        as: 'PartyAddressLastUpdatedByPartyRoles',
        through: PartyAddress,
        foreignKey: 'AddressID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.belongsToMany(Party, {
        as: 'PartyAddressParties',
        through: PartyAddress,
        foreignKey: 'AddressID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Address.belongsToMany(ResidencyType, {
        as: 'PartyAddressResidencyTypes',
        through: PartyAddress,
        foreignKey: 'AddressID',
        otherKey: 'ResidencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
