'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyRoleTypeApplicationTypeRoleType', {
        partyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleTypeID',
            allowNull: false,
            references: {
                model: 'PartyRoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        applicationTypeID: {
            type: DataTypes.INTEGER,
            field: 'ApplicationTypeID',
            allowNull: false,
            references: {
                model: 'ApplicationType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        roleTypeID: {
            type: DataTypes.INTEGER,
            field: 'RoleTypeID',
            allowNull: false,
            references: {
                model: 'RoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'PartyRoleTypeApplicationTypeRoleType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyRoleTypeApplicationTypeRoleType = model.PartyRoleTypeApplicationTypeRoleType;
    var ApplicationType = model.ApplicationType;
    var PartyRoleType = model.PartyRoleType;
    var RoleType = model.RoleType;

    PartyRoleTypeApplicationTypeRoleType.belongsTo(ApplicationType, {
        as: 'ApplicationType',
        foreignKey: 'ApplicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleTypeApplicationTypeRoleType.belongsTo(PartyRoleType, {
        as: 'PartyRoleType',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleTypeApplicationTypeRoleType.belongsTo(RoleType, {
        as: 'RoleType',
        foreignKey: 'RoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
