'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ObjectType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false,
            defaultValue: true
        }
    }, {
        schema: 'public',
        tableName: 'ObjectType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var ObjectType = model.ObjectType;
    var ObjectColourType = model.ObjectColourType;

    ObjectType.hasMany(ObjectColourType, {
        as: 'ObjectColourTypeObjecttypeFks',
        foreignKey: 'ObjectTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
