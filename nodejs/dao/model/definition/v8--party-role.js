'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('V8Partyrole', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false
        },
        crumbsPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CrumbsPartyRoleID',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'v8_PartyRole',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var V8Partyrole = model.V8Partyrole;

};
