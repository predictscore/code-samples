'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportComponentSearchType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(150),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportComponentSearchType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportComponentSearchType = model.IdentityVerificationReportComponentSearchType;
    var IdentityVerificationReportComponentSearch = model.IdentityVerificationReportComponentSearch;
    var IdentityVerificationReportComponentSearchMatchIndicatorType = model.IdentityVerificationReportComponentSearchMatchIndicatorType;
    var IdentityVerificationReportComponentSearchNameType = model.IdentityVerificationReportComponentSearchNameType;
    var IdentityVerificationReportComponentSearchResultCodeType = model.IdentityVerificationReportComponentSearchResultCodeType;
    var IdentityVerificationReport = model.IdentityVerificationReport;

    IdentityVerificationReportComponentSearchType.hasMany(IdentityVerificationReportComponentSearch, {
        as: 'FKIdentityverificationreportcomponentsearchesIdentityverific4s',
        foreignKey: 'SearchTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchType.belongsToMany(IdentityVerificationReportComponentSearchMatchIndicatorType, {
        as: 'IdentityVerificationReportComponentSearchMatchIndicatorTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'SearchTypeID',
        otherKey: 'MatchIndicatorTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchType.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentSearchNameTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'SearchTypeID',
        otherKey: 'NameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchType.belongsToMany(IdentityVerificationReportComponentSearchResultCodeType, {
        as: 'IdentityVerificationReportComponentSearchResultCodeTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'SearchTypeID',
        otherKey: 'ResultCodeTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchType.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentSearchIdentityVerificationReports',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'SearchTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
