'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('DocumentationType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(150),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: true
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'DocumentationType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var DocumentationType = model.DocumentationType;
    var Trust = model.Trust;
    var PartyRole = model.PartyRole;
    var Country = model.Country;
    var Party = model.Party;
    var TrustPurposeType = model.TrustPurposeType;
    var TrustStructureType = model.TrustStructureType;

    DocumentationType.hasMany(Trust, {
        as: 'TrustDocumentationtypeFks',
        foreignKey: 'DocumentationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    DocumentationType.belongsToMany(PartyRole, {
        as: 'TrustAccountantPartyRoles',
        through: Trust,
        foreignKey: 'DocumentationTypeID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    DocumentationType.belongsToMany(Country, {
        as: 'TrustCountryEstablisheds',
        through: Trust,
        foreignKey: 'DocumentationTypeID',
        otherKey: 'CountryEstablishedID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    DocumentationType.belongsToMany(PartyRole, {
        as: 'TrustLastUpdatedByPartyRoles',
        through: Trust,
        foreignKey: 'DocumentationTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    DocumentationType.belongsToMany(Party, {
        as: 'TrustParties',
        through: Trust,
        foreignKey: 'DocumentationTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    DocumentationType.belongsToMany(TrustPurposeType, {
        as: 'TrustTrustPurposeTypes',
        through: Trust,
        foreignKey: 'DocumentationTypeID',
        otherKey: 'TrustPurposeTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    DocumentationType.belongsToMany(TrustStructureType, {
        as: 'TrustTrustStructureTypes',
        through: Trust,
        foreignKey: 'DocumentationTypeID',
        otherKey: 'TrustStructureTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
