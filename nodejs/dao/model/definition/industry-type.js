'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IndustryType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        aNZSICCode: {
            type: DataTypes.STRING(10),
            field: 'ANZSICCode',
            allowNull: true
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'IndustryType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IndustryType = model.IndustryType;
    var Company = model.Company;
    var MainBusinessActivityType = model.MainBusinessActivityType;
    var AccountRankingType = model.AccountRankingType;
    var PartyRole = model.PartyRole;
    var AccreditationStatusType = model.AccreditationStatusType;
    var CompanyType = model.CompanyType;
    var Party = model.Party;
    var State = model.State;

    IndustryType.hasMany(Company, {
        as: 'CompanyIndustrytypeFks',
        foreignKey: 'IndustryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IndustryType.hasMany(MainBusinessActivityType, {
        as: 'MainBusinessActivityTypeIndustrytypeFks',
        foreignKey: 'IndustryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IndustryType.belongsToMany(AccountRankingType, {
        as: 'CompanyAccountRankingTypes',
        through: Company,
        foreignKey: 'IndustryTypeID',
        otherKey: 'AccountRankingTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IndustryType.belongsToMany(PartyRole, {
        as: 'CompanyAccountantPartyRoles',
        through: Company,
        foreignKey: 'IndustryTypeID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IndustryType.belongsToMany(AccreditationStatusType, {
        as: 'CompanyAccreditationStatusTypes',
        through: Company,
        foreignKey: 'IndustryTypeID',
        otherKey: 'AccreditationStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IndustryType.belongsToMany(CompanyType, {
        as: 'CompanyCompanyTypes',
        through: Company,
        foreignKey: 'IndustryTypeID',
        otherKey: 'CompanyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IndustryType.belongsToMany(PartyRole, {
        as: 'CompanyLastUpdatedByPartyRoles',
        through: Company,
        foreignKey: 'IndustryTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IndustryType.belongsToMany(MainBusinessActivityType, {
        as: 'CompanyMainBusinessActivities',
        through: Company,
        foreignKey: 'IndustryTypeID',
        otherKey: 'MainBusinessActivityID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IndustryType.belongsToMany(Party, {
        as: 'CompanyParties',
        through: Company,
        foreignKey: 'IndustryTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IndustryType.belongsToMany(State, {
        as: 'CompanyRegisteredInStates',
        through: Company,
        foreignKey: 'IndustryTypeID',
        otherKey: 'RegisteredInStateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
