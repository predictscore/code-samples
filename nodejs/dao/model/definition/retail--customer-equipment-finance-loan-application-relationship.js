'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('RetailCustomerequipmentfinanceloanapplicationrelationship', {
        customerEquipmentFinanceLoanApplicationRelationshipID: {
            type: DataTypes.INTEGER,
            field: 'CustomerEquipmentFinanceLoanApplicationRelationshipID',
            allowNull: false
        },
        equipmentFinanceLoanApplicationID: {
            type: DataTypes.INTEGER,
            field: 'EquipmentFinanceLoanApplicationID',
            allowNull: false
        },
        customerID: {
            type: DataTypes.INTEGER,
            field: 'CustomerID',
            allowNull: false
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        rowStatusTypeID: {
            type: DataTypes.INTEGER,
            field: 'RowStatusTypeID',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'retail_CustomerEquipmentFinanceLoanApplicationRelationship',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var RetailCustomerequipmentfinanceloanapplicationrelationship = model.RetailCustomerequipmentfinanceloanapplicationrelationship;

};
