'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyAddress', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: false,
            references: {
                model: 'Party',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        addressID: {
            type: DataTypes.INTEGER,
            field: 'AddressID',
            allowNull: false,
            references: {
                model: 'Address',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        addressTypeID: {
            type: DataTypes.INTEGER,
            field: 'AddressTypeID',
            allowNull: false,
            references: {
                model: 'AddressType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        communicationTypeID: {
            type: DataTypes.INTEGER,
            field: 'CommunicationTypeID',
            allowNull: false,
            references: {
                model: 'CommunicationType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        currencyTypeID: {
            type: DataTypes.INTEGER,
            field: 'CurrencyTypeID',
            allowNull: false,
            references: {
                model: 'CurrencyType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        residencyTypeID: {
            type: DataTypes.INTEGER,
            field: 'ResidencyTypeID',
            allowNull: true,
            references: {
                model: 'ResidencyType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        locationTypeID: {
            type: DataTypes.INTEGER,
            field: 'LocationTypeID',
            allowNull: true,
            references: {
                model: 'LocationType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        periodOfOccupancyMonths: {
            type: DataTypes.INTEGER,
            field: 'PeriodOfOccupancyMonths',
            allowNull: true
        },
        periodOfOccupancyYears: {
            type: DataTypes.INTEGER,
            field: 'PeriodOfOccupancyYears',
            allowNull: true
        },
        priority: {
            type: DataTypes.INTEGER,
            field: 'Priority',
            allowNull: true
        },
        optOut: {
            type: DataTypes.BOOLEAN,
            field: 'OptOut',
            allowNull: true
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        lastUpdated: {
            type: DataTypes.DATE,
            field: 'LastUpdated',
            allowNull: false
        },
        lastUpdatedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'LastUpdatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'PartyAddress',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyAddress = model.PartyAddress;
    var AddressType = model.AddressType;
    var Address = model.Address;
    var CommunicationType = model.CommunicationType;
    var PartyRole = model.PartyRole;
    var CurrencyType = model.CurrencyType;
    var LocationType = model.LocationType;
    var Party = model.Party;
    var ResidencyType = model.ResidencyType;

    PartyAddress.belongsTo(AddressType, {
        as: 'AddressType',
        foreignKey: 'AddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyAddress.belongsTo(Address, {
        as: 'Address',
        foreignKey: 'AddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyAddress.belongsTo(CommunicationType, {
        as: 'CommunicationType',
        foreignKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyAddress.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyAddress.belongsTo(CurrencyType, {
        as: 'CurrencyType',
        foreignKey: 'CurrencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyAddress.belongsTo(LocationType, {
        as: 'LocationType',
        foreignKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyAddress.belongsTo(PartyRole, {
        as: 'LastUpdatedByPartyRole',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyAddress.belongsTo(Party, {
        as: 'Party',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyAddress.belongsTo(ResidencyType, {
        as: 'ResidencyType',
        foreignKey: 'ResidencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
