'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Message', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        messageTypeID: {
            type: DataTypes.INTEGER,
            field: 'MessageTypeID',
            allowNull: false,
            references: {
                model: 'MessageType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        messagePriorityTypeID: {
            type: DataTypes.INTEGER,
            field: 'MessagePriorityTypeID',
            allowNull: false,
            references: {
                model: 'MessagePriorityType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        subject: {
            type: DataTypes.STRING(100),
            field: 'Subject',
            allowNull: true
        },
        body: {
            type: DataTypes.TEXT,
            field: 'Body',
            allowNull: false
        },
        sent: {
            type: DataTypes.DATE,
            field: 'Sent',
            allowNull: false
        },
        sentFromSiteTypeID: {
            type: DataTypes.INTEGER,
            field: 'SentFromSiteTypeID',
            allowNull: true,
            references: {
                model: 'SiteType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        read: {
            type: DataTypes.DATE,
            field: 'Read',
            allowNull: true
        },
        readFromSiteTypeID: {
            type: DataTypes.INTEGER,
            field: 'ReadFromSiteTypeID',
            allowNull: true,
            references: {
                model: 'SiteType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        parentID: {
            type: DataTypes.INTEGER,
            field: 'ParentID',
            allowNull: true,
            references: {
                model: 'Message',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        deleted: {
            type: DataTypes.DATE,
            field: 'Deleted',
            allowNull: true
        },
        contentType: {
            type: DataTypes.STRING(50),
            field: 'ContentType',
            allowNull: true
        },
        retailMessageID: {
            type: DataTypes.INTEGER,
            field: 'RetailMessageID',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'Message',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var Message = model.Message;
    var MessageIdentifier = model.MessageIdentifier;
    var MessagePriorityType = model.MessagePriorityType;
    var MessageType = model.MessageType;
    var SiteType = model.SiteType;
    var IdentifierType = model.IdentifierType;

    Message.hasMany(Message, {
        as: 'ParentFks',
        foreignKey: 'ParentID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Message.hasMany(MessageIdentifier, {
        as: 'IdentifierMessageFks',
        foreignKey: 'MessageID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Message.belongsTo(MessagePriorityType, {
        as: 'MessagePriorityType',
        foreignKey: 'MessagePriorityTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Message.belongsTo(MessageType, {
        as: 'MessageType',
        foreignKey: 'MessageTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Message.belongsTo(Message, {
        as: 'Parent',
        foreignKey: 'ParentID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Message.belongsTo(SiteType, {
        as: 'ReadFromSiteType',
        foreignKey: 'ReadFromSiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Message.belongsTo(SiteType, {
        as: 'SentFromSiteType',
        foreignKey: 'SentFromSiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Message.belongsToMany(MessagePriorityType, {
        as: 'MessageMessagePriorityTypes',
        through: Message,
        foreignKey: 'ParentID',
        otherKey: 'MessagePriorityTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Message.belongsToMany(MessageType, {
        as: 'MessageMessageTypes',
        through: Message,
        foreignKey: 'ParentID',
        otherKey: 'MessageTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Message.belongsToMany(SiteType, {
        as: 'MessageReadFromSiteTypes',
        through: Message,
        foreignKey: 'ParentID',
        otherKey: 'ReadFromSiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Message.belongsToMany(SiteType, {
        as: 'MessageSentFromSiteTypes',
        through: Message,
        foreignKey: 'ParentID',
        otherKey: 'SentFromSiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Message.belongsToMany(IdentifierType, {
        as: 'MessageIdentifierIdentifierTypes',
        through: MessageIdentifier,
        foreignKey: 'MessageID',
        otherKey: 'IdentifierTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
