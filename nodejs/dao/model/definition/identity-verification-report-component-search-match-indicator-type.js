'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportComponentSearchMatchIndicatorType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(150),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportComponentSearchMatchIndicatorType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportComponentSearchMatchIndicatorType = model.IdentityVerificationReportComponentSearchMatchIndicatorType;
    var IdentityVerificationReportComponentSearch = model.IdentityVerificationReportComponentSearch;
    var IdentityVerificationReportComponentSearchValue = model.IdentityVerificationReportComponentSearchValue;
    var IdentityVerificationReportComponentSearchNameType = model.IdentityVerificationReportComponentSearchNameType;
    var IdentityVerificationReportComponentSearchResultCodeType = model.IdentityVerificationReportComponentSearchResultCodeType;
    var IdentityVerificationReportComponentSearchType = model.IdentityVerificationReportComponentSearchType;
    var IdentityVerificationReport = model.IdentityVerificationReport;
    var IdentityVerificationReportComponentSearchSubCategoryType = model.IdentityVerificationReportComponentSearchSubCategoryType;
    var IdentityVerificationReportComponentSearchCategoryType = model.IdentityVerificationReportComponentSearchCategoryType;

    IdentityVerificationReportComponentSearchMatchIndicatorType.hasMany(IdentityVerificationReportComponentSearch, {
        as: 'FKIdentityverificationreportcomponentsearchesIdentityverific1s',
        foreignKey: 'MatchIndicatorTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchMatchIndicatorType.hasMany(IdentityVerificationReportComponentSearchValue, {
        as: 'FKIdentityverificationreportcomponentsearchvaluesIdentityver2s',
        foreignKey: 'MatchIndicatorTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchMatchIndicatorType.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentSearchNameTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'MatchIndicatorTypeID',
        otherKey: 'NameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchMatchIndicatorType.belongsToMany(IdentityVerificationReportComponentSearchResultCodeType, {
        as: 'IdentityVerificationReportComponentSearchResultCodeTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'MatchIndicatorTypeID',
        otherKey: 'ResultCodeTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchMatchIndicatorType.belongsToMany(IdentityVerificationReportComponentSearchType, {
        as: 'IdentityVerificationReportComponentSearchSearchTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'MatchIndicatorTypeID',
        otherKey: 'SearchTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchMatchIndicatorType.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentSearchIdentityVerificationReports',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'MatchIndicatorTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchMatchIndicatorType.belongsToMany(IdentityVerificationReportComponentSearch, {
        as: 'IdentityVerificationReportComponentSearchValueSearches',
        through: IdentityVerificationReportComponentSearchValue,
        foreignKey: 'MatchIndicatorTypeID',
        otherKey: 'SearchID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchMatchIndicatorType.belongsToMany(IdentityVerificationReportComponentSearchSubCategoryType, {
        as: 'IdentityVerificationReportComponentSearchValueSubCategoryTypes',
        through: IdentityVerificationReportComponentSearchValue,
        foreignKey: 'MatchIndicatorTypeID',
        otherKey: 'SubCategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchMatchIndicatorType.belongsToMany(IdentityVerificationReportComponentSearchCategoryType, {
        as: 'IdentityVerificationReportComponentSearchValueCategoryTypes',
        through: IdentityVerificationReportComponentSearchValue,
        foreignKey: 'MatchIndicatorTypeID',
        otherKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
