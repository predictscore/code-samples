'use strict';

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('CampaignDeliveryType', {
    iD: {
      type: DataTypes.INTEGER,
      field: 'ID',
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(512),
      field: 'Name',
      allowNull: false
    },
    description: {
      type: DataTypes.INTEGER,
      field: 'Description',
      allowNull: false
    }
  }, {
    schema: 'public',
    tableName: 'CampaignDeliveryType',
    timestamps: false
  });
};

module.exports.initRelations = function() {
  delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
  var model = require('../index');
  var Campaign = model.Campaign;
  var CampaignDeliveryType = model.CampaignDeliveryType;

  CampaignDeliveryType.hasMany(Campaign, {
    as: 'CampaignDeliverytypeFK',
    foreignKey: 'deliveryTypeID',
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

};
