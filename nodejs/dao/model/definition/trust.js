'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Trust', {
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'Party',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        trustPurposeTypeID: {
            type: DataTypes.INTEGER,
            field: 'TrustPurposeTypeID',
            allowNull: true,
            references: {
                model: 'TrustPurposeType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        trustStructureTypeID: {
            type: DataTypes.INTEGER,
            field: 'TrustStructureTypeID',
            allowNull: true,
            references: {
                model: 'TrustStructureType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        trustName: {
            type: DataTypes.STRING(150),
            field: 'TrustName',
            allowNull: false
        },
        businessName: {
            type: DataTypes.STRING(150),
            field: 'BusinessName',
            allowNull: true
        },
        aBN: {
            type: DataTypes.STRING(15),
            field: 'ABN',
            allowNull: true
        },
        aCN: {
            type: DataTypes.STRING(15),
            field: 'ACN',
            allowNull: true
        },
        settlorName: {
            type: DataTypes.STRING(150),
            field: 'SettlorName',
            allowNull: true
        },
        countryEstablishedID: {
            type: DataTypes.INTEGER,
            field: 'CountryEstablishedID',
            allowNull: true,
            references: {
                model: 'Country',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        documentationTypeID: {
            type: DataTypes.INTEGER,
            field: 'DocumentationTypeID',
            allowNull: true,
            references: {
                model: 'DocumentationType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        establishmentDate: {
            type: DataTypes.DATEONLY,
            field: 'EstablishmentDate',
            allowNull: true
        },
        vestingDate: {
            type: DataTypes.DATEONLY,
            field: 'VestingDate',
            allowNull: true
        },
        numberOfTrustees: {
            type: DataTypes.INTEGER,
            field: 'NumberOfTrustees',
            allowNull: true
        },
        numberOfBeneficiaries: {
            type: DataTypes.INTEGER,
            field: 'NumberOfBeneficiaries',
            allowNull: true
        },
        accountantPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'AccountantPartyRoleID',
            allowNull: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        accountantContactName: {
            type: DataTypes.STRING(150),
            field: 'AccountantContactName',
            allowNull: true
        },
        isTradingEntity: {
            type: DataTypes.BOOLEAN,
            field: 'IsTradingEntity',
            allowNull: true
        },
        aCNRegistered: {
            type: DataTypes.DATEONLY,
            field: 'ACNRegistered',
            allowNull: true
        },
        aCNNotSupplied: {
            type: DataTypes.BOOLEAN,
            field: 'ACNNotSupplied',
            allowNull: true
        },
        lastUpdated: {
            type: DataTypes.DATE,
            field: 'LastUpdated',
            allowNull: false
        },
        lastUpdatedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'LastUpdatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        v8ID: {
            type: DataTypes.INTEGER,
            field: 'V8ID',
            allowNull: true
        },
        contactName: {
            type: DataTypes.STRING(200),
            field: 'ContactName',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'Trust',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var Trust = model.Trust;
    var PartyRole = model.PartyRole;
    var Country = model.Country;
    var DocumentationType = model.DocumentationType;
    var Party = model.Party;
    var TrustPurposeType = model.TrustPurposeType;
    var TrustStructureType = model.TrustStructureType;

    Trust.belongsTo(PartyRole, {
        as: 'AccountantPartyRole',
        foreignKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Trust.belongsTo(Country, {
        as: 'CountryEstablished',
        foreignKey: 'CountryEstablishedID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Trust.belongsTo(DocumentationType, {
        as: 'DocumentationType',
        foreignKey: 'DocumentationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Trust.belongsTo(PartyRole, {
        as: 'LastUpdatedByPartyRole',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Trust.belongsTo(Party, {
        as: 'Party',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Trust.belongsTo(TrustPurposeType, {
        as: 'TrustPurposeType',
        foreignKey: 'TrustPurposeTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Trust.belongsTo(TrustStructureType, {
        as: 'TrustStructureType',
        foreignKey: 'TrustStructureTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
