'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('CommunicationType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'CommunicationType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var CommunicationType = model.CommunicationType;
    var PartyAddress = model.PartyAddress;
    var PartyEmail = model.PartyEmail;
    var PartyTelephone = model.PartyTelephone;
    var AddressType = model.AddressType;
    var Address = model.Address;
    var PartyRole = model.PartyRole;
    var CurrencyType = model.CurrencyType;
    var LocationType = model.LocationType;
    var Party = model.Party;
    var ResidencyType = model.ResidencyType;
    var EmailAddress = model.EmailAddress;
    var EmailType = model.EmailType;
    var TelephoneNumber = model.TelephoneNumber;
    var TelephoneType = model.TelephoneType;

    CommunicationType.hasMany(PartyAddress, {
        as: 'PartyAddressCommunicationtypeFks',
        foreignKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.hasMany(PartyEmail, {
        as: 'PartyEmailCommunicationtypeFks',
        foreignKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.hasMany(PartyTelephone, {
        as: 'PartyTelephoneCommunicationtypeFks',
        foreignKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(AddressType, {
        as: 'PartyAddressAddressTypes',
        through: PartyAddress,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'AddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(Address, {
        as: 'PartyAddressAddresses',
        through: PartyAddress,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'AddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(PartyRole, {
        as: 'PartyAddressCreatedByPartyRoles',
        through: PartyAddress,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(CurrencyType, {
        as: 'PartyAddressCurrencyTypes',
        through: PartyAddress,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'CurrencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(LocationType, {
        as: 'PartyAddressLocationTypes',
        through: PartyAddress,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(PartyRole, {
        as: 'PartyAddressLastUpdatedByPartyRoles',
        through: PartyAddress,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(Party, {
        as: 'PartyAddressParties',
        through: PartyAddress,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(ResidencyType, {
        as: 'PartyAddressResidencyTypes',
        through: PartyAddress,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'ResidencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(PartyRole, {
        as: 'PartyEmailCreatedByPartyRoles',
        through: PartyEmail,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(EmailAddress, {
        as: 'PartyEmailEmailAddresses',
        through: PartyEmail,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'EmailAddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(EmailType, {
        as: 'PartyEmailEmailTypes',
        through: PartyEmail,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'EmailTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(PartyRole, {
        as: 'PartyEmailLastUpdatedByPartyRoles',
        through: PartyEmail,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(LocationType, {
        as: 'PartyEmailLocationTypes',
        through: PartyEmail,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(Party, {
        as: 'PartyEmailParties',
        through: PartyEmail,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(PartyRole, {
        as: 'PartyTelephoneCreatedByPartyRoles',
        through: PartyTelephone,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(PartyRole, {
        as: 'PartyTelephoneLastUpdatedByPartyRoles',
        through: PartyTelephone,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(LocationType, {
        as: 'PartyTelephoneLocationTypes',
        through: PartyTelephone,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(Party, {
        as: 'PartyTelephoneParties',
        through: PartyTelephone,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(TelephoneNumber, {
        as: 'PartyTelephoneTelephoneNumbers',
        through: PartyTelephone,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'TelephoneNumberID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CommunicationType.belongsToMany(TelephoneType, {
        as: 'PartyTelephoneTelephoneTypes',
        through: PartyTelephone,
        foreignKey: 'CommunicationTypeID',
        otherKey: 'TelephoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
