'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ApplicationType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'ApplicationType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var ApplicationType = model.ApplicationType;
    var ApplicationRole = model.ApplicationRole;
    var LoginPermissionType = model.LoginPermissionType;
    var PartyRoleTypeApplicationTypeRoleType = model.PartyRoleTypeApplicationTypeRoleType;
    var Login = model.Login;
    var RoleType = model.RoleType;
    var PermissionType = model.PermissionType;
    var PartyRoleType = model.PartyRoleType;

    ApplicationType.hasMany(ApplicationRole, {
        as: 'ApplicationRoleApplicationtypeFks',
        foreignKey: 'ApplicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ApplicationType.hasMany(LoginPermissionType, {
        as: 'LoginPermissionTypeApplicationtypeFks',
        foreignKey: 'ApplicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ApplicationType.hasMany(PartyRoleTypeApplicationTypeRoleType, {
        as: 'PartyRoleTypeApplicationTypeRoleTypeApplicationtypeFks',
        foreignKey: 'ApplicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ApplicationType.belongsToMany(Login, {
        as: 'ApplicationRoleLogins',
        through: ApplicationRole,
        foreignKey: 'ApplicationTypeID',
        otherKey: 'LoginID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ApplicationType.belongsToMany(RoleType, {
        as: 'ApplicationRoleRoleTypes',
        through: ApplicationRole,
        foreignKey: 'ApplicationTypeID',
        otherKey: 'RoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ApplicationType.belongsToMany(Login, {
        as: 'LoginPermissionTypeLogins',
        through: LoginPermissionType,
        foreignKey: 'ApplicationTypeID',
        otherKey: 'LoginID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ApplicationType.belongsToMany(PermissionType, {
        as: 'LoginPermissionTypePermissionTypes',
        through: LoginPermissionType,
        foreignKey: 'ApplicationTypeID',
        otherKey: 'PermissionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ApplicationType.belongsToMany(PartyRoleType, {
        as: 'PartyRoleTypeApplicationTypeRoleTypePartyRoleTypes',
        through: PartyRoleTypeApplicationTypeRoleType,
        foreignKey: 'ApplicationTypeID',
        otherKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    ApplicationType.belongsToMany(RoleType, {
        as: 'PartyRoleTypeApplicationTypeRoleTypeRoleTypes',
        through: PartyRoleTypeApplicationTypeRoleType,
        foreignKey: 'ApplicationTypeID',
        otherKey: 'RoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
