'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyRoleTypeRoleType', {
        partyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleTypeID',
            allowNull: false,
            references: {
                model: 'PartyRoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        roleTypeID: {
            type: DataTypes.INTEGER,
            field: 'RoleTypeID',
            allowNull: false,
            references: {
                model: 'RoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'PartyRoleTypeRoleType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyRoleTypeRoleType = model.PartyRoleTypeRoleType;
    var PartyRoleType = model.PartyRoleType;
    var RoleType = model.RoleType;

    PartyRoleTypeRoleType.belongsTo(PartyRoleType, {
        as: 'PartyRoleType',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleTypeRoleType.belongsTo(RoleType, {
        as: 'RoleType',
        foreignKey: 'RoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
