'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('V8Applicationpartyroleemploymentcontact', {
        applicationPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'ApplicationPartyRoleID',
            allowNull: false
        },
        applicationContainerID: {
            type: DataTypes.INTEGER,
            field: 'ApplicationContainerID',
            allowNull: false
        },
        crumbsPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CrumbsPartyRoleID',
            allowNull: true
        },
        employmentID: {
            type: DataTypes.INTEGER,
            field: 'EmploymentID',
            allowNull: true
        },
        crumbsEmployerTelephoneNumberID: {
            type: DataTypes.INTEGER,
            field: 'CrumbsEmployerTelephoneNumberID',
            allowNull: true
        },
        employerEmailAddressID: {
            type: DataTypes.INTEGER,
            field: 'EmployerEmailAddressID',
            allowNull: true
        },
        employerFaxNumberID: {
            type: DataTypes.INTEGER,
            field: 'EmployerFaxNumberID',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'v8_ApplicationPartyRoleEmploymentContacts',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var V8Applicationpartyroleemploymentcontact = model.V8Applicationpartyroleemploymentcontact;

};
