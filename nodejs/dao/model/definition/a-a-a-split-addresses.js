'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('AAASplitAddress', {
        cRMAddressID: {
            type: DataTypes.INTEGER,
            field: 'CRMAddressID',
            allowNull: false,
            primaryKey: true
        },
        cRMAddressLine1: {
            type: DataTypes.STRING(500),
            field: 'CRMAddressLine1',
            allowNull: true
        },
        col1: {
            type: DataTypes.STRING(500),
            field: 'Col1',
            allowNull: true
        },
        col2: {
            type: DataTypes.STRING(500),
            field: 'Col2',
            allowNull: true
        },
        col3: {
            type: DataTypes.STRING(500),
            field: 'Col3',
            allowNull: true
        },
        col4: {
            type: DataTypes.STRING(500),
            field: 'Col4',
            allowNull: true
        },
        col5: {
            type: DataTypes.STRING(500),
            field: 'Col5',
            allowNull: true
        },
        col6: {
            type: DataTypes.STRING(500),
            field: 'Col6',
            allowNull: true
        },
        col7: {
            type: DataTypes.STRING(500),
            field: 'Col7',
            allowNull: true
        },
        col8: {
            type: DataTypes.STRING(500),
            field: 'Col8',
            allowNull: true
        },
        col9: {
            type: DataTypes.STRING(500),
            field: 'Col9',
            allowNull: true
        },
        col10: {
            type: DataTypes.STRING(500),
            field: 'Col10',
            allowNull: true
        },
        col11: {
            type: DataTypes.STRING(500),
            field: 'Col11',
            allowNull: true
        },
        col12: {
            type: DataTypes.STRING(500),
            field: 'Col12',
            allowNull: true
        },
        col13: {
            type: DataTypes.STRING(500),
            field: 'Col13',
            allowNull: true
        },
        col14: {
            type: DataTypes.STRING(500),
            field: 'Col14',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'AAASplitAddresses',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var AAASplitAddress = model.AAASplitAddress;

};
