'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportComponentRuleReasonType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(500),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportComponentRuleReasonType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportComponentRuleReasonType = model.IdentityVerificationReportComponentRuleReasonType;
    var IdentityVerificationReportComponentRule = model.IdentityVerificationReportComponentRule;
    var IdentityVerificationReportComponentRuleNameType = model.IdentityVerificationReportComponentRuleNameType;
    var IdentityVerificationReport = model.IdentityVerificationReport;

    IdentityVerificationReportComponentRuleReasonType.hasMany(IdentityVerificationReportComponentRule, {
        as: 'FKIdentityverificationreportcomponentrulesIdentityverificati2s',
        foreignKey: 'ReasonTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentRuleReasonType.belongsToMany(IdentityVerificationReportComponentRuleNameType, {
        as: 'IdentityVerificationReportComponentRuleNameTypes',
        through: IdentityVerificationReportComponentRule,
        foreignKey: 'ReasonTypeID',
        otherKey: 'NameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentRuleReasonType.belongsToMany(IdentityVerificationReport, {
        as: 'IdentityVerificationReportComponentRuleIdentityVerificationReports',
        through: IdentityVerificationReportComponentRule,
        foreignKey: 'ReasonTypeID',
        otherKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
