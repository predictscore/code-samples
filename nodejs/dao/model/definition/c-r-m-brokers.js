'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('CRMBroker', {
        accountID: {
            type: DataTypes.UUID,
            field: 'AccountID',
            allowNull: false
        },
        accountNumber: {
            type: DataTypes.INTEGER,
            field: 'AccountNumber',
            allowNull: true
        },
        name: {
            type: DataTypes.STRING(500),
            field: 'Name',
            allowNull: true
        },
        created: {
            type: DataTypes.DATEONLY,
            field: 'Created',
            allowNull: true
        },
        countApps: {
            type: DataTypes.INTEGER,
            field: 'CountApps',
            allowNull: true
        },
        importStatus: {
            type: DataTypes.INTEGER,
            field: 'ImportStatus',
            allowNull: true
        },
        parentAccountID: {
            type: DataTypes.UUID,
            field: 'ParentAccountID',
            allowNull: true
        },
        relationshipTypeID: {
            type: DataTypes.INTEGER,
            field: 'RelationshipTypeID',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'CRMBrokers',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var CRMBroker = model.CRMBroker;

};
