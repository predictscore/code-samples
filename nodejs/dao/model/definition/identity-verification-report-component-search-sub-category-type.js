'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportComponentSearchSubCategoryType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(250),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportComponentSearchSubCategoryType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportComponentSearchSubCategoryType = model.IdentityVerificationReportComponentSearchSubCategoryType;
    var IdentityVerificationReportComponentSearchValue = model.IdentityVerificationReportComponentSearchValue;
    var IdentityVerificationReportComponentSearch = model.IdentityVerificationReportComponentSearch;
    var IdentityVerificationReportComponentSearchMatchIndicatorType = model.IdentityVerificationReportComponentSearchMatchIndicatorType;
    var IdentityVerificationReportComponentSearchCategoryType = model.IdentityVerificationReportComponentSearchCategoryType;

    IdentityVerificationReportComponentSearchSubCategoryType.hasMany(IdentityVerificationReportComponentSearchValue, {
        as: 'FKIdentityverificationreportcomponentsearchvaluesIdentityver3s',
        foreignKey: 'SubCategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchSubCategoryType.belongsToMany(IdentityVerificationReportComponentSearch, {
        as: 'IdentityVerificationReportComponentSearchValueSearches',
        through: IdentityVerificationReportComponentSearchValue,
        foreignKey: 'SubCategoryTypeID',
        otherKey: 'SearchID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchSubCategoryType.belongsToMany(IdentityVerificationReportComponentSearchMatchIndicatorType, {
        as: 'IdentityVerificationReportComponentSearchValueMatchIndicatorTypes',
        through: IdentityVerificationReportComponentSearchValue,
        foreignKey: 'SubCategoryTypeID',
        otherKey: 'MatchIndicatorTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchSubCategoryType.belongsToMany(IdentityVerificationReportComponentSearchCategoryType, {
        as: 'IdentityVerificationReportComponentSearchValueCategoryTypes',
        through: IdentityVerificationReportComponentSearchValue,
        foreignKey: 'SubCategoryTypeID',
        otherKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
