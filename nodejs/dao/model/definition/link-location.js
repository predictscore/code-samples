'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('LinkLocation', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(512),
            field: 'Name',
            allowNull: false
        },
        href: {
            type: DataTypes.STRING(512),
            field: 'Href',
            allowNull: false
        },
        expireOnAccess: {
            type: DataTypes.BOOLEAN,
            field: 'ExpireOnAccess',
            allowNull: false
        },
        lifespan: {
            type: DataTypes.BIGINT,
            field: 'Lifespan',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'LinkLocation',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var LinkLocation = model.LinkLocation;
    var Link = model.Link;
    var SiteTypeTemplateLinkLocation = model.SiteTypeTemplateLinkLocation;
    var SiteType = model.SiteType;

    LinkLocation.hasMany(Link, {
        as: 'LinkLinklocationFks',
        foreignKey: 'LinkLocationID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LinkLocation.hasMany(SiteTypeTemplateLinkLocation, {
        as: 'SiteTypeTemplateLinkLocationLinklocationFks',
        foreignKey: 'LinkLocationID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    LinkLocation.belongsToMany(SiteType, {
        as: 'SiteTypeTemplateLinkLocationSiteTypes',
        through: SiteTypeTemplateLinkLocation,
        foreignKey: 'LinkLocationID',
        otherKey: 'SiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
