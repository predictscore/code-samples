'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('RetailUserinfo', {
        userInfoID: {
            type: DataTypes.INTEGER,
            field: 'UserInfoID',
            allowNull: false
        },
        rowStatusTypeID: {
            type: DataTypes.INTEGER,
            field: 'RowStatusTypeID',
            allowNull: true
        },
        includeInUserDimension: {
            type: DataTypes.BOOLEAN,
            field: 'IncludeInUserDimension',
            allowNull: true
        },
        includeInSalesDashboard: {
            type: DataTypes.BOOLEAN,
            field: 'IncludeInSalesDashboard',
            allowNull: true
        },
        partyRoleID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleID',
            allowNull: true
        },
        tontoUserID: {
            type: DataTypes.INTEGER,
            field: 'TontoUserID',
            allowNull: true
        },
        bDMPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'BDMPartyRoleID',
            allowNull: true
        },
        crumbsLoginID: {
            type: DataTypes.INTEGER,
            field: 'CrumbsLoginID',
            allowNull: true
        },
        v8ID: {
            type: DataTypes.INTEGER,
            field: 'V8ID',
            allowNull: true
        },
        crumbsPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CrumbsPartyRoleID',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'retail_UserInfo',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var RetailUserinfo = model.RetailUserinfo;

};
