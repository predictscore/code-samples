'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportComponentRule', {
        identityVerificationReportID: {
            type: DataTypes.INTEGER,
            field: 'IdentityVerificationReportID',
            allowNull: false,
            references: {
                model: 'IdentityVerificationReport',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        nameTypeID: {
            type: DataTypes.INTEGER,
            field: 'NameTypeID',
            allowNull: false,
            references: {
                model: 'IdentityVerificationReportComponentRuleNameType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        indicator: {
            type: DataTypes.STRING(50),
            field: 'Indicator',
            allowNull: false
        },
        reasonTypeID: {
            type: DataTypes.INTEGER,
            field: 'ReasonTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentRuleReasonType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportComponentRules',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportComponentRule = model.IdentityVerificationReportComponentRule;
    var IdentityVerificationReportComponentRuleNameType = model.IdentityVerificationReportComponentRuleNameType;
    var IdentityVerificationReportComponentRuleReasonType = model.IdentityVerificationReportComponentRuleReasonType;
    var IdentityVerificationReport = model.IdentityVerificationReport;

    IdentityVerificationReportComponentRule.belongsTo(IdentityVerificationReportComponentRuleNameType, {
        as: 'NameType',
        foreignKey: 'NameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentRule.belongsTo(IdentityVerificationReportComponentRuleReasonType, {
        as: 'ReasonType',
        foreignKey: 'ReasonTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentRule.belongsTo(IdentityVerificationReport, {
        as: 'IdentityVerificationReport',
        foreignKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
