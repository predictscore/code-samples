'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('CurrencyType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        },
        vedaCode: {
            type: DataTypes.STRING(10),
            field: 'VedaCode',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'CurrencyType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var CurrencyType = model.CurrencyType;
    var BrandText = model.BrandText;
    var PartyAddress = model.PartyAddress;
    var AlignmentType = model.AlignmentType;
    var BrandType = model.BrandType;
    var DocumentLocationType = model.DocumentLocationType;
    var PartyRole = model.PartyRole;
    var AddressType = model.AddressType;
    var Address = model.Address;
    var CommunicationType = model.CommunicationType;
    var LocationType = model.LocationType;
    var Party = model.Party;
    var ResidencyType = model.ResidencyType;

    CurrencyType.hasMany(BrandText, {
        as: 'BrandTextCurrencytypeFks',
        foreignKey: 'CurrencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CurrencyType.hasMany(PartyAddress, {
        as: 'PartyAddressCurrencytypeFks',
        foreignKey: 'CurrencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CurrencyType.belongsToMany(AlignmentType, {
        as: 'BrandTextAlignmentTypes',
        through: BrandText,
        foreignKey: 'CurrencyTypeID',
        otherKey: 'AlignmentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CurrencyType.belongsToMany(BrandType, {
        as: 'BrandTextBrandTypes',
        through: BrandText,
        foreignKey: 'CurrencyTypeID',
        otherKey: 'BrandTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CurrencyType.belongsToMany(DocumentLocationType, {
        as: 'BrandTextDocumentLocationTypes',
        through: BrandText,
        foreignKey: 'CurrencyTypeID',
        otherKey: 'DocumentLocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CurrencyType.belongsToMany(PartyRole, {
        as: 'BrandTextPartyRoles',
        through: BrandText,
        foreignKey: 'CurrencyTypeID',
        otherKey: 'PartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CurrencyType.belongsToMany(AddressType, {
        as: 'PartyAddressAddressTypes',
        through: PartyAddress,
        foreignKey: 'CurrencyTypeID',
        otherKey: 'AddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CurrencyType.belongsToMany(Address, {
        as: 'PartyAddressAddresses',
        through: PartyAddress,
        foreignKey: 'CurrencyTypeID',
        otherKey: 'AddressID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CurrencyType.belongsToMany(CommunicationType, {
        as: 'PartyAddressCommunicationTypes',
        through: PartyAddress,
        foreignKey: 'CurrencyTypeID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CurrencyType.belongsToMany(PartyRole, {
        as: 'PartyAddressCreatedByPartyRoles',
        through: PartyAddress,
        foreignKey: 'CurrencyTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CurrencyType.belongsToMany(LocationType, {
        as: 'PartyAddressLocationTypes',
        through: PartyAddress,
        foreignKey: 'CurrencyTypeID',
        otherKey: 'LocationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CurrencyType.belongsToMany(PartyRole, {
        as: 'PartyAddressLastUpdatedByPartyRoles',
        through: PartyAddress,
        foreignKey: 'CurrencyTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CurrencyType.belongsToMany(Party, {
        as: 'PartyAddressParties',
        through: PartyAddress,
        foreignKey: 'CurrencyTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    CurrencyType.belongsToMany(ResidencyType, {
        as: 'PartyAddressResidencyTypes',
        through: PartyAddress,
        foreignKey: 'CurrencyTypeID',
        otherKey: 'ResidencyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
