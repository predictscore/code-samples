'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Brand', {
        partyRoleID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        sPVName: {
            type: DataTypes.STRING(300),
            field: 'SPVName',
            allowNull: true
        },
        sPVACN: {
            type: DataTypes.STRING(20),
            field: 'SPVACN',
            allowNull: true
        },
        ultracsBranchID: {
            type: DataTypes.INTEGER,
            field: 'UltracsBranchID',
            allowNull: true
        },
        vedaSubscriberID: {
            type: DataTypes.STRING(50),
            field: 'VedaSubscriberID',
            allowNull: true
        },
        vedaOperatorID: {
            type: DataTypes.STRING(50),
            field: 'VedaOperatorID',
            allowNull: true
        },
        documentBrandLevelTypeID: {
            type: DataTypes.INTEGER,
            field: 'DocumentBrandLevelTypeID',
            allowNull: true,
            references: {
                model: 'BrandLevelType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        onlineBrandLevelTypeID: {
            type: DataTypes.INTEGER,
            field: 'OnlineBrandLevelTypeID',
            allowNull: true,
            references: {
                model: 'BrandLevelType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        lastUpdated: {
            type: DataTypes.DATE,
            field: 'LastUpdated',
            allowNull: false
        },
        lastUpdatedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'LastUpdatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'Brand',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var Brand = model.Brand;
    var PartyRole = model.PartyRole;
    var BrandLevelType = model.BrandLevelType;

    Brand.belongsTo(BrandLevelType, {
        as: 'DocumentBrandLevelType',
        foreignKey: 'DocumentBrandLevelTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Brand.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Brand.belongsTo(PartyRole, {
        as: 'LastUpdatedByPartyRole',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Brand.belongsTo(BrandLevelType, {
        as: 'OnlineBrandLevelType',
        foreignKey: 'OnlineBrandLevelTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Brand.belongsTo(PartyRole, {
        as: 'PartyRole',
        foreignKey: 'PartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
