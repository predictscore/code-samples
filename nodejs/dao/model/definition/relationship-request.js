'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('RelationshipRequest', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        relationshipTypeID: {
            type: DataTypes.INTEGER,
            field: 'RelationshipTypeID',
            allowNull: false,
            references: {
                model: 'RelationshipType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        requestorPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'RequestorPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        requesteePartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'RequesteePartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        approved: {
            type: DataTypes.DATE,
            field: 'Approved',
            allowNull: true
        },
        approvedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'ApprovedByPartyRoleID',
            allowNull: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        declined: {
            type: DataTypes.DATE,
            field: 'Declined',
            allowNull: true
        },
        declinedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'DeclinedByPartyRoleID',
            allowNull: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'RelationshipRequest',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var RelationshipRequest = model.RelationshipRequest;
    var PartyRole = model.PartyRole;
    var RelationshipType = model.RelationshipType;

    RelationshipRequest.belongsTo(PartyRole, {
        as: 'ApprovedByPartyRole',
        foreignKey: 'ApprovedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipRequest.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipRequest.belongsTo(PartyRole, {
        as: 'DeclinedByPartyRole',
        foreignKey: 'DeclinedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipRequest.belongsTo(RelationshipType, {
        as: 'RelationshipType',
        foreignKey: 'RelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipRequest.belongsTo(PartyRole, {
        as: 'RequesteePartyRole',
        foreignKey: 'RequesteePartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    RelationshipRequest.belongsTo(PartyRole, {
        as: 'RequestorPartyRole',
        foreignKey: 'RequestorPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
