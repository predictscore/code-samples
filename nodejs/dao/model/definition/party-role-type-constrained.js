'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyRoleTypeConstrained', {
        parentPartyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'ParentPartyRoleTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'PartyRoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        constrainedPartyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'ConstrainedPartyRoleTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'PartyRoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'PartyRoleTypeConstrained',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyRoleTypeConstrained = model.PartyRoleTypeConstrained;
    var PartyRoleType = model.PartyRoleType;

    PartyRoleTypeConstrained.belongsTo(PartyRoleType, {
        as: 'ConstrainedPartyRoleType',
        foreignKey: 'ConstrainedPartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleTypeConstrained.belongsTo(PartyRoleType, {
        as: 'ParentPartyRoleType',
        foreignKey: 'ParentPartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
