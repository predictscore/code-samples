'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('MessageIdentifier', {
        messageID: {
            type: DataTypes.INTEGER,
            field: 'MessageID',
            allowNull: false,
            references: {
                model: 'Message',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        identifierTypeID: {
            type: DataTypes.INTEGER,
            field: 'IdentifierTypeID',
            allowNull: false,
            references: {
                model: 'IdentifierType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        value: {
            type: DataTypes.STRING(512),
            field: 'Value',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'MessageIdentifier',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var MessageIdentifier = model.MessageIdentifier;
    var IdentifierType = model.IdentifierType;
    var Message = model.Message;

    MessageIdentifier.belongsTo(IdentifierType, {
        as: 'IdentifierType',
        foreignKey: 'IdentifierTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    MessageIdentifier.belongsTo(Message, {
        as: 'Message',
        foreignKey: 'MessageID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
