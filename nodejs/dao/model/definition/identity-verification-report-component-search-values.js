'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportComponentSearchValue', {
        searchID: {
            type: DataTypes.INTEGER,
            field: 'SearchID',
            allowNull: false,
            references: {
                model: 'IdentityVerificationReportComponentSearches',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        categoryTypeID: {
            type: DataTypes.INTEGER,
            field: 'CategoryTypeID',
            allowNull: false,
            references: {
                model: 'IdentityVerificationReportComponentSearchCategoryType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        subCategoryTypeID: {
            type: DataTypes.INTEGER,
            field: 'SubCategoryTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentSearchSubCategoryType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        value: {
            type: DataTypes.STRING(1000),
            field: 'Value',
            allowNull: true
        },
        score: {
            type: DataTypes.STRING(500),
            field: 'Score',
            allowNull: true
        },
        weight: {
            type: DataTypes.STRING(50),
            field: 'Weight',
            allowNull: true
        },
        matchIndicatorTypeID: {
            type: DataTypes.INTEGER,
            field: 'MatchIndicatorTypeID',
            allowNull: true,
            references: {
                model: 'IdentityVerificationReportComponentSearchMatchIndicatorType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        recordID: {
            type: DataTypes.STRING(150),
            field: 'RecordID',
            allowNull: true
        },
        otherID: {
            type: DataTypes.STRING(150),
            field: 'OtherID',
            allowNull: true
        },
        otherIDReference: {
            type: DataTypes.STRING(150),
            field: 'OtherIDReference',
            allowNull: true
        },
        otherValue: {
            type: DataTypes.STRING(1000),
            field: 'OtherValue',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportComponentSearchValues',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportComponentSearchValue = model.IdentityVerificationReportComponentSearchValue;
    var IdentityVerificationReportComponentSearch = model.IdentityVerificationReportComponentSearch;
    var IdentityVerificationReportComponentSearchMatchIndicatorType = model.IdentityVerificationReportComponentSearchMatchIndicatorType;
    var IdentityVerificationReportComponentSearchSubCategoryType = model.IdentityVerificationReportComponentSearchSubCategoryType;
    var IdentityVerificationReportComponentSearchCategoryType = model.IdentityVerificationReportComponentSearchCategoryType;

    IdentityVerificationReportComponentSearchValue.belongsTo(IdentityVerificationReportComponentSearch, {
        as: 'Search',
        foreignKey: 'SearchID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchValue.belongsTo(IdentityVerificationReportComponentSearchMatchIndicatorType, {
        as: 'MatchIndicatorType',
        foreignKey: 'MatchIndicatorTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchValue.belongsTo(IdentityVerificationReportComponentSearchSubCategoryType, {
        as: 'SubCategoryType',
        foreignKey: 'SubCategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchValue.belongsTo(IdentityVerificationReportComponentSearchCategoryType, {
        as: 'CategoryType',
        foreignKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
