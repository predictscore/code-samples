'use strict';

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Campaign', {
    iD: {
      type: DataTypes.INTEGER,
      field: 'ID',
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(512),
      field: 'Name',
      allowNull: false
    },
    listID: {
      type: DataTypes.INTEGER,
      field: 'ListID',
      allowNull: false,
      references: {
        model: 'List',
        key: 'ID'
      },
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION'
    },
    deliveryTypeID: {
      type: DataTypes.INTEGER,
      field: 'DeliveryTypeID',
      allowNull: false,
      references: {
        model: 'DeliveryTypeID',
        key: 'ID'
      },
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION'
    },
    emailTemplateID: {
      type: DataTypes.STRING(512),
      field: 'EmailTemplateID',
      allowNull: true
    },
    smsTemplateID: {
      type: DataTypes.STRING(512),
      field: 'SmsTemplateID',
      allowNull: true
    },
    jobTypeID: {
      type: DataTypes.INTEGER,
      field: 'JobTypeID',
      allowNull: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      field: 'Active',
      allowNull: true
    },
    runDate: {
      type: DataTypes.DATE,
      field: 'RunDate',
      allowNull: true
    },
    created: {
      type: DataTypes.DATE,
      field: 'Created',
      allowNull: false
    },
    createdByPartyRoleID: {
      type: DataTypes.INTEGER,
      field: 'CreatedByPartyRoleID',
      allowNull: false,
      references: {
        model: 'PartyRole',
        key: 'ID'
      },
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION'
    },
    lastUpdated: {
      type: DataTypes.DATE,
      field: 'LastUpdated',
      allowNull: false
    },
    lastUpdatedByPartyRoleID: {
      type: DataTypes.INTEGER,
      field: 'LastUpdatedByPartyRoleID',
      allowNull: false,
      references: {
        model: 'PartyRole',
        key: 'ID'
      },
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION'
    }
  }, {
    schema: 'public',
    tableName: 'Campaign',
    timestamps: false
  });
};

module.exports.initRelations = function() {
  delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
  var model = require('../index');
  var Campaign = model.Campaign;
  var List = model.List;
  var CreatedBy = model.PartyRole;
  var LastUpdatedBy = model.PartyRole;
  var CampaignDeliveryType = model.CampaignDeliveryType;

  Campaign.belongsTo(CreatedBy, {
    as: 'Campaign',
    foreignKey: 'createdByPartyRoleID',
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

  Campaign.belongsTo(LastUpdatedBy, {
    as: 'LastUpdatedBy',
    foreignKey: 'lastUpdatedByPartyRoleID',
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

  Campaign.belongsTo(List, {
    as: 'List',
    foreignKey: 'listID',
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

  Campaign.belongsTo(CampaignDeliveryType, {
    as: 'CampaignDeliveryType',
    foreignKey: 'deliveryTypeID',
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

};
