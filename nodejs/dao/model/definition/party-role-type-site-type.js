'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyRoleTypeSiteType', {
        partyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleTypeID',
            allowNull: false,
            references: {
                model: 'PartyRoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        siteTypeID: {
            type: DataTypes.INTEGER,
            field: 'SiteTypeID',
            allowNull: false,
            references: {
                model: 'SiteType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'PartyRoleTypeSiteType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyRoleTypeSiteType = model.PartyRoleTypeSiteType;
    var PartyRoleType = model.PartyRoleType;
    var SiteType = model.SiteType;

    PartyRoleTypeSiteType.belongsTo(PartyRoleType, {
        as: 'PartyRoleType',
        foreignKey: 'PartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleTypeSiteType.belongsTo(SiteType, {
        as: 'SiteType',
        foreignKey: 'SiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
