'use strict';

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ListExclusion', {
    iD: {
      type: DataTypes.INTEGER,
      field: 'ID',
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    listId: {
      type: DataTypes.INTEGER,
      field: 'ListID',
      allowNull: false,
      references: {
        model: 'List',
        key: 'ID'
      },
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION'
    },
    partyID: {
      type: DataTypes.INTEGER,
      field: 'PartyID',
      allowNull: false,
      references: {
        model: 'Party',
        key: 'ID'
      },
      onUpdate: 'NO ACTION',
      onDelete: 'NO ACTION'
    }
  }, {
    schema: 'public',
    tableName: 'ListExclusion',
    timestamps: false
  });
};

module.exports.initRelations = function() {
  delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
  var model = require('../index');
  var ListExclusion = model.ListExclusion;
  var List = model.List;
  var Party = model.Party;

  ListExclusion.belongsTo(List, {
    as: 'ListExclusion',
    foreignKey: 'listID',
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

  ListExclusion.belongsTo(Party, {
    as: 'ListExclusion',
    foreignKey: 'partyID',
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

};
