'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyBankAccountType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'PartyBankAccountType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyBankAccountType = model.PartyBankAccountType;
    var PartyBankAccount = model.PartyBankAccount;
    var BankAccount = model.BankAccount;
    var BankBSB = model.BankBSB;
    var PartyRole = model.PartyRole;
    var Party = model.Party;

    PartyBankAccountType.hasMany(PartyBankAccount, {
        as: 'PartyBankAccountPartybankaccounttypeFks',
        foreignKey: 'PartyBankAccountTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyBankAccountType.belongsToMany(BankAccount, {
        as: 'PartyBankAccountBankAccounts',
        through: PartyBankAccount,
        foreignKey: 'PartyBankAccountTypeID',
        otherKey: 'BankAccountID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyBankAccountType.belongsToMany(BankBSB, {
        as: 'PartyBankAccountBankBSBs',
        through: PartyBankAccount,
        foreignKey: 'PartyBankAccountTypeID',
        otherKey: 'BankBSBID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyBankAccountType.belongsToMany(PartyRole, {
        as: 'PartyBankAccountCreatedByPartyRoles',
        through: PartyBankAccount,
        foreignKey: 'PartyBankAccountTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyBankAccountType.belongsToMany(PartyRole, {
        as: 'PartyBankAccountLastUpdateByPartyRoles',
        through: PartyBankAccount,
        foreignKey: 'PartyBankAccountTypeID',
        otherKey: 'LastUpdateByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyBankAccountType.belongsToMany(Party, {
        as: 'PartyBankAccountParties',
        through: PartyBankAccount,
        foreignKey: 'PartyBankAccountTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
