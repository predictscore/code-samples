'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('SearchType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'SearchType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var SearchType = model.SearchType;
    var PartySearchAvailability = model.PartySearchAvailability;
    var PartyRole = model.PartyRole;
    var Party = model.Party;

    SearchType.hasMany(PartySearchAvailability, {
        as: 'PartySearchAvailabilitySearchtypeFks',
        foreignKey: 'SearchTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SearchType.belongsToMany(PartyRole, {
        as: 'PartySearchAvailabilityCreatedByPartyRoles',
        through: PartySearchAvailability,
        foreignKey: 'SearchTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SearchType.belongsToMany(PartyRole, {
        as: 'PartySearchAvailabilityLastUpdatedByPartyRoles',
        through: PartySearchAvailability,
        foreignKey: 'SearchTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SearchType.belongsToMany(Party, {
        as: 'PartySearchAvailabilityParties',
        through: PartySearchAvailability,
        foreignKey: 'SearchTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
