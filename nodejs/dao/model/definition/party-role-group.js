'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyRoleGroup', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(100),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'PartyRoleGroup',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyRoleGroup = model.PartyRoleGroup;
    var PartyRoleType = model.PartyRoleType;
    var PartyRoleCategory = model.PartyRoleCategory;
    var PartyRoleSuite = model.PartyRoleSuite;
    var PartyType = model.PartyType;

    PartyRoleGroup.hasMany(PartyRoleType, {
        as: 'PartyRoleTypePartyrolegroupFks',
        foreignKey: 'PartyRoleGroupID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleGroup.belongsToMany(PartyRoleCategory, {
        as: 'PartyRoleTypePartyRoleCategories',
        through: PartyRoleType,
        foreignKey: 'PartyRoleGroupID',
        otherKey: 'PartyRoleCategoryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleGroup.belongsToMany(PartyRoleSuite, {
        as: 'PartyRoleTypePartyRoleSuites',
        through: PartyRoleType,
        foreignKey: 'PartyRoleGroupID',
        otherKey: 'PartyRoleSuiteID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleGroup.belongsToMany(PartyType, {
        as: 'PartyRoleTypePartyTypes',
        through: PartyRoleType,
        foreignKey: 'PartyRoleGroupID',
        otherKey: 'PartyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
