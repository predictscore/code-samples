'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyRoleTypeRelationshipType', {
        parentPartyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'ParentPartyRoleTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'PartyRoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        childPartyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'ChildPartyRoleTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'PartyRoleType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        relationshipTypeID: {
            type: DataTypes.INTEGER,
            field: 'RelationshipTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'RelationshipType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        parentRoleText: {
            type: DataTypes.STRING(100),
            field: 'ParentRoleText',
            allowNull: false
        },
        childRoleText: {
            type: DataTypes.STRING(100),
            field: 'ChildRoleText',
            allowNull: false
        },
        singular: {
            type: DataTypes.BOOLEAN,
            field: 'Singular',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'PartyRoleTypeRelationshipType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyRoleTypeRelationshipType = model.PartyRoleTypeRelationshipType;
    var PartyRoleType = model.PartyRoleType;
    var RelationshipType = model.RelationshipType;

    PartyRoleTypeRelationshipType.belongsTo(PartyRoleType, {
        as: 'ChildPartyRoleType',
        foreignKey: 'ChildPartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleTypeRelationshipType.belongsTo(PartyRoleType, {
        as: 'ParentPartyRoleType',
        foreignKey: 'ParentPartyRoleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyRoleTypeRelationshipType.belongsTo(RelationshipType, {
        as: 'RelationshipType',
        foreignKey: 'RelationshipTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
