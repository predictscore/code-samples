'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportComponentSearchCategoryType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(150),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportComponentSearchCategoryType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportComponentSearchCategoryType = model.IdentityVerificationReportComponentSearchCategoryType;
    var IdentityVerificationReportComponentSearchValue = model.IdentityVerificationReportComponentSearchValue;
    var IdentityVerificationReportComponentSearch = model.IdentityVerificationReportComponentSearch;
    var IdentityVerificationReportComponentSearchMatchIndicatorType = model.IdentityVerificationReportComponentSearchMatchIndicatorType;
    var IdentityVerificationReportComponentSearchSubCategoryType = model.IdentityVerificationReportComponentSearchSubCategoryType;

    IdentityVerificationReportComponentSearchCategoryType.hasMany(IdentityVerificationReportComponentSearchValue, {
        as: 'FKIdentityverificationreportcomponentsearchvaluesIdentityveris',
        foreignKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchCategoryType.belongsToMany(IdentityVerificationReportComponentSearch, {
        as: 'IdentityVerificationReportComponentSearchValueSearches',
        through: IdentityVerificationReportComponentSearchValue,
        foreignKey: 'CategoryTypeID',
        otherKey: 'SearchID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchCategoryType.belongsToMany(IdentityVerificationReportComponentSearchMatchIndicatorType, {
        as: 'IdentityVerificationReportComponentSearchValueMatchIndicatorTypes',
        through: IdentityVerificationReportComponentSearchValue,
        foreignKey: 'CategoryTypeID',
        otherKey: 'MatchIndicatorTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportComponentSearchCategoryType.belongsToMany(IdentityVerificationReportComponentSearchSubCategoryType, {
        as: 'IdentityVerificationReportComponentSearchValueSubCategoryTypes',
        through: IdentityVerificationReportComponentSearchValue,
        foreignKey: 'CategoryTypeID',
        otherKey: 'SubCategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
