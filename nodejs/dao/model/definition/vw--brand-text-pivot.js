'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('VwBrandtextPivot', {
        partyRoleID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleID',
            allowNull: true
        },
        brandTypeID: {
            type: DataTypes.INTEGER,
            field: 'BrandTypeID',
            allowNull: true
        },
        currencyTypeID: {
            type: DataTypes.INTEGER,
            field: 'CurrencyTypeID',
            allowNull: true
        },
        australianPhone: {
            type: DataTypes.TEXT,
            field: 'AustralianPhone',
            allowNull: true
        },
        internationalPhone: {
            type: DataTypes.TEXT,
            field: 'InternationalPhone',
            allowNull: true
        },
        emailAddress: {
            type: DataTypes.TEXT,
            field: 'EmailAddress',
            allowNull: true
        },
        headerLogo: {
            type: DataTypes.TEXT,
            field: 'HeaderLogo',
            allowNull: true
        },
        headerAlignmentTypeID: {
            type: DataTypes.TEXT,
            field: 'HeaderAlignmentTypeID',
            allowNull: true
        },
        headerAlignmentTypeName: {
            type: DataTypes.TEXT,
            field: 'HeaderAlignmentTypeName',
            allowNull: true
        },
        signatureImage: {
            type: DataTypes.TEXT,
            field: 'SignatureImage',
            allowNull: true
        },
        signatureTextLine1: {
            type: DataTypes.TEXT,
            field: 'SignatureTextLine1',
            allowNull: true
        },
        signatureTextLine2: {
            type: DataTypes.TEXT,
            field: 'SignatureTextLine2',
            allowNull: true
        },
        footerAlignment: {
            type: DataTypes.TEXT,
            field: 'FooterAlignment',
            allowNull: true
        },
        footerAlignmentTypeID: {
            type: DataTypes.TEXT,
            field: 'FooterAlignmentTypeID',
            allowNull: true
        },
        footerAlignmentTypeName: {
            type: DataTypes.TEXT,
            field: 'FooterAlignmentTypeName',
            allowNull: true
        },
        footerLine1Column1: {
            type: DataTypes.TEXT,
            field: 'FooterLine1Column1',
            allowNull: true
        },
        footerLine2Column1: {
            type: DataTypes.TEXT,
            field: 'FooterLine2Column1',
            allowNull: true
        },
        footerLine3Column1: {
            type: DataTypes.TEXT,
            field: 'FooterLine3Column1',
            allowNull: true
        },
        footerLine4Column1: {
            type: DataTypes.TEXT,
            field: 'FooterLine4Column1',
            allowNull: true
        },
        footerLine5Column1: {
            type: DataTypes.TEXT,
            field: 'FooterLine5Column1',
            allowNull: true
        },
        servicesUrl: {
            type: DataTypes.TEXT,
            field: 'ServicesUrl',
            allowNull: true
        },
        footerLine1Column2: {
            type: DataTypes.TEXT,
            field: 'FooterLine1Column2',
            allowNull: true
        },
        footerLine2Column2: {
            type: DataTypes.TEXT,
            field: 'FooterLine2Column2',
            allowNull: true
        },
        footerLine3Column2: {
            type: DataTypes.TEXT,
            field: 'FooterLine3Column2',
            allowNull: true
        },
        footerLine4Column2: {
            type: DataTypes.TEXT,
            field: 'FooterLine4Column2',
            allowNull: true
        },
        footerLine5Column2: {
            type: DataTypes.TEXT,
            field: 'FooterLine5Column2',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'vw_BrandText_pivot',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var VwBrandtextPivot = model.VwBrandtextPivot;

};
