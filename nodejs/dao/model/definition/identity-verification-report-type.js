'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReportType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            field: 'Name',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        },
        justiceID: {
            type: DataTypes.INTEGER,
            field: 'JusticeID',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReportType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReportType = model.IdentityVerificationReportType;
    var IdentityVerificationReport = model.IdentityVerificationReport;
    var PartyRole = model.PartyRole;
    var Party = model.Party;

    IdentityVerificationReportType.hasMany(IdentityVerificationReport, {
        as: 'FKIdentityverificationreportIdentityverificationreports',
        foreignKey: 'IdentityVerificationReportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportType.belongsToMany(PartyRole, {
        as: 'IdentityVerificationReportCreatedByPartyRoles',
        through: IdentityVerificationReport,
        foreignKey: 'IdentityVerificationReportTypeID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportType.belongsToMany(PartyRole, {
        as: 'IdentityVerificationReportLastUpdatedByPartyRoles',
        through: IdentityVerificationReport,
        foreignKey: 'IdentityVerificationReportTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReportType.belongsToMany(Party, {
        as: 'IdentityVerificationReportParties',
        through: IdentityVerificationReport,
        foreignKey: 'IdentityVerificationReportTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
