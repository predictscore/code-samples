'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('RetailApplicationcustomerlogin', {
        applicationReferenceID: {
            type: DataTypes.INTEGER,
            field: 'ApplicationReferenceID',
            allowNull: false
        },
        productCategoryID: {
            type: DataTypes.INTEGER,
            field: 'ProductCategoryID',
            allowNull: false
        },
        crumbsLoginID: {
            type: DataTypes.INTEGER,
            field: 'CrumbsLoginID',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'retail_ApplicationCustomerLogins',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var RetailApplicationcustomerlogin = model.RetailApplicationcustomerlogin;

};
