'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('SecondaryAuthorityParameter', {
        secondaryAuthorityID: {
            type: DataTypes.INTEGER,
            field: 'SecondaryAuthorityID',
            allowNull: false,
            references: {
                model: 'SecondaryAuthority',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        value: {
            type: DataTypes.STRING(512),
            field: 'Value',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'SecondaryAuthorityParameter',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var SecondaryAuthorityParameter = model.SecondaryAuthorityParameter;
    var SecondaryAuthority = model.SecondaryAuthority;

    SecondaryAuthorityParameter.belongsTo(SecondaryAuthority, {
        as: 'SecondaryAuthority',
        foreignKey: 'SecondaryAuthorityID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
