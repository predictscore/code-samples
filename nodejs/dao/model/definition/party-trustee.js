'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyTrustee', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        trusteePartyID: {
            type: DataTypes.INTEGER,
            field: 'TrusteePartyID',
            allowNull: false,
            references: {
                model: 'Party',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        trustPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'TrustPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        appointmentDate: {
            type: DataTypes.DATE,
            field: 'AppointmentDate',
            allowNull: true
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'PartyTrustee',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyTrustee = model.PartyTrustee;
    var PartyRole = model.PartyRole;
    var Party = model.Party;

    PartyTrustee.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyTrustee.belongsTo(PartyRole, {
        as: 'TrustPartyRole',
        foreignKey: 'TrustPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    PartyTrustee.belongsTo(Party, {
        as: 'TrusteeParty',
        foreignKey: 'TrusteePartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
