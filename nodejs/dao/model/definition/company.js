'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Company', {
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'Party',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        companyTypeID: {
            type: DataTypes.INTEGER,
            field: 'CompanyTypeID',
            allowNull: true,
            references: {
                model: 'CompanyType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        companyName: {
            type: DataTypes.STRING(150),
            field: 'CompanyName',
            allowNull: false
        },
        preferredName: {
            type: DataTypes.STRING(150),
            field: 'PreferredName',
            allowNull: true
        },
        shortName: {
            type: DataTypes.STRING(50),
            field: 'ShortName',
            allowNull: true
        },
        aCN: {
            type: DataTypes.STRING(15),
            field: 'ACN',
            allowNull: true
        },
        aBN: {
            type: DataTypes.STRING(15),
            field: 'ABN',
            allowNull: true
        },
        aBNHeldForMoreThan3Years: {
            type: DataTypes.BOOLEAN,
            field: 'ABNHeldForMoreThan3Years',
            allowNull: true
        },
        aBNHeldForMonths: {
            type: DataTypes.INTEGER,
            field: 'ABNHeldForMonths',
            allowNull: true
        },
        aBNHeldForYears: {
            type: DataTypes.INTEGER,
            field: 'ABNHeldForYears',
            allowNull: true
        },
        aBNRegistered: {
            type: DataTypes.DATEONLY,
            field: 'ABNRegistered',
            allowNull: true
        },
        aBRN: {
            type: DataTypes.STRING(20),
            field: 'ABRN',
            allowNull: true
        },
        registeredInStateID: {
            type: DataTypes.INTEGER,
            field: 'RegisteredInStateID',
            allowNull: true,
            references: {
                model: 'State',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        aBNVerified: {
            type: DataTypes.BOOLEAN,
            field: 'ABNVerified',
            allowNull: true
        },
        numberOfDirectors: {
            type: DataTypes.INTEGER,
            field: 'NumberOfDirectors',
            allowNull: true
        },
        numberOfShareholders: {
            type: DataTypes.INTEGER,
            field: 'NumberOfShareholders',
            allowNull: true
        },
        numberOfPartners: {
            type: DataTypes.INTEGER,
            field: 'NumberOfPartners',
            allowNull: true
        },
        accountantPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'AccountantPartyRoleID',
            allowNull: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        accountantContactName: {
            type: DataTypes.STRING(150),
            field: 'AccountantContactName',
            allowNull: true
        },
        industryTypeID: {
            type: DataTypes.INTEGER,
            field: 'IndustryTypeID',
            allowNull: true,
            references: {
                model: 'IndustryType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        mainBusinessActivityID: {
            type: DataTypes.INTEGER,
            field: 'MainBusinessActivityID',
            allowNull: true,
            references: {
                model: 'MainBusinessActivityType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        commissionPayable: {
            type: DataTypes.BOOLEAN,
            field: 'CommissionPayable',
            allowNull: true
        },
        accreditationStatusTypeID: {
            type: DataTypes.INTEGER,
            field: 'AccreditationStatusTypeID',
            allowNull: true,
            references: {
                model: 'AccreditationStatusType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        lastUpdated: {
            type: DataTypes.DATE,
            field: 'LastUpdated',
            allowNull: false
        },
        lastUpdatedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'LastUpdatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        v8ID: {
            type: DataTypes.INTEGER,
            field: 'V8ID',
            allowNull: true
        },
        aCNRegistered: {
            type: DataTypes.DATEONLY,
            field: 'ACNRegistered',
            allowNull: true
        },
        contactName: {
            type: DataTypes.STRING(200),
            field: 'ContactName',
            allowNull: true
        },
        accountRankingTypeID: {
            type: DataTypes.INTEGER,
            field: 'AccountRankingTypeID',
            allowNull: true,
            references: {
                model: 'AccountRankingType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        declarationConfirmed: {
            type: DataTypes.BOOLEAN,
            field: 'DeclarationConfirmed',
            allowNull: true
        },
        prospectSource: {
            type: DataTypes.STRING(200),
            field: 'ProspectSource',
            allowNull: true
        },
        equipmentFinanceProductCategoryID: {
            type: DataTypes.INTEGER,
            field: 'EquipmentFinanceProductCategoryID',
            allowNull: true
        },
        alternateDisplayName: {
            type: DataTypes.STRING(150),
            field: 'AlternateDisplayName',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'Company',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var Company = model.Company;
    var AccountRankingType = model.AccountRankingType;
    var PartyRole = model.PartyRole;
    var AccreditationStatusType = model.AccreditationStatusType;
    var CompanyType = model.CompanyType;
    var IndustryType = model.IndustryType;
    var MainBusinessActivityType = model.MainBusinessActivityType;
    var Party = model.Party;
    var State = model.State;

    Company.hasMany(PartyRole, {
        as: 'RolePartyFks',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Company.belongsTo(AccountRankingType, {
        as: 'AccountRankingType',
        foreignKey: 'AccountRankingTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Company.belongsTo(PartyRole, {
        as: 'AccountantPartyRole',
        foreignKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Company.belongsTo(AccreditationStatusType, {
        as: 'AccreditationStatusType',
        foreignKey: 'AccreditationStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Company.belongsTo(CompanyType, {
        as: 'CompanyType',
        foreignKey: 'CompanyTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Company.belongsTo(IndustryType, {
        as: 'IndustryType',
        foreignKey: 'IndustryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Company.belongsTo(PartyRole, {
        as: 'LastUpdatedByPartyRole',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Company.belongsTo(MainBusinessActivityType, {
        as: 'MainBusinessActivity',
        foreignKey: 'MainBusinessActivityID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Company.belongsTo(Party, {
        as: 'Party',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    Company.belongsTo(State, {
        as: 'RegisteredInState',
        foreignKey: 'RegisteredInStateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
