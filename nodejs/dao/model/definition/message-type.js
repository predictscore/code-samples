'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('MessageType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(512),
            field: 'Name',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'MessageType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var MessageType = model.MessageType;
    var Message = model.Message;
    var MessagePriorityType = model.MessagePriorityType;
    var SiteType = model.SiteType;

    MessageType.hasMany(Message, {
        as: 'MessageMessagetypeFks',
        foreignKey: 'MessageTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    MessageType.belongsToMany(MessagePriorityType, {
        as: 'MessageMessagePriorityTypes',
        through: Message,
        foreignKey: 'MessageTypeID',
        otherKey: 'MessagePriorityTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    MessageType.belongsToMany(Message, {
        as: 'MessageParents',
        through: Message,
        foreignKey: 'MessageTypeID',
        otherKey: 'ParentID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    MessageType.belongsToMany(SiteType, {
        as: 'MessageReadFromSiteTypes',
        through: Message,
        foreignKey: 'MessageTypeID',
        otherKey: 'ReadFromSiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    MessageType.belongsToMany(SiteType, {
        as: 'MessageSentFromSiteTypes',
        through: Message,
        foreignKey: 'MessageTypeID',
        otherKey: 'SentFromSiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
