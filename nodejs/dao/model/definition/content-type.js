'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('ContentType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'ContentType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var ContentType = model.ContentType;
    var DocumentLocationType = model.DocumentLocationType;

    ContentType.hasMany(DocumentLocationType, {
        as: 'DocumentLocationTypeContenttypeFks',
        foreignKey: 'ContentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
