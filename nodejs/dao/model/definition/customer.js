'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Customer', {
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: true
        },
        partyRoleID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleID',
            allowNull: true
        },
        partyRoleTypeID: {
            type: DataTypes.INTEGER,
            field: 'PartyRoleTypeID',
            allowNull: true
        },
        partyRoleTypeName: {
            type: DataTypes.STRING(100),
            field: 'PartyRoleTypeName',
            allowNull: true
        },
        nameSurnameFirst: {
            type: DataTypes.STRING,
            field: 'NameSurnameFirst',
            allowNull: true
        },
        name: {
            type: DataTypes.STRING,
            field: 'Name',
            allowNull: true
        },
        homeEmail: {
            type: DataTypes.STRING(250),
            field: 'HomeEmail',
            allowNull: true
        },
        workEmail: {
            type: DataTypes.STRING(250),
            field: 'WorkEmail',
            allowNull: true
        },
        homeMobile: {
            type: DataTypes.STRING(20),
            field: 'HomeMobile',
            allowNull: true
        },
        workMobile: {
            type: DataTypes.STRING(20),
            field: 'WorkMobile',
            allowNull: true
        },
        homePhone: {
            type: DataTypes.STRING(20),
            field: 'HomePhone',
            allowNull: true
        },
        workPhone: {
            type: DataTypes.STRING(20),
            field: 'WorkPhone',
            allowNull: true
        },
        dateOfBirth: {
            type: DataTypes.DATEONLY,
            field: 'DateOfBirth',
            allowNull: true
        },
        physicalAddressID: {
            type: DataTypes.INTEGER,
            field: 'PhysicalAddressID',
            allowNull: true
        },
        physicalAddressString: {
            type: DataTypes.STRING(500),
            field: 'PhysicalAddressString',
            allowNull: true
        },
        postalAddressID: {
            type: DataTypes.INTEGER,
            field: 'PostalAddressID',
            allowNull: true
        },
        postalAddressString: {
            type: DataTypes.STRING(500),
            field: 'PostalAddressString',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'Customer',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var Customer = model.Customer;

};
