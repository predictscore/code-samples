'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('HistoryNote', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        historyNoteTypeID: {
            type: DataTypes.INTEGER,
            field: 'HistoryNoteTypeID',
            allowNull: false,
            references: {
                model: 'HistoryNoteType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        historyNoteStatusTypeID: {
            type: DataTypes.INTEGER,
            field: 'HistoryNoteStatusTypeID',
            allowNull: false,
            references: {
                model: 'HistoryNoteStatusType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: false,
            references: {
                model: 'Party',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        followUp: {
            type: DataTypes.DATE,
            field: 'FollowUp',
            allowNull: true
        },
        followUpByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'FollowUpByPartyRoleID',
            allowNull: true,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        title: {
            type: DataTypes.STRING(100),
            field: 'Title',
            allowNull: false
        },
        note: {
            type: DataTypes.TEXT,
            field: 'Note',
            allowNull: false
        },
        cRMGuid: {
            type: DataTypes.UUID,
            field: 'CRMGuid',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'HistoryNote',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var HistoryNote = model.HistoryNote;
    var PartyRole = model.PartyRole;
    var HistoryNoteStatusType = model.HistoryNoteStatusType;
    var HistoryNoteType = model.HistoryNoteType;
    var Party = model.Party;

    HistoryNote.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    HistoryNote.belongsTo(PartyRole, {
        as: 'FollowUpByPartyRole',
        foreignKey: 'FollowUpByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    HistoryNote.belongsTo(HistoryNoteStatusType, {
        as: 'HistoryNoteStatusType',
        foreignKey: 'HistoryNoteStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    HistoryNote.belongsTo(HistoryNoteType, {
        as: 'HistoryNoteType',
        foreignKey: 'HistoryNoteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    HistoryNote.belongsTo(Party, {
        as: 'Party',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
