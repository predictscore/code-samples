'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('TrustPurposeType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'TrustPurposeType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var TrustPurposeType = model.TrustPurposeType;
    var Trust = model.Trust;
    var PartyRole = model.PartyRole;
    var Country = model.Country;
    var DocumentationType = model.DocumentationType;
    var Party = model.Party;
    var TrustStructureType = model.TrustStructureType;

    TrustPurposeType.hasMany(Trust, {
        as: 'TrustTrustpurposetypeFks',
        foreignKey: 'TrustPurposeTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TrustPurposeType.belongsToMany(PartyRole, {
        as: 'TrustAccountantPartyRoles',
        through: Trust,
        foreignKey: 'TrustPurposeTypeID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TrustPurposeType.belongsToMany(Country, {
        as: 'TrustCountryEstablisheds',
        through: Trust,
        foreignKey: 'TrustPurposeTypeID',
        otherKey: 'CountryEstablishedID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TrustPurposeType.belongsToMany(DocumentationType, {
        as: 'TrustDocumentationTypes',
        through: Trust,
        foreignKey: 'TrustPurposeTypeID',
        otherKey: 'DocumentationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TrustPurposeType.belongsToMany(PartyRole, {
        as: 'TrustLastUpdatedByPartyRoles',
        through: Trust,
        foreignKey: 'TrustPurposeTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TrustPurposeType.belongsToMany(Party, {
        as: 'TrustParties',
        through: Trust,
        foreignKey: 'TrustPurposeTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    TrustPurposeType.belongsToMany(TrustStructureType, {
        as: 'TrustTrustStructureTypes',
        through: Trust,
        foreignKey: 'TrustPurposeTypeID',
        otherKey: 'TrustStructureTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
