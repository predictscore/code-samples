'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('SocialMediaAccount', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        detail: {
            type: DataTypes.STRING(250),
            field: 'Detail',
            allowNull: false
        },
        socialMediaTypeID: {
            type: DataTypes.INTEGER,
            field: 'SocialMediaTypeID',
            allowNull: false,
            references: {
                model: 'SocialMediaType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: false,
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'SocialMediaAccount',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var SocialMediaAccount = model.SocialMediaAccount;
    var SocialMediaType = model.SocialMediaType;
    var PartyRole = model.PartyRole;
    var PartySocialMedia = model.PartySocialMedia;
    var CommunicationType = model.CommunicationType;
    var Party = model.Party;

    SocialMediaAccount.hasMany(PartySocialMedia, {
        foreignKey: 'SocialMediaAccountID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SocialMediaAccount.belongsTo(PartyRole, {
        as: 'PartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SocialMediaAccount.belongsTo(SocialMediaType, {
        as: 'SocialMediaType',
        foreignKey: 'SocialMediaTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SocialMediaAccount.belongsToMany(CommunicationType, {
        as: 'PartySocialMediaCommunicationTypes',
        through: PartySocialMedia,
        foreignKey: 'SocialMediaAccountID',
        otherKey: 'CommunicationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SocialMediaAccount.belongsToMany(PartyRole, {
        as: 'PartySocialMediaLastUpdated',
        through: PartySocialMedia,
        foreignKey: 'SocialMediaAccountID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SocialMediaAccount.belongsToMany(PartyRole, {
        as: 'PartySocialMediaPartyRole',
        through: PartySocialMedia,
        foreignKey: 'SocialMediaAccountID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SocialMediaAccount.belongsToMany(PartyRole, {
        as: 'PartySocialMediaPartyRole',
        through: PartySocialMedia,
        foreignKey: 'SocialMediaAccountID',
        otherKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SocialMediaAccount.belongsToMany(Party, {
        as: 'PartySocialMediaParty',
        through: PartySocialMedia,
        foreignKey: 'SocialMediaAccountID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SocialMediaAccount.belongsToMany(Party, {
        as: 'PartySocialMediaParty',
        through: PartySocialMedia,
        foreignKey: 'SocialMediaAccountID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });
};
