'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('PartyScoreHistory', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: false,
            references: {
                model: 'Party',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        bureauScore: {
            type: DataTypes.STRING(20),
            field: 'BureauScore',
            allowNull: true
        },
        firstmacScore: {
            type: DataTypes.INTEGER,
            field: 'FirstmacScore',
            allowNull: true
        },
        oldBureauScore: {
            type: DataTypes.STRING(20),
            field: 'OldBureauScore',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'PartyScoreHistory',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var PartyScoreHistory = model.PartyScoreHistory;
    var Party = model.Party;

    PartyScoreHistory.belongsTo(Party, {
        as: 'Party',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
