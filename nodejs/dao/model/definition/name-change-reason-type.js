'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('NameChangeReasonType', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: false
        },
        sortOrder: {
            type: DataTypes.INTEGER,
            field: 'SortOrder',
            allowNull: false
        },
        active: {
            type: DataTypes.BOOLEAN,
            field: 'Active',
            allowNull: false
        }
    }, {
        schema: 'public',
        tableName: 'NameChangeReasonType',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var NameChangeReasonType = model.NameChangeReasonType;
    var Person = model.Person;
    var PartyRole = model.PartyRole;
    var GenderType = model.GenderType;
    var MaritalStatusType = model.MaritalStatusType;
    var ObjectColourType = model.ObjectColourType;
    var Party = model.Party;
    var Country = model.Country;
    var SalutationType = model.SalutationType;
    var State = model.State;

    NameChangeReasonType.hasMany(Person, {
        as: 'PersonNamechangereasontypeFks',
        foreignKey: 'NameChangeReasonTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    NameChangeReasonType.belongsToMany(PartyRole, {
        as: 'PersonAccountantPartyRoles',
        through: Person,
        foreignKey: 'NameChangeReasonTypeID',
        otherKey: 'AccountantPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    NameChangeReasonType.belongsToMany(GenderType, {
        as: 'PersonGenderTypes',
        through: Person,
        foreignKey: 'NameChangeReasonTypeID',
        otherKey: 'GenderTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    NameChangeReasonType.belongsToMany(PartyRole, {
        as: 'PersonLastUpdatedByPartyRoles',
        through: Person,
        foreignKey: 'NameChangeReasonTypeID',
        otherKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    NameChangeReasonType.belongsToMany(MaritalStatusType, {
        as: 'PersonMaritalStatusTypes',
        through: Person,
        foreignKey: 'NameChangeReasonTypeID',
        otherKey: 'MaritalStatusTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    NameChangeReasonType.belongsToMany(ObjectColourType, {
        as: 'PersonMedicareCardColourTypes',
        through: Person,
        foreignKey: 'NameChangeReasonTypeID',
        otherKey: 'MedicareCardColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    NameChangeReasonType.belongsToMany(Party, {
        as: 'PersonParties',
        through: Person,
        foreignKey: 'NameChangeReasonTypeID',
        otherKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    NameChangeReasonType.belongsToMany(ObjectColourType, {
        as: 'PersonPassportColourTypes',
        through: Person,
        foreignKey: 'NameChangeReasonTypeID',
        otherKey: 'PassportColourTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    NameChangeReasonType.belongsToMany(Country, {
        as: 'PersonPassportCountries',
        through: Person,
        foreignKey: 'NameChangeReasonTypeID',
        otherKey: 'PassportCountryID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    NameChangeReasonType.belongsToMany(SalutationType, {
        as: 'PersonSalutationTypes',
        through: Person,
        foreignKey: 'NameChangeReasonTypeID',
        otherKey: 'SalutationTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    NameChangeReasonType.belongsToMany(State, {
        as: 'PersonStates',
        through: Person,
        foreignKey: 'NameChangeReasonTypeID',
        otherKey: 'StateID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
