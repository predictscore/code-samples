'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('TempCreditProvider', {
        creditProviderID: {
            type: DataTypes.INTEGER,
            field: 'CreditProviderID',
            allowNull: true
        },
        name: {
            type: DataTypes.STRING(200),
            field: 'Name',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'TempCreditProvider',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var TempCreditProvider = model.TempCreditProvider;

};
