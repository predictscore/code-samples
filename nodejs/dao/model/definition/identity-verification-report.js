'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('IdentityVerificationReport', {
        iD: {
            type: DataTypes.INTEGER,
            field: 'ID',
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        identityVerificationReportTypeID: {
            type: DataTypes.INTEGER,
            field: 'IdentityVerificationReportTypeID',
            allowNull: false,
            references: {
                model: 'IdentityVerificationReportType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        partyID: {
            type: DataTypes.INTEGER,
            field: 'PartyID',
            allowNull: false,
            references: {
                model: 'Party',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        identityVerified: {
            type: DataTypes.BOOLEAN,
            field: 'IdentityVerified',
            allowNull: false
        },
        created: {
            type: DataTypes.DATE,
            field: 'Created',
            allowNull: false
        },
        createdByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'CreatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        lastUpdated: {
            type: DataTypes.DATE,
            field: 'LastUpdated',
            allowNull: false
        },
        lastUpdatedByPartyRoleID: {
            type: DataTypes.INTEGER,
            field: 'LastUpdatedByPartyRoleID',
            allowNull: false,
            references: {
                model: 'PartyRole',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'IdentityVerificationReport',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var IdentityVerificationReport = model.IdentityVerificationReport;
    var IdentityVerificationReportComponentAnalysis = model.IdentityVerificationReportComponentAnalysis;
    var IdentityVerificationReportComponentRule = model.IdentityVerificationReportComponentRule;
    var IdentityVerificationReportComponentSearch = model.IdentityVerificationReportComponentSearch;
    var IdentityVerificationReportComponentVerification = model.IdentityVerificationReportComponentVerification;
    var IdentityVerificationReportMain = model.IdentityVerificationReportMain;
    var PartyRole = model.PartyRole;
    var IdentityVerificationReportType = model.IdentityVerificationReportType;
    var Party = model.Party;
    var IdentityVerificationReportComponentAnalysisContributingValueTyp = model.IdentityVerificationReportComponentAnalysisContributingValueTyp;
    var IdentityVerificationReportComponentSearchNameType = model.IdentityVerificationReportComponentSearchNameType;
    var IdentityVerificationReportComponentAnalysisCategoryType = model.IdentityVerificationReportComponentAnalysisCategoryType;
    var IdentityVerificationReportComponentRuleNameType = model.IdentityVerificationReportComponentRuleNameType;
    var IdentityVerificationReportComponentRuleReasonType = model.IdentityVerificationReportComponentRuleReasonType;
    var IdentityVerificationReportComponentSearchMatchIndicatorType = model.IdentityVerificationReportComponentSearchMatchIndicatorType;
    var IdentityVerificationReportComponentSearchResultCodeType = model.IdentityVerificationReportComponentSearchResultCodeType;
    var IdentityVerificationReportComponentSearchType = model.IdentityVerificationReportComponentSearchType;
    var IdentityVerificationReportOutcomeType = model.IdentityVerificationReportOutcomeType;
    var IdentityVerificationReportProfileType = model.IdentityVerificationReportProfileType;

    IdentityVerificationReport.hasMany(IdentityVerificationReportComponentAnalysis, {
        as: 'FKIdentityverificationreportcomponentanalysisIdentityverificas',
        foreignKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.hasMany(IdentityVerificationReportComponentRule, {
        as: 'FKIdentityverificationreportcomponentrulesIdentityverificatios',
        foreignKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.hasMany(IdentityVerificationReportComponentSearch, {
        as: 'FKIdentityverificationreportcomponentsearchesIdentityverificas',
        foreignKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.hasMany(IdentityVerificationReportComponentVerification, {
        as: 'FKIdentityverificationreportcomponentverificationIdentityveris',
        foreignKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.hasMany(IdentityVerificationReportMain, {
        as: 'FKIdentityverificationreportmainIdentityverificationreportmais',
        foreignKey: 'IdentityVerificationReportID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsTo(PartyRole, {
        as: 'CreatedByPartyRole',
        foreignKey: 'CreatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsTo(IdentityVerificationReportType, {
        as: 'IdentityVerificationReportType',
        foreignKey: 'IdentityVerificationReportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsTo(PartyRole, {
        as: 'LastUpdatedByPartyRole',
        foreignKey: 'LastUpdatedByPartyRoleID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsTo(Party, {
        as: 'Party',
        foreignKey: 'PartyID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePhoneTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'ContributePhoneTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeTransactionTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'ContributeTransactionTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentAnalysisSearchNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'SearchNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentAnalysisCategoryType, {
        as: 'IdentityVerificationReportComponentAnalysisCategoryTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'CategoryTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeAddressTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'ContributeAddressTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDateOfBirthTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'ContributeDateOfBirthTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDocumentTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'ContributeDocumentTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeDriversTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'ContributeDriversTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeEntitlementTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'ContributeEntitlementTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeMultipleTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'ContributeMultipleTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributeNameTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'ContributeNameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentAnalysisContributingValueTyp, {
        as: 'IdentityVerificationReportComponentAnalysisContributePassportTypes',
        through: IdentityVerificationReportComponentAnalysis,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'ContributePassportTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentRuleNameType, {
        as: 'IdentityVerificationReportComponentRuleNameTypes',
        through: IdentityVerificationReportComponentRule,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'NameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentRuleReasonType, {
        as: 'IdentityVerificationReportComponentRuleReasonTypes',
        through: IdentityVerificationReportComponentRule,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'ReasonTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentSearchMatchIndicatorType, {
        as: 'IdentityVerificationReportComponentSearchMatchIndicatorTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'MatchIndicatorTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentSearchNameType, {
        as: 'IdentityVerificationReportComponentSearchNameTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'NameTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentSearchResultCodeType, {
        as: 'IdentityVerificationReportComponentSearchResultCodeTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'ResultCodeTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportComponentSearchType, {
        as: 'IdentityVerificationReportComponentSearchSearchTypes',
        through: IdentityVerificationReportComponentSearch,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'SearchTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportOutcomeType, {
        as: 'IdentityVerificationReportComponentVerificationIndicatorTypes',
        through: IdentityVerificationReportComponentVerification,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'IndicatorTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportOutcomeType, {
        as: 'IdentityVerificationReportMainOverallOutcomes',
        through: IdentityVerificationReportMain,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'OverallOutcomeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    IdentityVerificationReport.belongsToMany(IdentityVerificationReportProfileType, {
        as: 'IdentityVerificationReportMainProfileTypes',
        through: IdentityVerificationReportMain,
        foreignKey: 'IdentityVerificationReportID',
        otherKey: 'ProfileTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
