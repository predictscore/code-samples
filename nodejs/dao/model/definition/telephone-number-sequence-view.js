'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('TelephoneNumberSequenceView', {
        a: {
            type: DataTypes.BIGINT,
            field: 'a',
            allowNull: true
        }
    }, {
        schema: 'public',
        tableName: 'TelephoneNumberSequenceView',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var TelephoneNumberSequenceView = model.TelephoneNumberSequenceView;

};
