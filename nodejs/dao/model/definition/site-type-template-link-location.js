'use strict';

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('SiteTypeTemplateLinkLocation', {
        siteTypeID: {
            type: DataTypes.INTEGER,
            field: 'SiteTypeID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'SiteType',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        },
        templateName: {
            type: DataTypes.STRING(200),
            field: 'TemplateName',
            allowNull: false,
            primaryKey: true
        },
        linkLocationID: {
            type: DataTypes.INTEGER,
            field: 'LinkLocationID',
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'LinkLocation',
                key: 'ID'
            },
            onUpdate: 'NO ACTION',
            onDelete: 'NO ACTION'
        }
    }, {
        schema: 'public',
        tableName: 'SiteTypeTemplateLinkLocation',
        timestamps: false
    });
};

module.exports.initRelations = function() {
    delete module.exports.initRelations; // Destroy itself to prevent repeated calls.
    var model = require('../index');
    var SiteTypeTemplateLinkLocation = model.SiteTypeTemplateLinkLocation;
    var LinkLocation = model.LinkLocation;
    var SiteType = model.SiteType;

    SiteTypeTemplateLinkLocation.belongsTo(LinkLocation, {
        as: 'LinkLocation',
        foreignKey: 'LinkLocationID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

    SiteTypeTemplateLinkLocation.belongsTo(SiteType, {
        as: 'SiteType',
        foreignKey: 'SiteTypeID',
        onDelete: 'NO ACTION',
        onUpdate: 'NO ACTION'
    });

};
