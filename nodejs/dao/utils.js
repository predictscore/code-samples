const dao = require("./conn");
const _ = require('lodash');
Q = require('q')

module.exports.partyNameByParty = function (party) {
  return (
    (_.get(party, 'PersonPartyFks[0].firstName', '') || '')  + ' ' +
    (_.get(party, 'PersonPartyFks[0].middleName', '') || '') + ' ' +
    (_.get(party, 'PersonPartyFks[0].lastName', '') || '')).trim().replace(/  +/g, ' ') ||
    _.get(party, 'CompanyPartyFks[0].companyName', '') || _.get(party, 'TrustPartyFks[0].trustName', '');
};

module.exports.getRoles = function(user){
  return Q([]);
}

module.exports.getPerms = function(user){
  return Q([]);
}
