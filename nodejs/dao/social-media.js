"use strict";

/** Social media data access
 * @module dao/social */

const Q = require("q");

const dao = require("./conn"),
  common = require("fm-common"),
  logger = common.logger;

/** Saves a social media to the database
 * @func save
 * @param socialMediaAccount {Object} The data object to save
 * @param userAdapter {Object} The user invoking this function */
module.exports.save = function (socialMediaAccount, userAdapter) {

  let when = new Date(),
    who = userAdapter.getPartyRoleId();

  return dao.crumbs.SocialMediaAccount.findOne({
    where: { detail: socialMediaAccount.detail }
  }).then(existingSocialMediaAccount => {

    // if we've already found one, then we're not updating; we'll just use it
    if (existingSocialMediaAccount &&
      existingSocialMediaAccount.dataValues) {
      socialMediaAccount.iD = existingSocialMediaAccount.dataValues.iD;
      existingSocialMediaAccount.updateAttributes(socialMediaAccount);
      return existingSocialMediaAccount.dataValues.iD;
    }

    // set the creational and mutation attributes
    delete socialMediaAccount.iD;
    socialMediaAccount.created = when;
    socialMediaAccount.createdByPartyRoleID = who;
    socialMediaAccount.lastUpdated = when;
    socialMediaAccount.lastUpdatedByPartyRoleID = who;

    // no existing record was found, so we're going to build the write request
    return dao.crumbs.SocialMediaAccount.build(socialMediaAccount)
      .save()
      .then(createdSocialMediaAccount => {
        return createdSocialMediaAccount.iD;
      });


  });


};