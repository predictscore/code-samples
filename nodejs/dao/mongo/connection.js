"use strict"


var config    = require('config'),
    logger    = require('fm-common').logger,
    mongoose  = require('mongoose'),
    connection = require('./connection');

function _initConnection(connection, cstr) {


  /* this control variable tells this module if the data connection has been closed by
     choice (meaning, it's closed because it received an interrupt from the operating
     system). When this isn't the case, the dao module is going to try and re-connect */
  var closedByChoice = false;

  connection.on('connected', function () {
    logger.info(`Mongo has connected`);
  });

  connection.on('error', function (err) {
    logger.fatal(`Mongo is under error`);
    logger.error(err);
  });

  connection.on('disconnected', function () {

    if (!closedByChoice) {

      setTimeout(function () {
        logger.info('Mongo has disconnected, attempting reconnect . . .');
        connection.open(cstr);
      }, 1000);

    } else {

      logger.info('Mongo has disconnected under expected conditions');

    }

  });

  process.on('SIGINT', function() {

    closedByChoice = true;

    connection.close(function () {
      logger.info('Mongoose connection disconnected through app termination');
      process.exit(0);
    });

  });

  return connection;
}

module.exports = {
  initConnection:_initConnection
};
