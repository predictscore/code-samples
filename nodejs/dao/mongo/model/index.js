"use strict"

var schemas = require('../schema'),
    mongoose = require('mongoose');

mongoose.model("SmsTemplate", schemas.smsTemplate, "smsTemplates");
mongoose.model("EmailTemplate", schemas.emailTemplate, "emailTemplates");
mongoose.model("Emails", schemas.emails, "emails");
mongoose.model("FlyerTemplate", schemas.flyerTemplate, "flyerTemplates");

module.exports = {
  smsTemplate:mongoose.model("SmsTemplate"),
  emails:mongoose.model("Emails"),
  emailTemplate:mongoose.model("EmailTemplate"),
  flyerTemplate:mongoose.model("FlyerTemplate")
}
