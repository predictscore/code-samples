'use strict'


var mongoose = require('mongoose'),
    mongo = require(".."),
    common = require('./common');

var ObjectId  = mongoose.Schema.Types.ObjectId;

var options = {
  versionKey: false,          /* no version key on sub-objects */
  collection: "smsTemplates"
};

var smsTemplateSchema = mongoose.Schema({
  name: String,
  tags: [String],
  to: {type:common.address, required:false},
  message: String,
  created: {type:common.userDate, required:false},
  updated: {type:common.userDate, required:false}
}, options)


module.exports = {
  smsTemplate: smsTemplateSchema
};
