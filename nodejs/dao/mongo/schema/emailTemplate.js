'use strict'


var mongoose = require('mongoose'),
  mongo = require(".."),
  common = require('./common');

var Mixed = mongoose.Schema.Types.Mixed;

var options = {
    versionKey: false,          /* no version key on sub-objects */
    collection: "emailTemplates"
};

var nestedObjectOptions = {
    versionKey:false,
    _id:false
};

var crumbsAddressSchema = mongoose.Schema({
    emailTypeId: Number,
    locationTypeId: Number,
    communicationTypeId: Number
}, nestedObjectOptions);

var literalAddressSchema = mongoose.Schema({
    emailAddress: String,
    displayName: String
}, nestedObjectOptions);

var modelAddressSchema = mongoose.Schema({
    emailProperty: String,
    displayNameProperty: String
}, nestedObjectOptions);

var addressSchema = mongoose.Schema({
    crumbs:{type:crumbsAddressSchema, required:false},
    literal:{type:literalAddressSchema, required:false},
    model:{type:modelAddressSchema, required:false}
}, nestedObjectOptions);

var emailTemplateSchema = mongoose.Schema({
    name: String,
    tags: [String],
    headers: Mixed,
    from: addressSchema,
    to: [addressSchema],
    cc: [addressSchema],
    bcc: [addressSchema],
    subject: String,
    isHtml: Boolean,
    body: String,
    attachments: [common.attachment],
    brand: {type:common.brand, required:false},
    created: common.userDate,
    updated: common.userDate
}, options);

module.exports = {
    emailTemplate: emailTemplateSchema
};
