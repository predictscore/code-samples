'use strict';

var mongoose = require('mongoose'),
    mongo = require(".."),
    common = require('./common');

var ObjectId  = mongoose.Schema.Types.ObjectId;

var options = {
    versionKey: false,          /* no version key on sub-objects */
    collection: "flyerTemplates"
};

var flyerTemplateSchema = mongoose.Schema({
    name: String,
    tags: [String],
    created: {type:common.userDate, required:false},
    updated: {type:common.userDate, required:false},
    file: ObjectId,
    sharedUsers: [common.idValue]
}, options);


module.exports = {
    flyerTemplate: flyerTemplateSchema
};
