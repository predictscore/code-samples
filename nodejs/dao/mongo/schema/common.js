'use strict'


var mongoose = require('mongoose'),
    mongo = require(".."),
    ObjectId  = mongoose.Schema.Types.ObjectId,
    Buffer = mongoose.Schema.Types.Buffer;

var options = {
  versionKey:false,
  _id:false
};

var crumbsAddressSchema = mongoose.Schema({
  phoneTypeId: Number,
  locationTypeId: Number,
  communicationTypeId: Number
}, options);

var literalAddressSchema = mongoose.Schema({
  mobileNumber:String
}, options);

var modelAddressSchema = mongoose.Schema({
  numberProperty:String
}, options);

var localSchema = mongoose.Schema({
  name:String,
  mediaType:String,
  fileId:ObjectId,
  data:Buffer
}, options);

var remoteSchema = mongoose.Schema({
  url:String
}, options);

var addressSchema = mongoose.Schema({
  crumbs:{type:crumbsAddressSchema, required:false},
  literal:{type:literalAddressSchema, required:false},
  model:{type:modelAddressSchema, required:false}
}, options);

var idValueSchema = mongoose.Schema({
  id:Number,
  name:String
}, options);

var userDateSchema = mongoose.Schema({
  date:{type:Date, required:false},
  user:{type:idValueSchema, required:false}
}, options);

var attachmentSchema = mongoose.Schema({
  local:{type:localSchema, required:false},
  remote:{type:remoteSchema, required:false}
}, options);

var brandSchema = mongoose.Schema({

}, options);

var emailAddressSchema = mongoose.Schema({
    name:String,
    address:String
}, options);

var emailAttachmentSchema = mongoose.Schema({
    name:String,
    mediaType:String,
    fileId:ObjectId
}, options);

var attachmentSchema = mongoose.Schema({
  local:{type:localSchema, required:false},
  remote:{type:remoteSchema, required:false}
}, options);

var brandSchema = mongoose.Schema({
}, options);

module.exports = {
  address:addressSchema,
  userDate:userDateSchema,
  emailAddress:emailAddressSchema,
  emailAttachment:emailAttachmentSchema,
  attachment:attachmentSchema,
  brand:brandSchema,
  idValue:idValueSchema
};
