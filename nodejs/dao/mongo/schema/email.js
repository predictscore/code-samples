'use strict'


var mongoose = require('mongoose'),
    mongo = require(".."),
    common = require('./common');

var ObjectId  = mongoose.Schema.Types.ObjectId,
    Mixed = mongoose.Schema.Types.Mixed;

var options = {
    versionKey: false,          /* no version key on sub-objects */
    collection: "emails"
};

var emailSchema = mongoose.Schema({
    partyId: {type:Number, required:false},
    templateId: ObjectId,
    headers: Mixed,
    from: common.emailAddress,
    to: [common.emailAddress],
    cc: [common.emailAddress],
    bcc: [common.emailAddress],
    subject: String,
    isHtml: Boolean,
    body: String,
    attachments: [common.emailAttachment],
    sent: common.userDate
}, options);


module.exports = {
    emails: emailSchema
};
