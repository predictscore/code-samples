"use strict"

module.exports = {
  common:require('./common'),
  smsTemplate:require('./smsTemplate'),
  emails:require('./email'),
  emailTemplate:require('./emailTemplate'),
  flyerTemplate: require('./flyerTemplate')
};
