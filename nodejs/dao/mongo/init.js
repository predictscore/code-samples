"use strict"

var config    = require('config'),
    mongoose  = require('mongoose'),
    connection = require('./connection');

var mongoConfig = config.get('mongoDb');
var Grid = require('gridfs-stream');

if (mongoConfig.debug) {
  mongoose.set('debug', true);
}
mongoose.Promise = require('q').Promise;

mongoose.connect(mongoConfig.connectionStrings.crumbs);
var crumbsConnection = mongoose.connection;

module.exports = {
  crumbs: connection.initConnection(crumbsConnection, mongoConfig.connectionStrings.crumbs),
  grid: Grid(crumbsConnection.db, mongoose.mongo)
};
