"use strict";

/** Email address data access
  * @module dao/address */

const Q = require("q");

const dao = require("./conn"),
    common = require("fm-common"),
    logger = common.logger;

/** Saves a email address to the database
  * @func save
  * @param emailAddress {Object} The data object to save
  * @param userAdapter {Object} The user invoking this function */
module.exports.save = function (emailAddress, userAdapter) {

  let when = new Date(),
      who = userAdapter.getPartyRoleId();

  /* check if only the id has been sent through; because this will force us to 
     not perform any data work */
  if (!emailAddress.emailAddress) {
    return Q(emailAddress.iD);
  }

  return dao.crumbs.EmailAddress.findOne({
    where: { emailAddress: emailAddress.emailAddress },
    attributes: [ 'iD' ]
  }).then(existingEmailAddress => {

    // if we've already found one, then we're not updating; we'll just use it
    if (existingEmailAddress && 
        existingEmailAddress.dataValues && 
        existingEmailAddress.dataValues.iD) {
      return existingEmailAddress.dataValues.iD;
    }

    // set the creational and mutation attributes
    delete emailAddress.iD;
    emailAddress.created = when;
    emailAddress.createdByPartyRoleID = who;
    emailAddress.lastUpdated = when;
    emailAddress.lastUpdatedByPartyRoleID = who;

    // no existing record was found, so we're going to build the write request
    return dao.crumbs.EmailAddress.build(emailAddress)
      .save()
      .then(createdEmailAddress => {
        return createdEmailAddress.iD;
      });


  });


};