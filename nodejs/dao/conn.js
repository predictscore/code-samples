"use strict";

/** Data connection module
  * @module dao/conn */

const config  = require('config'),
    _     = require('lodash'),
    Sequelize = require('sequelize'),
    logger  = require('fm-common').logger;

let crumbsDb = config.get('crumbsDb');

let override = require('./override');

let staticOptions = {

  "logging": function (x) {
    logger.info(x);
  }

};

const sequelize = new Sequelize(
  crumbsDb.connectionString,
  _.merge(crumbsDb.options, staticOptions)
);

module.exports = {
  sequelize: sequelize,
  crumbs: override(require('./model/index.js').init(sequelize)),
};