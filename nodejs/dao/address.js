"use strict";

/** Address data access
  * @module dao/address */

const Q = require("q");

const dao = require("./conn"),
	  common = require("fm-common"),
	  logger = common.logger;


/** Saves an Address record to the database
  * @func save
  * @param address {Object} The data object to save
  * @param userAdapter {Object} The user performing this action
  * @returns {Integer} The id of the Address record written */
module.exports.save = function (address, userAdapter) {

	let when = new Date(),
		who = userAdapter.getPartyRoleId();

	/* check if only the id has been sent through, because this will mean that the address
	   record has been found by other means; and we won't need to continue on */
	if (!address.propertyName && !address.unitNumber && !address.streetNumber &&
		!address.pOBox && !address.streetName && !address.streetTypeID &&
		!address.suburb && !address.stateID && !address.countryID &&
		!address.postCode && !address.streetTypeText && !address.stateText) {
		return Q(address.iD);
	}

	let match = {
		"propertyName": address.propertyName,
    "unitNumber": address.unitNumber,
    "streetNumber": address.streetNumber,
    "pOBox": address.pOBox,
    "streetName": address.streetName,
    "streetTypeID": address.streetTypeID,
    "suburb": address.suburb,
    "stateID": address.stateID,
    "countryID": address.countryID,
    "postCode": address.postCode,
    "streetTypeText": address.streetTypeText,
    "stateText": address.stateText
	};

	// try to find this address now
	return dao.crumbs.Address.findOne({
		where: match,
		attributes: ['iD']
	}).then(existingAddress => {
		// if we've already found one, then we're not updating; we'll just use it
		if (existingAddress && existingAddress.dataValues && existingAddress.dataValues.iD) {
			return existingAddress.dataValues.iD;
		}

		// set the creational and mutation attributes
		address.created = when;
		address.createdByPartyRoleID = who;
		address.lastUpdated = when;
		address.lastUpdatedByPartyRoleID = who;

		// no existing record was found, so we're going to build the write request
		return dao.crumbs.Address.build(address)
			.save()
			.then(createdAddress => {
				return createdAddress.iD;
			});

	});

}