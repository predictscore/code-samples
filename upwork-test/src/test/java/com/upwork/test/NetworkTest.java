package com.upwork.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.upwork.test.exception.InvalidParameterException;

public class NetworkTest {
	@Test
	public void shouldCreateNetwork() {
		int elementsCount = 12;
		Network network = new Network(elementsCount);
		assertEquals(network.getElementsCount(), elementsCount);
		assertEquals(network.getConnections().size(), elementsCount);
	}
	
	@Test(expected=InvalidParameterException.class)
	public void shouldThrowExceptionOnCreateNetwork() {
		int elementsCount = 1;
		new Network(elementsCount);
	}
	
	@Test
	public void shouldConnectNewPair() {
		int source = 3;
		int destination = 7;
		Network network = composeTestNetwork();
		network.connect(source, destination);
		assertEquals(network.getConnections().get(source).size(), 1);
		assertEquals(network.query(source, destination), true);
	}
	
	@Test(expected=InvalidParameterException.class)
	public void shouldNotConnectNewPairMinValueIsIncorrect() {
		int source = -1;
		int destination = 7;
		Network network = composeTestNetwork();
		network.connect(source, destination);
	}
	
	@Test(expected=InvalidParameterException.class)
	public void shouldNotConnectNewPairMaxValueIsIncorrect() {
		int source = 1;
		int destination = 72;
		Network network = composeTestNetwork();
		network.connect(source, destination);
	}
	
	@Test
	public void shouldFoundPath() {
		int source = 1;
		int destination = 2;
		Network network = composeTestNetwork();
		boolean hasConnection = network.query(source, destination);
		assertEquals(hasConnection, true);
	}
	
	@Test
	public void shouldNotFoundPath() {
		int source = 1;
		int destination = 7;
		Network network = composeTestNetwork();
		boolean hasConnection = network.query(source, destination);
		assertEquals(hasConnection, false);
	}
	
	private Network composeTestNetwork() {
		Network network = new Network(8);
		network.connect(1, 2);
		network.connect(6, 2);
		network.connect(2, 4);
		network.connect(5, 8);
		return network;
	}
}
