package com.upwork.test.exception;

public class InvalidParameterException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public InvalidParameterException() {
	}

	public InvalidParameterException(String message) {
		super(message);
	}
}
