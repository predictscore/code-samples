package com.upwork.test;

import com.upwork.test.exception.InvalidParameterException;

public class Main {
	public static void main(String[] args) {
		Network network;
		try {
			network = new Network(8);

			network.connect(1, 2);
			network.connect(6, 2);
			network.connect(2, 4);
			network.connect(5, 8);

			System.out.println(network.query(1, 2));
			System.out.println(network.query(1, 1));
			System.out.println(network.query(6, 2));

		} catch (InvalidParameterException e) {
			// TODO processing error (logging etc)
			System.out.println(e.getMessage());
		}
	}
}
