package com.upwork.test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.upwork.test.exception.InvalidParameterException;

public class Network {
	private int elementsCount;
	private Map<Integer, Set<Integer>> connections;

	public Network(int elementsCount) {
		if (elementsCount < 2) {
			throw new InvalidParameterException("Create ERROR: Elements count should be more than 1");
		}
		this.elementsCount = elementsCount;
		this.connections = new HashMap<>();
		composeEmptyNetwork();
	}

	private void composeEmptyNetwork() {
		for (int i = 1; i <= elementsCount; i++) {
			connections.put(i, new HashSet<>());
		}
	}

	public void connect(int source, int destination) {
		int min = Integer.min(source, destination);
		int max = Integer.max(source, destination);
		if (min < 1) {
			throw new InvalidParameterException("Connect ERROR: Element can not be less than 1");
		}
		if (max > elementsCount) {
			throw new InvalidParameterException("Connect ERROR: Element can not be more than elements count");
		}
		if (min == max) {
			throw new InvalidParameterException("Connect ERROR: Elements must be different");
		}
		connections.get(min).add(max);
	}

	public boolean query(int source, int destination) {
		int min = Integer.min(source, destination);
		int max = Integer.max(source, destination);
		if (min == max || min < 1 || max > elementsCount) {
			return false;
		}
		return connections.getOrDefault(min, new HashSet<>()).contains(max);
	}

	public int getElementsCount() {
		return elementsCount;
	}

	public Map<Integer, Set<Integer>> getConnections() {
		return connections;
	}
}
